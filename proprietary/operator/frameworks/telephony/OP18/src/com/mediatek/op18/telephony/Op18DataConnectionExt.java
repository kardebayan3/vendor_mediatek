/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op18.telephony;

import android.content.Context;
import android.text.TextUtils;
import android.telephony.Rlog;
import android.telephony.SubscriptionManager;

import com.android.internal.telephony.PhoneConstants;
import com.mediatek.internal.telephony.dataconnection.DataConnectionExt;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.telephony.MtkTelephonyManagerEx;

public class Op18DataConnectionExt extends DataConnectionExt {
    static final String TAG = "Op18DataConnectionExt";
    String[] mImsMccMncList = {"405840", "405854",
                                  "405855", "405856",
                                    "405857", "405858",
                                    "405859", "405860",
                                    "405861", "405862",
                                    "405863", "405864",
                                    "405865", "405866",
                                    "405867", "405868",
                                    "405869", "405870",
                                    "405871", "405872",
                                    "405873", "405874"};

    public Op18DataConnectionExt(Context context) {
        super(context);
    }

    @Override
    public boolean ignoreDataRoaming(String apnType) {
        if (TextUtils.equals(apnType, PhoneConstants.APN_TYPE_IMS)) {
            Rlog.d(TAG, "ignoreDataRoaming, apnType = " + apnType);
            return true;
        }
        return false;
    }

    @Override
    /**
     * To get mcc&mnc from IMPI, see TelephonyManagerEx.getIsimImpi().
     * @param defaultValue default value
     * @param phoneId phoneId used to get IMPI.
     * @return
     */
    public String getOperatorNumericFromImpi(String defaultValue, int phoneId) {
        final String mccTag = "mcc";
        final String mncTag = "mnc";
        final int mccLength = 3;
        final int mncLength = 3;
        Rlog.d(TAG, "getOperatorNumbericFromImpi got default mccmnc: " + defaultValue);
        //getMccMncList();
        //Log.d(TAG, "got mccMnc Array of size" + mImsMccMncList.length);
        if (mImsMccMncList == null  || mImsMccMncList.length == 0) {
            Rlog.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        String impi = null;
        int masterPhoneId = RadioCapabilitySwitchUtil.getMainCapabilityPhoneId();
        Rlog.d(TAG, "Impi requested by phoneId: " + phoneId);
        Rlog.d(TAG, "masterPhoneId:" + masterPhoneId);
        /*if (masterPhoneId != phoneId) {
            Log.d(TAG, "Request from Secondry Sim So Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }*/
        int subIds[] = SubscriptionManager.getSubId(phoneId);
        impi = MtkTelephonyManagerEx.getDefault().getIsimImpi(subIds[0]);
        Rlog.d(TAG, "Impi from subId: " + impi);
        /*impi = MtkTelephonyManagerEx.getDefault()
                .getIsimImpi(SubscriptionManager.getSubIdUsingPhoneId(phoneId));*/

        if (impi == null  || impi.equals("")) {
            Rlog.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        int mccPosition = impi.indexOf(mccTag);
        int mncPosition = impi.indexOf(mncTag);
        if (mccPosition == -1 || mncPosition == -1) {
            Rlog.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        String masterMccMnc = impi.substring(mccPosition + mccTag.length(), mccPosition
                + mccTag.length() + mccLength) + impi.substring(mncPosition + mncTag.length(),
                mncPosition + mncTag.length() + mncLength);
        Rlog.d(TAG, "master MccMnc: " + masterMccMnc);
        if (masterMccMnc == null || masterMccMnc.equals("")) {
            Rlog.d(TAG, "Returning default mccmnc: " + defaultValue);
            return defaultValue;
        }
        for (String mccMnc : mImsMccMncList) {
            if (masterMccMnc.equals(mccMnc)) {
                Rlog.d(TAG, "mccMnc matched:" + mccMnc);
                Rlog.d(TAG, "Returning mccmnc from IMPI: " + masterMccMnc);
                return masterMccMnc;
            }
        }
        Rlog.d(TAG, "Returning default mccmnc: " + defaultValue);
        return defaultValue;
    }

}
