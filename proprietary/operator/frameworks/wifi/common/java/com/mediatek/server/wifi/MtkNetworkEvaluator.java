package com.mediatek.server.wifi;

import android.annotation.Nullable;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.util.Log;
import android.util.Pair;

import com.android.server.wifi.*;
import com.android.server.wifi.util.TelephonyUtil;

import com.mediatek.server.wifi.MtkWifiService.AutoConnectManager;
import com.mediatek.server.wifi.WifiOperatorFactoryBase.IMtkWifiServiceExt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MtkNetworkEvaluator implements WifiNetworkSelector.NetworkEvaluator {
    private static final String NAME = "MtkNetworkEvaluator";
    private final Context mContext;
    private final WifiConfigManager mWifiConfigManager;
    private final MtkWifiService mService;

    public MtkNetworkEvaluator(
            Context context, WifiConfigManager configManager, MtkWifiService service) {
        mContext = context;
        mWifiConfigManager = configManager;
        mService = service;
    }

    /**
      * Get the evaluator name.
      */
    @Override
    public String getName() {
        return NAME;
    }

    /**
     * Update the evaluator.
     *
     * Certain evaluators have to be updated with the new scan results. For example
     * the ExternalScoreEvalutor needs to refresh its Score Cache.
     *
     * @param scanDetails    a list of scan details constructed from the scan results
     */
    @Override
    public void update(List<ScanDetail> scanDetails) {
        // do nothing
    }

    /**
     * Evaluate all the networks from the scan results.
     *
     * @param scanDetails    a list of scan details constructed from the scan results
     * @param currentNetwork configuration of the current connected network
     *                       or null if disconnected
     * @param currentBssid   BSSID of the current connected network or null if
     *                       disconnected
     * @param connected      a flag to indicate if WifiStateMachine is in connected
     *                       state
     * @param untrustedNetworkAllowed a flag to indidate if untrusted networks like
     *                                ephemeral networks are allowed
     * @param connectableNetworks     a list of the ScanDetail and WifiConfiguration
     *                                pair which is used by the WifiLastResortWatchdog
     * @return configuration of the chosen network;
     *         null if no network in this category is available.
     */
    @Override
    @Nullable
    public WifiConfiguration evaluateNetworks(List<ScanDetail> scanDetails,
                    WifiConfiguration currentNetwork, String currentBssid,
                    boolean connected, boolean untrustedNetworkAllowed,
                    List<Pair<ScanDetail, WifiConfiguration>> connectableNetworks) {

        // We do NOT evaluate networks here, only trigger ACM callbacks
        IMtkWifiServiceExt ext = mService.getOpExt();

        if (ext.hasNetworkSelection() == IMtkWifiServiceExt.OP_01) {
            MtkWifiService.log(NAME + ".evaluateNetworks: IMtkWifiServiceExt.OP_01");
            AutoConnectManager acm = mService.getACM();
            // 1. notify user if necessary
            // init this flag
            acm.setShowReselectDialog(false);
            if (acm.getScanForWeakSignal()) {
                // this scan is triggered due to weak signal, auto connect
                // a valid network/show reselection dialog
                acm.showReselectionDialog();
            }
            // reset this record which contains the last disconnected network
            // (aosp disconnection/disconnection due to poor RSSI)
            acm.clearDisconnectNetworkId();

        }
        
        return null;
    }
}
