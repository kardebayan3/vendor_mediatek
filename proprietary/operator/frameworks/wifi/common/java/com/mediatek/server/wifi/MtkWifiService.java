/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.server.wifi;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiScanner.ScanSettings;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiScanner;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.WorkSource;
import android.provider.Settings;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.util.Protocol;
import com.android.internal.util.IState;
import com.android.internal.util.State;
import com.android.internal.util.StateMachine;
import com.android.server.wifi.*;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader.OperatorFactoryInfo;
import com.mediatek.provider.MtkSettingsExt;
import com.mediatek.server.wifi.MtkWifiServiceAdapter.IMtkWifiService;
import com.mediatek.server.wifi.WifiOperatorFactoryBase;
import com.mediatek.server.wifi.WifiOperatorFactoryBase.IMtkWifiServiceExt;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static android.net.wifi.WifiConfiguration.INVALID_NETWORK_ID;
import static android.net.wifi.WifiManager.WIFI_AP_STATE_DISABLED;
import static android.net.wifi.WifiManager.WIFI_AP_STATE_DISABLING;
import static android.net.wifi.WifiManager.WIFI_AP_STATE_ENABLED;
import static android.net.wifi.WifiManager.WIFI_AP_STATE_ENABLING;
import static android.net.wifi.WifiManager.WIFI_AP_STATE_FAILED;
import static android.net.wifi.WifiManager.WIFI_STATE_DISABLED;
import static android.net.wifi.WifiManager.WIFI_STATE_DISABLING;
import static android.net.wifi.WifiManager.WIFI_STATE_ENABLED;
import static android.net.wifi.WifiManager.WIFI_STATE_ENABLING;
import static android.net.wifi.WifiManager.WIFI_STATE_UNKNOWN;
import static com.android.internal.util.StateMachine.HANDLED;
import static com.android.internal.util.StateMachine.NOT_HANDLED;

public final class MtkWifiService implements IMtkWifiService {
    private static final String TAG = "MtkWifiService";
    public static final String SYSUI_PACKAGE_NAME = "com.android.systemui";

    private static final boolean DEBUG = Log.isLoggable(TAG, Log.DEBUG);

    private IMtkWifiServiceExt mExt = null;
    private AutoConnectManager mACM = null;
    private static final List<OperatorFactoryInfo> sFactoryInfoList
        = new ArrayList<OperatorFactoryInfo>();

    private Context mContext = null;

    static {
        sFactoryInfoList.add(
            new OperatorFactoryInfo(
            "Op01WifiService.apk",
            "com.mediatek.op.wifi.Op01WifiOperatorFactory",
            "com.mediatek.server.wifi.op01",
            "OP01"
        ));
    }

    public synchronized IMtkWifiServiceExt getOpExt() {
        if (mExt == null) {
            WifiOperatorFactoryBase factory =
                    (WifiOperatorFactoryBase) OperatorCustomizationFactoryLoader
                            .loadFactory(mContext, sFactoryInfoList);
            if (factory == null) {
                factory = new WifiOperatorFactoryBase();
            }
            log("Factory is : " + factory.getClass());
            mExt = factory.createWifiFwkExt(mContext, this);
            mExt.init();
        }
        return mExt;
    }

    private BroadcastReceiver mOperatorReceiver = new MtkWifiOpReceiver();
    public class MtkWifiOpReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            log("mOperatorReceiver.onReceive: " + action);
            if (action.equals(IMtkWifiServiceExt.AUTOCONNECT_SETTINGS_CHANGE)) {
                getACM().updateAutoConnectSettings(sWsmAdapter.getLastNetworkId());
            } else if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                final int previousState = intent.getIntExtra(WifiManager.EXTRA_PREVIOUS_WIFI_STATE, -1);
                final int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, -1);
                log("previous state: " + previousState);
                log("previous state: " + state);

                AutoConnectManager acm = getACM();
                acm.setWaitForScanResult(false);
                acm.setShowReselectDialog(false);
                if (state == WIFI_STATE_ENABLING) {
                    if (getOpExt().hasCustomizedAutoConnect()) {
                        getACM().resetDisconnectNetworkStates();
                    }
                }

                updateWifiState(previousState, state);
            } else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo info = (NetworkInfo) intent.getExtra(WifiManager.EXTRA_NETWORK_INFO);
                DetailedState detailedState = info.getDetailedState();
                log("detailed state: " + detailedState);

                AutoConnectManager acm = getACM();
                if (detailedState == DetailedState.CONNECTED) {
                    acm.setWaitForScanResult(false);
                    acm.setDisconnectOperation(false);
                } else if (detailedState == DetailedState.DISCONNECTED) {
                    acm.handleNetworkDisconnect();
                    acm.setShowReselectDialog(false);
                    if (acm.getNetworkState() == DetailedState.CONNECTED) {
                        acm.setWaitForScanResult(false);
                        acm.handleWifiDisconnect();
                    }
                }
                // update states
                acm.setNetworkState(detailedState);
                acm.setLastNetworkId(sWsmAdapter.getLastNetworkId());
            } else if (action.equals(IMtkWifiServiceExt.ACTION_SUSPEND_NOTIFICATION)) {
                int type = intent.getIntExtra(IMtkWifiServiceExt.EXTRA_SUSPEND_TYPE, -1);
                getOpExt().suspendNotification(type);
            }
        }
    };

    private static class WifiStateTracker {
        int previousState;
        int currentState;
    }
    private static WifiStateTracker sWifiStateTracker = new WifiStateTracker();
    private static void updateWifiState(int current, int previous) {
        sWifiStateTracker.previousState = previous;
        sWifiStateTracker.currentState = current;
    }

    public synchronized AutoConnectManager getACM() {
        if (mACM == null) {
            mACM = new AutoConnectManager(mContext, getOpExt());
        }
        return mACM;
    }

    public class AutoConnectManager {
        private static final int MIN_RSSI = -200;
        private static final int MAX_RSSI = 256;

        List<ScanResult> mScanResults = null;

        private List<Integer> mDisconnectNetworks = new ArrayList<Integer>();
        private int mSystemUiUid = -1;
        // whether we should show reselect dialog, this flag stores this info across related calling
        // stacks
        private boolean mShowReselectDialog = false;
        // this flag indicates this scan is triggered by bad RSSI/passive disconnection, it's for
        // network reselection
        private boolean mScanForWeakSignal = false;
        // record contains the last disconnected network (passive disconnection/disconnection due to
        // bad RSSI)
        private int mDisconnectNetworkId = INVALID_NETWORK_ID;
        // indicate whether supplicant is connecting, it's updated in handleSupplicantStateChange()
        private boolean mIsConnecting = false;
        // indicates whether the last disconnection is an active disconnection, will be reset to false
        // when:
        // * wifi is re-enabled
        // * a network is connected
        // * it is consumed in handleNetworkDisconnect()'s customized auto connect codes
        private boolean mDisconnectOperation = false;
        private boolean mWaitForScanResult = false;
        // log last weak signal checking time to prevent excessive scan trigger by weak signal handle
        // codes
        private long mLastCheckWeakSignalTime = 0;
        private Context mContext;
        private IMtkWifiServiceExt mExt;
        private DetailedState mNetworkState = DetailedState.IDLE;
        private int mLastNetworkId = INVALID_NETWORK_ID;

        public AutoConnectManager(Context context, IMtkWifiServiceExt ext) {
            mContext = context;
            mExt = ext;
            log("AutoConnectManager: mExt is " + mExt.getClass());
            try {
                mSystemUiUid = context.getPackageManager().getPackageUidAsUser(
                    SYSUI_PACKAGE_NAME,
                    PackageManager.MATCH_SYSTEM_ONLY,
                    UserHandle.USER_SYSTEM);
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(TAG, "Unable to resolve SystemUI's UID.");
            }
        }

        public void setLastNetworkId(int id) {
            mLastNetworkId = id;
        }

        public void setNetworkState(DetailedState state) {
            mNetworkState = state;
        }

        public DetailedState getNetworkState() {
            return mNetworkState;
        }

        public void addDisconnectNetwork(int netId) {
            log("addDisconnectNetwork: " + netId);
            synchronized (mDisconnectNetworks) {
                mDisconnectNetworks.add(netId);
            }
        }

        public void removeDisconnectNetwork(int netId) {
            log("removeDisconnectNetwork: " + netId);
            synchronized (mDisconnectNetworks) {
                mDisconnectNetworks.remove((Integer) netId);
            }
        }

        public void clearDisconnectNetworks() {
            log("clearDisconnectNetworks");
            synchronized (mDisconnectNetworks) {
                mDisconnectNetworks.clear();
            }
        }

        public List<Integer> getDisconnectNetworks() {
            List<Integer> networks = new ArrayList<Integer>();
            synchronized (mDisconnectNetworks) {
                for (Integer netId : mDisconnectNetworks) {
                    networks.add(netId);
                }
            }
            return networks;
        }

        public boolean disableNetwork(int networkId) {
            WifiConfigManager configManager = WifiInjector.getInstance().getWifiConfigManager();
            return configManager.disableNetwork(networkId, mSystemUiUid);
        }

        public boolean enableNetwork(int networkId) {
            WifiConfigManager configManager = WifiInjector.getInstance().getWifiConfigManager();
            return configManager.enableNetwork(networkId, false, mSystemUiUid);
        }

        public boolean getShowReselectDialog() {
            return mShowReselectDialog;
        }

        public void setShowReselectDialog(boolean value) {
            log("setShowReselectDialog: " + mShowReselectDialog + " -> " + value);
            mShowReselectDialog = value;
        }

        public boolean getScanForWeakSignal() {
            return mScanForWeakSignal;
        }

        public void setScanForWeakSignal(boolean value) {
            log("setScanForWeakSignal: " + mScanForWeakSignal + " -> " + value);
            mScanForWeakSignal = value;
        }

        public void setWaitForScanResult(boolean value) {
            log("setWaitForScanResult: " + mWaitForScanResult + " -> " + value);
            mWaitForScanResult = value;
        }

        public boolean getWaitForScanResult() {
            return mWaitForScanResult;
        }

        private boolean getDisconnectOperation() {
            return mDisconnectOperation;
        }

        private void setDisconnectOperation(boolean value) {
            log("setDisconnectOperation: " + mDisconnectOperation + " -> " + value);
            new Throwable().printStackTrace();
            mDisconnectOperation = value;
        }

        public void showReselectionDialog() {
            // consume the flag
            setScanForWeakSignal(false);
            Log.d(TAG, "showReselectionDialog mDisconnectNetworkId:" + mDisconnectNetworkId);
            int networkId = getHighPriorityNetworkId();
            if (networkId == INVALID_NETWORK_ID) {
                return;
            }
            if (mExt.shouldAutoConnect()) {
                if (!mIsConnecting &&
                    !"WpsRunningState".equals(sWsmAdapter.getCurrentState().getName())) {
                    sWsmAdapter.sendMessage(
                        sWsmAdapter.obtainMessage(
                            WifiStateMachineAdapter.CMD_ENABLE_NETWORK,
                            networkId,
                            1));
                } else {
                    Log.d(TAG, "WiFi is connecting!");
                }
            } else {
                setShowReselectDialog(mExt.handleNetworkReselection());
            }
        }

        private int getHighPriorityNetworkId() {
            WifiConfigManager wcm = WifiInjector.getInstance().getWifiConfigManager();
            List<WifiConfiguration> networks = wcm.getSavedNetworks();
            if (networks == null || networks.size() == 0) {
                log("ACM: getHighPriorityNetworkId No configured networks");
                return INVALID_NETWORK_ID;
            }
            LinkedList<WifiConfiguration> found = new LinkedList<WifiConfiguration>();
            if (mScanResults != null && !mScanResults.isEmpty()) {
                for (WifiConfiguration network : networks) {
                    if (network.networkId != mDisconnectNetworkId) {
                        log("ACM: getHighPriorityNetworkId iterate scan result cache");
                        for (ScanResult scanResult : mScanResults) {
                            log("ACM: network.SSID = " + network.SSID);
                            log("ACM: scanResult.SSID = " + scanResult.SSID);
                            log("ACM: getSecurity(network) = " + mExt.getSecurity(network));
                            log("ACM: getSecurity(scanResult) = " + mExt.getSecurity(scanResult));
                            log("ACM: scanResult.level = " + scanResult.level);
                            if ((network.SSID != null) &&
                                (scanResult.SSID != null) &&
                                network.SSID.equals("\"" + scanResult.SSID + "\"") &&
                                mExt.getSecurity(network) == mExt.getSecurity(scanResult) &&
                                scanResult.level > IMtkWifiServiceExt.BEST_SIGNAL_THRESHOLD) {
                                    log("ACM: add network to found: " + network);
                                    found.add(network);
                            }
                        }
                        log("ACM: getHighPriorityNetworkId iterate scan result cache done");
                    }
                }
            }

            log("ACM: found.size() = " + found.size());
            if (found.size() < IMtkWifiServiceExt.MIN_NETWORKS_NUM) {
                log("ACM: getHighPriorityNetworkId Configured networks number less than two");
                return INVALID_NETWORK_ID;
            }

            WifiConfiguration result = Collections.max(found, new Comparator<Object>() {
                public int compare(Object obj1, Object obj2) {
                    WifiConfiguration n1 = (WifiConfiguration) obj1;
                    WifiConfiguration n2 = (WifiConfiguration) obj2;
                    return n2.priority - n1.priority;
                }
            });

            Log.d(TAG, "Found the highest priority AP, networkId:" + result.networkId);
            return result.networkId;
        }

        public void clearDisconnectNetworkId() {
            mDisconnectNetworkId = INVALID_NETWORK_ID;
        }

        public int getDisconnectNetworkId() {
            return mDisconnectNetworkId;
        }

        public void setDisconnectNetworkId(int id) {
            mDisconnectNetworkId = id;
        }

        public void handleScanResults(List<ScanDetail> full, List<ScanDetail> unsaved) {
            log("ACM: handleScanResults enter");
            // update scan results cache
            mScanResults = new ArrayList<ScanResult>();
            for (ScanDetail detail : full) {
                mScanResults.add(detail.getScanResult());
            }
            log("ACM: handleScanResults scan results cache updated");

            if (mExt.hasCustomizedAutoConnect()) {
                log("ACM: unsaved size " + unsaved.size());
                if (unsaved.isEmpty()) {
                    if (getWaitForScanResult()) {
                        showSwitchDialog();
                    }
                } else {
                    if (isWifiConnecting()) {
                        return;
                    } else {
                        if (getWaitForScanResult()) {
                            showSwitchDialog();
                        }
                    }
                }
            }
            log("ACM: handleScanResults exit");
        }

        private boolean isDataAvailable() {
            try {
                ITelephony phone = ITelephony.Stub.asInterface(ServiceManager.getService("phone"));
                TelephonyManager tm =
                    (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
                if (phone == null || !phone.isRadioOn(mContext.getPackageName()) || tm == null) {
                    return false;
                }

                boolean isSim1Insert = tm.hasIccCard(PhoneConstants.SIM_ID_1);
                boolean isSim2Insert = false;
                if (tm.getDefault().getPhoneCount() >= 2) {
                    isSim2Insert = tm.hasIccCard(PhoneConstants.SIM_ID_2);
                }
                if (!isSim1Insert && !isSim2Insert) {
                return false;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }

        private void showSwitchDialog() {
            setWaitForScanResult(false);
            if (!getShowReselectDialog() && isDataAvailable()) {
                turnOffDataConnection();
                sendWifiFailoverGprsDialog();
            }
        }

        private boolean isWifiConnecting() {
            WifiInfo wifiInfo = sWsmAdapter.getWifiInfo();
            int networkId = mIsConnecting ? wifiInfo.getNetworkId() : INVALID_NETWORK_ID;
            return (mExt.isWifiConnecting(networkId, getDisconnectNetworks()) ||
                    "WpsRunningState".equals(sWsmAdapter.getCurrentState().getName()));
        }

        public void setIsConnecting(boolean value) {
            mIsConnecting = value;
        }

        public boolean getIsConnecting() {
            return mIsConnecting;
        }

        public void resetStates() {
            Log.d(TAG, "resetStates");
            setDisconnectOperation(false);
            setScanForWeakSignal(false);
            setShowReselectDialog(false);
            setWaitForScanResult(false);
            mLastCheckWeakSignalTime = 0;
            mIsConnecting = false;
            resetDisconnectNetworkStates();
        }

        private void resetDisconnectNetworkStates() {
            Log.d(TAG, "resetDisconnectNetworkStates");
            if (!mExt.shouldAutoConnect()) {
                // disable all networks to prevent auto connection in manual/ask mode
                disableAllNetworks(false);
            } else {
                enableNetworks(getDisconnectNetworks());
            }
            clearDisconnectNetworks();
        }

        private void enableNetworks(List<Integer> networks) {
            if (networks != null) {
                for (int netId : networks) {
                    log("enableNetwork: " + netId);
                    boolean succeeded = enableNetwork(netId);
                    if (!succeeded) {
                        Log.e(TAG, "enableNetworks: failed to enable network " + netId);
                    }
                }
            }
        }

        private void disableAllNetworks(boolean exceptLastNetwork) {
            Log.d(TAG, "disableAllNetworks, exceptLastNetwork:" + exceptLastNetwork);
            WifiConfigManager wcm = WifiInjector.getInstance().getWifiConfigManager();
            List<WifiConfiguration> networks = wcm.getSavedNetworks();
            if (exceptLastNetwork) {
                if (null != networks) {
                    for (WifiConfiguration network : networks) {
                        int lastNetworkId = sWsmAdapter.getLastNetworkId();
                        if (network.networkId != lastNetworkId &&
                            network.status != WifiConfiguration.Status.DISABLED) {
                            disableNetwork(network.networkId);
                        }
                    }
                }
            } else {
                if (null != networks) {
                    for (WifiConfiguration network : networks) {
                        if (network.status != WifiConfiguration.Status.DISABLED) {
                            disableNetwork(network.networkId);
                        }
                    }
                }
            }
        }
        public void updateRSSI(Integer newRssi, int ipAddr, int lastNetworkId) {
            if (mExt.hasCustomizedAutoConnect()) {
                if (newRssi != null && newRssi < IMtkWifiServiceExt.WEAK_SIGNAL_THRESHOLD) {
                    long time = android.os.SystemClock.elapsedRealtime();
                    boolean autoConnect = mExt.shouldAutoConnect();
                    Log.d(TAG, "fetchRssi, ip:" + ipAddr
                            + ", mDisconnectOperation:" + mDisconnectOperation
                            + ", time:" + time + ", lasttime:" + mLastCheckWeakSignalTime);
                    final long lastCheckInterval = time - mLastCheckWeakSignalTime;
                    if ((ipAddr != 0 &&
                            !mDisconnectOperation &&
                            lastCheckInterval > IMtkWifiServiceExt.MIN_INTERVAL_CHECK_WEAK_SIGNAL_MS) ||
                        (autoConnect &&
                            lastCheckInterval > IMtkWifiServiceExt.MIN_INTERVAL_SCAN_SUPRESSION_MS)) {
                        Log.d(TAG, "Rssi < -85, scan to check signal!");
                        mLastCheckWeakSignalTime = time;
                        mDisconnectNetworkId = lastNetworkId;
                        setScanForWeakSignal(true);
                        sWsmAdapter.startScan();
                    }
                }
            }
        }

        public void handleNetworkDisconnect() {
            if (mExt.hasCustomizedAutoConnect()) {
                log("handleNetworkDisconnect, oldState:" +
                        mNetworkState + ", mDisconnectOperation:" + getDisconnectOperation());
                if (mNetworkState == DetailedState.CONNECTED) {
                    // record the disconnected network
                    mDisconnectNetworkId = mLastNetworkId;
                    if (!getDisconnectOperation()) {
                        // this is not an active disconnection, treat this as a weak signal case too
                        setScanForWeakSignal(true);
                        sWsmAdapter.startScan();
                    }
                }

                // in manual/ask mode, all network should be disabled except the current connected
                // one.
                // disable the disconnected network to prevent auto reconnecting to it
                if (!mExt.shouldAutoConnect()) {
                    disableNetwork(mLastNetworkId);
                }

                // consume this flag
                setDisconnectOperation(false);
                // reset this timestamp
                mLastCheckWeakSignalTime = 0;
            }
        }

        private boolean isPsDataAvailable() {
            // Check SIM ready
            TelephonyManager telMgr = (TelephonyManager) mContext.getSystemService(
                    Context.TELEPHONY_SERVICE);
            if (telMgr == null) {
                log("TelephonyManager is null");
                return false;
            }

            boolean isSIMReady = false;
            int i = 0;
            int n = telMgr.getSimCount();
            for (i = 0; i < n; i++) {
                if (telMgr.getSimState(i) == TelephonyManager.SIM_STATE_READY) {
                    isSIMReady = true;
                    break;
                }
            }

            log("isSIMReady: " + isSIMReady);
            if (!isSIMReady) {
                return false;
            }

            // check radio on
            ITelephony iTel = ITelephony.Stub.asInterface(
                    ServiceManager.getService(Context.TELEPHONY_SERVICE));
            if (iTel == null) {
                log("ITelephony is null");
                return false;
            }

            SubscriptionManager subMgr = SubscriptionManager.from(mContext);
            if (subMgr == null) {
                log("SubscriptionManager is null");
                return false;
            }

            int[] subIdList = subMgr.getActiveSubscriptionIdList();
            n = 0;
            if (subIdList != null) {
                n = subIdList.length;
            }

            boolean isRadioOn = false;
            for (i = 0; i < n; i++) {
                try {
                    isRadioOn = iTel.isRadioOnForSubscriber(
                            subIdList[i],
                            mContext.getPackageName());
                    if (isRadioOn) {
                        break;
                    }
                } catch (RemoteException e) {
                    log("isRadioOnForSubscriber RemoteException");
                    isRadioOn = false;
                }
            }
            if (!isRadioOn) {
                log("All sub Radio OFF");
                return false;
            }

            // Check flight mode
            int airplanMode = Settings.System.getInt(
                    mContext.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0);
            log("airplanMode:" + airplanMode);
            if (airplanMode == 1) {
                return false;
            }

            return true;
        }

        public void handleWifiDisconnect() {
            if (mExt.hasCustomizedAutoConnect()) {
                log("handleWifiDisconnect");

                int isAsking = Settings.System.getInt(mContext.getContentResolver(),
                        MtkSettingsExt.System.WIFI_CONNECT_REMINDER,
                        IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS);
                if (isAsking != IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS) {
                    log("Not ask mode");
                    return;
                }

                boolean dataAvailable = isPsDataAvailable();
                log("dataAvailable: " + dataAvailable);
                if (!dataAvailable) {
                    return;
                }

                if (!hasConnectableAp()) {
                    turnOffDataConnection();
                    sendWifiFailoverGprsDialog();
                }
            }
        }

        private void sendWifiFailoverGprsDialog() {
            Intent intent = new Intent(IMtkWifiServiceExt.ACTION_WIFI_FAILOVER_GPRS_DIALOG);
            intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
            intent.setClassName("com.mediatek.server.wifi.op01", "com.mediatek.op.wifi.DataConnectionReceiver");
            mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
            log("ACTION_WIFI_FAILOVER_GPRS_DIALOG sent");
        }

        public void updateAutoConnectSettings(int lastConnectId) {
            boolean isConnecting = (mIsConnecting ||
                                    "WpsRunningState".equals(sWsmAdapter.getCurrentState().getName()));
            Log.d(TAG, "updateAutoConnectSettings, isConnecting:" + isConnecting);
            List<WifiConfiguration> networks = WifiInjector.getInstance()
                                                           .getWifiConfigManager()
                                                           .getSavedNetworks();
            if (null != networks) {
                if (mExt.shouldAutoConnect()) {
                    if (!isConnecting) {
                        Collections.sort(networks, new Comparator<WifiConfiguration>() {
                            public int compare(WifiConfiguration obj1, WifiConfiguration obj2) {
                                return obj2.priority - obj1.priority;
                            }
                        });
                        for (WifiConfiguration network : networks) {
                            if (network.networkId != lastConnectId) {
                                enableNetwork(network.networkId);
                            }
                        }
                    }
                } else {
                    if (!isConnecting) {
                        for (WifiConfiguration network : networks) {
                            if (network.networkId != lastConnectId
                                && network.status != WifiConfiguration.Status.DISABLED) {
                                disableNetwork(network.networkId);
                            }
                        }
                    }
                }
            }
        }

        public boolean preProcessMessage(State state, Message msg) {
            log("preProcessMessage(" + state.getName() + ", " + msg.what + ")");
            int netId;
            switch (state.getName()) {
            case "ConnectModeState":
                switch(msg.what) {
                case WifiStateMachineAdapter.CMD_REMOVE_NETWORK:
                    if (mExt.hasCustomizedAutoConnect()) {
                        netId = msg.arg1;
                        removeDisconnectNetwork(netId);
                        if (netId == sWsmAdapter.getWifiInfo().getNetworkId()) {
                            setDisconnectOperation(true);
                            setScanForWeakSignal(false);
                        }
                    }
                    break;
                case WifiStateMachineAdapter.CMD_ENABLE_NETWORK:
                    if (mExt.hasCustomizedAutoConnect()) {
                        netId = msg.arg1;
                        boolean disableOthers = msg.arg2 == 1;
                        if (!disableOthers && !mExt.shouldAutoConnect()) {
                            Log.d(TAG,
                                "Shouldn't auto connect, ignore the enable network operation!");
                            sWsmAdapter.replyToMessage(
                                msg,
                                msg.what,
                                WifiStateMachineAdapter.SUCCESS);
                            return HANDLED;
                        }
                    }
                    break;
                default:
                    return NOT_HANDLED;
                }
                return NOT_HANDLED;
            default:
                log("State " + state.getName() + " NOT_HANDLED");
                return NOT_HANDLED;
            }
        }

        public boolean postProcessMessage(State state, Message msg, Object... args) {
            log("postProcessMessage(" + state.getName() + ", " + msg.what + ", " + args + ")");
            int netId;
            switch (state.getName()) {
            case "ConnectModeState":
                switch(msg.what) {
                case WifiStateMachineAdapter.CMD_ENABLE_NETWORK:
                    if (mExt.hasCustomizedAutoConnect()) {
                        netId = msg.arg1;
                        boolean disableOthers = msg.arg2 == 1;
                        boolean ok = (Boolean) args[0];
                        if (disableOthers && ok) {
                            removeDisconnectNetwork(netId);
                            setDisconnectOperation(true);
                            setScanForWeakSignal(false);
                        }
                    }
                    break;
                case WifiManager.DISABLE_NETWORK:
                    if (mExt.hasCustomizedAutoConnect()) {
                        // check whether it's succefully disabled
                        netId = msg.arg1;
                        WifiConfiguration config = WifiInjector.getInstance()
                                                               .getWifiConfigManager()
                                                               .getConfiguredNetwork(netId);
                        // we need to ensure this network was successfully disabled
                        if (config != null &&
                            config.getNetworkSelectionStatus() != null &&
                            !config.getNetworkSelectionStatus().isNetworkEnabled()) {
                            addDisconnectNetwork(netId);
                            if (netId == sWsmAdapter.getWifiInfo().getNetworkId()) {
                                setDisconnectOperation(true);
                                setScanForWeakSignal(false);
                            }
                        }
                    }
                    break;
                case WifiManager.CONNECT_NETWORK:
                    if (mExt.hasCustomizedAutoConnect()) {
                        netId = msg.arg1;
                        NetworkUpdateResult result = (NetworkUpdateResult) args[1];
                        boolean hasCredentialChanged = false;
                        if (result != null) {
                            netId = result.getNetworkId();
                            hasCredentialChanged = result.hasCredentialChanged();
                        }
                        if (sWsmAdapter.getWifiInfo().getNetworkId() != netId ||
                            hasCredentialChanged) {
                            // set this flag because we will disconnect current network due to
                            // either user specified a different network or the credential change
                            // forced us to do a reconnect
                            setDisconnectOperation(true);
                        }
                        setScanForWeakSignal(false);
                        removeDisconnectNetwork(netId);
                    }
                    break;
                case WifiManager.FORGET_NETWORK:
                    // check whether the network has been deleted
                    if (mExt.hasCustomizedAutoConnect()) {
                        netId = msg.arg1;
                        WifiConfiguration config = WifiInjector.getInstance()
                                                               .getWifiConfigManager()
                                                               .getConfiguredNetwork(netId);
                        if (config == null) {
                            removeDisconnectNetwork(netId);
                            if (netId == sWsmAdapter.getWifiInfo().getNetworkId()) {
                                setDisconnectOperation(true);
                                setScanForWeakSignal(false);
                            }
                        }
                    }
                    break;
                default:
                    return NOT_HANDLED;
                }
                return NOT_HANDLED;
            default:
                log("State " + state.getName() + " NOT_HANDLED");
                return NOT_HANDLED;
            }
        }

        public void turnOffDataConnection() {
            TelephonyManager tm =
                (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            // Remember last status on(1) or off(-1)
            Settings.System.putLong(mContext.getContentResolver(),
                                MtkSettingsExt.System.LAST_SIMID_BEFORE_WIFI_DISCONNECTED,
                                tm.getDataEnabled() ? 1 : -1);
            if (tm != null) {
                tm.setDataEnabled(false);
            }
        }

        public boolean hasConnectableAp() {
            if (MtkWifiService.this.hasConnectableAp() && mExt.hasConnectableAp()) {
                setWaitForScanResult(true);
                return true;
            }
            return false;
        }

        public List<ScanResult> getLatestScanResults() {
            return mScanResults;
        }
    }

    // enable access to various internal WifiStateMachine members
    private static WifiStateMachineAdapter sWsmAdapter = null;
    public static class WifiStateMachineAdapter {
        // NOTE: keep these values sync with WifiStateMachine
        static final int BASE = Protocol.BASE_WIFI;
        static final int CMD_REMOVE_NETWORK                                 = BASE + 53;
        static final int CMD_ENABLE_NETWORK                                 = BASE + 54;

        static final int SUCCESS = 1;
        static final int FAILURE = -1;

        private final WifiStateMachine mWsm;
        private final Class<?> mWsmCls;

        public WifiStateMachineAdapter(WifiStateMachine wsm) {
            mWsm = wsm;
            mWsmCls = wsm.getClass();
        }

        public void replyToMessage(Message msg, int what) {
            try {
                Method method = mWsmCls.getDeclaredMethod(
                    "replyToMessage",
                    Message.class,
                    Integer.class);
                method.setAccessible(true);
                method.invoke(mWsm, msg, what);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void replyToMessage(Message msg, int what, int arg1) {
            try {
                Method method = mWsmCls.getDeclaredMethod(
                    "replyToMessage",
                    Message.class,
                    Integer.class,
                    Integer.class);
                method.setAccessible(true);
                method.invoke(mWsm, msg, what, arg1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void replyToMessage(Message msg, int what, Object obj) {
            try {
                Method method = mWsmCls.getDeclaredMethod(
                    "replyToMessage",
                    Message.class,
                    Integer.class,
                    Object.class);
                method.setAccessible(true);
                method.invoke(mWsm, msg, what, obj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public WifiInfo getWifiInfo() {
            return mWsm.getWifiInfo();
        }

        public int getLastNetworkId() {
            int id = INVALID_NETWORK_ID;
            try {
                Field field = mWsmCls.getDeclaredField("mLastNetworkId");
                field.setAccessible(true);
                id = (Integer) field.get(mWsm);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                return id;
            }
        }

        public IState getCurrentState() {
            State state = null;
            try {
                Method method = StateMachine.class.getDeclaredMethod("getCurrentState");
                method.setAccessible(true);
                state = (State) method.invoke(mWsm);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                return state;
            }
        }

        public void startScan() {
            WifiInjector injector = WifiInjector.getInstance();
            ScanSettings settings = new ScanSettings();
            settings.type = WifiScanner.TYPE_HIGH_ACCURACY; // always do high accuracy scans.
            settings.band = WifiScanner.WIFI_BAND_BOTH_WITH_DFS;
            settings.reportEvents = WifiScanner.REPORT_EVENT_FULL_SCAN_RESULT
                                | WifiScanner.REPORT_EVENT_AFTER_EACH_SCAN;
            settings.numBssidsPerScan = 0;

            List<ScanSettings.HiddenNetwork> hiddenNetworkList =
                    injector.getWifiConfigManager().retrieveHiddenNetworkList();
            settings.hiddenNetworks =
                    hiddenNetworkList.toArray(new ScanSettings.HiddenNetwork[hiddenNetworkList.size()]);

            WifiScanner.ScanListener scanListener = new WifiScanner.ScanListener() {
                @Override
                public void onSuccess() {
                }
                @Override
                public void onFailure(int reason, String description) {
                }
                @Override
                public void onResults(WifiScanner.ScanData[] results) {
                }
                @Override
                public void onFullResult(ScanResult fullScanResult) {
                }
                @Override
                public void onPeriodChanged(int periodInMs) {
                }
            };
            injector.getWifiScanner().startScan(settings, scanListener);
        }

        public void sendMessage(int what) {
            mWsm.sendMessage(what);
        }

        public void sendMessage(Message message) {
            mWsm.sendMessage(message);
        }

        public Message obtainMessage(int what, int arg1, int arg2) {
            return mWsm.obtainMessage(what, arg1, arg2);
        }

        public String getInterfaceName() {
            String interfaceName = null; 
            try {
                Field field = mWsmCls.getDeclaredField("mInterfaceName");
                field.setAccessible(true);
                interfaceName = (String) field.get(mWsm);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                return interfaceName;
            }
        }
    }

    public MtkWifiService(Context context) {
        log("[MtkWifiService] " + context);
        mContext = context;
    }

    // NOTE: this method should be invoked in an looper thread
    public void initialize() {
        log("[initialize]");

        // 1. register OP network evaluator
        IMtkWifiServiceExt ext = getOpExt();
        WifiInjector injector = WifiInjector.getInstance();
        if (ext.hasNetworkSelection() == IMtkWifiServiceExt.OP_01) {
            try {
                Field field = WifiInjector.class
                                      .getDeclaredField("mWifiNetworkSelector");
                field.setAccessible(true);
                WifiNetworkSelector selector = (WifiNetworkSelector) field.get(injector);
                selector.registerNetworkEvaluator(
                    new MtkNetworkEvaluator(mContext, injector.getWifiConfigManager(), this),
                    0 /* before any AOSP evaluators */);
            } catch (Exception e) {
                // simply ignore all reflection exceptions
                e.printStackTrace();
            }
        }

        // 2. modify PERIODIC_SCAN_INTERVAL_MS
        final int interval = ext.defaultFrameworkScanIntervalMs();
        log("defaultFrameworkScanIntervalMs: " + interval);
        log("PERIODIC_SCAN_INTERVAL_MS: " + WifiConnectivityManager.PERIODIC_SCAN_INTERVAL_MS);
        try {
            Field nameField = WifiConnectivityManager.class
                    .getDeclaredField("PERIODIC_SCAN_INTERVAL_MS");
            // remove final modifier
            Field modifiersField = Field.class.getDeclaredField("accessFlags");
            modifiersField.setAccessible(true);
            log("accessFlags: " + modifiersField.getInt(nameField));
            modifiersField.setInt(nameField, (nameField.getModifiers() & ~Modifier.FINAL));
            log("accessFlags: " + modifiersField.getInt(nameField));
            log(
                "old: " + nameField.getInt(null));
            nameField.setInt(null, interval);
            log(
                "new: " + nameField.getInt(null));
            // restore final modifier
            // modifiersField.setInt(nameField, (nameField.getModifiers() | Modifier.FINAL));
        } catch (Exception e) {
            // simply ignore all reflection exceptions
            e.printStackTrace();
        }
        log("PERIODIC_SCAN_INTERVAL_MS: " + WifiConnectivityManager.PERIODIC_SCAN_INTERVAL_MS);
        // check if we succeeded
        if (WifiConnectivityManager.PERIODIC_SCAN_INTERVAL_MS != interval) {
            Log.e(TAG, "Failed to modify PERIODIC_SCAN_INTERVAL_MS");
        }

        // 3. register receiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(IMtkWifiServiceExt.AUTOCONNECT_SETTINGS_CHANGE);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(IMtkWifiServiceExt.ACTION_SUSPEND_NOTIFICATION);
        mContext.registerReceiver(mOperatorReceiver, intentFilter);

        // 4. make adapters
        sWsmAdapter = new WifiStateMachineAdapter(injector.getWifiStateMachine());
        Log.d(TAG, "initialize done");

        // 5. register supplicant state handler
        Handler handler = new Handler() {
            @Override
            public final void handleMessage(Message msg) {
                Log.d(TAG, "Supplicant message: " + msg.what);
                switch (msg.what) {
                case WifiMonitor.SUPPLICANT_STATE_CHANGE_EVENT:
                    StateChangeResult stateChangeResult = (StateChangeResult) msg.obj;
                    SupplicantState state = null;
                    try {
                        Field field = StateChangeResult.class.getDeclaredField("state");
                        field.setAccessible(true);
                        state = (SupplicantState) field.get(stateChangeResult);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    getACM().setIsConnecting(SupplicantState.isConnecting(state));
                    break;
                case WifiMonitor.SUP_CONNECTION_EVENT:
                    getACM().resetStates();
                    break;
                default:
                    Log.e(TAG, "Invalid message: " + msg.what);
                }
            }
        };

        String interfaceName = sWsmAdapter.getInterfaceName();
        WifiMonitor monitor = injector.getWifiMonitor();
        monitor.registerHandler(
            interfaceName,
            WifiMonitor.SUPPLICANT_STATE_CHANGE_EVENT,
            handler);
        monitor.registerHandler(
            interfaceName,
            WifiMonitor.SUP_CONNECTION_EVENT,
            handler);
    }

    // NOTE: keep these values in-sync with WifiSettingsStore
    private static final int WIFI_DISABLED = 0;
    private static final int WIFI_DISABLED_AIRPLANE_ON = 3;
    private int getPersistedWifiState() {
        final ContentResolver cr = mContext.getContentResolver();
        try {
            return Settings.Global.getInt(cr, Settings.Global.WIFI_ON);
        } catch (Settings.SettingNotFoundException e) {
            Settings.Global.putInt(cr, Settings.Global.WIFI_ON, WIFI_DISABLED);
            return WIFI_DISABLED;
        }
    }

    public boolean hasConnectableAp() {
        int persistedWifiState = getPersistedWifiState();
        return !(persistedWifiState == WIFI_DISABLED ||
                 persistedWifiState == WIFI_DISABLED_AIRPLANE_ON);
    }

    public static void log(String message) {
        if (DEBUG) {
            Log.d(TAG, message);
        }
    }

    @Override
    public void handleScanResults(List<ScanDetail> full, List<ScanDetail> unsaved) {
        getACM().handleScanResults(full, unsaved);
    }

    @Override
    public void updateRSSI(Integer newRssi, int ipAddr, int lastNetworkId) {
        getACM().updateRSSI(newRssi, ipAddr, lastNetworkId);
    }

    @Override
    public boolean preProcessMessage(State state, Message msg) {
        return getACM().preProcessMessage(state, msg);
    }

    @Override
    public boolean postProcessMessage(State state, Message msg, Object... args) {
        return getACM().postProcessMessage(state, msg, args);
    }

    @Override
    public List<ScanResult> getLatestScanResults() {
        return getACM().getLatestScanResults();
    }

    @Override
    public boolean getShowReselectDialog() {
        return getACM().getShowReselectDialog();
    }
}
