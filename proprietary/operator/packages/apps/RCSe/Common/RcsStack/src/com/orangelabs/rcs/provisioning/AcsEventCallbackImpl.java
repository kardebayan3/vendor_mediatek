/*******************************************************************************
 * Software Name : RCS IMS Stack
 *
 * Copyright (C) 2010 France Telecom S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.orangelabs.rcs.provisioning;

import android.content.Context;

import com.mediatek.ims.rcsua.AcsConfiguration;
import com.mediatek.ims.rcsua.AcsEventCallback;

//import com.mediatek.ims.rcsua.RcsUaService;

import com.orangelabs.rcs.service.UaServiceManager;
import com.orangelabs.rcs.utils.logger.Logger;


public class AcsEventCallbackImpl {

    private Context mContext;
    private Logger logger = Logger.getLogger(
            AcsEventCallbackImpl.class.getSimpleName());
    //private RcsUaService mRcsUaService;
    private RcsEventCallback mRcsEventCallback;

    public AcsEventCallbackImpl(Context context) {
        mContext = context;
        //mRcsUaService = rcsService;
        if (!AcsTApiServiceManager.initialize(context)) {
            logger.error("AcsTApiServiceManager initialization failed!");
        }
    }

    public void requestAcsService() {
        logger.debug("requestAcsService");
        mRcsEventCallback = new RcsEventCallback();
        UaServiceManager.getInstance().registerAcsCallback(mRcsEventCallback);
        //mRcsUaService.registerAcsEventCallback(mRcsEventCallback);
    }

    public void releaseAcsService() {
        logger.debug("releaseAcsService");
        UaServiceManager.getInstance().unregisterAcsCallback(mRcsEventCallback);
        mRcsEventCallback = null;
    }

    public class RcsEventCallback extends AcsEventCallback {

        public RcsEventCallback(){}

        public void onConfigurationStatusChanged(boolean valid, int version) {
            logger.debug("onConfigurationStatusChanged: valid: " + valid + " version: " + version);
            if (valid == true) {
                 processConfigXMLData();
            }
        }

        public void onAcsConnected() {
             logger.debug("onAcsConnected");
             //processConfigXMLData();
        }

        public void onAcsDisconnected() {
            logger.debug("onAcsDisconnected");
        }
    }

    private void processConfigXMLData() {
        //getConfigfile
        AcsConfiguration config = null;
        config = UaServiceManager.getInstance().getService().getAcsConfiguration();
        if (config == null) {
            return;
        }

        if (config.readXmlData() == null) {
            return;
        }
        // Parse the received content
        ProvisioningParser parser = new ProvisioningParser(config.readXmlData());
        parser.parse();
    }
}
