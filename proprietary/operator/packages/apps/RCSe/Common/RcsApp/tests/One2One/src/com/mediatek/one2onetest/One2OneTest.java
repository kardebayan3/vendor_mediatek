package com.mediatek.one2onetest;

import java.util.ArrayList;
import java.util.Iterator;

import android.app.Activity;
import junit.framework.Assert;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.CallLog;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.ImageView;
import android.widget.ImageButton;
import android.content.BroadcastReceiver;
import android.widget.EditText;
import com.gsma.services.rcs.capability.Capabilities;
import com.jayway.android.robotium.solo.Solo;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.File;

import com.android.mms.ui.ConversationList;
import com.android.mms.ui.ComposeMessageActivity;

public class One2OneTest extends ActivityInstrumentationTestCase2<ConversationList> {

    private Solo solo;

    private final static int RCS_CORE_LOADED = 0;
    private final static int RCS_CORE_FAILED = 1;
    private final static int RCS_CORE_STARTED = 2;
    private final static int RCS_CORE_STOPPED = 3;
    private final static int RCS_CORE_IMS_CONNECTED = 4;
    private final static int RCS_CORE_IMS_TRY_CONNECTION = 5;
    private final static int RCS_CORE_IMS_CONNECTION_FAILED = 6;
    private final static int RCS_CORE_IMS_TRY_DISCONNECT = 7;
    private final static int RCS_CORE_IMS_BATTERY_DISCONNECTED = 8;
    private final static int RCS_CORE_IMS_DISCONNECTED = 9;
    private final static int RCS_CORE_NOT_LOADED = 10;
    
    private final static int WAIT_TIME = 10000;  
    private final static int MAX_TRY = 10;

    /**
     * Boolean value "true"
     */
    public static final String TRUE = Boolean.toString(true);

    /**
     * Boolean value "false"
     */
    public static final String FALSE = Boolean.toString(false);

    public static final String TAG = "one2onetests";
    public static final int MAX_NUMBER_OF_SCROLLS = 100;
    boolean fileTransferResult = false;

    public One2OneTest() {
        super(ConversationList.class);
    }

    public void setUp() throws Exception {
        solo = new Solo(getInstrumentation(), getActivity());
        Log.d(TAG, "setUp entry ");
       // setServicePermissionState(true, getActivity().getApplicationContext());
    }

    @Override
    public void tearDown() throws Exception {
        Log.d(TAG, "tearDown entry ");
        solo.finishOpenedActivities();  
    }

    /**
     * AT test case to send a One2One message 
    */
    public void testSendMessage() {
  
        Log.d(TAG, "testSendMessage entry "); 
        solo.assertCurrentActivity("wrong activity", ConversationList.class);
        boolean state = false;
        int currentState = RCS_CORE_NOT_LOADED;
        boolean stateSwitch = false;
        
        Intent intent = new Intent();
        Uri uri;
        String number = "+14253262243";
        intent.setAction(Intent.ACTION_SENDTO);
        intent.putExtra("chatmode", 1);
        intent.putExtra("isjoyn", true);
        uri = Uri.parse("smsto: " + "9+++" + number);
      
        intent.setData(uri);
        getActivity().startActivity(intent);
        solo.sleep(3000);
        
        //ArrayList<android.view.View> views = solo.getCurrentViews();
        
        // Iterator<android.view.View> iterator = views.iterator();
         
        // while (iterator.hasNext()) { Log.d(TAG, iterator.next().toString());}
         
          ImageButton sendMessageButton = (ImageButton)solo.getView("send_button_sms");

          EditText textMessage = (EditText)solo.getView("embedded_text_editor");
          solo.enterText(textMessage,"FirstMessage");
          solo.clickOnView(sendMessageButton);
        
          solo.sleep(10000);
        //assertTrue(state);
    }
    
    /**
     * AT test case to send a One2One File 
    */
    public void testsendFile() {
  
        Log.d(TAG, "testSendMessage entry "); 
        solo.assertCurrentActivity("wrong activity", ConversationList.class);
      
        
        Intent intent = new Intent();
        Uri uri;
        String number = "+14253262243";
       
        solo.sleep(3000);
        
        String name = "AtTestFile.txt";
        
        String directory = "/sdcard/"; //Environment.getExternalStorageDirectory().toString() + "/joyn/files/";
        String finalName = directory + name;
        Log.d(TAG, "testSendMessage filename is: " + directory + name); 
        File testFile = new File(directory, name);
        BufferedOutputStream testFileStream = null;
        try {
         testFileStream = new BufferedOutputStream(new FileOutputStream(testFile));
        }catch(Exception e){
         Log.d(TAG, "testSendMessage fail file not found");
         e.printStackTrace();
        }
        
        String fileContent = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for(int i = 0; i < 1000 ; i++){
        	fileContent = fileContent + " Dummy ";
        }
        byte[] b = fileContent.getBytes();
        try {
         testFileStream.write(b, 0, b.length);
         testFileStream.flush();
         testFileStream.close();
        }catch(Exception e){
         Log.d(TAG, "testSendMessage fail "); 
         e.printStackTrace();  	
        }       
        
        //number = "9+++" + number;
        ArrayList<Parcelable> list = new ArrayList<Parcelable>();
        ArrayList<Parcelable> mFileUriList = new ArrayList<Parcelable>();
        Uri external = Uri.fromFile(testFile);
        mFileUriList.add(external);

        ArrayList<String> files = new ArrayList<String>();
        files.add(finalName);
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        intent.setAction(Intent.ACTION_SENDTO);
        Intent it = new Intent();
        it.setAction("com.mediatek.mms.ipmessage.fileTransfer");
        it.putExtra("sendFileFromContacts", true);
        it.putExtra("contact", number);
        it.putExtra("filePath", files.get(0));
        it.putStringArrayListExtra("filePaths", files);
        ArrayList<Integer> fileSize = new ArrayList<Integer>();
        for (int i = 0; i < files.size(); i++) {
            File mfile = new File(files.get(i));
            Long size = mfile.length();
            int sizeInt = size.intValue();
            fileSize.add(i, sizeInt);
        }

        it.putIntegerArrayListExtra("size", fileSize);
        it.putParcelableArrayListExtra(Intent.EXTRA_STREAM, list);
        //getActivity().getApplicationContext().sendBroadcast(it);

        // Send MMS intent to open joyn composer window

        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, list);
        String mimeType = null;
        int size = files.size();
        Log.d(TAG, "startFileTransfer() file size is " + size);
        
    String extension = MimeTypeMap.getFileExtensionFromUrl(files.get(0));
    if (extension != null) {
       mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
   }

        number = "9+++" + number;
        intent.putExtra("chatmode", 1);
        Log.d(TAG, "startFileTransfer() mimeType is " + mimeType);
        intent.setType(mimeType);
        intent.putExtra("isjoyn", true);
        intent.putExtra("address", number);
        intent.setClassName("com.android.mms", "com.android.mms.ui.ComposeMessageActivity");
        Log.d(TAG, "startFileTransfer() new intent number is " + number
                + ", mimetype is" + mimeType + ", list" + files.get(0));
        
        getActivity().startActivity(intent);
        solo.sleep(5000);
        getActivity().getApplicationContext().sendBroadcast(it);
        solo.sleep(20000);
        Log.d(TAG, "startFileTransfer() success");
        assertTrue(fileTransferResult);
  }
  
    /**
     *Receiver class to receive result of file transfer 
    */
  public class MyReceiver extends BroadcastReceiver {

 @Override
 public void onReceive(Context context, Intent intent) {
  fileTransferResult = true;
 }
}
  
}
