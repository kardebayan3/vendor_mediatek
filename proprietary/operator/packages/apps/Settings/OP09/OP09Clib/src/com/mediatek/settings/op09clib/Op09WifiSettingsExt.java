package com.mediatek.settings.op09clib;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.android.settingslib.wifi.WifiTracker;

import com.mediatek.settings.ext.DefaultWifiSettingsExt;

public class Op09WifiSettingsExt extends DefaultWifiSettingsExt {

    private static final String TAG = "OP09WIFISETTINGSEXT";
    private Context mContext;
    private static final String KEY_REFRESH_PREFERENCE = "refersh_preference_key";
    private static final String PREF_KEY_ADDITIONAL_SETTINGS = "additional_settings";
    private static WifiTracker mWifiTracker = null;
    private static Preference mRefreshPreference;
    /**
     * Op09WifiSettingsExt.
     * @param context Context
     */
    public Op09WifiSettingsExt(Context context) {
        super();
        mContext = context;
        Log.d("@M_" + TAG, "WifiSettingsExt mContext = " + mContext);
    }

    @Override
    public void addRefreshPreference(PreferenceScreen screen,
            Object wifiTracker,
            boolean isUiRestricted) {
        Log.d(TAG, "addRefreshPreference, isUiRestricted = " + isUiRestricted);
        if (isUiRestricted) {
            return;
        } else {
            mWifiTracker = (WifiTracker)wifiTracker;
            PreferenceCategory preferenceCategory
                = (PreferenceCategory)screen.findPreference(PREF_KEY_ADDITIONAL_SETTINGS);
            if (null == mRefreshPreference) {
                mRefreshPreference = new Preference(
                        preferenceCategory.getPreferenceManager().getContext());
                mRefreshPreference.setKey(KEY_REFRESH_PREFERENCE);
                mRefreshPreference.setTitle(mContext.getResources()
                        .getString(R.string.menu_stats_refresh));
                mRefreshPreference.setOrder(-1);
            }
            if (null != mRefreshPreference) {
                if (null != preferenceCategory.findPreference(KEY_REFRESH_PREFERENCE)) {
                    preferenceCategory.removePreference(mRefreshPreference);
                }
                preferenceCategory.addPreference(mRefreshPreference);
                mRefreshPreference.setEnabled(mWifiTracker.isWifiEnabled());
            }
        }
    }

    @Override
    public boolean customRefreshButtonClick(Preference preference) {
        if (null != preference
                && KEY_REFRESH_PREFERENCE.equals(preference.getKey())) {
            Log.d(TAG, "customRefreshButtonClick");
            if (null != mWifiTracker) {
                Log.d(TAG, "customRefreshButtonClick, mWifiTracker.forceScan()");
                mWifiTracker.forceScan();
            }
            return true;
        }
        return false;
    }

    @Override
    public void customRefreshButtonStatus(boolean checkStatus) {
        Log.d(TAG, "customRefreshButtonStatus checkStatus = " + checkStatus);
        if (null != mRefreshPreference) {
            Log.d(TAG, "customRefreshButtonStatus checkStatus1 = " + checkStatus);
            mRefreshPreference.setEnabled(checkStatus);
        }
    }
}

