package com.mediatek.op03.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Switch;

import com.mediatek.op03.settings.R;
import com.mediatek.settings.ext.DefaultDataUsageSummaryExt;

/**
 * Plugin implementation for Data Usage Summary.
 */
public class OP03DataUsageSummaryExt extends DefaultDataUsageSummaryExt {

    private static final String TAG = "OP03DataUsageSummaryExt";
    private Context mContext;
    private Context mHostAppContext;
    private DialogInterface mDialog;
    private View.OnClickListener mDialogListener;
    private TelephonyManager mTelephonyManager;
    private View mView;

    /** Constructor.
     * @param context context
     */
    public OP03DataUsageSummaryExt(Context context) {
        super(context);
        mContext = context;
        mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
    }

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        boolean isEnable = mTelephonyManager.getDataEnabled();
        int message = isEnable ? R.string.networksettings_tips_data_disabled
                : R.string.networksettings_tips_data_enabled;
        AlertDialog.Builder dialogBuild = new AlertDialog.Builder(mHostAppContext);
        dialogBuild.setMessage(mContext.getText(message))
        .setTitle(android.R.string.dialog_alert_title)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mDialogListener.onClick(mView);
                }
        })
        .setNegativeButton(android.R.string.no, null)
        .create();
        dialogBuild.show();
        }
    };

    @Override
    public void onBindViewHolder(Context context, View dataEnabledView, View.OnClickListener
            dataEnabledDialogListerner) {
        mHostAppContext = context;
        mView = dataEnabledView;
        mDialogListener = dataEnabledDialogListerner;
        dataEnabledView.setOnClickListener(mClickListener);
    }
}