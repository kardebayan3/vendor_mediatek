package com.mediatek.op08.settings;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
//import android.net.wifi.HotspotClient;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import mediatek.net.wifi.HotspotClient;

import java.util.ArrayList;
import java.util.List;

public class AllowedDevicesActivity extends Activity {
    private static final String TAG = "OP08AllowedDevicesActivity";
    private static final String ALLOWED_DEVICES_CHECKBOX_KEY = "allow_all_devices_checkBox";
    private static final String ALLOWED_DEVICES_LIST_KEY = "allowed_device_list";
    private static final String ADD_BUTTON_KEY = "add";
    private static final String DELETE_BUTTON_KEY = "delete";

    private static final int DIALOG_ALLOW_DEVICE_TURNOFF_NOT_ALLOWED = 0;
    private static final int DIALOG_ADD_ALL_DEVICES_CONFIRMATION_DIALOG = 1;
    private static final int DIALOG_ADD_DEVICE_MANUALLY = 2;
    private static final int DIALOG_EMPTY_ALLOWED_LIST = 3;

    private ActionBar mActionBar;

    private CheckBox mAllowedDevicesCheckBox;
    private ListView mAllowedDeivesListView;
    private TextView mEmptyText;
    private Button mAddButton;
    private Button mDeleteButton;
    private List<HotspotClient> mAllowedDevicesList = new ArrayList<HotspotClient>();

    private AllowedDevicesListAdapter mAdapter;
    private WifiManager mWifiManager;
    private WifiTetheringDialogUtil mWifiTetherDlg;

    private CompoundButton.OnCheckedChangeListener mCheckBoxChangeListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Log.d(TAG, "switchTurnedOn:" + isChecked);
            if (isChecked) {
                mWifiManager.getWifiHotspotManager().setAllDevicesAllowed(true, false);
            } else {
                // As per planner Yanni, only connected device list should be checked
                if (connectedDevicesPresent()) {
                    showDialogs(DIALOG_ADD_ALL_DEVICES_CONFIRMATION_DIALOG);
                } else {
                    /* No device present in connected device:
                     * 1) If device/s present in allowed device list: turn Off checkbox
                     * 2) If no device present in allowed device: Show 'turn off not allowed' popup
                     */
                    Log.d(TAG, "mAllowedDevicesList:" + mAllowedDevicesList);
                    if (mAllowedDevicesList.size() > 0) {
                        mWifiManager.getWifiHotspotManager().setAllDevicesAllowed(false, false);
                    } else {
                        showDialogs(DIALOG_ALLOW_DEVICE_TURNOFF_NOT_ALLOWED);
                    }
                }
            }
        }
    };

    private View.OnClickListener mViewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick view:" + v);
            if (v == mAddButton) {
                showDialogs(DIALOG_ADD_DEVICE_MANUALLY);
            } else if (v == mDeleteButton) {
                int selectedItem = mAdapter.getSelectedItemId();
                if (selectedItem < 0) {
                    return;
                }
                mWifiManager.getWifiHotspotManager().disallowDevice(mAllowedDevicesList.
                        get(selectedItem).deviceAddress);
                mAdapter.remove(mAllowedDevicesList.get(selectedItem));
                refreshAllowedDevicesList();
                v.setEnabled(false);
                if (mAllowedDevicesList.size() == 0 && !mAllowedDevicesCheckBox.isChecked()) {
                    Log.d(TAG, "List is empty, set checkbox true:");
                    showDialogs(DIALOG_EMPTY_ALLOWED_LIST);
                    mAllowedDevicesCheckBox.setChecked(true);
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.allowed_devices_layout);

        mActionBar = getActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayHomeAsUpEnabled(true);
        }

        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        mAllowedDevicesCheckBox = (CheckBox) findViewById(R.id.allow_all_devices_checkBox);
        mAllowedDevicesCheckBox.setOnCheckedChangeListener(mCheckBoxChangeListener);

        mAllowedDeivesListView = (ListView) findViewById(R.id.allowed_device_list);
        mEmptyText = (TextView) findViewById(android.R.id.empty);
        mAdapter = new AllowedDevicesListAdapter(this, mAllowedDevicesList);
        mAllowedDeivesListView.setAdapter(mAdapter);
        mAllowedDeivesListView.setEmptyView(mEmptyText);
        mAllowedDeivesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                View prevSelectedView = getViewByPosition(mAdapter.getSelectedItemId(),
                        mAllowedDeivesListView);
                if (prevSelectedView != null) {
                    prevSelectedView.setBackgroundColor(getResources()
                            .getColor(android.R.color.white));
                }
                mAdapter.setSelectedItemId(position);
                Log.d(TAG, "position clicked:" + position);
                view.setBackgroundColor(getResources().getColor(R.color.grey_color));
                mDeleteButton.setEnabled(true);
            }
        });

        mAddButton = (Button) findViewById(R.id.add);
        mDeleteButton = (Button) findViewById(R.id.delete);
        mDeleteButton.setEnabled(false);
        mAddButton.setOnClickListener(mViewClickListener);
        mDeleteButton.setOnClickListener(mViewClickListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAllowedDevicesCheckBox.setChecked(mWifiManager.getWifiHotspotManager().
                isAllDevicesAllowed());
        Log.d(TAG, "Checked:" + mWifiManager.getWifiHotspotManager().isAllDevicesAllowed());
        refreshAllowedDevicesList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkIfDuplicateClient(String mac) {
        for (HotspotClient h : mAllowedDevicesList) {
            if (h.deviceAddress.equals(mac)) {
                Log.d(TAG, "Duplicate client:" + h);
                return true;
            }
        }
        return false;
    }

    private void refreshAllowedDevicesList() {
        List<HotspotClient> b = mWifiManager.getWifiHotspotManager().getAllowedDevices();
        Log.d(TAG, "refreshed list:" + b);
        mAllowedDevicesList.clear();
        if (b != null && b.size() > 0) {
            mAllowedDevicesList.addAll(0, b);
        }
        mAdapter.notifyDataSetChanged();
    }

    private void showDialogs(int dialogId) {
        Log.d(TAG, "showDialog, Dialog id:" + dialogId);
        switch (dialogId) {
            case DIALOG_ALLOW_DEVICE_TURNOFF_NOT_ALLOWED:
                showAllowDevicesTurnOffNtAllowedDialog();
            break;
            case DIALOG_ADD_ALL_DEVICES_CONFIRMATION_DIALOG:
                showAddAllDevicesConfirmationDialog();
            break;
            case DIALOG_ADD_DEVICE_MANUALLY:
                showAddDeviceDialog();
                break;
            case DIALOG_EMPTY_ALLOWED_LIST:
                showEmptyAllowedListDialog();
                break;
            default:
                Log.d(TAG, "invalid dialog");
            break;
        }
    }

    private void showEmptyAllowedListDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(
                R.string.show_empty_allowed_list).setNegativeButton(
                R.string.close, null).create();
        dialog.show();
    }

    private void showAllowDevicesTurnOffNtAllowedDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(
                R.string.allowed_devices_turn_off_not_allowed_dialog_text).setNegativeButton(
                R.string.close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // turn switch on
                        mAllowedDevicesCheckBox.setChecked(true);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        Log.d(TAG, "Dialog canceled by user");
                        mAllowedDevicesCheckBox.setChecked(true);
                    }
                }).create();
        dialog.show();
    }

    private void showAddAllDevicesConfirmationDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this).setMessage(
                R.string.add_all_devices_confirmation_dialog_text)
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /* Connected Devices present & User chose 'No':
                         * 1)If device present in Allowed device:turn off checkbox & do not import
                         * 2)If no device present in Allowed device:Show 'TURNOFF_NOT_ALLOWED'popup
                         */
                        Log.d(TAG, "mAllowedDevicesList:" + mAllowedDevicesList);
                        if (mAllowedDevicesList.size() > 0) {
                            mWifiManager.getWifiHotspotManager().
                                    setAllDevicesAllowed(false, false);
                        } else {
                            showDialogs(DIALOG_ALLOW_DEVICE_TURNOFF_NOT_ALLOWED);
                        }
                    }
                }).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        /* Connected Devices present & User chose 'Yes':
                         * Turn OFF checkbox & import devices from connected devices
                         */
                        mWifiManager.getWifiHotspotManager().setAllDevicesAllowed(false, true);
                        refreshAllowedDevicesList();
                        mAllowedDeivesListView.smoothScrollToPosition(0);
                    }
                }).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        Log.d(TAG, "Dialog canceled by user");
                        mAllowedDevicesCheckBox.setChecked(true);
                    }
                }).create();
        dialog.show();
    }

    private void showAddDeviceDialog() {
        mWifiTetherDlg = new WifiTetheringDialogUtil(this, mListener);
        mWifiTetherDlg.show();
        Button saveButton = mWifiTetherDlg.getButton(AlertDialog.BUTTON_POSITIVE);
        if (saveButton != null) {
            saveButton.setEnabled(false);
        }
    }

    private DialogInterface.OnClickListener mListener =
        new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int button) {
                if (button == DialogInterface.BUTTON_POSITIVE) {
                    String dvcName = mWifiTetherDlg.getDeviceName();
                    String macAddr = mWifiTetherDlg.getMacAddr();
                    Log.d(TAG, "onClick save deviceName = " + dvcName);
                    if (checkIfDuplicateClient(macAddr)) {
                        showToast(R.string.add_allowed_device_failed);
                        return;
                    }
                    boolean b = mWifiManager.getWifiHotspotManager().allowDevice(macAddr, dvcName);
                    // TODO: Allow device macAddress and DeviceName
                    if (b) {
                         Log.d(TAG, "Device Added successfully to the allowed device list");
                         refreshAllowedDevicesList();
                         mAllowedDeivesListView.smoothScrollToPosition(mAdapter.getCount());
                    } else {
                        Log.d(TAG, "Can't add this device:Invalid Mac address");
                        showToast(R.string.add_allowed_device_failed);
                    }
                } else {
                    Log.d(TAG, "onClick cancel");
                }
            }
    };

    private void showToast(int id) {
        Toast.makeText(AllowedDevicesActivity.this, id, Toast.LENGTH_SHORT).show();
    }

    private View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;
        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            Log.d(TAG, "view not visible");
            return null;
        } else {
            final int childIndex = pos - firstListItemPosition;
            Log.d(TAG, "index:" + childIndex);
            return listView.getChildAt(childIndex);
        }
    }

    private boolean connectedDevicesPresent() {
        List<HotspotClient> list = mWifiManager.getWifiHotspotManager().getHotspotClients();
        boolean ret = false;
        if (list != null && list.size() > 0) {
           for (HotspotClient h : list) {
             if (!h.isBlocked) {
                 Log.d(TAG, "atleast one client connected:" + h);
                 ret = true;
                 break;
             }
           }
           Log.d(TAG, "no client connected");
        }
        return ret;
    }
}
