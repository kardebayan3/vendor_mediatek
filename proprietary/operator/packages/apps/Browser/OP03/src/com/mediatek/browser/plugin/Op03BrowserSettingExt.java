package com.mediatek.browser.plugin;

import android.os.Build;
import android.os.SystemProperties;
import android.util.Log;

import com.mediatek.browser.ext.DefaultBrowserSettingExt;

public class Op03BrowserSettingExt extends DefaultBrowserSettingExt {

    private static final String TAG = "Op03BrowserSettingExt";
    final static boolean DEBUG =
        (!Build.TYPE.equals("user")) ? true :
        SystemProperties.getBoolean("ro.mtk_browser_debug_enablelog", false);

    public String getOperatorUA(String defaultUA) {
        if(DEBUG) {
            Log.i(TAG, "getOperatorUA, defaultUA= " + defaultUA + " --OP03 implement");
        }
        String op03UA = defaultUA;
        if (!defaultUA.contains("-orange")) {
            op03UA = defaultUA.replace(Build.MODEL, Build.MODEL + "-orange");
        }
        if(DEBUG) {
            Log.i(TAG, "getOperatorUA, updated UA = " + op03UA);
        }
        return op03UA;
    }

}
