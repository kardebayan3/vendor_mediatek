package com.mediatek.mms.plugin;

import android.util.Log;
import com.mediatek.mms.ext.DefaultOpMmsConfigExt;

public class Op17MmsConfigExt extends DefaultOpMmsConfigExt {

    private static final String TAG = "Op17MmsConfigExt";

    public int getSmsToMmsTextThreshold() {
        Log.d(TAG, "get SmsToMmsTextThreshold: 11");
        return 11;
    }
}
