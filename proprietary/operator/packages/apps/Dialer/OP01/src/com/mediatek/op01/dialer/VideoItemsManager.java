package com.mediatek.op01.dialer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.View;

import com.android.dialer.app.DialtactsActivity;
import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.android.dialer.calldetails.CallDetailsActivity;

import com.mediatek.op01.dialer.ItemsController;

import com.mediatek.op01.dialer.calllog.CallDetailItemsController;
import com.mediatek.op01.dialer.calllog.CallLogItemsController;
import com.mediatek.op01.dialer.speeddial.DialpadItemsController;

import java.util.HashMap;
import java.util.Map;

public class VideoItemsManager {
    private static final String TAG = "VideoItemsManager";

    private static VideoItemsManager sVideoItemsManager;

    private Map<Fragment, ItemsController> mFragmentMap =
            new HashMap<Fragment, ItemsController>();

    private Map<Activity, ItemsController> mActivityMap =
            new HashMap<Activity, ItemsController>();

    public static synchronized VideoItemsManager getInstance() {
        if (sVideoItemsManager == null) {
            sVideoItemsManager = new VideoItemsManager();
        }
        return sVideoItemsManager;
    }

    public ItemsController initByActivity(Activity activity) {
        Log.d(TAG, "initByActivity, activity = " + activity);
        ItemsController controller = getControllerByActivity(activity);
        if (controller != null) {
            Log.d(TAG, "initByActivity, already inited.");
            return controller;
        }

        if (activity instanceof DialtactsActivity) {
            controller = new DialpadItemsController(activity);
            Log.d(TAG, "initByActivity, host is DialtactsActivity");
        } else if (activity instanceof CallDetailsActivity) {
            controller = new CallDetailItemsController(activity);
            Log.d(TAG, "initByActivity, host is CallDetailsActivity");
        }

        if (controller != null) {
            mActivityMap.put(activity, controller);
        }
        return controller;
    }

    public void deInitByActivity(Activity activity) {
        Log.d(TAG, "deInitByActivity, activity = " + activity);
        ItemsController controller = getControllerByActivity(activity);
        if (controller != null) {
            controller.clear();
            mActivityMap.remove(activity);
        }
    }

    public ItemsController initByFragment(Fragment fragment) {
        Log.d(TAG, "initByFragment, fragment = " + fragment);
        if (getControllerByFragment(fragment) != null) {
            ItemsController controller = getControllerByFragment(fragment);
            controller.reInitInfo(fragment);
            return controller;
        }

        ItemsController controller = new CallLogItemsController(fragment);
        mFragmentMap.put(fragment, controller);
        return controller;
    }

    public void deInitByFragment(Fragment fragment) {
        Log.d(TAG, "deInitByFragment, fragment = " + fragment);
        ItemsController controller = getControllerByFragment(fragment);
        if (controller != null) {
            controller.clear();
            mFragmentMap.remove(fragment);
        }
    }

    public ItemsController getControllerByFragment(Fragment fragment) {
        if (mFragmentMap != null && mFragmentMap.containsKey(fragment)) {
            return mFragmentMap.get(fragment);
        }
        return null;
    }

    public ItemsController getControllerByActivity(Activity activity) {
        if (mActivityMap != null && mActivityMap.containsKey(activity)) {
            return mActivityMap.get(activity);
        }
        return null;
    }

    public void onAttachedToController(
            Fragment fragment, CallLogListItemViewHolder holder) {
        Log.d(TAG, "onAttachedToController, fragment = " + fragment);
        setFragmenTag(fragment, holder);
        ItemsController controller = getControllerByFragment(fragment);
        if (controller == null) {
            controller = initByFragment(fragment);
        }

        controller.addCallLogViewHolder(holder);
    }

    public void onDetachedFromController(CallLogListItemViewHolder holder) {
        Fragment fragment = getFragmentTag(holder);
        Log.d(TAG, "onDetachedFromController, fragment = " + fragment);
        if (fragment == null) {
            return;
        }

        ItemsController controller = getControllerByFragment(fragment);
        if (controller != null) {
            controller.removeCallLogViewHolder(holder);
        }
        clearFragmentTag(holder);
    }

    public void showActions(CallLogListItemViewHolder holder, boolean show) {
        Fragment fragment = getFragmentTag(holder);
        if (fragment == null) {
            Log.d(TAG, "showActions, fragment is null");
            return;
        }

        ItemsController controller = getControllerByFragment(fragment);
        if (controller == null) {
            controller = initByFragment(fragment);
        }

        controller.showAction(holder, show);
    }

    public void customizeVideoItem(View view, int type) {
        Activity activity = getActivityByView(view);
        Log.d(TAG, "customizeVideoItem, activity = " + activity);

        if (activity != null) {
            ItemsController controller = getControllerByActivity(activity);
            if (controller == null) {
                controller = initByActivity(activity);
            }

            controller.customizeVideoItem(view, type);
        }
    }

    private Activity getActivityByView(View view) {
        Context cnx = view.getContext();

        if (cnx instanceof DialtactsActivity) {
            return (DialtactsActivity) cnx;
        } else if (cnx instanceof CallDetailsActivity) {
            return (CallDetailsActivity) cnx;
        }
        return null;
    }

    private void setFragmenTag(Fragment fragment, CallLogListItemViewHolder holder) {
        if (holder.rootView != null) {
            holder.rootView.setTag(fragment);
            Log.d(TAG, "setFragmenTag");
        }
    }

    private void clearFragmentTag(CallLogListItemViewHolder holder) {
        if (holder.rootView != null) {
            holder.rootView.setTag(null);
            Log.d(TAG, "clearFragmentTag");
        }
    }

    private Fragment getFragmentTag(CallLogListItemViewHolder holder) {
        if (holder.rootView != null) {
            return (Fragment) holder.rootView.getTag();
        }
        return null;
    }
}
