package com.mediatek.op01.dialer.calllog;

import android.app.Activity;

import com.mediatek.op01.dialer.ItemsController;

public class CallDetailItemsController
        extends ItemsController{

    public CallDetailItemsController(Activity activity) {
        super(activity);
    }

    @Override
    protected boolean isTypeVideo(int type) {
        return (type == CALLBACK_TYPE_IMS_VIDEO);
    }
}