package com.mediatek.op08.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.util.Log;
import android.view.View;

import com.android.dialer.app.calllog.CallLogListItemViewHolder;

import com.mediatek.dialer.calllog.CallLogMultipleDeleteFragment;
import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.op08.dialer.VideoItemsManager;
import com.mediatek.op08.presence.ContactNumberUtils;
import com.mediatek.op08.presence.PresenceApiManager;
import com.mediatek.op08.presence.PresenceApiManager.CapabilitiesChangeListener;
import com.mediatek.op08.presence.PresenceApiManager.ContactInformation;

import java.util.HashMap;

/**
 * Plugin implementation for Call Log Common Presence.
 */
public class Op08CalllogExtension extends
        DefaultCallLogExtension {
    private static final String TAG = "Op08CalllogExtension";
    VideoItemsManager mVideoItemsManager;

    /**
    * Constructor.
    * @param context Context
    */
    public Op08CalllogExtension(Context context) {
        Log.d(TAG, "Op08CalllogExtension, context = " + context);
        mVideoItemsManager = VideoItemsManager.getInstance();
    }

    @Override
    public void onCreate(Fragment fragment, Bundle bundle) {
        Log.d(TAG, "onCreate, fragment = " + fragment);
        mVideoItemsManager.createCallLogItemsController(fragment);
    }

    @Override
    public void onDestroy(Fragment fragment) {
        Log.d(TAG, "onDestroy, fragment = " + fragment);
        mVideoItemsManager.destroyCallLogItemsController(fragment);
    }

    @Override
    public void onViewCreated(Fragment fragment, View view) {
        //mVideoItemsManager.initByFragment(fragment);
    }

    @Override
    public void showActions(Object obj, boolean show) {
        Log.d(TAG, "showActions, show = " + show);
        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.showActions(holder, show);
        }
    }

    @Override
    public void onAttachedToWindow(Fragment fragment, Object obj) {
        if (fragment == null) {
            Log.d(TAG, "onAttachedToWindow, fragment is null");
            return;
        }

        if (fragment instanceof CallLogMultipleDeleteFragment) {
            Log.d(TAG, "onAttachedToWindow, incorrect fragment");
            return;
        }

        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.onAttachedToController(fragment, holder);
        }
    }

    @Override
    public void onDetachedFromWindow(Object obj) {
        Log.d(TAG, "onDetachedFromWindow");
        if (obj instanceof CallLogListItemViewHolder) {
            CallLogListItemViewHolder holder = (CallLogListItemViewHolder) obj;
            mVideoItemsManager.onDetachedFromController(holder);
        }
    }
}