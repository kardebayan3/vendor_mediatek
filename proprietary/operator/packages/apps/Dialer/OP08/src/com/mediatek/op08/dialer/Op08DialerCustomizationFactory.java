package com.mediatek.op08.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.ICallDetailExtension;
import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.dialer.ext.IDialPadExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;
import com.mediatek.op08.dialer.calllog.Op08CalllogExtension;
import com.mediatek.op08.dialer.calllog.Op08CallDetailExtension;

public class Op08DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    private Context mContext;

    public Op08DialerCustomizationFactory(Context context) {
        mContext = context;
    }

    public IDialPadExtension makeDialPadExt() {
        return new Op08DialPadExtension(mContext);
    }

    public ICallLogExtension makeCallLogExt() {
        return new Op08CalllogExtension(mContext);
    }

    @Override
    public ICallDetailExtension makeCallDetailExt() {
        return new Op08CallDetailExtension(mContext);
    }
}
