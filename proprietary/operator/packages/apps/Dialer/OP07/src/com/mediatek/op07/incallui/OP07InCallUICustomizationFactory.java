package com.mediatek.op07.incallui;

import android.content.Context;

import com.mediatek.incallui.ext.IInCallExt;
import com.mediatek.incallui.ext.IVideoCallExt;
import com.mediatek.incallui.ext.OpInCallUICustomizationFactoryBase;

/**
 * InCallUI customization factory implementation.
 */
public class OP07InCallUICustomizationFactory extends OpInCallUICustomizationFactoryBase {
    private Context mContext;

    /** Constructor.
     * @param context context
     */
    public OP07InCallUICustomizationFactory(Context context) {
        mContext = context;
    }

    /** Interface to create the InCall extension implementation object.
     * @return IVideoCallExt Object for VideoCallExt interface implementation
     */
    public IVideoCallExt getVideoCallExt() {
        return new OP07VideoCallExt(mContext);
    }

    /** Interface to create the InCall extension implementation object.
     * @return IIncallExt Object for InCallExt interface implementation
     */
    public IInCallExt getInCallExt() {
        return new OP07InCallExt(mContext);
    }

}