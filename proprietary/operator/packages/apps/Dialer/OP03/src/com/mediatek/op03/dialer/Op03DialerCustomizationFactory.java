package com.mediatek.op03.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.IDialPadExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;

public class Op03DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    private Context mContext;

    public Op03DialerCustomizationFactory(Context context) {
        mContext = context;
    }

    public IDialPadExtension makeDialPadExt() {
        return new Op03DialPadExtension(mContext);
    }
}
