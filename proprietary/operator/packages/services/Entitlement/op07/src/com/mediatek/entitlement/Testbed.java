package com.mediatek.entitlement;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebViewClient;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.webkit.WebView;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class Testbed extends Activity {
    private static final String TAG = "Entitlement-Testbed";

    private static final String SERVICE_PKG_NAME = "com.mediatek.entitlement";
    private static final String SERVICE_NAME = "com.mediatek.entitlement.EntitlementService";

    private ISesService mSesService;

    static final int STATE_NOT_ENTITLED = -1;
    static final int STATE_PENDING_WITH_NO_TC = 0;
    static final int STATE_PENDING_WITH_NO_ADDRESS = 1;
    static final int STATE_PENDING_WITH_ADDRESS = 2;
    static final int STATE_ENTITlED = 3;
    static final int STATE_ENTITLEMENT_FAILED = 4;

    private boolean mVoWiFiOn;
    private ToggleButton mEntitleToggle;
    private ToggleButton mE911Toggle;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testbed);

        mEntitleToggle = (ToggleButton) findViewById(R.id.togglebutton);
        mEntitleToggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!mVoWiFiOn) {
                    try {
                        setVoWiFiOnOff(false);
                        mSesService.startEntitlementCheck("vowifi", 0, 0);
                        return;
                    } catch (RemoteException e) {
                        Log.e(TAG, "Exception happened! ", e);
                    }
                }
                setVoWiFiOnOff(!mVoWiFiOn);
            }
        });

        mE911Toggle = (ToggleButton) findViewById(R.id.e911button);
        mE911Toggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    mSesService.updateLocationAndTc("vowifi");
                } catch (RemoteException e) {
                    Log.e(TAG, "Exception happened! ", e);
                }
            }
        });

        bindWithSesService();
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            Log.d(TAG, "SES service Connected");
            mSesService = ISesService.Stub.asInterface(binder);
            try {
                mSesService.registerListener(mSesStateListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Exception happened! ", e);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "SES service DisConnected");
            try {
                mSesService.unregisterListener(mSesStateListener);
            } catch (RemoteException e) {
                Log.e(TAG, "Exception happened! ", e);
            }
            mSesService = null;
        }
    };

    private ISesServiceListener.Stub mSesStateListener = new ISesServiceListener.Stub() {
        @Override
        public void onEntitlementEvent(final String service, final String event, final Bundle extras) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onEntitlementEvent: " + service + ", " + event + ", " + extras);
                    switch (event) {
                        case "entitled":
                            setVoWiFiOnOff(true);
                            Toast.makeText(Testbed.this, "Entitlement succeeded", Toast.LENGTH_LONG).show();
                            break;
                        case "failed":
                            setVoWiFiOnOff(false);
                            Toast.makeText(Testbed.this, "Entitlement failed", Toast.LENGTH_LONG).show();
                            break;
                        case "pending":
                            Toast.makeText(Testbed.this, "Entitlement pending", Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            });
        }

        public void onWebsheetPost(final String url, final String postData) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startWebView(url, postData);
                }
            });
        }
    };

    private boolean bindWithSesService() {
        if (isPackageExist()) {
            Intent intent = new Intent();
            intent.setClassName(SERVICE_PKG_NAME, SERVICE_NAME);
            startService(intent);
            return bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        }
        return false;
    }

    private boolean isPackageExist() {
        try {
            getPackageManager().getPackageInfo(SERVICE_PKG_NAME, PackageManager.GET_SERVICES);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "package exist: false");
            return false;
        }
        Log.d(TAG, "package exist: true");
        return true;
    }

    private void setVoWiFiOnOff(boolean on) {
        Log.d(TAG, "setVoWiFiOnOff: " + on);
        mVoWiFiOn = on;
        mEntitleToggle.setChecked(mVoWiFiOn);
    }

    private class WiFiCallingWebViewControllerCallback {
        @JavascriptInterface
        public void phoneServicesAccountStatusChanged(final boolean isFinished) {
            Log.d(TAG, "phoneServicesAccountStatusChanged: " + isFinished);

            mWebView.post(new Runnable() {
                @Override
                public void run() {
                    finishWebView();
                }
            });

            try {
                if (isFinished) {
                    mSesService.startEntitlementCheck("vowifi", 0, 0);
                } else {
                    mSesService.startEntitlementCheck("vowifi", 30, 2);
                }
            } catch (RemoteException e) {
                Log.e(TAG, "Exception happened! ", e);
            }
        }
    }


    private class NsdsWebSheetController {

        /**
         * Constructor.
         *
         * @param context context
         */
        public NsdsWebSheetController() {
        }

        /**
         * Callback on user cancel(as per NSDS2.0).
         *
         * @return
         */
        @JavascriptInterface
        public void workflowAbandoned() {
            Log.d(TAG, "workflowAbandoned!!");
            finishWebView();
        }

        /**
         * Callback on user complete(as per NSDS2.0).
         *
         * @return
         */
        @JavascriptInterface
        public void workflowCompleted() {
            Log.d(TAG, "workflowCompleted!!");
            mWebView.loadUrl(
                    "javascript:window.WebSheetHandling.phoneServicesAccountStatusChanged("
                            + "WiFiCallingWebViewController.phoneServicesAccountStatusChanged())");
            finishWebView();
        }
    }

    public static String fromStream(InputStream inputStream) throws IOException {
        int bufSize = 1028;
        byte[] buffer = new byte[bufSize];
        int inSize;
        StringBuilder stringBuilder = new StringBuilder();
        while ((inSize = inputStream.read(buffer)) > 0) {
            stringBuilder.append(new String(buffer, 0, inSize));
        }
        return stringBuilder.toString();
    }

    private void startWebView(final String url, final String data) {
        Log.d(TAG, "going to open webview..");

        Log.d(TAG, "url=" + url);
        Log.d(TAG, "data=" + data);

        if (mWebView == null) {
            mWebView = new WebView(Testbed.this);
            setContentView(mWebView);

            mWebView.setWebViewClient(new WebViewClient() {
                @Override
                public void onReceivedHttpError(android.webkit.WebView view, android.webkit.WebResourceRequest request, android.webkit.WebResourceResponse errorResponse) {
                    int status = errorResponse.getStatusCode();
                    Log.e(TAG, "HTTP error " + status);

                    try {
                        switch (status) {
                            case 400: // Bad Request
                                break;
                            case 401:
                                JSONObject body = new JSONObject(fromStream(errorResponse.getData()));
                                String error = body.getString("error");
                                Log.e(TAG, "error: " + error);
                                switch (error) {
                                    case "invalid_authtoken":
                                        // Nothing we can do...
                                        break;
                                    case "expired_authtoken":
                                        // TODO: obtain a new server-data string from SES and try again
                                        startWebView(url, data);
                                        break;
                                }
                                break;
                            case 500:   // Internal Server Error
                                // TODO: should retry
                                break;
                        }
                    } catch (IOException | JSONException e) {
                        Log.e(TAG, "failed to read body", e);
                    }
                }
            });

            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.getSettings().setDomStorageEnabled(true);

            mWebView.addJavascriptInterface(new NsdsWebSheetController(), "NsdsWebSheetController");
            mWebView.addJavascriptInterface(new WiFiCallingWebViewControllerCallback(), "WiFiCallingWebViewController");

            mWebView.setWebContentsDebuggingEnabled(true);
        }
        mWebView.postUrl(url, data.getBytes());
    }

    private void finishWebView() {
        mWebView.destroy();
        mWebView.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        if (mWebView != null && mWebView.getVisibility() != View.GONE) {
            finishWebView();
        } else {
            super.onBackPressed();
        }
    }
}
