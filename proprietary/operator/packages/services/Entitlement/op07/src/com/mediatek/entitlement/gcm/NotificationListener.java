package com.mediatek.entitlement.gcm;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.gcm.GcmListenerService;

public class NotificationListener extends GcmListenerService {

    private static final String TAG = "Entitlement-gcm";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Data: " + data);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        Log.d(TAG, "notify entitlement service for the received notification!!");
        Intent intent = new Intent("com.mediatek.entitlement.gcm.PushNotification");
        intent.putExtras(data);
        getApplicationContext().sendBroadcast(intent);
    }
}
