package com.mediatek.op12.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class VideoSettingReceiver extends BroadcastReceiver {
    private static final String TAG = "Op12VideoSettingReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d(TAG, "Boot Completed");
        intent.setClass(context, VideoSettingReceiverService.class);
        context.startService(intent);
    }
}