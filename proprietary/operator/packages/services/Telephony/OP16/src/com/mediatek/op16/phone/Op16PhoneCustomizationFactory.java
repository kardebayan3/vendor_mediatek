package com.mediatek.op16.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op16PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private static final String TAG = "Op16PhoneCustomizationFactory";
    private Context mContext;

    public Op16PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    @Override
    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        Log.d(TAG, "makeCallFeaturesSettingExt");
        return new Op16CallFeaturesSettingExt(mContext);
    }

    @Override
    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.d(TAG, "makeMobileNetworkSettingsExt");
        return new Op16MobileNetworkSettingsExt(mContext);
    }
}
