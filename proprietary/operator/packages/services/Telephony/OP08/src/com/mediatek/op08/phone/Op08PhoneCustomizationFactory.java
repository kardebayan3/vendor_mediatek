package com.mediatek.op08.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.IDigitsUtilExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.ISsRoamingServiceExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op08PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    public Op08PhoneCustomizationFactory(Context context) {
        mContext = context;
    }


    @Override
    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.i("OP08PhoneCustomizationFactory", "makeMobileNetworkSettingsExt");
        return new OP08MobileNetworkSettingsExt(mContext);
    }

    @Override
    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        Log.d("OP08PhoneCustomizationFactory", "makeCallFeaturesSettingExt");
        return new Op08CallFeaturesSettingExt(mContext);
    }

    @Override
    public IDigitsUtilExt makeDigitsUtilExt() {
        return new Op08DigitsUtilExt();
    }

    @Override
    public ISsRoamingServiceExt makeSsRoamingServiceExt() {
        Log.d("OP08PhoneCustomizationFactory", "makeSsRoamingServiceExt");
        return new Op08SsRoamingServiceExt(mContext);
    }
}
