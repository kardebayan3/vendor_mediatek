#include "Rcs.h"

namespace vendor {
namespace mediatek {
namespace hardware {
namespace rcs {
namespace V1_0 {
namespace implementation {

// Methods from ::vendor::mediatek::hardware::rcs::V1_0::IRcs follow.
Return<void> Rcs::setResponseFunctions(const sp<IRcsIndication>& rcsIndication) {
    // TODO implement
    return Void();
}

Return<void> Rcs::writeEvent(int32_t request_id, int32_t length, const hidl_vec<int8_t>& value) {
    // TODO implement
    return Void();
}

Return<void> Rcs::writeBytes(const hidl_vec<int8_t>& value) {
    // TODO implement
    return Void();
}

Return<void> Rcs::writeInt(int32_t value) {
    // TODO implement
    return Void();
}


// Methods from ::android::hidl::base::V1_0::IBase follow.

IRcs* HIDL_FETCH_IRcs(const char* /* name */) {
    return new Rcs();
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace rcs
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor
