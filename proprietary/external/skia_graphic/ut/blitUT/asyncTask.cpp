#include "asyncTask.h"
#include "SkSize.h"
#include "SkCanvas.h"
#include "SkPaint.h"
#include "SkBitmap.h"
#include "SkStream.h"
#include "SkString.h"

#include "SkPngChunkReader.h"
#include "SkAndroidCodec.h"
#include "SkBRDAllocator.h"
#include "SkPixelRef.h"
#include "SkStream.h"
#include <memory>
#include "SkImageEncoder.h"
#include "SkMatrix.h"
#include "SkXfermode.h"


extern "C" {
    #include <stdio.h>
    #include <sys/time.h>
    #include <unistd.h>
    #include <stdlib.h>
}

#include <future>

MtkAsyncTask::MtkAsyncTask() {}

void MtkAsyncTask::doAsyncTask(SkImageInfo &decodeInfo, SkBitmap &decodingBitmap, SkBitmap &dstbm) {
    std::async(std::launch::async, &MtkAsyncTask::onDoAsnycTask, this,
        std::ref(decodeInfo), std::ref(decodingBitmap), std::ref(dstbm));
}

void MtkAsyncTask::onDoAsnycTask(SkImageInfo &decodeInfo, SkBitmap &decodingBitmap, SkBitmap &dstbm) {
    dstbm.setInfo(SkImageInfo::Make(decodeInfo.width(), decodeInfo.height(),
            decodeInfo.colorType(),
            decodeInfo.alphaType()));
    printf("destination bitmap width = %d height = %d\n", dstbm.width(), dstbm.height());
    if (!dstbm.tryAllocPixels(nullptr, nullptr)) {
        // This should only fail on OOM.  The recyclingAllocator should have
        // enough memory since we check this before decoding using the
        // scaleCheckingAllocator.
        printf("allocation failed for scaled bitmap");
        return;
    }

    typedef void (*Blit)(int width, int height, int alpha, uint32_t *dst,
                            int dstRB, const uint32_t *src, int srcRB, Func proc);
    Blit blitFuncs[3] = {
        blitRect_aosp,
        blitRect_half,
        blitRect_quater
    };
    
    /*measure interval*/
    struct  timeval start;
    struct  timeval end;  
    unsigned  long diff;
    for (int j = 0; j < 3; j++) {
        gettimeofday(&start,NULL);
        for (int i = 0; i < 1000; i ++) {
            std::async(std::launch::async, blitFuncs[j],dstbm.width(), dstbm.height(), 255, (uint32_t *)dstbm.pixelRef()->pixels(),
                        dstbm.rowBytes(), (const uint32_t *)decodingBitmap.pixelRef()->pixels(), decodingBitmap.rowBytes(),
                        blit_row_s32a_opaque);
        }
        gettimeofday(&end,NULL);
        diff = 1000000 * (end.tv_sec-start.tv_sec)+ end.tv_usec-start.tv_usec;
        printf("blitRect_aosp consumed %ld us\n",diff);
    }
}



