#ifndef _BOOTMODE_H
#define _BOOTMODE_H

#include "global_var.h"
#include "config.h"

typedef enum {
	NORMAL_BOOT = 0,
	META_BOOT = 1,
	RECOVERY_BOOT = 2,
	SW_REBOOT = 3,
	FACTORY_BOOT = 4,
	ADVMETA_BOOT = 5,
	ATE_FACTORY_BOOT = 6,
	ALARM_BOOT = 7,
	UNKNOWN_BOOT
} BOOTMODE;

extern BOOTMODE g_bootmode;
extern int ext_sdcard_ready;
extern int meta_mblogenable;

void set_status(MBLOGSTATUS status);
int check_internal_sdcard();
int check_external_sdcard();
int init_debug_config(void);
int deinit_debug_config(void);
void follow_up_work(void);
int get_bootmode(void);
int init_bootmode_and_config(void);
const char* read_bootmode_config(const char *key);
int timeout_in_this_boot(void);
// sd_type_t get_sd_type(void);
#define ItemMaxLen    32
//#define BOOTMAX       4
#define BOOTMAX       3

int getItemReverse(FILE* fp, char item[], int len, int no);


#endif
