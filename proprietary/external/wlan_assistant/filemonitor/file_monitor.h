#ifndef _FILE_MONITOR_H_
#define _FILE_MONITOR_H_

void tag_log(int type, const char* tag, const char *fmt, ...);

#ifdef LOGD
#undef LOGD
#endif
#ifdef LOGW
#undef LOGW
#endif
#ifdef LOGE
#undef LOGE
#endif
#define LOGD(...) tag_log(0, "[WLAN-ASSISTANT]", __VA_ARGS__);
#define LOGW(...) tag_log(0, " WARNING: [WLAN-ASSISTANT]", __VA_ARGS__);
#define LOGE(...) tag_log(1, " ERR: [WLAN-ASSISTANT]", __VA_ARGS__);

void *wlan_files_monitor(void*);
#endif
