#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sched.h>
#include <stdarg.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <mrdump_tool.h>
#include <mrdump_user.h>
#include <mrdump_support_ext4.h>

#include <sysenv_utils.h>

int data_os_type;

void md_printf(int log_prio, const char *fmt, ...)
{
    va_list ap;

    va_start(ap, fmt);
    __android_log_vprint(log_prio, LOG_TAG, fmt, ap);
    vprintf(fmt, ap);
    va_end(ap);
}

static void error(const char *msg, ...) __attribute__((noreturn));
static void error(const char *msg, ...)
{
    va_list ap;
    char msgbuf[128];

    va_start(ap, msg);
    vsnprintf(msgbuf, sizeof(msgbuf), msg, ap);
    printf("Error: %s", msgbuf);
    __android_log_vprint(ANDROID_LOG_ERROR, LOG_TAG, msg, ap);
    va_end(ap);

    exit(2);
}

static void ok(const char *msg, ...) __attribute__((noreturn));
static void ok(const char *msg, ...)
{
    va_list ap;
    char msgbuf[128];

    va_start(ap, msg);
    vsnprintf(msgbuf, sizeof(msgbuf), msg, ap);
    printf("OK: %s", msgbuf);
    __android_log_vprint(ANDROID_LOG_INFO, LOG_TAG, msg, ap);
    va_end(ap);

    exit(0);
}

static void usage(const char *prog) __attribute__((noreturn));
static void usage(const char *prog)
{
    printf("Usage\n"
        "\t%1$s is-supported\n\n"
        "\t%1$s status-get\n"
        "\t%1$s status-log\n"
        "\t%1$s status-clear\n\n"
        "\t%1$s file-setup\n"
        "\t%1$s file-allocate n\n"
        "\t\tn is 0(disable file output) 1(halfmem) 2(fullmem) >256\n"
        "\t%1$s file-extract-core [-r] filename\n"
        "\t\t-r\tre-init pre-allocated file\n"
        "\t%1$s file-info\n\n"
        "\t%1$s mem-size-set n\n"
        "\t\tn is between 64 ~ 16384(m), 0 is output total-mem-size\n\n"
        "\t%1$s output-set output\n"
        "\t\tsetting mrdump output device (none, null, usb, internal-storage:ext4, internal-storage:vfat)\n"
        "\t%1$s output-get\n"
        "\t\tgetting mrdump output device\n",
        prog);
    exit(1);
}

static void dump_status_ok(const struct mrdump_status_result *result)
{
    printf("Ok\n");
    printf("\tMode: %s\n\tOutput: ", result->mode);

    switch (result->output) {
    case MRDUMP_OUTPUT_NULL:
        printf("null\n");
        break;
    case MRDUMP_OUTPUT_USB:
        printf("usb\n");
        break;
    case MRDUMP_OUTPUT_EXT4_DATA:
        printf("ext4/data partition\n");
        break;
    case MRDUMP_OUTPUT_VFAT_INT_STORAGE:
        printf("vfat/internal partition\n");
        break;
    }
}

static int file_setup_command(int argc, char * __attribute__((unused)) argv[])
{
    if (argc != 1) {
        error("Invalid file-setup command argument\n");
    }
    mrdump_file_setup(false);
    return 0;
}

static void file_allocate_command(int argc, char *argv[])
{
    if (argc != 2) {
        error("Invaid file-allocate command argument\n");
    }
    int size_m = atoi(argv[1]);

    if (size_m < 0)
        size_m = 0;

    if ((size_m <= 2) || (size_m >= MRDUMP_EXT4_MIN_ALLOCATE)) {
        // enable condition: only 0, 1, 2, >256
        mrdump_file_set_maxsize(size_m);
    }
    else {
        error("Invalid dump size %d\n", size_m);
    }
}

static int file_extract_core_command(int argc, char *argv[])
{
    int opt;
    bool reinit = false;

    while ((opt = getopt(argc, argv, "r")) != -1) {
        switch (opt) {
        case 'r':
            reinit = true;
            break;
        default:
            error("Invalid file-extract-core parameter\n");
        }
    }
    if (optind >= argc) {
        error("Expected filename after options\n");
    }

    const char *fn = argv[optind];
    if (mrdump_file_fetch_zip_coredump(fn)) {
        mrdump_file_setup(reinit);
        return 0;
    }
    error("Fetching Coredump data failed\n");
}

static int file_info_command(int argc, char * __attribute__((unused)) argv[])
{
    if (argc != 1) {
        error("Invalid file-info command argument\n");
    }

    struct mrdump_pafile_info info;
    if (mrdump_file_get_info(MRDUMP_EXT4_ALLOCATE_FILE, &info)) {
        printf("\tInfo LBA :      %" PRIu32 "\n"
               "\tAddress LBA :   %" PRIu32 "\n"
               "\tFile Size :     %" PRIu64 "\n"
               "\tCoredump Size : %" PRIu64 "\n"
               "\tTimestamp :     %" PRIx64 "\n",
               info.info_lba, info.addr_lba,
               info.filesize, info.coredump_size,
               info.timestamp
              );
        return 0;
    }
    else {
        error("Cannot get pre-allocate file info\n");
    }
}

static void mem_size_set_command(int argc, char *argv[])
{
    char msize[5];

    if (argc != 2) {
        error("Invaid mem-size-set command argument\n");
    }

    int size_m = atoi(argv[1]);
    if ((size_m == 0) || ((size_m >= 64) && (size_m <= 16 * 1024))) {
        if (size_m != 0) {
            snprintf(msize, sizeof(msize), "%d", size_m);
            if (sysenv_set("mrdump_mem_size", msize) == 0) {
                ok("mem-size-set done.\n");
            }
        }
        else {
            if (sysenv_set("mrdump_mem_size", "") == 0) {
                ok("total-mem-size done.\n");
            }
        }

        /* sysenv_set return -1 */
        error("failed to set memory dump size, plz try again later.\n");
    }
    else {
        error("Invalid memory dump size\n");
    }
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        usage(argv[0]);
    }

    data_os_type = mrdump_get_data_os();

    if (strcmp(argv[1], "is-supported") == 0) {
        if (mrdump_is_supported()) {
            printf("MT-RAMDUMP support ok\n");
        }
        else {
            printf("MT-RAMDUMP not support\n");
        }
    }
    else if (strcmp(argv[1], "status-get") == 0) {
        struct mrdump_status_result result;
        if (mrdump_status_get(&result)) {
            printf("MT-RAMDUMP\n\tStatus:");
            switch (result.status) {
            case MRDUMP_STATUS_NONE:
                printf("None\n");
                break;
            case MRDUMP_STATUS_FAILED:
                printf("Failed\n");
                break;
            case MRDUMP_STATUS_OK:
                dump_status_ok(&result);
                break;
            }
        }
        else {
            printf("MT-RAMDUMP get status failed\n");
        }
    }
    else if (strcmp(argv[1], "status-log") == 0) {
        struct mrdump_status_result result;
        if (!mrdump_status_get(&result)) {
            printf("MT-RAMDUMP get status failed\n");
        }
        printf("=>status line:\n%s\n=>log:\n%s\n", result.status_line, result.log_buf);
    }
    else if (strcmp(argv[1], "status-clear") == 0) {
        if (!mrdump_is_supported()) {
            error("MT-RAMDUMP not support\n");
        }
        if (!mrdump_status_clear()) {
            printf("MT-RAMDUMP Status clear failed\n");
        }
    }
    else if (strcmp(argv[1], "file-setup") == 0) {
        if (mrdump_is_supported()) {
            file_setup_command(argc - 1, &argv[1]);
        }
        else {
            printf("file-setup not allowed in this mode.\n");
        }
    }
    else if (strcmp(argv[1], "file-allocate") == 0) {
        if (mrdump_is_supported()) {
            file_allocate_command(argc - 1, &argv[1]);
        }
        else {
            printf("file-allocate not allowed in this mode.\n");
        }
    }
    else if (strcmp(argv[1], "file-extract-core") == 0) {
        file_extract_core_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "file-info") == 0) {
        file_info_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "mem-size-set") == 0) {
        mem_size_set_command(argc - 1, &argv[1]);
    }
    else if (strcmp(argv[1], "output-set") == 0) {
        if(argc == 2) {
            error("Invalid output device, valid input [default, none, null, usb, internal-storage:ext4, internal-storage:vfat]\n");
        }
        else {
            const char *output_dev = argv[2];
            if ((strcmp(output_dev, "none") == 0) ||
                (strcmp(output_dev, "null") == 0) ||
                (strcmp(output_dev, "usb") == 0) ||
                (strcmp(output_dev, "internal-storage:ext4") == 0) ||
                (strcmp(output_dev, "internal-storage:vfat") == 0)) {
                if (sysenv_set("mrdump_output", output_dev) == 0) {
                    ok("mrdump_output = %s\n", output_dev);
                }
            }
            else if (strcmp(output_dev, "default") == 0) {
                if (sysenv_set("mrdump_output", "") == 0) {
                    ok("default mrdump_output device\n");
                }
            }
            else {
                error("Invalid output device, valid input [default, none, null, usb, internal-storage:ext4, internal-storage:vfat]\n");
            }

            /* sysenv_set return -1 */
            error("failed to set mrdump output dev, plz try again later.\n");
        }
    }
    else if (strcmp(argv[1], "output-get") == 0) {
        const char *output_dev = sysenv_get("mrdump_output");
        if (!output_dev) {
            printf("default\n");
        }
        else {
            printf("%s\n", output_dev);
        }
    }
    else {
        error("Unknown command %s\n", argv[1]);
    }

    return 0;
}
