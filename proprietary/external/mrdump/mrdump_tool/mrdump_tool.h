#pragma once

#define LOG_TAG "mrdump_tool"
#include <android/log.h>

void md_printf(int log_prio, const char *fmt, ...);

/* LOG macro support */
#define MD_LOGV(...) md_printf(ANDROID_LOG_VERBOSE, __VA_ARGS__)
#define MD_LOGD(...) md_printf(ANDROID_LOG_DEBUG, __VA_ARGS__)
#define MD_LOGI(...) md_printf(ANDROID_LOG_INFO, __VA_ARGS__)
#define MD_LOGW(...) md_printf(ANDROID_LOG_WARN, __VA_ARGS__)
#define MD_LOGE(...) md_printf(ANDROID_LOG_ERROR, __VA_ARGS__)

/* common variables */
extern int data_os_type;
