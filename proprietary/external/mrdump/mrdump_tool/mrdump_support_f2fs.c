#include "mrdump_support_ext4.h"
#include "mrdump_support_f2fs.h"
#include "mrdump_tool.h"

bool mount_as_f2fs(const char *mountp)
{
    struct statfs fs;
    if(statfs(mountp, &fs) == 0) {
        if(fs.f_type == F2FS_SUPER_MAGIC)
            return true;

        MD_LOGI("%s: %s is not f2fs.(0x%08x)\n", __func__, mountp, fs.f_type);
        return false;
    }
    MD_LOGE("%s: statfs error.\n", __func__);
    return false;
}

bool f2fs_fallocate(const char *allocfile, uint64_t allocsize)
{
    int i;
    int zQ = (allocsize + F2FS_MAPSIZE - 1) / F2FS_MAPSIZE;
    unsigned int val;

    if((allocfile == NULL) || (allocsize == 0)) {
        MD_LOGE("%s: allocfile is NULL or allocsize = %" PRIu64 "\n", __func__, allocsize);
        return false;
    }

    void *ZeroPage = mmap(NULL, F2FS_MAPSIZE, PROT_READ, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
    if(ZeroPage == NULL) {
        MD_LOGE("%s: ZeroPage map failed.\n", __func__);
        return false;
    }

    int fd = open(allocfile, O_RDWR | O_CREAT, 0400);
    if(0 > fd) {
        MD_LOGE("%s: open fd failed.\n", __func__);
        munmap(ZeroPage, F2FS_MAPSIZE);
        return false;
    }

    for(i=0; i<zQ; i++) {
        if(0 >= write(fd,ZeroPage, F2FS_MAPSIZE)) {
            MD_LOGE("%s: ZeroPage write failed\n", __func__);
            close(fd);
            unlink(allocfile);
            munmap(ZeroPage, F2FS_MAPSIZE);
            return false;
        }
    }

    fsync(fd);

    val = 31337;
    if(0 > ioctl(fd, F2FS_IOC_SET_PIN_FILE, &val)) {
        MD_LOGE("%s: F2FS_IOC_SET_PIN_FILE(%u) failed(%d), %s\n", __func__, val, errno, strerror(errno));
    }

    close(fd);
    munmap(ZeroPage, F2FS_MAPSIZE);
    sync();

    return true;
}
