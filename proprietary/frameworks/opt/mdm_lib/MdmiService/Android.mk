LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE := MdmiService
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_SUFFIX := .apk
LOCAL_PROPRIETARY_MODULE := false
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := MdmiService.apk
include $(BUILD_PREBUILT)
endif
