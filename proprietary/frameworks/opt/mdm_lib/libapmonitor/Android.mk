LOCAL_PATH := $(call my-dir)

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE := libapmonitor
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES_64 := libcutils libasn1c_core libasn1c_mdmi libasn1c_mapi vendor.mediatek.hardware.apmonitor@1.0 libhidlbase libhidltransport libhwbinder
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/libapmonitor/include
LOCAL_MULTILIB := 64
LOCAL_SRC_FILES_64 := arm64_$(TARGET_ARCH_VARIANT)_$(TARGET_CPU_VARIANT)/libapmonitor.so
include $(BUILD_PREBUILT)
endif

ifeq (,$(wildcard vendor/mediatek/proprietary/frameworks/opt/mdm))
include $(CLEAR_VARS)
LOCAL_MODULE := libapmonitor
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
LOCAL_MODULE_TAGS := optional
LOCAL_SHARED_LIBRARIES := libcutils libasn1c_core libasn1c_mdmi libasn1c_mapi vendor.mediatek.hardware.apmonitor@1.0 libhidlbase libhidltransport libhwbinder
LOCAL_EXPORT_C_INCLUDE_DIRS := $(LOCAL_PATH)/libapmonitor/include
LOCAL_MULTILIB := 32
LOCAL_SRC_FILES_32 := arm_$(TARGET_ARCH_VARIANT)_$(TARGET_CPU_VARIANT)/libapmonitor.so
include $(BUILD_PREBUILT)
endif
