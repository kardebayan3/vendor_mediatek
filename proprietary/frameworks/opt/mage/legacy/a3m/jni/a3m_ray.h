/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Ray "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_RAY_H
#define JNI_A3M_RAY_H

#include <a3m/ray.h>

class A3mRay
{
private:
  a3m::Ray m_native;

protected:
  A3mRay(a3m::Ray const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mRay toWrapper(a3m::Ray const& native)
  {
    return A3mRay(native);
  }

  a3m::Ray const& getNative() const
  {
    return m_native;
  }

public:
  virtual ~A3mRay()
  {
  }

  void setPosition(float positionX, float positionY, float positionZ)
  {
    m_native.setPosition(a3m::Vector3f(positionX, positionY, positionZ));
  }

  float getPositionX() const
  {
    return m_native.getPosition().x;
  }

  float getPositionY() const
  {
    return m_native.getPosition().y;
  }

  float getPositionZ() const
  {
    return m_native.getPosition().z;
  }

  void setDirection(float directionX, float directionY, float directionZ)
  {
    m_native.setDirection(a3m::Vector3f(directionX, directionY, directionZ));
  }

  float getDirectionX() const
  {
    return m_native.getDirection().x;
  }

  float getDirectionY() const
  {
    return m_native.getDirection().y;
  }

  float getDirectionZ() const
  {
    return m_native.getDirection().z;
  }

  void setToCameraRay(
      A3mCamera camera,
      float screenWidth,
      float screenHeight,
      float pickX,
      float pickY)
  {
    // In screen space (post-projection), the screen exists on a 2x2 plane at
    // -1 on the z-axis, so to find the ray that corresponds to a particular
    // screen coordinate, we must map the screen coordinate in pixels onto the
    // 2x2 plane, and then find the vector from the origin to that point.  The
    // ray is then transformed back into world-space using the inverse world
    // view projection matrix.
    A3M_FLOAT x = (pickX / screenWidth - 0.5f) * 2.0f;
    A3M_FLOAT y = (pickY / screenHeight - 0.5f) * -2.0f;

    // The inverse view-projection matrix is required to transform the
    // normalized coordinates into world-space.
    A3M_FLOAT aspectRatio = screenWidth / screenHeight;
    a3m::Matrix4f view = a3m::inverse(camera.getNative()->getWorldTransform());
    a3m::Matrix4f projection;
    camera.getNativeCamera()->getProjection(&projection, aspectRatio);
    a3m::Matrix4f inverseViewProjection = a3m::inverse(projection * view);

    // Calculate the points corresponding to the screen position at the near
    // and far clipping planes, and find the vector between them.
    a3m::Vector4f rayStart =
      inverseViewProjection * a3m::Vector4f(x, y, -1.0f, 1.0f);
    rayStart = rayStart / rayStart.w;

    a3m::Vector4f rayEnd =
      inverseViewProjection * a3m::Vector4f(x, y, 1.0f, 1.0f);
    rayEnd = rayEnd / rayEnd.w;

    a3m::Vector4f rayVector = normalize(rayEnd - rayStart);

    // Set the ray state.
    m_native.setPosition(a3m::Vector3f(rayStart));
    m_native.setDirection(a3m::Vector3f(rayVector));
  }
};

#endif // JNI_A3M_RAY_H
