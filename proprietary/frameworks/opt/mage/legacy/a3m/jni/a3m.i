// Instructions for wrapping a new class:
//   1. Copy an example of an already wrapped class.  Choose an class based on
//      whether the native A3M class is Shared or not, as the wrappers use
//      slightly different semantics and helper functions.  If the class is
//      part of an inheritance hierarchy, copy the RenderBlock class hierarchy,
//      and make sure to add any sub-class getNative*() functions to the ignore
//      list below.
//   2. Add a SWIG_OWNS_CLASS() macro call for the class below.  This ensures
//      that SWIG takes ownership of pointers returned from functions.
//   3. Add a WRAP_CLASS() macro call below.  This performs a variety of
//      mappings required to wrap an A3M class.
//   4. Run genjni.bat and test the new wrapper.

// The module containing any global functions will be called "A3m" and the
// intermediary JNI wrapper class will be called "A3mJni".
%module (jniclassname="A3mJni") A3m

// Required to wrap C arrays as Java arrays.
%include arrays_java.i
// Required for typedefs like BYTE.
%include various.i

// Import all J3M classes into Java module class.
%pragma(java) moduleimports=%{
import com.mediatek.j3m.*;
%}

%define WRAP_CLASS(name, filename)
// Tell this class to implement the corresponding J3M interface.
%typemap(javainterfaces) A3m ## name #name

// Tell SWIG to use the J3M interface in the Java function signitures.
%typemap(jstype) A3m ## name #name
%typemap(jstype) A3m ## name const #name
%typemap(jstype) A3m ## name& #name
%typemap(jstype) A3m ## name const& #name
%typemap(jstype) A3m ## name* #name
%typemap(jstype) A3m ## name const* #name

// Tell SWIG to cast J3M interface back down to the concrete class.
%typemap(javain, pgcppname="(A3m" #name ") $javainput") A3m ## name "A3m" #name ".getCPtr((A3m" #name ") $javainput)"
%typemap(javain, pgcppname="(A3m" #name ") $javainput") A3m ## name const "A3m" #name ".getCPtr((A3m" #name ") $javainput)"
%typemap(javain, pgcppname="(A3m" #name ") $javainput") A3m ## name& "A3m" #name ".getCPtr((A3m" #name ") $javainput)"
%typemap(javain, pgcppname="(A3m" #name ") $javainput") A3m ## name const& "A3m" #name ".getCPtr((A3m" #name ") $javainput)"
%typemap(javain, pgcppname="(A3m" #name ") $javainput") A3m ## name* "A3m" #name ".getCPtr((A3m" #name ") $javainput)"
%typemap(javain, pgcppname="(A3m" #name ") $javainput") A3m ## name const* "A3m" #name ".getCPtr((A3m" #name ") $javainput)"

// Import all J3M classes.
%typemap(javaimports) A3m ## name %{
import com.mediatek.j3m.*;
%}

// Pull in the C++ code to wrap.
%include #filename

// Add include into the generated C++ file.
%{ #include "filename" %}
%enddef

// Give SWIG ownership of returned pointers.
%define SWIG_OWNS_CLASS(name)
%typemap(javaout) A3m ## name* {
    long cPtr = $jnicall;
    return (cPtr == 0) ? null : new A3m ## name(cPtr, true);
  }
%enddef

/***************************************************
 * Typemaps for (GraphicBuffer *)
 ***************************************************/
%typemap(in)(android::sp<android::GraphicBuffer>)(){

sp<GraphicBuffer> buffer(graphicBufferForJavaObject(jenv, $input));
$1=buffer;
}

%typemap(jni)(android::sp<android::GraphicBuffer>)"jobject"
%typemap(jtype)(android::sp<android::GraphicBuffer>)"android.view.GraphicBuffer"
%typemap(jstype)(android::sp<android::GraphicBuffer>)"android.view.GraphicBuffer"
%typemap(javain)(android::sp<android::GraphicBuffer>)"$javainput"

/*
%typemap(javaout)(android::sp<android::GraphicBuffer>){
   return $jnicall;
}
*/

// List of functions to omit from all classes.
%ignore "toWrapper";
%ignore "toNative";
%ignore "getNative";
%ignore "getNativeCamera";
%ignore "getNativeLight";
%ignore "getNativeSolid";
%ignore "getNativeRenderBlock";
%ignore "getNativeRenderBlockGroup";
%ignore "getNativeRenderTarget";
%ignore "getNativePlane";
%ignore "getNativeSquare";
%ignore "getNativeSphere";

// List names to change when converting to Java.
%rename and_ "and";
%rename or_ "or";
%rename xor_ "xor";

// Add Java code manually for special cases.

// We need a proxy method to ensure that an array of the correct size is passed
// to the function.
%typemap(javacode, noblock=1) A3mJ3m
%{
  public byte[] getPixels(int x, int y, int width, int height) {
    byte[] pixels = new byte[width * height * 4];
    getPixels(pixels, x, y, width, height);
    return pixels;
  }
%}

%typemap(javacode, noblock=1) A3mAssetPool
%{
  public void registerSource(android.content.res.Resources resources) {
    A3m.A3mAssetPool_registerSource_ResourceDataSource(
        this, new ResourceDataSource(resources));
  }

  public void registerSource(android.content.res.AssetManager assets) {
    A3m.A3mAssetPool_registerSource_AssetManager(this, assets);
  }
%}

// Make SWIG own pointers to all classes, but handle SceneNode specially.
// The order of this list is important where the interface of one class depends
// upon another. E.g. RenderBlock has a method "setRenderTarget" which takes a
// RenderTarget object. RenderTarget should appear on the list before
// RenderBlock.
SWIG_OWNS_CLASS(FlagMask)
SWIG_OWNS_CLASS(Texture2D)
SWIG_OWNS_CLASS(TextureCube)
SWIG_OWNS_CLASS(ShaderProgram)
SWIG_OWNS_CLASS(Appearance)
SWIG_OWNS_CLASS(Mesh)
SWIG_OWNS_CLASS(Camera)
SWIG_OWNS_CLASS(Light)
SWIG_OWNS_CLASS(Solid)
SWIG_OWNS_CLASS(Animation)
SWIG_OWNS_CLASS(AnimationController)
SWIG_OWNS_CLASS(Model)
SWIG_OWNS_CLASS(AssetPool)
SWIG_OWNS_CLASS(RenderContext)
SWIG_OWNS_CLASS(Renderer)
SWIG_OWNS_CLASS(RenderTarget)
SWIG_OWNS_CLASS(RenderBlockBase)
SWIG_OWNS_CLASS(RenderBlock)
SWIG_OWNS_CLASS(RenderBlockGroup)
SWIG_OWNS_CLASS(Ray)
SWIG_OWNS_CLASS(Shape)
SWIG_OWNS_CLASS(Plane)
SWIG_OWNS_CLASS(Square)
SWIG_OWNS_CLASS(Version)
SWIG_OWNS_CLASS(J3m)

// When returning a SceneNode, make sure to create the correct type.
%typemap(javaout) A3mSceneNode* {
    long ptr = $jnicall;

    if (ptr == 0) {
      return null;
    }

    // Do not given ownership initially, otherwise the underlying pointer will
    // be deleted multiple times.
    A3mSceneNode node = new A3mSceneNode(ptr, false);

    switch (node.getNodeType()) {
      case Solid.NODE_TYPE:
        node = new A3mSolid(ptr, true);
        break;

      case Light.NODE_TYPE:
        node = new A3mLight(ptr, true);
        break;

      case Camera.NODE_TYPE:
        node = new A3mCamera(ptr, true);
        break;

      default:
        node = new A3mSceneNode(ptr, true);
        break;
    }

    return node;
  }

// Wrap all classes.
// The order of this list is important where the interface of one class depends
// upon another. E.g. RenderBlock has a method "setRenderTarget" which takes a
// RenderTarget object. RenderTarget should appear on the list before
// RenderBlock.
WRAP_CLASS(FlagMask, a3m_flagmask.h)
WRAP_CLASS(Texture2D, a3m_texture2d.h)
WRAP_CLASS(TextureCube, a3m_texturecube.h)
WRAP_CLASS(ShaderProgram, a3m_shaderprogram.h)
WRAP_CLASS(Appearance, a3m_appearance.h)
WRAP_CLASS(Mesh, a3m_mesh.h)
WRAP_CLASS(SceneNode, a3m_scenenode.h)
WRAP_CLASS(Camera, a3m_camera.h)
WRAP_CLASS(Light, a3m_light.h)
WRAP_CLASS(Solid, a3m_solid.h)
WRAP_CLASS(Animation, a3m_animation.h)
WRAP_CLASS(AnimationController, a3m_animationcontroller.h)
WRAP_CLASS(Model, a3m_model.h)
WRAP_CLASS(AssetPool, a3m_assetpool.h)
WRAP_CLASS(RenderContext, a3m_rendercontext.h)
WRAP_CLASS(Renderer, a3m_renderer.h)
WRAP_CLASS(RenderTarget, a3m_rendertarget.h)
WRAP_CLASS(RenderBlockBase, a3m_renderblockbase.h)
WRAP_CLASS(RenderBlock, a3m_renderblock.h)
WRAP_CLASS(RenderBlockGroup, a3m_renderblockgroup.h)
WRAP_CLASS(Ray, a3m_ray.h)
WRAP_CLASS(Shape, a3m_shape.h)
WRAP_CLASS(Plane, a3m_plane.h)
WRAP_CLASS(Square, a3m_square.h)
WRAP_CLASS(Sphere, a3m_sphere.h)
WRAP_CLASS(Version, a3m_version.h)
WRAP_CLASS(J3m, a3m_j3m.h)

// Wrap the custom JNI function as a global, since SWIG doesn't support custom
// class methods.
%native (A3mAssetPool_registerSource_ResourceDataSource) void registerSource_ResourceDataSource(A3mAssetPool, jobject);
%native (A3mAssetPool_registerSource_AssetManager) void registerSource_AssetManager(A3mAssetPool, jobject);
%{

#include <android/asset_manager_jni.h>      /* for AAssetManager_fromJava   */
#include <android/assetmgrstream_android.h> /* for AssetManagerStreamSource */
#include <resourcestream.h>                 /* for ResourceStreamSource     */
#include <core/SkBitmap.h>
#include <nativehelper/JNIHelp.h>
#include <android/bitmap.h>

#ifdef __cplusplus
extern "C" {
#endif

SWIGEXPORT void JNICALL Java_com_mediatek_ja3m_A3mJni_A3mAssetPool_1registerSource_1ResourceDataSource(
    JNIEnv* env,
    jclass jClass,
    jlong jSelf,
    jobject jThis,
    jobject jResourceDataSource)
{
  if (!jResourceDataSource)
  {
    SWIG_JavaThrowException(env, SWIG_JavaNullPointerException, "ResourceDataSource resourceDataSource is null");
    return;
  }

  A3mAssetPool* self = reinterpret_cast<A3mAssetPool*>(jSelf);
  ResourceStreamSource::Ptr source(new ResourceStreamSource(
        env, jResourceDataSource));
  self->getNative()->registerSource(source);
}

SWIGEXPORT void JNICALL Java_com_mediatek_ja3m_A3mJni_A3mAssetPool_1registerSource_1AssetManager(
    JNIEnv* env,
    jclass jClass,
    jlong jSelf,
    jobject jThis,
    jobject jAssetManager)
{
  if (!jAssetManager)
  {
    SWIG_JavaThrowException(env, SWIG_JavaNullPointerException, "AssetManager assets is null");
    return;
  }

  A3mAssetPool* self = reinterpret_cast<A3mAssetPool*>(jSelf);

  a3m::StreamSource::Ptr source(new a3m::AssetMgrStreamSource(
        AAssetManager_fromJava(env, jAssetManager)));

  self->getNative()->registerSource(source);
}

#ifdef __cplusplus
}
#endif
%}
