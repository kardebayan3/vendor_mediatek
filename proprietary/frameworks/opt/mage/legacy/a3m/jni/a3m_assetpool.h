/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M AssetPool "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_ASSETPOOL_H
#define JNI_A3M_ASSETPOOL_H

#include <a3m/assetcachepool.h>
#include <a3m/sceneutility.h>
#include <a3m_appearance.h>
#include <a3m_model.h>
#include <a3m_shaderprogram.h>
#include <a3m_texture2d.h>
#include <a3m_texturecube.h>
#include <GraphicBuffer.h>
#include <core/SkBitmap.h>

class A3mAssetPool
{
private:
  a3m::AssetCachePool::Ptr m_native;

  A3mAssetPool(a3m::AssetCachePool::Ptr const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mAssetPool* toWrapper(a3m::AssetCachePool::Ptr const& native)
  {
    return native ? new A3mAssetPool(native) : 0;
  }

  static a3m::AssetCachePool::Ptr toNative(A3mAssetPool* wrapper)
  {
    return wrapper ? wrapper->getNative() : a3m::AssetCachePool::Ptr();
  }

  a3m::AssetCachePool::Ptr const& getNative() const
  {
    return m_native;
  }

public:
  void registerSource(char const* path)
  {
    a3m::registerSource(*m_native, path);
  }

  void setCacheSource(char const* path)
  {
    a3m::setCacheSource(*m_native, path);
  }

  void flush()
  {
    m_native->flush();
  }

  void release()
  {
    m_native->release();
  }

  A3mShaderProgram* getShaderProgram(char const* name)
  {
    return A3mShaderProgram::toWrapper(
        m_native->shaderProgramCache()->get(name));
  }

  A3mTexture2D* getTexture2D(char const* name)
  {
    return A3mTexture2D::toWrapper(
        m_native->texture2DCache()->get(name));
  }

  // BYTE tells SWIG to interpret this as a byte array rather than a string.
  A3mTexture2D* createTexture2D(
      int width, int height, int format, int type, char const* BYTE,
      char const* name = 0)
  {
    return A3mTexture2D::toWrapper(m_native->texture2DCache()->create(
          width,
          height,
          static_cast<a3m::Texture::Format>(format),
          static_cast<a3m::Texture::Type>(type),
          BYTE,
          name));
  }

  // BITMAP_PIXELS tells SWIG to interpret this as a Android bitmap pixels point.
  A3mTexture2D* createTexture2D(
      int width, int height, int format, int type, void const* BITMAP_PIXELS,
      char const* name = 0)
  {
    return A3mTexture2D::toWrapper(m_native->texture2DCache()->create(
          width,
          height,
          static_cast<a3m::Texture::Format>(format),
          static_cast<a3m::Texture::Type>(type),
          BITMAP_PIXELS,
          name));
  }

  A3mTexture2D* createTexture2D(android::sp<android::GraphicBuffer> buffer)
  {
    return A3mTexture2D::toWrapper(m_native->texture2DCache()->create(buffer));
  }

  A3mTextureCube* getTextureCube(char const* name)
  {
    return A3mTextureCube::toWrapper(
        m_native->textureCubeCache()->get(name));
  }

  A3mAppearance* loadAppearance(char const* name)
  {
    return A3mAppearance::toWrapper(
        a3m::loadAppearance(*m_native, name));
  }

  void applyAppearance(A3mAppearance appearance, char const* name)
  {
    appearance.getNative()->apply(*m_native, name);
  }

  A3mModel* loadModel(char const* name)
  {
    return A3mModel::toWrapper(
        a3m::loadGloFile(*m_native, a3m::SceneNode::Ptr(), name));
  }

  A3mModel* loadModel(char const* name, A3mSceneNode& scene)
  {
    return A3mModel::toWrapper(
        a3m::loadGloFile(*m_native, scene.getNative(), name));
  }

  int getTexture2DMemoryUsage() const
  {
    return a3m::getTotalAssetSizeInBytes(*m_native->texture2DCache());
  }

  A3mSolid* createSquare()
  {
    return A3mSolid::toWrapper(a3m::createSquare(*m_native));
  }

  A3mSolid* createSquare(float scaleU, float scaleV)
  {
    return A3mSolid::toWrapper(
        a3m::createSquare(*m_native, a3m::Vector2f(scaleU, scaleV)));
  }

  A3mSolid* createCube()
  {
    return A3mSolid::toWrapper(a3m::createCube(*m_native));
  }

  A3mSolid* createCube(float scaleU, float scaleV)
  {
    return A3mSolid::toWrapper(
        a3m::createCube(*m_native, a3m::Vector2f(scaleU, scaleV)));
  }

  A3mSolid* createSphere(int segmentCount, int wedgeCount)
  {
    return A3mSolid::toWrapper(
        a3m::createSphere(*m_native, segmentCount, wedgeCount));
  }

  A3mSolid* createSphere(
      int segmentCount, int wedgeCount,
      float scaleU, float scaleV)
  {
    return A3mSolid::toWrapper(
        a3m::createSphere(*m_native,
          segmentCount, wedgeCount,
          a3m::Vector2f(scaleU, scaleV)));
  }
};

#endif // JNI_A3M_ASSETPOOL_H
