/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * Java API internal class for acquiring Resources as byte arrays
 */
package com.mediatek.ja3m;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.InputStream;
import java.io.IOException;

/**
 * \ingroup a3mJrefBase
 * @{
 */
/**
 * ResourceDataSource is an underlying component of the JNI - it is not
 * directly relevant to an API user.
 *
 * The only function of this class is to provide access to Android resources
 * for the C++ ResourceStreamSource class.
 */
public class ResourceDataSource {
    private static final String TAG = "ResourceDataSource";

    /* Android Resources container */
    private Resources mResources;

    /**
     * Constructor.
     *
     * @param resources The Android Resources conainer to use
     */
    public ResourceDataSource(Resources resources) {
        mResources = resources;
    }

    /**
     * Gets the contents of a Resource as a byte array.
     *
     * @param name Name of the resource to get
     * @return Byte array containing the Resource data, or null if the resource
     * was not found
     */
    public byte[] get(String name) {
        byte[] data = null;

        // Get the resource's ID from its name.  We assume that the resource
        // type and package data are part of the name.
        int id = mResources.getIdentifier(name, null, null);

        if (id > 0) {
            InputStream inputStream = null;
            ByteArrayOutputStream outputStream = null;
            try {
                inputStream = mResources.openRawResource(id);

                int byteCount = (int) AssetFileDescriptor.UNKNOWN_LENGTH;
                int bytesRead = 0;

                // Only uncompressed resources have descriptors
                AssetFileDescriptor desc = null;
                try {
                    desc = mResources.openRawResourceFd(id);
                    byteCount = (int) desc.getLength();
                } catch (Resources.NotFoundException ex) {
                    ex.printStackTrace();
                } finally {
                    if (desc != null) {
                        desc.close();
                    }
                }

                // Check whether absolute size of stream is known
                if (byteCount == AssetFileDescriptor.UNKNOWN_LENGTH) {
                    Log.v(TAG, "Resource estimation start: " + name);

                    // available() is only guarenteed to give a rough estimate
                    // of the size of the stream, so we must loop until no more
                    // bytes are read.
                    byteCount = inputStream.available();

                    // If no estimate is even available, we give up.  We could
                    // try to read the stream, but this is probably very
                    // unlikely to happen anyway.
                    int totalBytesRead = 0;
                    if (byteCount > 0) {
                        byte[] buffer = new byte[byteCount];
                        outputStream = new ByteArrayOutputStream(byteCount);

                        do {
                            bytesRead = inputStream.read(buffer, 0, byteCount);
                            if (bytesRead > 0) {
                                outputStream.write(buffer, 0, bytesRead);
                                totalBytesRead += bytesRead;
                            }
                        } while (bytesRead != -1);

                        data = outputStream.toByteArray();

                    } else {
                        Log.v(TAG,
                              "Resource estimated size is 0; cannot read: " +
                              name);
                    }

                    Log.v(TAG, "Resource of estimated size " + totalBytesRead +
                          " Bytes read: " + name);
                } else {
                    // Size is known, so read stream in one go
                    data = new byte[byteCount];
                    bytesRead = inputStream.read(data, 0, byteCount);

                    // Ensure the stream is of the correct size, and that the
                    // end of the stream has been reached
                    assert(bytesRead == byteCount);
                    assert(inputStream.read() == -1);

                    Log.v(TAG, "Resource of known size " + bytesRead +
                          " Bytes read: " + name);
                }

            } catch (IOException ex) {
                Log.e(TAG, "Resource loading failed due to IOException: " +
                      name);
            } catch (Resources.NotFoundException ex) {
                Log.e(TAG, "Resource of incorrect type to stream: " + name);
            } finally {
                closeQuietly(inputStream);
                closeQuietly(outputStream);
            }
        } else {
            Log.e(TAG, "Resource not found: " + name);
        }

        return data;
    }

    private void closeQuietly(Closeable i) {
        if (i != null) {
            try {
                i.close();
            } catch (IOException e) {
                // ignore 'cause this function will only be called in finally block.
                e.printStackTrace();
            }
        }
    }

    /**
     * Checks whether a Resource exists.
     *
     * @param name Name of the resource to check
     * @return True if the resource exists
     */
    public boolean exists(String name) {
        boolean flag = true;

        // Get the resource's ID from its name.  We assume that the resource
        // type and package data are part of the name.
        int id = mResources.getIdentifier(name, null, null);

        if (id == 0) {
            flag = false;
        } else {
            InputStream inputStream = null;
            try {
                inputStream = mResources.openRawResource(id);
            } catch (Resources.NotFoundException ex) {
                flag = false;
            } finally {
                closeQuietly(inputStream);
            }
        }

        return flag;
    }

}
/** @} */
