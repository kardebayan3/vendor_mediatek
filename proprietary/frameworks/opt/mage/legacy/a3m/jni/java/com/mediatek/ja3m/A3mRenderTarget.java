/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.mediatek.ja3m;

import com.mediatek.j3m.*;

public class A3mRenderTarget implements RenderTarget {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected A3mRenderTarget(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(A3mRenderTarget obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        A3mJni.delete_A3mRenderTarget(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Texture2D getColourTexture() {
    long cPtr = A3mJni.A3mRenderTarget_getColourTexture(swigCPtr, this);
    return (cPtr == 0) ? null : new A3mTexture2D(cPtr, true);
  }

  public Texture2D getDepthTexture() {
    long cPtr = A3mJni.A3mRenderTarget_getDepthTexture(swigCPtr, this);
    return (cPtr == 0) ? null : new A3mTexture2D(cPtr, true);
  }

}
