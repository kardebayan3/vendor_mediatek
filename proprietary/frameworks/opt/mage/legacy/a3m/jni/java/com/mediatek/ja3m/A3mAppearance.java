/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.8
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.mediatek.ja3m;

import com.mediatek.j3m.*;

public class A3mAppearance implements Appearance {
  private long swigCPtr;
  protected boolean swigCMemOwn;

  protected A3mAppearance(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(A3mAppearance obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        A3mJni.delete_A3mAppearance(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public A3mAppearance() {
    this(A3mJni.new_A3mAppearance(), true);
  }

  public void setName(String name) {
    A3mJni.A3mAppearance_setName(swigCPtr, this, name);
  }

  public String getName() {
    return A3mJni.A3mAppearance_getName(swigCPtr, this);
  }

  public void setShaderProgram(ShaderProgram program) {
    A3mJni.A3mAppearance_setShaderProgram(swigCPtr, this, A3mShaderProgram.getCPtr((A3mShaderProgram) program), (A3mShaderProgram) program);
  }

  public ShaderProgram getShaderProgram() {
    long cPtr = A3mJni.A3mAppearance_getShaderProgram(swigCPtr, this);
    return (cPtr == 0) ? null : new A3mShaderProgram(cPtr, true);
  }

  public void setCullingMode(int cullingMode) {
    A3mJni.A3mAppearance_setCullingMode(swigCPtr, this, cullingMode);
  }

  public int getCullingMode() {
    return A3mJni.A3mAppearance_getCullingMode(swigCPtr, this);
  }

  public void setLineWidth(float width) {
    A3mJni.A3mAppearance_setLineWidth(swigCPtr, this, width);
  }

  public float getLineWidth() {
    return A3mJni.A3mAppearance_getLineWidth(swigCPtr, this);
  }

  public void setWindingOrder(int windingOrder) {
    A3mJni.A3mAppearance_setWindingOrder(swigCPtr, this, windingOrder);
  }

  public int getWindingOrder() {
    return A3mJni.A3mAppearance_getWindingOrder(swigCPtr, this);
  }

  public void setBlendColour(float r, float g, float b, float a) {
    A3mJni.A3mAppearance_setBlendColour(swigCPtr, this, r, g, b, a);
  }

  public float getBlendColourR() {
    return A3mJni.A3mAppearance_getBlendColourR(swigCPtr, this);
  }

  public float getBlendColourG() {
    return A3mJni.A3mAppearance_getBlendColourG(swigCPtr, this);
  }

  public float getBlendColourB() {
    return A3mJni.A3mAppearance_getBlendColourB(swigCPtr, this);
  }

  public float getBlendColourA() {
    return A3mJni.A3mAppearance_getBlendColourA(swigCPtr, this);
  }

  public void setBlendFactors(int src, int dst) {
    A3mJni.A3mAppearance_setBlendFactors__SWIG_0(swigCPtr, this, src, dst);
  }

  public void setBlendFactors(int srcRgb, int srcAlpha, int dstRgb, int dstAlpha) {
    A3mJni.A3mAppearance_setBlendFactors__SWIG_1(swigCPtr, this, srcRgb, srcAlpha, dstRgb, dstAlpha);
  }

  public int getBlendFactorSrcRgb() {
    return A3mJni.A3mAppearance_getBlendFactorSrcRgb(swigCPtr, this);
  }

  public int getBlendFactorSrcAlpha() {
    return A3mJni.A3mAppearance_getBlendFactorSrcAlpha(swigCPtr, this);
  }

  public int getBlendFactorDstRgb() {
    return A3mJni.A3mAppearance_getBlendFactorDstRgb(swigCPtr, this);
  }

  public int getBlendFactorDstAlpha() {
    return A3mJni.A3mAppearance_getBlendFactorDstAlpha(swigCPtr, this);
  }

  public void setBlendFunctions(int rgb, int alpha) {
    A3mJni.A3mAppearance_setBlendFunctions(swigCPtr, this, rgb, alpha);
  }

  public int getBlendFunctionRgb() {
    return A3mJni.A3mAppearance_getBlendFunctionRgb(swigCPtr, this);
  }

  public int getBlendFunctionAlpha() {
    return A3mJni.A3mAppearance_getBlendFunctionAlpha(swigCPtr, this);
  }

  public void setForceOpaque(boolean flag) {
    A3mJni.A3mAppearance_setForceOpaque(swigCPtr, this, flag);
  }

  public boolean getForceOpaque() {
    return A3mJni.A3mAppearance_getForceOpaque(swigCPtr, this);
  }

  public boolean isOpaque() {
    return A3mJni.A3mAppearance_isOpaque(swigCPtr, this);
  }

  public void setColourMask(boolean r, boolean g, boolean b, boolean a) {
    A3mJni.A3mAppearance_setColourMask(swigCPtr, this, r, g, b, a);
  }

  public boolean getColourMaskR() {
    return A3mJni.A3mAppearance_getColourMaskR(swigCPtr, this);
  }

  public boolean getColourMaskG() {
    return A3mJni.A3mAppearance_getColourMaskG(swigCPtr, this);
  }

  public boolean getColourMaskB() {
    return A3mJni.A3mAppearance_getColourMaskB(swigCPtr, this);
  }

  public boolean getColourMaskA() {
    return A3mJni.A3mAppearance_getColourMaskA(swigCPtr, this);
  }

  public void setDepthOffsetFactor(float factor) {
    A3mJni.A3mAppearance_setDepthOffsetFactor(swigCPtr, this, factor);
  }

  public float getDepthOffsetFactor() {
    return A3mJni.A3mAppearance_getDepthOffsetFactor(swigCPtr, this);
  }

  public void setDepthOffsetUnits(float units) {
    A3mJni.A3mAppearance_setDepthOffsetUnits(swigCPtr, this, units);
  }

  public float getDepthOffsetUnits() {
    return A3mJni.A3mAppearance_getDepthOffsetUnits(swigCPtr, this);
  }

  public void setDepthTestFunction(int function) {
    A3mJni.A3mAppearance_setDepthTestFunction(swigCPtr, this, function);
  }

  public int getDepthTestFunction() {
    return A3mJni.A3mAppearance_getDepthTestFunction(swigCPtr, this);
  }

  public void setDepthTestEnabled(boolean flag) {
    A3mJni.A3mAppearance_setDepthTestEnabled(swigCPtr, this, flag);
  }

  public boolean isDepthTestEnabled() {
    return A3mJni.A3mAppearance_isDepthTestEnabled(swigCPtr, this);
  }

  public void setDepthWriteEnabled(boolean flag) {
    A3mJni.A3mAppearance_setDepthWriteEnabled(swigCPtr, this, flag);
  }

  public boolean isDepthWriteEnabled() {
    return A3mJni.A3mAppearance_isDepthWriteEnabled(swigCPtr, this);
  }

  public void setScissorTestEnabled(boolean flag) {
    A3mJni.A3mAppearance_setScissorTestEnabled(swigCPtr, this, flag);
  }

  public boolean isScissorTestEnabled() {
    return A3mJni.A3mAppearance_isScissorTestEnabled(swigCPtr, this);
  }

  public void setScissorBox(int left, int bottom, int width, int height) {
    A3mJni.A3mAppearance_setScissorBox(swigCPtr, this, left, bottom, width, height);
  }

  public int getScissorBoxLeft() {
    return A3mJni.A3mAppearance_getScissorBoxLeft(swigCPtr, this);
  }

  public int getScissorBoxBottom() {
    return A3mJni.A3mAppearance_getScissorBoxBottom(swigCPtr, this);
  }

  public int getScissorBoxWidth() {
    return A3mJni.A3mAppearance_getScissorBoxWidth(swigCPtr, this);
  }

  public int getScissorBoxHeight() {
    return A3mJni.A3mAppearance_getScissorBoxHeight(swigCPtr, this);
  }

  public void setStencilFunction(int face, int function, int reference, int mask) {
    A3mJni.A3mAppearance_setStencilFunction(swigCPtr, this, face, function, reference, mask);
  }

  public int getStencilFunction(int face) {
    return A3mJni.A3mAppearance_getStencilFunction(swigCPtr, this, face);
  }

  public int getStencilReference(int face) {
    return A3mJni.A3mAppearance_getStencilReference(swigCPtr, this, face);
  }

  public int getStencilReferenceMask(int face) {
    return A3mJni.A3mAppearance_getStencilReferenceMask(swigCPtr, this, face);
  }

  public void setStencilOperations(int face, int stencilFail, int stencilPassDepthFail, int stencilPassDepthPass) {
    A3mJni.A3mAppearance_setStencilOperations(swigCPtr, this, face, stencilFail, stencilPassDepthFail, stencilPassDepthPass);
  }

  public int getStencilFail(int face) {
    return A3mJni.A3mAppearance_getStencilFail(swigCPtr, this, face);
  }

  public int getStencilPassDepthFail(int face) {
    return A3mJni.A3mAppearance_getStencilPassDepthFail(swigCPtr, this, face);
  }

  public int getStencilPassDepthPass(int face) {
    return A3mJni.A3mAppearance_getStencilPassDepthPass(swigCPtr, this, face);
  }

  public void setStencilMask(int face, int mask) {
    A3mJni.A3mAppearance_setStencilMask(swigCPtr, this, face, mask);
  }

  public int getStencilMask(int face) {
    return A3mJni.A3mAppearance_getStencilMask(swigCPtr, this, face);
  }

  public boolean propertyExists(String name) {
    return A3mJni.A3mAppearance_propertyExists(swigCPtr, this, name);
  }

  public void setBoolean(String name, boolean value, int i) {
    A3mJni.A3mAppearance_setBoolean__SWIG_0(swigCPtr, this, name, value, i);
  }

  public void setBoolean(String name, boolean value) {
    A3mJni.A3mAppearance_setBoolean__SWIG_1(swigCPtr, this, name, value);
  }

  public boolean getBoolean(String name, int i) {
    return A3mJni.A3mAppearance_getBoolean__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getBoolean(String name) {
    return A3mJni.A3mAppearance_getBoolean__SWIG_1(swigCPtr, this, name);
  }

  public void setInt(String name, int value, int i) {
    A3mJni.A3mAppearance_setInt__SWIG_0(swigCPtr, this, name, value, i);
  }

  public void setInt(String name, int value) {
    A3mJni.A3mAppearance_setInt__SWIG_1(swigCPtr, this, name, value);
  }

  public int getInt(String name, int i) {
    return A3mJni.A3mAppearance_getInt__SWIG_0(swigCPtr, this, name, i);
  }

  public int getInt(String name) {
    return A3mJni.A3mAppearance_getInt__SWIG_1(swigCPtr, this, name);
  }

  public void setFloat(String name, float value, int i) {
    A3mJni.A3mAppearance_setFloat__SWIG_0(swigCPtr, this, name, value, i);
  }

  public void setFloat(String name, float value) {
    A3mJni.A3mAppearance_setFloat__SWIG_1(swigCPtr, this, name, value);
  }

  public float getFloat(String name, int i) {
    return A3mJni.A3mAppearance_getFloat__SWIG_0(swigCPtr, this, name, i);
  }

  public float getFloat(String name) {
    return A3mJni.A3mAppearance_getFloat__SWIG_1(swigCPtr, this, name);
  }

  public void setVector2b(String name, boolean x, boolean y, int i) {
    A3mJni.A3mAppearance_setVector2b__SWIG_0(swigCPtr, this, name, x, y, i);
  }

  public void setVector2b(String name, boolean x, boolean y) {
    A3mJni.A3mAppearance_setVector2b__SWIG_1(swigCPtr, this, name, x, y);
  }

  public boolean getVector2bX(String name, int i) {
    return A3mJni.A3mAppearance_getVector2bX__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector2bX(String name) {
    return A3mJni.A3mAppearance_getVector2bX__SWIG_1(swigCPtr, this, name);
  }

  public boolean getVector2bY(String name, int i) {
    return A3mJni.A3mAppearance_getVector2bY__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector2bY(String name) {
    return A3mJni.A3mAppearance_getVector2bY__SWIG_1(swigCPtr, this, name);
  }

  public void setVector3b(String name, boolean x, boolean y, boolean z, int i) {
    A3mJni.A3mAppearance_setVector3b__SWIG_0(swigCPtr, this, name, x, y, z, i);
  }

  public void setVector3b(String name, boolean x, boolean y, boolean z) {
    A3mJni.A3mAppearance_setVector3b__SWIG_1(swigCPtr, this, name, x, y, z);
  }

  public boolean getVector3bX(String name, int i) {
    return A3mJni.A3mAppearance_getVector3bX__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector3bX(String name) {
    return A3mJni.A3mAppearance_getVector3bX__SWIG_1(swigCPtr, this, name);
  }

  public boolean getVector3bY(String name, int i) {
    return A3mJni.A3mAppearance_getVector3bY__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector3bY(String name) {
    return A3mJni.A3mAppearance_getVector3bY__SWIG_1(swigCPtr, this, name);
  }

  public boolean getVector3bZ(String name, int i) {
    return A3mJni.A3mAppearance_getVector3bZ__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector3bZ(String name) {
    return A3mJni.A3mAppearance_getVector3bZ__SWIG_1(swigCPtr, this, name);
  }

  public void setVector4b(String name, boolean x, boolean y, boolean z, boolean w, int i) {
    A3mJni.A3mAppearance_setVector4b__SWIG_0(swigCPtr, this, name, x, y, z, w, i);
  }

  public void setVector4b(String name, boolean x, boolean y, boolean z, boolean w) {
    A3mJni.A3mAppearance_setVector4b__SWIG_1(swigCPtr, this, name, x, y, z, w);
  }

  public boolean getVector4bX(String name, int i) {
    return A3mJni.A3mAppearance_getVector4bX__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector4bX(String name) {
    return A3mJni.A3mAppearance_getVector4bX__SWIG_1(swigCPtr, this, name);
  }

  public boolean getVector4bY(String name, int i) {
    return A3mJni.A3mAppearance_getVector4bY__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector4bY(String name) {
    return A3mJni.A3mAppearance_getVector4bY__SWIG_1(swigCPtr, this, name);
  }

  public boolean getVector4bZ(String name, int i) {
    return A3mJni.A3mAppearance_getVector4bZ__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector4bZ(String name) {
    return A3mJni.A3mAppearance_getVector4bZ__SWIG_1(swigCPtr, this, name);
  }

  public boolean getVector4bW(String name, int i) {
    return A3mJni.A3mAppearance_getVector4bW__SWIG_0(swigCPtr, this, name, i);
  }

  public boolean getVector4bW(String name) {
    return A3mJni.A3mAppearance_getVector4bW__SWIG_1(swigCPtr, this, name);
  }

  public void setVector2i(String name, int x, int y, int i) {
    A3mJni.A3mAppearance_setVector2i__SWIG_0(swigCPtr, this, name, x, y, i);
  }

  public void setVector2i(String name, int x, int y) {
    A3mJni.A3mAppearance_setVector2i__SWIG_1(swigCPtr, this, name, x, y);
  }

  public int getVector2iX(String name, int i) {
    return A3mJni.A3mAppearance_getVector2iX__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector2iX(String name) {
    return A3mJni.A3mAppearance_getVector2iX__SWIG_1(swigCPtr, this, name);
  }

  public int getVector2iY(String name, int i) {
    return A3mJni.A3mAppearance_getVector2iY__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector2iY(String name) {
    return A3mJni.A3mAppearance_getVector2iY__SWIG_1(swigCPtr, this, name);
  }

  public void setVector3i(String name, int x, int y, int z, int i) {
    A3mJni.A3mAppearance_setVector3i__SWIG_0(swigCPtr, this, name, x, y, z, i);
  }

  public void setVector3i(String name, int x, int y, int z) {
    A3mJni.A3mAppearance_setVector3i__SWIG_1(swigCPtr, this, name, x, y, z);
  }

  public int getVector3iX(String name, int i) {
    return A3mJni.A3mAppearance_getVector3iX__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector3iX(String name) {
    return A3mJni.A3mAppearance_getVector3iX__SWIG_1(swigCPtr, this, name);
  }

  public int getVector3iY(String name, int i) {
    return A3mJni.A3mAppearance_getVector3iY__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector3iY(String name) {
    return A3mJni.A3mAppearance_getVector3iY__SWIG_1(swigCPtr, this, name);
  }

  public int getVector3iZ(String name, int i) {
    return A3mJni.A3mAppearance_getVector3iZ__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector3iZ(String name) {
    return A3mJni.A3mAppearance_getVector3iZ__SWIG_1(swigCPtr, this, name);
  }

  public void setVector4i(String name, int x, int y, int z, int w, int i) {
    A3mJni.A3mAppearance_setVector4i__SWIG_0(swigCPtr, this, name, x, y, z, w, i);
  }

  public void setVector4i(String name, int x, int y, int z, int w) {
    A3mJni.A3mAppearance_setVector4i__SWIG_1(swigCPtr, this, name, x, y, z, w);
  }

  public int getVector4iX(String name, int i) {
    return A3mJni.A3mAppearance_getVector4iX__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector4iX(String name) {
    return A3mJni.A3mAppearance_getVector4iX__SWIG_1(swigCPtr, this, name);
  }

  public int getVector4iY(String name, int i) {
    return A3mJni.A3mAppearance_getVector4iY__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector4iY(String name) {
    return A3mJni.A3mAppearance_getVector4iY__SWIG_1(swigCPtr, this, name);
  }

  public int getVector4iZ(String name, int i) {
    return A3mJni.A3mAppearance_getVector4iZ__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector4iZ(String name) {
    return A3mJni.A3mAppearance_getVector4iZ__SWIG_1(swigCPtr, this, name);
  }

  public int getVector4iW(String name, int i) {
    return A3mJni.A3mAppearance_getVector4iW__SWIG_0(swigCPtr, this, name, i);
  }

  public int getVector4iW(String name) {
    return A3mJni.A3mAppearance_getVector4iW__SWIG_1(swigCPtr, this, name);
  }

  public void setVector2f(String name, float x, float y, int i) {
    A3mJni.A3mAppearance_setVector2f__SWIG_0(swigCPtr, this, name, x, y, i);
  }

  public void setVector2f(String name, float x, float y) {
    A3mJni.A3mAppearance_setVector2f__SWIG_1(swigCPtr, this, name, x, y);
  }

  public float getVector2fX(String name, int i) {
    return A3mJni.A3mAppearance_getVector2fX__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector2fX(String name) {
    return A3mJni.A3mAppearance_getVector2fX__SWIG_1(swigCPtr, this, name);
  }

  public float getVector2fY(String name, int i) {
    return A3mJni.A3mAppearance_getVector2fY__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector2fY(String name) {
    return A3mJni.A3mAppearance_getVector2fY__SWIG_1(swigCPtr, this, name);
  }

  public void setVector3f(String name, float x, float y, float z, int i) {
    A3mJni.A3mAppearance_setVector3f__SWIG_0(swigCPtr, this, name, x, y, z, i);
  }

  public void setVector3f(String name, float x, float y, float z) {
    A3mJni.A3mAppearance_setVector3f__SWIG_1(swigCPtr, this, name, x, y, z);
  }

  public float getVector3fX(String name, int i) {
    return A3mJni.A3mAppearance_getVector3fX__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector3fX(String name) {
    return A3mJni.A3mAppearance_getVector3fX__SWIG_1(swigCPtr, this, name);
  }

  public float getVector3fY(String name, int i) {
    return A3mJni.A3mAppearance_getVector3fY__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector3fY(String name) {
    return A3mJni.A3mAppearance_getVector3fY__SWIG_1(swigCPtr, this, name);
  }

  public float getVector3fZ(String name, int i) {
    return A3mJni.A3mAppearance_getVector3fZ__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector3fZ(String name) {
    return A3mJni.A3mAppearance_getVector3fZ__SWIG_1(swigCPtr, this, name);
  }

  public void setVector4f(String name, float x, float y, float z, float w, int i) {
    A3mJni.A3mAppearance_setVector4f__SWIG_0(swigCPtr, this, name, x, y, z, w, i);
  }

  public void setVector4f(String name, float x, float y, float z, float w) {
    A3mJni.A3mAppearance_setVector4f__SWIG_1(swigCPtr, this, name, x, y, z, w);
  }

  public float getVector4fX(String name, int i) {
    return A3mJni.A3mAppearance_getVector4fX__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector4fX(String name) {
    return A3mJni.A3mAppearance_getVector4fX__SWIG_1(swigCPtr, this, name);
  }

  public float getVector4fY(String name, int i) {
    return A3mJni.A3mAppearance_getVector4fY__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector4fY(String name) {
    return A3mJni.A3mAppearance_getVector4fY__SWIG_1(swigCPtr, this, name);
  }

  public float getVector4fZ(String name, int i) {
    return A3mJni.A3mAppearance_getVector4fZ__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector4fZ(String name) {
    return A3mJni.A3mAppearance_getVector4fZ__SWIG_1(swigCPtr, this, name);
  }

  public float getVector4fW(String name, int i) {
    return A3mJni.A3mAppearance_getVector4fW__SWIG_0(swigCPtr, this, name, i);
  }

  public float getVector4fW(String name) {
    return A3mJni.A3mAppearance_getVector4fW__SWIG_1(swigCPtr, this, name);
  }

  public void setMatrix4f(String name, float[] mtx, int i) {
    A3mJni.A3mAppearance_setMatrix4f__SWIG_0(swigCPtr, this, name, mtx, i);
  }

  public void setMatrix4f(String name, float[] mtx) {
    A3mJni.A3mAppearance_setMatrix4f__SWIG_1(swigCPtr, this, name, mtx);
  }

  public void setTexture2D(String name, Texture2D value, int i) {
    A3mJni.A3mAppearance_setTexture2D__SWIG_0(swigCPtr, this, name, A3mTexture2D.getCPtr((A3mTexture2D) value), (A3mTexture2D) value, i);
  }

  public void setTexture2D(String name, Texture2D value) {
    A3mJni.A3mAppearance_setTexture2D__SWIG_1(swigCPtr, this, name, A3mTexture2D.getCPtr((A3mTexture2D) value), (A3mTexture2D) value);
  }

  public Texture2D getTexture2D(String name, int i) {
    long cPtr = A3mJni.A3mAppearance_getTexture2D__SWIG_0(swigCPtr, this, name, i);
    return (cPtr == 0) ? null : new A3mTexture2D(cPtr, true);
  }

  public Texture2D getTexture2D(String name) {
    long cPtr = A3mJni.A3mAppearance_getTexture2D__SWIG_1(swigCPtr, this, name);
    return (cPtr == 0) ? null : new A3mTexture2D(cPtr, true);
  }

  public void setTextureCube(String name, TextureCube value, int i) {
    A3mJni.A3mAppearance_setTextureCube__SWIG_0(swigCPtr, this, name, A3mTextureCube.getCPtr((A3mTextureCube) value), (A3mTextureCube) value, i);
  }

  public void setTextureCube(String name, TextureCube value) {
    A3mJni.A3mAppearance_setTextureCube__SWIG_1(swigCPtr, this, name, A3mTextureCube.getCPtr((A3mTextureCube) value), (A3mTextureCube) value);
  }

  public TextureCube getTextureCube(String name, int i) {
    long cPtr = A3mJni.A3mAppearance_getTextureCube__SWIG_0(swigCPtr, this, name, i);
    return (cPtr == 0) ? null : new A3mTextureCube(cPtr, true);
  }

  public TextureCube getTextureCube(String name) {
    long cPtr = A3mJni.A3mAppearance_getTextureCube__SWIG_1(swigCPtr, this, name);
    return (cPtr == 0) ? null : new A3mTextureCube(cPtr, true);
  }

}
