echo off

rem
rem  -n means no backup file is created. It's pointless anyway
rem     as the 2nd run deletes the backup you needed.
rem
rem  -r means recurse down subfolders from here
rem


rem
rem   Core A3M engine folders - MTK source code folders only.
rem

astyle --options=a3m_cpp.txt -r -n ..\..\engine\render\*.cpp
astyle --options=a3m_cpp.txt -r -n ..\..\engine\render\*.h

astyle --options=a3m_cpp.txt -r -n ..\..\engine\facility\*.cpp
astyle --options=a3m_cpp.txt -r -n ..\..\engine\facility\*.h

astyle --options=a3m_cpp.txt -r -n ..\..\engine\common\*.cpp
astyle --options=a3m_cpp.txt -r -n ..\..\engine\common\*.h

astyle --options=a3m_cpp.txt -r -n ..\..\engine\maths\*.cpp
astyle --options=a3m_cpp.txt -r -n ..\..\engine\maths\*.h

astyle --options=a3m_cpp.txt -r -n ..\..\engine\collision\*.cpp
astyle --options=a3m_cpp.txt -r -n ..\..\engine\collision\*.h

astyle --options=a3m_cpp.txt -r -n ..\..\engine\pss\*.cpp
astyle --options=a3m_cpp.txt -r -n ..\..\engine\pss\*.h


rem
rem   J3M Folder
rem

astyle --options=a3m_java.txt -r -n ..\..\j3m\*.java


rem
rem   JNI/JNI
rem   Here we list individual files to avoid processing the machine-
rem   generated ones.
rem

astyle --options=a3m_cpp.txt -r -n ..\..\jni\jni\common.*
astyle --options=a3m_cpp.txt -r -n ..\..\jni\jni\nodeutility.*
astyle --options=a3m_cpp.txt -r -n ..\..\jni\jni\renderflags.*
astyle --options=a3m_cpp.txt -r -n ..\..\jni\jni\resourcestream.*


rem
rem   A3M Autotest Folder
rem

rem
rem   Autotest Java Source
rem   Here we list individual files to avoid processing the machine-
rem   generated ones.
rem

astyle --options=a3m_java.txt -n ..\..\autotest\java\com\mediatek\a3m\autotest\Main.java
astyle --options=a3m_java.txt -n ..\..\autotest\java\com\mediatek\a3m\autotest\SurfaceView.java
astyle --options=a3m_java.txt -n ..\..\autotest\java\com\mediatek\a3m\autotest\Test.java


rem
rem   Autotest C++ Source
rem

astyle --options=a3m_cpp.txt -n ..\..\autotest\src\*.cpp
astyle --options=a3m_cpp.txt -n ..\..\autotest\src\*.h
