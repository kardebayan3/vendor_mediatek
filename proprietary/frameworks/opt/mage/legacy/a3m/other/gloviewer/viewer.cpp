/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
#include "viewer.h"               /* This class declaration   */
#include "scenedialog.h"
#include "statsdialog.h"
#include "renderflags.h"          /* For node visibility flag */
#include <windows.h>              /* Gdi identifies           */
#include <a3m/background.h>       /* Background class         */
#include <a3m/renderdevice.h>     /* RenderDevice::clear()    */
#include <a3m/camera.h>           /* Camera class             */
#include <a3m/info.h>             /* For getBuildInfo()       */
#include <a3m/light.h>            /* To add default light     */
#include <a3m/renderblock.h>      /* For RenderBlock          */
#include <a3m/solid.h>            /* Solid class              */
#include <a3m/scenenodevisitor.h> /* SceneNodeVisitor class   */
#include <a3m/sceneutility.h>     /* For visitScene()         */

using namespace a3m;

namespace
{
  // Proportional to the speed with which the camera moves at any given zoom
  // level.  The more zoomed-in a camera is, the slower it will move.
  static const float CAMERA_MOVE_SPEED = 0.2f;

  // Size of axes indicator in pixels
  static float const AXES_SIZE = 40.0f;

  static const float defaultLightAmbientLevel = 0.1f;
  static const Vector3f defaultLightPosition( 10000.f, 10000.f, 10000.f );

  // Class to set Solid ShaderPrograms
  class ShaderProgramSetter : public SceneNodeVisitor
  {
  public:
    ShaderProgramSetter( ShaderProgram::Ptr program ) : m_program( program ) {}

    void visit( Solid *solid )
    {
      solid->getAppearance()->setShaderProgram( m_program );
    }
  private:
    ShaderProgram::Ptr m_program;
  };

  // Class to set Solid Appearances
  class AppearanceSetter : public SceneNodeVisitor
  {
  public:
    AppearanceSetter( A3M_CHAR8 const* appearance, a3m::AssetCachePool& pool )
        : m_appearance( appearance ), m_pool ( pool ) {}

    void visit( Solid *solid )
    {
        solid->getAppearance()->apply( m_pool, m_appearance );
    }
  private:
    A3M_CHAR8 const* m_appearance;
    a3m::AssetCachePool& m_pool;
  };

  // Class to set find cameras in scene
  class CameraCollector : public SceneNodeVisitor
  {
  public:
    CameraCollector( std::vector< Camera * > &cameras ) : m_cameras( cameras ) {}

    void visit( Camera *camera )
    {
      m_cameras.push_back( camera );
    }
  private:
    std::vector< Camera * > &m_cameras;
  };

  // Class to count lights in a scene
  class LightCounter : public SceneNodeVisitor
  {
  public:
    LightCounter() : m_count( 0 ) {}

    void visit( Light * ) { ++m_count; }

    A3M_UINT32 getCount() const { return m_count; }

  private:
    A3M_UINT32 m_count;
  };

  // Find the node specified the next token in stream.
  // The special name "ROOT" specifies the node passed in root, and "LAST"
  // specifies the node passed in last.
  SceneNode *findNode( std::istream &stream, SceneNode *root, SceneNode *last )
  {
    std::string name;
    stream >> name;
    if( name == "ROOT" )
    {
      return root;
    }
    else if( name == "LAST" )
    {
      return last;
    }
    else
    {
      SceneNode *ret = 0;
      if( last )
      {
        ret = last->find( name.c_str() ).get();
      }
      if( !ret && root )
      {
        ret = root->find( name.c_str() ).get();
      }
      return ret;
    }

  }
}

Viewer::Viewer( std::string const& cwd, std::string const& filename,
                Window::Ptr const& window ) :
  m_autoClippingPlanes( A3M_TRUE ),
  m_filename( filename ),
  m_scene( new SceneNode ),
  m_overlay( new SceneNode ),
  m_pool( new AssetCachePool ),
  m_cameraHRotate( new SceneNode ),
  m_cameraVRotate( new SceneNode ),
  m_camera( new Camera ),
  m_overlayCamera( new Camera ),
  m_hRotation(),
  m_vRotation(),
  m_focus( 0.f, 0.f, 0.f ),
  m_distance( 100.f ),
  m_fov(Anglef::DEGREES, 45.0f),
  m_near( .1f ),
  m_far( 1000.f ),
  m_lastX( 0 ),
  m_lastY( 0 ),
  m_ctrlDown( A3M_FALSE ),
  m_shiftDown( A3M_FALSE ),
  m_consoleVisible( A3M_FALSE ),
  m_animationPlaying( A3M_TRUE ),
  m_background( .2f, .2f, .2f, 1.f ),
  m_currentCamera( 0 ),
  m_window( window )
{
  m_cameras.push_back( m_camera.get() );
  m_scene->setName( "Root" );
  m_sceneDialog.reset( new SceneDialog( HWND( m_window->getId() ) ) );
  m_sceneDialog->visible( A3M_FALSE );
  m_statsDialog.reset( new StatsDialog( HWND( m_window->getId() ) ) );
  m_statsDialog->visible( A3M_FALSE );
  m_animDialog.reset( new AnimationDialog( HWND( m_window->getId() ) ) );
  m_animDialog->visible( A3M_FALSE );

  // Add paths for both development and release versions
  registerSource( *m_pool, (cwd + "/../assets").c_str() );
  registerSource( *m_pool, (cwd + "/assets").c_str() );

  // Set up rendering blocks
  SimpleRenderer::Ptr renderer(
    new SimpleRenderer( RenderContext::Ptr( new RenderContext() ), m_pool ) );

  m_sceneBlock.reset( new RenderBlock( renderer, m_scene, m_camera ) );
  m_sceneBlock->setBackgroundColour( m_background );
  m_sceneBlock->setRenderFlags( VISIBLE, VISIBLE );
  m_renderBlockGroup.addBlock( m_sceneBlock );

  m_overlayCamera->setProjectionType( Camera::ORTHOGRAPHIC );
  m_overlayCamera->setFar( 200.0f );
  m_overlayCamera->setNear( -200.0f );

  m_overlayBlock.reset( new RenderBlock( renderer, m_overlay, m_overlayCamera ) );
  m_overlayBlock->setColourClear( A3M_FALSE );
  m_overlayBlock->setRenderFlags( VISIBLE, VISIBLE );
  m_renderBlockGroup.addBlock( m_overlayBlock );

  // Construct overlay
  Glo::Ptr axesGlo = loadGloFile( *m_pool, SceneNode::Ptr(), "gloviewer#axes.glo" );
  m_axes = axesGlo->getSceneNode();
  m_axes->setParent( m_overlay );
  m_axes->setScale( Vector3f( AXES_SIZE, AXES_SIZE, AXES_SIZE ) );

  // Align overlay contents with window
  onResize( m_window->getWidth(), m_window->getHeight() );

  // Output build info to console
  A3M_LOG_INFO("Build Information:");
  A3M_LOG_INFO(getBuildInfo());
}

Viewer::~Viewer()
{
  m_pool->release();
}

A3M_BOOL Viewer::init()
{
  initConfig();
  initCameras();
  // if there is only one camera found from glo file, use it. For multiple
  // glo cameras, still use default non-glo camera.
  if ( m_cameras.size() == 2 )
  {
    m_currentCamera = 1;
  }
  initLights();

  // Add the grid floor to the scene
  Glo::Ptr gridFloorGlo = loadGloFile( *m_pool, SceneNode::Ptr(),
                                       "gloviewer#gridfloor.glo" );
  m_gridfloor = gridFloorGlo->getSceneNode();
  m_gridfloor->setParent( m_scene );

  m_sceneDialog->init( m_scene.get() );

  m_timer.reset();

  return A3M_TRUE;
}

// Read config file and load glo files etc.
void Viewer::initConfig()
{
  std::ifstream stream( m_filename.c_str() );
  std::string line;
  std::string pathName;

  std::size_t lastIndex = m_filename.find_last_of("/\\");
  if (lastIndex == std::string::npos) //not find back slash, register current directory.
  {
    pathName = ".";
  }
  else
  {
    pathName = m_filename.substr(0, lastIndex );
  }

  // Register path into asset pool.
  registerSource( *m_pool, pathName.c_str() );

  pathName.append("\\images", 7);
  // Register images folder under this directory for resources
  registerSource( *m_pool, pathName.c_str() );

  // Keep track of last GLO file loaded. Subsequent lines in the file might
  // Refer to it (using "LAST").
  Glo::Ptr lastGlo;

  //find the stream extension
  std::string extension;
  extension = m_filename.substr(m_filename.length() - 4, 4);
  if (extension == ".glo")
  {
    if (lastIndex == std::string::npos)
    {
      lastGlo = loadGloFile( *m_pool, m_scene, m_filename.c_str() );
    }
    else
    {
      lastGlo = loadGloFile( *m_pool, m_scene,
                             m_filename.substr( lastIndex + 1 ).c_str() );
    }

    if (lastGlo)
    {
      m_glos.push_back( lastGlo );
      if( lastGlo->getSceneNode() )
      {
        lastGlo->getSceneNode()->setParent( m_scene );
      }
    }
    else
    {
      A3M_LOG_ERROR("Failed to load .glo file %s.", m_filename.c_str());
    }
  }
  else if (extension == ".glv")
  {
    while( getline( stream, line ) )
    {
      if( line.substr( 0, 5 ) == "path " )
      {
        registerSource( *m_pool, line.substr( 5 ).c_str() );
      }
      else if( line.substr( 0, 4 ) == "glo " )
      {
        lastGlo = loadGloFile( *m_pool, m_scene, line.substr( 4 ).c_str() );
        m_glos.push_back( lastGlo );
        if( lastGlo->getSceneNode() )
        {
          lastGlo->getSceneNode()->setParent( m_scene );
          lastGlo->getSceneNode()->setName( line.substr( 4 ).c_str() );
        }
      }
      else if( line.substr( 0, 4 ) == "seq " )
      {
        m_gloSeqs.push_back( GloList() );
        std::istringstream strStream( line.substr( 4 ).c_str() );
        while( strStream )
        {
          std::string gloName;
          strStream >> gloName;
          m_gloSeqs.back().push_back(
            loadGloFile( *m_pool, m_scene, gloName.c_str() ) );
          if( m_gloSeqs.back().back()->getAnimation() )
          {
            m_gloSeqs.back().back()->getAnimation()->setEnabled( A3M_FALSE );
          }
          if( m_gloSeqs.back().back()->getSceneNode() )
          {
            m_gloSeqs.back().back()->getSceneNode()->setParent( m_scene );
          }
        }
      }
      else if( line.substr( 0, 8 ) == "program " )
      {
        std::istringstream strStream( line.substr( 8 ).c_str() );
        SceneNode *node = findNode( strStream, m_scene.get(), lastGlo->getSceneNode().get() );
        if( node )
        {
          std::string programName;
          strStream >> programName;
          ShaderProgram::Ptr program = m_pool->shaderProgramCache()->get(
            programName.c_str() );

          if( program )
          {
            ShaderProgramSetter setter( program );
            visitScene( setter, *node );
          }
        }
      }
      else if( line.substr( 0, 11 ) == "appearance " )
      {
        std::istringstream strStream( line.substr( 11 ).c_str() );
        SceneNode *node = findNode( strStream, m_scene.get(), lastGlo->getSceneNode().get() );
        if( node )
        {
          std::string appearanceName;
          strStream >> appearanceName;

          AppearanceSetter setter( appearanceName.c_str(), *m_pool );
          visitScene( setter, *node );
        }
      }
      else if( line.substr( 0, 16 ) == "camera_distance " )
      {
        std::istringstream strStream( line.substr( 16 ) );

        strStream >> m_distance;

        m_camera->setPosition( Vector3f( 0.f, 0.f, m_distance) );
      }
      else if( line.substr( 0, 11 ) == "camera_fov " )
      {
        std::istringstream strStream( line.substr( 11 ) );
        A3M_FLOAT fov;
        strStream >> fov;
        m_fov = degrees( fov );
        m_camera->setFov ( m_fov );
      }
      else if( line.substr( 0, 12 ) == "camera_near " )
      {
        std::istringstream strStream( line.substr( 12 ) );
        strStream >> m_near;
        m_camera->setNear ( m_near);
        m_autoClippingPlanes = A3M_FALSE;
      }
      else if( line.substr( 0, 11 ) == "camera_far " )
      {
        std::istringstream strStream( line.substr( 11 ) );
        strStream >> m_far;
        m_camera->setFar ( m_far );
        m_autoClippingPlanes = A3M_FALSE;
      }
      else if( line.substr( 0, 11 ) == "background " )
      {
        std::istringstream strStream( line.substr( 11 ) );

        strStream >> m_background.r >> m_background.g >> m_background.b;
        m_sceneBlock->setBackgroundColour( m_background );
      }
    }
  }

  for( A3M_UINT32 i = 0; i < m_glos.size(); ++i )
  {
    // Control last added animation
    m_animDialog->setAnimation( lastGlo->getAnimation() );

    m_statsDialog->add( *m_glos[i] );
  }
}

// Find cameras in scene
void Viewer::initCameras()
{
  CameraCollector collector( m_cameras );
  visitScene( collector, *m_scene );

  // Connect camera nodes.
  m_cameraHRotate->setParent( m_scene );
  m_cameraVRotate->setParent( m_cameraHRotate );
  m_camera->setParent( m_cameraVRotate );

  m_camera->setPosition( Vector3f( 0.f, 0.f, m_distance ) );
  m_camera->setFov ( m_fov );
  updateCameraClippingPlanes();
}

// Add light if there are none in the scene
void Viewer::initLights()
{
  LightCounter counter;
  visitScene( counter, *m_scene );

  if( counter.getCount() == 0 )
  {
    //Scene has no lights, so add a default light
    Light::Ptr light( new Light );
    light->setLightType( Light::LIGHTTYPE_OMNI );
    light->setAmbientLevel( defaultLightAmbientLevel );
    light->setIsAttenuated( A3M_FALSE );
    light->setPosition( defaultLightPosition );
    light->setParent( m_scene );
  }
}

void Viewer::update()
{
  A3M_FLOAT dt = m_timer.elapsedSeconds();
  m_timer.reset();

  m_cameraHRotate->setRotation( Quaternionf( Vector3f( 0.f, 1.f, 0.f ),
                                            m_hRotation ) );
  m_cameraVRotate->setRotation( Quaternionf( Vector3f( 1.f, 0.f, 0.f ),
                                            m_vRotation ) );

  m_animDialog->update();
  updateAxesIndicator();
  m_renderBlockGroup.update( dt );

  for( A3M_UINT32 i = 0; i != m_glos.size(); ++i )
  {
    if( m_glos[i]->getAnimation() )
    {
      m_glos[i]->getAnimation()->update( dt );
    }
  }
  for( A3M_UINT32 seq = 0; seq < m_gloSeqs.size(); ++seq )
  {
    for( A3M_UINT32 i = 0; i < m_gloSeqs[seq].size(); ++i )
    {
      if( m_gloSeqs[seq][i]->getAnimation() )
      {
        m_gloSeqs[seq][i]->getAnimation()->update( dt );
      }
    }
  }
  m_sceneDialog->update();
}

void Viewer::draw()
{
  m_sceneBlock->setCamera( createSharedPtr( m_cameras[ m_currentCamera ] ) );
  m_renderBlockGroup.render();
}

void Viewer::touchMove( A3M_INT32 x, A3M_INT32 y )
{
  if( m_currentCamera == 0 ) // Only allow control of default camera
  {
    if( m_ctrlDown && m_shiftDown ) // Move focus up/down forwards/backwards
    {
      Anglef hRotation = -radians(( x - m_lastX ) * 0.005f);
      Anglef vRotation = -radians(( y - m_lastY ) * 0.005f);
      Vector4f pos = m_camera->getWorldTransform().t;
      m_focus += 0.001f * m_distance *
        ( -Vector3f( m_camera->getWorldTransform().i ) * ( x - m_lastX ) +
          -Vector3f( m_camera->getWorldTransform().k ) * ( y - m_lastY ) );
      m_cameraHRotate->setPosition( m_focus );
    }
    else if( m_ctrlDown ) // free-look (camera position stays the same)
    {
      Anglef hRotation = -radians(( x - m_lastX ) * 0.005f);
      Anglef vRotation = -radians(( y - m_lastY ) * 0.005f);
      Vector4f pos = m_camera->getWorldTransform().t;
      m_focus = Vector3f( translation( pos ) *
                rotation( Vector4f( 0.f, 1.f, 0.f, 0.f ), hRotation ) *
                rotation( m_camera->getWorldTransform().i, vRotation ) *
                translation( Vector4f( -Vector3f( pos ), 1.f ) ) *
                Vector4f( m_focus, 1.f ) );
      m_cameraHRotate->setPosition( m_focus );
      m_hRotation += hRotation;
      m_vRotation += vRotation;
    }
    else if( m_shiftDown ) // Move focus up/down left/right
    {
      Anglef hRotation = -radians(( x - m_lastX ) * 0.005f);
      Anglef vRotation = -radians(( y - m_lastY ) * 0.005f);
      Vector4f pos = m_camera->getWorldTransform().t;
      m_focus += 0.001f * m_distance *
        ( -Vector3f( m_camera->getWorldTransform().i ) * ( x - m_lastX ) +
          Vector3f( m_camera->getWorldTransform().j ) * ( y - m_lastY ) );
      m_cameraHRotate->setPosition( m_focus );
    }
    else // Orbit camera around focus point
    {
      m_hRotation -= radians(( x - m_lastX ) * 0.01f);
      m_vRotation -= radians(( y - m_lastY ) * 0.01f);
    }
  }
  m_lastX = x;
  m_lastY = y;
}

void Viewer::touchDown( A3M_INT32 x, A3M_INT32 y )
{
  m_lastX = x;
  m_lastY = y;
}

void Viewer::touchUp( A3M_INT32 x, A3M_INT32 y )
{
}

void Viewer::keyDown( KeyCode key )
{
  HWND hwnd = GetConsoleWindow();

  A3M_CHAR8 keyString[] =
    "Space:\t\tstop/start animation\n"
    "Key S:\t\tshow scene graph\n"
    "Key T:\t\tshow scene statistics\n"
    "Key A:\t\tshow animation controls\n"
    "Key C:\t\tselect camera in sequence\n"
    "Key L:\t\tswitch window orientation\n"
    "Key O:\t\ttoggle overlay visibility\n"
    "Key D:\t\tshow debug terminal\n"
    "Page Up:\t\tmove camera focus up\n"
    "Page Down:\tmove camera focus down\n"
    "Arrow Up:\tmove camera focus backwards\n"
    "Arrow Down:\tmove camera focus forwards\n"
    "Arrow Left:\tmove camera focus left\n"
    "Arrow Right:\tmove camera focus right\n"
    "Home:\t\tdecrease camera distance\n"
    "End:\t\tincrease camera distance\n"
    "Mouse scroll\tincrease/decrease camera distance:\n"
    "Left Mouse\torbit camera around focus\n"
    "  (with shift)\tmove camera focus up/down and left/right\n"
    "  (with shift & ctrl)\tmove camera focus up/down and forward/backward\n"
    "  (with ctrl)\trotate camera in place\n";

  switch( key )
  {
  case KEY_CONTROL:
    m_ctrlDown = A3M_TRUE;
    break;

  case KEY_SHIFT:
    m_shiftDown = A3M_TRUE;
    break;

  case KEY_F1:
    MessageBoxA((HWND)m_window->getId(), keyString, "About keys", MB_ICONINFORMATION);
    break;

  case KEY_S:
    m_sceneDialog->visible( !m_sceneDialog->visible() );
    break;

  case KEY_D:
    m_consoleVisible = !( m_consoleVisible );
    ShowWindow(hwnd, m_consoleVisible);
    break;

  case KEY_T:
    m_statsDialog->visible( !m_statsDialog->visible() );
    break;

  case KEY_A:
    m_animDialog->visible( !m_animDialog->visible() );
    break;

  case KEY_O:
    // Toggle whether the overlay view and grid are visible
    setFlags( *m_overlay, VISIBLE, !getFlags( *m_overlay, VISIBLE ) );
    setFlags( *m_gridfloor, VISIBLE, !getFlags( *m_gridfloor, VISIBLE ) );
    break;

  case KEY_C:
    ++m_currentCamera;
    if( m_currentCamera >= m_cameras.size() )
    {
      m_currentCamera = 0;
    }
    break;

  case KEY_L:
    swapOrientation(*m_window);
    break;
  }

  doAnimKeys( key );
  if( m_currentCamera == 0 ) // Only allow control of default camera
  {
    doCameraKeys( key );
  }
}

void Viewer::keyUp( KeyCode key )
{
  if( key == KEY_CONTROL )
  {
    m_ctrlDown = A3M_FALSE;
  }
  else if( key == KEY_SHIFT )
  {
    m_shiftDown = A3M_FALSE;
  }
}

void Viewer::doAnimKeys( KeyCode key )
{
  if( key == KEY_SPACE )
  {
    A3M_BOOL allFinished = A3M_TRUE;

    // If playing, pause all the animations
    if( m_animationPlaying )
    {
      for( A3M_UINT32 i = 0; i != m_glos.size(); ++i )
      {
        AnimationController::Ptr animation = m_glos[i]->getAnimation();

        if( animation )
        {
          if( !isFinished( *animation ) )
          {
            allFinished = A3M_FALSE;
          }

          m_glos[i]->getAnimation()->setPaused( A3M_TRUE );
        }
      }

      if( allFinished )
      {
        for( A3M_UINT32 i = 0; i != m_glos.size(); ++i )
        {
          if( m_glos[i]->getAnimation() )
          {
            rewind( *m_glos[i]->getAnimation() );
          }
        }

        m_animationPlaying = A3M_FALSE;
      }
    }

    if( !m_animationPlaying )
    {
      for( A3M_UINT32 i = 0; i != m_glos.size(); ++i )
      {
        if( m_glos[i]->getAnimation() )
        {
          play( *m_glos[i]->getAnimation() );
        }
      }
      m_timer.reset();
    }
    m_animationPlaying = !m_animationPlaying;
  }
  else if( (key >= KEY_1) && (key <= KEY_9) )
  {
    A3M_INT32 index = key - KEY_1;
    for( A3M_UINT32 seq = 0; seq < m_gloSeqs.size(); ++seq )
    {
      for( A3M_UINT32 i = 0; i < m_gloSeqs[seq].size(); ++i )
      {
        if( m_gloSeqs[seq][i]->getAnimation() )
        {
          if( index == i )
          {
            m_gloSeqs[seq][i]->getAnimation()->setProgress( 0.0f );
            m_gloSeqs[seq][i]->getAnimation()->setEnabled( A3M_TRUE );
          }
          else
          {
            m_gloSeqs[seq][i]->getAnimation()->setEnabled( A3M_FALSE );
          }
        }
      }
    }
  }
}

// The camera is attached to a node which is attached to a focal point, and is
// always pointing towards this focal point.  The user can move the focal point
// around, adjust the distance from the camera to the focal point, and rotate
// the camera node around the focal point.
void Viewer::doCameraKeys( KeyCode key )
{
  // Calculate the size of step to take when moving the camera around the
  // scene.  The smaller the distance between camera and focal point, the
  // slower the camera will move.
  float stepSize = m_distance * CAMERA_MOVE_SPEED;

  if( key == KEY_LEFT )
  {
    m_focus -= Vector3f( m_cameraHRotate->getWorldTransform().i ) * stepSize;
    m_cameraHRotate->setPosition( m_focus );
  }
  else if( key == KEY_RIGHT )
  {
    m_focus += Vector3f( m_cameraHRotate->getWorldTransform().i ) * stepSize;
    m_cameraHRotate->setPosition( m_focus );
  }
  else if( key == KEY_UP )
  {
    m_focus -= Vector3f( m_cameraHRotate->getWorldTransform().k ) * stepSize;
    m_cameraHRotate->setPosition( m_focus );
  }
  else if( key == KEY_DOWN )
  {
    m_focus += Vector3f( m_cameraHRotate->getWorldTransform().k ) * stepSize;
    m_cameraHRotate->setPosition( m_focus );
  }
  else if( key == KEY_PAGE_UP )
  {
    m_focus += Vector3f( m_cameraHRotate->getWorldTransform().j ) * stepSize;
    m_cameraHRotate->setPosition( m_focus );
  }
  else if( key == KEY_PAGE_DOWN )
  {
    m_focus -= Vector3f( m_cameraHRotate->getWorldTransform().j ) * stepSize;
    m_cameraHRotate->setPosition( m_focus );
  }
  else if( key == KEY_HOME )
  {
    m_distance *= 0.9f;
    m_camera->setPosition( Vector3f( 0.f, 0.f, m_distance ) );
    updateCameraClippingPlanes();
  }
  else if( key == KEY_END )
  {
    m_distance *= 1.1f;
    m_camera->setPosition( Vector3f( 0.f, 0.f, m_distance ) );
    updateCameraClippingPlanes();
  }
}

void Viewer::mouseWheel( A3M_INT32 delta )
{
  if( m_currentCamera == 0 ) // Only allow control of default camera
  {
    if( delta > 0 )
    {
      m_distance *= 0.9f;
    }
    else
    {
      m_distance *= 1.1f;
    }

    m_camera->setPosition( Vector3f( 0.f, 0.f, m_distance ) );
    updateCameraClippingPlanes();
  }
}

void Viewer::onResize( A3M_INT32 width, A3M_INT32 height )
{
  m_sceneBlock->setViewport(0, 0, width, height);
  m_overlayBlock->setViewport(0, 0, width, height);

  m_overlayCamera->setWidth( static_cast< A3M_FLOAT >( width ) );

  A3M_FLOAT const AXES_PADDING = 16.0f;
  m_axes->setPosition( Vector3f(
    AXES_SIZE + AXES_PADDING - width / 2.0f,
    AXES_SIZE + AXES_PADDING - height / 2.0f,
    0.0f ) );
}

void Viewer::updateCameraClippingPlanes()
{
  if ( m_autoClippingPlanes )
  {
    // Always render 100 times the distance to the focal point, and use a
    // near:far ratio of 1:1000.
    m_camera->setFar( m_distance * 100.0f );
    m_camera->setNear( m_camera->getFar() / 1000.0f );
  }
}

void Viewer::updateAxesIndicator()
{
  Matrix4f const& transform = m_cameras[ m_currentCamera ]->getWorldTransform();
  m_axes->setRotation( inverse( toQuaternion( transform ) ) );

  point( *m_axes->find("X"), -Vector3f::Z_AXIS );
  point( *m_axes->find("Y"), -Vector3f::Z_AXIS );
  point( *m_axes->find("Z"), -Vector3f::Z_AXIS );
}
