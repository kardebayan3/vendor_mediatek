/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/**
 * Viewer Dialog
 *
 */

#pragma once
#ifndef A3M_VIEWERDIALOG_H
#define A3M_VIEWERDIALOG_H
#include <a3m/base_types.h>  /* A3M_INT32 etc.             */
#define NOMINMAX             /* Don't want windows.h defining min & max */
#include <windows.h>         /* HWND etc.                  */
#include <commctrl.h>        /* Common control identifiers */
#include <a3m/vector2.h>     /* Vector types               */
#include <a3m/colour.h>      /* Colour4f type              */
/*
 * A base class for viewer dialogs
 */
class ViewerDialog
{
public:
  ViewerDialog( HWND parentWindowHandle, A3M_UINT32 resource );

  // Overridden by subclasses to react to control notifications etc.
  virtual void onCommand( A3M_INT32 control, A3M_INT32 event, A3M_INT32 lparam ) {}
  virtual void onNotify( A3M_INT32 control, A3M_INT32 lparam ) {}
  virtual void onHScroll( A3M_UINT32 pos ) {}
  virtual void onHScrollEnd() {}
  virtual void onDrawCtl( A3M_INT32 control, HDC dc, RECT const &rect );

  // Returns true if the dialog is visible
  A3M_BOOL visible() const;
  // Show or hide dialog.
  void visible( A3M_BOOL makeVisible );

  // Return the window handle for this dialog.
  HWND hwnd() { return m_wnd; }
  // Return handle for a control in this dialog.
  HWND hwnd( A3M_UINT32 control ) const;
  // Return control corresponding to a handle
  A3M_UINT32 control( HWND hwnd );

  // Sets whether a checkbox is checked
  void setChecked( A3M_UINT32 control, A3M_BOOL checked ) const;
  // Returns whether a checkbox is checked
  A3M_BOOL isChecked( A3M_UINT32 control ) const;

  // Set the visibility of a control on the dialog
  void setCtlVisible( A3M_UINT32 control, A3M_BOOL visible );
  // Set whether a control is enabled (accepts user input)
  void setCtlEnabled( A3M_UINT32 control, A3M_BOOL visible );

  // Scroll bar functions
  void setScrollPos( A3M_UINT32 control, A3M_INT32 position );
  void setScrollRange( A3M_UINT32 control, A3M_INT32 min, A3M_INT32 max );
  void setScrollPageSize( A3M_UINT32 control, A3M_INT32 size );

  // Convenience functions for setting the text of edit controls
  void setEditText( A3M_UINT32 control, A3M_CHAR8 const *text );
  void setEditText( A3M_UINT32 control, A3M_FLOAT v );
  void setEditText( A3M_UINT32 control, A3M_INT32 value );
  void setEditText( A3M_UINT32 control1, A3M_UINT32 control2, A3M_UINT32 control3,
                    a3m::Vector3f const &v );
  void setEditText( A3M_UINT32 control1, A3M_UINT32 control2,
                    A3M_UINT32 control3, A3M_UINT32 control4,
                    a3m::Vector4f const &v );
  void setEditText( A3M_UINT32 control1, A3M_UINT32 control2, A3M_UINT32 control3,
                    a3m::Colour4f const &c );
  void setEditText( A3M_UINT32 control1, A3M_UINT32 control2,
                    A3M_UINT32 control3, A3M_UINT32 control4,
                    a3m::Colour4f const &c );
  void setEditColour( A3M_UINT32 control, a3m::Colour4f const &c );
  void setEditColour( A3M_UINT32 control, a3m::Vector4f const &c );

private:
  HWND m_wnd; // The window handle of this dialog
};

#endif // A3M_VIEWERDIALOG_H
