/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
#include "scenedialog.h"        /* Class header */
#include "renderflags.h"        /* For getting/setting node visibility     */
#include <a3m/scenenode.h>      /* SceneNode class                         */
#include <a3m/sceneutility.h>   /* Visiting scene nodes                    */
#include <a3m/quaternion.h>     /* For Quaternion                          */
#include <a3m/solid.h>          /* So that Solid can be cast to SceneNode  */
#include <a3m/light.h>          /* So that Light can be cast to SceneNode  */
#include <a3m/camera.h>         /* So that Camera can be cast to SceneNode */

#include "resource.h"  /* Dialog resource identifier */

namespace
{
  /*
   * class used to fill a Windows tree-view control with a scene graph
   */
  class TreeViewPopulator : public a3m::SceneNodeVisitor
  {
  public:
    TreeViewPopulator( HWND wnd ) : m_wnd( wnd ) {m_parents.push_back( TVI_ROOT);}

    void visit( a3m::SceneNode *node ) { addNode( node ); }
    void visit( a3m::Solid *node ) { addNode( node ); }
    void visit( a3m::Light *node ) { addNode( node ); }
    void visit( a3m::Camera *node ) { addNode( node ); }

    void addNode( a3m::SceneNode *node )
    {
      TVINSERTSTRUCT tvins;
      HTREEITEM hti;

      tvins.item.mask = TVIF_TEXT | TVIF_CHILDREN | TVIF_PARAM;

      std::string name = node->getClassName();
      name += ": ";
      name += node->getName();
      tvins.item.pszText = (LPSTR)name.c_str();
      tvins.item.cChildren = node->getChildCount() > 0 ? 1 : 0;
      tvins.item.lParam = (LPARAM)node;

      tvins.hParent = m_parents.back();
      tvins.hInsertAfter = TVI_LAST;

      hti = (HTREEITEM)SendMessage(m_wnd, TVM_INSERTITEM,
          0, (LPARAM)(LPTVINSERTSTRUCT)&tvins);

      m_parents.push_back( hti );
      for( A3M_UINT32 i = 0; i < node->getChildCount(); ++i )
        node->getChild(i)->accept( *this );
      m_parents.pop_back();
    }

  private:
    std::vector< HTREEITEM > m_parents;

    HWND m_wnd;
  };
}

SceneDialog::SceneDialog( HWND parentWindowHandle )
  : ViewerDialog( parentWindowHandle, IDD_SCENEGRAPH ),
    m_lightDlg( hwnd() ),
    m_cameraDlg( hwnd() ),
    m_solidDlg( hwnd() ),
    m_selected( 0 )
{
  hideChildDialogs();
}

void SceneDialog::init( a3m::SceneNode *node )
{
  TreeViewPopulator tvp( GetDlgItem( hwnd(), IDC_SCENETREE ) );
  tvp.addNode( node );
}

void SceneDialog::onCommand( A3M_INT32 control, A3M_INT32 event, A3M_INT32 lparam )
{
  if( ( control == IDC_CHECKVISIBLE ) &&
      ( event == BN_CLICKED ) &&
      m_selected )
  {
    setFlags( *m_selected, VISIBLE,
      IsDlgButtonChecked( hwnd(), IDC_CHECKVISIBLE ) == BST_CHECKED );
  }
}

void SceneDialog::onNotify( A3M_INT32 control, A3M_INT32 lparam )
{
  if( ( control == IDC_SCENETREE ) &&
      ( ((LPNMHDR)lparam)->code == TVN_SELCHANGED ) )
  {
    NMTREEVIEW *tv = (LPNMTREEVIEW) lparam;

    m_selected = reinterpret_cast< a3m::SceneNode * >( tv->itemNew.lParam );

    hideChildDialogs();

    if( m_selected )
    {
      m_selected->accept( *this );
      update();
    }
  }
}

void SceneDialog::update()
{
  if( m_selected )
  {
    CheckDlgButton( hwnd(), IDC_CHECKVISIBLE, getFlags( *m_selected, VISIBLE ) );
    setEditText( IDC_POS_X, IDC_POS_Y, IDC_POS_Z, m_selected->getPosition() );
    setEditText( IDC_SCALE_X, IDC_SCALE_Y, IDC_SCALE_Z, m_selected->getScale() );

    a3m::Quaternionf const& rotation = m_selected->getRotation();
    setEditText( IDC_AXIS_X, IDC_AXIS_Y, IDC_AXIS_Z, getAxis( rotation ) );
    setEditText( IDC_ANGLE, getDegrees( getAngle( rotation ) ) );

    m_solidDlg.update();
    m_lightDlg.update();
    m_cameraDlg.update();
  }
}


void SceneDialog::visit( a3m::SceneNode *node )
{
}

void SceneDialog::visit( a3m::Solid *solid )
{
  m_solidDlg.init( solid );
  m_solidDlg.visible( A3M_TRUE );
}

void SceneDialog::visit( a3m::Light *light )
{
  m_lightDlg.init( light );
  m_lightDlg.visible( A3M_TRUE );
}

void SceneDialog::visit( a3m::Camera *camera )
{
  m_cameraDlg.init( camera );
  m_cameraDlg.visible( A3M_TRUE );
}

void SceneDialog::hideChildDialogs()
{
  m_lightDlg.visible( A3M_FALSE );
  m_cameraDlg.visible( A3M_FALSE );
  m_solidDlg.visible( A3M_FALSE );
}
