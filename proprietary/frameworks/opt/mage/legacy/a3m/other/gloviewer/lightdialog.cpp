/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
#include "lightdialog.h"    /* Class header */
#include <a3m/light.h>      /* Extract information from Light object */

#include "resource.h"       /* Dialog and control resource identifiers */

namespace
{
}

LightDialog::LightDialog( HWND parentWindowHandle )
  : ViewerDialog( parentWindowHandle, IDD_LIGHT ), m_light( 0 )
{
}

void LightDialog::init( a3m::Light *light )
{
  m_light = light;
  update();
}

void LightDialog::update()
{
  if( m_light )
  {
    a3m::Light::LightType type = m_light->getLightType();

    CheckDlgButton( hwnd(), IDC_POINT, type == a3m::Light::LIGHTTYPE_OMNI );
    CheckDlgButton( hwnd(), IDC_DIRECTIONAL, type == a3m::Light::LIGHTTYPE_DIRECTIONAL );
    CheckDlgButton( hwnd(), IDC_SPOT, type == a3m::Light::LIGHTTYPE_SPOT );

    setEditText( IDC_LC_R, IDC_LC_G, IDC_LC_B, m_light->getColour() );
    setEditColour( IDC_LIGHT_COLOUR, m_light->getColour() );
    setEditText( IDC_LIGHT_INTENSITY, m_light->getIntensity() );
    setEditText( IDC_LIGHT_AMBIENT_LEVEL, m_light->getAmbientLevel() );

    CheckDlgButton( hwnd(), IDC_CHECK_ATTENUATE, m_light->getIsAttenuated() );
    setEditText( IDC_ATTENUATE_START, m_light->getAttenuationNear() );
    setEditText( IDC_ATTENUATE_END, m_light->getAttenuationFar() );

    setEditText( IDC_SPOT_INNER, getDegrees( m_light->getSpotInnerAngle() ) );
    setEditText( IDC_SPOT_OUTER, getDegrees( m_light->getSpotOuterAngle() ) );
  }
}
