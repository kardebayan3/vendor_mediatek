/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Mesh utility functions
 *
 */
#pragma once
#ifndef A3M_MESHUTILITY_H
#define A3M_MESHUTILITY_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/mesh.h> /* for Mesh */

namespace a3m
{
  /** \defgroup a3mMeshUtility MeshUtility
   * \ingroup  a3mRefScene
   *
   * Utility functions for creating commonly used mesh primitives.
   *
   * @{
   */

  /**
   * Creates a unit square mesh.
   * The square will be centred around the origin, with its normal in the
   * positive z-direction: (0, 0, 1).
   *
   * \return Square mesh
   */
  Mesh::Ptr createSquareMesh(
    MeshCache& meshCache,
    /**< Cache used to create Mesh */
    Vector2f const& uvScale
    /**< Scale factor applied to the UV coordinates */
    = Vector2f(1.0f, 1.0f));

  /**
   * Creates a unit cube mesh.
   * The cube will be centred around the origin, with the face normals
   * aligned with the axes.
   *
   * \return Cube mesh
   */
  Mesh::Ptr createCubeMesh(
    MeshCache& meshCache,
    /**< Cache used to create Mesh */
    Vector2f const& uvScale
    /**< Scale factor applied to the UV coordinates */
    = Vector2f(1.0f, 1.0f));

  /**
   * Creates a unit sphere mesh (radius == 0.5).
   * The sphere will be centred around the origin, and constructed from a
   * specified number of latitudinal and longitudinal divisions, where the
   * north and south poles are aligned with the y-axis.  The segment and wedge
   * counts will be constrained to the minimum value.
   *
   * \return Sphere mesh
   */
  Mesh::Ptr createSphereMesh(
    MeshCache& meshCache,
    /**< Cache used to create Mesh */
    A3M_UINT32 segmentCount,
    /**< The number of spherical segments (latitudinal).
     * The minimum number of segments is 2 */
    A3M_UINT32 wedgeCount,
    /**< The number of spherical wedges (longitudinal).
     * The minimum number of wedges is 2 */
    Vector2f const& uvScale
    /**< Scale factor applied to the UV coordinates */
    = Vector2f(1.0f, 1.0f));

  /** @} */

} /* namespace a3m */

#endif /* A3M_MESHUTILITY_H */

