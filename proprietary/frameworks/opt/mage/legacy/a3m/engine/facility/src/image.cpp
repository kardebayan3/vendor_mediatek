/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/fpmaths.h>        /* for clamp()                               */
#include <a3m/image.h>          /* This module's header                      */
#include <a3m/noncopyable.h>    /* for NonCopyable                           */
#include <a3m/pointer.h>        /* for SharedPtr                             */
#include <a3m/stb_image.h>      /* Third-party STB image library             */
#include <fileutility.h>        /* FileToString etc.                         */

namespace a3m
{
  Image::Image(
    A3M_INT32 width,
    A3M_INT32 height,
    A3M_INT32 channelCount,
    A3M_UINT8 const* data) :
    m_usingStb(false),
    m_width(width),
    m_height(height),
    m_channelCount(channelCount)
  {
    if (m_width < 0 || m_height < 0 || m_channelCount < 1)
    {
      m_data = 0;
      A3M_LOG_ERROR("Invalid image construction inputs.");
      return;
    }

    A3M_INT32 dataSize = m_width * m_height * m_channelCount;

    // Would use a std::vector to manage memory, but STB uses a normal pointer.
    m_data = new A3M_UINT8[dataSize];

    if (data)
    {
      memcpy(m_data, data, dataSize);
    }
  }

  Image::Image(Stream& stream) :
    m_usingStb(true), m_data(0)
  {
    FileToString buffer(stream);
    if (!buffer.get())
    {
      A3M_LOG_ERROR("Failed to allocate memory for file!", 0);
    }
    else
    {
      m_data = stbi_load_from_memory(
                 buffer,
                 buffer.length(),
                 &m_width,
                 &m_height,
                 &m_channelCount,
                 0);
    }
  }

  Image::~Image()
  {
    if (m_data)
    {
      if (m_usingStb)
      {
        stbi_image_free(m_data);
      }
      else
      {
        delete[] m_data;
      }
    }
  }

  A3M_INT32 Image::width() const
  {
    return m_width;
  }

  A3M_INT32 Image::height() const
  {
    return m_height;
  }

  A3M_INT32 Image::channelCount() const
  {
    return m_channelCount;
  }

  A3M_UINT8* Image::data()
  {
    return const_cast<A3M_UINT8*>(reinterpret_cast<Image const*>(this)->data());
  }

  A3M_UINT8 const* Image::data() const
  {
    return m_data;
  }

  Image::Ptr crop(
    Image const& source,
    A3M_INT32 left,
    A3M_INT32 top,
    A3M_INT32 width,
    A3M_INT32 height)
  {
    Image::Ptr image;

    if (!source.data())
    {
      A3M_LOG_ERROR("Cannot crop: source image has no data.");
      return image;
    }

    // Adjust the cropping extents so that they are not invalid.
    A3M_INT32 actualLeft = clamp(left, 0, source.width() - 1);
    A3M_INT32 actualTop = clamp(top, 0, source.height() - 1);
    A3M_INT32 actualWidth = clamp(width, 1, source.width() - actualLeft);
    A3M_INT32 actualHeight = clamp(height, 1, source.height() - actualTop);

    if (left != actualLeft ||
        top != actualTop ||
        width != actualWidth ||
        height != actualHeight)
    {
      A3M_LOG_WARN("Invalid crop extents adjusted to fit source.");
    }

    A3M_INT32 channelCount = source.channelCount();

    // Create uninitialized image.
    image.reset(new Image(actualWidth, actualHeight, channelCount, 0));

    // Now copy the cropped part of the image over.
    A3M_UINT8 const* srcData = source.data();
    A3M_UINT8* dstData = image->data();
    A3M_INT32 lineSize = channelCount * actualWidth;

    for (A3M_INT32 y = 0; y < actualHeight; ++y)
    {
      A3M_UINT8 const* src = srcData + channelCount *
                             (source.width() * (actualTop + y) + actualLeft);
      A3M_UINT8* dst = dstData + channelCount * actualWidth * y;

      memcpy(dst, src, lineSize);
    }

    return image;
  }

} /* namespace a3m */
