/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Background class
 *
 */
#pragma once
#ifndef A3M_BACKGROUND_H
#define A3M_BACKGROUND_H

#include <a3m/base_types.h>            /* Base types used by all A3M modules */
#include <a3m/colour.h>                /* for Colour4f                       */

namespace a3m
{
  /** \defgroup a3mBackground Background
   * \ingroup  a3mRefScene
   * @{
   */

  class RenderContext;

  /**
   * Used to selectively clear different parts of the frame buffer.
   *
   * It is often necessary to reset, or "clear", pixel values in the frame
   * buffer.  The frame buffer may comprise any combination of the colour
   * buffer, the depth buffer, and the stencil buffer.  Background provides a
   * convenient way to selectively reset these buffers to defined values, by
   * using the clear() function.
   *
   * The colour, depth and stencil values specify the values the buffers will
   * be reset to, while the masks specify which parts to clear.  Background
   * must be used in conjunction with a RenderContext when performing the
   * clear operation.
   *
   * Example usage:
   * \code
   *   // Create an instance of Background class
   *   a3m::Background bg;
   *   // Define colour buffer clear value
   *   bg.setColour(0.5f, 0.5f, 0.5f, 1.0f);
   *   // Apply colour buffer clear mask
   *   bg.setColourMask( A3M_FALSE, A3M_TRUE, A3M_TRUE, A3M_FALSE );
   *
   *   // Clear colour and other buffers based on their corresponding masks
   *   bg.enable(context);
   * \endcode
   */
  class Background
  {
  public:
    /** Initializes the background with default values.
     * By default, the RGBA colour buffer channels are 0.0, the depth value is
     * 1.0, and all stencil bits are zero.  The masks are all set (i.e. all
     * parts of all buffers are affected by a clear operation).
     */
    Background();

    /** Initializes the background with a specified RGBA colour value.
     * All other values are as set in Background().
     */
    Background( Colour4f const& colour );

    /** Value the colour buffer pixels are reset to when calling clear().
     */
    void setColour( const Colour4f& colour );
    Colour4f const& getColour() { return m_colour; }

    /** Value the depth buffer pixels are reset to when calling clear().
     */
    void setDepth( A3M_FLOAT depth /**< Should be in range [0.0, 1.0] */ );
    A3M_FLOAT getDepth() { return m_depth; }

    /** Value the stencil buffer pixels are reset to when calling clear().
     * Each bit of the stencil value is treated separately (setStencilMask()
     * takes a bit mask).
     */
    void setStencil( A3M_INT32 stencil );
    A3M_INT32 getStencil() { return m_stencil; }

    /** Specifies whether each channel in the colour buffer is reset when
     * calling clear().
     */
    void setColourMask( A3M_BOOL r, A3M_BOOL g, A3M_BOOL b, A3M_BOOL a );
    A3M_BOOL getColourMaskR() { return m_colourMaskR; }
    A3M_BOOL getColourMaskG() { return m_colourMaskG; }
    A3M_BOOL getColourMaskB() { return m_colourMaskB; }
    A3M_BOOL getColourMaskA() { return m_colourMaskA; }

    /** Specifies whether the depth buffer is reset when calling clear().
     */
    void setDepthMask( A3M_BOOL depthMask );
    A3M_BOOL getDepthMask() { return m_depthMask; }

    /** Specifies which bits in the stencil buffer are reset when calling clear().
     */
    void setStencilMask( A3M_UINT32 stencilMask /**< Bit mask */ );
    A3M_UINT32 getStencilMask() { return m_stencilMask; }

    /**
     * Clears the frame buffer using current background settings.
     */
    void enable(RenderContext& context) const;

  private:
    Colour4f      m_colour;
    A3M_FLOAT     m_depth;
    A3M_INT32     m_stencil;
    A3M_BOOL      m_colourMaskR;
    A3M_BOOL      m_colourMaskG;
    A3M_BOOL      m_colourMaskB;
    A3M_BOOL      m_colourMaskA;
    A3M_BOOL      m_depthMask;
    A3M_UINT32    m_stencilMask;
  };

  /** @} */

} /* a3m namespace */

#endif /* A3M_BACKGROUND_H */
