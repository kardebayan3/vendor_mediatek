/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Texture2DLoader Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/log.h>                 /* for logging                          */
#include <a3m/detail/assetpath.h>    /* AssetPath                            */
#include <fileutility.h>             /* FileToString etc.                    */
#include <stdstringutility.h>        /* for endsWithI()                      */
#include <texture2dloader.h>         /* This class's API                     */
#include <a3m/image.h>               /* for Image                            */

namespace
{
  /*
   * Local function to calculate the pixel format based on the image channel
   * count returned by STB.
   */
  a3m::Texture::Format formatFromChannelCount( A3M_INT32 n )
  {
    static const a3m::Texture::Format format[] =
    {
      a3m::Texture::ALPHA,
      a3m::Texture::ALPHA,
      a3m::Texture::LUMINANCE_ALPHA,
      a3m::Texture::RGB,
      a3m::Texture::RGBA
    };

    return format[n];
  }
} /* anonymous namespace */

namespace a3m
{
  /*
   * Loads a new 2D texture from the file system.
   */
  Texture2D::Ptr Texture2DLoader::load(
    Texture2DCache& cache,
    A3M_CHAR8 const* name)
  {
    Stream::Ptr stream = cache.getStream(name);
    if (!stream)
    {
      return Texture2D::Ptr();
    }

    Image image (*stream);

    Texture2D::Ptr texturePtr;

    if( image.data() && ( image.channelCount() > 0 ) &&
        ( image.channelCount() < 5 ) )
    {
      // ... x = width, y = height, n = # 8-bit components per pixel ...

      texturePtr = cache.create(
                     image.width(),
                     image.height(),
                     formatFromChannelCount( image.channelCount() ),
                     Texture::UNSIGNED_BYTE,
                     image.data(),
                     name );
    }
    else if(  image.data() )
    {
      A3M_LOG_ERROR(
        "Invalid bits per pixel, %d, in \"%s\" (can be 8, 16, 24 or 32).",
        image.channelCount() * 8, name);
    }
    else
    {
      // we don't report pvr as bad image because pvr is loaded from different
      // loader and error will be reported there.
      if (!endsWithI(name, ".pvr"))
      {
        A3M_LOG_ERROR("Image failed to load texture \"%s\"", name);
      }
    }

    /* Return a null pointer if failed to load */
    return texturePtr;
  }

  A3M_BOOL Texture2DLoader::isKnown(A3M_CHAR8 const* name)
  {
    std::string str = name;
    return
      endsWithI(str, ".jpg") || endsWithI(str, ".jpeg") ||
      endsWithI(str, ".bmp") || endsWithI(str, ".png") ||
      endsWithI(str, ".tga");
  }

} /* namespace a3m */
