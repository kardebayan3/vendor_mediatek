/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * GlResource class
 *
 */
#ifndef A3M_GLRESOURCE_H
#define A3M_GLRESOURCE_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/detail/resource.h>   /* for Resource */

namespace a3m
{
  namespace detail
  {
    /**
     * \ingroup  a3mResource
     * A base class for OpenGL resources.
     */
    class GlResource : public Resource
    {
    public:
      /**
       * Constructor
       */
      GlResource();

      /**
       * Constructor to wrap an existing OpenGL resource.
       */
      explicit GlResource(A3M_UINT32 id /**< Integer ID of GL resource */);

      /**
       * Destructor
       */
      ~GlResource();

      // Override
      A3M_BOOL deallocate();

      // Override
      A3M_BOOL release();

      // Override
      State getState() const { return m_state; }

      /**
       * Allocates an OpenGL resource.
       * Allocation can only occur if the resource was previously UNALLOCATED.
       *
       * \return A3M_TRUE if successful, A3M_FALSE if state is not UNALLOCATED,
       * or allocation failed
       */
      A3M_BOOL allocate();

      /**
       * Returns the allocated OpenGL texture's name.
       *
       * \return Texture name if resource state is ALLOCATED, otherwise zero.
       */
      A3M_UINT32 getId() { return m_id; }

    private:
      /**
       * Performs actual allocation of resource.
       * This function must be implemented in concrete resources.
       *
       * \return Integer ID of OpenGL resource.
       */
      virtual A3M_UINT32 doAllocate() = 0;

      /**
       * Performs actual deallocation of resource.
       * This function must be implemented in concrete resources.
       */
      virtual void doDeallocate(A3M_UINT32 id
                                /**< Integer ID of GL resource */) = 0;

      State m_state;
      A3M_UINT32 m_id;
    };

  } /* namespace detail */

} /* namespace a3m */

#endif /* A3M_GLRESOURCE_H */

