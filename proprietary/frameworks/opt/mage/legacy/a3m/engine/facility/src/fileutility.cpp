/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Utility class to load a file into memory
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <fileutility.h>                     /* This class's API        */
#include <cctype>                                 /* for isspace etc.        */
#include <cmath>                                  /* pow */

//lint -esym(960, 33) Disable Lint warning about side effects on right hand of
// logical operators ||and &&. Many functions in this file rely on
// left-to-right evaluation and short-circuiting.

namespace a3m
{
  /*
   * consume whitespace at beginning of range
   */
  void eatWhite( CharRange& range )
  {
    while( !range.empty() && isspace( range.front() ) ) { ++range; }
  }

  /*
   * require character at start of range and consume it
   */
  A3M_BOOL requireChar( CharRange& range, A3M_INT32 ch )
  {
    eatWhite( range );
    if( range.empty() || ( range.front() != ch ) ) { return false; }
    ++range;
    return true;
  }

  /*
   * consume beginning of range until character is found.
   */
  void readTo( CharRange& range, A3M_INT32 ch )
  {
    while( !range.empty() && ( range.front() != ch ) ) { ++range; }
  }

  /*
   * read token from range and set following character to null
   * If the first character is a quote, the token comprises all characters
   * including whitespace up to the next quote, which is consumed.
   * Otherwise the token comprises all characters until the next whitespace.
   */
  CharRange readToken( CharRange& range )
  {
    eatWhite( range );
    A3M_CHAR8* begin = range.begin;
    if( *begin == '"' )
    {
      ++range;
      ++begin;
      readTo( range, '"' );
    }
    else
    {
      while( !range.empty() && !isspace( range.front() ) ) { ++range; }
    }
    CharRange ret( begin, range.begin );

    // Ensure the returned token is null-terminated. If there are no more
    // characters left in the range then the token will be already be
    // null-terminated.
    if( !range.empty() )
    {
      *range.begin = 0;
      ++range;
    }
    return ret;
  }

  /*
   * read A3M_FLOAT from range
   */
  A3M_FLOAT readFloat( CharRange& range, A3M_FLOAT def )
  {
    eatWhite( range );

    if( range.empty() ) { return def; }

    A3M_FLOAT mant = 0.0f;
    A3M_BOOL mantIsNegative = ( range.front() == '-' );
    if( mantIsNegative || ( range.front() == '+' ) ) { ++range; }

    if( range.empty() && !isdigit( range.front() ) ) { return def; }

    while( !range.empty() && isdigit( range.front() ) )
    {
      mant = ( mant * 10 ) + range.front() - '0';
      ++range;
    }
    if( !range.empty() && ( range.front() == '.' ) )
    {
      ++range;
      A3M_FLOAT mul = 0.1f;
      while( isdigit( range.front() ) && !range.empty() )
      {
        mant += ( (range.front() - '0') * mul );
        mul *= 0.1f;
        ++range;
      }
    }
    if( !range.empty() &&
        ( ( range.front() == 'e' ) || ( range.front() == 'E' ) ) )
    {
      ++range;
      A3M_BOOL expIsNegative = ( range.front() == '-' );
      if( expIsNegative || ( range.front() == '+' ) ) { ++range; }
      A3M_FLOAT exponent = 0.0f;
      while( !range.empty() && isdigit( range.front() ) )
      {
        exponent = ( exponent * 10 ) + range.front() - '0';
        ++range;
      }
      if( expIsNegative ) { exponent = -exponent; }
      mant *= pow( 10.0f, exponent );
    }
    if( mantIsNegative ) { mant = -mant; }
    return mant;
  }

  /*
   * read int from range
   */
  A3M_INT32 readInt( CharRange& range, A3M_INT32 def )
  {
    eatWhite( range );

    if( range.empty() ) { return def; }
    A3M_BOOL neg = ( range.front() == '-' );
    if( neg || ( range.front() == '+' ) ) { ++range; }

    if( range.empty() || !isdigit( range.front() ) ) { return def; }

    A3M_INT32 i = 0;

    while( !range.empty() && isdigit( range.front() ) )
    {
      i = ( i * 10 ) + range.front() - '0';
      ++range;
    }
    if( neg ) { i = -i; }
    return i;
  }

  /*
   * read unsigned int from range
   */
  A3M_UINT32 readUInt( CharRange& range, A3M_UINT32 def )
  {
    eatWhite( range );

    if( range.empty() ) { return def; }

    if( !isdigit( range.front() ) ) { return def; }

    A3M_UINT32 i = 0;

    while( !range.empty() && isdigit( range.front() ) )
    {
      i = i * 10 + range.front() - '0';
      ++range;
    }
    return i;
  }

  /*
   * read bool from range
   */
  A3M_BOOL readBool( CharRange& range )
  {
    eatWhite( range );
    CharRange token = readToken( range );

    if( token.empty() ) { return A3M_FALSE; }

    return( token.front() == '1' ) ||
          ( token.front() == 't' ) ||
          ( token.front() == 'T' );
  }




  /*
   * constructor
   */
  FileToString::FileToString( Stream& stream )
    : m_string()
  {
    read( stream );
  }

  FileToString::FileToString( A3M_CHAR8 const* filename )
    : m_string()
  {
    a3m::StreamSource::Ptr pssSource = a3m::StreamSource::get( "." );

    if( !pssSource ) { return; }
    if( !pssSource->exists( filename ) ) { return; }

    a3m::Stream::Ptr pssInStream = pssSource->open( filename );

    if( !pssInStream ) { return; }

    read( *pssInStream );
  }

  void FileToString::read( Stream& pssInStream )
  {
    A3M_UINT32 len = static_cast<A3M_UINT32>(pssInStream.size());
    len -= pssInStream.tell();

    m_string.begin = new A3M_CHAR8[ len + 1 ];
    if( m_string.begin )
    {
      /* read data as a block and set end to begin + number of bytes actually
       * read. */
      m_string.end = m_string.begin +
                     pssInStream.read( m_string.begin, len );
      *m_string.end = 0;
    }
    else
    {
      m_string.end = 0;
    }
  }

  /*
  * destructor
  */
  FileToString::~FileToString()
  {
    delete [] m_string.begin;
  }

} /* namespace a3m */
