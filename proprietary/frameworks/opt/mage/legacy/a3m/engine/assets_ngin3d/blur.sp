1.0
vertex_source_file ngin3d#blur.vert
fragment_source_file ngin3d#blur.frag

uniform u_m_diffuseTexture M_DIFFUSE_TEXTURE
uniform u_m_sampleMult M_SAMPLE_MULT
