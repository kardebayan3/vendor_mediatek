1.0
vertex_source_file ngin3d#ripple.vert
fragment_source_file ngin3d#ripple.frag

uniform u_t_modelViewProjection T_MODEL_VIEW_PROJECTION

uniform u_m_diffuseTexture M_DIFFUSE_TEXTURE
uniform u_mirrorTexture M_MIRROR_TEXTURE

uniform u_animationTime TIME
