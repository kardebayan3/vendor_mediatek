/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A vertex shader for ripple effect.
 *
 */

// Ripple (noise) texture coordinate multiplier.
// Higher values result in more tightly packed ripples.
#define RIPPLE_DENSITY 3.0

// Ripple speed. Higher values mean ripples move more quickly.
#define RIPPLE_SPEED 0.15


/* Transformation uniforms */
uniform mat4 u_t_modelViewProjection;
uniform float u_animationTime;

attribute vec4 a_position;    // Vertex position (model space)
attribute vec2 a_uv0;         // Texture coordinate

varying mediump vec2 v_texCoord0;
varying mediump vec4 v_texCoord1;

void main()
{
  gl_Position = u_t_modelViewProjection * a_position;

  v_texCoord0 = a_uv0.xy * RIPPLE_DENSITY + u_animationTime * RIPPLE_SPEED;
  v_texCoord1 = gl_Position;
}
