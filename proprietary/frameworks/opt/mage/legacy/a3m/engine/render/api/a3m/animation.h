/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Animation classes
 */

#pragma once
#ifndef ANIMATION_H
#define ANIMATION_H

#include <algorithm>                    /* for std::sort() etc.               */
#include <vector>                       /* for std::vector                    */
#include <a3m/pointer.h>                /* for Shared and SharedPtr           */
#include <a3m/noncopyable.h>            /* for NonCopyable                    */

namespace a3m
{

  /**
   * Interface used to generate a value which varies between a start and end
   * point.
   * An AnimatedValue generates a value for a given progress.  Clients can
   * implement the interface to provide their own animation generators, or can
   * use the AnimationKeySequence class provided with A3M.
   */
  template<typename T>
  class AnimatedValue : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS(AnimatedValue)

    /** Smart pointer type for this class */
    typedef SharedPtr<AnimatedValue> Ptr;

    /**
     * Destructor.
     */
    virtual ~AnimatedValue() {}

    /**
     * Returns the value at the given progress.
     * \return Value at progress
     */
    virtual T getValue(A3M_FLOAT progress /**< Progress */) const = 0;

    /**
     * Returns the start point of the value.
     * \return Value start point
     */
    virtual A3M_FLOAT getStart() const = 0;

    /**
     * Returns the end point of the value.
     * \return Value end point
     */
    virtual A3M_FLOAT getEnd() const = 0;
  };

  /**
   * An animation key with a position and value.
   * Keys may be used to construct a discretely defined sequence of values. The
   * key itself is just a simple position-value pair of a specific type.
   */
  template<typename T>
  class AnimationKey
  {
  public:
    /**
     * Constructs a key with a given position and value.
     */
    AnimationKey(
      A3M_FLOAT position = 0.0f, /**< Key position */
      T const& value = T() /**< Key value */);

    /**
     * Returns the key position.
     * \return Key position
     */
    A3M_FLOAT getPosition() const;

    /**
     * Returns the key value.
     * \return Key value
     */
    T const& getValue() const;

  private:
    A3M_FLOAT m_position; /* Key position */
    T m_value;            /* Key value    */
  };

  /**
   * AnimatedValue comprised of AnimationKey objects.
   * The keys are sorted in ascending order according to their position, and
   * for a given progress, the value of the key with the greatest position
   * less than or equal to the progress will be returned.  In other words, the
   * sequence is a step function defined by the most recent key in the
   * sequence.
   */
  template<typename T>
  class AnimationKeySequence : public AnimatedValue<T>
  {
  public:
    A3M_NAME_SHARED_CLASS(AnimationKeySequence)

    /** Smart pointer type for this class */
    typedef SharedPtr<AnimationKeySequence> Ptr;

    /** Type of AnimationKey used by this sequence. */
    typedef AnimationKey<T> KeyType;

    /**
     * Constructs a sequence from an array of keys.
     * The keys will be sorted in order of position, but construction will be
     * more efficient if the keys have been sorted beforehand.
     */
    AnimationKeySequence(
      KeyType const* key, /**< Array of keys */
      A3M_INT32 keyCount /**< Size of key array */);

    /**
     * Returns the number of keys in the sequence.
     * \return Key count
     */
    A3M_INT32 getKeyCount() const;

    /**
     * Returns a key in the sequence.
     * The index is valid from 0 to N - 1, where N is the key count.
     * \return The key, or a default key if the index is invalid
     */
    KeyType getKey(A3M_INT32 i /**< Index */) const;

    // Override
    T getValue(A3M_FLOAT progress /**< Progress */) const;

    // Override
    A3M_FLOAT getStart() const;

    // Override
    A3M_FLOAT getEnd() const;

  private:
    /*
     * Returns whether one key is positioned before another.
     */
    static A3M_BOOL positionedBefore(
      AnimationKey<T> const& lhs,
      AnimationKey<T> const& rhs);

    typedef std::vector<AnimationKey<T> > AnimationKeyVector;

    AnimationKeyVector m_keys;  /* List of all the keys */
  };

  /**
   * Interface used to encapsulate user-defined animations.
   * Animation represents some kind of functionality which may vary between a
   * start and an end point.  The user may drive whatever function the
   * Animation provides by calling apply() at the required points.
   *
   * Generally, an Animation will control one or more properties of one or more
   * objects over time.  For example, a Camera may be smoothly animated from
   * one position to another in a pre-defined path.
   */
  class Animation : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS(Animation)

    /** Smart pointer type for this class */
    typedef SharedPtr<Animation> Ptr;

    /**
     * Destructor.
     */
    virtual ~Animation() {}

    /**
     * Applies the animation at the given progress.
     * Animated properties will generally vary over the course of an animation.
     * This function can be called, usually at regular intervals, to cause an
     * animation to progress.  Progress will often correspond to the time
     * elapsed since the animation started, but other inputs may be used
     * instead.
     */
    virtual void apply(
      A3M_FLOAT progress /**< Point at which to apply the animation */) = 0;

    /**
     * Returns the start point of the Animation.
     * \return Start point
     */
    virtual A3M_FLOAT getStart() const = 0;

    /**
     * Returns the end point of the Animation.
     * \return Start point
     */
    virtual A3M_FLOAT getEnd() const = 0;
  };

  /**
   * Templated interface used to effect an animation.
   * This interface is intended to be implemented by the user to use an
   * animated value in a specific way.  For example, an Animator sub-class
   * might use an animated Vector3f to animate the position of a SceneNode, or
   * to modulate the intensity of a Light.  The animated value can be used in
   * any way the user desires.
   */
  template<typename T>
  class Animator : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS(Animator)

    /** Smart pointer type for this class */
    typedef SharedPtr<Animator> Ptr;

    /**
     * Destructor.
     */
    virtual ~Animator() {}

    /**
     * Passes a value for use by the animator.
     */
    virtual void apply(T const& value /**< Animated value */) = 0;
  };

  /**
   * Templated class linking an AnimatedValue and an Animator.
   * This class takes the animated value output by an AnimatedValue, and
   * passes it to an Animator which takes the same type.  AnimationChannel is
   * an Animation, which means it may be stored in an AnimationGroup, or driven
   * directly or by an AnimationController.
   */
  template<typename T>
  class AnimationChannel : public Animation
  {
  public:
    A3M_NAME_SHARED_CLASS(AnimationChannel)

    /** Smart pointer type for this class */
    typedef SharedPtr<AnimationChannel> Ptr;

    /** Type of AnimatedValue correponding to this channel. */
    typedef AnimatedValue<T> ValueType;

    /** Type of Animator correponding to this channel. */
    typedef Animator<T> AnimatorType;

    /**
     * Constructs a channel from an AnimatedValue and Animator.
     */
    AnimationChannel(
      typename ValueType::Ptr const& value, /**< AnimatedValue */
      typename AnimatorType::Ptr const& animator /**< Animator */);

    // Override
    void apply(
      A3M_FLOAT progress /**< Point at which to apply the animation */);

    // Override
    A3M_FLOAT getStart() const;

    // Override
    A3M_FLOAT getEnd() const;

  private:
    typename ValueType::Ptr m_value;        /* Animated value      */
    typename AnimatorType::Ptr m_animator;  /* Animator object     */
  };

  /**
   * Allows multiple Animation objects to be treated as one.
   * Often, individual properties will need to be animated in-sync with one
   * another.  Rather than driving each Animation separately, an AnimationGroup
   * may be used to drive all the animations from a single location.
   * AnimationGroup is an Animation, so an AnimationGroup may be placed inside
   * another AnimationGroup.
   */
  class AnimationGroup : public Animation
  {
  public:
    A3M_NAME_SHARED_CLASS(AnimationGroup)

    /** Smart pointer type for this class */
    typedef SharedPtr<AnimationGroup> Ptr;

    /**
     * Adds an Animation to the group.
     */
    void addAnimation(Animation::Ptr const& animation /**< Animation to add */);

    /**
     * Returns the number of Animations in the group.
     * \return Animation count
     */
    A3M_INT32 getAnimationCount() const;

    /**
     * Returns an Animation stored in the group.
     * The index is valid from 0 to N - 1, where N is the animation count.
     * \return Animation, or null if the index is invalid
     */
    Animation::Ptr getAnimation(A3M_INT32 i /**< Index */) const;

    // Override
    void apply(
      A3M_FLOAT progress /**< Point at which to apply the animation */);

    // Override
    A3M_FLOAT getStart() const;

    // Override
    A3M_FLOAT getEnd() const;

  private:
    typedef std::vector<Animation::Ptr> AnimationVector;

    AnimationVector m_animations; /* Grouped animations */
  };

  /**
   * Controls the playback of animations.
   * Animations may be controlled directly, but this class provides useful
   * playback functionality usually expected of an animation system, such as
   * progress tracking, pausing, speed adjustment and looping.
   */
  class AnimationController : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS(AnimationController)

    /** Smart pointer type for this class */
    typedef SharedPtr<AnimationController> Ptr;

    /**
     * Constructs a controller to control an animation.
     */
    AnimationController(
      Animation::Ptr const& animation /**< Animation to control */);

    /**
     * Increments the progress of the controller and applies the animation.
     * If the controller is not enabled, this function will do nothing.
     *
     * If the controller is enabled and paused, the progress will not change,
     * but the animation will still be applied.
     *
     * If the controller is enabled and not paused, the controller progress
     * will advance by delta multiplied by the controller speed.
     *
     * If looping is enabled, loop points will be taken into account when
     * incrementing the progress.  If the progress advances beyond either end
     * of the loop, the progress will be wrapped around to the other end of the
     * loop.  To exit a loop, set the progress directly to a point outside of
     * the loop.  If the loop end is less than or equal to the loop start, then
     * the loop is undefined and the progress will not be looped, even if
     * looping is enabled.
     */
    void update(
      A3M_FLOAT delta = 0.0f
                        /**< Amount by which to increment the progress */);

    /**
     * Sets the progress of the controller.
     * This function can be used to set the progress of controller directly,
     * without taking into account loop points.  You can jump out of a loop by
     * using this function.  You can also set the progess beyond the bounds set
     * using setStart() and setEnd(), although the progress will be clamped to
     * the bounds when update() is next called, assuming the controller is not
     * paused.
     */
    void setProgress(A3M_FLOAT progress /**< Progress of controller */);

    /**
     * Returns the current progress of the controller.
     * The progress will loop back on itself if looping is enabled.
     * \return Progress of controller
     */
    A3M_FLOAT getProgress() const;

    /**
     * Sets whether the controller is enabled.
     * When a controller is disabled, calls to setProgress() and update() will
     * do nothing.  Controllers are enabled by default.
     */
    void setEnabled(A3M_BOOL enabled /**< Whether to enable the controller */);

    /**
     * Returns whether the controller is enabled.
     * \return Whether controller is enabled
     */
    A3M_BOOL getEnabled() const;

    /**
     * Sets whether the controller is paused.
     * When a controller is paused, calls to update() will not progress the
     * animation.  Controllers are not paused by default.
     */
    void setPaused(A3M_BOOL paused /**< Whether to pause the controller */);

    /**
     * Returns whether the controller is paused.
     * \return Whether controller is paused
     */
    A3M_BOOL getPaused() const;

    /**
     * Sets whether the controller playback is looping.
     * If a controller is looping, the loop points will take effect when
     * advancing the progress with a call to update().  Looping is enabled by
     * default, but the animation will only loop if a loop is defined.
     */
    void setLooping(A3M_BOOL looping /**< Whether the controller is looping */);

    /**
     * Returns whether the controller playback is looping.
     * \return Whether controller playback is looping
     */
    A3M_BOOL getLooping() const;

    /**
     * Sets the playback speed of the controller.
     * The controller speed determines the rate at which the controller
     * progress is advanced when calling update().  The speed is set to 1.0 by
     * default.
     */
    void setSpeed(A3M_FLOAT speed /**< Controller speed */);

    /**
     * Returns the controller playback speed.
     * \return Playback speed
     */
    A3M_FLOAT getSpeed() const;

    /**
     * Sets start point of the animation.
     * The lower bound of the range to which the controller progress is clamped
     * when update() is called.  By default this is set to start point of the
     * controlled animation.
     */
    void setStart(A3M_FLOAT start /**< Controller start point */);

    /**
     * Returns start point of the animation.
     * \return Animation start point
     */
    A3M_FLOAT getStart() const;

    /**
     * Sets end point of the animation.
     * The upper bound of the range to which the controller progress is clamped
     * when update() is called.  By default this is set to end point of the
     * controlled animation.
     */
    void setEnd(A3M_FLOAT end /**< Controller end point */);

    /**
     * Returns end point of the animation.
     * \return Animation end point
     */
    A3M_FLOAT getEnd() const;

    /**
     * Sets loop start point of the animation.
     * The lower bound of the range over which the controller progress is
     * looped when update() is called.  If the start and end points are equal
     * the the loop is undefined.  The loop start point is set to 0.0 by
     * default.
     */
    void setLoopStart(A3M_FLOAT start /**< Controller loop start point */);

    /**
     * Returns loop start point of the animation.
     * \return Animation loop start point
     */
    A3M_FLOAT getLoopStart() const;

    /**
     * Sets loop end point of the animation.
     * The upper bound of the range over which the controller progress is
     * looped when update() is called.  If the start and end points are equal
     * the the loop is undefined.  The loop end point is set to 0.0 by default.
     */
    void setLoopEnd(A3M_FLOAT end /**< Controller loop end point */);

    /**
     * Returns loop end point of the animation.
     * \return Animation loop end point
     */
    A3M_FLOAT getLoopEnd() const;

    /**
     * Returns the animation controlled by the controller.
     * \return Animation
     */
    Animation::Ptr const& getAnimation() const;

  private:
    A3M_BOOL m_enabled;         /* Whether controller is enabled  */
    A3M_BOOL m_paused;          /* Whether controller is paused   */
    A3M_BOOL m_looping;         /* Whether controller is looping  */
    A3M_FLOAT m_speed;          /* Speed of playback              */
    A3M_FLOAT m_progress;       /* Progress of playback           */
    A3M_FLOAT m_start;          /* Start point of playback        */
    A3M_FLOAT m_end;            /* End point of playback          */
    A3M_FLOAT m_loopStart;      /* Loop start point               */
    A3M_FLOAT m_loopEnd;        /* Loop end point                 */
    Animation::Ptr m_animation; /* Controlled animation           */
  };

  /**
   * Constructs an AnimationKeySequence directly in the smart pointer.
   */
  template<typename T>
  typename AnimationKeySequence<T>::Ptr createAnimationKeySequence(
    AnimationKey<T> const* key,
    /**< Array of keys */
    A3M_INT32 keyCount
    /**< Size of array */);

  /**
   * Constructs an AnimationChannel directly in the smart pointer.
   */
  template<typename T>
  typename AnimationChannel<T>::Ptr createAnimationChannel(
    typename AnimatedValue<T>::Ptr const& value,
    /**< AnimatedValue */
    typename Animator<T>::Ptr const& animator
    /**< Animator */);

  /**
   * Enables and unpauses a controller, rewinding if playback has finished.
   * This is a convenience function for those who prefer to use the more
   * traditional play/pause/stop interface.  The animation is updated by
   * default, but this can be optionally disabled.
   */
  void play(
    AnimationController& controller, /**< Controller */
    A3M_BOOL update = A3M_TRUE /**< Whether to update the animation */);

  /**
   * Pauses a controller.
   * This is a convenience function for those who prefer to use the more
   * traditional play/pause/stop interface.  The animation is updated by
   * default, but this can be optionally disabled.
   */
  void pause(
    AnimationController& controller, /**< Controller */
    A3M_BOOL update = A3M_TRUE /**< Whether to update the animation */);

  /**
   * Disables and rewinds a controller.
   * This is a convenience function for those who prefer to use the more
   * traditional play/pause/stop interface.  The animation is updated by
   * default, but this can be optionally disabled.
   */
  void stop(
    AnimationController& controller, /**< Controller */
    A3M_BOOL update = A3M_TRUE /**< Whether to update the animation */);

  /**
   * Updates a controller and its animation to a specific point.
   * This function is equivilent to setting a controller's progress and then
   * updating the animation with a zero delta.
   */
  void seek(
    AnimationController& controller, /**< Controller */
    A3M_FLOAT progress /**< Progress to which to seek */);

  /**
   * Rewinds a controller to the beginning of playback.
   * If the controller speed is positive, the progress will be set to the start
   * of the animation; if speed is negative, it will be set to the end.  If
   * speed is zero, this function does nothing.  By default, the animation is
   * updated, but this can be optionally disabled.
   */
  void rewind(
    AnimationController& controller /**< Controller */,
    A3M_BOOL update = A3M_TRUE /**< Whether to update the animation */);

  /**
   * Determines whether a controller has finished playback.
   * If the controller speed is positive, playback is complete once the
   * progress has reached the end of the animation; if speed is negative, it is
   * complete when at the start. If speed is zero, this function will return
   * FALSE.  If playback is inside a loop, this function will return FALSE.
   * \return TRUE if playback has finished
   */
  A3M_BOOL isFinished(AnimationController const& controller /**< Controller */);

  /**
   * Determines whether a controller has a loop defined.
   * A loop is defined if the loop end is greater than the loop start.
   * \return TRUE if controller has a loop
   */
  A3M_BOOL hasLoop(AnimationController const& controller /**< Controller */);

  /**
   * Determines whether a controller is currently inside a loop.
   * The controller is inside a loop if looping is enabled, a loop is defined
   * and the progress is inside the loop range.
   * \return TRUE if inside a loop
   */
  A3M_BOOL isInsideLoop(AnimationController const& controller /**< Controller */);

  /**
   * Sets the start and end of a controller's playback.
   */
  void setRange(
    AnimationController& controller, /**< Controller */
    A3M_FLOAT start, /**< Playback start point */
    A3M_FLOAT end /**< Playback end point */);

  /**
   * Sets the start and end of a controller's playback.
   */
  void setLoopRange(
    AnimationController& controller, /**< Controller */
    A3M_FLOAT start, /**< Loop start point */
    A3M_FLOAT end /**< Loop end point */);

  /**
   * Returns the length of an animation.
   * The length is always greater or equal to zero.
   * \return Animation length
   */
  A3M_FLOAT getLength(Animation const& animation /**< Animation */);

  /**
   * Returns the playback length of an animation controller.
   * The length is always greater or equal to zero.
   * \return Playback length
   */
  A3M_FLOAT getLength(AnimationController const& controller /**< Controller */);

  /**
   * Returns the length of an animation controller loop.
   * The length is always greater or equal to zero.
   * \return Loop length
   */
  A3M_FLOAT getLoopLength(AnimationController const& controller /**< Controller */);


  /****************************************************************************
   *                              AnimationKey                                *
   ****************************************************************************/

  template<typename T>
  AnimationKey<T>::AnimationKey(A3M_FLOAT position, T const& value) :
    m_position(position),
    m_value(value)
  {
  }

  template<typename T>
  A3M_FLOAT AnimationKey<T>::getPosition() const
  {
    return m_position;
  }

  template<typename T>
  T const& AnimationKey<T>::getValue() const
  {
    return m_value;
  }

  /****************************************************************************
   *                          AnimationKeySequence                            *
   ****************************************************************************/

  template<typename T>
  AnimationKeySequence<T>::AnimationKeySequence(
    KeyType const* key, A3M_INT32 keyCount) :
    m_keys(key, key + keyCount)
  {
    std::sort(m_keys.begin(), m_keys.end(), positionedBefore);
  }

  template<typename T>
  A3M_INT32 AnimationKeySequence<T>::getKeyCount() const
  {
    return static_cast<A3M_INT32>(m_keys.size());
  }

  template<typename T>
  typename AnimationKeySequence<T>::KeyType AnimationKeySequence<T>::getKey(
    A3M_INT32 i) const
  {
    if (i >= getKeyCount())
    {
      A3M_LOG_WARN("Index %d exceeds key count %d.", i, getKeyCount());
      return KeyType();
    }

    return m_keys[i];
  }

  template<typename T>
  T AnimationKeySequence<T>::getValue(A3M_FLOAT progress) const
  {
    if (m_keys.empty())
    {
      return T();
    }

    // Find the first key with a position greater than progress (the next key)
    typename AnimationKeyVector::const_iterator it = std::upper_bound(
          m_keys.begin(), m_keys.end(), KeyType(progress), positionedBefore);

    // But we want the current key, so move one back
    if (it != m_keys.begin())
    {
      --it;
    }

    return it->getValue();
  }

  template<typename T>
  A3M_FLOAT AnimationKeySequence<T>::getStart() const
  {
    if (getKeyCount() > 0)
    {
      return m_keys.front().getPosition();
    }

    return 0.0f;
  }

  template<typename T>
  A3M_FLOAT AnimationKeySequence<T>::getEnd() const
  {
    if (getKeyCount() > 0)
    {
      return m_keys.back().getPosition();
    }

    return 0.0f;
  }

  template<typename T>
  A3M_BOOL AnimationKeySequence<T>::positionedBefore(
    AnimationKey<T> const& lhs,
    AnimationKey<T> const& rhs)
  {
    return lhs.getPosition() < rhs.getPosition();
  }

  /****************************************************************************
   *                            AnimationChannel                              *
   ****************************************************************************/

  template<typename T>
  AnimationChannel<T>::AnimationChannel(
    typename ValueType::Ptr const& value,
    typename AnimatorType::Ptr const& animator) :
    m_value(value),
    m_animator(animator)
  {
  }

  template<typename T>
  void AnimationChannel<T>::apply(A3M_FLOAT progress)
  {
    m_animator->apply(m_value->getValue(progress));
  }

  template<typename T>
  A3M_FLOAT AnimationChannel<T>::getStart() const
  {
    return m_value->getStart();
  }

  template<typename T>
  A3M_FLOAT AnimationChannel<T>::getEnd() const
  {
    return m_value->getEnd();
  }

  /****************************************************************************
   *                           AnimationController                            *
   ****************************************************************************/

  inline void AnimationController::setProgress(A3M_FLOAT progress)
  {
    m_progress = progress;
  }

  inline A3M_FLOAT AnimationController::getProgress() const
  {
    return m_progress;
  }

  inline void AnimationController::setEnabled(A3M_BOOL enabled)
  {
    m_enabled = enabled;
  }

  inline A3M_BOOL AnimationController::getEnabled() const
  {
    return m_enabled;
  }

  inline void AnimationController::setPaused(A3M_BOOL paused)
  {
    m_paused = paused;
  }

  inline A3M_BOOL AnimationController::getPaused() const
  {
    return m_paused;
  }

  inline void AnimationController::setLooping(A3M_BOOL looping)
  {
    m_looping = looping;
  }

  inline A3M_BOOL AnimationController::getLooping() const
  {
    return m_looping;
  }

  inline void AnimationController::setSpeed(A3M_FLOAT speed)
  {
    m_speed = speed;
  }

  inline A3M_FLOAT AnimationController::getSpeed() const
  {
    return m_speed;
  }

  inline void AnimationController::setStart(A3M_FLOAT start)
  {
    m_start = start;
  }

  inline A3M_FLOAT AnimationController::getStart() const
  {
    return m_start;
  }

  inline void AnimationController::setEnd(A3M_FLOAT end)
  {
    m_end = end;
  }

  inline A3M_FLOAT AnimationController::getEnd() const
  {
    return m_end;
  }

  inline void AnimationController::setLoopStart(A3M_FLOAT start)
  {
    m_loopStart = start;
  }

  inline A3M_FLOAT AnimationController::getLoopStart() const
  {
    return m_loopStart;
  }

  inline void AnimationController::setLoopEnd(A3M_FLOAT end)
  {
    m_loopEnd = end;
  }

  inline A3M_FLOAT AnimationController::getLoopEnd() const
  {
    return m_loopEnd;
  }

  inline Animation::Ptr const& AnimationController::getAnimation() const
  {
    return m_animation;
  }

  /****************************************************************************
   *                             Free Functions                               *
   ****************************************************************************/

  template<typename T>
  typename AnimationKeySequence<T>::Ptr createAnimationKeySequence(
    AnimationKey<T> const* key, A3M_INT32 keyCount)
  {
    return typename AnimationKeySequence<T>::Ptr(
             new AnimationKeySequence<T>(key, keyCount));
  }

  template<typename T>
  typename AnimationChannel<T>::Ptr createAnimationChannel(
    typename AnimatedValue<T>::Ptr const& value,
    typename Animator<T>::Ptr const& animator)
  {
    return typename AnimationChannel<T>::Ptr(
             new AnimationChannel<T>(value, animator));
  }

} // namespace a3m

#endif // ANIMATION_H
