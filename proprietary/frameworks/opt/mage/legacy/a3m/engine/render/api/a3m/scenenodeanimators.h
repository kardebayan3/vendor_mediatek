/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Scene node animator classes.
 */

#pragma once
#ifndef A3M_SCENE_NODE_ANIMATORS_H
#define A3M_SCENE_NODE_ANIMATORS_H

#include <a3m/animation.h> /* for Animator */
#include <a3m/scenenode.h> /* for SceneNode */

namespace a3m
{

  /**
   * Animates a SceneNode's position.
   */
  class SceneNodePositionAnimator : public Animator<Vector3f>
  {
  public:
    /**
     * Creates an animator for a SceneNode.
     */
    SceneNodePositionAnimator(SceneNode* node /**< Scene node */) :
      m_node(node)
    {
    }

    // Override
    void apply(Vector3f const& position)
    {
      m_node->setPosition(position);
    }

  private:
    SceneNode* m_node; /* Scene node */
  };

  /**
   * Animates a SceneNode's rotation.
   */
  class SceneNodeRotationAnimator : public Animator<Vector4f>
  {
  public:
    /**
     * Creates an animator for a SceneNode.
     */
    SceneNodeRotationAnimator(SceneNode* node /**< Scene node */) :
      m_node( node )
    {
    }

    // Override
    void apply(Vector4f const& axisAngle)
    {
      m_node->setRotation(Quaternionf(
                            Vector3f(axisAngle), radians(axisAngle[3])));
    }

  private:
    SceneNode* m_node; /* Scene node */
  };

  /**
   * Animates a SceneNode's scale.
   */
  class SceneNodeScaleAnimator : public Animator<Vector3f>
  {
  public:
    /**
     * Creates an animator for a SceneNode.
     */
    SceneNodeScaleAnimator(SceneNode* node /**< Scene node */) :
      m_node(node),
      m_lastScale(m_node->getScale())
    {
    }

    // Override
    void apply(Vector3f const& scale)
    {
      if (scale != m_lastScale)
      {
        m_node->setScale(scale);
        m_lastScale = scale;
      }
    }

  private:
    SceneNode* m_node; /* Scene node */
    Vector3f m_lastScale;
  };

} // namespace a3m

#endif // A3M_SCENE_NODE_ANIMATORS_H
