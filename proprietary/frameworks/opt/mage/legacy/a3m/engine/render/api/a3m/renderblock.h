/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 *  Concrete render block class
 */
#pragma once
#ifndef A3M_RENDERBLOCK_H
#define A3M_RENDERBLOCK_H

#include <a3m/renderblockbase.h> /* for this class's base class              */
#include <a3m/flags.h>           /* Flags passed to renderer                 */
#include <a3m/background.h>      /* Used to clear viewport before rendering  */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \defgroup a3mSimpleRenderBlock A3M Simple Render Block
   * \ingroup  a3mRefRender
   *
   * @{
   */

  class Camera;
  class Renderer;
  class SceneNode;
  class RenderTarget;

  /**
   * RenderBlock class
   *
   * RenderBlock objects can be used to associate a scene graph, camera and
   * renderer. They can then be grouped together (using \ref RenderBlockGroup)
   * to render an application view which is composed of several scenes
   * composited together.
   *
   * For example, an application might have a 3D model viewed from a camera
   * (using a perspective projection) and a collection of textured rectangles
   * (billboards) rendered using an orthographic projection.
   *
   * in application class definition:
   *
   * \code
   *  SharedPtr< RenderBlockGroup > m_renderBlockGroup;
   *  SharedPtr< RenderBlock >      m_rectBlock;
   *  SharedPtr< RenderBlock >      m_sceneBlock;
   * \endcode
   *
   * in application class initialize method (e.g. constructor):
   *
   * \code
   *  // Create RenderBlock to draw 3D scene
   *  m_sceneBlock.reset( new RenderBlock( m_renderer, m_scene, m_camera ) );
   *
   *  // Create transparent RenderBlock to draw billboards
   *  m_rectBlock.reset( new RenderBlock( m_renderer, m_billboards, m_orthoCamera ) );
   *  m_rectBlock->setColourClear( A3M_FALSE );
   *
   *  // Create a RenderBlockGroup and add the two RenderBlocks
   *  m_renderBlockGroup.reset( new RenderBlockGroup );
   *  m_renderBlockGroup->addBlock( m_sceneBlock );
   *  m_renderBlockGroup->addBlock( m_rectBlock );
   * \endcode
   *
   * in application class draw method:
   *
   * \code
   *  // Draw all layers for view
   *  m_renderBlockGroup->render();
   * \endcode
   *
   * You can also use a RenderBlockGroup to create an effect which requires
   * first rendering to a texture, then using that texture in a subsequent
   * stage.
   *
   * \code
   *  // create a texture the same size as the back buffer and a render
   *  // target which uses it
   *  Texture2D::Ptr texture =
   *    m_pool->texture2DCache()->createFromBackbuffer(Texture::RGB);
   *  RenderTarget::Ptr target ( new RenderTarget( texture ) );
   *  m_sceneBlock->setRenderTarget( target );
   *
   *  // create an appearance using file "post.ap" which references a
   *  // shader program to do some post-processing
   *  Appearance::Ptr quadAppearance( new Appearance );
   *  quadAppearance->apply( *m_pool, "post.ap" );
   *
   *  // apply the same texture which is bound to the first render block
   *  quadAppearance->setProperty( "M_TEXTURE", texture );
   *
   *  // apply the appearance to a new quad object
   *  a3m::Solid::Ptr quad = createSquare( *m_pool, Vector2f( 1, 1 ) );
   *  quad->setAppearance( quadAppearance );
   *
   *  // QuadRenderer just renders the supplied node ignoring the camera
   *  RenderBlock::Ptr quadBlock(
   *    new RenderBlock( Renderer::Ptr( new QuadRenderer ), quad, m_camera ) );
   *
   *  m_renderBlockGroup.reset( new RenderBlockGroup );
   *  m_renderBlockGroup->addBlock( m_sceneBlock );
   *  m_renderBlockGroup->addBlock( quadBlock );
   * \endcode
   */
  class RenderBlock : public RenderBlockBase
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< RenderBlock > Ptr;
    /** Constructor.
     */
    RenderBlock(
      SharedPtr< Renderer  > const& renderer,   /**< Renderer for this block*/
      SharedPtr< SceneNode > const& sceneNode,  /**< Scene to render        */
      SharedPtr< Camera > const& camera         /**< Camera to use          */);

    /** Destructor.
     */
    ~RenderBlock();

    /** Set camera.
     * Set the camera used for rendering.
     */
    void setCamera( SharedPtr< Camera > const& camera /**< Camera to use */ );

    /** Set render target.
     * Set the destination render target. If no render target is set the scene
     * will be rendered to the back buffer.
     */
    void setRenderTarget( SharedPtr< RenderTarget > const& target
                          /**< Destination render target */ );

    /** Set flags for renderer.
     * Flags will be passed to the renderer to specify which nodes are rendered.
     */
    void setRenderFlags(
      FlagMask const& renderFlags,   /**< Mask for specifying which scene nodes
                                          to include in render */
      FlagMask const& recursiveFlags /**< Mask specifying whether flags affect
                                          the scene graph recursively when in
                                          their non-default state */ );

    /** Set viewport.
     * If no viewport is set, the viewport will be the viewport current when the
     * render block was created (usually the whole screen).
     */
    void setViewport( A3M_INT32 left,   /**< left edge of viewport in pixels  */
                      A3M_INT32 bottom, /**< bottom edge of viewport in pixels*/
                      A3M_INT32 width,  /**< width of viewport in pixels      */
                      A3M_INT32 height  /**< height of viewport in pixels   */);

    /** Set the background colour.
     * This is only used if the background clear mask is true (see
     * setColourClear).
     *
     * By default the background colour is opaque black.
     */
    void setBackgroundColour( Colour4f const& background /**< background */ );

    /** Set colour clear flag.
     * Call this method with A3M_TRUE if you want the colour buffer to be
     * cleared before rendering. If you set the flag to A3M_FALSE the colour
     * buffer will retain the values of the last RenderBlock to be rendered.
     *
     * By default the background is cleared to black.
     */
    void setColourClear( A3M_BOOL clear
                         /**< A3M_TRUE to clear colour buffer */ );

    /** Set depth clear flag.
     * Call this method with A3M_TRUE if you want the depth buffer to be cleared
     * before rendering. If you set the flag to A3M_FALSE the depth buffer will
     * retain the values of the last RenderBlock to be rendered.
     *
     * By default the depth buffer is cleared before rendering.
     */
    void setDepthClear( A3M_BOOL clear /**< A3M_TRUE to clear depth buffer */ );

    /** Render this block.
     */
    virtual void render();

    /** Update the time for shader based effects.
     */
    virtual void update( A3M_FLOAT timeInSeconds
                         /**< Time (mod 60) to set in uniform u_time */ );

    /** Set stereoscopic parameters for camera
     */
    virtual void setStereo(
      A3M_FLOAT zFocal /**< distance camera to focal plane in world units */,
      A3M_FLOAT eyeSep /**< distance between camera positions in world units */
    );

  private:
    SharedPtr< Renderer >     m_renderer;
    SharedPtr< SceneNode >    m_sceneNode;
    SharedPtr< Camera >       m_camera;
    SharedPtr< RenderTarget > m_renderTarget;

    FlagMask m_flags;           // Flags passed to renderer
    FlagMask m_recursiveFlags; // Recursive flags passed to renderer

    Background m_background; // background

    A3M_INT32 m_viewLeft;   // left edge of viewport in pixels
    A3M_INT32 m_viewBottom; // bottom edge of viewport in pixels
    A3M_INT32 m_viewWidth;  // width of viewport in pixels
    A3M_INT32 m_viewHeight; // height of viewport in pixels
  };
  /** @} */

} /* namespace a3m */

#endif /* A3M_RENDERBLOCK_H */
