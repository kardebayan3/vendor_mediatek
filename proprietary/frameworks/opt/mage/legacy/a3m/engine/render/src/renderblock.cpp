/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * A3M Baseline render block
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/renderblock.h>       /* Class declaration */
#include <a3m/rendercontext.h>     /* For RenderContext */
#include <a3m/renderer.h>          /* Call Renderer::render */
#include <a3m/camera.h>            /* class declaration needed by destructor */
#include <a3m/scenenode.h>         /* class declaration needed by destructor */
#include <a3m/renderdevice.h>      /* For clear() and setViewport()          */
#include <a3m/rendertarget.h>      /* For RenderTarget::enable()             */

namespace a3m
{
  RenderBlock::RenderBlock(
    SharedPtr< Renderer > const& renderer,
    SharedPtr< SceneNode > const& sceneNode,
    SharedPtr< Camera > const& camera )
    :   m_renderer( renderer ),
        m_sceneNode( sceneNode ),
        m_camera( camera ),
        m_viewLeft( renderer->getRenderContext()->getViewportLeft() ),
        m_viewBottom( renderer->getRenderContext()->getViewportBottom() ),
        m_viewWidth( renderer->getRenderContext()->getViewportWidth() ),
        m_viewHeight( renderer->getRenderContext()->getViewportHeight() )
  {
  }

  /*
   * Empty destructor declared out of line so camera etc. don't need to be
   * included in header (SharedPtr destructors need to see whole class)
   */
  RenderBlock::~RenderBlock()
  {
  }

  void RenderBlock::setCamera( SharedPtr< Camera > const& camera )
  {
    m_camera = camera;
  }

  void RenderBlock::setRenderTarget(
    SharedPtr< RenderTarget > const& target )
  {
    m_renderTarget = target;
  }

  /*
  * Set flags for renderer.
  */
  void RenderBlock::setRenderFlags( FlagMask const& renderFlags,
                                    FlagMask const& recursiveFlags )
  {
    m_flags = renderFlags;
    m_recursiveFlags = recursiveFlags;
  }


  /*
   * Set viewport.
   */
  void RenderBlock::setViewport( A3M_INT32 left,
                                 A3M_INT32 bottom,
                                 A3M_INT32 width,
                                 A3M_INT32 height )
  {
    m_viewLeft = left;
    m_viewBottom = bottom;
    m_viewWidth = width;
    m_viewHeight = height;
  }

  /*
   * Set the background colour.
   */
  void RenderBlock::setBackgroundColour( Colour4f const& background )
  {
    m_background.setColour( background );
  }

  /*
   * Set colour clear flag.
   */
  void RenderBlock::setColourClear( A3M_BOOL clear )
  {
    m_background.setColourMask( clear, clear, clear, clear );
  }

  /*
   * Set depth clear flag.
   */
  void RenderBlock::setDepthClear( A3M_BOOL clear )
  {
    m_background.setDepthMask( clear );
  }

  /*
   * Render this block
   */
  void RenderBlock::render()
  {
    if( m_renderTarget )
    {
      m_renderTarget->enable();
    }

    m_renderer->getRenderContext()->setViewport(
      m_viewLeft, m_viewBottom, m_viewWidth, m_viewHeight );

    m_background.enable( *m_renderer->getRenderContext() );

    // We might just have a background, so it is not an error if we are missing
    // one or more of these.
    if( m_renderer && m_sceneNode )
    {
      m_renderer->render( m_camera.get(), *m_sceneNode,
                          m_flags, m_recursiveFlags );
    }

    if( m_renderTarget )
    {
      m_renderTarget->disable();
    }
  }

  /*
   * Update this block
   */
  void RenderBlock::update( A3M_FLOAT timeInSeconds )
  {
    if( m_renderer )
    {
      m_renderer->update(timeInSeconds);
    }
  }

  /*
   * Set stereoscopic parameters for camera
   */
  void RenderBlock::setStereo( A3M_FLOAT zFocal, A3M_FLOAT eyeSep )
  {
    if( m_camera )
    {
      m_camera->setStereo(zFocal, eyeSep);
    }
  }
} /* namespace a3m */
