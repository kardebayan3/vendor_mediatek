/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Generic type definitions
 */

#pragma once
#ifndef A3M_BASE_TYPES_H
#define A3M_BASE_TYPES_H

/**
 * \defgroup a3mTypesIntrinsic Wrappers for intrinsic types
 * \ingroup  a3mRefTypes
 *
 * A3M_INT8, A3M_UINT32, A3M_BOOL, A3M_FLOAT etc.
 *
 * These types are defined for use within the A3M and by A3M API users.
 *
 * @{
 */

/** Boolean type */
typedef bool               A3M_BOOL;

/** Boolean false */
const A3M_BOOL A3M_FALSE = false;

/** Boolean true */
const A3M_BOOL A3M_TRUE  = true;

/** Byte type used to reference raw memory where signed/unsigned has no
    meaning */
typedef unsigned char      A3M_BYTE;

/** 8-bit character type used in character string manipulation */
typedef char               A3M_CHAR8;

/** Signed 8-bit integer type used to represent a signed integer number for
    mathematical operations */
typedef signed char        A3M_INT8;

/** Unsigned 8-bit integer type used to represent an unsigned integer number
    for mathematical operations */
typedef unsigned char      A3M_UINT8;

/** Signed 16-bit integer */
typedef signed short       A3M_INT16;

/** Unsigned 16-bit integer */
typedef unsigned short     A3M_UINT16;

/** Signed 32-bit integer */
typedef signed int         A3M_INT32;

/** Unsigned 32-bit integer */
typedef unsigned int       A3M_UINT32;

/** Signed 64-bit integer */
typedef signed long long   A3M_INT64;

/** Unsigned 64-bit integer */
typedef unsigned long long A3M_UINT64;

/** 32-bit floating point number */
typedef float              A3M_FLOAT;

/** 64-bit floating point number */
typedef double             A3M_DOUBLE;

/** @} */


/******************************************************************************
 * Macros
 ******************************************************************************/
/** \ingroup a3mIntUtil
 *  Macro to remove compiler warnings for unused function parameters */
#define A3M_PARAM_NOT_USED(p)   ((void)(p))

#endif
