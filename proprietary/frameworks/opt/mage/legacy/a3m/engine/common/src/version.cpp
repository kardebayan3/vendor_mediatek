/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Version class implementation
 *
 */

#include <a3m/version.h>

namespace a3m
{

  Version::Version(
    A3M_INT32 major,
    A3M_INT32 minor,
    A3M_INT32 patch,
    A3M_CHAR8 const* extra) :
    m_major(major),
    m_minor(minor),
    m_patch(patch),
    m_extra(extra)
  {
  }

  A3M_INT32 Version::getMajor() const
  {
    return m_major;
  }

  A3M_INT32 Version::getMinor() const
  {
    return m_minor;
  }

  A3M_INT32 Version::getPatch() const
  {
    return m_patch;
  }

  A3M_CHAR8 const* Version::getExtra() const
  {
    return m_extra.c_str();
  }

  A3M_BOOL operator==(Version const& lhs, Version const& rhs)
  {
    return
      lhs.getPatch() == rhs.getPatch() &&
      lhs.getMinor() == rhs.getMinor() &&
      lhs.getMajor() == rhs.getMajor();
  }

  A3M_BOOL operator!=(Version const& lhs, Version const& rhs)
  {
    return !(lhs == rhs);
  }

  A3M_BOOL operator<(Version const& lhs, Version const& rhs)
  {
    if (lhs.getMajor() == rhs.getMajor())
    {
      if (lhs.getMinor() == rhs.getMinor())
      {
        return lhs.getPatch() < rhs.getPatch();
      }
      else
      {
        return lhs.getMinor() < rhs.getMinor();
      }
    }
    else
    {
      return lhs.getMajor() < rhs.getMajor();
    }
  }

  A3M_BOOL operator>(Version const& lhs, Version const& rhs)
  {
    return !(lhs < rhs || lhs == rhs);
  }

  A3M_BOOL operator<=(Version const& lhs, Version const& rhs)
  {
    return !(lhs > rhs);
  }

  A3M_BOOL operator>=(Version const& lhs, Version const& rhs)
  {
    return !(lhs < rhs);
  }

} // namespace a3m
