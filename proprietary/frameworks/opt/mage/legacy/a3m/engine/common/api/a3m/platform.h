/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Preprocessor platform check macros.
 *
 */
#pragma once
#ifndef A3M_PLATFORM_H
#define A3M_PLATFORM_H

/******************************************************************************
 * Include Files
 ******************************************************************************/

/** Indicates a platform that is unsupported by A3M. */
#define A3M_UNSUPPORTED_PLATFORM 0
/** Used for all Microsoft Windows operating systems. */
#define A3M_WINDOWS 1
/** Used for all versions of Google Android. */
#define A3M_ANDROID 2
/** Used for all Linux operating systems. */
#define A3M_LINUX 3

#if defined _WIN32 || defined WIN32
#define A3M_PLATFORM A3M_WINDOWS
#elif defined __ANDROID__
#define A3M_PLATFORM A3M_ANDROID
#elif defined __linux__
#define A3M_PLATFORM A3M_LINUX
#else
// Only the last occurrence is parsed by doxygen.
/** Set to one of the defined platform values depending on the platform on
 *  which the code is being compiled.
 */
#define A3M_PLATFORM A3M_UNSUPPORTED_PLATFORM
#endif

#endif /* A3M_PLATFORM_H */
