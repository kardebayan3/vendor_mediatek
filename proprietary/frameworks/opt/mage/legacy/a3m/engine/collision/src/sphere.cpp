/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * Sphere collision shape class
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <cmath>                   /* for std::abs() and std::sqrt()         */
#include <a3m/sphere.h>            /* This module's header                   */

namespace
{

  A3M_FLOAT const RADIUS2 = 0.25f;

} // namespace

namespace a3m
{

  A3M_BOOL Sphere::localRaycast(
    A3M_FLOAT& distance,
    Vector3f& normal,
    Ray const& ray)
  {
    // Get the centre of the sphere and the direction of the ray.  To simplify
    // the calculation, the ray is assumed to start at the origin, and the
    // sphere is given a relative offset.
    Vector3f centre = -ray.getPosition();
    Vector3f direction = ray.getDirection();

    // Calculate the distance from the ray position to the point on the ray
    // which is nearest to the centre of the sphere.
    A3M_FLOAT d = dot(centre, direction);

    // Calculate the square of the distance from this point to where the ray
    // and sphere intersect.  This value is the result of a simple application
    // of Pythagorus' Theorem.
    A3M_FLOAT delta2 = d * d + RADIUS2 - lengthSquared(centre);

    // If the square is negative, there is no intersection.
    if (delta2 < 0.0f)
    {
      return A3M_FALSE;
    }

    // Determine the nearest intersection point.
    A3M_FLOAT delta = std::sqrt(delta2);
    A3M_FLOAT distanceA = d + delta;
    A3M_FLOAT distanceB = d - delta;

    if (std::abs(distanceA) < std::abs(distanceB))
    {
      distance = distanceA;
    }
    else
    {
      distance = distanceB;
    }

    // Calculate the point of intersection, and from that the normal of the
    // sphere at that point.
    Vector3f point = distance * direction;
    normal = normalize(point - centre);

    return A3M_TRUE;
  }

} // namespace a3m
