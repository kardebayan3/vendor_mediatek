/******************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ******************************************************************************/

package com.mediatek.a3m.autotest;

import android.app.Activity;
import android.os.Bundle;

public class Main extends Activity {
    static {
        System.loadLibrary("a3m");
        System.loadLibrary("a3mautotest");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public boolean run() {
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.surface_view);
        return surfaceView.run();
    }
}
