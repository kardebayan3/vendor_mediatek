@echo off
REM Generates "memory files" for the autotest
python ../buildtools/genmemfiles.py -n autotest -i testassets -o testassets -v
