/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M SceneNode unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/scenenode.h>              /* for SceneNode                      */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mSceneNodeTest : public AutotestTest
  {
    SceneNode::Ptr node;

    A3mSceneNodeTest() :
      node(new SceneNode())
    {
    }
  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mSceneNodeTest, InitialState)
{
  EXPECT_FALSE(node->getLocalMirrored());
  EXPECT_FALSE(node->getWorldMirrored());

  EXPECT_EQ(Vector3f::ZERO, node->getPosition());
  EXPECT_EQ(Quaternionf::IDENTITY, node->getRotation());
  EXPECT_EQ(Vector3f(1.0f, 1.0f, 1.0f), node->getScale());

  EXPECT_FALSE(node->getParent());
  EXPECT_EQ(0U, node->getChildCount());
}

/*
 * Test setting and then destroying a parent node.
 */
TEST_F(A3mSceneNodeTest, ParentDestroy)
{
  SceneNode::Ptr parent(new SceneNode());
  node->setParent(parent);
  EXPECT_EQ(parent, node->getParent());
  ASSERT_EQ(1U, parent->getChildCount());
  EXPECT_EQ(node, parent->getChild(0));

  parent.reset();
  EXPECT_FALSE(node->getParent());
}

/*
 * Test setting and then removing a parent node.
 */
TEST_F(A3mSceneNodeTest, ParentRemove)
{
  SceneNode::Ptr parent(new SceneNode());
  node->setParent(parent);
  EXPECT_EQ(parent, node->getParent());
  ASSERT_EQ(1U, parent->getChildCount());
  EXPECT_EQ(node, parent->getChild(0));

  node->setParent(SceneNode::Ptr());
  EXPECT_FALSE(node->getParent());
  EXPECT_EQ(0U, parent->getChildCount());
}

/*
 * Test setting and then replacing a parent node.
 */
TEST_F(A3mSceneNodeTest, ParentReplace)
{
  SceneNode::Ptr parentA(new SceneNode());
  SceneNode::Ptr parentB(new SceneNode());

  node->setParent(parentA);
  EXPECT_EQ(parentA, node->getParent());
  ASSERT_EQ(1U, parentA->getChildCount());
  EXPECT_EQ(node, parentA->getChild(0));

  node->setParent(parentB);
  EXPECT_EQ(parentB, node->getParent());
  ASSERT_EQ(1U, parentB->getChildCount());
  EXPECT_EQ(node, parentB->getChild(0));
  EXPECT_EQ(0U, parentA->getChildCount());

  node->setParent(SceneNode::Ptr());
  EXPECT_FALSE(node->getParent());
  EXPECT_EQ(0U, parentB->getChildCount());
}

/*
 * Test adding two children to a node and removing them.
 */
TEST_F(A3mSceneNodeTest, TwoChildren)
{
  SceneNode::Ptr childA(new SceneNode());
  SceneNode::Ptr childB(new SceneNode());

  childA->setParent(node);
  childB->setParent(node);
  EXPECT_EQ(node, childA->getParent());
  EXPECT_EQ(node, childB->getParent());
  ASSERT_EQ(2U, node->getChildCount());
  EXPECT_EQ(childA, node->getChild(0));
  EXPECT_EQ(childB, node->getChild(1));

  childA->setParent(SceneNode::Ptr());
  EXPECT_FALSE(childA->getParent());
  EXPECT_EQ(node, childB->getParent());
  ASSERT_EQ(1U, node->getChildCount());
  EXPECT_EQ(childB, node->getChild(0));

  childB->setParent(SceneNode::Ptr());
  EXPECT_FALSE(childB->getParent());
  EXPECT_EQ(0U, node->getChildCount());
}

/*
 * Test moving a scene node up the hierarchy.
 */
TEST_F(A3mSceneNodeTest, ParentAscend)
{
  SceneNode::Ptr parentA(new SceneNode());
  SceneNode::Ptr parentB(new SceneNode());

  parentB->setParent(parentA);
  node->setParent(parentB);

  EXPECT_EQ(parentA, parentB->getParent());
  EXPECT_EQ(parentB, node->getParent());
  ASSERT_EQ(1U, parentA->getChildCount());
  EXPECT_EQ(parentB, parentA->getChild(0));
  ASSERT_EQ(1U, parentB->getChildCount());
  EXPECT_EQ(node, parentB->getChild(0));

  node->setParent(parentA);
  EXPECT_EQ(parentA, parentB->getParent());
  EXPECT_EQ(parentA, node->getParent());
  ASSERT_EQ(2U, parentA->getChildCount());
  EXPECT_EQ(parentB, parentA->getChild(0));
  EXPECT_EQ(node, parentA->getChild(1));
  ASSERT_EQ(0U, parentB->getChildCount());
}

/*
 * Test that cycles are forbidden.
 */
TEST_F(A3mSceneNodeTest, Cycles)
{
  SceneNode::Ptr nodeA(new SceneNode());
  SceneNode::Ptr nodeB(new SceneNode());
  SceneNode::Ptr nodeC(new SceneNode());

  nodeB->setParent(nodeA);
  nodeC->setParent(nodeB);
  nodeA->setParent(nodeC);

  EXPECT_EQ(nodeA, nodeB->getParent());
  EXPECT_EQ(nodeB, nodeC->getParent());
  EXPECT_EQ(SceneNode::Ptr(), nodeA->getParent());
}

/*
 * Test setting the parent twice.
 */
TEST_F(A3mSceneNodeTest, SetParentTwice)
{
  SceneNode::Ptr nodeA(new SceneNode());
  SceneNode::Ptr nodeB(new SceneNode());

  nodeB->setParent(nodeA);
  nodeB->setParent(nodeA);

  EXPECT_EQ(nodeA, nodeB->getParent());
  EXPECT_EQ(SceneNode::Ptr(), nodeA->getParent());
}
