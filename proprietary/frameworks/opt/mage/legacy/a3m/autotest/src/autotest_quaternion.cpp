/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */


/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <cstdlib>                      /* for std::rand()                    */
#include <a3m/quaternion.h>             /* for Quaternionf                    */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */
using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mQuaternionTest : public AutotestTest
  {
  };

  A3M_FLOAT random(A3M_FLOAT min, A3M_FLOAT max)
  {
    return static_cast<A3M_FLOAT>(rand()) / static_cast<A3M_FLOAT>(RAND_MAX) *
           (max - min) + min;
  }

} // namespace

/*
 * Test constructor.
 */
TEST_F(A3mQuaternionTest, DefaultConstructor)
{
  Quaternionf q;
  EXPECT_FLOAT_EQ(1.0f, q.a);
  EXPECT_FLOAT_EQ(0.0f, q.b);
  EXPECT_FLOAT_EQ(0.0f, q.c);
  EXPECT_FLOAT_EQ(0.0f, q.d);
}

/*
 * Test constructor.
 */
TEST_F(A3mQuaternionTest, Constructor)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  EXPECT_FLOAT_EQ(1.0f, q.a);
  EXPECT_FLOAT_EQ(2.0f, q.b);
  EXPECT_FLOAT_EQ(3.0f, q.c);
  EXPECT_FLOAT_EQ(4.0f, q.d);
}

/*
 * Test indexing.
 */
TEST_F(A3mQuaternionTest, IndexOperator)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  EXPECT_FLOAT_EQ(1.0f, q[0]);
  EXPECT_FLOAT_EQ(2.0f, q[1]);
  EXPECT_FLOAT_EQ(3.0f, q[2]);
  EXPECT_FLOAT_EQ(4.0f, q[3]);
}

/*
 * Test const indexing.
 */
TEST_F(A3mQuaternionTest, ConstIndexOperator)
{
  Quaternionf const q(1.0f, 2.0f, 3.0f, 4.0f);
  EXPECT_FLOAT_EQ(1.0f, q[0]);
  EXPECT_FLOAT_EQ(2.0f, q[1]);
  EXPECT_FLOAT_EQ(3.0f, q[2]);
  EXPECT_FLOAT_EQ(4.0f, q[3]);
}

/*
 * Test axis-angle constructor.
 */
TEST_F(A3mQuaternionTest, AxisAngleConstructor)
{
  // For axis = (x, y, z) and angle = a:
  // q = (cos(a), sin(a / 2) * x, sin(a / 2) * y, sin(a / 2) * z)
  Vector3f v(6.0f, 5.0f, 2.0f);
  Anglef a = radians(1.845f);
  Quaternionf q(v, a);
  EXPECT_FLOAT_EQ(cos(a / 2.0f), q.a);
  EXPECT_FLOAT_EQ(sin(a / 2.0f) * v.x, q.b);
  EXPECT_FLOAT_EQ(sin(a / 2.0f) * v.y, q.c);
  EXPECT_FLOAT_EQ(sin(a / 2.0f) * v.z, q.d);
}

/*
 * Test equality.
 */
TEST_F(A3mQuaternionTest, Equal)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  EXPECT_TRUE(p == q);
  EXPECT_FALSE(p != q);
}

/*
 * Test inequality.
 */
TEST_F(A3mQuaternionTest, NotEqual)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf p(1.0f, 5.0f, 3.0f, 4.0f);
  EXPECT_TRUE(p != q);
  EXPECT_FALSE(p == q);
}

/*
 * Test copy constructor.
 */
TEST_F(A3mQuaternionTest, CopyConstructor)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf p(q);
  EXPECT_TRUE(p == q);
}

/*
 * Test copy assignment.
 */
TEST_F(A3mQuaternionTest, CopyAssignment)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf p(9.0f, 8.0f, 7.0f, 6.0f);
  p = q;
  EXPECT_TRUE(p == q);
}

/*
 * Test unary negation.
 */
TEST_F(A3mQuaternionTest, Negation)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf p(-q);
  EXPECT_FLOAT_EQ(p.a, -q.a);
  EXPECT_FLOAT_EQ(p.b, -q.b);
  EXPECT_FLOAT_EQ(p.c, -q.c);
  EXPECT_FLOAT_EQ(p.d, -q.d);
}

/*
 * Test addition.
 */
TEST_F(A3mQuaternionTest, Addition)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  Quaternionf r(p + q);
  Quaternionf s(q + p);
  EXPECT_FLOAT_EQ(r.a, p.a + q.a);
  EXPECT_FLOAT_EQ(r.b, p.b + q.b);
  EXPECT_FLOAT_EQ(r.c, p.c + q.c);
  EXPECT_FLOAT_EQ(r.d, p.d + q.d);
  EXPECT_EQ(r, s);
}

/*
 * Test subtraction.
 */
TEST_F(A3mQuaternionTest, Subtraction)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  Quaternionf r(p - q);
  Quaternionf s(q - p);
  EXPECT_FLOAT_EQ(r.a, p.a - q.a);
  EXPECT_FLOAT_EQ(r.b, p.b - q.b);
  EXPECT_FLOAT_EQ(r.c, p.c - q.c);
  EXPECT_FLOAT_EQ(r.d, p.d - q.d);
  EXPECT_EQ(r, -s);
}

/*
 * Test multiplication.
 */
TEST_F(A3mQuaternionTest, Multiplication)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  Quaternionf r = p * q;
  EXPECT_FLOAT_EQ(r.a, p.a * q.a - p.b * q.b - p.c * q.c - p.d * q.d);
  EXPECT_FLOAT_EQ(r.b, p.a * q.b + p.b * q.a + p.c * q.d - p.d * q.c);
  EXPECT_FLOAT_EQ(r.c, p.a * q.c - p.b * q.d + p.c * q.a + p.d * q.b);
  EXPECT_FLOAT_EQ(r.d, p.a * q.d + p.b * q.c - p.c * q.b + p.d * q.a);
}

/*
 * Test scalar multiplication.
 */
TEST_F(A3mQuaternionTest, ScalarMultiplication)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  A3M_FLOAT k = 3.0f;
  Quaternionf q(p * k);
  Quaternionf r(k * p);
  EXPECT_FLOAT_EQ(q.a, p.a * k);
  EXPECT_FLOAT_EQ(q.b, p.b * k);
  EXPECT_FLOAT_EQ(q.c, p.c * k);
  EXPECT_FLOAT_EQ(q.d, p.d * k);
  EXPECT_EQ(q, r);
}

/*
 * Test assignment addition.
 */
TEST_F(A3mQuaternionTest, AssignmentAddition)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  Quaternionf r(p);
  r += q;
  EXPECT_EQ(r, p + q);
}

/*
 * Test assignment subtraction.
 */
TEST_F(A3mQuaternionTest, AssignmentSubtraction)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  Quaternionf r(p);
  r -= q;
  EXPECT_EQ(r, p - q);
}

/*
 * Test assignment multiplication.
 */
TEST_F(A3mQuaternionTest, AssignmentMultiplication)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  Quaternionf r(p);
  r *= q;
  EXPECT_EQ(r, p * q);
}

/*
 * Test assignment scalar multiplication.
 */
TEST_F(A3mQuaternionTest, AssignmentScalarMultiplication)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf r(p);
  A3M_FLOAT k = 3.0f;
  r *= k;
  EXPECT_EQ(r, p * k);
}

/*
 * Test assignment scalar division.
 */
TEST_F(A3mQuaternionTest, AssignmentScalarDivision)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf r(p);
  A3M_FLOAT k = 3.0f;
  r /= k;
  EXPECT_EQ(r, p / k);
}

/*
 * Test absolute value.
 */
TEST_F(A3mQuaternionTest, Abs)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  float expected = std::sqrt(q.a * q.a + q.b * q.b + q.c * q.c + q.d * q.d);
  EXPECT_FLOAT_EQ(expected, abs(q));
}

/*
 * Test abs squared.
 */
TEST_F(A3mQuaternionTest, Abs2)
{
  Quaternionf q(1.0f, 2.0f, 3.0f, 4.0f);
  float expected = q.a * q.a + q.b * q.b + q.c * q.c + q.d * q.d;
  EXPECT_FLOAT_EQ(expected, abs2(q));
}

/*
 * Test dot product.
 */
TEST_F(A3mQuaternionTest, Dot)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(9.0f, 8.0f, 7.0f, 6.0f);
  float expected = p.a * q.a + p.b * q.b + p.c * q.c + p.d * q.d;
  EXPECT_FLOAT_EQ(expected, dot(p, q));
}

/*
 * Test conjugate.
 */
TEST_F(A3mQuaternionTest, Conjugate)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(p);
  q.b *= -1;
  q.c *= -1;
  q.d *= -1;
  EXPECT_EQ(conjugate(p), q);
}

/*
 * Test normalize.
 */
TEST_F(A3mQuaternionTest, Normalize)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(p / abs(p));
  EXPECT_EQ(normalize(p), q);
}

/*
 * Test inverse.
 */
TEST_F(A3mQuaternionTest, Inverse)
{
  Quaternionf p(1.0f, 2.0f, 3.0f, 4.0f);
  Quaternionf q(conjugate(p) / abs2(p));
  EXPECT_EQ(inverse(p), q);
}

/*
 * Test conversion from axis-angle.
 */
TEST_F(A3mQuaternionTest, FromAxisAngle)
{
  Vector3f axis(1.0f, 2.0f, 3.0f);
  Anglef angle = radians(4.0f);
  Quaternionf p(axis, angle);
  Quaternionf q = toQuaternion(axis, angle);
  EXPECT_EQ(p, q);
}

/*
 * Test conversion to axis-angle.
 */
TEST_F(A3mQuaternionTest, ToAxisAngle)
{
  // Seed so we get the same test each time.
  srand(0);

  for (int i = 0; i < 100000; ++i)
  {
    Vector3f axis(random(-1, 1), random(-1, 1), random(-1, 1));
    axis = normalize(normalize(axis));
    Anglef angle = radians(random(-3.14159f, 3.14159f));

    // Just invert the operation and do an approximate check
    Quaternionf p(axis, angle);
    Vector3f axis2 = getAxis(p);
    Anglef angle2 = getAngle(p);

    // Try to detect if the axis-angle is the opposite sign
    if (abs(angle - angle2) > angle)
    {
      angle2 *= -1.0f;
      axis2 *= -1.0f;
    }

    // Axis determination will break down at small angles (to be expected)
    if (abs(angle) > radians(0.01f))
    {
      EXPECT_NEAR(axis.x, axis2.x, 0.01f);
      EXPECT_NEAR(axis.y, axis2.y, 0.01f);
      EXPECT_NEAR(axis.z, axis2.z, 0.01f);
      EXPECT_NEAR(getRadians(angle), getRadians(angle2), 0.01f);
    }
  }
}

/*
 * Test conversion to matrix and back.
 */
TEST_F(A3mQuaternionTest, ToAndFromMatrix)
{
  // Conversion to matrix and back
  srand(0);

  for (int i = 0; i < 100000; ++i)
  {
    Vector3f axis(random(-1, 1), random(-1, 1), random(-1, 1));
    axis = normalize(axis);
    Anglef angle = radians(random(-3.14159f, 3.14159f));

    // Just invert the operation and do an approximate check
    // Since the two algorithms are very different, if we get back to what we
    // had before then we can be confident that it works.
    Quaternionf q = Quaternionf(axis, angle);
    Matrix4f m = toMatrix4(q);
    Quaternionf p = toQuaternion(m);

    // 0.06 seems to be an acceptable level of error
    EXPECT_NEAR(p.a, q.a, 0.06f);
    EXPECT_NEAR(p.b, q.b, 0.06f);
    EXPECT_NEAR(p.c, q.c, 0.06f);
    EXPECT_NEAR(p.d, q.d, 0.06f);
  }
}

/*
 * Test rotation from one vector to another.
 */
TEST_F(A3mQuaternionTest, VectorToVectorRotation)
{
  Quaternionf q1 = toQuaternion(Vector3f::X_AXIS, Vector3f::Y_AXIS);
  Quaternionf p1 = toQuaternion(Vector3f::Z_AXIS, degrees(90.0f));
  EXPECT_FLOAT_EQ(q1.a, p1.a);
  EXPECT_FLOAT_EQ(q1.b, p1.b);
  EXPECT_FLOAT_EQ(q1.c, p1.c);
  EXPECT_FLOAT_EQ(q1.d, p1.d);

  Quaternionf q2 = toQuaternion(Vector3f::X_AXIS, -Vector3f::X_AXIS,
                                normalize(Vector3f(1.0f, 1.0f, 0.0f)));
  Quaternionf p2 = toQuaternion(Vector3f::Y_AXIS, degrees(180.0f));
  EXPECT_FLOAT_EQ(q2.a, p2.a);
  EXPECT_FLOAT_EQ(q2.b, p2.b);
  EXPECT_FLOAT_EQ(q2.c, p2.c);
  EXPECT_FLOAT_EQ(q2.d, p2.d);
}
