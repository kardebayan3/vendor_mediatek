/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Light unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/light.h>                  /* for Light                          */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  Colour4f TEST_COLOUR(0.5f, 0.5f, 0.5f, 0.2f);
  Colour4f WHITE(1.0f, 1.0f, 1.0f, 1.0f);
  Colour4f BLACK(0.0f, 0.0f, 0.0f, 1.0f);

  /*
   * Test fixture.
   */
  struct A3mLightTest : public AutotestTest
  {
    Light::Ptr light;

    A3mLightTest() :
      light(new Light())
    {
    }
  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mLightTest, InitialState)
{
  EXPECT_TRUE(light->getActive());
  EXPECT_EQ(Light::LIGHTTYPE_OMNI, light->getLightType());

  EXPECT_EQ(WHITE, light->getColour());
  EXPECT_EQ(0.0f, light->getAmbientLevel());

  EXPECT_FLOAT_EQ(1.0f, light->getIntensity());
  EXPECT_FLOAT_EQ(1.0f, light->getAttenuationNear());
  EXPECT_FLOAT_EQ(1000.0f, light->getAttenuationFar());
  EXPECT_FLOAT_EQ(30.0f, getDegrees(light->getSpotInnerAngle()));
  EXPECT_FLOAT_EQ(45.0f, getDegrees(light->getSpotOuterAngle()));
}

/*
 * Test Light::set/getActive().
 */
TEST_F(A3mLightTest, Active)
{
  light->setActive(A3M_FALSE);
  EXPECT_EQ(A3M_FALSE, light->getActive());
}

/*
 * Test Light::set/getLightType().
 */
TEST_F(A3mLightTest, LightType)
{
  light->setLightType(Light::LIGHTTYPE_OMNI);
  EXPECT_EQ(Light::LIGHTTYPE_OMNI, light->getLightType());

  light->setLightType(Light::LIGHTTYPE_DIRECTIONAL);
  EXPECT_EQ(Light::LIGHTTYPE_DIRECTIONAL, light->getLightType());

  light->setLightType(Light::LIGHTTYPE_SPOT);
  EXPECT_EQ(Light::LIGHTTYPE_SPOT, light->getLightType());
}

/*
 * Test Light::set/getColour().
 */
TEST_F(A3mLightTest, Colour)
{
  light->setColour(TEST_COLOUR);
  EXPECT_EQ(TEST_COLOUR, light->getColour());
}

/*
 * Test Light::set/getAmbientLevel().
 */
TEST_F(A3mLightTest, AmbientLevel)
{
  light->setAmbientLevel(0.5f);
  EXPECT_EQ(0.5f, light->getAmbientLevel());
}

/*
 * Test Light::set/getIntensity().
 */
TEST_F(A3mLightTest, Intensity)
{
  light->setIntensity(0.4f);
  EXPECT_FLOAT_EQ(0.4f, light->getIntensity());

  light->setIntensity(-1.0f);
  EXPECT_FLOAT_EQ(-1.0f, light->getIntensity());

  light->setIntensity(2.1f);
  EXPECT_FLOAT_EQ(2.1f, light->getIntensity());
}

/*
 * Test Light::set/getAttenuationNear().
 */
TEST_F(A3mLightTest, AttenuationNear)
{
  light->setAttenuationNear(3.2f);
  EXPECT_FLOAT_EQ(3.2f, light->getAttenuationNear());

  light->setAttenuationNear(500.5f);
  EXPECT_FLOAT_EQ(500.5f, light->getAttenuationNear());

  light->setAttenuationNear(500000.5f);
  EXPECT_FLOAT_EQ(500000.5f, light->getAttenuationNear());

  light->setAttenuationNear(-500.5f);
  EXPECT_FLOAT_EQ(500.5f, light->getAttenuationNear());

}

/*
 * Test Light::set/getAttenuationFar().
 */
TEST_F(A3mLightTest, AttenuationFar)
{
  light->setAttenuationFar(3.2f);
  EXPECT_FLOAT_EQ(3.2f, light->getAttenuationFar());

  light->setAttenuationFar(500.5f);
  EXPECT_FLOAT_EQ(500.5f, light->getAttenuationFar());

  light->setAttenuationFar(500000.5f);
  EXPECT_FLOAT_EQ(500000.5f, light->getAttenuationFar());

  light->setAttenuationFar(-3.2f);
  EXPECT_FLOAT_EQ(3.2f, light->getAttenuationFar());
}

/*
 * Test Light::set/getSpotInnerAngle().
 */
TEST_F(A3mLightTest, SpotInnerAngle)
{
  light->setSpotInnerAngle(degrees(15.4f));
  EXPECT_FLOAT_EQ(15.4f, getDegrees(light->getSpotInnerAngle()));

  light->setSpotInnerAngle(degrees(360.0f));
  EXPECT_FLOAT_EQ(360.0f, getDegrees(light->getSpotInnerAngle()));

  light->setSpotInnerAngle(degrees(-45.0f));
  EXPECT_FLOAT_EQ(45.0f, getDegrees(light->getSpotInnerAngle()));
}

/*
 * Test Light::set/getSpotOuterAngle().
 */
TEST_F(A3mLightTest, SpotOuterAngle)
{
  light->setSpotOuterAngle(degrees(15.4f));
  EXPECT_FLOAT_EQ(15.4f, getDegrees(light->getSpotOuterAngle()));

  light->setSpotOuterAngle(degrees(360.0f));
  EXPECT_FLOAT_EQ(360.0f, getDegrees(light->getSpotOuterAngle()));

  light->setSpotOuterAngle(degrees(-45.0f));
  EXPECT_EQ(45.0f, getDegrees(light->getSpotOuterAngle()));
}
