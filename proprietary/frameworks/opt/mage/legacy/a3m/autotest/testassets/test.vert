uniform vec4 u_dummy;

attribute vec4 a_position;
attribute vec4 a_colour;
varying vec4 v_colour;

void main()
{
  gl_Position = a_position;
  v_colour = a_colour * u_dummy;
}
