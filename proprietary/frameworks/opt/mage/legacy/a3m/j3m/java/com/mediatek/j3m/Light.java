/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Light interface.
 */

package com.mediatek.j3m;

/* Extend JNI Overview */ /*! \addtogroup a3mJusr

<h2>%Light</h2>

The Light SceneNode acts as a source of light for illuminating the scene.
There are three distinct kinds of light: omni-directional (point), directional
(sun) and spot.

Omni-directional lights have only a position and emit light
equally in all direction (imagine a naked bulb); direcitonal lights have only a
direction, and emit light everywhere in the scene (imagine light coming from a
very distant object, like the sun); spot lights have both a position and a
direction, as well as an inner and outer falloff angle (imagine a
torch/flashlight or lamp covered by a conic lampshade).

The intensity of omni-directional and spot lights falls off with distance from
the light.  Currently, A3M supports two schemes for lighting attenuation to
match the two tools supported by the Glo3Tools exporter pipeline: Blender and
3DS Max.  Blender-style attenuation is linear, dropping from the maximum light
intensity down to zero between the attenutation near and far distances; 3DS
Max-style attenuation similarly occurs between a near and far distance, but
falls off by the square root of the distance.

Internally, lights are used by the renderer to set a series of shader program
uniforms, which are interpreted by the default shaders in the way the engine
intends them to be used.  However, there is no such limitation imposed on the
user's own custom shaders, and the light parameters may be interpreted however
the user likes.

*/

/*!
 * \ingroup a3mJusrScenenodes
 * @{
 */
/**
 * Light SceneNode interface.
 * Lights provide illumination for a scene, which can help add depth and
 * realism to what might otherwise be a very flat and unconvincing virtual
 * world.
 */
public interface Light extends SceneNode {
    /** Identifier for this node type */
    char NODE_TYPE = 'L';

    /**
     * Light type enum.
     */
    public static class Type {
        /** Omni-directional "point" light */
        public static final int OMNI = 0;
        /** Directional "sun" light */
        public static final int DIRECTIONAL = 1;
        /** Spot light with conic falloff */
        public static final int SPOT = 2;
    }

    /**
     * Sets the type of the light.
     *
     * @param type Light type
     */
    void setLightType(int type);

    /**
     * Returns the type of the light.
     *
     * @return Light type
     */
    int getLightType();

    /**
     * Sets the colour of the light.
     *
     * @param r Red component
     * @param g Green component
     * @param b Blue component
     * @param a Alpha component
     */
    void setColour(float r, float g, float b, float a);

    /**
     * Returns the red light colour component.
     *
     * @return Colour component
     */
    float getColourR();

    /**
     * Returns the green light colour component.
     *
     * @return Colour component
     */
    float getColourG();

    /**
     * Returns the blue light colour component.
     *
     * @return Colour component
     */
    float getColourB();

    /**
     * Returns the alpha light colour component.
     *
     * @return Colour component
     */
    float getColourA();

    /**
     * Sets the ambient light level.
     *
     * @param level Level
     */
    void setAmbientLevel(float level);

    /**
     * Returns the ambient light level.
     *
     * @return Level
     */
    float getAmbientLevel();

    /**
     * Sets the intensity of the light.
     * Light intensity is the amount by which the attenuation of a light is
     * multiplied to provide the illumination level.
     *
     * @param intensity Light intensity
     */
    void setIntensity(float intensity);

    /**
     * Returns the intensity of the light.
     *
     * @return Light intensity
     */
    float getIntensity();

    /**
     * Sets whether the illumination level should attenuate with distance.
     *
     * @param isAttenuated true if light value should attenuate with distance.
     */
    void setIsAttenuated(boolean isAttenuated);

    /**
     * Returns whether the illumination level attenuates with distance.
     *
     * @return true if light value attenuates with distance.
     */
    boolean getIsAttenuated();

    /**
     * Sets the point at which the light starts to attenuate.
     *
     * @param distance Near attenuation distance
     */
    void setAttenuationNear(float distance);

    /**
     * Returns the point at which the light starts to attenuate.
     *
     * @return Near attenuation distance
     */
    float getAttenuationNear();

    /**
     * Sets the point at which the light is completely attenuated.
     *
     * @param distance Far attenuation distance
     */
    void setAttenuationFar(float distance);

    /**
     * Returns the point at which the light is completely attenuated.
     *
     * @return Far attenuation distance
     */
    float getAttenuationFar();

    /**
     * Sets the angle at which a spot light starts to attenuate.
     * The spot angle is measured relative to the positive local z-axis (i.e.
     * the direction in which the light is pointing).
     *
     * @param units AngularUnits in which the angle is given
     * @param value Inner attenuation angle
     */
    void setSpotInnerAngle(int units, float value);

    /**
     * Returns the angle at which a spot light starts to attenuate.
     *
     * @param units AngularUnits in which the angle is given
     * @return Inner attenuation angle
     */
    float getSpotInnerAngle(int units);

    /**
     * Sets the angle at which a spot light is completely attenuated.
     *
     * @param units AngularUnits in which the angle is given
     * @param value Outer attenuation angle
     */
    void setSpotOuterAngle(int units, float value);

    /**
     * Returns the angle at which a spot light is completely attenuated.
     *
     * @param units AngularUnits in which the angle is given
     * @return Outer attenuation angle
     */
    float getSpotOuterAngle(int units);
}

/*! @} */
