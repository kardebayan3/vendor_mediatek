/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Animation interface.
 */

package com.mediatek.j3m;

/* User might look for animation in either 'how things render' or 'how objects
 * behave' so cover both options:
 */
/*!
 * \ingroup a3mJusrRender
 * \ingroup a3mJusrScenenodes
 * @{
 */

/**
 * Controls the playback of animations.
 * Animations may be controlled directly, but this class provides useful
 * playback functionality usually expected of an animation system, such as
 * progress tracking, pausing, speed adjustment and looping.
 */
public interface AnimationController {
    /**
     * Applies the animation without advancing the progress of the controller.
     * If the controller is not enabled, this function will do nothing.
     */
    void update();

    /**
     * Increments the progress of the controller and applies the animation.
     * If the controller is not enabled, this function will do nothing.
     *
     * If the controller is enabled and paused, the progress will not change,
     * but the animation will still be applied.
     *
     * If the controller is enabled and not paused, the controller progress
     * will advance by delta multiplied by the controller speed.
     *
     * If looping is enabled, loop points will be taken into account when
     * incrementing the progress.  If the progress advances beyond either end
     * of the loop, the progress will be wrapped around to the other end of the
     * loop.  To exit a loop, set the progress directly to a point outside of
     * the loop.  If the loop end is less than or equal to the loop start, then
     * the loop is undefined and the progress will not be looped, even if
     * looping is enabled.
     *
     * @param delta Amount by which to increment the progress
     */
    void update(float delta);

    /**
     * Enables and unpauses the controller, rewinding if playback has finished.
     * This is a convenience function for those who prefer to use the more
     * traditional play/pause/stop interface.
     */
    void play();

    /**
     * Enables and unpauses the controller, rewinding if playback has finished.
     * The caller can optionally update the animation.
     *
     * @see #play()
     * @param update Whether to update the animation
     */
    void play(boolean update);

    /**
     * Pauses the controller.
     * This is a convenience function for those who prefer to use the more
     * traditional play/pause/stop interface.
     */
    void pause();

    /**
     * Pauses the controller.
     * The caller can optionally update the animation.
     *
     * @see #pause()
     * @param update Whether to update the animation
     */
    void pause(boolean update);

    /**
     * Disables and rewinds the controller.
     * This is a convenience function for those who prefer to use the more
     * traditional play/pause/stop interface.
     */
    void stop();

    /**
     * Disables and rewinds the controller, optionally updating the animation.
     *
     * @see #stop()
     * @param update Whether to update the animation
     */
    void stop(boolean update);

    /**
     * Updates a controller and its animation to a specific point.
     * This function is equivilent to setting a controller's progress and then
     * updating the animation with a zero delta.
     *
     * @param progress Progress to which to seek
     */
    void seek(float progress);

    /**
     * Rewinds a controller to the beginning of playback, updating the
     * animation.
     * If the controller speed is positive, the progress will be set to the
     * start of the animation; if speed is negative, it will be set to the end.
     * If speed is zero, this function does nothing.
     */
    void rewind();

    /**
     * Rewinds a controller to the beginning of playback, optionally updating
     * the animation.
     *
     * @see #rewind()
     * @param update Whether to update the animation
     */
    void rewind(boolean update);

    /**
     * Determines whether a controller has finished playback.
     * If the controller speed is positive, playback is complete once the
     * progress has reached the end of the animation; if speed is negative, it is
     * complete when at the start. If speed is zero, this function will return
     * FALSE.  If playback is inside a loop, this function will return FALSE.
     *
     * @return True if playback has finished
     */
    boolean isFinished();

    /**
     * Determines whether a controller is currently inside a loop.
     * The controller is inside a loop if looping is enabled, a loop is defined
     * and the progress is inside the loop range.
     *
     * @return True if inside a loop
     */
    boolean isInsideLoop();

    /**
     * Sets the progress of the controller.
     * This function can be used to set the progress of controller directly,
     * without taking into account loop points.  You can jump out of a loop by
     * using this function.  You can also set the progess beyond the bounds set
     * using setStart() and setEnd(), although the progress will be clamped to
     * the bounds when update() is next called, assuming the controller is not
     * paused.
     *
     * @param progress Progress of controller
     */
    void setProgress(float progress);

    /**
     * Returns the current progress of the controller.
     * The progress will loop back on itself if looping is enabled.
     *
     * @return Progress of controller
     */
    float getProgress();

    /**
     * Sets whether the controller is enabled.
     * When a controller is disabled, calls to setProgress() and update() will
     * do nothing.  Controllers are enabled by default.
     *
     * @param enabled Whether to enable the controller
     */
    void setEnabled(boolean enabled);

    /**
     * Returns whether the controller is enabled.
     *
     * @return Whether controller is enabled
     */
    boolean getEnabled();

    /**
     * Sets whether the controller is paused.
     * When a controller is paused, calls to update() will not progress the
     * animation.  Controllers are not paused by default.
     *
     * @param paused Whether to pause the controller
     */
    void setPaused(boolean paused);

    /**
     * Returns whether the controller is paused.
     *
     * @return Whether controller is paused
     */
    boolean getPaused();

    /**
     * Sets whether the controller playback is looping.
     * If a controller is looping, the loop points will take effect when
     * advancing the progress with a call to update().  Looping is enabled by
     * default, but the animation will only loop if a loop is defined.
     *
     * @param looping Whether the controller is looping
     */
    void setLooping(boolean looping);

    /**
     * Returns whether the controller playback is looping.
     *
     * @return Whether controller playback is looping
     */
    boolean getLooping();

    /**
     * Sets the playback speed of the controller.
     * The controller speed determines the rate at which the controller
     * progress is advanced when calling update().  The speed is set to 1.0 by
     * default.
     *
     * @param speed Controller speed
     */
    void setSpeed(float speed);

    /**
     * Returns the controller playback speed.
     *
     * @return Playback speed
     */
    float getSpeed();

    /**
     * Sets start point of the animation.
     * The lower bound of the range to which the controller progress is clamped
     * when update() is called.  By default this is set to start point of the
     * controlled animation.
     *
     * @param start Controller start point
     */
    void setStart(float start);

    /**
     * Returns start point of the animation.
     *
     * @return Animation start point
     */
    float getStart();

    /**
     * Sets end point of the animation.
     * The upper bound of the range to which the controller progress is clamped
     * when update() is called.  By default this is set to end point of the
     * controlled animation.
     *
     * @param start Controller end point
     */
    void setEnd(float end);

    /**
     * Returns end point of the animation.
     *
     * @return Animation end point
     */
    float getEnd();

    /**
     * Sets the start and end of a controller's playback.
     *
     * @param start Playback start point
     * @param end Playback end point
     */
    void setRange(float start, float end);

    /**
     * Returns the playback length of the controller.
     * The length is always greater or equal to zero.
     *
     * @return Playback length
     */
    float getLength();

    /**
     * Sets loop start point of the animation.
     * The lower bound of the range over which the controller progress is
     * looped when update() is called.  If the start and end points are equal
     * the the loop is undefined.  The loop start point is set to 0.0 by
     * default.
     *
     * @param start Controller loop start point
     */
    void setLoopStart(float start);

    /**
     * Returns loop start point of the animation.
     *
     * @return Animation loop start point
     */
    float getLoopStart();

    /**
     * Sets loop end point of the animation.
     * The upper bound of the range over which the controller progress is
     * looped when update() is called.  If the start and end points are equal
     * the the loop is undefined.  The loop end point is set to 0.0 by default.
     *
     * @param start Controller loop end point
     */
    void setLoopEnd(float end);

    /**
     * Returns loop end point of the animation.
     *
     * @return Animation loop end point
     */
    float getLoopEnd();

    /**
     * Sets the start and end of a controller's playback.
     *
     * @param start Loop start point
     * @param end Loop end point
     */
    void setLoopRange(float start, float end);

    /**
     * Returns the loop length of the controller.
     * The length is always greater or equal to zero.
     *
     * @return Loop length
     */
    float getLoopLength();

    /**
     * Determines whether a controller has a loop defined.
     * A loop is defined if the loop end is greater than the loop start.
     *
     * @return True if controller has a loop
     */
    boolean hasLoop();

    /**
     * Returns the animation controlled by the controller.
     *
     * @return Animation
     */
    Animation getAnimation();
}

/*! @} */
