$(info J3M-JNI J3M-JAVA ANDROID.MK)
#
# Makes J3M package
#
# Used by NDK builds and by makeMtk builds.
#

#################################################
# Build as STATIC java library - for use in Ngin3d

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(call all-java-files-under, java)
LOCAL_MODULE := com.mediatek.j3m-static

include $(BUILD_STATIC_JAVA_LIBRARY)



# The following are here for reference (hence commented out).
# We expect that the shared lib may be needed for non-ngin apps
# at which point the permission-to-access-lib file may be needed.

#################################################
# Build as system java library

#LOCAL_PATH := $(call my-dir)

#include $(CLEAR_VARS)

#LOCAL_MODULE_TAGS := optional
#LOCAL_SRC_FILES := $(call all-java-files-under, java)
#LOCAL_MODULE := com.mediatek.j3m

#include $(BUILD_JAVA_LIBRARY)


#################################################
# permission file identifying shared library

#include $(CLEAR_VARS)
#LOCAL_MODULE_TAGS := eng
#LOCAL_MODULE := com.mediatek.j3m.xml
#LOCAL_MODULE_CLASS := ETC

# This will install the file in /system/etc/permissions
#LOCAL_MODULE_PATH := $(TARGET_OUT_ETC)/permissions
#LOCAL_SRC_FILES := $(LOCAL_MODULE)

#include $(BUILD_PREBUILT)

##################################################

include $(call all-makefiles-under,$(LOCAL_PATH))
