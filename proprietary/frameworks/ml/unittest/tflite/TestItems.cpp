/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <string.h>
#include "TestItems.h"

#define FP32_ITEM(name) \
    { \
        "" name "", \
        "/sdcard/" name "/TFLite/FP32/float_model.tflite", \
        "/sdcard/" name "/TFLite/FP32/golden/input_0.npy", \
        "/sdcard/" name "/TFLite/FP32/golden/output_0.npy", \
        FP32, \
        FP32, \
    },

#define UINT8_ITEM(name) \
    { \
        "" name "", \
        "/sdcard/" name "/TFLite/UINT8/uint8_model.tflite", \
        "/sdcard/" name "/TFLite/UINT8/golden/input_0.npy", \
        "/sdcard/" name "/TFLite/UINT8/golden/output_0.npy", \
        UINT8, \
        UINT8, \
    },

#define LEGACY_ITEM(name, folder, in_type, out_type) \
    { \
        "" name "", \
        "data/local/tmp/data/" folder "/" name "/export/model.lite",\
        "data/local/tmp/data/" folder "/" name "/export/batch_xs.npy", \
        "data/local/tmp/data/" folder "/" name "/export/ys.npy", \
        in_type, \
        out_type, \
    },

static const TestItem testItems[] = {
    FP32_ITEM("mobilenet_v1_224_model")
    FP32_ITEM("mobilenet_v2_224_model")
    FP32_ITEM("inception_v1_model")
    FP32_ITEM("inception_v3_model")
    FP32_ITEM("resnet_v1_50_model")
    FP32_ITEM("resnet_v2_50_model")
    FP32_ITEM("vgg_16_fc_model")
    FP32_ITEM("vgg_16_model")

    UINT8_ITEM("mobilenet_v1_224_model")
    UINT8_ITEM("mobilenet_v2_224_model")
    UINT8_ITEM("inception_v1_model")
    UINT8_ITEM("inception_v3_model")
    UINT8_ITEM("resnet_v1_50_model")
    UINT8_ITEM("resnet_v2_50_model")
    UINT8_ITEM("vgg_16_fc_model")
    UINT8_ITEM("vgg_16_model")
};

static const TestItem legacyTestItems[] = {
    LEGACY_ITEM("avg_pool"                     , "float-ops", FP32, FP32)
    LEGACY_ITEM("concat"                       , "float-ops", FP32, FP32)
    LEGACY_ITEM("conv2d"                       , "float-ops", FP32, FP32)
    LEGACY_ITEM("conv2d_transpose"             , "float-ops", FP32, FP32)
    LEGACY_ITEM("depthwise_conv2d"             , "float-ops", FP32, FP32)
    LEGACY_ITEM("fully_connected"              , "float-ops", FP32, FP32)
    LEGACY_ITEM("l2_normalize"                 , "float-ops", FP32, FP32)
    LEGACY_ITEM("l2_pool"                      , "float-ops", FP32, FP32)
    LEGACY_ITEM("local_response_normalization" , "float-ops", FP32, FP32)
    LEGACY_ITEM("max_pool_2x2_s2"              , "float-ops", FP32, FP32)
    LEGACY_ITEM("max_pool_3x3_s2"              , "float-ops", FP32, FP32)
    LEGACY_ITEM("mean"                         , "float-ops", FP32, FP32)
    LEGACY_ITEM("multiply"                     , "float-ops", FP32, FP32)
    LEGACY_ITEM("pad"                          , "float-ops", FP32, FP32)
    LEGACY_ITEM("pad_avg_pool"                 , "float-ops", FP32, FP32)
    LEGACY_ITEM("pad_conv2d"                   , "float-ops", FP32, FP32)
    LEGACY_ITEM("pad_max_pool"                 , "float-ops", FP32, FP32)
    LEGACY_ITEM("relu"                         , "float-ops", FP32, FP32)
    LEGACY_ITEM("relu6"                        , "float-ops", FP32, FP32)
    LEGACY_ITEM("resize_bilinear"              , "float-ops", FP32, FP32)
    LEGACY_ITEM("sigmoid"                      , "float-ops", FP32, FP32)
    LEGACY_ITEM("softmax"                      , "float-ops", FP32, FP32)
    LEGACY_ITEM("space_to_depth"               , "float-ops", FP32, FP32)
    LEGACY_ITEM("tanh"                         , "float-ops", FP32, FP32)

    LEGACY_ITEM("googlenet"                    , "float-models", FP32, FP32)
    LEGACY_ITEM("inception_v1"                 , "float-models", FP32, FP32)
    LEGACY_ITEM("inception_v3"                 , "float-models", FP32, FP32)
    LEGACY_ITEM("mobilenet"                    , "float-models", FP32, FP32)
    LEGACY_ITEM("mobilenet_v1_224"             , "float-models", FP32, FP32)
    LEGACY_ITEM("resnet_v1_50"                 , "float-models", FP32, FP32)
    LEGACY_ITEM("resnet_v2_50"                 , "float-models", FP32, FP32)
    LEGACY_ITEM("squeezenet_v1_1"              , "float-models", FP32, FP32)
    LEGACY_ITEM("vgg_v16"                      , "float-models", FP32, FP32)

    LEGACY_ITEM("avg_pool"                     , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("add"                          , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("concat"                       , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("conv2d"                       , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("depthwise_conv2d"             , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("fully_connected"              , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("leaky_relu"                   , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("max_pool"                     , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("mean"                         , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("multiply"                     , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("pad"                          , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("resize_bilinear"              , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("slice"                        , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("softmax"                      , "uint8-ops", UINT8, UINT8)
    LEGACY_ITEM("space_to_depth"               , "uint8-ops", UINT8, UINT8)

    LEGACY_ITEM("dequantize"                   , "quant-ops", UINT8, FP32)

    LEGACY_ITEM("inception_v1"                 , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("inception_v3"                 , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("resnet_v1_34"                 , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("resnet_v1_50"                 , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("resnet_v2_50"                 , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("mobilenet_v1_224"             , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("squeezenet_v1_1"              , "quant-models", UINT8, UINT8)
    LEGACY_ITEM("vgg_v16"                      , "quant-models", UINT8, UINT8)
};

const TestItem* getTestItem(const char* name, int in_data_type, int out_data_type) {
    for (int i = 0; i < (int)(sizeof(testItems)/sizeof(TestItem)); i++) {
        if (strcmp(name, testItems[i].name) == 0 &&
            in_data_type == testItems[i].in_data_type &&
            out_data_type == testItems[i].out_data_type) {
            return &testItems[i];
        }
    }

    return nullptr;
}

int getTestItemSize() {
    return sizeof(testItems)/sizeof(TestItem);
}

const TestItem* getTestItemByIndex(int index) {
    return &testItems[index];
}

const TestItem* getLegacyTestItem(const char* name, int in_data_type, int out_data_type) {
    for (int i = 0; i < (int)(sizeof(legacyTestItems)/sizeof(TestItem)); i++) {
        if (strcmp(name, legacyTestItems[i].name) == 0 &&
            in_data_type == legacyTestItems[i].in_data_type &&
            out_data_type == legacyTestItems[i].out_data_type) {
            return &legacyTestItems[i];
        }
    }

    return nullptr;
}

int getLegacyTestItemSize() {
    return sizeof(legacyTestItems)/sizeof(TestItem);
}

const TestItem* getLegacyTestItemByIndex(int index) {
    return &legacyTestItems[index];
}