#include <iostream>
#include <fstream>
#include "Common.h"

static std::string datapath;

const char* getDataPath(std::string file_path) {
    std::string path = "/data/local/tmp/data";
    char* str = getenv("TFLITE_DATA_PATH");
    if (str)
        path = str;
    datapath = path + file_path;
    return datapath.c_str();
}
