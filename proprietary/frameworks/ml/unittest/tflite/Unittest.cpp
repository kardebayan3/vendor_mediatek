/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include <getopt.h>
#include <stdio.h>
#include "InterpreterWrapper.h"
#include "NeuroPilotTFLiteWrapper.h"
#include "NeuroPilotTFLiteWrapper_Legacy.h"
#include "gtest/gtest.h"
#include <cutils/properties.h>

using namespace std;

struct Settings {
    string data_path;  // the path of model and input/golden
    string model_name;
};

int g_iteration;
Settings g_settings;

class customListener : public testing::EmptyTestEventListener {
  public:
    customListener(int* iteration) : m_iteration(iteration) {}
    virtual void OnTestIterationStart(const testing::UnitTest& unit_test,
        int iteration) {
        (void)unit_test;
        *m_iteration = iteration;
    }

  private:
    int* m_iteration;
};

int getLoopCount(void) {
    char buf[PROPERTY_VALUE_MAX] = {'\0',};

    if (property_get("debug.tflite.test.loop", buf, "") > 0) {
        return atoi(buf);
    }

    return 1;
}

int loopModelRecreation(const TestItem* item) {
    int ret = 0;

    for (int i = 0; i < getLoopCount(); i++) {
#ifdef USE_INTERPRETER_CPU_BACKEND
        ret = runInterpreter(item, false);
#else
        ret = runInterpreter(item);
#endif
        if (ret != 0) {
            break;
        }
    }

    return ret;
}

int loopInvoke(const TestItem* item) {
    InterpreterWrapperConfig c;
    c.loop_count = getLoopCount();
    c.show_output = false;
    c.save_output = false;
#ifdef USE_INTERPRETER_CPU_BACKEND
    c.use_nnapi = false;
#else
    c.use_nnapi = true;
#endif
    c.relax_computation_float32_to_float16 = isAllowFp32RelaxToFp16();

    return runInterpreter(item, &c);
}

int invokeWithContinuousInputs(const TestItem* item) {
    InterpreterWrapperConfig c;
    c.loop_count = getLoopCount();
    c.show_output = false;
    c.save_output = false;
#ifdef USE_INTERPRETER_CPU_BACKEND
    c.use_nnapi = false;
#else
    c.use_nnapi = true;
#endif
    c.relax_computation_float32_to_float16 = isAllowFp32RelaxToFp16();
    c.allow_threshold = true;
    c.continuous_input = true;

    return runInterpreter(item, &c);
}

void TFLiteTest(const char* name, int inDataType, int outDataType) {
    const TestItem* item = getTestItem(name, inDataType, outDataType);
    ASSERT_NE(nullptr, item);

    string input = string(item->input);
    string goldent = string(item->golden);

    string postfix = std::to_string(g_iteration) + string(".npy");
    string input_path = input.replace(input.end() - 5, input.end(), postfix);
    string golden_path = goldent.replace(goldent.end() - 5, goldent.end(), postfix);

    TestItem newItem;
    memcpy(&newItem, item, sizeof(TestItem));
    newItem.input = input_path.c_str();
    newItem.golden = golden_path.c_str();

    ASSERT_EQ(0, loopModelRecreation(&newItem));
    ASSERT_EQ(0, loopInvoke(&newItem));
}

void TFLiteLegacyTest(const char* name, int inDataType, int outDataType) {
    const TestItem* item = getLegacyTestItem(name, inDataType, outDataType);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

void verify(Settings* s) {
    string model_path = s->data_path + s->model_name;
    string input_path = s->data_path + string("golden/input_");
    string golden_path = s->data_path + string("golden/output_");

    TestItem item;
    item.tflite = model_path.c_str();
    item.input = input_path.c_str();
    item.golden = golden_path.c_str();

    ASSERT_EQ(0, invokeWithContinuousInputs(&item));
}

TEST(TFLiteStressTestFp32, mobilenet_v1_224) {
    const TestItem* item = getTestItem("mobilenet_v1_224_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, mobilenet_v2_224) {
    const TestItem* item = getTestItem("mobilenet_v2_224_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, inception_v1) {
    const TestItem* item = getTestItem("inception_v1_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, inception_v3) {
    const TestItem* item = getTestItem("inception_v3_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, resnet_v1_50) {
    const TestItem* item = getTestItem("resnet_v1_50_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, resnet_v2_50) {
    const TestItem* item = getTestItem("resnet_v2_50_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, vgg_16_fc) {
    const TestItem* item = getTestItem("vgg_16_fc_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, vgg_16) {
    const TestItem* item = getTestItem("vgg_16_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, mobilenet_v1_224) {
    const TestItem* item = getTestItem("mobilenet_v1_224_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, mobilenet_v2_224) {
    const TestItem* item = getTestItem("mobilenet_v2_224_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, inception_v1) {
    const TestItem* item = getTestItem("inception_v1_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, inception_v3) {
    const TestItem* item = getTestItem("inception_v3_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, resnet_v1_50) {
    const TestItem* item = getTestItem("resnet_v1_50_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, resnet_v2_50) {
    const TestItem* item = getTestItem("resnet_v2_50_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, vgg_16_fc) {
    const TestItem* item = getTestItem("vgg_16_fc_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, vgg_16) {
    const TestItem* item = getTestItem("vgg_16_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(NpWrapperTest, utNpWrapperGetInferencePreference) {
    int ret = utNpWrapperGetInferencePreference();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyCreateNonExistentModel) {
    int ret = utTFLiteWrapperLegacyCreateNonExistentModel();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyCreateWithBuffer) {
    int ret = utTFLiteWrapperLegacyCreateWithBuffer();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyCreate) {
    int ret = utTFLiteWrapperLegacyCreate();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyGetInputTensor) {
    int ret = utTFLiteWrapperLegacyGetInputTensor();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyGetOutputTensor) {
    int ret = utTFLiteWrapperLegacyGetOutputTensor();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyInvoke) {
    int ret = utTFLiteWrapperLegacyInvoke();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperLegacyTest, utTFLiteWrapperLegacyCreateCustomWithBuffer) {
    int ret = utTFLiteWrapperLegacyCreateCustomWithBuffer();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperCreateNonExistentModel) {
    int ret = utTFLiteWrapperCreateNonExistentModel();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperCreateWithBuffer) {
    int ret = utTFLiteWrapperCreateWithBuffer();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperCreate) {
    int ret = utTFLiteWrapperCreate();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperGetInputTensor) {
    int ret = utTFLiteWrapperGetInputTensor();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperGetOutputTensor) {
    int ret = utTFLiteWrapperGetOutputTensor();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperInvoke) {
    int ret = utTFLiteWrapperInvoke();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperCreateCustomWithBuffer) {
    int ret = utTFLiteWrapperCreateCustomWithBuffer();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperCreateCustomWithQuantBuffer) {
    int ret = utTFLiteWrapperCreateCustomWithQuantBuffer();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperSetExecParallel) {
    int ret = utTFLiteWrapperSetExecParallel();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperGetDequantizedOutputByIndex) {
    int ret = utTFLiteWrapperGetDequantizedOutputByIndex();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperGetDequantizedOutputByIndexFromFloatModel) {
    int ret = utTFLiteWrapperGetDequantizedOutputByIndexFromFloatModel();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperSsetAllowFp16PrecisionForFp32) {
    int ret = utTFLiteWrapperSsetAllowFp16PrecisionForFp32();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteWrapperTest, utTFLiteWrapperSsetAllowFp16PrecisionForFp32WithUint8Model) {
    int ret = utTFLiteWrapperSsetAllowFp16PrecisionForFp32WithUint8Model();
    ASSERT_EQ(1, ret);
}

TEST(TFLiteTestFp32, mobilenet_v1_224) {
    TFLiteTest("mobilenet_v1_224_model", FP32, FP32);
}

TEST(TFLiteTestFp32, mobilenet_v2_224) {
    TFLiteTest("mobilenet_v2_224_model", FP32, FP32);
}

TEST(TFLiteTestFp32, inception_v1) {
    TFLiteTest("inception_v1_model", FP32, FP32);
}

TEST(TFLiteTestFp32, inception_v3) {
    TFLiteTest("inception_v3_model", FP32, FP32);
}

TEST(TFLiteTestFp32, resnet_v1_50) {
    TFLiteTest("resnet_v1_50_model", FP32, FP32);
}

TEST(TFLiteTestFp32, resnet_v2_50) {
    TFLiteTest("resnet_v2_50_model", FP32, FP32);
}

TEST(TFLiteTestFp32, vgg_16_fc) {
    TFLiteTest("vgg_16_fc_model", FP32, FP32);
}

TEST(TFLiteTestFp32, vgg_16) {
    TFLiteTest("vgg_16_model", FP32, FP32);
}

TEST(TFLiteTestUint8, mobilenet_v1_224) {
    TFLiteTest("mobilenet_v1_224_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, mobilenet_v2_224) {
    TFLiteTest("mobilenet_v2_224_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, inception_v1) {
    TFLiteTest("inception_v1_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, inception_v3) {
    TFLiteTest("inception_v3_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, resnet_v1_50) {
    TFLiteTest("resnet_v1_50_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, resnet_v2_50) {
    TFLiteTest("resnet_v2_50_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, vgg_16_fc) {
    TFLiteTest("vgg_16_fc_model", UINT8, UINT8);
}

TEST(TFLiteTestUint8, vgg_16) {
    TFLiteTest("vgg_16_model", UINT8, UINT8);
}

TEST(TFLiteTest, utAvgPool) {
    TFLiteLegacyTest("avg_pool", FP32, FP32);
}

TEST(TFLiteTest, utConcat) {
    TFLiteLegacyTest("concat", FP32, FP32);
}

TEST(TFLiteTest, utConv2D) {
    TFLiteLegacyTest("conv2d", FP32, FP32);
}

TEST(TFLiteTest, utDepthWiseConv2D) {
    TFLiteLegacyTest("depthwise_conv2d", FP32, FP32);
}

TEST(TFLiteTest, utFully_Connected) {
    TFLiteLegacyTest("fully_connected", FP32, FP32);
}

TEST(TFLiteTest, utL2_Normalize) {
    TFLiteLegacyTest("l2_normalize", FP32, FP32);
}

TEST(TFLiteTest, utL2_Pool) {
    TFLiteLegacyTest("l2_pool", FP32, FP32);
}

TEST(TFLiteTest, utLeakyRelu) {
    TFLiteLegacyTest("leaky_relu", FP32, FP32);
}

TEST(TFLiteTest, utPad) {
    TFLiteLegacyTest("pad", FP32, FP32);
}

TEST(TFLiteTest, utLocal_Response_Normalization) {
    TFLiteLegacyTest("local_response_normalization", FP32, FP32);
}

TEST(TFLiteTest, utMaxPool2x2) {
    TFLiteLegacyTest("max_pool_2x2_s2", FP32, FP32);
}

TEST(TFLiteTest, utMaxPool3x3) {
    TFLiteLegacyTest("max_pool_3x3_s2", FP32, FP32);
}

TEST(TFLiteTest, utMultiply) {
    TFLiteLegacyTest("multiply", FP32, FP32);
}

TEST(TFLiteTest, utMean) {
    TFLiteLegacyTest("mean", FP32, FP32);
}

TEST(TFLiteTest, utPadAvgPool) {
    TFLiteLegacyTest("pad_avg_pool", FP32, FP32);
}

TEST(TFLiteTest, utPadConv2D) {
    TFLiteLegacyTest("pad_conv2d", FP32, FP32);
}

TEST(TFLiteTest, utPadMaxPool) {
    TFLiteLegacyTest("pad_max_pool", FP32, FP32);
}

TEST(TFLiteTest, utRelu) {
    TFLiteLegacyTest("relu", FP32, FP32);
}

TEST(TFLiteTest, utRelu6) {
    TFLiteLegacyTest("relu6", FP32, FP32);
}

TEST(TFLiteTest, utResize_Bilinear) {
    TFLiteLegacyTest("resize_bilinear", FP32, FP32);
}

TEST(TFLiteTest, utSigmoid) {
    TFLiteLegacyTest("sigmoid", FP32, FP32);
}

TEST(TFLiteTest, utSoftmax) {
    TFLiteLegacyTest("softmax", FP32, FP32);
}

TEST(TFLiteTest, utSpaceToDepth) {
    TFLiteLegacyTest("space_to_depth", FP32, FP32);
}

TEST(TFLiteTest, utTanh) {
    TFLiteLegacyTest("tanh", FP32, FP32);
}


TEST(TFLiteTest, utQuantAdd) {
    TFLiteLegacyTest("add", UINT8, UINT8);
}

TEST(TFLiteTest, utQuant_Avg_Pool) {
    TFLiteLegacyTest("avg_pool", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantConcat) {
    TFLiteLegacyTest("concat", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantConv2D) {
    TFLiteLegacyTest("conv2d", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantDepthWiseConv2D) {
    TFLiteLegacyTest("depthwise_conv2d", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantFullyConnected) {
    TFLiteLegacyTest("fully_connected", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantLeakyRelu) {
    TFLiteLegacyTest("leaky_relu", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantMaxPool) {
    TFLiteLegacyTest("max_pool", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantMean) {
    TFLiteLegacyTest("mean", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantMultiply) {
    TFLiteLegacyTest("multiply", UINT8, UINT8);
}


TEST(TFLiteTest, utQuantResizeBilinear) {
    TFLiteLegacyTest("resize_bilinear", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantSoftmax) {
    TFLiteLegacyTest("softmax", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantPad) {
    TFLiteLegacyTest("pad", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantSpaceToDepth) {
    TFLiteLegacyTest("space_to_depth", UINT8, UINT8);
}

TEST(TFLiteTest, utInceptionV1) {
    TFLiteLegacyTest("inception_v1", FP32, FP32);
}

TEST(TFLiteTest, utInceptionV3) {
    TFLiteLegacyTest("inception_v3", FP32, FP32);
}

TEST(TFLiteTest, utMobileNet) {
    TFLiteLegacyTest("mobilenet", FP32, FP32);
}

TEST(TFLiteTest, utMobileNetV1_224) {
    TFLiteLegacyTest("mobilenet_v1_224", FP32, FP32);
}

TEST(TFLiteTest, utResnetV1_50) {
    TFLiteLegacyTest("resnet_v1_50", FP32, FP32);
}

TEST(TFLiteTest, utResnetV2_50) {
    TFLiteLegacyTest("resnet_v2_50", FP32, FP32);
}

TEST(TFLiteTest, utVggV16) {
    TFLiteLegacyTest("vgg_v16", FP32, FP32);
}

TEST(TFLiteTest, utSqueezenetV1_1) {
    TFLiteLegacyTest("squeezenet_v1_1", FP32, FP32);
}

TEST(TFLiteTest, utQuantInceptionV1) {
    TFLiteLegacyTest("inception_v1", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantInceptionV3) {
    TFLiteLegacyTest("inception_v3", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantMobileNetV1_224) {
    TFLiteLegacyTest("mobilenet_v1_224", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantResnetV1_34) {
    TFLiteLegacyTest("resnet_v1_34", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantResnetV1_50) {
    TFLiteLegacyTest("resnet_v1_50", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantResnetV2_50) {
    TFLiteLegacyTest("resnet_v2_50", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantVggV16) {
    TFLiteLegacyTest("vgg_v16", UINT8, UINT8);
}

TEST(TFLiteTest, utQuantSqueezenetV1_1) {
    TFLiteLegacyTest("squeezenet_v1_1", UINT8, UINT8);
}

TEST(TFLiteTest, verifyModel) {
    ASSERT_EQ(false, g_settings.data_path.empty());
    ASSERT_EQ(false, g_settings.model_name.empty());

    verify(&g_settings);
}

void processOptions(int argc, char** argv, Settings* outSettings) {
    static struct option long_options[] = {
        {"data_path", required_argument, 0, 'p'},
        {"model_name", required_argument, 0, 'm'},
        {0, 0, 0, 0},
    };

    int c = -1;
    int option_index = 0;

    while (1) {
        /* getopt_long stores the option index here. */
        c = getopt_long(argc, argv, "p:m:", long_options,
                        &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c) {
            case 'p':
                outSettings->data_path = optarg;
                break;
            case 'm':
                outSettings->model_name = optarg;
                break;
            default:
                break;
        }
    }
}

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    testing::TestEventListeners& listners =
        testing::UnitTest::GetInstance()->listeners();
    listners.Append(new customListener(&g_iteration));
    processOptions(argc, argv, &g_settings);
    int ret = RUN_ALL_TESTS();
    return ret;
}
