#include <getopt.h>
#include <iostream>
#include <fstream>
#include <glob.h> // glob(), globfree()
#include <thread>
#include <unistd.h>
#include <vector>
#include <gtest/gtest.h>
#include "TestItems.h"
#include "InterpreterWrapper.h"

using namespace std;

enum OutputType {
    OUTPUT_NONE = 0,
    SHOW_OUTPUT = 1,
    SAVE_OUTPUT = 2,
};

enum ModeType {
    DEFAULT_MODE = 0,   // Glob model/input/golden by the given root path and file name prefix
    SPECIFIED_MODEL_MODE = 1,
    SPECIFIED_MODEL_WITH_RANDOM_INPUT = 2,
};

struct Settings {
    bool accel = true;
    // 0: default mode, 1 specified model with input/output, 2. specified model with random input
    int mode = 0;
    int output_type = 0;
    int loop_count = 1;
    int data_type = FP32;
    string model_path;
    string input_path;
    string golden_path;
    bool relax_computation_float32_to_float16 = true;
    bool print_interpreter_state = false;
    bool allow_threshold = true;
    bool continuous_input = false;
    bool gtest_mode = false;
    int thread_num = 1;
    string model_root_path;
};

static Settings g_settings;

static std::string model_path = "/data/local/tmp/data/tfliterunner/model.lite";
static std::string input_path =
    "/data/local/tmp/data/tfliterunner/batch_xs.npy";
static std::string golden_path = "/data/local/tmp/data/tfliterunner/ys.npy";

Settings processOptions(int argc, char** argv) {
    static struct option long_options[] = {
        {"accelerated", required_argument, 0, 'a'},
        {"specified model", required_argument, 0, 's'},
        {"loop count", required_argument, 0, 'c'},
        {"show output", required_argument, 0, 'o'},
        {"tflite_model", required_argument, 0, 'm'},
        {"batch_xs", required_argument, 0, 'x'},
        {"ys", required_argument, 0, 'y'},
        {"relax computation float32 to float16", required_argument, 0, 'r'},
        {"print interpreter state", required_argument, 0, 'i'},
        {"allow threshold when comparing output", required_argument, 0, 't'},
        {"number of continous input", required_argument, 0, 'n'},
        {"use gtest mode", required_argument, 0, 'g'},
        {"number of threads", required_argument, 0, 'd'},
        {"model_root", required_argument, 0, 'l'},
        {0, 0, 0, 0},
    };

    Settings s;
    int c = -1;
    int option_index = 0;
    s.loop_count = 1;
    s.model_path = model_path;
    s.input_path = input_path;
    s.golden_path = golden_path;
    s.relax_computation_float32_to_float16 = true; // Relax to FP16 by default
    s.allow_threshold = true;
    s.thread_num = 1;

    while (1) {
        /* getopt_long stores the option index here. */
        c = getopt_long(argc, argv, "a:s:c:o:m:x:y:r:i:t:n:g:d:l:", long_options,
                &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c) {
            case 'a':
                s.accel = strtol(optarg, (char**)NULL, 10);
                break;

            case 'c':
                s.loop_count = strtol(optarg, (char**)NULL, 10);
                break;

            case 's':
                s.mode = strtol(optarg, (char**)NULL, 10);
                break;

            case 'o':
                s.output_type = strtol(optarg, (char**)NULL, 10);
                break;

            case 'm':
                s.model_path = optarg;
                break;

            case 'x':
                s.input_path = optarg;
                break;

            case 'y':
                s.golden_path = optarg;
                break;

            case 'r':
                s.relax_computation_float32_to_float16 = strtol(optarg, (char**)NULL, 10);
                break;

            case 'i':
                s.print_interpreter_state = strtol(optarg, (char**)NULL, 10);
                break;

            case 't':
                s.allow_threshold = strtol(optarg, (char**)NULL, 10);
                break;

            case 'n':
                s.continuous_input = strtol(optarg, (char**)NULL, 10);
                break;

            case 'g':
                s.gtest_mode = strtol(optarg, (char**)NULL, 10);
                break;

            case 'd':
                s.thread_num = strtol(optarg, (char**)NULL, 10);
                break;

            case 'l':
                s.model_root_path = optarg;
                break;

            default:
                exit(-1);
        }
    }

    cout << "Loop count: " << s.loop_count << endl;
    cout << "Use NNAPI: " << s.accel << endl;
    cout << "Mode: " << s.mode << endl;
    cout << "Output type: " << s.output_type << endl;
    cout << "Allow threshold when comparing outout: " << s.allow_threshold << endl;

    return s;
}

void runThread(TestItem* item, InterpreterWrapperConfig* config, int& result) {
    result = runInterpreter(item, config);
    cout << "From thread ID: " << hex << std::this_thread::get_id() << " result: "
        << dec << result << endl;
}

int runUserSpecified(TestItem* item, InterpreterWrapperConfig* config) {
    std::vector<std::thread> interpreter_threads; // Create a vector of threads
    std::vector<int> results(g_settings.thread_num); // Create a vector of threads
    int ret = 0;

    for (int i = 0; i < g_settings.thread_num; i++) {
        std::thread th(runThread, item, config, std::ref(results[i]));
        interpreter_threads.push_back(std::move(th));
    }

    // Iterate over the thread vector
    for (std::thread& th : interpreter_threads) {
        // If thread object is joinable then join that thread.
        if (th.joinable()) {
            th.join();
        }
    }

    if (std::all_of(results.cbegin(), results.cend(),
            [](int result) {return result != 0;})) {
        ret = 1;
    }

    interpreter_threads.clear();
    results.clear();
    return ret;
}

int run() {
    InterpreterWrapperConfig c;
    int ret = 0;

    c.use_nnapi = g_settings.accel;
    c.show_output = (g_settings.output_type == SHOW_OUTPUT ? true : false);
    c.save_output = (g_settings.output_type == SAVE_OUTPUT ? true : false);
    c.loop_count = g_settings.loop_count;
    c.relax_computation_float32_to_float16 =
        g_settings.relax_computation_float32_to_float16;
    c.print_interpreter_state = g_settings.print_interpreter_state;
    c.allow_threshold = g_settings.allow_threshold;
    c.continuous_input = g_settings.continuous_input;
    c.break_on_failure = g_settings.gtest_mode;

    switch (g_settings.mode) {
        case DEFAULT_MODE: {
            // Glob all sub folders in the root path
            cout << "glob with model root: " << g_settings.model_root_path << endl;
            string model_root_pattern = g_settings.model_root_path + string("*/");
            // glob struct resides on the stack
            glob_t glob_result;
            memset(&glob_result, 0, sizeof(glob_result));

            // do the glob operation
            int return_value = glob(model_root_pattern.c_str(), GLOB_TILDE, NULL,
                    &glob_result);

            if (return_value != 0) {
                globfree(&glob_result);
                cout << "glob() failed with return_value " << return_value << endl;
                ret = 1;
                break;
            }

            for (size_t i = 0; i < glob_result.gl_pathc; ++i) {
                string model_prefix = string(glob_result.gl_pathv[i]) + g_settings.model_path;
                string input_prefix = string(glob_result.gl_pathv[i]) + g_settings.input_path;
                string golden_prefix = string(glob_result.gl_pathv[i]) + g_settings.golden_path;

                TestItem item = {
                    "",
                    model_prefix.c_str(),
                    input_prefix.c_str(),
                    golden_prefix.c_str(),
                    g_settings.data_type,
                    g_settings.data_type,
                };
                ret = runUserSpecified(&item, &c);

                if (ret != 0) {
                    break;
                }
            }

            // cleanup
            globfree(&glob_result);
        }
        break;

        case SPECIFIED_MODEL_MODE: {
            TestItem item = {
                "",
                g_settings.model_path.c_str(),
                g_settings.input_path.c_str(),
                g_settings.golden_path.c_str(),
                g_settings.data_type,
                g_settings.data_type,
            };

            ret = runUserSpecified(&item, &c);
        }
        break;

        case SPECIFIED_MODEL_WITH_RANDOM_INPUT: {
            TestItem item = {
                "",
                g_settings.model_path.c_str(),
                nullptr,
                nullptr,
                g_settings.data_type,
                g_settings.data_type,
            };

            ret = runUserSpecified(&item, &c);
        }
        break;

        default:
            break;
    }

    return ret;
}

TEST(TFLiteRunner, run) {
    int ret = run();
    EXPECT_EQ(0, ret);
}

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    g_settings = processOptions(argc, argv);

    int ret = 0;

    if (g_settings.gtest_mode) {
        ret = RUN_ALL_TESTS();
    } else {
        ret = run();
    }

    _exit(0); // Call exit here to prevent cleanup/finalize. Let kernel do the cleanup.
    return ret;
}

