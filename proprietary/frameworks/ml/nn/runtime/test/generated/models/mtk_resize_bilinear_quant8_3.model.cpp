// Generated file (from: mtk_resize_bilinear_quant8_3.mod.py). Do not edit
void CreateModel(Model *model) {
  OperandType type2(Type::INT32, {});
  OperandType type0(Type::TENSOR_QUANT8_ASYMM, {1, 256, 256, 1}, 1.0f, 0);
  OperandType type1(Type::TENSOR_QUANT8_ASYMM, {1, 480, 480, 1}, 1.0f, 0);
  // Phase 1, operands
  auto op1 = model->addOperand(&type0);
  auto op2 = model->addOperand(&type1);
  auto width = model->addOperand(&type2);
  auto height = model->addOperand(&type2);
  // Phase 2, operations
  static int32_t width_init[] = {480};
  model->setOperandValue(width, width_init, sizeof(int32_t) * 1);
  static int32_t height_init[] = {480};
  model->setOperandValue(height, height_init, sizeof(int32_t) * 1);
  model->addOperation(ANEURALNETWORKS_RESIZE_BILINEAR, {op1, width, height}, {op2});
  // Phase 3, inputs and outputs
  model->identifyInputsAndOutputs(
    {op1},
    {op2});
  assert(model->isValid());
}

bool is_ignored(int i) {
  static std::set<int> ignore = {};
  return ignore.find(i) != ignore.end();
}
