// Generated file (from: mtk_avg_pool_float_random_gen_filter_4_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.2426573112f, 0.3566121164f, 1.992444648f, 1.2822226393f, 0.7021563718f, 1.2416358389f, 0.5919583863f, 0.0791935622f, 0.2236869457f, 0.0264277952f, 1.1530372777f, 0.867914587f, 1.6948078747f, 1.2759742292f, 1.1991557008f, 1.2946061368f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.8857653737068176f, 1.045662760734558f, 0.9864548444747925f, 0.5484766960144043f, 0.7532647848129272f, 0.6730259656906128f, 0.8052242398262024f, 0.9136487245559692f, 1.1286784410476685f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
