// Generated file (from: mtk_softmax_2D_float_random_gen_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.8179308922f, 0.0646354388f, 1.0954351055f, 0.0312213804f, 0.1423216214f, 0.0211464475f, 1.4575397704f, 1.4328052691f, 0.1544127642f, 0.8646019029f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.6475658416748047f, 0.04464425891637802f, 0.21510322391986847f, 0.04242575541138649f, 0.05026096850633621f, 0.04272659122943878f, 0.38218724727630615f, 0.36803585290908813f, 0.05235814303159714f, 0.1546921730041504f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
