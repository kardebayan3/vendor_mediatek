// Generated file (from: vendor_avg_pool_float_random_gen_filter_5_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.2258824484f, 0.9706734256f, 1.6413920364f, 0.2832369346f, 0.8185727565f, 0.0342471476f, 1.8229091121f, 0.1073363719f, 1.4949156007f, 0.4734207296f, 0.4183717469f, 1.1009587853f, 0.1920909402f, 1.7236344284f, 1.8909641336f, 1.5252055463f, 0.6745594691f, 1.5186810196f, 0.2044424729f, 0.3467906231f, 1.3631865107f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.000101387500762939453f, 0.000100493431091308594f, 0.000073671340942382812f, 0.000002264976501464844f, 0.000040173530578613281f, 0.000054478645324707031f, 0.000136196613311767578f, 0.000158369541168212891f, 0.000108361244201660156f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
