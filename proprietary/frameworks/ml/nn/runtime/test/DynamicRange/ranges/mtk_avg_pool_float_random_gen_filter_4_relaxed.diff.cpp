// Generated file (from: vendor_avg_pool_float_random_gen_filter_4_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.2426573112f, 0.3566121164f, 1.992444648f, 1.2822226393f, 0.7021563718f, 1.2416358389f, 0.5919583863f, 0.0791935622f, 0.2236869457f, 0.0264277952f, 1.1530372777f, 0.867914587f, 1.6948078747f, 1.2759742292f, 1.1991557008f, 1.2946061368f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.000023186206817626953f, 0.000235676765441894531f, 0.000126719474792480469f, 0.000136852264404296875f, 0.000153183937072753906f, 0.000174403190612792969f, 0.000048458576202392578f, 0.000413775444030761719f, 0.000227808952331542969f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
