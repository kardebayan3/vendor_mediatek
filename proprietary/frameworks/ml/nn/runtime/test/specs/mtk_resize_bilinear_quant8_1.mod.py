# model
model = Model()
i1 = Input("op1", "TENSOR_QUANT8_ASYMM", "{1, 2, 2, 1}, 1.0f, 0")
i2 = Output("op2", "TENSOR_QUANT8_ASYMM", "{1, 3, 3, 1}, 1.0f, 0")
w = Int32Scalar("width", 3) # an int32_t scalar bias
h = Int32Scalar("height", 3)
model = model.Operation("RESIZE_BILINEAR", i1, w, h).To(i2)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [11, 11, 17, 17]}
output0 = {i2: # output 0
           [11, 11, 11,
            15, 15, 15,
            17, 17, 17]}

input1 = {i1: [0, 0, 255, 255]}
output1 = {i2: [0, 0, 0, 170, 170, 170, 255, 255, 255]}


# Instantiate an example
Example((input0, output0))
Example((input1, output1))
