model = Model()
i1 = Input("input", "TENSOR_QUANT8_ASYMM", "{1, 3, 3, 2}, 1.0, 0")
perms = Parameter("perms", "TENSOR_INT32", "{4}", [0, 2, 3, 1])
output = Output("output", "TENSOR_QUANT8_ASYMM", "{1, 3, 2, 3}, 1.0, 0")

model = model.Operation("TRANSPOSE", i1, perms).To(output)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [1,   2,   3,   4,   5,   6,
           7,   8,   9,  10,  11,  12,
           13, 14,  15,  16,  17,  18]}

output0 = {output: # output 0
          [1,   7,  13,   2,   8,  14,
           3,   9,  15,   4,  10,  16,
           5,  11,  17,   6,  12,  18]}

# Instantiate an example
Example((input0, output0))
