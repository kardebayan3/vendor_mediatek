# model
model = Model()
i1 = Input("op1", "TENSOR_QUANT8_ASYMM", "{1, 2, 2, 2}, 1.0f, 0")
i2 = Output("op2", "TENSOR_QUANT8_ASYMM", "{1, 3, 3, 2}, 1.0f, 0")
w = Int32Scalar("width", 3) # an int32_t scalar bias
h = Int32Scalar("height", 3)
model = model.Operation("RESIZE_BILINEAR", i1, w, h).To(i2)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [0, 11, 255, 11, 0, 17, 255, 17]}
output0 = {i2: # output 0
           [0, 11, 170, 11, 255, 11,
            0, 15, 170, 15, 255, 15,
            0, 17, 170, 17, 255, 17]}

# Instantiate an example
Example((input0, output0))
