model = Model()
i1 = Input("input", "TENSOR_QUANT8_ASYMM", "{1, 3, 3, 1}, 1.0, 0")
perms = Parameter("perms", "TENSOR_INT32", "{4}", [0, 2, 1, 3])
output = Output("output", "TENSOR_QUANT8_ASYMM", "{1, 3, 3, 1}, 1.0, 0")

model = model.Operation("TRANSPOSE", i1, perms).To(output)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [1,   2,   3,   4,   5,   6,   7,   8,   9]}

output0 = {output: # output 0
          [1,   4,   7,   2,   5,   8,   3,   6,   9]}

# Instantiate an example
Example((input0, output0))
