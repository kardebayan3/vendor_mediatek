/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef ANDROID_ML_NN_RUNTIME_NEURO_PILOT_SHIM_LEGACY_H
#define ANDROID_ML_NN_RUNTIME_NEURO_PILOT_SHIM_LEGACY_H

#if __ANDROID_API__ >= __ANDROID_API_O_MR1__

#include "NeuralNetworks.h"
#include "NeuroPilotShim.h"
#include <android/log.h>

// Profiler
typedef int (*ANeuroPilotCompilationLegacy_enableProfiler_fn)(
        ANeuralNetworksCompilation *compilation, uint32_t type);

typedef int (*ANeuroPilotExecutionLegacy_getProfilerInfoCount_fn)(
        ANeuralNetworksExecution *execution, uint32_t *count);

typedef int (*ANeuroPilotExecutionLegacy_getProfilerInfo_fn)(
        ANeuralNetworksExecution *execution, uint32_t index, ProfilerInfo* info);

typedef int (*ANeuroPilotExecutionLegacy_keepOperations_fn)(
        ANeuralNetworksExecution *execution, uint32_t* list, uint32_t listSize);

typedef int (*ANeuroPilotExecutionLegacy_getOperationOutput_fn)(
        ANeuralNetworksExecution *execution, uint32_t keeppingIndex,
        uint32_t outputIndex, uint8_t** buffer);

//////////////////////////////////////////////////////
// Deprecated: Use ANeuroPilotModelWrapper_getAvailableDeviceName
static std::string regDevs;
inline const char* ANeuralNetworks_listRegDevice(void) {
    if (ANeuroPilotModelWrapper_getAvailableDeviceName(regDevs) != ANEURALNETWORKS_NO_ERROR) {
        __android_log_print(ANDROID_LOG_ERROR, "NeuroPilotShimLegacy", "List device error");
    }
    return regDevs.c_str();
}

// Deprecated: Use ANeuroPilotModelWrapper_bindOperationToDeviceByName
inline int ANeuralNetworksModel_setRuntime(ANeuralNetworksModel* model, uint32_t operation,
        const char* deviceName) {
    return ANeuroPilotModelWrapper_bindOperationToDeviceByName(model, operation, deviceName);
}

// Deprecated: Use ANeuroPilotModelWrapper_getAvailableDevice
inline uint32_t ANeuralNetworksModel_getAvailableDevice(void) {
    uint32_t deviceId = ANEURALNETWORKS_CPU;
    if (ANeuroPilotModelWrapper_getAvailableDevice(&deviceId) != ANEURALNETWORKS_NO_ERROR) {
        __android_log_print(ANDROID_LOG_ERROR, "NeuroPilotShimLegacy", "Get device error");
    }
    return deviceId;
}

// Deprecated: Use ANeuroPilotModelWrapper_bindOperationToDevice
inline int ANeuralNetworksModel_bindOperationToDevice(
        ANeuralNetworksModel* model, uint32_t operation, uint32_t deviceId) {
    return ANeuroPilotModelWrapper_bindOperationToDevice(model, operation, deviceId);
}

// Deprecated: Use ANeuroPilotModelWrapper_bindAllOperationsToDevice
inline int ANeuralNetworksModel_bindAllOperationsToDevice(
        ANeuralNetworksModel* model, uint32_t device) {
    return ANeuroPilotModelWrapper_bindAllOperationsToDevice(model, device);
}
/// M: Bind operation @}

// Profiler
// Deprecated: Use ANeuroPilotCompilationWrapper_setPartitionExtType
inline int ANeuralNetworksCompilation_enableProfiler(
        ANeuralNetworksCompilation *compilation, uint32_t type) {
    LOAD_NP_FUNCTION(ANeuroPilotCompilationLegacy_enableProfiler);
    EXECUTE_NP_FUNCTION_RETURN_INT(compilation, type);
}

// Deprecated: Use ANeuroPilotExecutionWrapper_getProfilerInfo
inline int ANeuralNetworksExecution_getProfilerInfoCount(
        ANeuralNetworksExecution *execution, uint32_t *count) {
    LOAD_NP_FUNCTION(ANeuroPilotExecutionLegacy_getProfilerInfoCount);
    EXECUTE_NP_FUNCTION_RETURN_INT(execution, count);
}

// Deprecated: Use ANeuroPilotExecutionWrapper_getProfilerInfo
inline int ANeuralNetworksExecution_getProfilerInfo(
        ANeuralNetworksExecution *execution, uint32_t index, ProfilerInfo* info) {
    LOAD_NP_FUNCTION(ANeuroPilotExecutionLegacy_getProfilerInfo);
    EXECUTE_NP_FUNCTION_RETURN_INT(execution, index, info);
}

// Deprecated: Use ANeuroPilotExecutionWrapper_getProfilerInfo
inline int ANeuralNetworksExecution_keepOperations(
        ANeuralNetworksExecution *execution, uint32_t* list, uint32_t listSize) {
    LOAD_NP_FUNCTION(ANeuroPilotExecutionLegacy_keepOperations);
    EXECUTE_NP_FUNCTION_RETURN_INT(execution, list, listSize);
}

// Deprecated: Use ANeuroPilotExecutionWrapper_getProfilerInfo
inline int ANeuralNetworksExecution_getOperationOutput(
        ANeuralNetworksExecution *execution,
                              uint32_t keeppingIndex,
                              uint32_t outputIndex,
                              uint8_t** buffer) {
    LOAD_NP_FUNCTION(ANeuroPilotExecutionLegacy_getOperationOutput);
    EXECUTE_NP_FUNCTION_RETURN_INT(execution, keeppingIndex, outputIndex, buffer);
}

// Utils
// Deprecated: Use ANeuroPilotUtilsWrapper_setCpuOnly
inline void ANeuralNetworks_setCpuOnly(bool onlyCpu) {
    ANeuroPilotUtilsWrapper_setCpuOnly(onlyCpu);
}

#endif  //  __ANDROID_API__ >= 27
#endif  // ANDROID_ML_NN_RUNTIME_NEURO_PILOT_SHIM_LEGACY_H
