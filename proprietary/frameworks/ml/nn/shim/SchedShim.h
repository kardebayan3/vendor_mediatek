/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef MEDIATEK_ML_NN_RUNTIME_SCHED_SHIM_H
#define MEDIATEK_ML_NN_RUNTIME_SCHED_SHIM_H

#include <dlfcn.h>
#include <android/log.h>

#define ANEUROPILOT_SCHED_NO_ERROR 0
#define ANEUROPILOT_SCHED_ERROR -1
#define ANEUROPILOT_SCHED_FEATURE_NOT_SUPPORT -999

#define ANEUROPILOT_SCHED_PREFER_NONE   0
#define ANEUROPILOT_SCHED_PREFER_BIG    1
#define ANEUROPILOT_SCHED_PREFER_LITTLE 2

#define SCHED_LOG(format, ...) \
    __android_log_print(ANDROID_LOG_DEBUG, "SchedShim", format "\n", ##__VA_ARGS__);

#define LOAD_SCHED_FUNCTION(name) \
  static name##_fn fn = reinterpret_cast<name##_fn>(loadSchedFunction(#name));


#define EXECUTE_SCHED_FUNCTION_RETURN_INT(...) \
    return fn != nullptr ? fn(__VA_ARGS__) : ANEUROPILOT_SCHED_FEATURE_NOT_SUPPORT;

/*************************************************************************************************/
typedef int (*sched_hint_fn)(pid_t pid, int hint);

/*************************************************************************************************/
// For add-on
static void* sSchedHandle;
inline void* loadSchedLibrary(const char* name) {
    sSchedHandle = dlopen(name, RTLD_LAZY | RTLD_LOCAL);
    if (sSchedHandle == nullptr) {
        SCHED_LOG("npapi error: unable to open library %s", name);
    }
    return sSchedHandle;
}

inline void* getSchedLibraryHandle() {
    if (sSchedHandle == nullptr) {
        sSchedHandle = loadSchedLibrary("libneuropilot_sched_hint.so");
    }
    return sSchedHandle;
}

inline void* loadSchedFunction(const char* name) {
    void* fn = nullptr;
    if (getSchedLibraryHandle() != nullptr) {
        fn = dlsym(getSchedLibraryHandle(), name);
    }

    if (fn == nullptr) {
        SCHED_LOG("npapi error: unable to open function %s", name);
    }

    return fn;
}

/*************************************************************************************************/

/**
 * Schedule the given thread to prefer CPU.
 *
 * @param pid "Thread ID"
 *
 * It's a debug function, use turn on feature before using it.
 * Command: adb setprop debug.nn.bindoperation.supported X
 *                 X = 1: on
 *                 X = 0: off
 *
 * @param hint Prefer to schedule CPU to what kind of CPU core?
 *                    SCHED_PREFER_NONE   : System decide
 *                    SCHED_PREFER_BIG    : Prefer to execute on big core
 *                    SCHED_PREFER_LITTLE : Prefer to execute on small core
 *
 * @return SCHED_EXECUTION_NO_ERROR  : Successful.
 *         SCHED_FEATURE_NOT_SUPPORT : The feature is not supported.
 *         SCHED_EXECUTION_ERROR     : Execution error.
 */
inline int ASchedHintWrapper_setSchedHint(pid_t pid, int hint) {
    LOAD_SCHED_FUNCTION(sched_hint);
    EXECUTE_SCHED_FUNCTION_RETURN_INT(pid, hint);
}

#endif  // MEDIATEK_ML_NN_RUNTIME_SCHED_SHIM_H
