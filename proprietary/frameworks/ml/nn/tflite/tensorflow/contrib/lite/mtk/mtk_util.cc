/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include "tensorflow/contrib/lite/nnapi_delegate.h"
#define LOG_TAG "MtkUtil"
#include "mtk_log.h"
#include "mtk_util.h"
#include <cstdlib>
#include <unordered_map>

namespace tflite {
namespace mtk {

static bool sOptions[26] = {false};
static bool sReadProperty = false;

static std::unordered_map<NNAPIDelegate*, bool> g_exec_parallel_;
void SetExecParallel(void* key, bool enableParallel) {
  NNAPIDelegate *delegate = reinterpret_cast<NNAPIDelegate*>(key);
  LOG_D("Set exec parallel for key(%lu): %d", (unsigned long)delegate, enableParallel);
  if (!enableParallel) {
    g_exec_parallel_.erase(delegate);
  } else {
    g_exec_parallel_[delegate] = enableParallel;
  }
}

bool GetExecParallel(void* key) {
  NNAPIDelegate *delegate = reinterpret_cast<NNAPIDelegate*>(key);
  auto it = g_exec_parallel_.find(delegate);
  bool execParallel = (it != g_exec_parallel_.end() ? it->second : false);
  LOG_D("Get exec parallel for key(%lu): %d", (unsigned long)delegate, execParallel);
  return execParallel;
}

int32_t Hash(const char* str) {
  int32_t hash = 5381;
  int c;
  size_t count = 0;
  size_t len = strlen(str);
  LOG_D("Hash string: %s", str);

  while (count < len) {
    c = *(str + count);
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    count++;
  }

  return hash;
}

/*
 * The format of data will be:
 *  -------------------------------------------------------------------------------
 *  | 1 byte typeLen  | N bytes type     | 4 bytes dataLen  | N bytes data        |
 *  -------------------------------------------------------------------------------
 */
int EncodeOperandValue(OemOperandValue* operand, uint8_t* output) {
  size_t currPos = 0;

  // 1 byte for typeLen, 4 bytes for bufferLen
  if (output == nullptr) {
    return -1;
  }

  // Set length of type
  *output = operand->typeLen;
  currPos += sizeof(uint8_t);

  // Copy type to output
  memcpy(output + currPos, operand->type, operand->typeLen);
  currPos += operand->typeLen;

  // Set the length of buffer
  uint32_t *dataLen = (uint32_t *) &output[currPos];
  *dataLen = operand->dataLen;
  currPos += sizeof(uint32_t);

  // Copy  operand value to output
  memcpy(&output[currPos], operand->data, operand->dataLen);

  return 0;
}

int DecodeOperandValue(uint8_t *input, OemOperandValue *operand) {
  size_t currPos = 0;

  // Get Type length
  operand->typeLen = *input;
  currPos += sizeof(uint8_t);

  // Get Types
  operand->type = (uint8_t*) malloc(operand->typeLen);
  memcpy(operand->type, &input[currPos], operand->typeLen);
  currPos += operand->typeLen;

  // Get Buffer length
  uint32_t *dataLen = (uint32_t *) &input[currPos];
  operand->dataLen = *dataLen;
  currPos += sizeof(uint32_t);

  // Get Buffer
  operand->data = (uint8_t*)malloc(operand->dataLen);
  memcpy(operand->data, &input[currPos], operand->dataLen);

  return 0;
}

size_t PackOemScalarString(const char* str, uint8_t** out_buffer) {
  size_t out_len = 0;
  uint8_t type[] = {'s', 't', 'r', 'i', 'n', 'g'};
  OemOperandValue operand_value;

  operand_value.typeLen = sizeof(type);
  operand_value.type = type;
  operand_value.dataLen = strlen(str);
  operand_value.data = (uint8_t*)malloc(operand_value.dataLen);
  memcpy(operand_value.data, str, operand_value.dataLen);

  out_len = operand_value.typeLen + operand_value.dataLen + (sizeof(size_t) * 2);
  *out_buffer = (uint8_t*)calloc(out_len, sizeof(uint8_t));
  EncodeOperandValue(&operand_value, *out_buffer);
  free(operand_value.data);

  LOG_D("PackOemScalarString: %s, buffer size:%zu", str, out_len);
  return out_len;
}

bool IsPropertyEnabled(const char* key) {
  char var[PROP_VALUE_MAX];
  int length = __system_property_get(key, var);
  if (length != 0) {
    return (atoi(var) > 0);
  }
  return false;
}

static std::vector<std::string> Split(const std::string& s, char delimiter) {
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

static void InitOptions(const char* key) {
  char var[PROP_VALUE_MAX];
  int length = __system_property_get(key, var);

  if (length == 0) {
    LOG_D("Empty NeuroPilot feature property");
    return;
  }

  const std::string vOptionSetting = std::string(var);
  std::vector<std::string> elements = Split(vOptionSetting, ',');
  for (const auto& elem : elements) {
    if (elem.length() != 1) {
      //LOG_E("Unknown property: %s", elem);
      continue;
    }

    int index = -1;
    char c = elem.at(0);
    index = tolower(c) - 'a';
    if (index < 0 || index > 25) {
      LOG_E("Invaild index: %d", index);
      continue;
    }
    LOG_D("Property index: %d", index);
    sOptions[index] = true;
  }
}

static bool OverwriteOptions() {
    char var[PROP_VALUE_MAX];
    if (__system_property_get("debug.nn.mtk_nn.option", var) != 0) {
      LOG_D("For debug: feature support by debug proerty: %s", var);

      // Reset old data first.
      for (int i = 0; i <= 25; i++) {
        sOptions[i] = false;
      }
      InitOptions("debug.nn.mtk_nn.option");
      return true;
    }
    return false;
}

bool IsFeatureSupported(int feature) {
    if (feature < 0 || feature > 25) {
        LOG_E("Unknown feature: %d", feature);
        return false;
    }

    if (OverwriteOptions()) {
        return sOptions[feature];
    }

    if (!sReadProperty) {
        InitOptions("ro.vendor.mtk_nn.option");
        sReadProperty = true;
    }

    return sOptions[feature];
}

}
}  // namespace tflite
