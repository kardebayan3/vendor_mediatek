/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include <unistd.h>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <limits>

#include "tensorflow/contrib/lite/builtin_op_data.h"
#include "tensorflow/contrib/lite/context.h"
#include "tensorflow/contrib/lite/model.h"
#include "tensorflow/contrib/lite/mtk/kernels/internal/reference/mtk_reference_ops.h"
#include "tensorflow/contrib/lite/kernels/internal/quantization_util.h"
#include "tensorflow/contrib/lite/kernels/internal/tensor.h"
#include "tensorflow/contrib/lite/kernels/kernel_util.h"
#include "tensorflow/contrib/lite/kernels/op_macros.h"
#include "tensorflow/contrib/lite/kernels/padding.h"
#include "tensorflow/contrib/lite/nnapi/NeuralNetworksShim.h"

#include "mtk_log.h"
#define LOG_TAG "MtkMinPooling"
#include "tensorflow/contrib/lite/mtk/mtk_util.h"
#include "tensorflow/contrib/lite/mtk/kernels/mtk_ops.h"

#include "flatbuffers/flexbuffers.h"

namespace tflite {
namespace ops {
namespace mtk {
namespace pooling {

#define CHECK_NN(x)                                         \
  if (x != ANEURALNETWORKS_NO_ERROR) {                      \
    LOG_E("Aborting since NN returned failure.");           \
    std::cout << "Aborting since NN returned failure."      \
              << __FILE__ << ":" << __LINE__ << std::endl;  \
    exit(1);                                                \
  }

constexpr int kOutputShapeTensor = 0;
constexpr int kWeightsTensor = 1;
constexpr int kDataInputTensor = 2;
constexpr int kBiasTensor = 3;
constexpr int kOutputTensor = 0;

// This file has two implementation of each pooling op.
enum KernelType {
  kReference,
  kGenericOptimized,
};

enum PoolType {
  kMin,
};


void* Init(TfLiteContext* context, const char* buffer, size_t length) {
  // This is a builtin op, so we don't use the contents in 'buffer', if any.
  // Instead, we allocate a new object to carry information from Prepare() to
  // Eval().
  auto parse_padding = [](Padding padding) {
    switch (padding) {
      case Padding_SAME:
        return kTfLitePaddingSame;
      case Padding_VALID:
        return kTfLitePaddingValid;
    }
    return kTfLitePaddingUnknown;
  };
  auto parse_activation = [](ActivationFunctionType activation) {
    switch (activation) {
      case ActivationFunctionType_NONE:
        return kTfLiteActNone;
      case ActivationFunctionType_RELU:
        return kTfLiteActRelu;
      case ActivationFunctionType_RELU_N1_TO_1:
        return kTfLiteActRelu1;
      case ActivationFunctionType_RELU6:
        return kTfLiteActRelu6;
      case ActivationFunctionType_TANH:
        return kTfLiteActTanh;
      case ActivationFunctionType_SIGN_BIT:
        return kTfLiteActSignBit;
    }
    return kTfLiteActNone;
  };

  OpData* data = new OpData;
  const uint8_t* buffer_t = reinterpret_cast<const uint8_t*>(buffer);
  const flexbuffers::Map& m = flexbuffers::GetRoot(buffer_t, length).AsMap();
  data->padding = parse_padding(static_cast<Padding>(m["PaddingType"].AsInt64()));
  data->stride_width = m["stride_width"].AsInt64();
  data->stride_height = m["stride_height"].AsInt64();
  data->kwidth = m["kwidth"].AsInt64();
  data->kheight = m["kheight"].AsInt64();
  data->activation = parse_activation(static_cast<ActivationFunctionType>(m["activation"].AsInt64()));

  return data;
}

void Free(TfLiteContext* context, void* buffer) {
  delete reinterpret_cast<OpData*>(buffer);
}

template <PoolType pool_type>
TfLiteStatus GenericPrepare(TfLiteContext* context, TfLiteNode* node) {
  auto* params = reinterpret_cast<TfLitePoolParams*>(node->builtin_data);
  OpData* data = reinterpret_cast<OpData*>(node->user_data);

  TF_LITE_ENSURE_EQ(context, NumInputs(node), 1);
  TF_LITE_ENSURE_EQ(context, NumOutputs(node), 1);
  TfLiteTensor* output = GetOutput(context, node, 0);
  const TfLiteTensor* input = GetInput(context, node, 0);
  TF_LITE_ENSURE_EQ(context, NumDimensions(input), 4);

  const TfLiteType input_type = input->type;
  const TfLiteType output_type = output->type;

  if (input_type == kTfLiteFloat32) {
    TF_LITE_ENSURE_EQ(context, input->type, output->type);
  }
  else {
    TF_LITE_ENSURE(context, input_type == kTfLiteUInt8 || input_type == kTfLiteInt16);
    TF_LITE_ENSURE(context, output_type == kTfLiteUInt8 || output_type == kTfLiteInt16);
  }

  int batches = input->dims->data[0];
  int height = input->dims->data[1];
  int width = input->dims->data[2];
  int channels_out = input->dims->data[3];

  // Matching GetWindowedOutputSize in TensorFlow.
  auto padding = data->padding;

  auto compute_out_size = [padding](int image_size, int filter_size,
                                    int stride) -> int {
    return padding == kTfLitePaddingSame
               ? (image_size + stride - 1) / stride
               : padding == kTfLitePaddingValid
                     ? (image_size - filter_size + stride) / stride
                     : 0;
  };

  int out_width =
      compute_out_size(width, data->kwidth, data->stride_width);
  int out_height =
      compute_out_size(height, data->kheight, data->stride_height);

  data->paddingValues.height = ComputePadding(data->stride_height, 1, height,
                                        data->kheight, out_height);
  data->paddingValues.width = ComputePadding(data->stride_width, 1, width,
                                       data->kwidth, out_width);

  TfLiteIntArray* output_size = TfLiteIntArrayCreate(4);
  output_size->data[0] = batches;
  output_size->data[1] = out_height;
  output_size->data[2] = out_width;
  output_size->data[3] = channels_out;

  return context->ResizeTensor(context, output, output_size);
}


template <KernelType kernel_type>
void MinEvalFloat(TfLiteContext* context, TfLiteNode* node,
                  TfLitePoolParams* params, OpData* data, const TfLiteTensor* input,
                  TfLiteTensor* output) {
  float activation_min, activation_max;
  CalculateActivationRange(data->activation, &activation_min,
                           &activation_max);
#define TF_LITE_MIN_POOL(type)                                                    \
  tflite::PoolParams op_params;                                                   \
  op_params.stride_height = data->stride_height;                                  \
  op_params.stride_width = data->stride_width;                                    \
  op_params.filter_height = data->kheight;                                        \
  op_params.filter_width = data->kwidth;                                          \
  op_params.padding_values.height = data->paddingValues.height;                   \
  op_params.padding_values.width = data->paddingValues.width;                     \
  op_params.float_activation_min = activation_min;                                \
  op_params.float_activation_max = activation_max;                                \
  reference_ops::mtk::MinPool(                                                     \
      op_params,                                                                  \
      GetTensorShape(input), GetTensorData<float>(input),                         \
      GetTensorShape(output), GetTensorData<float>(output))

  TF_LITE_MIN_POOL();

#undef TF_LITE_MIN_POOL
}

template <KernelType kernel_type>
void MinEvalQuantized(TfLiteContext* context, TfLiteNode* node,
                      TfLitePoolParams* params, OpData* data,
                      const TfLiteTensor* input, TfLiteTensor* output) {
  int32_t activation_min;
  int32_t activation_max;
  CalculateActivationRangeQuantized(context, data->activation, output,
                                    &activation_min, &activation_max);

  const int input_offset = -input->params.zero_point;
  const int output_offset = output->params.zero_point;
  const double input_scale = input->params.scale;
  const double output_scale = output->params.scale;
  const double real_output_multiplier = input_scale / output_scale;
  int32 output_multiplier;
  int output_shift;
  QuantizeMultiplier(real_output_multiplier, &output_multiplier, &output_shift);

#define TF_LITE_MIN_POOL_REQUANT(input_type, output_type)                        \
  do {                                                                           \
    tflite::PoolParams op_params;                                                \
    op_params.stride_height = data->stride_height;                               \
    op_params.stride_width = data->stride_width;                                 \
    op_params.filter_height = data->kheight;                                     \
    op_params.filter_width = data->kwidth;                                       \
    op_params.padding_values.height = data->paddingValues.height;                \
    op_params.padding_values.width = data->paddingValues.width;                  \
    op_params.input_offset = input_offset;                                       \
    op_params.output_offset = output_offset;                                     \
    op_params.output_multiplier = output_multiplier;                             \
    op_params.output_shift = output_shift;                                       \
    op_params.quantized_activation_min = activation_min;                         \
    op_params.quantized_activation_max = activation_max;                         \
    reference_ops::mtk::nbits::MinPoolRequantize<input_type, output_type>(         \
                  op_params, GetTensorShape(input),                              \
                  GetTensorData<uint8_t>(input), GetTensorShape(output),         \
                  GetTensorData<uint8_t>(output));                               \
  } while (0)

  const auto itype = input->type;
  const auto otype = output->type;

  if (itype == kTfLiteUInt8 && otype == kTfLiteUInt8)
    TF_LITE_MIN_POOL_REQUANT(uint8_t, uint8_t);
  else if (itype == kTfLiteUInt8 && otype == kTfLiteInt16)
    TF_LITE_MIN_POOL_REQUANT(uint8_t, int16_t);
  else if (itype == kTfLiteInt16 && otype == kTfLiteUInt8)
    TF_LITE_MIN_POOL_REQUANT(int16_t, uint8_t);
  else if (itype == kTfLiteInt16 && otype == kTfLiteInt16)
    TF_LITE_MIN_POOL_REQUANT(int16_t, int16_t);

#undef TF_LITE_MIN_POOL_REQUANT
}

template <KernelType kernel_type>
TfLiteStatus MinEval(TfLiteContext* context, TfLiteNode* node) {
  auto* params = reinterpret_cast<TfLitePoolParams*>(node->builtin_data);
  OpData* data = reinterpret_cast<OpData*>(node->user_data);

  TfLiteTensor* output = GetOutput(context, node, 0);
  const TfLiteTensor* input = GetInput(context, node, 0);
  switch (input->type) {  // Already know in/out types are same.
    case kTfLiteFloat32:
      MinEvalFloat<kernel_type>(context, node, params, data, input, output);
      break;
    case kTfLiteInt16:
    case kTfLiteUInt8:
      MinEvalQuantized<kernel_type>(context, node, params, data, input, output);
      break;
    default:
      context->ReportError(context, "Type not currently supported.");
      return kTfLiteError;
  }
  return kTfLiteOk;
}

}  // namespace pooling

TfLiteRegistration* Register_MIN_POOL_REF() {
  static TfLiteRegistration r = {pooling::Init, pooling::Free,
                                 pooling::GenericPrepare<pooling::kMin>,
                                 pooling::MinEval<pooling::kReference>};
  return &r;
}

TfLiteRegistration* Register_MTK_MIN_POOL_2D() {
  return Register_MIN_POOL_REF();
}

}  // namespace mtk
}  // namespace ops

int32_t add_min_pooling_params(ANeuralNetworksModel* nn_model,
        std::vector<uint32_t>& augmented_inputs, uint32_t& next_id, void* data) {
  auto add_scalar_int32 = [&nn_model, &augmented_inputs,
                           &next_id](int value) {
    ANeuralNetworksOperandType operand_type{.type = ANEURALNETWORKS_INT32};
    CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
    CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id, &value,
                                                  sizeof(int32_t)))
    augmented_inputs.push_back(next_id++);
  };

  auto builtin = reinterpret_cast<ops::mtk::pooling::OpData*>(data);
  add_scalar_int32(builtin->padding);
  add_scalar_int32(builtin->stride_width);
  add_scalar_int32(builtin->stride_height);
  add_scalar_int32(builtin->kwidth);
  add_scalar_int32(builtin->kheight);
  add_scalar_int32(builtin->activation);
  return mtk::Hash("minpoolingmtk");
}

}  // namespace tflite
