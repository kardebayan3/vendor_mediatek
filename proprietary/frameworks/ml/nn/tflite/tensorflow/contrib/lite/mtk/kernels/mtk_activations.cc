/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <unistd.h>
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <limits>

#include "tensorflow/contrib/lite/builtin_op_data.h"
#include "tensorflow/contrib/lite/context.h"
#include "tensorflow/contrib/lite/kernels/internal/quantization_util.h"
#include "tensorflow/contrib/lite/kernels/internal/reference/reference_ops.h"
#include "tensorflow/contrib/lite/kernels/internal/tensor.h"
#include "tensorflow/contrib/lite/kernels/kernel_util.h"
#include "tensorflow/contrib/lite/kernels/op_macros.h"

// MTK reference op implementation
#include "tensorflow/contrib/lite/mtk/kernels/internal/reference/mtk_reference_ops.h"

namespace tflite {
namespace ops {
namespace mtk {
namespace activations {

struct PreluOpData {
  int32_t pos_output_multiplier = 0;
  int32_t pos_output_shift = 0;
  int32_t neg_output_multiplier = 0;
  int32_t neg_output_shift = 0;
};

void* PreluInit(TfLiteContext* context, const char* buffer, size_t length) {
  // This is a builtin op, so we don't use the contents in 'buffer', if any.
  // Instead, we allocate a new object to carry information from Prepare() to
  // Eval().
  return new PreluOpData;
}

void PreluFree(TfLiteContext* context, void* buffer) {
  delete reinterpret_cast<PreluOpData*>(buffer);
}

TfLiteStatus PreluPrepare(TfLiteContext* context, TfLiteNode* node) {
  PreluOpData* data = reinterpret_cast<PreluOpData*>(node->user_data);

  TF_LITE_ENSURE_EQ(context, NumInputs(node), 2);
  TF_LITE_ENSURE_EQ(context, NumOutputs(node), 1);
  const TfLiteTensor* input = GetInput(context, node, 0);
  TfLiteTensor* output = GetOutput(context, node, 0);
  const TfLiteTensor* alpha = GetInput(context, node, 1);

  output->type = input->type;

  // mtk limitation
  if(input->dims->size != 4) {
    printf("WARNING: MTK only supports PReLU with input-dim == 4!\n");
  }
  if(alpha->dims->size > 1) {
    printf("WARNING: MTK only supports PReLU with alpha-dim <= 1!\n");
  } else {
    if (alpha->dims->size == 1) {
      if((alpha->dims->data[0] != 1) && (alpha->dims->data[0] != input->dims->data[3])) {
        printf("WARNING: MTK only supports PReLU with shape of alpha == 1 or == channel of input!\n");
      }
    }
  }

  // PRelu (parameteric Relu) shares the same alpha value on "shared axis".
  // This means it's always required to "broadcast" alpha values in PRelu.
  TfLiteIntArray* output_size = nullptr;
  TF_LITE_ENSURE_OK(
      context, CalculateShapeForBroadcast(context, input, alpha, &output_size));

  if (input->type != kTfLiteFloat32) {
    float neg_multiplier = input->params.scale * alpha->params.scale / output->params.scale;
    float pos_multiplier = input->params.scale / output->params.scale;

    TF_LITE_ENSURE(context, neg_multiplier <  1);

    QuantizeMultiplierSmallerThanOneExp(neg_multiplier, &data->neg_output_multiplier, &data->neg_output_shift);
    data->neg_output_shift *= -1;
    if (pos_multiplier > 1){
        QuantizeMultiplierGreaterThanOne(pos_multiplier, &data->pos_output_multiplier, &data->pos_output_shift);
    } else if (pos_multiplier < 1){
        QuantizeMultiplierSmallerThanOneExp(pos_multiplier, &data->pos_output_multiplier, &data->pos_output_shift);
    } else { // equal to one
        data->pos_output_multiplier = 0;
        data->pos_output_shift = 0;
    }
  }

  TF_LITE_ENSURE_OK(context,
                    context->ResizeTensor(context, output, output_size));
  // After broadcasting, the output shape should always be the same as the
  // input shape.
  TF_LITE_ENSURE(context, HaveSameShapes(input, output));

  return kTfLiteOk;
}

template <typename T>
T ApplyPrelu(T input, T alpha) {
  return input >= 0.0 ? input : input * alpha;
}

TfLiteStatus PreluEval(TfLiteContext* context, TfLiteNode* node) {
  const TfLiteTensor* input = GetInput(context, node, 0);
  const TfLiteTensor* alpha = GetInput(context, node, 1);
  TfLiteTensor* output = GetOutput(context, node, 0);

  PreluOpData* data = reinterpret_cast<PreluOpData*>(node->user_data);

  /*
  if (input->type != kTfLiteFloat32) {
    context->ReportError(context, "Only float32 supported currently, got %d.",
                         input->type);
    return kTfLiteError;
  }
  */

  switch (input->type) {
    case kTfLiteFloat32: {
      reference_ops::BroadcastBinaryFunction<float, float, float>(
          GetTensorData<float>(input), GetTensorDims(input),
          GetTensorData<float>(alpha), GetTensorDims(alpha),
          GetTensorData<float>(output), GetTensorDims(output), ApplyPrelu<float>);
      return kTfLiteOk;
    } break;
    case kTfLiteUInt8: {
      reference_ops::mtk::PRelu(GetTensorData<uint8_t>(input), GetTensorDims(input),
                               GetTensorData<uint8_t>(output), GetTensorDims(output),
                               GetTensorData<uint8_t>(alpha), GetTensorDims(alpha),
                               input->params.zero_point, output->params.zero_point,
                               alpha->params.zero_point,
                               data->pos_output_multiplier, data->pos_output_shift,
                               data->neg_output_multiplier, data->neg_output_shift);
      return kTfLiteOk;
    } break;
    default:
      context->ReportError(context, "Only float32 and uint8 supported currently, got %d.",
                           input->type);
      return kTfLiteError;
  }
}
}  // namespace activations

TfLiteRegistration* Register_MTK_PRELU() {
  static TfLiteRegistration r = {activations::PreluInit, activations::PreluFree,
                                 activations::PreluPrepare,
                                 activations::PreluEval};
  return &r;
}

}  // namespace mtk
}  // namespace ops
}  // namespace tflite