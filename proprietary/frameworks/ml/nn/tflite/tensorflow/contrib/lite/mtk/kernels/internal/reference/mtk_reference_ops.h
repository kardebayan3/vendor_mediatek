/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#ifndef TENSORFLOW_CONTRIB_LITE_KERNELS_INTERNAL_REFERENCE_MTK_REFERENCE_OPS_H_
#define TENSORFLOW_CONTRIB_LITE_KERNELS_INTERNAL_REFERENCE_MTK_REFERENCE_OPS_H_

#include "tensorflow/contrib/lite/kernels/internal/reference/reference_ops.h"

namespace tflite {
namespace reference_ops {
namespace mtk {

inline void LeakyRelu(const uint8_t* input_data, const Dims<4>& input_dims,
               uint8_t* output_data, const Dims<4>& output_dims,
               int32_t input_zero_point, int32_t output_zero_point,
               int32_t pos_output_multiplier, int32_t pos_output_shift,
               int32_t neg_output_multiplier, int32_t neg_output_shift
               ) {
  const int batches = MatchingArraySize(input_dims, 3, output_dims, 3);
  const int height = MatchingArraySize(input_dims, 2, output_dims, 2);
  const int width = MatchingArraySize(input_dims, 1, output_dims, 1);
  const int depth = MatchingArraySize(input_dims, 0, output_dims, 0);
  for (int b = 0; b < batches; ++b) {
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        for (int c = 0; c < depth; ++c) {
            int32_t input_val = input_data[Offset(input_dims, c, x, y, b)] - input_zero_point;
            int32_t unclamped_output;
            if (input_val >= 0) {  // Positive Case
              if (pos_output_multiplier == 0 && pos_output_shift == 0) {
                  // Special Case, pos_multiplier == 1
                  unclamped_output = (input_val + output_zero_point);
              } else if (pos_output_shift <= 0) {    // SmallerThanOne
                  int32_t pos_scaled = MultiplyByQuantizedMultiplierSmallerThanOneExp(input_val,
                                          pos_output_multiplier, -1 * pos_output_shift);
                  unclamped_output = pos_scaled + output_zero_point;
              } else {    // GreaterThanOne
                  int32_t pos_scaled = MultiplyByQuantizedMultiplierGreaterThanOne(input_val,
                                          pos_output_multiplier, pos_output_shift);
                  unclamped_output = pos_scaled + output_zero_point;
              }
            } else {  // Negative Case
              int32_t neg = 255 * input_val;    // map float-alpha to 255
              int32_t neg_scaled = MultiplyByQuantizedMultiplierSmallerThanOneExp(
                  neg, neg_output_multiplier, neg_output_shift);
              unclamped_output = neg_scaled + output_zero_point;
            }
          int32_t clamped_output = std::min(255, std::max(0, unclamped_output));
          output_data[Offset(output_dims, c, x, y, b)] =
              static_cast<uint8>(clamped_output);
        }
      }
    }
  }
}

inline void TransposeConv(const float* input_data, const Dims<4>& input_dims,
                          const float* filter_data, const Dims<4>& filter_dims,
                          const float* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, float* output_data,
                          const Dims<4>& output_dims, float* /*im2col_data*/,
                          const Dims<4>& /*im2col_dims*/) {
  const int batches = MatchingArraySize(input_dims, 3, output_dims, 3);
  const int input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);
  const int output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);
  const int input_height = ArraySize(input_dims, 2);
  const int input_width = ArraySize(input_dims, 1);
  const int filter_height = ArraySize(filter_dims, 2);
  const int filter_width = ArraySize(filter_dims, 1);
  const int output_height = ArraySize(output_dims, 2);
  const int output_width = ArraySize(output_dims, 1);

  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 3), ArraySize(bias_dims, 0));
  }

  // Although transpose convolution simplifies to convolution with transposed
  // weights for strides of 1, non-unitary striding complicates matters. To
  // keep this reference implementation as clear as possible, we use a
  // "scatter" access pattern, where we loop through all the input elements,
  // computing their influence on the output, rather than looping through the
  // output elements in the typical "gather" access pattern of a conv. We
  // therefore must initialize the output array to zero.
  for (int batch = 0; batch < batches; ++batch) {
    for (int in_y = 0; in_y < output_height; ++in_y) {
      for (int in_x = 0; in_x < output_width; ++in_x) {
        for (int out_channel = 0; out_channel < output_depth; ++out_channel) {
          const int output_index = Offset(output_dims, out_channel, in_x, in_y, batch);
          if (bias_data) {
            output_data[output_index] = bias_data[out_channel];
          } else {
            output_data[output_index] = 0.0f;
          }
        }
      }
    }
  }

  // Loop through input elements one at a time.
  for (int batch = 0; batch < batches; ++batch) {
    for (int in_y = 0; in_y < input_height; ++in_y) {
      for (int in_x = 0; in_x < input_width; ++in_x) {
        for (int in_channel = 0; in_channel < input_depth; ++in_channel) {
          // Loop through the output elements it will influence
          const int out_x_origin = (in_x * stride_width) - pad_width;
          const int out_y_origin = (in_y * stride_height) - pad_height;
          for (int filter_y = 0; filter_y < filter_height; ++filter_y) {
            for (int filter_x = 0; filter_x < filter_width; ++filter_x) {
              for (int out_channel = 0; out_channel < output_depth;
                   ++out_channel) {
                // Compute output element location
                const int out_x = out_x_origin + filter_x;
                const int out_y = out_y_origin + filter_y;
                // We cannot accumulate out of bounds
                if ((out_x >= 0) && (out_x < output_width) && (out_y >= 0) &&
                    (out_y < output_height)) {
                  float input_value = input_data[Offset(input_dims, in_channel,
                                                        in_x, in_y, batch)];
                  float filter_value =
                      filter_data[Offset(filter_dims, in_channel, filter_x,
                                         filter_y, out_channel)];
                  output_data[Offset(output_dims, out_channel, out_x, out_y,
                                     batch)] += input_value * filter_value;
                }
              }
            }
          }
        }
      }
    }
  }
}

inline void TransposeConv(const float* input_data, const Dims<4>& input_dims,
                          const float* filter_data, const Dims<4>& filter_dims,
                          const float* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, float output_activation_min,
                          float output_activation_max, float* output_data,
                          const Dims<4>& output_dims) {
  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 0), ArraySize(bias_dims, 0));
  }

  int32 input_batches = ArraySize(input_dims, 3);
  int32 input_height = ArraySize(input_dims, 2);
  int32 input_width = ArraySize(input_dims, 1);
  int32 input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);
  //printf("input shape=(%d,%d,%d,%d)\n", input_depth, input_width, input_height, input_batches);

  int32 filter_height = ArraySize(filter_dims, 2);
  int32 filter_width = ArraySize(filter_dims, 1);
  //printf("filter shape=(%d,%d,%d,%d)\n", filter_dims.sizes[0], filter_dims.sizes[1], filter_dims.sizes[2], filter_dims.sizes[3]);

  int32 output_height = ArraySize(output_dims, 2);
  int32 output_width = ArraySize(output_dims, 1);
  int32 output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);
  //printf("output shape=(%d,%d,%d,%d)\n", output_dims.sizes[0], output_dims.sizes[1], output_dims.sizes[2], output_dims.sizes[3]);

  int filter_left_offset =
    // std::max(((input_width - 1) * stride_width + filter_width - output_width + 1) / 2, 0);
    std::max(((input_width - 1) * stride_width + filter_width - output_width) / 2, 0);
  int filter_top_offset =
    // std::max(((input_height - 1) * stride_height + filter_height - output_height + 1) / 2, 0);
    std::max(((input_height - 1) * stride_height + filter_height - output_height) / 2, 0);
  // printf("filter_left_offset=%d, filter_top_offset=%d\n", filter_left_offset, filter_top_offset);

  for (int batch = 0; batch < input_batches; batch++) {
    // For each channel in the output (which is the input of the conv2d).
    for (int c = 0; c < output_depth; c++) {
      // NOTE: output_data should be initialized to 0 or there will
      // be some issues (weird number) during accumulation.
      for (int x = 0; x < output_height; x++) {
        for (int y = 0; y < output_width; y++) {
          const int output_index = Offset(output_dims, c, y, x, batch);
          if (bias_data) {
            // Assign bias directly.
            output_data[output_index] = bias_data[c];
          } else {
            output_data[output_index] = 0.0f;
          }
        }
      }
      // We know that output_data is initialized as an array with zeros
      // h and w are the coordinate for an element in the gradient of output
      // (input_data)
      for (int h = 0; h < input_height; h++) {
        for (int w = 0; w < input_width; w++) {
          // x and y are the coordinate of the center of the kernel that
          // outputs the element at (h, w).
          int x = filter_height / 2 + h * stride_height- filter_top_offset;
          int y = filter_width / 2 + w * stride_width- filter_left_offset;
          //printf("input(n,h,w)=(%d,%d,%d),  output_central(x,y,c) = (%d,%d,%d)\n", batch,h,w, x,y,c);
          if ((h >= 0) && (h < input_height) && (w >= 0) && (w < input_width)){
            for (int kx = 0; kx < filter_height; kx++) {
              for (int ky = 0; ky < filter_width; ky++) {
                int ox = x + kx - filter_height / 2;
                int oy = y + ky - filter_width / 2;
                //printf("k(h,w)=(%d,%d) o(h,w)=(%d,%d)\n",kx,ky, ox,oy);
                if ((ox >= 0) && (ox < output_height) && (oy >= 0) && (oy < output_width)){
                  const int output_index = Offset(output_dims, c, oy, ox, batch);
                  // initialize
                  auto total = output_data[output_index];
                  for (int f = 0; f < input_depth; f++) {
                    const auto input_source_value =
                      input_data[Offset(input_dims, f, w, h, batch)];

                    const auto filter_source_value =
                      filter_data[Offset(filter_dims, f, ky, kx, c)];
                    //printf("acc=%f by in=(%d,%d,%d,%d)=%f, f=(%d,%d,%d)=%f\n",input_source_value * filter_source_value,
                    //        batch,h,w,f, input_source_value, c,kx,ky, filter_source_value);
                    total += input_source_value * filter_source_value;
                  }
                  output_data[output_index] = total;
                  //printf("output_data(%d)=%f\n", output_index, total);
                }
              }
            }
          }
        }
      }
    }
  }
  // Apply activation.
  for (int o_i = 0; o_i < input_batches*output_width*output_height*output_depth; o_i++) {
    output_data[o_i] =
      ActivationFunctionWithMinMax(output_data[o_i],
                                   output_activation_min, output_activation_max);
  }
  return;
}

inline void TransposeConv(const uint8* input_data, const Dims<4>& input_dims,
                          int32 input_offset, const uint8* filter_data,
                          const Dims<4>& filter_dims, int32 filter_offset,
                          const int32* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, int32 output_offset, int32 output_multiplier,
                          int output_shift, int32 output_activation_min,
                          int32 output_activation_max, uint8* output_data,
                          const Dims<4>& output_dims) {
  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 0), ArraySize(bias_dims, 0));
  }

  int32 input_batches = ArraySize(input_dims, 3);
  int32 input_height = ArraySize(input_dims, 2);
  int32 input_width = ArraySize(input_dims, 1);
  int32 input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);

  int32 filter_height = ArraySize(filter_dims, 2);
  int32 filter_width = ArraySize(filter_dims, 1);

  int32 output_height = ArraySize(output_dims, 2);
  int32 output_width = ArraySize(output_dims, 1);
  int32 output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);

  //int32 output_data_meta[input_batches*output_height*output_width*output_depth];
  int32* output_data_meta = new int32[input_batches*output_height*output_width*output_depth];

  int filter_left_offset =
    // std::max(((input_width - 1) * stride_width + filter_width - output_width + 1) / 2, 0);
    std::max(((input_width - 1) * stride_width + filter_width - output_width) / 2, 0);
  int filter_top_offset =
    // std::max(((input_height - 1) * stride_height + filter_height - output_height + 1) / 2, 0);
    std::max(((input_height - 1) * stride_height + filter_height - output_height) / 2, 0);

  for (int batch = 0; batch < input_batches; batch++) {
    // For each channel in the output (which is the input of the conv2d).
    for (int c = 0; c < output_depth; c++) {
      // NOTE: output_data should be initialized to 0 or there will
      // be some issues (weird number) during accumulation.
      for (int x = 0; x < output_height; x++) {
        for (int y = 0; y < output_width; y++) {
          const int output_index = Offset(output_dims, c, y, x, batch);
                                   //(batch * output_height * output_width * output_depth) +
                                   //(x * output_width * output_depth) + (y * output_depth) + (c);
          if (bias_data) {
            // Assign bias directly.
            output_data_meta[output_index] = bias_data[c];
          } else {
            output_data_meta[output_index] = 0.0f;
          }
        }
      }
      // We know that output_data is initialized as an array with zeros
      // h and w are the coordinate for an element in the gradient of output
      // (input_data)
      for (int h = 0; h < input_height; h++) {
        for (int w = 0; w < input_width; w++) {
          // x and y are the coordinate of the center of the kernel that
          // outputs the element at (h, w).
          int x = filter_height / 2 + h * stride_height- filter_top_offset;
          int y = filter_width / 2 + w * stride_width- filter_left_offset;
          if ((h >= 0) && (h < input_height) && (w >= 0) && (w < input_width)){
            for (int kx = 0; kx < filter_height; kx++) {
              for (int ky = 0; ky < filter_width; ky++) {
                int ox = x + kx - filter_height / 2;
                int oy = y + ky - filter_width / 2;
                if ((ox >= 0) && (ox < output_height) && (oy >= 0) && (oy < output_width)){
                  const int output_index = Offset(output_dims, c, oy, ox, batch);
                  // initialize
                  int32 total = output_data_meta[output_index];
                  for (int f = 0; f < input_depth; f++) {
                    const auto input_source_value =
                      input_data[Offset(input_dims, f, w, h ,batch)];

                    const auto filter_source_value =
                      filter_data[Offset(filter_dims, f, ky, kx, c)];

                    const int32 input_value =
                      static_cast<int32>(input_source_value) - input_offset;
                    const int32 filter_value =
                      static_cast<int32>(filter_source_value) - filter_offset;

                    total += input_value * filter_value;
                  }
                  output_data_meta[output_index] = total;
                }
              }
            }
          }
        }
      }
    }
  }
  // Apply activation and requant.
  for (int o_i = 0; o_i < input_batches*output_width*output_height*output_depth; o_i++) {
    auto acc = output_data_meta[o_i];
    acc = MultiplyByQuantizedMultiplierSmallerThanOneExp(acc, output_multiplier, ::tflite::reference_ops::kReverseShift * output_shift);
    acc += output_offset;
    acc = std::max(acc, output_activation_min);
    acc = std::min(acc, output_activation_max);
    output_data[o_i] = acc;
  }
  delete [] output_data_meta;
  return;
}

inline void PRelu(const uint8_t* input_data, const Dims<4>& input_dims,
                  uint8_t* output_data, const Dims<4>& output_dims,
                  const uint8_t * alpha_data, const Dims<4>& alpha_dims,
                  int32_t input_zero_point, int32_t output_zero_point,
                  int32_t alpha_zero_point,
                  int32_t pos_output_multiplier, int32_t pos_output_shift,
                  int32_t neg_output_multiplier, int32_t neg_output_shift) {
  const int batches = MatchingArraySize(input_dims, 3, output_dims, 3);
  const int height = MatchingArraySize(input_dims, 2, output_dims, 2);
  const int width = MatchingArraySize(input_dims, 1, output_dims, 1);
  const int depth = MatchingArraySize(input_dims, 0, output_dims, 0);
  for (int b = 0; b < batches; ++b) {
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        for (int c = 0; c < depth; ++c) {
          int32_t input_val = input_data[Offset(input_dims, c, x, y, b)] - input_zero_point;
          int32_t unclamped_output;
          if (input_val >= 0) {// Positive Case
            if (pos_output_multiplier == 0 && pos_output_shift == 0){
              // Special Case, pos_multiplier == 1
              unclamped_output = (input_val + output_zero_point);
            } else if (pos_output_shift <= 0 ){    // SmallerThanOne
              int32_t pos_scaled = MultiplyByQuantizedMultiplierSmallerThanOneExp(input_val,
                                      pos_output_multiplier, pos_output_shift);
              unclamped_output = pos_scaled + output_zero_point;
            } else {    // GreaterThanOne
              int32_t pos_scaled = MultiplyByQuantizedMultiplierGreaterThanOne(input_val,
                                      pos_output_multiplier, pos_output_shift);
              unclamped_output = pos_scaled + output_zero_point;
            }
          } else {// Negative Case
            int32_t alpha_idx_b = b % alpha_dims.sizes[3];
            int32_t alpha_idx_y = y % alpha_dims.sizes[2];
            int32_t alpha_idx_x = x % alpha_dims.sizes[1];
            int32_t alpha_idx_c = c % alpha_dims.sizes[0];
            int32_t alpha_val = alpha_data[Offset(alpha_dims, alpha_idx_c, alpha_idx_x, alpha_idx_y, alpha_idx_b)] - alpha_zero_point;
            int32_t neg = alpha_val * input_val;
            int32_t neg_scaled = MultiplyByQuantizedMultiplierSmallerThanOneExp(neg,
                                    neg_output_multiplier, kReverseShift * neg_output_shift);
            unclamped_output = neg_scaled + output_zero_point;
          }
          int32_t clamped_output = std::min(255, std::max(0, unclamped_output));
          output_data[Offset(output_dims, c, x, y, b)] =
              static_cast<uint8>(clamped_output);
        }
      }
    }
  }
}

template <typename T>
inline void MtkDepthToSpace(const T* input_data, const Dims<4>& input_dims,
                            int block_size, T* output_data,
                            const Dims<4>& output_dims) {
  const int input_depth = ArraySize(input_dims, 0);
  const int input_width = ArraySize(input_dims, 1);
  const int input_height = ArraySize(input_dims, 2);
  const int input_batch = ArraySize(input_dims, 3);

  const int output_depth = ArraySize(output_dims, 0);
  const int output_width = ArraySize(output_dims, 1);
  const int output_height = ArraySize(output_dims, 2);
  const int output_batch = ArraySize(output_dims, 3);

  TFLITE_DCHECK_EQ(input_width * block_size, output_width);
  TFLITE_DCHECK_EQ(input_height * block_size, output_height);
  TFLITE_DCHECK_EQ(input_depth, output_depth * block_size * block_size);
  TFLITE_DCHECK_EQ(input_batch, output_batch);

  for (int out_b = 0; out_b < output_batch; ++out_b) {
    for (int out_h = 0; out_h < output_height; ++out_h) {
      for (int out_w = 0; out_w < output_width; ++out_w) {
        for (int out_d = 0; out_d < output_depth; ++out_d) {
          const int in_d =
              out_d + ((out_h % block_size) * block_size + out_w % block_size) *
                          output_depth;

          const int in_w = out_w / block_size;
          const int in_h = out_h / block_size;
          const int in_b = out_b;

          const int output_index =
              Offset(output_dims, out_d, out_w, out_h, out_b);
          const int input_index = Offset(input_dims, in_d, in_w, in_h, in_b);

          output_data[output_index] = input_data[input_index];
        }
      }
    }
  }
}

inline void MinPool(const PoolParams& params, const RuntimeShape& input_shape,
                    const float* input_data, const RuntimeShape& output_shape,
                    float* output_data) {
  TFLITE_DCHECK_EQ(input_shape.DimensionsCount(), 4);
  TFLITE_DCHECK_EQ(output_shape.DimensionsCount(), 4);
  const int batches = MatchingDim(input_shape, 0, output_shape, 0);
  const int depth = MatchingDim(input_shape, 3, output_shape, 3);
  const int input_height = input_shape.Dims(1);
  const int input_width = input_shape.Dims(2);
  const int output_height = output_shape.Dims(1);
  const int output_width = output_shape.Dims(2);
  const int stride_height = params.stride_height;
  const int stride_width = params.stride_width;
  for (int batch = 0; batch < batches; ++batch) {
    for (int out_y = 0; out_y < output_height; ++out_y) {
      for (int out_x = 0; out_x < output_width; ++out_x) {
        for (int channel = 0; channel < depth; ++channel) {
          const int in_x_origin =
              (out_x * stride_width) - params.padding_values.width;
          const int in_y_origin =
              (out_y * stride_height) - params.padding_values.height;
          // Compute the boundaries of the filter region clamped so as to
          // ensure that the filter window fits in the input array.
          const int filter_x_start = std::max(0, -in_x_origin);
          const int filter_x_end =
              std::min(params.filter_width, input_width - in_x_origin);
          const int filter_y_start = std::max(0, -in_y_origin);
          const int filter_y_end =
              std::min(params.filter_height, input_height - in_y_origin);
          float min = std::numeric_limits<float>::max();
          for (int filter_y = filter_y_start; filter_y < filter_y_end;
               ++filter_y) {
            for (int filter_x = filter_x_start; filter_x < filter_x_end;
                 ++filter_x) {
              const int in_x = in_x_origin + filter_x;
              const int in_y = in_y_origin + filter_y;
              min = std::min(
                  min,
                  input_data[Offset(input_shape, batch, in_y, in_x, channel)]);
            }
          }
          output_data[Offset(output_shape, batch, out_y, out_x, channel)] =
              ActivationFunctionWithMinMax(min, params.float_activation_min,
                                           params.float_activation_max);
        }
      }
    }
  }
}

// TODO:
// box_shape is 2D
// box_index_shape is 1D
// output_size_shape is 1D
inline void RoiAlign(const MtkRoiAlignParams& params,
                     const RuntimeShape& input_shape, const float* input_data,
                     const RuntimeShape& box_shape, const float* box_data,
                     const RuntimeShape& box_index_shape,
                     const int32* box_index_data,
                     const RuntimeShape& output_size_shape,
                     const int32* output_size_data,
                     const RuntimeShape& resized_image_shape,
                     float* resized_image_data,
                     const RuntimeShape& output_shape, float* output_data) {
  int32 input_height = input_shape.Dims(1);
  int32 input_width = input_shape.Dims(2);
  int32 depth = MatchingDim(input_shape, 3, output_shape, 3);

  TFLITE_DCHECK_EQ(output_size_shape.Dims(0), 2);

  int32 batches = MatchingDim(box_shape, 0, output_shape, 0);
  int32 output_height = resized_image_shape.Dims(1);
  int32 output_width = resized_image_shape.Dims(2);
  float ori_height_scale =
      static_cast<float>(input_height - 1) / (output_height - 1);
  float ori_width_scale =
      static_cast<float>(input_width - 1) / (output_width - 1);

  int32 kernel_height = params.kernel_height;
  int32 kernel_width = params.kernel_width;
  for (int b = 0; b < batches; b++) {
    float box_y1 = box_data[b * box_shape.Dims(1) + 0];
    float box_x1 = box_data[b * box_shape.Dims(1) + 1];
    float box_y2 = box_data[b * box_shape.Dims(1) + 2];
    float box_x2 = box_data[b * box_shape.Dims(1) + 3];
    float height_scale = static_cast<float>(box_y2 - box_y1) *
        ori_height_scale;
    float width_scale = static_cast<float>(box_x2 - box_x1) *
        ori_width_scale;
    float offset_y = box_y1 * (input_height - 1);
    float offset_x = box_x1 * (input_width - 1);
    int32 batch_index = box_index_data[b];
    for (int y = 0; y < output_height; y++) {
      float input_y = y * height_scale + offset_y;
      int y0 = std::floor(input_y);
      int y1 = std::min(input_height - 1, y0 + 1);
      for (int x = 0; x < output_width; x++) {
        float input_x = x * width_scale + offset_x;
        int x0 = std::floor(input_x);
        int x1 = std::min(input_width - 1, x0 + 1);
        for (int c = 0; c < depth; c++) {
          resized_image_data[Offset(resized_image_shape, b, y, x, c)] =
              static_cast<float>(
                  input_data[Offset(input_shape, batch_index, y0, x0, c)] *
                  (1 - (input_y - y0)) * (1 - (input_x - x0)) +
                  input_data[Offset(input_shape, batch_index, y1, x0, c)] *
                  (input_y - y0) * (1 - (input_x - x0)) +
                  input_data[Offset(input_shape, batch_index, y0, x1, c)] *
                  (1 - (input_y - y0)) * (input_x - x0) +
                  input_data[Offset(input_shape, batch_index, y1, x1, c)] *
                  (input_y - y0) * (input_x - x0));
        }
      }
    }
  }
  // this is the same reference code as tflite version
  int stride_width = kernel_width;
  int stride_height = kernel_height;
  int pad_width = 0;
  int pad_height = 0;
  int filter_width = kernel_width;
  int filter_height = kernel_height;
  output_height = output_size_data[0];
  output_width = output_size_data[1];
  for (int batch = 0; batch < batches; ++batch) {
    for (int out_y = 0; out_y < output_height; ++out_y) {
      for (int out_x = 0; out_x < output_width; ++out_x) {
        for (int channel = 0; channel < depth; ++channel) {
          const int in_x_origin = (out_x * stride_width) - pad_width;
          const int in_y_origin = (out_y * stride_height) - pad_height;
          // Compute the boundaries of the filter region clamped so as to
          // ensure that the filter window fits in the input array.
          const int filter_x_start = std::max(0, -in_x_origin);
          const int filter_x_end =
              std::min(filter_width, input_width - in_x_origin);
          const int filter_y_start = std::max(0, -in_y_origin);
          const int filter_y_end =
              std::min(filter_height, input_height - in_y_origin);
          float max = std::numeric_limits<float>::lowest();
          for (int filter_y = filter_y_start; filter_y < filter_y_end;
               ++filter_y) {
            for (int filter_x = filter_x_start; filter_x < filter_x_end;
                 ++filter_x) {
              const int in_x = in_x_origin + filter_x;
              const int in_y = in_y_origin + filter_y;
              max = std::max(max, resized_image_data[Offset(
                    resized_image_shape, batch, in_y, in_x, channel)]);
            }
          }
          output_data[Offset(output_shape, batch, out_y, out_x, channel)] = max;
        }
      }
    }
  }
}

inline void RoiAlign(const MtkRoiAlignParams& params,
                     const RuntimeShape& input_shape, const uint8* input_data,
                     const RuntimeShape& box_shape, const uint8* box_data,
                     const RuntimeShape& box_index_shape,
                     const int32* box_index_data,
                     const RuntimeShape& output_size_shape,
                     const int32* output_size_data,
                     const RuntimeShape& resized_image_shape,
                     uint8* resized_image_data,
                     const RuntimeShape& output_shape, uint8* output_data) {
  int32 input_height = input_shape.Dims(1);
  int32 input_width = input_shape.Dims(2);
  int32 depth = MatchingDim(input_shape, 3, output_shape, 3);

  TFLITE_DCHECK_EQ(output_size_shape.Dims(0), 2);

  int32 batches = MatchingDim(box_shape, 0, output_shape, 0);
  int32 output_height = resized_image_shape.Dims(1);
  int32 output_width = resized_image_shape.Dims(2);
  float ori_height_scale =
      static_cast<float>(input_height - 1) / (output_height - 1);
  float ori_width_scale =
      static_cast<float>(input_width - 1) / (output_width - 1);

  int32 kernel_height = params.kernel_height;
  int32 kernel_width = params.kernel_width;
  float box_scale = params.box_scale;
  int32 box_offset = params.box_offset;
  for (int b = 0; b < batches; b++) {
    float box_y1 = static_cast<float>(
        box_data[b * box_shape.Dims(1) + 0] + box_offset) * box_scale;
    float box_x1 = static_cast<float>(
        box_data[b * box_shape.Dims(1) + 1] + box_offset) * box_scale;
    float box_y2 = static_cast<float>(
        box_data[b * box_shape.Dims(1) + 2] + box_offset) * box_scale;
    float box_x2 = static_cast<float>(
        box_data[b * box_shape.Dims(1) + 3] + box_offset) * box_scale;
    float height_scale = static_cast<float>(box_y2 - box_y1) *
        ori_height_scale;
    float width_scale = static_cast<float>(box_x2 - box_x1) *
        ori_width_scale;
    float offset_y = box_y1 * (input_height - 1);
    float offset_x = box_x1 * (input_width - 1);
    int32 batch_index = box_index_data[b];
    for (int y = 0; y < output_height; y++) {
      float input_y = y * height_scale + offset_y;
      int y0 = std::floor(input_y);
      int y1 = std::min(input_height - 1, y0 + 1);
      for (int x = 0; x < output_width; x++) {
        float input_x = x * width_scale + offset_x;
        int x0 = std::floor(input_x);
        int x1 = std::min(input_width - 1, x0 + 1);
        for (int c = 0; c < depth; c++) {
          resized_image_data[Offset(resized_image_shape, b, y, x, c)] =
              static_cast<uint8>(
                  input_data[Offset(input_shape, batch_index, y0, x0, c)] *
                  (1 - (input_y - y0)) * (1 - (input_x - x0)) +
                  input_data[Offset(input_shape, batch_index, y1, x0, c)] *
                  (input_y - y0) * (1 - (input_x - x0)) +
                  input_data[Offset(input_shape, batch_index, y0, x1, c)] *
                  (1 - (input_y - y0)) * (input_x - x0) +
                  input_data[Offset(input_shape, batch_index, y1, x1, c)] *
                  (input_y - y0) * (input_x - x0));
        }
      }
    }
  }
  // this is the same reference code as tflite version
  int stride_width = kernel_width;
  int stride_height = kernel_height;
  int pad_width = 0;
  int pad_height = 0;
  int filter_width = kernel_width;
  int filter_height = kernel_height;
  output_height = output_size_data[0];
  output_width = output_size_data[1];
  for (int batch = 0; batch < batches; ++batch) {
    for (int out_y = 0; out_y < output_height; ++out_y) {
      for (int out_x = 0; out_x < output_width; ++out_x) {
        for (int channel = 0; channel < depth; ++channel) {
          const int in_x_origin = (out_x * stride_width) - pad_width;
          const int in_y_origin = (out_y * stride_height) - pad_height;
          // Compute the boundaries of the filter region clamped so as to
          // ensure that the filter window fits in the input array.
          const int filter_x_start = std::max(0, -in_x_origin);
          const int filter_x_end =
              std::min(filter_width, input_width - in_x_origin);
          const int filter_y_start = std::max(0, -in_y_origin);
          const int filter_y_end =
              std::min(filter_height, input_height - in_y_origin);
          uint8 max = 0;
          for (int filter_y = filter_y_start; filter_y < filter_y_end;
               ++filter_y) {
            for (int filter_x = filter_x_start; filter_x < filter_x_end;
                 ++filter_x) {
              const int in_x = in_x_origin + filter_x;
              const int in_y = in_y_origin + filter_y;
              max = std::max(max, resized_image_data[Offset(
                    resized_image_shape, batch, in_y, in_x, channel)]);
            }
          }
          output_data[Offset(output_shape, batch, out_y, out_x, channel)] = max;
        }
      }
    }
  }
}

namespace nbits {
template <typename input_type, typename filter_type, typename output_type>
inline void TransposeConv(const void* input_data, const Dims<4>& input_dims,
                          int32 input_offset, const void* filter_data,
                          const Dims<4>& filter_dims, int32 filter_offset,
                          const int32* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, int32 output_offset, int32 output_multiplier,
                          int output_shift, int32 output_activation_min,
                          int32 output_activation_max, void* output_data,
                          const Dims<4>& output_dims) {
  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 0), ArraySize(bias_dims, 0));
  }

  const auto* typed_input_data = reinterpret_cast<const input_type*>(input_data);
  const auto* typed_filter_data = reinterpret_cast<const filter_type*>(filter_data);
  auto* typed_output_data = reinterpret_cast<output_type*>(output_data);

  int32 input_batches = ArraySize(input_dims, 3);
  int32 input_height = ArraySize(input_dims, 2);
  int32 input_width = ArraySize(input_dims, 1);
  int32 input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);

  int32 filter_height = ArraySize(filter_dims, 2);
  int32 filter_width = ArraySize(filter_dims, 1);

  int32 output_height = ArraySize(output_dims, 2);
  int32 output_width = ArraySize(output_dims, 1);
  int32 output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);

  //int32 output_data_meta[input_batches*output_height*output_width*output_depth];
  int32* output_data_meta = new int32[input_batches*output_height*output_width*output_depth];

  int filter_left_offset =
    // std::max(((input_width - 1) * stride_width + filter_width - output_width + 1) / 2, 0);
    std::max(((input_width - 1) * stride_width + filter_width - output_width) / 2, 0);
  int filter_top_offset =
    // std::max(((input_height - 1) * stride_height + filter_height - output_height + 1) / 2, 0);
    std::max(((input_height - 1) * stride_height + filter_height - output_height) / 2, 0);

  for (int batch = 0; batch < input_batches; batch++) {
    // For each channel in the output (which is the input of the conv2d).
    for (int c = 0; c < output_depth; c++) {
      // NOTE: output_data should be initialized to 0 or there will
      // be some issues (weird number) during accumulation.
      for (int x = 0; x < output_height; x++) {
        for (int y = 0; y < output_width; y++) {
          const int output_index = Offset(output_dims, c, y, x, batch);
                                   //(batch * output_height * output_width * output_depth) +
                                   //(x * output_width * output_depth) + (y * output_depth) + (c);
          if (bias_data) {
            // Assign bias directly.
            output_data_meta[output_index] = bias_data[c];
          } else {
            output_data_meta[output_index] = 0.0f;
          }
        }
      }
      // We know that output_data is initialized as an array with zeros
      // h and w are the coordinate for an element in the gradient of output
      // (input_data)
      for (int h = 0; h < input_height; h++) {
        for (int w = 0; w < input_width; w++) {
          // x and y are the coordinate of the center of the kernel that
          // outputs the element at (h, w).
          int x = filter_height / 2 + h * stride_height- filter_top_offset;
          int y = filter_width / 2 + w * stride_width- filter_left_offset;
          if ((h >= 0) && (h < input_height) && (w >= 0) && (w < input_width)){
            for (int kx = 0; kx < filter_height; kx++) {
              for (int ky = 0; ky < filter_width; ky++) {
                int ox = x + kx - filter_height / 2;
                int oy = y + ky - filter_width / 2;
                if ((ox >= 0) && (ox < output_height) && (oy >= 0) && (oy < output_width)){
                  const int output_index = Offset(output_dims, c, oy, ox, batch);
                  // initialize
                  int32 total = output_data_meta[output_index];
                  for (int f = 0; f < input_depth; f++) {
                    const auto input_source_value =
                      typed_input_data[Offset(input_dims, f, w, h ,batch)];

                    const auto filter_source_value =
                      typed_filter_data[Offset(filter_dims, f, ky, kx, c)];

                    const int32 input_value =
                      static_cast<int32>(input_source_value) - input_offset;
                    const int32 filter_value =
                      static_cast<int32>(filter_source_value) - filter_offset;

                    total += input_value * filter_value;
                  }
                  output_data_meta[output_index] = total;
                }
              }
            }
          }
        }
      }
    }
  }
  // Apply activation and requant.
  for (int o_i = 0; o_i < input_batches*output_width*output_height*output_depth; o_i++) {
    auto acc = output_data_meta[o_i];
    acc = MultiplyByQuantizedMultiplierSmallerThanOneExp(acc, output_multiplier, ::tflite::reference_ops::kReverseShift * output_shift);
    acc += output_offset;
    acc = std::max(acc, output_activation_min);
    acc = std::min(acc, output_activation_max);
    typed_output_data[o_i] = static_cast<output_type>(acc);
  }
  delete [] output_data_meta;
  return;
}

template <typename input_type, typename output_type>
inline void Requantize(int left_shift, const void* input_data, const Dims<4>& input_dims,
                       int input_offset, int output_offset,
                       int output_multiplier, int output_shift,
                       void* output_data, const Dims<4>& output_dims) {

  const auto* typed_input_data = reinterpret_cast<const input_type*>(input_data);
  auto* typed_output_data = reinterpret_cast<output_type*>(output_data);

  const int flat_size = MatchingFlatSize(output_dims, input_dims);

  for (int i = 0; i < flat_size; i++) {
    const int32 val = typed_input_data[i] + input_offset;
    const int32 shifted_val = val * (1 << left_shift);
    int32 output_val = MultiplyByQuantizedMultiplierSmallerThanOneExp(
        shifted_val, output_multiplier, output_shift) + output_offset;

    output_val = std::max(output_val, int32(std::numeric_limits<output_type>::min()));
    output_val = std::min(output_val, int32(std::numeric_limits<output_type>::max()));

    typed_output_data[i] = static_cast<output_type>(output_val);
  }
}

template <typename input_type, typename output_type>
inline void Abs(const MtkQuantizedAbsParams& params,
                const RuntimeShape& input_shape, const void* input_data,
                const RuntimeShape& output_shape, void* output_data) {

  const auto* typed_input_data =
      reinterpret_cast<const input_type*>(input_data);
  auto* typed_output_data =
      reinterpret_cast<output_type*>(output_data);

  const int flat_size = MatchingFlatSize(output_shape, input_shape);
  for (int i = 0; i < flat_size; i++) {
    const int32 val = std::abs(typed_input_data[i] + params.input_offset);
    int32 output_val =
        MultiplyByQuantizedMultiplier(val, params.output_multiplier,
                                      params.output_shift);

    output_val += params.output_offset;

    output_val = std::max(output_val, params.quantized_activation_min);
    output_val = std::min(output_val, params.quantized_activation_max);
    typed_output_data[i] = static_cast<output_type>(output_val);
  }
}

template <typename input_type, typename output_type>
inline void Elu(const MtkQuantizedEluParams& params,
                const RuntimeShape& input_shape, const void* input_data,
                const RuntimeShape& output_shape, void* output_data) {

  const auto* typed_input_data =
      reinterpret_cast<const input_type*>(input_data);
  auto* typed_output_data =
      reinterpret_cast<output_type*>(output_data);

  const int batches = MatchingDim(input_shape, 0, output_shape, 0);
  const int height = MatchingDim(input_shape, 1, output_shape, 1);
  const int width = MatchingDim(input_shape, 2, output_shape, 2);
  const int depth = MatchingDim(input_shape, 3, output_shape, 3);
  for (int b = 0; b < batches; ++b) {
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        for (int c = 0; c < depth; ++c) {
          const int32 input_val =
              typed_input_data[Offset(input_shape, b, y, x, c)] +
              params.input_offset;
          int32 unclamped_output;
          if (input_val <= -params.input_range_radius) {
            unclamped_output = -params.quantized_one; // quantized negative 1
          } else if (input_val >= 0) {
            const int32 input_val_rescaled =
                MultiplyByQuantizedMultiplierGreaterThanOne(
                    input_val, params.input_multiplier, params.input_shift);
            unclamped_output =
                MultiplyByQuantizedMultiplierSmallerThanOneExp(
                    input_val_rescaled, params.output_multiplier,
                    params.output_shift);
          } else {
            const int32 input_val_rescaled =
                MultiplyByQuantizedMultiplierGreaterThanOne(
                    input_val, params.input_multiplier, params.input_shift);
            using FixedPoint4 = gemmlowp::FixedPoint<int32, 4>;
            using FixedPoint0 = gemmlowp::FixedPoint<int32, 0>;
            const FixedPoint4 input_val_f4 =
                FixedPoint4::FromRaw(input_val_rescaled);
            const FixedPoint0 output_val_f0 =
                gemmlowp::exp_on_negative_values(input_val_f4);
            int32 output_val_rescaled =
                MultiplyByQuantizedMultiplierSmallerThanOneExp(
                    output_val_f0.raw(), params.output_multiplier,
                    params.output_shift);
            using gemmlowp::RoundingDivideByPOT;
            int32 output_val_s32 = RoundingDivideByPOT(output_val_rescaled, 4);
            unclamped_output = output_val_s32 - params.quantized_one;
          }
          unclamped_output += params.output_offset;
          const int32 clamped_output =
              std::min(params.quantized_activation_max, std::max(
                    params.quantized_activation_min, unclamped_output));
          typed_output_data[Offset(output_shape, b, y, x, c)] =
              static_cast<output_type>(clamped_output);
        }
      }
    }
  }
}

template <typename T>
inline void Reverse(const RuntimeShape& input_shape, const T* input_data,
                    const RuntimeShape& axis_shape, const int* axis_data,
                    const RuntimeShape& output_shape, T* output_data) {

  const int batches = MatchingDim(input_shape, 0, output_shape, 0);
  const int height = MatchingDim(input_shape, 1, output_shape, 1);
  const int width = MatchingDim(input_shape, 2, output_shape, 2);
  const int depth = MatchingDim(input_shape, 3, output_shape, 3);

  auto input_dim = input_shape.DimensionsCount();
  auto axis_count = axis_shape.Dims(0);

  // Check if the axis is valid
  for (auto i = 0 ; i < axis_count ; i ++) {
    int current = axis_data[i] < 0 ? (axis_data[i] + input_dim) : axis_data[i];
    TFLITE_DCHECK(current >= 0 && current < input_dim);
  }

  // Resolve the axis to be reversed
  auto* reversed_axis = new bool[input_dim];
  for (auto i = 0 ; i < input_dim ; i ++) {
    reversed_axis[i] = false;
  }
  for (auto i = 0 ; i < axis_count ; i ++) {
    int current = axis_data[i] < 0 ? (axis_data[i] + input_dim) : axis_data[i];
    reversed_axis[current] = true;
  }

  for (int in_b = 0; in_b < batches; ++in_b) {
    const int out_b = (reversed_axis[0]) ? batches - 1 - in_b : in_b;
    for (int in_h = 0; in_h < height; ++in_h) {
      const int out_h = (reversed_axis[1]) ? height - 1 - in_h : in_h;
      for (int in_w = 0; in_w < width; ++in_w) {
        const int out_w = (reversed_axis[2]) ? width - 1 - in_w : in_w;
        for (int in_d = 0; in_d < depth; ++in_d) {
          const int out_d = (reversed_axis[3]) ? depth - 1 - in_d : in_d;

          const int input_index = Offset(input_shape, in_b, in_h, in_w, in_d);
          const int output_index =
              Offset(output_shape, out_b, out_h, out_w, out_d);
          output_data[output_index] = input_data[input_index];
        }
      }
    }
  }

  delete [] reversed_axis;
}

template <typename input_type, typename output_type>
inline void MinPoolRequantize(const PoolParams& params,
                              const RuntimeShape& input_shape,
                              const void* input_data,
                              const RuntimeShape& output_shape,
                              void* output_data) {

  const auto* typed_input_data =
      reinterpret_cast<const input_type*>(input_data);
  auto* typed_output_data =
      reinterpret_cast<output_type*>(output_data);

  const int input_offset = params.input_offset;
  const int output_offset = params.output_offset;
  const int32 output_multiplier = params.output_multiplier;
  const int output_shift = params.output_shift;

  TFLITE_DCHECK_LE(params.quantized_activation_min,
                   params.quantized_activation_max);
  TFLITE_DCHECK_GE(params.quantized_activation_min, 0);
  TFLITE_DCHECK_LE(params.quantized_activation_max, 255);
  TFLITE_DCHECK_EQ(input_shape.DimensionsCount(), 4);
  TFLITE_DCHECK_EQ(output_shape.DimensionsCount(), 4);
  const int batches = MatchingDim(input_shape, 0, output_shape, 0);
  const int depth = MatchingDim(input_shape, 3, output_shape, 3);
  const int input_height = input_shape.Dims(1);
  const int input_width = input_shape.Dims(2);
  const int output_height = output_shape.Dims(1);
  const int output_width = output_shape.Dims(2);
  const int stride_height = params.stride_height;
  const int stride_width = params.stride_width;
  for (int batch = 0; batch < batches; ++batch) {
    for (int out_y = 0; out_y < output_height; ++out_y) {
      for (int out_x = 0; out_x < output_width; ++out_x) {
        for (int channel = 0; channel < depth; ++channel) {
          const int in_x_origin =
              (out_x * stride_width) - params.padding_values.width;
          const int in_y_origin =
              (out_y * stride_height) - params.padding_values.height;
          // Compute the boundaries of the filter region clamped so as to
          // ensure that the filter window fits in the input array.
          const int filter_x_start = std::max(0, -in_x_origin);
          const int filter_x_end =
              std::min(params.filter_width, input_width - in_x_origin);
          const int filter_y_start = std::max(0, -in_y_origin);
          const int filter_y_end =
              std::min(params.filter_height, input_height - in_y_origin);

          int32 min = int32(std::numeric_limits<input_type>::max());
          for (int filter_y = filter_y_start; filter_y < filter_y_end;
               ++filter_y) {
            for (int filter_x = filter_x_start; filter_x < filter_x_end;
                 ++filter_x) {
              const int in_x = in_x_origin + filter_x;
              const int in_y = in_y_origin + filter_y;
              int32 input_val = typed_input_data[Offset(input_shape, batch,
                                                        in_y, in_x, channel)];
              min = std::min(min, input_val);
            }
          }
          // Requantize here
          const int32 min_val = input_offset + min;
          const int32 output = MultiplyByQuantizedMultiplier(
                  min_val, output_multiplier, output_shift) + output_offset;

          int32 clamped_output = output;
          clamped_output =
              std::max<int32>(clamped_output, params.quantized_activation_min);
          clamped_output =
              std::min<int32>(clamped_output, params.quantized_activation_max);

          typed_output_data[Offset(
              output_shape, batch, out_y, out_x, channel)] =
              static_cast<output_type>(clamped_output);
        }
      }
    }
  }
}

}  // namespace nbits
}  // namespace mtk
}  // namespace reference_ops
}  // namespace tflite

#endif  // TENSORFLOW_CONTRIB_LITE_KERNELS_INTERNAL_REFERENCE_MTK_REFERENCE_OPS_H_
