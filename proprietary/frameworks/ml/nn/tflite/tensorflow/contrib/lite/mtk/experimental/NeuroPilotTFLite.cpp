/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Contains all the entry points to the C Neural Networks API.
// We do basic validation of the operands and then call the class
// that implements the functionality.

#define LOG_TAG "NeuroPilotTFLite"
#include "tensorflow/contrib/lite/mtk/kernels/mtk_register.h"
#include "tensorflow/contrib/lite/mtk/mtk_interpreter.h"
#include "tensorflow/contrib/lite/mtk/mtk_model.h"
#include "tensorflow/contrib/lite/mtk/mtk_error_reporter.h"
#include "tensorflow/contrib/lite/mtk/mtk_log.h"
#include "flatbuffers/flexbuffers.h"

#include "NeuroPilotTFLite.h"

//#include <android-base/logging.h>
#include <cutils/properties.h>

// #include <memory>
// #include <vector>

using namespace tflite;

int32_t getAndroidSdkVersion() {
#ifdef __ANDROID__
  const char* sdkProp = "ro.build.version.sdk";
  char sdkVersion[PROP_VALUE_MAX] = {0};
  int length = __system_property_get(sdkProp, sdkVersion);
  if (length != 0) {
    for (int i = 0; i < length; ++i) {
      int digit = sdkVersion[i] - '0';
      if (digit < 0 || digit > 9) {
        // Non-numeric SDK version, assume it's higher then expected;
        return 0xFFFF;
      }
    }
    LOG_D("%s:%s", sdkProp, sdkVersion);
    return atoi(sdkVersion);
  }
  LOG_E("No %s prop", sdkProp);
#endif  // __ANDROID__
  return 0;
}

// static const char* nn_ver_str =
//    #include "__nn_ver.inc"
//    ;

class TFLite {
  public:
    explicit TFLite(const char* modelPath) {
        model_ = FlatBufferModel::BuildFromFile(modelPath, MakeMtkErrorReporter());
        MtkInterpreterBuilder(*model_, resolver_)(&interpreter_);
    }

    TFLite(const char* modelPath,
           const std::vector<TFLiteCustomOpExt>& customOperations) {
        model_ = FlatBufferModel::BuildFromFile(modelPath, MakeMtkErrorReporter());
        ErrorReporter* error_reporter = model_->error_reporter();

        for (const auto& customOp : customOperations) {
            error_reporter->Report("Custom OP name '%s'\n", customOp.op_name);
            TfLiteRegistration reg = {
                .init = customOp.init,
                .free = customOp.free,
                .prepare = customOp.prepare,
                .invoke = nullptr,
                .custom_name = customOp.op_name,
            };
            resolver_.AddCustom(customOp.op_name,
                                customOp.vendor_name,
                                &reg);
        }

        MtkInterpreterBuilder(*model_, resolver_)(&interpreter_);

        for (auto& customOp : customOperations) {
            ((MtkInterpreter*)interpreter_.get())->SetParamsFunc(customOp.op_name,
                    customOp.target_name,
                    customOp.vendor_name,
                    customOp.add_params);
        }
    }

    TFLite(const char* buffer, size_t bufferSize) {
        cloneModelBuffer(buffer, bufferSize);
        const char* ptr = modelBuffer_;
        model_ = FlatBufferModel::BuildFromBuffer(ptr, bufferSize,
                 MakeMtkErrorReporter());
        MtkInterpreterBuilder(*model_, resolver_)(&interpreter_);
    }

    TFLite(const char* buffer, size_t bufferSize,
           const std::vector<TFLiteCustomOpExt>& customOperations) {
        cloneModelBuffer(buffer, bufferSize);
        const char* ptr = modelBuffer_;
        model_ = FlatBufferModel::BuildFromBuffer(ptr, bufferSize,
                 MakeMtkErrorReporter());
        ErrorReporter* error_reporter = model_->error_reporter();

        for (const auto& customOp : customOperations) {
            error_reporter->Report("Custom OP name '%s'\n", customOp.op_name);
            TfLiteRegistration reg = {
                .init = customOp.init,
                .free = customOp.free,
                .prepare = customOp.prepare,
                .invoke = nullptr,
                .custom_name = customOp.op_name,
            };

            resolver_.AddCustom(customOp.op_name,
                                customOp.vendor_name,
                                &reg);
        }

        MtkInterpreterBuilder(*model_, resolver_)(&interpreter_);

        for (auto& customOp : customOperations) {
            ((MtkInterpreter*)interpreter_.get())->SetParamsFunc(customOp.op_name,
                    customOp.target_name,
                    customOp.vendor_name,
                    customOp.add_params);
        }
    }

    TfLiteStatus BuildGraph() {
        interpreter_->UseNNAPI(true);
        interpreter_->SetAllowFp16PrecisionForFp32(true);
        TF_LITE_ENSURE_STATUS(interpreter_->AllocateTensors());
        if (property_get_bool("debug.tflite.disable.fakeinvoke", false)) {
            return kTfLiteOk;
        }
        TF_LITE_ENSURE_STATUS(interpreter_->Invoke());
        return kTfLiteOk;
    }

    TfLiteStatus RebuildInterpreter(uint32_t device) {
        MtkInterpreterBuilder(*model_, resolver_)(&interpreter_);
        interpreter_->UseNNAPI(true);
        interpreter_->SetAllowFp16PrecisionForFp32(true);
        interpreter_->AllocateTensors();
        ((MtkInterpreter*)interpreter_.get())->BindToDevice(device);
        ((MtkInterpreter*)interpreter_.get())->Invoke();
        return kTfLiteOk;
    }

    std::vector<float> GetDequantizedOutput(int outputTensorIndex) {
        // Transfer the output tensor index to actual tensor index
        int index = interpreter_->outputs()[outputTensorIndex];
        LOG_D("Output tensor id: %d", index);
        return Dequantize<uint8_t>(ExtractVector<uint8_t>(index),
                                   GetTensorScale(index),
                                   GetTensorZeroPoint(index));
    }

    ~TFLite() {
        if (modelBuffer_ != nullptr) {
            free(modelBuffer_);
        }
    };

    std::unique_ptr<Interpreter> interpreter_;
    std::unique_ptr<FlatBufferModel> model_;
    ops::builtin::MtkExtOpResolver resolver_;

  private:
    void cloneModelBuffer(const char* buffer, size_t bufferSize) {
        modelBuffer_ = (char*)malloc(bufferSize);
        memcpy(modelBuffer_, buffer, bufferSize);
    }

    size_t GetTensorSize(int index) const {
        TfLiteTensor* t = interpreter_->tensor(index);
        if (t == nullptr) {
            return -1;
        }
        size_t total_size = 1;
        for (size_t i = 0; i < t->dims->size; ++i) {
            total_size *= t->dims->data[i];
        }
        return total_size;
    }

    float GetTensorScale(int index) {
        TfLiteTensor* t = interpreter_->tensor(index);
        if (t != nullptr) {
            return t->params.scale;
        }
        LOG_E("Fail to get tensor: %d", index);
        return -1;
    }

    int32_t GetTensorZeroPoint(int index) {
        TfLiteTensor* t = interpreter_->tensor(index);
        if (t != nullptr) {
            return t->params.zero_point;
        }
        LOG_E("Fail to get tensor: %d", index);
        return -1;
    }

    // Return a vector with the flattened contents of a tensor.
    template <typename T>
    std::vector<T> ExtractVector(int index) {
        T* v = interpreter_->typed_tensor<T>(index);
        if (v == nullptr) {
            LOG_E("Fail to extract vector from tensor: %d", index);
            return std::vector<T>();
        }
        return std::vector<T>(v, v + GetTensorSize(index));
    }

    template <typename T>
    std::vector<float> Dequantize(const std::vector<T>& data, float scale, int32_t zero_point) {
        std::vector<float> f;
        for (T q : data) {
            f.push_back(scale * (q - zero_point));
        }
        return f;
    }

    char* modelBuffer_ = nullptr;
};

int ANeuroPilotTFLite_create(ANeuralNetworksTFLite** tflite,
                             const char* modelPath) {

    if (!tflite || !modelPath) {
        LOG_E("Null model file path");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    std::ifstream m(modelPath);

    if (!m.good()) {
        LOG_E("Fail to read model file: %s", modelPath);
        return ANEURALNETWORKS_BAD_DATA;
    }

    TFLite* tf = new TFLite(modelPath);

    if (tf == nullptr) {
        LOG_E("Fail to allocate TFLite instance");
        return ANEURALNETWORKS_OUT_OF_MEMORY;
    }

    if (tf->BuildGraph() != kTfLiteOk) {
        LOG_E("Fail to build graph");
        *tflite = nullptr;
        delete tf;
        return ANEURALNETWORKS_INCOMPLETE;
    }

    *tflite = reinterpret_cast<ANeuralNetworksTFLite*>(tf);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_createCustom(ANeuralNetworksTFLite** tflite,
                                   const char* modelPath,
                                   const std::vector<TFLiteCustomOpExt>& customOperations) {

    if (!tflite || !modelPath) {
        LOG_E("Null model file path");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    std::ifstream m(modelPath);

    if (!m.good()) {
        LOG_E("Fail to read model file: %s", modelPath);
        return ANEURALNETWORKS_BAD_DATA;
    }

    TFLite* tf = new TFLite(modelPath, customOperations);

    if (tf == nullptr) {
        LOG_E("Fail to allocate TFLite instance");
        return ANEURALNETWORKS_OUT_OF_MEMORY;
    }

    if (tf->BuildGraph() != kTfLiteOk) {
        LOG_E("Fail to build graph");
        *tflite = nullptr;
        delete tf;
        return ANEURALNETWORKS_INCOMPLETE;
    }

    *tflite = reinterpret_cast<ANeuralNetworksTFLite*>(tf);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_createWithBuffer(ANeuralNetworksTFLite** tflite,
                                       const char* buffer,
                                       size_t bufferSize) {

    if (!tflite || !buffer || bufferSize <= 0) {
        LOG_E("Null model buffer");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    TFLite* tf = new TFLite(buffer, bufferSize);

    if (tf == nullptr) {
        LOG_E("Fail to allocate TFLite instance");
        return ANEURALNETWORKS_OUT_OF_MEMORY;
    }

    if (tf->BuildGraph() != kTfLiteOk) {
        LOG_E("Fail to build graph");
        *tflite = nullptr;
        delete tf;
        return ANEURALNETWORKS_INCOMPLETE;
    }

    *tflite = reinterpret_cast<ANeuralNetworksTFLite*>(tf);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_createCustomWithBuffer(ANeuralNetworksTFLite** tflite,
        const char* buffer,
        size_t bufferSize, const std::vector<TFLiteCustomOpExt>& customOperations) {

    if (!tflite || !buffer || bufferSize <= 0) {
        LOG_E("Null model buffer");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    TFLite* tf = new TFLite(buffer, bufferSize, customOperations);

    if (tf == nullptr) {
        LOG_E("Fail to allocate TFLite instance");
        return ANEURALNETWORKS_OUT_OF_MEMORY;
    }

    if (tf->BuildGraph() != kTfLiteOk) {
        LOG_E("Fail to build graph");
        *tflite = nullptr;
        delete tf;
        return ANEURALNETWORKS_INCOMPLETE;
    }

    *tflite = reinterpret_cast<ANeuralNetworksTFLite*>(tf);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_getTensor(ANeuralNetworksTFLite* tflite,
                                TFLiteBufferType btype, TFLiteTensorExt* tfliteTensor) {
    return ANeuroPilotTFLite_getTensorByIndex(tflite, btype, tfliteTensor, 0);
}

int ANeuroPilotTFLite_getTensorByIndex(ANeuralNetworksTFLite* tflite,
                                       TFLiteBufferType btype,
                                       TFLiteTensorExt* tfliteTensor, int tensorIndex) {
    if (!tflite || !tfliteTensor) {
        LOG_E("Null tflite or tfliteTensor");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    int index = 0;

    tfliteTensor->type = TFLITE_TENSOR_TYPE_NONE;
    tfliteTensor->dimsSize = 0;
    memset(&tfliteTensor->dims[0], 0, sizeof(int) * TFLITE_TENSOR_MAX_DIMENSTIONS);
    tfliteTensor->buffer = nullptr;
    tfliteTensor->bufferSize = 0;

    if (btype == TFLITE_BUFFER_TYPE_INPUT) {
        index = tf->interpreter_->inputs()[tensorIndex];
    } else if (btype == TFLITE_BUFFER_TYPE_OUTPUT) {
        index = tf->interpreter_->outputs()[tensorIndex];
    }

    TfLiteTensor* tensor = tf->interpreter_->tensor(index);

    for (int i = 0; i < tensor->dims->size; i++) {
        if (i >= TFLITE_TENSOR_MAX_DIMENSTIONS)
            break;

        tfliteTensor->dims[i] = tensor->dims->data[i];
    }

    tfliteTensor->dimsSize = tensor->dims->size;
    tfliteTensor->dimsSize = (tfliteTensor->dimsSize >=
                              TFLITE_TENSOR_MAX_DIMENSTIONS)
                             ? TFLITE_TENSOR_MAX_DIMENSTIONS : tfliteTensor->dimsSize;

    if (float* data = tf->interpreter_->typed_tensor<float>(index)) {
        tfliteTensor->buffer = reinterpret_cast<void*>(data);
        tfliteTensor->type = TFLITE_TENSOR_TYPE_FLOAT;
        tfliteTensor->bufferSize = tensor->bytes;
    } else if (unsigned char* data = tf->interpreter_->typed_tensor<unsigned char>
                                     (index)) {
        tfliteTensor->buffer = reinterpret_cast<void*>(data);
        tfliteTensor->type = TFLITE_TENSOR_TYPE_UINT8;
        tfliteTensor->bufferSize = tensor->bytes;
    } else {
        LOG_E("Input or Output is not float nor uint8 data");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_getDequantizedOutputByIndex(ANeuralNetworksTFLite* tflite,
                                                void* buffer,
                                                size_t bufferByteSize,
                                                int tensorIndex) {
    if (!tflite) {
        LOG_E("NULL tflite instance");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    if (float* data = tf->interpreter_->typed_tensor<float>(0)) {
        LOG_E("Can't get dequantized output with float model");
        return ANEURALNETWORKS_BAD_DATA;
    }
    std::vector<float> v = tf->GetDequantizedOutput(tensorIndex);
    if (v.empty()) {
        LOG_E("Empty dequantized data");
        return ANEURALNETWORKS_BAD_DATA;
    }

    if (bufferByteSize != v.size() * sizeof(float)) {
        LOG_E("Invalid buffer size(%zu != %zu)", bufferByteSize,
            (size_t)(v.size() * sizeof(float)));
        return ANEURALNETWORKS_BAD_DATA;
    }

    std::copy(v.begin(), v.end(), (float*)buffer);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_invoke(ANeuralNetworksTFLite* tflite) {
    if (!tflite) {
        LOG_E("NULL tflite instance");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    return tf->interpreter_->Invoke();
}

void ANeuroPilotTFLite_free(ANeuralNetworksTFLite* tflite) {
    if (!tflite) {
        LOG_E("NULL tflite instance");
        return;
    }

    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    delete tf;
}

int ANeuroPilotTFLite_bindToDeivce(ANeuralNetworksTFLite* tflite,
                                   uint32_t device) {
    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    // Need to rebuild interpreter if fake invoke is enabled.
    tf->RebuildInterpreter(device);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLite_setExecParallel(ANeuralNetworksTFLite* tflite,
                                   bool enableParallel) {
    TfLiteStatus status = kTfLiteOk;
    if (!tflite) {
        LOG_E("NULL tflite instance");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }
    LOG_D("Set exec parallet: %d", enableParallel);
    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    MtkInterpreter* interpreter = (MtkInterpreter*)tf->interpreter_.get();
    interpreter->UseNNAPI(false); // Set false to reset NNAPIDelegate
    interpreter->SetExecParallel(enableParallel);
    interpreter->UseNNAPI(true);  // Set true to recreate NNAPIDelegate
    status = interpreter->Invoke();
    return (status == kTfLiteOk ? ANEURALNETWORKS_NO_ERROR : ANEURALNETWORKS_BAD_STATE);
}

int ANeuroPilotTFLite_setAllowFp16PrecisionForFp32(ANeuralNetworksTFLite* tflite,
                                   bool allow) {
    TfLiteStatus status = kTfLiteOk;
    if (!tflite) {
        LOG_E("NULL tflite instance");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }
    TFLite* tf = reinterpret_cast<TFLite*>(tflite);
    MtkInterpreter* interpreter = (MtkInterpreter*)tf->interpreter_.get();

    int input = interpreter->inputs()[0];
    TfLiteType input_data_type = interpreter->tensor(input)->type;
    if (input_data_type != kTfLiteFloat32) {
        LOG_E("Can't set allow FP16 precision with a non-float model");
        return ANEURALNETWORKS_BAD_STATE;
    }
    LOG_D("Set allow FP16 precision for FP32: %d", allow);

    interpreter->UseNNAPI(false); // Set false to reset NNAPIDelegate
    interpreter->SetAllowFp16PrecisionForFp32(allow);
    interpreter->UseNNAPI(true);  // Set true to recreate NNAPIDelegate
    status = interpreter->Invoke();
    return (status == kTfLiteOk ? ANEURALNETWORKS_NO_ERROR : ANEURALNETWORKS_BAD_STATE);
}

/*
const char* ANeuralNetworks_getNeuroPilotVersion(void) {
    return nn_ver_str;
}
*/
int ANeuroPilotTFLiteCustomOp_getInput(TfLiteContext* context, TfLiteNode* node,
                                       int index, TFLiteTensorExt* tfliteTensor) {
    TfLiteTensor* tensor = &context->tensors[node->inputs->data[index]];
    if (tensor == nullptr) {
        LOG_E("Fail to get custom OP's input at index:%d", index);
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }
    for (int i = 0; i < tensor->dims->size; i++) {
        if (i >= TFLITE_TENSOR_MAX_DIMENSTIONS)
            break;

        tfliteTensor->dims[i] = tensor->dims->data[i];
    }

    tfliteTensor->dimsSize = tensor->dims->size;
    tfliteTensor->dimsSize = (tfliteTensor->dimsSize >=
                              TFLITE_TENSOR_MAX_DIMENSTIONS)
                             ? TFLITE_TENSOR_MAX_DIMENSTIONS : tfliteTensor->dimsSize;

    if (tensor->type == kTfLiteFloat32) {
        tfliteTensor->buffer = reinterpret_cast<void*>(tensor->data.raw);
        tfliteTensor->type = TFLITE_TENSOR_TYPE_FLOAT;
        tfliteTensor->bufferSize = tensor->bytes;
    } else if (tensor->type == kTfLiteUInt8) {
        tfliteTensor->buffer = reinterpret_cast<void*>(tensor->data.raw);
        tfliteTensor->type = TFLITE_TENSOR_TYPE_UINT8;
        tfliteTensor->bufferSize = tensor->bytes;
    } else {
        LOG_E("Input or Output is not float nor uint8 data");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLiteCustomOp_getOutput(TfLiteContext* context,
                                        TfLiteNode* node,
                                        int index, TFLiteTensorExt* tfliteTensor) {
    TfLiteTensor* tensor = &context->tensors[node->outputs->data[index]];
    if (tensor == nullptr) {
        LOG_E("Fail to get custom OP's output at index:%d", index);
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }
    for (int i = 0; i < tensor->dims->size; i++) {
        if (i >= TFLITE_TENSOR_MAX_DIMENSTIONS)
            break;

        tfliteTensor->dims[i] = tensor->dims->data[i];
    }

    tfliteTensor->dimsSize = tensor->dims->size;
    tfliteTensor->dimsSize = (tfliteTensor->dimsSize >=
                              TFLITE_TENSOR_MAX_DIMENSTIONS)
                             ? TFLITE_TENSOR_MAX_DIMENSTIONS : tfliteTensor->dimsSize;

    if (tensor->type == kTfLiteFloat32) {
        tfliteTensor->buffer = reinterpret_cast<void*>(tensor->data.raw);
        tfliteTensor->type = TFLITE_TENSOR_TYPE_FLOAT;
        tfliteTensor->bufferSize = tensor->bytes;
    } else if (tensor->type == kTfLiteUInt8) {
        tfliteTensor->buffer = reinterpret_cast<void*>(tensor->data.raw);
        tfliteTensor->type = TFLITE_TENSOR_TYPE_UINT8;
        tfliteTensor->bufferSize = tensor->bytes;
    } else {
        LOG_E("Input or Output is not float nor uint8 data");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotTFLiteCustomOp_resizeOutput(TfLiteContext* context,
        TfLiteNode* node,
        int index,
        TfLiteIntArray* new_size) {
    TfLiteTensor* tensor = &context->tensors[node->outputs->data[index]];
    return (kTfLiteOk == context->ResizeTensor(context,
            tensor, (TfLiteIntArray*)new_size) ? ANEURALNETWORKS_NO_ERROR :
            ANEURALNETWORKS_BAD_DATA);
}

void* ANeuroPilotTFLiteCustomOp_getUserData(TfLiteNode* node) {
    return node->user_data;
}

int ANeuroPilotTFLiteCustomOp_getFloatAttribute(const char* buffer,
        size_t length,
        const char* attr, float* outValue) {
    if (attr == nullptr || outValue == nullptr) {
        return ANEURALNETWORKS_BAD_DATA;
    }

    flexbuffers::Map m = flexbuffers::GetRoot((unsigned char*)buffer,
                         length).AsMap();
    const auto& keys = m.Keys();

    for (size_t i = 0; i < keys.size(); ++i) {
        const auto key = keys[i].AsKey();

        if (std::strcmp(key, attr) == 0) {
            const auto& value = m[key];

            if (value.GetType() == flexbuffers::TYPE_FLOAT) {
                *outValue = value.AsFloat();
                return ANEURALNETWORKS_NO_ERROR;
            }
        }
    }

    return ANEURALNETWORKS_BAD_DATA;
}

int ANeuroPilotTFLiteCustomOp_getIntAttribute(const char* buffer, size_t length,
        const char* attr, int32_t* outValue) {
    if (attr == nullptr || outValue == nullptr) {
        return ANEURALNETWORKS_BAD_DATA;
    }

    flexbuffers::Map m = flexbuffers::GetRoot((unsigned char*)buffer,
                         length).AsMap();
    const auto& keys = m.Keys();
    LOG_D("Key size: %zu", keys.size());
    for (size_t i = 0; i < keys.size(); ++i) {
        const auto key = keys[i].AsKey();
        LOG_D("Key: %s", key);
        if (std::strcmp(key, attr) == 0) {
            const auto& value = m[key];
            LOG_D("Type: %d", value.GetType());
            if (value.GetType() == flexbuffers::TYPE_INT) {
                *outValue = value.AsInt32();
                return ANEURALNETWORKS_NO_ERROR;
            }
        }
    }

    return ANEURALNETWORKS_BAD_DATA;
}

TfLiteIntArray* ANeuroPilotTFLite_createIntArray(int size) {
    return TfLiteIntArrayCreate(size);
}

int ANeuroPilotTFLite_freeIntArray(TfLiteIntArray* v) {
    free(v);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilot_getInferencePreference(void) {
    int ret = NP_INFERENCE_TYPE_NONE;
    static int32_t version = getAndroidSdkVersion();
    if (version <= 27) {
        ret = NP_INFERENCE_TYPE_QNAUT;
    } else {
        ret = (property_get_bool("ro.vendor.mtk_nn_quant_preferred", false) ?
            NP_INFERENCE_TYPE_QNAUT : NP_INFERENCE_TYPE_FLOAT);
    }

    return ret;
}

/* Non-translated Legacy API section */
int ANeuroPilotTFLiteLegacy_createCustomWithBuffer(ANeuralNetworksTFLite**
        tflite, const char* buffer,
        size_t bufferSize, const std::vector<TFLiteCustomOp>& customOperations) {
    if (!tflite || !buffer || bufferSize <= 0) {
        LOG_E("Null model buffer");
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    std::vector<TFLiteCustomOpExt> customOpExtList;

    for (auto& customOp : customOperations) {
        TFLiteCustomOpExt ext = {
            .op_name = customOp.name,
            .vendor_name = nullptr,
            .init = customOp.opRegistration->init,
            .free = customOp.opRegistration->free,
            .prepare = customOp.opRegistration->prepare,
            .add_params = customOp.parameterFunc
        };
        customOpExtList.push_back(ext);
    }

    TFLite* tf = new TFLite(buffer, bufferSize, customOpExtList);

    if (tf == nullptr) {
        return ANEURALNETWORKS_OUT_OF_MEMORY;
    }

    *tflite = reinterpret_cast<ANeuralNetworksTFLite*>(tf);
    return ANEURALNETWORKS_NO_ERROR;
}
