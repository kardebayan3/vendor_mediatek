/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */


#ifndef MEDIATEK_ML_NN_RUNTIME_OPTIONS_H
#define MEDIATEK_ML_NN_RUNTIME_OPTIONS_H

#include <android-base/logging.h>
#include <cutils/properties.h>

namespace android {
namespace nn {

typedef enum {
    ANEUROPILOT_FEATURE_ION = 0,                    // A
    ANEUROPILOT_FEATURE_ENHANCE_PERFORMANCE = 1,    // B
    ANEUROPILOT_FEATURE_CPU_SCHEDULING = 2,         // C
    ANEUROPILOT_FEATURE_EARA_QOS = 3,               // D
    ANEUROPILOT_FEATURE_OEM_STRING_SCALAR = 4,      // E
    ANEUROPILOT_FEATURE_MAX = 25
} NpOptionType;

#define NP_VLOG         \
    if (!isNeuroPilotVLogSupported()) \
        ;                 \
    else                  \
        LOG(INFO)

bool isFeatureSupported(int feature);

inline bool isBindOperationSupported() {
    bool ret = false;
    if (property_get_bool("debug.nn.bindoperation.supported", false)) {
        ret = true;
    }
    LOG(DEBUG) << "isBindOperationSupported : " << ret;
    return ret;
}

inline bool isSysTraceSupported() {
    bool ret = false;
    if (property_get_bool("debug.nn.systrace.supported", false)) {
        ret = true;
    }
    LOG(DEBUG) << "isSysTraceSupported : " << ret;
    return ret;
}

inline bool isProfilerSupported() {
    bool ret = false;
    if (property_get_bool("debug.nn.profiler.supported", false)) {
        ret = true;
    }
    LOG(DEBUG) << "isProfilerSupported : " << ret;
    return ret;
}

inline bool isDumpModelEnable() {
    bool ret = false;
    if (property_get_bool("debug.nn.dump.graph.enable", false)) {
        ret = true;
    }
    LOG(DEBUG) << "isDumpModelEnable : " << ret;
    return ret;
}

inline bool ignoreSortRunOrder() {
    bool ret = false;
    if (property_get_bool("debug.nn.ignore.sort.order", false)) {
        ret = true;
    }
    LOG(DEBUG) << "ignoreSortRunOrder : " << ret;
    return ret;
}

inline bool getPartitionExtTypeFromProperty(char* type) {
    bool ret = false;
    if (property_get("debug.nn.partitionext.type", type, "") != 0) {
        ret = true;
    }
    LOG(DEBUG) << "getPartitionExtTypeFromProperty : " << type;
    return ret;
}

// Debug
inline bool getDumpPath(char* path) {
    bool ret = false;
    if (property_get("debug.nn.dump.path", path, "") != 0) {
        ret = true;
    }
    LOG(DEBUG) << "getDumpPath : " << path;
    return ret;
}

// Utils
inline bool isFallbackCpuSupported() {
    bool ret = true;
    if (!property_get_bool("debug.nn.fallback.cpu.supported", true)) {
        ret = false;
    }
    LOG(DEBUG) << "isFallbackCpuSupported : " << ret;
    return ret;
}

inline bool isNeuroPilotVLogSupported() {
    bool ret = false;
    if (property_get_bool("debug.neuropilot.vlog", false)) {
        ret = true;
    }
    return ret;
}

// Ion
inline bool isIonMemorySupported() {
    bool ret = false;
    if (isFeatureSupported(ANEUROPILOT_FEATURE_ION)) {
        ret = true;
    }
#ifdef NN_DEBUGGABLE
    char var[PROPERTY_VALUE_MAX];
    if (property_get("debug.nn.ion.supported", var, "") != 0) {
        ret = atoi(var) == 1 ? true : false;
        LOG(INFO) << "Override isIonMemorySupported by debug property";
    }
#endif
    NP_VLOG << "isIonMemorySupported : " << ret;
    return ret;
}

inline bool isPreCheckOperandTypeSupported() {
    bool ret = false;
    if (isFeatureSupported(ANEUROPILOT_FEATURE_ENHANCE_PERFORMANCE)) {
        ret = true;
    }
#ifdef NN_DEBUGGABLE
    char var[PROPERTY_VALUE_MAX];
    if (property_get("debug.nn.precheck.operand.supported", var, "") != 0) {
        ret = atoi(var) == 1 ? true : false;
        LOG(INFO) << "Override isPreCheckOperandTypeSupported by debug property";
    }
#endif
    NP_VLOG << "isPreCheckOperandTypeSupported : " << ret;
    return ret;
}

// CPU Schedule
inline bool isNeedScheduleCpu() {
    bool ret = false;
    if (isFeatureSupported(ANEUROPILOT_FEATURE_CPU_SCHEDULING)) {
        ret = true;
    }
    NP_VLOG << "isNeedScheduleCpu: " << ret;
    return ret;
}

inline bool getCpuScheduleSettingFromProperty(int* value) {
    bool ret = false;
    *value = 0;
#ifdef NN_DEBUGGABLE
    char var[PROPERTY_VALUE_MAX];
    if (property_get("debug.nn.sched.to", var, "") != 0) {
        *value = atoi(var);
        ret = true;
        LOG(INFO) << "override isNeedScheduleCpu by debug property, sched to " << *value;
    }
#endif
    return ret;
}

// EARA Qos & HRM
inline bool isEaraQosSupported() {
    bool ret = false;
    if (isFeatureSupported(ANEUROPILOT_FEATURE_EARA_QOS)) {
        ret = true;
    }
#ifdef NN_DEBUGGABLE
    char var[PROPERTY_VALUE_MAX];
    if (property_get("debug.nn.earaqos.supported", var, "") != 0) {
        ret = atoi(var) == 1 ? true : false;
        LOG(INFO) << "Override isEaraQosSupported by debug property";
    }
#endif
    NP_VLOG << "isEaraQosSupported: " << ret;
    return ret;
}

}  // namespace nn
}  // namespace android

#endif  //  MEDIATEK_ML_NN_RUNTIME_OPTIONS_H
