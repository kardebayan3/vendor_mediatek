/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.server.wm;

import android.graphics.Rect;
import android.os.IBinder;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Slog;
import android.view.DisplayInfo;
import android.view.WindowManager.LayoutParams;

import com.android.server.policy.WindowManagerPolicy.WindowState;
import com.android.server.am.ActivityRecord;
import com.android.server.wm.AppWindowToken;
import com.android.server.wm.Task;
import com.android.server.wm.WindowToken;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static android.view.WindowManager.LayoutParams.*;
import static com.android.server.wm.WindowManagerDebugConfig.DEBUG_LAYOUT;

public class WmsExtImpl extends WmsExt {
    private static final String TAG = "WmsExtImpl";

    /// M: add for fullscreen switch feature @{
    // used by compute window size.
    static final Rect mCropFrame = new Rect();
    // used to change display size.
    static final Rect mCropDisplayFrame = new Rect();
    private static final int SWITCH_TARGET_WIDTH = 9;
    private static final int SWITCH_TARGET_HEIGHT = 16;
    private static final int CROP_SCREEN_MODE = 0;
    private static boolean mSupportFullscreenSwitch = "1"
            .equals(SystemProperties.get("ro.vendor.fullscreen_switch"));
    private boolean mFrameUpdated = false;
    static Method getResolutionMode;
    static Object mModeManager;

    @Override
    public boolean isFullscreenSwitchSupport() {
        return mSupportFullscreenSwitch;
    }

    @Override
    public boolean isFocusWindowReady(WindowState focus) {
        return focus != null && !focus.isFullscreenOn();
    }

    @Override
    public Rect getSwitchFrame(WindowState win, WindowState focus,
            int mOverscanScreenWidth, int mOverscanScreenHeight) {
        if ((!win.isFullscreenOn() && !win.isInMultiWindowMode())
                || (isFocusWindowReady(focus) && win.getAttrs().type == TYPE_INPUT_METHOD)) {
            if (!mFrameUpdated) {
                computeSwitchFrame(mOverscanScreenWidth, mOverscanScreenHeight);
                mFrameUpdated = true;
            }
            if (DEBUG_LAYOUT)
                Slog.i(TAG, "applyFullScreenSwitch = " + " mTmpSwitchFrame ="
                        + mCropFrame);
            return mCropFrame;
        }
        return mEmptyFrame;
    }

    @Override
    public void resetSwitchFrame() {
        mCropFrame.setEmpty();
        mFrameUpdated = false;
    }

    @Override
    public boolean initFullscreenSwitchState(IBinder token) {
        boolean on = true;
        ActivityRecord ar = ActivityRecord.forTokenLocked(token);
        if (ar != null && ar.packageName != null) {
            on = getFullscreenMode(ar.packageName);
            Slog.v(TAG, "initFullscreenSwitchState, mode= " + on);
        }
        return on;
    }

    @Override
    public boolean isMultiWindow(AppWindowToken token) {
        final Task task = token != null ? token.getTask() : null;
        return task != null && !task.isFullscreen();
    }

    @Override
    public boolean isFullScreenCropState(AppWindowToken focusedApp) {
        return mSupportFullscreenSwitch && focusedApp != null
                && !focusedApp.isFullscreenOn && !isMultiWindow(focusedApp);
    }

    @Override
    public Rect getSwitchFrame(int logicWidth, int logicHeight) {
        mCropDisplayFrame.setEmpty();
        int diff = 0;
        if (logicWidth > logicHeight) {
            diff = (logicWidth - (logicHeight / SWITCH_TARGET_WIDTH)
                    * SWITCH_TARGET_HEIGHT) / 2;
            if (diff > 0) {
                mCropDisplayFrame.left = diff;
                mCropDisplayFrame.top = 0;
                mCropDisplayFrame.right = diff;
                mCropDisplayFrame.bottom = 0;
            }
        } else {
            diff = (logicHeight - (logicWidth / SWITCH_TARGET_WIDTH)
                    * SWITCH_TARGET_HEIGHT) / 2;
            if (diff > 0) {
                mCropDisplayFrame.left = 0;
                mCropDisplayFrame.top = diff;
                mCropDisplayFrame.right = 0;
                mCropDisplayFrame.bottom = diff;
            }
        }

        Slog.i(TAG, "updateDisplayAndOrientationLocked logicWidth = "
                + logicWidth + " logicHeight =" + logicHeight
                + " mSwitchFrame =" + mCropDisplayFrame);
        return mCropDisplayFrame;
    }

    @Override
    public boolean getFullscreenMode(String packageName) {
        boolean on = true;
        try {
            if (getResolutionMode == null || mModeManager == null) {
                Class stub = Class
                        .forName("com.mediatek.fullscreenswitch.IFullscreenSwitchManager$Stub");
                Method asInterface = stub.getDeclaredMethod("asInterface",
                        IBinder.class);
                mModeManager = asInterface.invoke(stub,
                        ServiceManager.checkService("FullscreenSwitchService"));

                getResolutionMode = mModeManager.getClass().getDeclaredMethod(
                        "getFullscreenMode", String.class);
            }
            int mode = (Integer) getResolutionMode.invoke(mModeManager,
                    packageName);
            if (mode == CROP_SCREEN_MODE) {
                on = false;
            }
            if (DEBUG_LAYOUT)
                Slog.d(TAG, packageName + " getFullscreenMode mode = " + mode);
        } catch (ClassNotFoundException e) {
            Slog.e(TAG, "initFullscreenSwitchState ClassNotFoundException:" + e);
        } catch (NoSuchMethodException e) {
            Slog.e(TAG, "initFullscreenSwitchState NoSuchMethodException:" + e);
        } catch (IllegalAccessException e) {
            Slog.e(TAG, "initFullscreenSwitchState IllegalAccessException:" + e);
        } catch (InvocationTargetException e) {
            if (DEBUG_LAYOUT) {
                Slog.e(TAG,
                        "initFullscreenSwitchState InvocationTargetException:"
                                + e);
            }
        }
        return on;
    }

    /**
     * Compute screen crop value if not at fullscreen mode.
     */
    private void computeSwitchFrame(int mOverscanScreenWidth,
            int mOverscanScreenHeight) {
        mCropFrame.setEmpty();
        int diff = 0;

        if (mOverscanScreenWidth > mOverscanScreenHeight) {
            diff = (mOverscanScreenWidth - (mOverscanScreenHeight / SWITCH_TARGET_WIDTH)
                    * SWITCH_TARGET_HEIGHT) / 2;
            if (diff > 0) {
                mCropFrame.left = diff;
                mCropFrame.top = 0;
                mCropFrame.right = diff;
                mCropFrame.bottom = 0;
            }
        } else {
            diff = (mOverscanScreenHeight - (mOverscanScreenWidth / SWITCH_TARGET_WIDTH)
                    * SWITCH_TARGET_HEIGHT) / 2;
            if (diff > 0) {
                mCropFrame.left = 0;
                mCropFrame.top = diff;
                mCropFrame.right = 0;
                mCropFrame.bottom = diff;
            }
        }

        if (DEBUG_LAYOUT) {
            Slog.i(TAG, "applyFullScreenSwitch mOverscanScreenWidth = "
                    + mOverscanScreenWidth + " mOverscanScreenHeight ="
                    + mOverscanScreenHeight + " diff =" + diff
                    + " mTmpSwitchFrame =" + mCropFrame);
        }
    }
    /// @}

    /// M: add for App Resolution Tuner feature @{
    @Override
    public boolean isAppResolutionTunerSupport() {
        return "1".equals(SystemProperties.get("ro.vendor.app_resolution_tuner"))
              && 0 == SystemProperties.getInt("persist.vendor.dbg.disable.art",0);
    }

    @Override
    public void loadResolutionTunerAppList() {
        getTunerList().loadTunerAppList();
    }

    @Override
    public void setWindowScaleByWL(com.android.server.wm.WindowState win,DisplayInfo displayInfo,LayoutParams attrs, int requestedWidth,int requestedHeight) {
        if (win.mNeedHWResizer) {
           /** mEnforceSizeCompat will update in relayout(), so we need update again here **/
           win.mEnforceSizeCompat = true;
           Slog.v("Scale_Test","setWindowScaleByWL - this window has set the scale param, win : "
                 + win + " ,win.mHWScale=" + win.mHWScale);
           return;
        }
        float scale = 1.f;
        int width = displayInfo.logicalWidth;
        int height = displayInfo.logicalHeight;
        String packageName = attrs != null ? attrs.packageName : null;
        String windowName = attrs != null && attrs.getTitle() != null ?
            attrs.getTitle().toString() : null;
        if (packageName != null && windowName != null && !windowName.contains("FastStarting")
               /** splash screen is transient **/
               && (!windowName.contains("Splash Screen"))
               /** PopupWindow is transient **/
               && (!windowName.contains("PopupWindow"))
               /** full screen window **/
               && ((height == requestedHeight && width == requestedWidth)
               || (attrs.width == -1 && attrs.height == -1 && attrs.x == 0 && attrs.y == 0))
               /** app contains in white list **/
               && getTunerList().contains(packageName,windowName)) {
            scale = getTunerList().getScaleValue(packageName);
        }

        if (scale != 1.f) {
            win.mEnforceSizeCompat = true;
            win.mHWScale = scale;
            win.mNeedHWResizer = true;
            Slog.v("Scale_Test","setWindowScaleByWL - new scale = " + scale
                + " ,set mEnforceSizeCompat/mNeedHWResizer = true" + " , win : " + win + " ,attrs=" + attrs.getTitle().toString());
         }
    }

    private ResolutionTunerAppList getTunerList() {
        return ResolutionTunerAppList.getInstance();
    }
    /// @}
}
