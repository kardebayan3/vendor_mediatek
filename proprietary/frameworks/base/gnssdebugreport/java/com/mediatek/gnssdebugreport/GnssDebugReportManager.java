package com.mediatek.gnssdebugreport;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

public class GnssDebugReportManager  {
    private static final String SERVICE_ACTION = "com.mediatek.gnssdebugreport.start";
    private static final String SERVICE_PKG = "com.mediatek.gnssdebugreport";
    public static boolean bindService(Context context, ServiceConnection serviceConnection) {
        Intent it = new Intent(SERVICE_ACTION);
        it.setPackage(SERVICE_PKG);
        return context.bindService(it, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    public static void unbindService(Context context, ServiceConnection serviceConnection) {
        context.unbindService(serviceConnection);
    }

}
