package com.mediatek.view.impl;

import com.mediatek.view.SurfaceExt;
import com.mediatek.view.SurfaceFactory;

public class SurfaceFactoryImpl extends SurfaceFactory {
    private static SurfaceExt mSurfaceExt = new SurfaceExtimpl();

    @Override
    public SurfaceExt getSurfaceExt() {
        return mSurfaceExt;
    }
}
