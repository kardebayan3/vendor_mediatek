package com.mediatek.view.impl;

import android.content.pm.IPackageManager;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

import com.mediatek.view.SurfaceExt;
import com.mediatek.view.impl.ResolutionTunerAppList;


public class SurfaceExtimpl extends SurfaceExt {
    private static final String TAG = "SurfaceExt";
    private static final boolean ENABLE_WHITE_LIST = SystemProperties.getBoolean(
            "debug.enable.whitelist", false);
    private static final int ENABLE_RESOLUTION_TUNING = SystemProperties.getInt(
            "ro.vendor.app_resolution_tuner", 0);
    private static final String WHITE_LIST[] = {"com.tencent.qqlive"};
    private String mPackageName;
    private boolean mContainPackageName = false;

    public SurfaceExtimpl() {
        if (ENABLE_RESOLUTION_TUNING == 1) {
            mPackageName = getCallerProcessName();
            getTunerList().loadTunerAppList();
            mContainPackageName = getTunerList().contains(mPackageName);
            Log.d(TAG, "SurfaceExtimpl, mPackageName:" + mPackageName +
                    ",mContainPackageName:" + mContainPackageName);
        }
    }

    @Override
    public boolean isInWhiteList() {
        if (ENABLE_WHITE_LIST) {
            return true;
            }
        boolean isContained = false;
        String packageName = getCallerProcessName();
        if (WHITE_LIST != null && packageName != null) {
            for (String item : WHITE_LIST) {
                if (item.equals(packageName)) {
                    isContained = true;
                    break;
                }
            }
        }
        return isContained;
    }

    public boolean isResolutionTuningPackage() {
        return mContainPackageName;
    }

    public float getXScale() {
        float xScale = getTunerList().getScaleValue(mPackageName);
        Log.d(TAG, "getXScale, xScale:" + xScale);
        return xScale;
    }

    public float getYScale() {
        float yScale = getTunerList().getScaleValue(mPackageName);
        Log.d(TAG, "getYScale,yScale:" + yScale);
        return yScale;
    }

    private ResolutionTunerAppList getTunerList() {
        return ResolutionTunerAppList.getInstance();
    }

    private String getCallerProcessName() {
        int binderuid = Binder.getCallingUid();
        IPackageManager pm = IPackageManager.Stub.asInterface(ServiceManager.getService("package"));
        if (pm != null) {
            try {
                String callingApp = pm.getNameForUid(binderuid);
                // Log.d(TAG, "callingApp: " + callingApp);
                return callingApp;
            } catch (RemoteException e) {
                Log.e(TAG, "getCallerProcessName exception :" + e);
            }
        }
        return null;
    }
}
