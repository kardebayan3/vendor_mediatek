package com.mediatek.powerhalmgr;

/** @hide */
interface IPowerHalMgr {
    int scnReg();
    oneway void scnConfig(int handle, int cmd, int param_1, int param_2, int param_3, int param_4);
    oneway void scnUnreg(int handle);
    oneway void scnEnable(int handle, int timeout);
    oneway void scnDisable(int handle);
    oneway void scnUltraCfg(int handle, int ultracmd, int param_1, int param_2, int param_3, int param_4);
    oneway void mtkCusPowerHint(int hint, int data);
}
