package com.mediatek.media.mediascanner;

import android.graphics.BitmapFactory;

import com.mediatek.media.mediascanner.ThumbnailUtilsEx;

public class ThumbnailUtilsExImpl extends ThumbnailUtilsEx {
    public void correctOptions(String filePath, BitmapFactory.Options options) {
        /// M: Add for OMA DRM, mark inSampleSize as decode thumbnail(or 0x100) @{
        if (filePath.endsWith(".dcf")) {
            options.inSampleSize |= 0x100;
        }
        /// @}
    }
}

