package com.mediatek.gnssdebugreport;

import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import com.mediatek.gnssdebugreport.DebugDataReport;
import com.mediatek.gnssdebugreport.IDebugReportCallback;
import com.mediatek.gnssdebugreport.IGnssDebugReportService;

public class GnssDebugReportServiceStub extends IGnssDebugReportService.Stub implements
IGnssDebugReportService, MnldConn.OnResponseListener {

    private static final String TAG = "GnssDebugReportServiceStub";
    private static final String TAG_TYPE = "type";
    private static final String TAG_TYPE_DEBUG = "debuginfo";
    private static final String TAG_TYPE_STATE = "stateinfo";
    private static final String DEBUG_DATA_HEAD = "$PMTK839";
    private static final int DEBUG_DATA_HEAD_LENGTH = 13;
    private static final int DEBUG_DATA_TYPE = 19;
    private static final String DEBUG_DATA_TAIL = "*";
    private static final String DEBUG_DATA_SPLIT = ",";
    private DebugDataReport mDebugDataReport = null;
    private MnldConn mMnldConn = new MnldConn(this);
    private final RemoteCallbackList<IDebugReportCallback> mCbList = new RemoteCallbackList<IDebugReportCallback>();


    @Override
    public IBinder asBinder() {
        return this;
    }

    @Override
    public synchronized boolean startDebug(IDebugReportCallback callback) {
        if (mCbList != null) {
            int oriRegisterNum = mCbList.getRegisteredCallbackCount();
            registerDebugReportCallback(callback);
            int newRigsteNum = mCbList.getRegisteredCallbackCount();
            Log.i(TAG, "startDebug ori:" + oriRegisterNum + " new:" + newRigsteNum);
            if ((oriRegisterNum == 0) && (newRigsteNum == 1)) {
                Log.i(TAG, "startDebug");
                mMnldConn.startDebug();
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean registerDebugReportCallback(IDebugReportCallback callback) {
        if (callback != null) {
            return mCbList.register(callback);
        } else {
            return false;
        }


    }

    private boolean unregisterDebugReportCallback(IDebugReportCallback callback) {
        if (callback != null) {
            return mCbList.unregister(callback);
        } else {
            return false;
        }
    }

    @Override
    public synchronized boolean stopDebug(IDebugReportCallback callback) {
        if (mCbList != null) {
            int oriRegisterNum = mCbList.getRegisteredCallbackCount();
            unregisterDebugReportCallback(callback);
            int newRigsteNum = mCbList.getRegisteredCallbackCount();
            Log.i(TAG, "stopDebug ori:" + oriRegisterNum + " new:" + newRigsteNum);
            if ((oriRegisterNum == 1) && (newRigsteNum == 0)) {
                Log.i(TAG, "stopDebug");
                mMnldConn.stopDebug();
            }
            return true;
        } else {
            return false;
        }

    }


    private void parseDebugData(String response) {
        //Extract data body
        int startPos = response.indexOf(DEBUG_DATA_HEAD);
        if (startPos == -1) {
            return;
        }
        startPos = startPos + DEBUG_DATA_HEAD_LENGTH;
        int endPos = response.indexOf(DEBUG_DATA_TAIL);
        if (endPos != -1) {
            response = response.substring(startPos, endPos);
        }
        //Parse data
        String[] debugData = response.split(DEBUG_DATA_SPLIT);
        if ((debugData == null) || (debugData.length != DEBUG_DATA_TYPE)) {
            return;
        }
        double CB = Double.valueOf(debugData[0]).doubleValue();
        double CompCB = Double.valueOf(debugData[1]).doubleValue();
        double ClkTemp = Double.valueOf(debugData[2]).doubleValue();
        int Saturation = Integer.valueOf(debugData[3]).intValue();
        int Pga = Integer.valueOf(debugData[4]).intValue();
        long Ttff = Long.valueOf(debugData[5]).longValue();
        int Svnum = Integer.valueOf(debugData[6]).intValue();
        long TT4SV = Long.valueOf(debugData[7]).longValue();
        float Top4CNR = Float.valueOf(debugData[8]).floatValue();
        double InitLlhLongi = Double.valueOf(debugData[9]).doubleValue();
        double InitLlhLati = Double.valueOf(debugData[10]).doubleValue();
        double InitLlhHeight = Double.valueOf(debugData[11]).doubleValue();
        int InitSrc = Integer.valueOf(debugData[12]).intValue();
        float InitPacc = Float.valueOf(debugData[13]).floatValue();
        int HaveEPO = Integer.valueOf(debugData[14]).intValue();
        int EPOage = Integer.valueOf(debugData[15]).intValue();
        float SensorHACC = Float.valueOf(debugData[16]).floatValue();
        int MPEvalid = Integer.valueOf(debugData[17]).intValue();
        int Lsvalid = Integer.valueOf(debugData[18]).intValue();
        mDebugDataReport = new DebugDataReport(CB, CompCB, ClkTemp, Saturation, Pga, Ttff,
                Svnum, TT4SV, Top4CNR, InitLlhLongi, InitLlhLati, InitLlhHeight, InitSrc,
                InitPacc, HaveEPO, EPOage, SensorHACC, MPEvalid, Lsvalid);

    }

    public void onDebugInfoResp(String response){
        parseDebugData(response);
        Bundle bundle = new Bundle();
        bundle.putParcelable(DebugDataReport.DATA_KEY, mDebugDataReport);
        int number = mCbList.beginBroadcast();
        for (int k = 0; k < number; k++) {
            try {
                mCbList.getBroadcastItem(k).onDebugReportAvailable(bundle);
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }
        mCbList.finishBroadcast();
    }



}
