/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.mediatek.server.telecom.tests;

import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.IContentProvider;
import android.content.pm.UserInfo;
import android.location.Country;
import android.location.CountryDetector;
import android.location.CountryListener;
import android.net.Uri;
import android.os.Looper;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.UserManager;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.support.test.filters.FlakyTest;
import android.telecom.DisconnectCause;
import android.telecom.Log;
import android.telecom.ParcelableCall;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.VideoProfile;
import android.telephony.CarrierConfigManager;
import android.telephony.TelephonyManager;
import android.telephony.PhoneNumberUtils;
import android.test.suitebuilder.annotation.MediumTest;
import android.test.suitebuilder.annotation.SmallTest;

import com.android.server.telecom.AsyncRingtonePlayer;
import com.android.server.telecom.Call;
import com.android.server.telecom.CallAudioManager;
import com.android.server.telecom.CallLogManager;
import com.android.server.telecom.CallState;
import com.android.server.telecom.CallerInfoAsyncQueryFactory;
import com.android.server.telecom.CallsManager;
import com.android.server.telecom.ClockProxy;
import com.android.server.telecom.ConnectionServiceFocusManager;
import com.android.server.telecom.ConnectionServiceWrapper;
import com.android.server.telecom.ContactsAsyncHelper;
import com.android.server.telecom.DefaultDialerCache;
import com.android.server.telecom.EmergencyCallHelper;
import com.android.server.telecom.HandoverState;
import com.android.server.telecom.HeadsetMediaButton;
import com.android.server.telecom.HeadsetMediaButtonFactory;
import com.android.server.telecom.InCallController;
import com.android.server.telecom.InCallControllerFactory;
import com.android.server.telecom.InCallTonePlayer;
import com.android.server.telecom.InCallWakeLockController;
import com.android.server.telecom.InCallWakeLockControllerFactory;
import com.android.server.telecom.MissedCallNotifier;
import com.android.server.telecom.PhoneAccountRegistrar;
import com.android.server.telecom.PhoneNumberUtilsAdapter;
import com.android.server.telecom.ProximitySensorManager;
import com.android.server.telecom.ProximitySensorManagerFactory;
import com.android.server.telecom.SystemStateProvider;
import com.android.server.telecom.TelecomSystem;
import com.android.server.telecom.TelephonyUtil;
import com.android.server.telecom.Timeouts;
import com.android.server.telecom.WiredHeadsetManager;
import com.android.server.telecom.ConnectionServiceFocusManager.ConnectionServiceFocusManagerFactory;
import com.android.server.telecom.bluetooth.BluetoothRouteManager;
import com.android.server.telecom.bluetooth.BluetoothStateReceiver;
import com.android.server.telecom.tests.TelecomSystemTest;

import mediatek.telephony.MtkCarrierConfigManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(JUnit4.class)
public class MtkCallsManagerTest extends TelecomSystemTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @SmallTest
    @Test
    public void testDisallowOutgoingCallsDuringConference() throws Exception {
        CarrierConfigManager mockCarrierConfigManager =
                (CarrierConfigManager) mComponentContextFixture.getTestDouble()
                .getApplicationContext().getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle bundle = new PersistableBundle();
        bundle.putBoolean(MtkCarrierConfigManager.MTK_KEY_DISALLOW_OUTGOING_CALLS_DURING_CONFERENCE_BOOL, true);
        when(mockCarrierConfigManager.getConfig()).thenReturn(bundle);

        // Make a conference call and then add another outgoing call in the same PhoneAccount
        ParcelableCall conferenceCall = makeConferenceCall();
        // ConnectionServiceFixture will not set PhoneAccount for FakeConference, update it here.
        for (Call call : mTelecomSystem.getCallsManager().getCalls()) {
            if (call.isConference()) {
                call.setTargetPhoneAccount(mPhoneAccountA0.getAccountHandle());
            }
        }
        int callCount = mTelecomSystem.getCallsManager().getCalls().size();
        // Make another MO call
        startOutgoingPhoneCallWaitForBroadcaster("650-555-1214", mPhoneAccountA0.getAccountHandle(),
                mConnectionServiceFixtureA, Process.myUserHandle(), VideoProfile.STATE_AUDIO_ONLY, false);
        // The new MO call should fail
        assertEquals(callCount, mTelecomSystem.getCallsManager().getCalls().size());
        for (Call call : mTelecomSystem.getCallsManager().getCalls()) {
            assertNotEquals("650-555-1214", call.getHandle());
        }

        // Change disallow outgoing call during conference to false, and try make MO call again.
        bundle.putBoolean(MtkCarrierConfigManager.MTK_KEY_DISALLOW_OUTGOING_CALLS_DURING_CONFERENCE_BOOL, false);
        when(mockCarrierConfigManager.getConfig()).thenReturn(bundle);
        // Make another MO call
        startOutgoingPhoneCallWaitForBroadcaster("650-555-1215", mPhoneAccountA0.getAccountHandle(),
                mConnectionServiceFixtureA, Process.myUserHandle(), VideoProfile.STATE_AUDIO_ONLY, false);
        // The new MO call should success
        Call newMoCall = null;
        for (Call call : mTelecomSystem.getCallsManager().getCalls()) {
            if (call.getTargetPhoneAccount().equals(mPhoneAccountA0.getAccountHandle()) &&
                    call.getHandle() != null && call.getHandle().equals("650-555-1215")) {
                newMoCall = call;
                break;
            }
        }
        assertEquals(callCount + 1, mTelecomSystem.getCallsManager().getCalls().size());
        assertNotNull(newMoCall);
    }
}
