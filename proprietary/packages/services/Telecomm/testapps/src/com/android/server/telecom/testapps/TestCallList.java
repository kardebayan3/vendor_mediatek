/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.android.server.telecom.testapps;

import android.content.Context;
/// M: Bundle for connection event.
import android.os.Bundle;
import android.telecom.Call;
/// M: Remotely held event and video state. @{
import android.telecom.Call.Details;
import android.telecom.Connection;
/// M: @}
import android.telecom.InCallService;
/// M: Emergency call.
import android.telecom.TelecomManager;
import android.telecom.VideoProfile;
import android.telecom.VideoProfile.CameraCapabilities;
/// M: Emergency call. @{
import android.telephony.PhoneNumberUtils;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
/// M: @}
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Log;
import android.widget.Toast;

import mediatek.telecom.MtkConnection;

/// M: Multi calls, sort calls. @{
import java.util.Collections;
import java.util.Comparator;
/// M: @}
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
/// M: Calls map.
import java.util.concurrent.ConcurrentHashMap;

/**
 * Maintains a list of calls received via the {@link TestInCallServiceImpl}.
 */
public class TestCallList extends Call.Callback {

    public static abstract class Listener {
        public void onCallAdded(Call call) {}
        public void onCallRemoved(Call call) {}
        public void onRttStarted(Call call) {}
        public void onRttStopped(Call call) {}
        public void onRttInitiationFailed(Call call, int reason) {}
        public void onRttRequest(Call call, int id) {}
        /// M: Connection event for remotely hold call.
        public void onConnectionEvent(Call call, String event) {}
    }

    private static final TestCallList INSTANCE = new TestCallList();
    private static final String TAG = "TestCallList";
    /// M: Have a reference of TestInCallUI to set video call.
    private TestInCallUI mTestInCallUI = null;

    private class TestVideoCallListener extends InCallService.VideoCall.Callback {
        private Call mCall;

        public TestVideoCallListener(Call call) {
            mCall = call;
        }

        @Override
        public void onSessionModifyRequestReceived(VideoProfile videoProfile) {
            Log.v(TAG,
                    "onSessionModifyRequestReceived: videoState = " + videoProfile.getVideoState()
                            + " call = " + mCall);
            /// M: Set accept video call button enabled when receive upgrade request.
            setAcceptVideoEnabled(mCall, videoProfile);
        }

        @Override
        public void onSessionModifyResponseReceived(int status, VideoProfile requestedProfile,
                VideoProfile responseProfile) {
            Log.v(TAG,
                    "onSessionModifyResponseReceived: status = " + status + " videoState = "
                            + responseProfile.getVideoState()
                            + " call = " + mCall);
            /// M: This event response from remote side, like upgrade/downgrade fail or success,
            // set property of video call, like camera, surface base responseProfile.
            setVideoCall(mCall, responseProfile);
        }

        @Override
        public void onCallSessionEvent(int event) {

        }

        @Override
        public void onPeerDimensionsChanged(int width, int height) {

        }

        @Override
        public void onVideoQualityChanged(int videoQuality) {
            Log.v(TAG,
                    "onVideoQualityChanged: videoQuality = " + videoQuality + " call = " + mCall);
        }

        @Override
        public void onCallDataUsageChanged(long dataUsage) {

        }

        @Override
        public void onCameraCapabilitiesChanged(CameraCapabilities cameraCapabilities) {

        }
    }

    // The calls the call list knows about.
    private List<Call> mCalls = new LinkedList<Call>();
    private Map<Call, TestVideoCallListener> mVideoCallListeners =
            new ArrayMap<Call, TestVideoCallListener>();
    /// M: Create listeners with initial capacity. @{
    private Set<Listener> mListeners = Collections.newSetFromMap(
            new ConcurrentHashMap<Listener, Boolean>(16, 0.9f, 1));
    private ArrayMap<String, Integer> mCallCategory = new ArrayMap<String, Integer>();
    /// M: @}
    private Context mContext;
    private int mLastRttRequestId = -1;
    /// M: ATT, TMO requirement. Store callId and result mappings indicates speech is present or
    // not on the RTT call.
    private ArrayMap<String, Boolean> mRttSpeechPresentCalls = new ArrayMap<String, Boolean>();

    /**
     * Singleton accessor.
     */
    public static TestCallList getInstance() {
        return INSTANCE;
    }

    public void addListener(Listener listener) {
        if (listener != null) {
            mListeners.add(listener);
        }
    }

    public boolean removeListener(Listener listener) {
        return mListeners.remove(listener);
    }

    public Call getCall(int position) {
        return mCalls.get(position);
    }

    public void addCall(Call call) {
        if (mCalls.contains(call)) {
            Log.e(TAG, "addCall: Call already added.");
            return;
        }
        Log.i(TAG, "addCall: " + call + " " + System.identityHashCode(this));
        mCalls.add(call);
        call.registerCallback(this);
        /// M: Handle add call event.
        handleCallAdded(call);

        for (Listener l : mListeners) {
            l.onCallAdded(call);
        }
    }

    public void removeCall(Call call) {
        if (!mCalls.contains(call)) {
            Log.e(TAG, "removeCall: Call cannot be removed -- doesn't exist.");
            return;
        }
        Log.i(TAG, "removeCall: " + call);
        mCalls.remove(call);
        call.unregisterCallback(this);
        /// M: VzW requirement.
        mCallCategory.remove(call.getDetails().getTelecomCallId());
        /// M: ATT, TMO requirement.
        mRttSpeechPresentCalls.remove(call.getDetails().getTelecomCallId());

        for (Listener l : mListeners) {
            if (l != null) {
                l.onCallRemoved(call);
            }
        }
    }

    public void clearCalls() {
        for (Call call : new LinkedList<Call>(mCalls)) {
            removeCall(call);
        }

        for (Call call : mVideoCallListeners.keySet()) {
            if (call.getVideoCall() != null) {
                call.getVideoCall().destroy();
            }
        }
        mVideoCallListeners.clear();
    }

    public int size() {
        return mCalls.size();
    }

    public int getLastRttRequestId() {
        return mLastRttRequestId;
    }

    /**
     * For any video calls tracked, sends an upgrade to video request.
     */
    public void sendUpgradeToVideoRequest(Call call, int videoState) {
        Log.v(TAG, "sendUpgradeToVideoRequest : videoState = " + videoState);

        /// M: Only send upgrade request to active call. @{
        // for (Call call : mCalls) {
            InCallService.VideoCall videoCall = call.getVideoCall();
            Log.v(TAG, "sendUpgradeToVideoRequest: checkCall " + call);
            if (videoCall == null) {
                return;
            }

            Log.v(TAG, "send upgrade to video request for call: " + call);
            videoCall.sendSessionModifyRequest(new VideoProfile(videoState));
        //}
        /// M: @}
    }

    /**
     * For any video calls which are active, sends an upgrade to video response with the specified
     * video state.
     *
     * @param videoState The video state to respond with.
     */
    public void sendUpgradeToVideoResponse(Call call, int videoState) {
        Log.v(TAG, "sendUpgradeToVideoResponse : videoState = " + videoState);

        /// M: Only send response to active call. @{
        // for (Call call : mCalls) {
            InCallService.VideoCall videoCall = call.getVideoCall();
            if (videoCall == null) {
                return;
            }

            Log.v(TAG, "send upgrade to video response for call: " + call);
            videoCall.sendSessionModifyResponse(new VideoProfile(videoState));
        //}
        /// M: @}
    }

    @Override
    public void onVideoCallChanged(Call call, InCallService.VideoCall videoCall) {
        Log.v(TAG, "onVideoCallChanged: call = " + call + " " + System.identityHashCode(this));
        if (videoCall != null) {
            if (!mVideoCallListeners.containsKey(call)) {
                TestVideoCallListener listener = new TestVideoCallListener(call);
                videoCall.registerCallback(listener);
                mVideoCallListeners.put(call, listener);
                Log.v(TAG, "onVideoCallChanged: added new listener");
                /// M: Temp solution for timing issue.
                setVideoCall(call, new VideoProfile(call.getDetails().getVideoState()));
            }
        }
    }

    @Override
    public void onRttStatusChanged(Call call, boolean enabled, Call.RttCall rttCall) {
        Log.v(TAG, "onRttStatusChanged: call = " + call + " " + System.identityHashCode(this));
        if (enabled) {
            for (Listener l : mListeners) {
                l.onRttStarted(call);
            }
        } else {
            for (Listener l : mListeners) {
                l.onRttStopped(call);
            }
        }
    }

    @Override
    public void onRttInitiationFailure(Call call, int reason) {
        for (Listener l : mListeners) {
            l.onRttInitiationFailed(call, reason);
        }
    }

    @Override
    public void onRttRequest(Call call, int id) {
        mLastRttRequestId = id;
        for (Listener l : mListeners) {
            l.onRttRequest(call, id);
        }
    }

    /** {@inheritDoc} */
    @Override
    public void onConnectionEvent(Call call, String event, Bundle extras) {
        Log.i(TAG, "onConnectionEvent: event = " + event + ", extras = " + extras);
        if ((Connection.EVENT_CALL_REMOTELY_HELD).equals(event)
                || (Connection.EVENT_CALL_REMOTELY_UNHELD).equals(event)
                /// M: ATT, TMO requirement.Add event notify speech is present or not on the
                // RTT call.
                || (MtkConnection.EVENT_RTT_SPEECH_INDICATION_CHANGED).equals(event)) {
            if ((MtkConnection.EVENT_RTT_SPEECH_INDICATION_CHANGED).equals(event)) {
                mRttSpeechPresentCalls.put(call.getDetails().getTelecomCallId(), extras.getBoolean(
                        MtkConnection.EXTRA_IS_RTT_SPEECH_PRESENT, false));
            }
            for (Listener l : mListeners) {
                l.onConnectionEvent(call, event);
            }
            return;
        }

        String text = MtkUtil.makeTextForConnectionEvent(call, event, extras);
        if (!TextUtils.isEmpty(text)) {
            MtkTelecomTestappsGlobals.getInstance().showToast(text);
        }
    }

    /**
     * M: Receive RTT mode change event to show toast.
     */
    @Override
    public void onRttModeChanged(Call call, int mode) {
        super.onRttModeChanged(call, mode);
        String text = MtkUtil.makeTextRttModeChanged(call, mode);
        if (!TextUtils.isEmpty(text)) {
            MtkTelecomTestappsGlobals.getInstance().showToast(text);
        }
    }

    /**
     * M: Receive call state change event.
     */
    @Override
    public void onStateChanged(Call call, int state) {
        if (mTestInCallUI != null) {
            mTestInCallUI.handleStateChanged();
        }
    }

    /**
     * M: Set a reference of TestInCallUI, support video call to
     * set camera, preview surface, display surface.
     *
     * @param testInCallUI The activity that need be used.
     */
    public void setActivity(TestInCallUI testInCallUI) {
        mTestInCallUI = testInCallUI;
    }

    /**
     * M: Get sorted calls list, the order is ACTIVE, HOLDING, RINGING, CONNECTING...
     *
     * @return The sorted call list.
     */
    public List getSortedCalls() {
        int callNum = size();

        if (callNum == 0) {
            return null;
        }

        if (callNum == 1) {
            return mCalls;
        }

        LinkedList<Call> sortedCalls = new LinkedList<Call>(mCalls);

        Call conferenceCall = null;
        for (Call call : sortedCalls) {
            if (call.getChildren().size() > 0) {
                // Only one conference call.
                conferenceCall = call;
                sortedCalls.remove(call);
                break;
            }
        }

        Collections.sort(sortedCalls, new Comparator<Call>() {
            @Override
            public int compare(Call call1, Call call2) {
                return call2.getState() - call1.getState();
            }
        });

        // Always add conference call as first.
        if (conferenceCall != null) {
            sortedCalls.addFirst(conferenceCall);
        }

        return sortedCalls;
    }

    /** {@inheritDoc} */
    @Override
    public void onDetailsChanged(Call call, Details details) {
        Log.i(TAG, "onDetailsChanged details = " + details);
        setVideoCall(call, new VideoProfile(details.getVideoState()));
    }

    /**
     * M: Check whether there is video call existed.
     *
     * @return Has video call or not.
     */
    public boolean hasVideoCall() {
        for (Call call : mCalls) {
            int status = call.getDetails().getVideoState();
            if (status == VideoProfile.STATE_BIDIRECTIONAL
                    || status == VideoProfile.STATE_TX_ENABLED
                    || status == VideoProfile.STATE_RX_ENABLED) {
                return true;
            }
        }

        return false;
    }

    /**
     * M: Get call category ArrayMap.
     *
     * @return The call category map.
     */
    public ArrayMap getCallCategory() {
        return mCallCategory;
    }

    /**
     * M: Set context.
     */
    public void setContext(Context context) {
        mContext = context;
    }

    /* M: Set video call, like camera, surface. */
    private void setVideoCall(Call call, VideoProfile videoProfile) {
        if (mTestInCallUI != null) {
            mTestInCallUI.setVideoCall(call, videoProfile);
        }
    }

    /* M: Set accept video call */
    private void setAcceptVideoEnabled(Call call, VideoProfile videoProfile) {
        if (mTestInCallUI != null) {
            mTestInCallUI.setAcceptVideoEnabled(call, videoProfile);
        }
    }

    /* M: Handle call added event. */
    private void handleCallAdded(Call call) {
        // Avoid timing issue. Video call change event maybe
        // received before add call event.
        onVideoCallChanged(call, call.getVideoCall());

        /// VzW requirement.
        mCallCategory.put(call.getDetails().getTelecomCallId(), 0);
        if (mTestInCallUI != null) {
            mTestInCallUI.updateButtonsStatus();
        }

        showEccNotificationIfNeeded(call);
    }

    /* M: Show ECC notification. */
    private void showEccNotificationIfNeeded(Call call) {
        boolean isRttRequest = call.getDetails().getIntentExtras().getBoolean(
                TelecomManager.EXTRA_START_CALL_WITH_RTT, false);
        if (isRttRequest) {
            String number = call.getDetails().getHandle().getSchemeSpecificPart();
            if (PhoneNumberUtils.isEmergencyNumber(number)) {
                TelephonyManager tm =
                    (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
                if (tm != null) {
                    int nt = tm.getDataNetworkType((SubscriptionManager.getSubId(0))[0]);
                    if (!(nt == TelephonyManager.NETWORK_TYPE_LTE
                            || nt == TelephonyManager.NETWORK_TYPE_LTE_CA)) {
                        Toast.makeText(mContext, "RTT is not available for " +
                                "this 911 call. Your call has been connected as a " +
                                "voice-only call. If you experience difficulties, " +
                                "please place a voice or relay call to 911.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    /**
     * M: ATT, TMO requirement. Check whether speech is present on the RTT call.
     * @param call The call.
     * @return True indicates speech is present on the RTT call.
     */
    public boolean isRttSpeechPresent(Call call) {
        return (call != null && mRttSpeechPresentCalls.get(call.getDetails().getTelecomCallId())
                != null) ? mRttSpeechPresentCalls.get(call.getDetails().getTelecomCallId())
                : false;
    }
}
