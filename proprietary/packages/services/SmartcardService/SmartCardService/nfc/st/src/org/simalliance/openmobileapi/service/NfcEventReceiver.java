package org.simalliance.openmobileapi.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.nfc.INfcAdapterExtras;
import android.nfc.NfcAdapter;
import com.st.android.nfc_extensions.IStNfcAdapterExtras;
import com.st.android.nfc_extensions.StConstants;
import com.st.android.nfc_extensions.StNfcAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.RemoteException;
import android.util.Log;

import com.mediatek.config.SeRuntimeOptions;

import org.simalliance.openmobileapi.internal.ByteArrayConverter;
import org.simalliance.openmobileapi.service.security.AccessControlEnforcer;

import java.util.ArrayList;
import java.util.List;

public final class NfcEventReceiver {
    private static final String LOG_TAG = "NfcEventReceiver";

    private static final boolean DBG = (! ("user".equals(Build.TYPE)));

    private static final String AID_SELECTED
        = "com.android.nfc_extras.action.AID_SELECTED";

    private static final String ACTION_CHECK_X509
        = "org.simalliance.openmobileapi.service.ACTION_CHECK_X509";

    private static final String TRANSACTION_EVENT
        = "com.gsma.services.nfc.action.TRANSACTION_EVENT";

    /* Broadcast receivers */
    private BroadcastReceiver mNfcEventReceiver;

    /* Async task */
    private InitialiseTask mInitialiseTask;

    private final SmartcardService mService;

    public NfcEventReceiver(SmartcardService service) {
        mService = service;

        if (DBG) {
            Log.v(LOG_TAG, Thread.currentThread().getName()
                + " NfcEventReceiver() for ST NFC chip");
        }
    }

    public void onCreate() {
        if (SeRuntimeOptions.isGSMASupport() == false) {
            if (DBG) {
                Log.v(LOG_TAG, "Do not need GSMA API");
            }
            return;
        }

        if (DBG) {
            Log.v(LOG_TAG, Thread.currentThread().getName() + " onCreate");
        }

        mInitialiseTask = new InitialiseTask();
        mInitialiseTask.execute();
    }

    public void onDestroy() {
        if (SeRuntimeOptions.isGSMASupport() == false) {
            if (DBG) {
                Log.v(LOG_TAG, "Do not need GSMA API");
            }
            return;
        }

        if (DBG) {
            Log.v(LOG_TAG, Thread.currentThread().getName() + " onDestroy");
        }
        unregisterNfcEvent(getApplicationContext());
    }

    private Context getApplicationContext() {
        if (mService == null) {
            Log.v(LOG_TAG, "Cannot get context");
            return null;
        }
        return mService.getApplicationContext();
    }

    private PackageManager getPackageManager() {
        if (mService == null) {
            Log.v(LOG_TAG, "Cannot get PackageManager");
            return null;
        }
        return mService.getPackageManager();
    }

    private Terminal getTerminal(String seName) {
        return mService.getTerminal(seName);
    }

    private AccessControlEnforcer getAccessControlEnforcer(String seName) {
        Terminal terminal = getTerminal(seName);
        if (terminal == null) {
            if (DBG) {
                Log.i(LOG_TAG, "Couldn't get terminal for " + seName);
            }
            return null;
        }

        return terminal.getAccessControlEnforcer();
    }

    private void initializeAccessControl(String seName) {
        Terminal terminal = getTerminal(seName);
        if (terminal == null) {
            if (DBG) {
                Log.i(LOG_TAG, "Couldn't get terminal for " + seName);
            }
            return;
        }

        getTerminal(seName).initializeAccessControl(false);
    }

    private final class InitialiseTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (DBG) {
                Log.i(LOG_TAG, "OnPostExecute()");
            }
            registerNfcEvent(getApplicationContext());
            mInitialiseTask = null;
        }
    }

    private void registerNfcEvent(Context context) {
        if (DBG) {
            Log.v(LOG_TAG, "registerNfcEvent()");
        }

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(AID_SELECTED);
        intentFilter.addAction(ACTION_CHECK_X509);

        mNfcEventReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();

                if (DBG) {
                    Log.v(LOG_TAG, "BroadcastReceiver() - action: " + action);
                }

                if (action.equals(ACTION_CHECK_X509)) {
                    actionCheckX509(context, intent);
                } else if (action.equals(AID_SELECTED)) {
                    actionAidSelected(context, intent);
                } else {
                    if (DBG) {
                        Log.i(LOG_TAG, "mNfcEventReceiver got unexpected intent:" + action);
                    }
                }
            }
        };

        context.registerReceiver(mNfcEventReceiver, intentFilter);
    }

    private void unregisterNfcEvent(Context context) {
        if (mNfcEventReceiver != null) {
            if (DBG) {
                Log.v(LOG_TAG, "unregister NFC event");
            }
            if (context != null) {
                context.unregisterReceiver(mNfcEventReceiver);
            }
            mNfcEventReceiver = null;
        }
    }

    private void actionCheckX509(Context context, Intent intent) {
        String pkg
            = intent.getStringExtra("org.simalliance.openmobileapi.service.EXTRA_PKG");
        String seName
            = intent.getStringExtra("org.simalliance.openmobileapi.service.EXTRA_SE_NAME");

        if (DBG) {
            Log.v(LOG_TAG, "BroadcastReceiver() - action: ACTION_CHECK_X509"
                + ", seName: " + seName);
        }

        boolean checkResult = false;

        // Get IStNfcAdapterExtras
        IStNfcAdapterExtras stExtras = getIStNfcAdapterExtras(context);
        if (stExtras == null) {
            if (DBG) {
                Log.i(LOG_TAG, "BroadcastReceiver() - Couldn't get stExtras");
            }
            return;
        }

        /* In normal case, applications will open session or channels for
         * accessing SEs. In this case, it only need to use the access rules
         * from SEs without session or channels. The access rules may be
         * changed without notifying device via external tools. Thus, we
         * should check access rules every time.
         */
        initializeAccessControl(seName);

        // Get AccessControlEnforcer
        AccessControlEnforcer acEnforcer = getAccessControlEnforcer(seName);
        if (acEnforcer == null) {
            if (DBG) {
                Log.i(LOG_TAG, "BroadcastReceiver() - Couldn't get AccessControlEnforcer for "
                    + seName);
            }
        } else {
            // isOperatorCertificatesAllowed ?
            String[] pkgNames = new String[1];
            pkgNames[0] = pkg;
            try {
                boolean[] res = acEnforcer.isOperatorCertificatesAllowed(pkgNames);
                if (res[0] == true) {
                    checkResult = true;
                }
            } catch (Exception e) {
                if (DBG) {
                    Log.e(LOG_TAG, "BroadcastReceiver() - RemoteException: " + e.toString());
                }
            }
        }

        try {
            notifyCheckCertResult(stExtras, pkg, checkResult);
        } catch (RemoteException e) {
            if (DBG) {
                Log.e(LOG_TAG, "BroadcastReceiver() - RemoteException: " + e.toString());
            }
        }
    }

    private void actionAidSelected(Context context, Intent intent) {
        byte[] aid = intent.getByteArrayExtra("com.android.nfc_extras.extra.AID");
        byte[] data = intent.getByteArrayExtra("com.android.nfc_extras.extra.DATA");
        String seName = intent.getStringExtra("com.android.nfc_extras.extra.SE_NAME");

        if (DBG) {
            Log.v(LOG_TAG, "BroadcastReceiver() - aid: "
                + ByteArrayConverter.byteArrayToHexString(aid)
                + ", seName: " + seName);
        }

        if ((aid == null) || (seName == null)) {
            if (DBG) {
                Log.i(LOG_TAG, "BroadcastReceiver() - got AID_SELECTED without AID or SE Name");
            }
            return;
        }

        // Get IStNfcAdapterExtras
        IStNfcAdapterExtras stExtras = getIStNfcAdapterExtras(context);
        if (stExtras == null) {
            if (DBG) {
                Log.i(LOG_TAG, "BroadcastReceiver() - Couldn't get stExtras");
            }
            return;
        }

        if (DBG) {
            Log.i(LOG_TAG, "BroadcastReceiver() - Checking access rules for AID Selected for "
                + seName);
        }

        // Get only packages which have Intentfilter for TRANSACTION_EVENT action
        StringBuffer strAid = new StringBuffer();
        for (int i = 0; i < aid.length; i++) {
            String hex = Integer.toHexString(0xFF & aid[i]);
            if (hex.length() == 1) {
                strAid.append('0');
            }
            strAid.append(hex);
        }

        Intent gsmaIntent = new Intent();
        gsmaIntent.setAction(TRANSACTION_EVENT);
        gsmaIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        gsmaIntent.setData(Uri.parse("nfc://secure:0/" + seName + "/" + strAid));

        List<ResolveInfo> resInfo = getPackageManager().queryIntentActivities(gsmaIntent,
            PackageManager.MATCH_DEFAULT_ONLY | PackageManager.GET_RESOLVED_FILTER);

        if (DBG) {
            Log.d(LOG_TAG, "BroadcastReceiver() - Found resInfo.size() =   " + resInfo.size());
        }

        /* In normal case, applications will open session or channels for
         * accessing SEs. In this case, it only need to use the access rules
         * from SEs without session or channels. The access rules may be
         * changed without notifying device via external tools. Thus, we
         * should check access rules every time.
         */
        initializeAccessControl(seName);

        // Get AccessControlEnforcer
        AccessControlEnforcer acEnforcer = getAccessControlEnforcer(seName);
        if (acEnforcer == null) {
            if (DBG) {
                Log.i(LOG_TAG, "BroadcastReceiver() - Couldn't get AccessControlEnforcer for "
                    + seName);
            }
        } else {
            // isNFCEventAllowed ?
            int index = 0;
            String[] packageNames = new String[resInfo.size()];
            for (ResolveInfo res : resInfo) {
                ActivityInfo activityInfo = res.activityInfo;
                if ((activityInfo != null) && (activityInfo.packageName != null)) {
                    packageNames[index++] = new String(activityInfo.packageName);
                }
            }

            boolean[] nfcEventAccessFinal = null;
            try {
                nfcEventAccessFinal = acEnforcer.isNFCEventAllowed(aid, packageNames);
            } catch (Exception e) {
                if (DBG) {
                    Log.e(LOG_TAG, "BroadcastReceiver() - RemoteException: " + e.toString());
                }
            }
            if (nfcEventAccessFinal != null) {
                List<String> allowedPkgNames = new ArrayList<String>();
                for (int i = 0; i < resInfo.size(); i++) {
                    if (nfcEventAccessFinal[i]) {
                        if (packageNames[i].equals("org.simalliance.openmobileapi.service")) {
                            continue;
                        }
                        allowedPkgNames.add(packageNames[i]);
                    }
                }

                Intent evtIntent = new Intent();
                evtIntent.setAction(getActionMultiEvtTransaction());
                evtIntent.putExtra("com.android.nfc_extras.extra.AID", aid);
                evtIntent.putExtra("com.android.nfc_extras.extra.DATA", data);
                evtIntent.putExtra("com.android.nfc_extras.extra.SE_NAME", seName);
                evtIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);

                try {
                    deliverSeIntent(stExtras, allowedPkgNames, evtIntent);
                } catch (Exception ignore) {
                    //ignore
                }
            }
        }
    }

    /*
     * ST Micro APIs for GSMA
     */
    private IStNfcAdapterExtras getIStNfcAdapterExtras(Context context) {
        NfcAdapter adapter = NfcAdapter.getNfcAdapter(context);
        INfcAdapterExtras adapterExtras = adapter.getNfcAdapterExtrasInterface();
        StNfcAdapter nfcAdapter = StNfcAdapter.getStNfcAdapter(adapter);
        IStNfcAdapterExtras stExtras
            = nfcAdapter.getStNfcAdapterExtrasInterface(adapterExtras);
        return stExtras;
    }

    private String getActionMultiEvtTransaction() {
        return StConstants.ACTION_MULTI_EVT_TRANSACTION;
    }

    private void notifyCheckCertResult(IStNfcAdapterExtras stExtras, String pkg, boolean success)
        throws RemoteException {
        stExtras.notifyCheckCertResult(pkg, success);
    }

    private void deliverSeIntent(IStNfcAdapterExtras stExtras, List<String> pkg, Intent seIntent)
        throws RemoteException {
        stExtras.deliverSeIntent(pkg, seIntent);
    }
}
