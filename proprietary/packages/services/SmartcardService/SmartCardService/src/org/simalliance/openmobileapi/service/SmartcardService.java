/*
 * Copyright (C) 2011, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * Contributed by: Giesecke & Devrient GmbH.
 */

package org.simalliance.openmobileapi.service;

import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
import android.os.RemoteCallbackList;
/// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
import android.os.RemoteException;
import android.util.Log;

/// M: Support GSMA TS.26/TS.27 with ST NFC chip @{
import com.mediatek.config.SeRuntimeOptions;
/// M: Support GSMA TS.26/TS.27 with ST NFC chip @}

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import dalvik.system.DexClassLoader;
/// M: @ {
import org.simalliance.openmobileapi.service.security.AccessControlEnforcer;
/// }

/**
 * The smartcard service is setup with privileges to access smart card hardware.
 * The service enforces the permission
 * 'org.simalliance.openmobileapi.SMARTCARD'.
 */
public final class SmartcardService extends Service {

    public static final String LOG_TAG = "SmartcardService";

    /**
     * For now this list is setup in onCreate(), not changed later and therefore
     * not synchronized.
     */
    private Map<String, Terminal> mTerminals = new TreeMap<>();

    /// M: Support GSMA TS.26/TS.27 with ST NFC chip @{
    private NfcEventReceiver mNfcEventReceiver = null;

    public SmartcardService() {
        super();

        if (SeRuntimeOptions.isGSMASupport() == true) {
            mNfcEventReceiver = new NfcEventReceiver(this);
        }
    }
    /// M: Support GSMA TS.26/TS.27 with ST NFC chip @}

    @Override
    public IBinder onBind(Intent intent) {
        Log.v(LOG_TAG, Thread.currentThread().getName() + " smartcard service onBind");
        if ("org.simalliance.openmobileapi.BIND_SERVICE".equals(intent.getAction())) {
            return new SmartcardServiceBinder();
        }
        return null;
    }

    @Override
    public void onCreate() {
        Log.v(LOG_TAG, Thread.currentThread().getName() + " smartcard service onCreate");

        /// M: All available readers shall be listed even if no card is inserted @{
        createTerminals();

        /// M: Support GSMA TS.26/TS.27 with ST NFC chip @{
        if (mNfcEventReceiver != null) {
            mNfcEventReceiver.onCreate();
        }
        /// M: Support GSMA TS.26/TS.27 with ST NFC chip @}
        /// M: All available readers shall be listed even if no card is inserted @}
    }

    @Override
    public void dump(FileDescriptor fd, PrintWriter writer, String[] args) {
        writer.println("SMARTCARD SERVICE (dumpsys activity "
                + "service org.simalliance.openmobileapi)");
        writer.println();

        String prefix = "  ";

        if (!Build.IS_DEBUGGABLE) {
            writer.println(prefix + "Your build is not debuggable!");
            writer.println(prefix + "Smartcard service dump is only available"
                    + "for userdebug and eng build");
        } else {
            writer.println(prefix + "List of terminals:");
            for (Terminal terminal : mTerminals.values()) {
                writer.println(prefix + "  " + terminal.getName());
            }
            writer.println();

            for (Terminal terminal : mTerminals.values()) {
                terminal.dump(writer, prefix);
            }
        }
    }

    public void onDestroy() {
        Log.v(LOG_TAG, " smartcard service onDestroy ...");

        /// M: Support GSMA TS.26/TS.27 with ST NFC chip @{
        if (mNfcEventReceiver != null) {
            mNfcEventReceiver.onDestroy();
        }
        /// M: Support GSMA TS.26/TS.27 with ST NFC chip @}

        for (Terminal terminal : mTerminals.values()) {
            terminal.onSmartcardServiceShutdown();
        }

        Log.v(LOG_TAG, Thread.currentThread().getName()
                + " ... smartcard service onDestroy");
    }

    private ISmartcardServiceReader getReader(String reader) {
        if (reader == null) {
            throw new NullPointerException("Reader must not be null");
        }
        Terminal terminal = mTerminals.get(reader);
        if (terminal == null) {
            throw new IllegalArgumentException("Unknown reader");
        }
        return terminal.getBinder();
    }

    /**
     * Checks if a terminal is valid or not. The policy is the following:
     * - Terminal must require the org.simalliance.openmobileapi.BIND_TERMINAL permission. This is
     *   to make sure that only SmartcardService can bind to it, so third-party apps cannot bypass
     *   SmartcardService.
     * - If terminal type is SIM, eSE or SD (i.e., is a "system" terminal), it must declare the
     *   org.simalliance.openmobileapi.SYSTEM_TERMINAL permission. This is to avoid that a malware
     *   app can impersonate a system terminal.
     *
     * @param terminalType The type of the terminal being used.
     * @param resolveInfo The information we have about the terminal.
     *
     * @return True if the terminal is valid, false otherwise.
     *
     * @throws PackageManager.NameNotFoundException If the package name could not be located.
     */
    private boolean isValidTerminal(String terminalType, ResolveInfo resolveInfo)
        throws PackageManager.NameNotFoundException {
            // Get terminal type
            String packageName = resolveInfo.serviceInfo.applicationInfo.packageName;
            Log.d(LOG_TAG, "Check if "+ packageName + " contains a valid Terminal");
            PackageInfo packageInfo = getPackageManager().getPackageInfo(
                    packageName, PackageManager.GET_PERMISSIONS);
            // Check that terminal service requires the appropriate permission
            if (!"org.simalliance.openmobileapi.BIND_TERMINAL".equals(
                        resolveInfo.serviceInfo.permission)) {
                Log.w(LOG_TAG, "Terminal does not require BIND_TERMINAL permission");
                return false;
            }
            if ("SIM".equalsIgnoreCase(terminalType)
                    || "eSE".equalsIgnoreCase(terminalType)
                    || "SD".equalsIgnoreCase(terminalType)) {
                String[] requestedPermissions = packageInfo.requestedPermissions;
                for(String permission : requestedPermissions) {
                    if("org.simalliance.openmobileapi.SYSTEM_TERMINAL".equals(permission)) {
                        return true;
                    }
                }
                Log.w(LOG_TAG, terminalType +
                    "terminal does not declare SYSTEM_TERMINAL permission");
                return false;
            }
            return true;
        }

    /**
     * Finds all the terminals that are present on the system and adds them to the mTerminals map.
     * or a terminal to be discovered, it must listen to the
     * org.simalliance.openmobileapi.TERMINAL_DISCOVERY intent.
     */
    private void createTerminals() {
        // Find Terminal packages
        PackageManager pm = getApplicationContext().getPackageManager();
        List<ResolveInfo> terminallist = pm.queryIntentServices(
                new Intent("org.simalliance.openmobileapi.TERMINAL_DISCOVERY"),
                PackageManager.GET_INTENT_FILTERS);
        Log.d(LOG_TAG, "Found " + terminallist.size() + " terminals.");
        Collections.sort(terminallist,new ServiceInfoPackageNameComparator());
        for(int i=0;i<terminallist.size();i++) {
            if(terminallist.get(0).serviceInfo.packageName.contains(
                        "org.simalliance.openmobileapi.uicc")) {
                break;
            } else {
                terminallist.add(terminallist.get(0));
                terminallist.remove(0);
            }
        }
        Log.d(LOG_TAG, "Dump for terminals:");
        for(int i=0;i<terminallist.size();i++) {
            Log.d(LOG_TAG, i+":"+terminallist.get(i).serviceInfo.packageName);
        }
        for (ResolveInfo info : terminallist) {
            try {
                String terminalType = (String) info.loadLabel(pm);
                if (!isValidTerminal(terminalType, info)) {
                    Log.w(LOG_TAG, "Invalid Terminal of type " + terminalType + ", not added");
                    continue;
                }
                String name = terminalType + getIndexForTerminal(terminalType);
                Log.d(LOG_TAG, "Adding terminal " + name);
                mTerminals.put(name, new Terminal(SmartcardService.this, name, info));
            } catch (Exception e) {
                Log.e(LOG_TAG, Thread.currentThread().getName()
                        + " CreateReaders Error: "
                        + ((e.getMessage() != null) ? e.getMessage()
                            : "unknown"), e);
            }
        }
    }

    private String[] createTerminalNamesList() {
        Set<String> names = mTerminals.keySet();
        ArrayList<String> list = new ArrayList<>(names);

        return list.toArray(new String[list.size()]);
    }

    /**
     * Computes the index that should be assigned to each terminal.
     *
     * @param type of the terminal to compute the index for.
     *
     * @return The index that shall be assigned to the given terminal.
     */
    private int getIndexForTerminal(String type) {
        return getTerminalsOfType(type).length + 1;
    }

    /**
     * Returns an array of terminals of the specified type (SIM/eSE/SD/...).
     *
     * @param terminalType The type of the terminals to be retrieved.
     *
     * @return An array of terminals of the specified type.
     */
    private Terminal[] getTerminalsOfType(String terminalType) {
        ArrayList<Terminal> terminals = new ArrayList<>();
        int index = 1;
        String name = terminalType + index;
        while (mTerminals.containsKey(name)) {
            terminals.add(mTerminals.get(name));
            index++;
            name = terminalType + index;
        }

        return terminals.toArray(new Terminal[terminals.size()]);
    }

    /**
     * The smartcard service interface implementation.
     */
    private class SmartcardServiceBinder extends ISmartcardService.Stub {

        @Override
            public String[] getReaders() throws RemoteException {
                return createTerminalNamesList();
            }

        @Override
            public ISmartcardServiceReader getReader(String reader, SmartcardError error)
            throws RemoteException {
                try {
                    return SmartcardService.this.getReader(reader);
                } catch (Exception e) {
                    Log.e(SmartcardService.LOG_TAG, "Error during getReader()", e);
                    error.set(e);
                    return null;
                }
            }
        /// M: @ {
        @Override
            public synchronized boolean[] isNFCEventAllowed(String reader,
                    byte[] aid, String[] packageNames,
                    ISmartcardServiceCallback callback, SmartcardError error)
            throws RemoteException {
                error.clear();
                try {
                    if (callback == null) {
                        error.set(new NullPointerException("callback must not be null"));
                        return null;
                    }
                    Terminal terminal = SmartcardService.this.mTerminals.get(reader);
                    if (terminal == null) {
                        return null;
                    }
                    if (aid == null || aid.length == 0) {
                        aid = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00 };
                    }
                    if (aid.length < 5 || aid.length > 16) {
                        error.set(new IllegalArgumentException("AID out of range"));
                        return null;
                    }
                    if (packageNames == null || packageNames.length == 0) {
                        error.set(new IllegalArgumentException("process names not specified"));
                        return null;
                    }

                    terminal.initializeAccessControl(true, callback);
                    AccessControlEnforcer ac = terminal.getAccessControlEnforcer();
                    if (ac == null) {
                        return null;
                    } else {
                        return ac.isNFCEventAllowed(aid, packageNames);
                    }
                } catch (Exception e) {
                    error.set(e);
                    Log.v(SmartcardService.LOG_TAG, "isNFCEventAllowed Exception: " +
                        e.getMessage());
                    return null;
                }
            }

        @Override
            public synchronized boolean[] isOperatorCertificatesAllowed(String reader,
                    String[] packageNames,
                    ISmartcardServiceCallback callback, SmartcardError error)
            throws RemoteException {
                error.clear();
                try {
                    if (callback == null) {
                        error.set(new NullPointerException("callback must not be null"));
                        return null;
                    }
                    Terminal terminal = SmartcardService.this.mTerminals.get(reader);
                    if (terminal == null) {
                        return null;
                    }

                    if (packageNames == null || packageNames.length == 0) {
                        error.set(new IllegalArgumentException("process names not specified"));
                        return null;
                    }

                    terminal.initializeAccessControl(true, callback);
                    AccessControlEnforcer ac = terminal.getAccessControlEnforcer();
                    if (ac == null) {
                        return null;
                    } else {
                        return ac.isOperatorCertificatesAllowed(packageNames);
                    }
                } catch (Exception e) {
                    error.set(e);
                    Log.v(SmartcardService.LOG_TAG, "isOperatorCertificatesAllowed Exception: " +
                            e.getMessage());
                    return null;
                }
            }
        /// }

        /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
        @Override
        public void registerCallback(ISmartcardServiceCallback cb) {
            if (cb != null) mCallbacks.register(cb);
        }

        @Override
        public void unregisterCallback(ISmartcardServiceCallback cb) {
            if (cb != null) mCallbacks.unregister(cb);
        }
        /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}
    }

    private static class ServiceInfoPackageNameComparator implements Comparator {
        public int compare(Object object1, Object object2) {
            ResolveInfo p1 = (ResolveInfo) object1;
            ResolveInfo p2 = (ResolveInfo) object2;
            return p1.serviceInfo.packageName.compareTo(p2.serviceInfo.packageName);
        }
    }

    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @{
    /**
     * This is a list of callbacks that have been registered with the service.
     */
    private final RemoteCallbackList<ISmartcardServiceCallback> mCallbacks
            = new RemoteCallbackList<ISmartcardServiceCallback>();

    /**
     * Called by the framework when the event occurs.
     *
     * @param eventType the event type generated by the framework.
     * @param name the name of the reader.
     */
    void notifyEvent(int eventType, String name) {
        if (mCallbackHandler != null) {
            Message msg = mCallbackHandler.obtainMessage(eventType, name);
            mCallbackHandler.sendMessage(msg);
        }
    }

    /**
     * IOErrorEventType – an IOError occurred on the reader.
     */
    static final int IOErrorEventType    = 0x1001;

    /**
     * SEInsertedEventType – the SE was in removed state and has been inserted in the reader.
     */
    static final int SEInsertedEventType = 0x2001;

    /**
     * SERemovalEventType – the SE was in inserted state and has been removed from the reader.
     */
    static final int SERemovalEventType  = 0x2002;

    private final Handler mCallbackHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // Broadcast to all clients
            if (mCallbacks != null) {
                synchronized(mCallbacks) {
                    int eventType = msg.what;
                    String name = (String) msg.obj;

                    final int N = mCallbacks.beginBroadcast();
                    Log.v(SmartcardService.LOG_TAG, "notify eventType=" + eventType
                        + " N=" + N + " name=" + name + " service=" + SmartcardService.this);
                    for (int i=0; i<N; i++) {
                        try {
                            mCallbacks.getBroadcastItem(i).notifyEvent(eventType, name);
                        } catch (RemoteException e) {
                            // The RemoteCallbackList will take care of removing
                            // the dead object for us.
                        }
                    }
                    mCallbacks.finishBroadcast();
                }
            }
        }
    };
    /// M: Support parts of OMAPI v3.2 for GSMA TS.26/TS.27 @}

    /// M: Support GSMA TS.26/TS.27 with ST NFC chip @{
    protected Terminal getTerminal(String name) {
        return mTerminals.get(name);
    }
    /// M: Support GSMA TS.26/TS.27 with ST NFC chip @}
}
