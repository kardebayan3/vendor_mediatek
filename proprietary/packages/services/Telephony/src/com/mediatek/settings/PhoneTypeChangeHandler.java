/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.TelephonyIntents;

public class PhoneTypeChangeHandler {

    private static final String TAG = "PhoneTypeChangeHandler";
    private Context mContext;
    private TelephonyManager mTelephonyManager;
    private int mSlotId;
    private int mPhoneType;
    private OnPhoneTypeChangeListener mListener = null;
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            handleRadioTechnologyChanged(intent);
        }
    };

    public PhoneTypeChangeHandler(Context context, int slotId) {
        mContext = context;
        mTelephonyManager = TelephonyManager.getDefault();
        mSlotId = slotId;
        mPhoneType = mTelephonyManager.getCurrentPhoneTypeForSlot(slotId);
        Log.d(TAG, "handler=" + this + ", slotId=" + mSlotId
                + ", phoneType=" + mPhoneType);
    }

    public void registerOnPhoneTypeChange(OnPhoneTypeChangeListener listener) {
        if (mContext != null) {
            mContext.registerReceiver(mReceiver, new IntentFilter(
                    TelephonyIntents.ACTION_RADIO_TECHNOLOGY_CHANGED));
            mListener = listener;
            Log.d(TAG, "registerOnPhoneTypeChange, handler=" + this
                    + ", listener=" + listener);
        }
    }

    public void unregisterOnPhoneTypeChange() {
        if (mContext != null) {
            mContext.unregisterReceiver(mReceiver);
            Log.d(TAG, "unregisterOnPhoneTypeChange, handler=" + this);
        }
        mListener = null;
    }

    private void handleRadioTechnologyChanged(Intent intent) {
        Bundle extra = intent.getExtras();
        if (extra == null) {
            Log.d(TAG, "handleRadioTechnologyChanged, extra=null");
            return;
        }

        int slotId = extra.getInt(PhoneConstants.PHONE_KEY,
                SubscriptionManager.INVALID_PHONE_INDEX);
        if (SubscriptionManager.isValidSlotIndex(slotId)) {
            int type = mTelephonyManager.getCurrentPhoneTypeForSlot(mSlotId);
            Log.d(TAG, "handleRadioTechnologyChanged, slotId=" + slotId
                    + ", prevPhoneType=" + mPhoneType + ", currPhoneType=" + type);
            if (mPhoneType != type && mListener != null) {
                mListener.onPhoneTypeChanged();
            }
            mPhoneType = type;
        }
    }

    public interface OnPhoneTypeChangeListener {
        void onPhoneTypeChanged();
    }
}

