package com.mediatek.phone.ext;

import android.content.Context;
import android.util.Log;

import com.mediatek.common.util.OperatorCustomizationFactoryLoader;
import com.mediatek.common.util.OperatorCustomizationFactoryLoader.OperatorFactoryInfo;

import java.util.ArrayList;
import java.util.List;

public class OpPhoneCustomizationUtils {

    private static final String LOG_TAG = "OpPhoneCustomizationUtils";
    // list every operator's factory path and name.
    private static final List<OperatorFactoryInfo> sOperatorFactoryInfoList
            = new ArrayList<OperatorFactoryInfo>();

    static OpPhoneCustomizationFactoryBase sFactory = null;

    static {
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP01TeleService.apk",
                        "com.mediatek.op01.phone.plugin.Op01PhoneCustomizationFactory",
                        "com.mediatek.op01.phone.plugin",
                        "OP01"
                )
        );

        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP02TeleService.apk",    // apk name
                         "com.mediatek.phone.op02.plugin.Op02PhoneCustomizationFactory",
                                                                           // factory class
                         "com.mediatek.phone.op02.plugin",                 // apk's package
                         "OP02"                                            // operator id
                                                                           // operator segment
                     ));

       sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP03TeleService.apk",    // apk name
                         "com.mediatek.op03.phone.Op03PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op03.phone",                 // apk's package
                         "OP03"                                            // operator id
                                                                           // operator segment
                     ));
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP06TeleService.apk",    // apk name
                         "com.mediatek.op06.phone.Op06PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op06.phone",                 // apk's package
                         "OP06"                                            // operator id
                )
        );
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP07TeleService.apk", // apk name
                        "com.mediatek.op07.phone.OP07PhoneCustomizationFactory", // factory class
                        "com.mediatek.op07.phone", // apk's package name
                        "OP07" // operator id
                )
        );
       sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP08TeleService.apk",    // apk name
                         "com.mediatek.op08.phone.Op08PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op08.phone",                        // apk's package
                         "OP08"                                            // operator id
                                                                           // operator segment
                )
        );
       sOperatorFactoryInfoList.add(
               new OperatorFactoryInfo("OP09ClibTeleService.apk",    // apk name
                        "com.mediatek.phone.op09Clib.plugin.Op09ClibPhoneCustomizationFactory",
                                                                             // factory class name
                        "com.mediatek.phone.op09Clib.plugin",                // apk's package name
                        "OP09",                                              // operator id
                        "SEGC"                                         // operator segment
                    ));
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP12TeleService.apk",    // apk name
                         "com.mediatek.op12.phone.Op12PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op12.phone",                 // apk's package
                         "OP12"                                            // operator id
                )
        );
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP16TeleService.apk",    // apk name
                         "com.mediatek.op16.phone.Op16PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op16.phone",                 // apk's package
                         "OP16"                                            // operator id
                )
        );
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP18TeleService.apk",    // apk name
                         "com.mediatek.op18.phone.Op18PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op18.phone",                 // apk's package
                         "OP18"                                            // operator id
                )
        );
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP20TeleService.apk",    // apk name
                         "com.mediatek.op20.phone.Op20PhoneCustomizationFactory",
                                                                  // factory class name
                         "com.mediatek.op20.phone",               // apk's package
                         "OP20"                                   // operator id
                )
        );
        sOperatorFactoryInfoList.add(
                new OperatorFactoryInfo("OP112TeleService.apk",    // apk name
                         "com.mediatek.op112.phone.Op112PhoneCustomizationFactory",
                                                                           // factory class name
                         "com.mediatek.op112.phone",                 // apk's package
                         "OP112"                                            // operator id
                                                                           // operator segment
                )
        );
    }

    private static void log(String msg) {
        Log.d(LOG_TAG, msg);
    }
    public static synchronized OpPhoneCustomizationFactoryBase getOpFactory(Context context) {
        if (sFactory == null) {

            sFactory = (OpPhoneCustomizationFactoryBase) OperatorCustomizationFactoryLoader
                        .loadFactory(context, sOperatorFactoryInfoList);
            if (sFactory == null) {
                sFactory = new OpPhoneCustomizationFactoryBase();
            }
        }
        return sFactory;
    }

    public static synchronized void resetOpFactory(Context context) {
        log("resetOpFactory");
        sFactory = null;
    }
}
