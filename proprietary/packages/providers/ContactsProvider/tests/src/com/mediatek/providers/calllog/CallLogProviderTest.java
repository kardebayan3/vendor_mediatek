package com.mediatek.providers.calllog;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.RawContacts;
import android.provider.ContactsContract.Data;
import android.telecom.PhoneAccountHandle;
import android.text.TextUtils;

import com.android.internal.telephony.CallerInfo;
import com.android.internal.telephony.PhoneConstants;
import com.android.providers.contacts.BaseContactsProvider2Test;
import com.android.providers.contacts.CallLogProvider;
import com.android.providers.contacts.CallLogProviderTestable;
import com.android.providers.contacts.testutil.RawContactUtil;

import com.mediatek.provider.MtkCallLog;
import com.mediatek.provider.MtkCallLog.ConferenceCalls;

public class CallLogProviderTest extends BaseContactsProvider2Test{

    public static final String CALL_LOG_UNION_QUERY_URI = "content://call_log/callsjoindataview";

    protected CallLogProviderTestable mCallLogProvider;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mCallLogProvider = addProvider(CallLogProviderTestable.class,
                CallLog.AUTHORITY);
    }

    @Override
    protected void tearDown() throws Exception {
        setUpWithVoicemailPermissions();
        mResolver.delete(Calls.CONTENT_URI_WITH_VOICEMAIL, null, null);
        mResolver.delete(RawContacts.CONTENT_URI, null, null);
        mResolver.delete(ConferenceCalls.CONTENT_URI, null, null);
        super.tearDown();
    }

    protected void setUpWithVoicemailPermissions() {
        mActor.addPermissions(ADD_VOICEMAIL_PERMISSION);
        mActor.addPermissions(READ_VOICEMAIL_PERMISSION);
        mActor.addPermissions(WRITE_VOICEMAIL_PERMISSION);
    }

    protected void setUpForNoVoicemailPermissions() {
        mActor.removePermissions(ADD_VOICEMAIL_PERMISSION);
        mActor.removePermissions(READ_VOICEMAIL_PERMISSION);
        mActor.removePermissions(WRITE_VOICEMAIL_PERMISSION);
    }

    public Uri addCallLog(ContentValues values) {
        CallerInfo ci = new CallerInfo();
        ci.name = "TestCallLog";
        ci.numberType = Phone.TYPE_CUSTOM;
        ci.numberLabel = "Directory";

        String componentName = values.getAsString(Calls.PHONE_ACCOUNT_COMPONENT_NAME);
        String accountId = values.getAsString(Calls.PHONE_ACCOUNT_ID);
        PhoneAccountHandle subscription = null;
        if (!TextUtils.isEmpty(componentName) && !TextUtils.isEmpty(accountId)) {
            String[] names = componentName.split("/");
            if (names.length > 1) {
                String pkg = names[0];
                final ComponentName sComponentName = new ComponentName(
                        names[0], names[1]);
                subscription = new PhoneAccountHandle(
                        sComponentName, accountId);
            }
        }

        String phoneNumber = values.getAsString(Calls.NUMBER);
        Integer callType = values.getAsInteger(Calls.TYPE);
        if (callType == null) {
            callType = Calls.INCOMING_TYPE;
        }
        Integer features = values.getAsInteger(Calls.FEATURES);
        if (features == null) {
            features = 0;
        }
        Long date = values.getAsLong(Calls.DATE);
        if (date == null) {
            date = 0L;
        }
        Integer duration = values.getAsInteger(Calls.DURATION);
        if (duration == null) {
            duration = 0;
        }
        Long dataUsage = values.getAsLong(Calls.DATA_USAGE);
        Boolean isRead = values.getAsBoolean(Calls.IS_READ);
        if (isRead == null) {
            isRead = false;
        }
        Long conferenceId = values.getAsLong(MtkCallLog.Calls.CONFERENCE_CALL_ID);
        if (conferenceId == null) {
            conferenceId = 0L;
        }

        Uri uri = MtkCallLog.Calls.addCall(ci, getMockContext(), phoneNumber, "", "",
                PhoneConstants.PRESENTATION_ALLOWED, callType, features, subscription, date,
                duration, dataUsage, false, null, isRead, conferenceId, 0);

        assertStoredValues(uri, values);
        return uri;
    }

    /**
     * Returns a predefined {@link ContentValues} object based on the provided index.
     */
    public ContentValues getTestCallLogValues(int i) {
        ContentValues values = new ContentValues();
        switch (i) {
            case 0:
                values.put(Calls.NUMBER, "123456");
                values.put(Calls.NUMBER_PRESENTATION, Calls.PRESENTATION_ALLOWED);
                values.put(Calls.TYPE, Calls.MISSED_TYPE);
                values.put(Calls.FEATURES, 0);
                values.put(Calls.DATE, 10);
                values.put(Calls.DURATION, 100);
                values.put(Calls.DATA_USAGE, 1000);
                values.put(Calls.PHONE_ACCOUNT_COMPONENT_NAME, (String) null);
                values.put(Calls.PHONE_ACCOUNT_ID, (Long) null);
                break;
            case 1:
                values.put(Calls.NUMBER, "654321");
                values.put(Calls.NUMBER_PRESENTATION, Calls.PRESENTATION_ALLOWED);
                values.put(Calls.TYPE, Calls.INCOMING_TYPE);
                values.put(Calls.FEATURES, 0);
                values.put(Calls.DATE, 5);
                values.put(Calls.DURATION, 200);
                values.put(Calls.DATA_USAGE, 0);
                values.put(Calls.PHONE_ACCOUNT_COMPONENT_NAME,
                        "com.android.server.telecom/TelecomServiceImpl");
                values.put(Calls.PHONE_ACCOUNT_ID, "sub0");
                break;
            case 2:
                values.put(Calls.NUMBER, "123456");
                values.put(Calls.NUMBER_PRESENTATION, Calls.PRESENTATION_ALLOWED);
                values.put(Calls.TYPE, Calls.OUTGOING_TYPE);
                values.put(Calls.FEATURES, Calls.FEATURES_VIDEO);
                values.put(Calls.DATE, 1);
                values.put(Calls.DURATION, 50);
                values.put(Calls.DATA_USAGE, 2000);
                values.put(Calls.PHONE_ACCOUNT_COMPONENT_NAME, (String) null);
                values.put(Calls.PHONE_ACCOUNT_ID, (Long) null);
                break;
        }
        return values;
    }

    public ContentValues getCallLogValues(int callType, String number) {
        ContentValues values = new ContentValues();
        values.put(Calls.TYPE, callType);
        values.put(Calls.NUMBER, number);
        values.put(Calls.NUMBER_PRESENTATION, Calls.PRESENTATION_ALLOWED);
        values.put(Calls.DATE, 1000);
        values.put(Calls.DURATION, 30);
        values.put(Calls.NEW, 1);
        return values;
    }

    /**
     * Create default contentValues with specified callType.
     */
    public ContentValues getDefaultValues(int callType) {
        ContentValues values = new ContentValues();
        values.put(Calls.TYPE, callType);
        values.put(Calls.NUMBER, "1-800-4664-411");
        values.put(Calls.NUMBER_PRESENTATION, Calls.PRESENTATION_ALLOWED);
        values.put(Calls.DATE, 1000);
        values.put(Calls.DURATION, 30);
        values.put(Calls.NEW, 1);
        return values;
    }

    protected ContentValues getDefaultCallValues() {
        return getDefaultValues(Calls.INCOMING_TYPE);
    }

    protected ContentValues getDefaultVoicemailValues() {
        return getDefaultValues(Calls.VOICEMAIL_TYPE);
    }

    /**
     * Create contact with the specified name and number.
     */
    public long createRawContact(ContentResolver resolver, String name, String number) {
        if (TextUtils.isEmpty(name) && TextUtils.isEmpty(number)) {
            return -1;
        }
        long rawContactId = RawContactUtil.createRawContactWithName(resolver, null, name);
        insertPhoneNumber(rawContactId, number);
        return rawContactId;
    }

//    public Uri getCallLogUnionQueryUri(String callLogId) {
//        if (!TextUtils.isEmpty(callLogId)) {
//            return Uri.parse(CALL_LOG_UNION_QUERY_URI).buildUpon().appendPath(callLogId).build();
//        } else {
//            return Uri.parse(CALL_LOG_UNION_QUERY_URI);
//        }
//    }

    public Uri insertCallRecord(ContentResolver resolver, ContentValues value) {
        return resolver.insert(Calls.CONTENT_URI, value);
    }

    public long clearCallLogs(ContentResolver resolver) {
        setUpWithVoicemailPermissions();
        return resolver.delete(Calls.CONTENT_URI_WITH_VOICEMAIL, null, null);
    }

    protected ContentValues insertCallLog(int callType, String phoneNumber, long date,
            long conferenceId) {
        ContentValues callLogValues = getCallLogValues(callType, phoneNumber);
        callLogValues.put(Calls.DATE, date);
        callLogValues.put(MtkCallLog.Calls.CONFERENCE_CALL_ID, conferenceId);
        Uri newCallLogUri = addCallLog(callLogValues);
        long callLogId = ContentUris.parseId(newCallLogUri);
        assertTrue("CallLog did not save successfully!!!", callLogId > 0);
        callLogValues.put(Calls._ID, callLogId);
        return callLogValues;
    }

    public long insertConferenceCall(ContentResolver resolver) {
        ContentValues values = new ContentValues();
        values.put(ConferenceCalls.CONFERENCE_DATE, System.currentTimeMillis());
        Uri result = resolver.insert(ConferenceCalls.CONTENT_URI, values);
        return ContentUris.parseId(result);
    }

    public int clearConferenceCalls(ContentResolver resolver) {
        return resolver.delete(ConferenceCalls.CONTENT_URI, null, null);
    }
}
