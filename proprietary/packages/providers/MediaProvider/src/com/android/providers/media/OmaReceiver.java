package com.android.providers.media;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;

import java.util.ArrayList;

public class OmaReceiver extends BroadcastReceiver {
    private static final String TAG = "MediaProvider/OmacpReceiver";

    // intent actions
    private static final String ACTION_SETTING = "com.mediatek.omacp.settings";
    private static final String ACTION_SETTING_RESULT = "com.mediatek.omacp.settings.result";
    private static final String ACTION_CAPABILITY = "com.mediatek.omacp.capability";
    private static final String ACTION_CAPABILITY_RESULT = "com.mediatek.omacp.capability.result";

    // capablility key
    private static final String KEY_CAPABILITY_RTSP = "rtsp";
    private static final String KEY_CAPABILITY_PROVIDER_ID = "rtsp_provider_id";
    private static final String KEY_CAPABILITY_NAME = "rtsp_name";
    private static final String KEY_CAPABILITY_TO_PROXY = "rtsp_to_proxy";
    private static final String KEY_CAPABILITY_TO_NAPID = "rtsp_to_napid";
    private static final String KEY_CAPABILITY_MAX_BANDWIDTH = "rtsp_max_bandwidth";
    private static final String KEY_CAPABILITY_NET_INFO = "rtsp_net_info";
    private static final String KEY_CAPABILITY_MIN_UDP_PORT = "rtsp_min_udp_port";
    private static final String KEY_CAPABILITY_MAX_UDP_PORT = "rtsp_max_udp_port";

    // setting key
    private static final String KEY_APPID = "APPID";
    private static final String KEY_RESULT = "result";
    private static final String KEY_RESULT_APPID = "appId";

    private static final String KEY_NAME = "NAME";
    private static final String KEY_PROVIDER_ID = "PROVIDER-ID";
    private static final String KEY_TO_PROXY = "TO-PROXY";
    private static final String KEY_TO_NAPID = "TO-NAPID";
    private static final String KEY_MAX_BANDWIDTH = "MAX-BANDWIDTH";
    private static final String KEY_NETINFO = "NETINFO";
    private static final String KEY_MIN_UDP_PORT = "MIN-UDP-PORT";
    private static final String KEY_MAX_UDP_PORT = "MAX-UDP-PORT";
    private static final String KEY_SIM_ID = "simId";

    private static final String AUTHORITY = "media";
    private static final String CONTENT_AUTHORITY_SLASH = "content://" + AUTHORITY + "/";

    /**
     * M: Streaming setting info for OMA. @{
     *
     * @hide
     */
    public static final class Streaming {

        /**
         * Key for oma rtsp setting. All this will be stored in Settings.db
         *
         * @hide
         */
        public interface OmaRtspSettingColumns {
            /**
             * User displayable name for Streaming settings.
             * <br>Type: TEXT
             */
            public static final String NAME = "mtk_rtsp_name";
            /**
             * Allows application settings to be binded to this specific RTSP setting.
             * <br>Type: TEXT
             */
            public static final String PROVIDER_ID = "mtk_rtsp_provider_id";
            /**
             * Logical proxy ID for the RTSP proxy to use. To avoid confusion
             * with other proxies, streaming should use a separate logical
             * proxy (PLogICAL) containing only one physical proxy (PXPHYSICAL).
             * It conrrespondes to the proxyid of apn setting.
             * <br>Type: TEXT
             */
            public static final String TO_PROXY = "mtk_rtsp_to_proxy";
            /**
             * Required if direct use of Network Access Point supported.
             * It conrespondes to the napid of apn setting.
             * <br>Type: TEXT
             */
            public static final String TO_NAPID = "mtk_rtsp_to_napid";
            /**
             * Maximum sustainable bandwidth for all transfer media, in bits
             * per second, indicating the maximum media data throughput in
             * the network. The default value is product-specific.
             * If transfer medium specific bandwidth values are used,
             * this value is the absolute maximum value for all media.
             * <br>Type: TEXT
             */
            public static final String MAX_BANDWIDTH = "mtk_rtsp_max_bandwidth";
            /**
             * Network performance characteristics for a transfer medium.
             * The NETINFO parameter defines the performance the client should
             * expect for audio/video streaming, and may differ from the
             * theoretical network capabilities.
             *
             * The parameters defined for NAPDEF characteristics are not sufficient
             * for a number of reasons: The actual transfer medium
             * and hence the bitrate used may vary even when the same NAP is used,
             * while NAPDEF only defines a single bandwidth value.
             * For example, even if a network supports WCDMA,
             * only GSM coverage may be currently available.
             * NAPDEF/BEARER also does not differentiate between GSM-GPRS and EDGE-GPRS,
             * but their performance is different.
             * Finally, the operator may desire to limit the bandwidth used
             * for streaming to a fraction of the total bandwidth available.
             * <br>Type: TEXT
             */
            public static final String NETINFO = "mtk_rtsp_netinfo";
            /**
             * Minimum UDP port number used for media data traffic (RTP).
             * The default value is product-specific. The value has to be even.
             * <br>Type: TEXT
             */
            public static final String MIN_UDP_PORT = "mtk_rtsp_min_udp_port";
            /**
             * Maximum UDP port number used for media data traffic (RTP).
             * The default value is product-specific.
             * The value must be at least MIN-UDP-PORT + 5 to have enough ports
             * for three media streams (audio, video, timed text), preferably much higher.
             * <br>Type: TEXT
             */
            public static final String MAX_UDP_PORT = "mtk_rtsp_max_udp_port";
            /**
             * The sim card id.
             * <br>Type: INTEGER
             */
            public static final String SIM_ID = "mtk_rtsp_sim_id";
        }

        /**
         * Contains all oma rtsp setting. Generally, it only has one row.
         * @hide
         */
        public static final class OmaRtspSetting implements OmaRtspSettingColumns {
            /**
             * The content:// style URI for the oma rts setting.
             */
            public static final Uri CONTENT_URI =
                Uri.parse(CONTENT_AUTHORITY_SLASH + "internal/streaming/omartspsetting");
            /**
             * The MIME type for this table.
             */
            public static final String CONTENT_TYPE = "vnd.android.cursor.dir/omartspsetting";

        }

        /**
         * Keys for streaming settings.
         * Oma has defined many keys for streamign, so here we just define some other keys.
         * @hide
         */
        public interface SettingColumns {
            /**
             * Whether enable rtsp proxy or not.
             */
            public static final String RTSP_PROXY_ENABLED = "mtk_rtsp_proxy_enabled";
            /**
             * The rtsp proxy host.
             * <br>Type: TEXT
             */
            public static final String RTSP_PROXY_HOST = "mtk_rtsp_proxy_host";
            /**
             * The rtsp proxy port.
             * <br>Type: INTEGER
             */
            public static final String RTSP_PROXY_PORT = "mtk_rtsp_proxy_port";
            /**
             * Whether enable http proxy or not.
             */
            public static final String HTTP_PROXY_ENABLED = "mtk_http_proxy_enabled";
            /**
             * The http proxy host.
             * <br>Type: TEXT
             */
            public static final String HTTP_PROXY_HOST = "mtk_http_proxy_host";
            /**
             * The http proxy port.
             * <br>Type: INTEGER
             */
            public static final String HTTP_PROXY_PORT = "mtk_http_proxy_port";
        }

        /**
         * Setting info for OMA.
         */
        public static final class Setting implements OmaRtspSettingColumns, SettingColumns {

        }
    }

    // app info
    private static final String MY_APP_ID = "554"; // defined in oma cp protocol
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        MtkLog.v(TAG, "onReceive(" + intent.getAction() + ")");
        mContext = context;
        if (ACTION_SETTING.equalsIgnoreCase(intent.getAction())) {
            saveRtspSetting(intent.getExtras());
        } else if (ACTION_CAPABILITY.equalsIgnoreCase(intent.getAction())) {
            sendRtspCapability();
        }
    }

    private void sendRtspCapability() {
        Intent intent = new Intent(ACTION_CAPABILITY_RESULT);
        intent.putExtra(KEY_RESULT_APPID, MY_APP_ID);
        intent.putExtra(KEY_CAPABILITY_RTSP, true);
        intent.putExtra(KEY_CAPABILITY_TO_PROXY, false);
        intent.putExtra(KEY_CAPABILITY_TO_NAPID, false);
        intent.putExtra(KEY_CAPABILITY_NET_INFO, false);
        intent.putExtra(KEY_CAPABILITY_MIN_UDP_PORT, true);
        intent.putExtra(KEY_CAPABILITY_MAX_UDP_PORT, true);
        intent.putExtra(KEY_CAPABILITY_NAME, false);
        intent.putExtra(KEY_CAPABILITY_PROVIDER_ID, false);
        intent.putExtra(KEY_CAPABILITY_MAX_BANDWIDTH, false);
        mContext.sendBroadcast(intent);
        MtkLog.v(TAG, "sendRtspCapability()...");
    }

    private void saveRtspSetting(Bundle extras) {
        MtkLog.v(TAG, "saveRtspSetting(" + extras + ")");
        if (extras != null) {
            String appid = extras.getString(KEY_APPID);
            if (MY_APP_ID.equalsIgnoreCase(appid)) {
                Intent intent = new Intent(ACTION_SETTING_RESULT);
                intent.putExtra(KEY_RESULT_APPID, MY_APP_ID);
                intent.putExtra(KEY_RESULT, writeSetting(extras));
                mContext.sendBroadcast(intent);
            } else {
                MtkLog.v(TAG, "not rtsp app id. appid=" + appid);
            }
        } else {
            MtkLog.w(TAG, "extras is null. cannot set rtsp configuration!");
        }

    }

    private boolean writeSetting(Bundle extras) {
        MtkLog.v(TAG, "writeSetting(" + extras + ")");
        int simid = extras.getInt(KEY_SIM_ID);
        String proxy = "";
        String port = "-1";
        boolean enableProxy = false; // validate(proxy, port);
        String[] key = new String[] {
                Streaming.Setting.NAME,
                Streaming.Setting.PROVIDER_ID,
                Streaming.Setting.MAX_BANDWIDTH,
                Streaming.Setting.MIN_UDP_PORT,
                Streaming.Setting.MAX_UDP_PORT,
                Streaming.Setting.TO_PROXY,
                Streaming.Setting.TO_NAPID,
                Streaming.Setting.NETINFO,
                Streaming.Setting.SIM_ID,
                Streaming.Setting.RTSP_PROXY_HOST,
                Streaming.Setting.RTSP_PROXY_PORT,
                Streaming.Setting.RTSP_PROXY_ENABLED
        };
        String[] value = new String[] {
                extras.getString(KEY_NAME),
                extras.getString(KEY_PROVIDER_ID),
                extras.getString(KEY_MAX_BANDWIDTH),
                extras.getString(KEY_MIN_UDP_PORT),
                extras.getString(KEY_MAX_UDP_PORT),
                catString(extras.getStringArrayList(KEY_TO_PROXY), ","),
                catString(extras.getStringArrayList(KEY_TO_NAPID), ","),
                catString(extras.getStringArrayList(KEY_NETINFO), ";"),
                String.valueOf(extras.getInt(KEY_SIM_ID)),
                proxy,
                port,
                enableProxy ? "1" : "0"
        };
        ContentResolver cr = mContext.getContentResolver();
        int count = 0;
        int size = key.length;
        for (int i = 0; i < size; i++) {
            if (Settings.System.putString(cr, key[i], value[i])) {
                count++;
            }
        }
        MtkLog.v(TAG, "writeSetting() count=" + count);
        if (count > 0) {
            return true;
        }
        return false;
    }

    private String catString(ArrayList<String> list, String seperator) {
        String elements = null;
        int size = list != null ? list.size() : 0;
        if (size > 0) {
            elements = "";
            for (int i = 0; i < size - 1; i++) {
                elements += list.get(i) + seperator;
            }
            elements += list.get(size - 1);
        }
        MtkLog.v(TAG, "catString() return " + elements);
        return elements;
    }
}
