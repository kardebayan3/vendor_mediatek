package com.mediatek.engineermode.mcfconfig;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncResult;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.EmUtils;
import com.mediatek.engineermode.R;

import java.util.Arrays;

public class McfConfigActivity extends Activity implements OnClickListener{

    private static final String TAG = "McfConfig";
    private static final String MCF_CONFIG_SHAREPRE= "mcf_config_settings";
    private final int MSG_MODEM_REBOOT_COMPLETE = 100;
    private static final int OTA_FILE_CODE = 0;
    private static final int OP_OTA_FILE_CODE = 1;
    private static final int SET_OTA_FILEPATH = 0;
    private static final int CLEAR_OTA_FILEPATH = 1;
    protected static final int MSG_RESET_MODEM = 2;
    private static final String DEFAULT_FILE_PATH = "/nvdata/md/mdota";
    private static final String SET_OTA_COMMAND = "AT+EMCFC=0";
    private static final String CMD_RESET_MODEM = "AT+CFUN=1,1";
    private static final String OP_OTA_SUFFIX = "mcfopota";
    private static final String OTA_SUFFIX = "mcfota";
    private static final String OTA_FILEPATH_KEY = "ota_file_path";
    private static final String SIM2_OPOTA_FILEPATH_KEY = "sim2_opota_file_path";
    private static final String SIM1_OPOTA_FILEPATH_KEY = "sim1_opota_file_path";
    private SharedPreferences mcfConfigSh;
    private TextView opOtaFile;
    private TextView otaFile;
    private Button addOtaBtn;
    private Button addOpOtaBtn;
    private Button applyOtaBtn;
    private Button clearOtaBtn;
    private int phoneid;
    private Phone mPhone;
    private String targetOtaFile;
    private String targetOpOtaFile;
    private String targetPath = "/system/vendor/nvdata/md/mdota";
    private boolean isOtaPathValid = false;
    private boolean isOpOtaPathValid = false;
    private boolean mIsModemEnabled = true;
    private CommandsInterface[] sCi = new CommandsInterface[2];

       private final Handler mHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                AsyncResult ar = null;
                switch (msg.what) {
                    case SET_OTA_FILEPATH:
                        ar = (AsyncResult) msg.obj;
                        boolean applyOTA = false;
                        if (ar.exception == null) {
                            applyOTA = true;
                            if (ar.result != null && ar.result instanceof String[]) {
                                String[] data = (String[]) ar.result;
                                Elog.d(TAG, "Apply OTA returned: " + Arrays.toString(data));
                                if ((data.length > 0) && (data[0] != null)) {
                                    if(data[0].indexOf("ERROR") >= 0){
                                        applyOTA = false;
                                    }
                                }
                            }
                        }else {
                            applyOTA = false;
                            Elog.e(TAG, "Apply OTA returned exception:" + ar.exception);
                        }
                        if(applyOTA) {
                            new saveOTAPathTask().execute();
                            rebootModem();
                        } else {
                            showDialog("Apply OTA", "Apply OTA Failed!");
                        }
                        break;
                    case CLEAR_OTA_FILEPATH:
                        ar = (AsyncResult) msg.obj;
                        boolean clearOTA = false;
                        if (ar.exception == null) {
                            clearOTA = true;
                            if (ar.result != null && ar.result instanceof String[]) {
                                String[] data = (String[]) ar.result;
                                Elog.d(TAG, "Clear OTA returned: " + Arrays.toString(data));
                                if ((data.length > 0) && (data[0] != null)) {
                                    if(data[0].indexOf("ERROR") >= 0){
                                        clearOTA = false;
                                    }
                                }
                            }
                        }else {
                            clearOTA = false;
                            Elog.e(TAG, "Clear OTA returned exception:" + ar.exception);
                        }
                        if(clearOTA) {
                            otaFile.setText("");
                            opOtaFile.setText("");
                            targetOtaFile = "";
                            targetOpOtaFile = "";
                            new saveOTAPathTask().execute();
                            rebootModem();
                        } else {
                            showDialog("Clear OTA", "Clear OTA Failed!");
                        }
                        break;
                    case MSG_RESET_MODEM:
                        ar = (AsyncResult) msg.obj;
                        if (ar.exception != null) {
                            Elog.e(TAG, ar.exception.getMessage());
                            showDialog("Reset Modem", "Reset Modem Failed!");
                        } else {
                            Elog.d(TAG, "Command with reset modem sent successfully!");
                            showDialog("Reset Modem", "Reset Modem Success!");
                        }
                        break;
                    case MSG_MODEM_REBOOT_COMPLETE:
                        if (mIsModemEnabled == true) {
                            return;
                        }
                        Elog.e(TAG, "Reset Modem Success!");
                        showDialog("Reset Modem", "Reset Modem Completed!");
                        mIsModemEnabled = true;
                        break;
                }
            }
       };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.em_mcf_config);
        addOtaBtn= (Button) findViewById(R.id.add_ota_browser);
        addOpOtaBtn= (Button) findViewById(R.id.add_op_ota_browser);
        addOtaBtn.setOnClickListener(this);
        addOpOtaBtn.setOnClickListener(this);
        applyOtaBtn = (Button) findViewById(R.id.ota_apply);
        clearOtaBtn = (Button) findViewById(R.id.ota_clear);
        applyOtaBtn.setOnClickListener(this);
        clearOtaBtn.setOnClickListener(this);
        opOtaFile = (TextView) findViewById(R.id.op_ota_file);
        otaFile = (TextView) findViewById(R.id.ota_file);
        Intent intent = getIntent();
        phoneid = intent.getIntExtra("mSimType", PhoneConstants.SIM_ID_1);
        mPhone = PhoneFactory.getPhone(phoneid);
        if (mPhone != null) {
            Elog.d(TAG, "phone id: " + phoneid);
            sCi[phoneid] = mPhone.mCi;
            sCi[phoneid].registerForOn(mHandler, MSG_MODEM_REBOOT_COMPLETE, null);
        } else {
            Log.d(TAG, "phone is null");
        }
        targetPath = "/vendor" + DEFAULT_FILE_PATH;

        mcfConfigSh = getSharedPreferences(MCF_CONFIG_SHAREPRE,
                android.content.Context.MODE_PRIVATE);
        new loadOTAPathTask().execute();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");

        if (sCi[phoneid] != null) {
            sCi[phoneid].unregisterForOn(mHandler);
            sCi[phoneid].unregisterForOffOrNotAvailable(mHandler);
        }

    }

    private void selectFile(int requestCode) {
        Elog.d(TAG, "[selectFile] storagePath: " + targetPath);
        McfFileSelectActivity.actionStart(McfConfigActivity.this, targetPath, requestCode);
    }

    private void showDialog(String title, String msg) {
        AlertDialog dialog = new AlertDialog.Builder(this).setCancelable(
                true).setTitle(title).setMessage(msg).
                setPositiveButton(android.R.string.ok, null).create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            Elog.e(TAG, "[onActivityResult] error, resultCode: " + resultCode);
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        Uri uri = data.getData();
        Elog.d(TAG, "[getSelectedFilePath] uri: " +
                ((uri != null) ? (uri.toString()) : "NULL"));
        if (uri == null) {
            return;
        }
        String srcOtaPath = uri.getPath();
        Elog.i(TAG, "[onActivityResult] otaFile: " + srcOtaPath);
        int ret = -1;
        int startPos = srcOtaPath.lastIndexOf("/") + 1;
        switch(requestCode) {
        case OTA_FILE_CODE:
            isOtaPathValid = checkPathValid(srcOtaPath, null);
            if(isOtaPathValid) {
                otaFile.setText(srcOtaPath);
                targetOtaFile = srcOtaPath;
                Elog.d(TAG, "isOtaPathValid: " + isOtaPathValid +
                        ",targetOtaPath :" + targetOtaFile);
            } else {
                otaFile.setText("");
                showDialog("Select OTA Path: ", "Invalid File! (ex:*.mcfota)");
            }
            break;
        case OP_OTA_FILE_CODE:
            isOpOtaPathValid = checkPathValid(srcOtaPath, null);
            if(isOpOtaPathValid) {
                opOtaFile.setText(srcOtaPath);
                targetOpOtaFile = srcOtaPath;
                Elog.d(TAG, "isOpOtaPathValid: " + isOpOtaPathValid +
                        ",targetOpOtaPath :" + targetOpOtaFile);
            }
            else {
                opOtaFile.setText("");
                showDialog("Select OP-OTA Path: ", "Invalid File! (ex:*.mcfopota)");
            }
            break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean checkPathValid(String filePath, String suffix) {
        if(suffix == null) return true;
        if(FileUtils.getFileExtension(filePath)== null ||
                !FileUtils.getFileExtension(filePath).equals(suffix)) {
            Elog.d(TAG, "[checkPathValid] " + filePath +
                    " has a invliad suffix: " + suffix);
            return false;
        }
        Elog.d(TAG, "[checkPathValid] file path is Valid " + filePath);
        return true;
    }

    private void sendATCommand(String cmd[], int what) {

        Log.d(TAG, "[sendATCommand] cmd: " + Arrays.toString(cmd));
        if (mPhone != null)
            mPhone.invokeOemRilRequestStrings(cmd, mHandler.obtainMessage(what));
        else {
            Elog.d(TAG, "mPhone is null");
        }
    }

    private void rebootModem() {
        Elog.d(TAG, "[rebootModem] begining ...");
        mIsModemEnabled = false;
        EmUtils.rebootModem();
      //  ((MtkRIL) sCi[phoneid]).setTrm(2, null);
    }



    public void getSelectedFilePath(Uri contentUri) {
        Elog.d(TAG, "[getSelectedFilePath] uri: " +
                ((contentUri != null) ? (contentUri.toString()) : "NULL"));
        if (contentUri == null) {
            return;
        }
        Elog.d(TAG, "[getUriForFile] path :" + contentUri.getPath());
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
        case R.id.add_ota_browser:
            selectFile(OTA_FILE_CODE);
            break;
        case R.id.add_op_ota_browser:
            selectFile(OP_OTA_FILE_CODE);
            break;
        case R.id.ota_apply:
            String setOtaCmd = SET_OTA_COMMAND+",";
            if(!isOtaPathValid && !isOpOtaPathValid) {
                showDialog("Apply OTA Error", "OTA and OP-OTA Files are invalid!");
                return;
            }
            if(targetOtaFile == null) {
                setOtaCmd += "\"\",\"\",";
            } else {
                int dirPos = targetOtaFile.lastIndexOf(DEFAULT_FILE_PATH) +
                        DEFAULT_FILE_PATH.length() + 1;
                setOtaCmd += ("\"" +
                        (isOtaPathValid ? targetOtaFile : "") +
                        "\",\""+
                        (isOtaPathValid && dirPos >= 0 ? targetOtaFile.substring(dirPos):"") +
                        "\",");
            }
            if(targetOpOtaFile == null) {
                setOtaCmd += "\"\",\"\"";
            } else {
                int opDirPos = targetOpOtaFile.lastIndexOf(DEFAULT_FILE_PATH) +
                        DEFAULT_FILE_PATH.length() + 1;
                setOtaCmd += ("\"" +
                        (isOpOtaPathValid ? targetOpOtaFile : "") +
                        "\",\""+
                        (isOpOtaPathValid  && opDirPos >= 0 ?
                                targetOpOtaFile.substring(opDirPos) : "") +
                        "\"");
            }
            Elog.d(TAG, "ATCommand: " + setOtaCmd);
            sendATCommand(new String[]{ setOtaCmd, ""}, SET_OTA_FILEPATH);
            break;
        case R.id.ota_clear:
            String clearOtaCmd = (SET_OTA_COMMAND+","+"\"\",\"\",\"\",\"\"");
            Elog.d(TAG, "ATCommand: " + clearOtaCmd);
            sendATCommand(new String[]{clearOtaCmd, ""}, CLEAR_OTA_FILEPATH);
            break;
        }
    }

    /**
     * Class for save OTA path.
     *
     */
    class saveOTAPathTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            saveSharedPreference();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            Elog.d(TAG,  "Save OTA file path success!" );
        }
    }

    /**
     * Class for save OTA path.
     *
     */
    class loadOTAPathTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            getSharedPreference();
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            otaFile.setText(targetOtaFile);
            opOtaFile.setText(targetOpOtaFile);
            Elog.d(TAG,  "Load OTA file path success! OtaFile: " +
                    targetOtaFile + "OpOtaFile: " + targetOpOtaFile);
        }
    }

    private void getSharedPreference() {
        targetOtaFile = mcfConfigSh.getString(OTA_FILEPATH_KEY, "");
        if (phoneid == PhoneConstants.SIM_ID_2) {
            targetOpOtaFile = mcfConfigSh.getString(SIM2_OPOTA_FILEPATH_KEY, "");
        } else {
            targetOpOtaFile = mcfConfigSh.getString(SIM1_OPOTA_FILEPATH_KEY, "");
        }
    }

    private void saveSharedPreference() {
        SharedPreferences.Editor editor = mcfConfigSh.edit();
        editor.putString(OTA_FILEPATH_KEY, targetOtaFile);
        if (phoneid == PhoneConstants.SIM_ID_2) {
            editor.putString(SIM2_OPOTA_FILEPATH_KEY, targetOpOtaFile);
        } else {
            editor.putString(SIM1_OPOTA_FILEPATH_KEY, targetOpOtaFile);
        }
        Elog.d(TAG, "[saveSharedPreference] otaFile success :" +
                targetOtaFile + ",SIM" + phoneid + ":" + targetOpOtaFile);
        editor.commit();
    }
}