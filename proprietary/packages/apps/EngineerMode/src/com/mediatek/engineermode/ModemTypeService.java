package com.mediatek.engineermode;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

import java.util.Arrays;


public class ModemTypeService extends Service {
    private static final String TAG = "ModemTypeService";
    private static final int MSG_GET_MD_TYPE = 0;
    private static Phone mPhone;
    private Handler mCommandHander = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            AsyncResult asyncResult;
            switch (msg.what) {
                case MSG_GET_MD_TYPE:
                    asyncResult = (AsyncResult) msg.obj;
                    final String[] result = (String[]) asyncResult.result;
                    if (asyncResult.exception == null) {
                        Elog.d(TAG, "Query Modem type return: " + Arrays.toString(result));
                        parseCurrentMode(result[0]);
                        FeatureSupport.mGetMdType = true;
                    } else {
                         Elog.e(TAG, "Query Modem type failed");
                    }
                    stopSelf();
                    break;
                default:
                    break;
            }
        }
    };

    private static void sendATCommand(String[] atCommand, Message msg) {
        Elog.d(TAG, "sendATCommand " + atCommand[0]);
        mPhone = PhoneFactory.getPhone(ModemCategory.getCapabilitySim());
        if (mPhone != null) {
            mPhone.invokeOemRilRequestStrings(atCommand, msg);
        }else{
            Elog.e(TAG, "mPhone is null");
        }
    }

    private void parseCurrentMode(String data) {
        int mode = -1;
        Elog.d(TAG, "parseCurrentMode data= " + data);
        try {
            mode = Integer.valueOf(data.substring("+ERXPATH:".length()).trim()).intValue();
        } catch (Exception e) {
            Elog.e(TAG, "Wrong current mode format: " + data);
        }
        if (mode == 0xFF) {
            FeatureSupport.is_support_95_md = true;
            Elog.d(TAG, "it is 95 modem");
        } else {
            FeatureSupport.is_support_95_md = false;
            Elog.d(TAG, "it is not 95 modem");
        }
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Elog.v(TAG, "ModemTypeService services exit");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Elog.v(TAG, "ModemTypeService services start");
        sendATCommand(new String[]{"AT+ERXPATH?", "+ERXPATH:"}, mCommandHander.obtainMessage
                (MSG_GET_MD_TYPE));
        return START_NOT_STICKY;
    }

}

