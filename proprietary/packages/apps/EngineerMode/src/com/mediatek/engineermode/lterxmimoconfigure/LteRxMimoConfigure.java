/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.engineermode.lterxmimoconfigure;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.EmUtils;
import com.mediatek.engineermode.R;


public class LteRxMimoConfigure extends Activity implements View.OnClickListener {
    private static final String TAG = "LteRxMimoConfigure";
    private final static int WARNING_MSG_MIMO = 1;
    private final static int WARNING_MSG_4RX = 2;
    private final static int WARNING_MSG_REBOOT = 3;
    private static final int MSG_QUERY_CMD = 101;
    private static final int MSG_SET_CMD = 102;
    private CheckBox mRxMimo44MomoCb;
    private CheckBox mRxMimoRas4Rx2RxCb;
    private CheckBox mRxMimoRas2Rx1RxCb;
    private CheckBox mRxMimo4RxCb;

    private String mRxMimo44MomoStatus;
    private String mRxMimoRas4Rx2RxStatus;
    private String mRxMimoRas2Rx1RxStatus;
    private String mRxMimo4RxStatus;

    private Button mSetBt;
    private Handler mCommandHander = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            AsyncResult asyncResult = (AsyncResult) msg.obj;
            switch (msg.what) {
                case MSG_QUERY_CMD:
                    final String[] result = (String[]) asyncResult.result;
                    if (result != null && result.length > 0) {
                        Elog.d(TAG, "Query LteRxMimo failed,result = " + result[0]);
                        parseCurrentLteMode(result[0]);
                    } else {
                        Elog.e(TAG, "LteRxMimoConfigure failed!");
                        //parseCurrentLteMode("123");
                    }

                    break;
                case MSG_SET_CMD:
                    if (null == asyncResult.exception) {
                        EmUtils.showToast("LteRxMimoConfigure Set Succeed!");
                        Elog.d(TAG, "LteRxMimoConfigure Set Succeed!");
                    } else {
                        EmUtils.showToast("LteRxMimoConfigure Set failed!");
                        Elog.d(TAG, "LteRxMimoConfigure Set failed!");
                    }
                    break;
                default:
                    break;
            }
        }
    };


    private void parseCurrentLteMode(String data) {
        int mode = -1;
        int rxMimo44MomoStatus = 0;
        int rxMimoRasStatus4rx2rx = 0;
        int rxMimoRasStatus2rx1rx = 0;
        int rxMimo4RxStatus = 0;
        String info[] = data.split(",");
        try {
            rxMimo44MomoStatus = Integer.valueOf(info[1].trim());
            rxMimoRasStatus4rx2rx = Integer.valueOf(info[2].trim());
            rxMimoRasStatus2rx1rx = Integer.valueOf(info[3].trim());
            // rxMimo4RxStatus = Integer.valueOf(info[4].trim());
        } catch (NumberFormatException e) {
            Elog.e(TAG, "Wrong current mode format: " + data);
        }
        mRxMimo44MomoCb.setChecked(rxMimo44MomoStatus == 1);
        mRxMimoRas4Rx2RxCb.setChecked(rxMimoRasStatus4rx2rx == 1);
        mRxMimoRas2Rx1RxCb.setChecked(rxMimoRasStatus2rx1rx == 1);
        //mRxMimo4RxCb.setChecked(rxMimo4RxStatus == 1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.lte_rx_mimo_configure);
        mRxMimo44MomoCb = (CheckBox) findViewById(R.id.lte_rx_mimo_4_momo);
        mRxMimoRas4Rx2RxCb = (CheckBox) findViewById(R.id.lte_rx_mimo_ras_4rx_2rx);
        mRxMimoRas2Rx1RxCb = (CheckBox) findViewById(R.id.lte_rx_mimo_ras_2rx_1rx);
        mRxMimo4RxCb = (CheckBox) findViewById(R.id.lte_rx_mimo_4_rx);
        mRxMimo4RxCb.setVisibility(View.GONE);
        mSetBt = (Button) findViewById(R.id.lte_ca_set_button);
        queryStatus();
        mSetBt.setOnClickListener(this);
    }

    private void queryStatus() {
        sendCommand(new String[]{"AT+EGMC=0,\"rx_mimo_set\"", "+EGMC:"}, MSG_QUERY_CMD);
    }


    private void sendCommand(String[] command, int msg) {
        Elog.d(TAG, "sendCommand " + command[0]);
        EmUtils.invokeOemRilRequestStringsEm(command, mCommandHander.obtainMessage(msg));
    }

    /*
        AT+EGMC=1,“rx_mimo_set", para1, para2, para3, para4
        Para1: 4x4 MIMO enable setting.
                1: UE capability support 4x4 MIMO.
                0: UE capability not support 4x4 MIMO.
        Para2: RAS_4RX_2RX switching enable setting.
                1: RAS_4RX_2RX switching enable.
                0: RAS_4RX_2RX switching disable.
                            Para3: RAS_2RX_1RX switching enable setting.
                            1: RAS_2RX_1RX switching enable.
                0: RAS_2RX_1RX switching disable.
                            Para4: 4RX enable setting.
                1: 4RX enable.
                            0: 4RX disable.
    */
    @Override
    public void onClick(View v) {
        /*if (mRxMimo44MomoCb.isChecked() && !mRxMimo4RxCb.isChecked()) {
            showDialog(WARNING_MSG_MIMO);
            return;
        }
        if ((mRxMimoRas4Rx2RxCb.isChecked() && !mRxMimoRas2Rx1RxCb.isChecked()) ||
                (!mRxMimoRas4Rx2RxCb.isChecked() && mRxMimoRas2Rx1RxCb.isChecked())) {
            showDialog(WARNING_MSG_4RX);
            return;
        }*/
        String cmd = "";
        mRxMimo44MomoStatus = mRxMimo44MomoCb.isChecked() ? "1" : "0";
        mRxMimoRas4Rx2RxStatus = mRxMimoRas4Rx2RxCb.isChecked() ? "1" : "0";
        mRxMimoRas2Rx1RxStatus = mRxMimoRas2Rx1RxCb.isChecked() ? "1" : "0";
        //mRxMimo4RxStatus = mRxMimo4RxCb.isChecked() ? "1" : "0";
        cmd = "AT+EGMC=1,\"rx_mimo_set\"," + mRxMimo44MomoStatus + ","
                + mRxMimoRas4Rx2RxStatus + "," + mRxMimoRas2Rx1RxStatus;
        sendCommand(new String[]{cmd, ""}, MSG_SET_CMD);
        showDialog(WARNING_MSG_REBOOT);
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        AlertDialog.Builder builder = null;
        switch (id) {
            case WARNING_MSG_MIMO:
                return new AlertDialog.Builder(this).setTitle(
                        "LteRxMimoConfigure").setMessage("This setting is invalid!!!\n" +
                        "Can not enable 4X4 MIMO without enable 4RX.\n" +
                        "Please reselect again.\n")
                        .setPositiveButton("OK", null).create();
            case WARNING_MSG_4RX:
                return new AlertDialog.Builder(this).setTitle(
                        "LteRxMimoConfigure").setMessage("This setting is invalid!!!\n" +
                        "Can not just enable one of RAS_4RX_2RX and RAS_2RX_1RX.\n" +
                        "Please reselect again.\n\n")
                        .setPositiveButton("OK", null).create();

            case WARNING_MSG_REBOOT:
                return new AlertDialog.Builder(this).setTitle(
                        "LteRxMimoConfigure").setMessage("“It needs to reboot modem to enable this setting\n")
                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == DialogInterface.BUTTON_POSITIVE) {
                                    EmUtils.rebootModem();
                                }
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", null).create();
        }
        return dialog;
    }
}
