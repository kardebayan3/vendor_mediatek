package com.mediatek.engineermode.desenseat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.mediatek.engineermode.Elog;

public class ATEServerBroadcastReceiver extends BroadcastReceiver {
    public static final String TAG = "ATEServer";
    private static String START_ACTION = "com.mediatek.NotifyServiceStart";
    private static String STOP_ACTION = "com.mediatek.NotifyServiceStop";

    @Override
    public void onReceive(Context context, Intent intent) {
        Elog.d(TAG, "ATEServerBroadcastReceiver -> onReceive");
        String action = intent.getAction();

        if (START_ACTION.equalsIgnoreCase(action)) {
            Elog.d(TAG, "ATEServerBroadcastReceiver -> start ");
            context.startService(new Intent(context, ATEServer.class));
        } else if (STOP_ACTION.equalsIgnoreCase(action)) {
            Elog.d(TAG, "ATEServerBroadcastReceiver -> stop");
            context.stopService(new Intent(context, ATEServer.class));
        }
    }

}
