package com.mediatek.connectivity;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.ConnectivityManager.NetworkCallback;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

public class CdsLceActivity extends Activity implements View.OnClickListener {
    private static final String TAG = "CDSINFO/LCE";

    public static final int MSG_UI_UPDATE = 0;
    public static final int MSG_CONN_UPDATE = 1;

    private Toast mToast;
    private static ConnectivityManager sConnMgr;

    private Button mLceBtnCmd;
    private Button mLceBtn2Cmd;
    private Context mContext;
    private Network mLceNetwork;
    private int     mLceKbps;
    private StringBuilder mResult;
    private TextView mOutputScreen;

    /* Register default mobile data connection */
    private static final NetworkRequest ALL_REQUESTS = new NetworkRequest.Builder()
        .removeCapability(NetworkCapabilities.NET_CAPABILITY_NOT_VPN)
        .removeCapability(NetworkCapabilities.NET_CAPABILITY_NOT_RESTRICTED)
        .removeCapability(NetworkCapabilities.NET_CAPABILITY_TRUSTED)
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .build();

    NetworkCallback mNetworkCallback = new NetworkCallback() {
        @Override
        public void onAvailable(Network network) {
                Log.d(TAG, "onAvailable:" + network);
                mHandler.sendMessage(mHandler.obtainMessage(MSG_CONN_UPDATE));
                mLceNetwork = network;
                mLceKbps = -1;
        }

        @Override
        public void onLost(Network network) {
            Log.d(TAG, "onLost:" + network);
            mHandler.sendMessage(mHandler.obtainMessage(MSG_CONN_UPDATE));
            if (network == mLceNetwork) {
                Log.d(TAG, "Remove network");
                mLceNetwork = null;
                mLceKbps = -1;
            }
        };
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cds_lce);

        mContext = this.getBaseContext();

        sConnMgr = (ConnectivityManager) mContext.getSystemService(
                                            Context.CONNECTIVITY_SERVICE);
        mLceBtnCmd = (Button) findViewById(R.id.LceStart);
        mLceBtnCmd.setOnClickListener(this);

        mLceBtn2Cmd = (Button) findViewById(R.id.LceStop);
        mLceBtn2Cmd.setOnClickListener(this);

        mOutputScreen = (TextView) findViewById(R.id.LceInfo);

        mToast = Toast.makeText(this, null, Toast.LENGTH_SHORT);

        mResult = new StringBuilder();

        Log.i(TAG, "onCreate in CdsLceActivity");
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "onResume");
        super.onResume();
        sConnMgr.registerNetworkCallback(ALL_REQUESTS, mNetworkCallback);
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        super.onPause();
        sConnMgr.unregisterNetworkCallback(mNetworkCallback);
    }

    public void onClick(View v) {
        int buttonId = v.getId();

        switch (buttonId) {
        case R.id.LceStart:
            startLceTest();
            break;
        case R.id.LceStop:
            stopLceTest();
            break;
        default:
            break;
        }

    }

    private void startLceTest() {
        Log.i(TAG, "startLceTest");

        if (!mHandler.hasMessages(MSG_CONN_UPDATE)) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CONN_UPDATE;
            mHandler.sendMessage(msg);
        }
    }

    private void stopLceTest() {
        if (mHandler.hasMessages(MSG_CONN_UPDATE)) {
            mHandler.removeMessages(MSG_CONN_UPDATE);
        }
        mResult.setLength(0);
        mOutputScreen.setText("");
    }

    /**
     *
     * Handler class for handle aysnc tasks or events.
     *
     **/
    private final Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_CONN_UPDATE:
                    runNetworkUpdate();
                    break;
                case MSG_UI_UPDATE:

                    break;
                default:
                    break;
            }
        }
    };

    void runNetworkUpdate() {
        if (mLceNetwork != null) {
            sConnMgr.requestBandwidthUpdate(mLceNetwork);
            updateUiState();
        }
        if (!mHandler.hasMessages(MSG_CONN_UPDATE)) {
            Message msg = mHandler.obtainMessage();
            msg.what = MSG_CONN_UPDATE;
            mHandler.sendMessageDelayed(msg, 3000);
        }
    }

    void updateUiState() {
        NetworkCapabilities info = sConnMgr.getNetworkCapabilities(mLceNetwork);
        if (info != null) {
            if (mResult.length() > 8 * 1024) {
                Log.i(TAG, "Trim the size");
                mResult.setLength(0);
            }
            if (mLceKbps != info.getLinkDownstreamBandwidthKbps()) {
                String currentTime = (String) DateFormat.format("hh:mm:ss", new Date());
                mLceKbps = info.getLinkDownstreamBandwidthKbps();
                mResult.append("DL speed:" + mLceKbps + " kbps (" + mLceKbps / 1024 + " Mbsp)"
                                    + currentTime + "\r\n");
            }
        }
        mOutputScreen.setText(mResult.toString());
     }

}