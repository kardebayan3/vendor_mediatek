package com.mediatek.security.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.IPackageManager;
import android.content.pm.ParceledListSlice;
import android.content.pm.ResolveInfo;
import android.os.Environment;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.AtomicFile;
import android.util.Log;
import android.util.Xml;

import com.android.internal.util.XmlUtils;
import com.mediatek.security.datamanager.CheckedPermRecord;
import com.mediatek.security.service.PermControlUtils;

public class AutoBootManager {
    private static final String TAG = "AutoBootManager";
    private File mFile;
    private static final String FILE_DIR = "data/com.mediatek.security";
    private static final String FILE_NAME = "bootreceiver.xml";
    private static final String PKG_TAG = "pkg";
    private List<CheckedPermRecord> mAutoBootList = new ArrayList<CheckedPermRecord>();
    private Context mContext;
    private int mUid;
    private boolean mIsChanged = false;
    private static AutoBootManager mInstance = null;
    public boolean mIsUsed = false;
    public static synchronized AutoBootManager getInstance(Context context, int uid){
        if (mInstance == null){
            mInstance = new AutoBootManager(context, uid);
        }
        Log.d(TAG, "AutoBootManager" + mInstance);
        return mInstance;
    }
    public AutoBootManager(Context context, int uid) {
        mContext = context;
        mUid = uid;
        File dataDir = Environment.getDataDirectory();
        File systemDir = new File(dataDir, FILE_DIR);
        mFile = new File(systemDir, FILE_NAME);
        Log.d(TAG, "AutoBootManager");
    }
    public void setflag(boolean used) {
        mIsUsed = used;
    }
    public List<CheckedPermRecord> getBootList() {
        return mAutoBootList;
    }

    public boolean isSettingsChanged() {
        return mIsChanged;
    }

    public void setPgkBootPolicy(CheckedPermRecord info) {
        synchronized (mAutoBootList){
            for (CheckedPermRecord record : mAutoBootList) {
                if (info.mPackageName.equals(record.mPackageName)) {
                    if (info.getStatus() == record.getStatus()) {
                        Log.e(TAG, "setPgkBootPolicy, policy:same");
                        return;
                    }
                    record.setStatus(info.getStatus());
                    mIsChanged = true;
                    break;
                }
            }
        }
    }

    public void initAutoBootList() {
        List<String> packages = getBootReceivers(mContext);
        List<String> denyPackages = loadDataFromFile();
        synchronized (mAutoBootList) {
            mAutoBootList.clear();
            for (String pkgName : packages) {
                boolean enable = true;
                if (denyPackages.contains(pkgName)) {
                    enable = false;
                    denyPackages.remove(pkgName);
                }
                mAutoBootList.add(new CheckedPermRecord(pkgName, "", enable));
            }
        }
        if (denyPackages.size() != 0) {
            saveToFile();
        }
    }

    private List<String> getBootReceivers(Context context) {
        List<String> bootReceivers = new ArrayList<String>();
        String[] policy = context
                .getResources()
                .getStringArray(
                        com.mediatek.internal.R.array.config_auto_boot_policy_intent_list);
        Log.d(TAG, "getBootReceivers, policy:" + Arrays.toString(policy));
        IPackageManager pm = IPackageManager.Stub.asInterface(ServiceManager
                .getService("package"));

        for (int i = 0; i < policy.length; i++) {
            Intent intent = new Intent(policy[i]);
            try {
                ParceledListSlice<ResolveInfo> parlist = pm
                        .queryIntentReceivers(intent, intent
                                .resolveTypeIfNeeded(context
                                        .getContentResolver()),
                                PackageManager.GET_META_DATA, mUid);
                if (parlist != null) {
                    List<ResolveInfo> receivers = parlist.getList();
                    if (receivers != null) {
                        for (int j = 0; j < receivers.size(); j++) {
                            ResolveInfo info = receivers.get(j);
                            String packageName =
                              (info.activityInfo != null) ? info.activityInfo.packageName : null;
                            ApplicationInfo appinfo = pm.getApplicationInfo(
                                    packageName, 0, mUid);
                            if (PermControlUtils.isSystemApp(appinfo)) {
                                continue;
                            }
                            if (packageName != null
                                    && !bootReceivers.contains(packageName)) {

                                Log.d(TAG, "getBootReceivers, packageName:"
                                        + packageName);
                                bootReceivers.add(packageName);
                            }
                        }
                    }
                }
            } catch (RemoteException e) {
                // Should never happend
                continue;
            }
        }
        return bootReceivers;
    }

    private List<String> loadDataFromFile() {
        Log.d(TAG, "loadDataFromFileToCache()");
        List<String> results = new ArrayList<String>();
        synchronized (mFile) {
            FileInputStream stream = null;
            boolean success = false;
            try {
                stream = new FileInputStream(mFile);
                XmlPullParser parser = Xml.newPullParser();
                parser.setInput(stream, null);
                int type;
                while ((type = parser.next()) != XmlPullParser.START_TAG
                        && type != XmlPullParser.END_DOCUMENT) {
                    ;
                }

                if (type != XmlPullParser.START_TAG) {
                    throw new IllegalStateException("no start tag found");
                }

                int outerDepth = parser.getDepth();
                while ((type = parser.next()) != XmlPullParser.END_DOCUMENT
                        && (type != XmlPullParser.END_TAG || parser.getDepth() > outerDepth)) {
                    if (type == XmlPullParser.END_TAG
                            || type == XmlPullParser.TEXT) {
                        continue;
                    }
                    String tagName = parser.getName();
                    if (tagName.equals(PKG_TAG)) {
                        String pkgName = parser.getAttributeValue(null, "n");
                        int userId = Integer.parseInt(parser.getAttributeValue(
                                null, "u"));
                        boolean enabled = Boolean.parseBoolean(parser
                                .getAttributeValue(null, "e"));
                        if (enabled == true) {
                            throw new Error();
                        }
                        Log.d(TAG, "Read package name: " + pkgName
                                + " enabled: " + enabled + " at User(" + userId
                                + ")");
                        if (mUid == userId) {
                            results.add(pkgName);
                        }
                    } else {
                        Log.w(TAG, "Unknown element under <boot-receiver>: "
                                + parser.getName());
                        XmlUtils.skipCurrentTag(parser);
                    }
                }
                success = true;
            } catch (IllegalStateException | NullPointerException | NumberFormatException
                    | XmlPullParserException | IOException | IndexOutOfBoundsException e ) {
                    Log.e(TAG, "loadDataFromFile, exception + " + e);
                    e.printStackTrace();
                } finally {
                try {
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                    throw new RuntimeException("Fail to read receiver list");
                }
            }
        }
        return results;
    }

    public void saveToFile() {
        Log.d(TAG, "saveToFile");
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (mFile) {
                List<String> denyList = new ArrayList<String>();
                for (CheckedPermRecord record : mAutoBootList) {
                    if (!record.isEnable()) {
                        denyList.add(record.mPackageName);
                    }
                }

                if (mFile.exists() && (!mFile.getAbsoluteFile().delete())) {
                    Log.e(TAG, "saveToFile, deleteFile failed");
                }

                if (denyList.size() == 0) {
                    Log.d(TAG, "saveToFile, size = 0");
                    return;
                }

                FileOutputStream os = null;
                try {
                    mFile.getAbsoluteFile().createNewFile();
                    os = new FileOutputStream(mFile);
                    XmlSerializer xml = Xml.newSerializer();
                    xml.setOutput(os, "UTF-8");
                    xml.startDocument("UTF-8", true);
                    xml.startTag(null, "bootlist");
                    for (String name : denyList) {
                        Log.d(TAG, "saveToFile, tag = " + name);
                        xml.startTag(null, PKG_TAG);
                        xml.attribute(null, "n", name);
                        xml.attribute(null, "u", String.valueOf(mUid));
                        xml.attribute(null, "e", "false");
                        xml.endTag(null, PKG_TAG);
                    }
                    xml.endTag(null, "bootlist");
                    xml.endDocument();
                } catch (IOException | IllegalArgumentException | IllegalStateException e) {
                    Log.e(TAG, "saveToFile, exception + " + e);
                    e.printStackTrace();
                } finally {
                    if (os != null) {
                        try {
                            os.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    mIsChanged = false;
                    Log.d(TAG, "mIsChanged: false");
                }
                }
            }
        });
        t.start();
    }
}
