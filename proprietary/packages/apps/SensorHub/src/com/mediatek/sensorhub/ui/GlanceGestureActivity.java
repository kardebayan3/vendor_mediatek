package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

public class GlanceGestureActivity extends BaseGestureActivity {
    private static final String TAPE = Sensor.STRING_TYPE_GLANCE_GESTURE;

    public GlanceGestureActivity() {
        super(TAPE);
    }
}
