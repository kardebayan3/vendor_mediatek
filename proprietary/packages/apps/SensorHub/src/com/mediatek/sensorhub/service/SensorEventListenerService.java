package com.mediatek.sensorhub.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEventListener;
import android.hardware.TriggerEvent;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.util.Log;

import com.mediatek.sensorhub.ui.R;
import com.mediatek.sensorhub.settings.MtkSensor;
import com.mediatek.sensorhub.settings.Utils;
import com.mediatek.sensorhub.stresstest.SensorStressTestActivity;

import java.util.Arrays;
import java.util.HashMap;

public class SensorEventListenerService extends Service implements SensorEventListener {
    private static final String TAG = "SensorEventListenerService";
    private static final String NOTIFICATION_CHANNEL = "Sensorhub";
    private static final CharSequence SENSORHUB_NOTIFICATION_NAME = "Sensorhub test is running";
    private static final int ID_SENSORHUB_SERVICE = 180131;
    private SensorManager mSensorManager;
    private NotificationManager mNotificationManager;
    private NotificationChannel mChannel;
    private TriggerListener mTriggerListener = new TriggerListener();
    // record log
    private String mRecordStr;

    // Play notify @{
    private SoundPool mSounds;
    private int mSoundIdF;
    private int mSoundIdT;
    private int mSoundStreamId;
    private AudioManager mAudioManager;
    private int mUiSoundsStreamType;
    // @}
    // for UI update
    private MainHandler mMainHandler;
    // for background sensor change data handle
    private BackgroundHandler mBackgroundHandler;
    private HandlerThread mBackgroundThread;
    private static final String BUNDLE_KEY_LISTENER = "listener_key";
    private static final String BUNDLE_KEY_SENSOR_VALUE = "sensor_value";
    private static final String BUNDLE_KEY_SENSOR_TYPE = "sensor_type";

    // Add for event record string OOM
    private int mCounter;

    private class BackgroundHandler extends Handler {
        static final int MSG_ON_SENSOR_CHANGE = 1;
        static final int MSG_ON_ACCURACY_CHANGE = 2;

        BackgroundHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_ON_SENSOR_CHANGE: {
                SensorEvent sensorEvent = (SensorEvent) msg.obj;
                OnSensorChangedListener listener = null;
                String sensorTypeStr = sensorEvent.sensor.getStringType();
                if (Sensor.STRING_TYPE_STEP_COUNTER.equals(sensorTypeStr)) {
                    Utils.setSensorValues(Utils.KEY_TOTAL_STEP_COUNTER, sensorEvent.values[0]);
                } else if (MtkSensor.STRING_TYPE_FLOOR_COUNT.equals(sensorTypeStr)){
                    Utils.setSensorValues(Utils.KEY_TOTAL_FLOOR_COUNT, sensorEvent.values[0]);
                }
                // Composite sensors
                if (Utils.isCompositeSesnor(sensorTypeStr)) {
                    listener = onSensorChangedListenerMap.get(Utils.KEY_COMPOSITE_STATUS);
                    if (listener != null) {
                        Bundle bundle = new Bundle();
                        bundle.putFloatArray(BUNDLE_KEY_SENSOR_VALUE, sensorEvent.values);
                        bundle.putString(BUNDLE_KEY_LISTENER, Utils.KEY_COMPOSITE_STATUS);
                        bundle.putString(BUNDLE_KEY_SENSOR_TYPE, sensorTypeStr);
                        Message message = mMainHandler.obtainMessage(
                                MainHandler.MSG_COMPOSITE_CHANGE, bundle);
                        mMainHandler.sendMessage(message);
                    }
                } else if (sensorTypeStr != null) {
                    listener = onSensorChangedListenerMap.get(sensorTypeStr);
                    if (listener != null) {
                        Bundle bundle = new Bundle();
                        bundle.putFloatArray(BUNDLE_KEY_SENSOR_VALUE, sensorEvent.values);
                        bundle.putString(BUNDLE_KEY_LISTENER, sensorTypeStr);
                        Message message = mMainHandler.obtainMessage(
                                MainHandler.MSG_COMMON_SENSOR_CHANGE, bundle);
                        mMainHandler.sendMessage(message);
                        if (Sensor.SENSOR_STRING_TYPE_TILT_DETECTOR.equals(sensorTypeStr)) {
                            notifyUser(sensorTypeStr, sensorEvent.values[0]);
                        }
                    }
                }
            }
                break;
            case MSG_ON_ACCURACY_CHANGE:
                Log.d(TAG, "MSG_ON_ACCURACY_CHANGE");
                Bundle bundle = (Bundle) msg.obj;
                String sensorTypeStr = bundle.getString(BUNDLE_KEY_SENSOR_TYPE);
                OnSensorChangedListener listener = null;
                Log.d(TAG, "MSG_ON_ACCURACY_CHANGE : " + sensorTypeStr);

                if (sensorTypeStr != null) {
                    listener = onSensorChangedListenerMap.get(sensorTypeStr);
                    if (listener != null) {
                        Bundle bundleSent = new Bundle();
                        bundleSent.putInt(BUNDLE_KEY_SENSOR_VALUE, bundle
                                .getInt(BUNDLE_KEY_SENSOR_VALUE));
                        bundleSent.putString(BUNDLE_KEY_LISTENER, sensorTypeStr);
                        Message message = mMainHandler.obtainMessage(
                                MainHandler.MSG_ACCURACY_CHANGE, bundleSent);
                        mMainHandler.sendMessage(message);
                        Log.d(TAG, "MSG_ON_ACCURACY_CHANGE : sendMessage");
                    }
                }
                break;
            }
        }
    }

    class MainHandler extends Handler {
        static final int MSG_COMPOSITE_CHANGE = 1;
        static final int MSG_COMMON_SENSOR_CHANGE = 2;
        static final int MSG_ACCURACY_CHANGE = 3;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_COMMON_SENSOR_CHANGE: {
                Bundle bundle = (Bundle) msg.obj;
                onSensorChangedListenerMap.get(bundle.getString(BUNDLE_KEY_LISTENER))
                        .onSensorChanged(bundle.getFloatArray(BUNDLE_KEY_SENSOR_VALUE));
            }
                break;
            case MSG_COMPOSITE_CHANGE: {
                Bundle bundle = (Bundle) msg.obj;
                onSensorChangedListenerMap.get(bundle.getString(BUNDLE_KEY_LISTENER))
                        .onSensorChanged(bundle.getString(BUNDLE_KEY_SENSOR_TYPE),
                                bundle.getFloatArray(BUNDLE_KEY_SENSOR_VALUE));
            }
                break;
            case MSG_ACCURACY_CHANGE: {
                Log.d(TAG, "MSG_ON_ACCURACY_CHANGE : sendMessage");
                Bundle bundle = (Bundle) msg.obj;
                onSensorChangedListenerMap.get(bundle.getString(BUNDLE_KEY_LISTENER))
                        .onAccuracyChanged(bundle.getInt(BUNDLE_KEY_SENSOR_VALUE));
            }
                break;
            }
        }
    }

    private final IBinder mBinder = new LocalBinder();

    public SensorEventListenerService() {

    }

    public interface OnSensorChangedListener {
        public void onSensorChanged(float[] value);

        public void onSensorChanged(String sensorType, float[] value);

        public void onSensorChanged(String sensorType, float[] value, long time, boolean register);

        public void onAccuracyChanged(int accuracy);

        public void onAccuracyChanged(String sensorType, int accuracy);
    }

    private HashMap<String, OnSensorChangedListener> onSensorChangedListenerMap =
        new HashMap<String, OnSensorChangedListener>();

    public void putOnSensorChangedListener(String key, OnSensorChangedListener listener) {
        onSensorChangedListenerMap.put(key, listener);
    }

    public void removeOnSensorChangedListener(String key) {
        onSensorChangedListenerMap.remove(key);
    }

    /**
     * Class used for the client Binder. Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class LocalBinder extends Binder {
        public SensorEventListenerService getService(String key, OnSensorChangedListener listener) {
            putOnSensorChangedListener(key, listener);
            // Return this instance of SensorEventListenerService so clients can
            // call public methods
            return SensorEventListenerService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive: " + action);
            if (Intent.ACTION_SHUTDOWN.equals(action)) {
                stopSelf();
            } else if (Intent.ACTION_SCREEN_ON.equals(action)) {
                // when screen on, flush data
                flush();
            }
        }
    };

    private void flush() {
        if (Utils.getSensorStatus(MtkSensor.STRING_TYPE_ACTIVITY)) {
            Log.d(TAG, "when screen on, register activity use normal mode");
            mSensorManager.flush(this);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        mSensorManager = Utils.getSensorManager();

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mSounds = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
        mSoundIdF = mSounds.load(this, R.raw.in_pocket, 0);
        mSoundIdT = mSounds.load(this, R.raw.non_in_pocket, 0);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SHUTDOWN);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mReceiver, filter);

        mBackgroundThread = new HandlerThread("SensorEventListenerService.changed",
                Process.THREAD_PRIORITY_BACKGROUND);
        mBackgroundThread.start();
        mMainHandler = new MainHandler();
        mBackgroundHandler = new BackgroundHandler(mBackgroundThread.getLooper());
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL, SENSORHUB_NOTIFICATION_NAME,
                NotificationManager.IMPORTANCE_LOW);
        mNotificationManager.createNotificationChannel(mChannel);
        Notification notification = new Notification.Builder(getApplicationContext())
                .setContentTitle(SENSORHUB_NOTIFICATION_NAME)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setChannelId(NOTIFICATION_CHANNEL)
                .build();
        startForeground(ID_SENSORHUB_SERVICE, notification);
        return super.onStartCommand(intent, flag, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        unRegisterAllSensors();
        Utils.restoreStatusToDefult();
        recordLogs(false);
        unregisterReceiver(mReceiver);
        mBackgroundHandler.removeMessages(BackgroundHandler.MSG_ON_SENSOR_CHANGE);
        mBackgroundHandler.removeMessages(BackgroundHandler.MSG_ON_ACCURACY_CHANGE);
        mMainHandler.removeMessages(MainHandler.MSG_COMMON_SENSOR_CHANGE);
        mMainHandler.removeMessages(MainHandler.MSG_COMPOSITE_CHANGE);
        mMainHandler.removeMessages(MainHandler.MSG_ACCURACY_CHANGE);
        if (mBackgroundThread != null) {
            mBackgroundThread.quit();
        }
        super.onDestroy();
    }

    // Register or unRegister sensor
    public void registerSensor(String sensorType, boolean isRegister) {
        Log.d(TAG, "registerSensor " + sensorType + " status " + isRegister);
        // Preference key is the sensor string type
        Sensor sensor = Utils.getSensorKeyMap().get(sensorType);
        boolean isTriggerSensor = (sensor.getReportingMode() == Sensor.REPORTING_MODE_ONE_SHOT);
        if (isTriggerSensor) {
            if (isRegister) {
                mSensorManager.requestTriggerSensor(mTriggerListener, sensor);
            } else {
                mSensorManager.cancelTriggerSensor(mTriggerListener, sensor);
            }
        } else {
            if (isRegister) {
                if (MtkSensor.STRING_TYPE_ACTIVITY.equals(sensorType)) {
                    mSensorManager.registerListener(this, sensor, 1000000, 600000000); // 1s 10min
                } else {
                    mSensorManager
                            .registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
                }
            } else {
                mSensorManager.unregisterListener(this, sensor);
            }
        }
    }

    public void registerSensorWithCustomeTime(String sensorType, boolean isRegister, int rate,
            int latency) {
        Log.d(TAG, "registerSensorWithCustomeTime " + sensorType + " status " + isRegister
                + " rate " + rate + " latency " + latency);
        // Preference key is the sensor string type
        Sensor sensor = Utils.getSensorKeyMap().get(sensorType);
        if (isRegister) {
            mSensorManager.registerListener(this, sensor, rate, latency);
        } else {
            mSensorManager.unregisterListener(this, sensor);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG, "onAccuracyChanged " + sensor.getName());
        Bundle bundle = new Bundle();
        bundle.putInt(BUNDLE_KEY_SENSOR_VALUE, accuracy);
        bundle.putString(BUNDLE_KEY_SENSOR_TYPE, sensor.getStringType());
        Message msg = mBackgroundHandler.obtainMessage(
                BackgroundHandler.MSG_ON_ACCURACY_CHANGE, bundle);
        mBackgroundHandler.sendMessage(msg);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        // For record log
        if (Utils.getSensorStatus(Utils.LOG_STATUS)) {
            mCounter++;
            if (mCounter == 600) { // 1s a event, record once every ten minutes
                mCounter = 0;
                recordLogs(false);
            }
            String pRecordStr = event.sensor.getName() + "@" + event.timestamp
                    + Arrays.toString(event.values);
            mRecordStr = mRecordStr + "\n" + pRecordStr;
        }
        Message msg = mBackgroundHandler.obtainMessage(BackgroundHandler.MSG_ON_SENSOR_CHANGE,
                event);
        mBackgroundHandler.sendMessage(msg);
    }

    class TriggerListener extends TriggerEventListener {
        public void onTrigger(TriggerEvent event) {
            String pRecordStr = event.sensor.getName() + "@" + event.timestamp
                    + Arrays.toString(event.values);
            mRecordStr = mRecordStr + "\n" + pRecordStr;
            String sensorTypeStr = event.sensor.getStringType();
            OnSensorChangedListener listener = null;

            // Notify
            notifyUser(sensorTypeStr, event.values[0]);

            // For others on-shot sensors
            if (sensorTypeStr != null) {
                listener = onSensorChangedListenerMap.get(sensorTypeStr);
                if (listener != null) {
                    listener.onSensorChanged(event.values);
                }
                if (!Utils.getSensorStatus(sensorTypeStr + Utils.KEY_AUTO_TRIGGER_STATUS_SUFFIX)) {
                    Utils.setSensorStatus(sensorTypeStr, false);
                } else {
                    // auto enabled
                    if (Sensor.STRING_TYPE_SIGNIFICANT_MOTION.equals(sensorTypeStr)) {
                        try {
                            Thread.sleep(1000); // SMD sleep 1s
                        } catch (InterruptedException e) {
                            Log.d(TAG, "interrupt");
                        }
                    }
                    mSensorManager.requestTriggerSensor(mTriggerListener, event.sensor);
                }
            }

        }
    }

    // notify user when sensor event change
    private void notifyUser(String key, float value) {
        if (Utils.getSensorStatus(key + Utils.KEY_NOTIFY_STATUS_SUFFIX)) {
            if (value == 1) {
                playSound(mSoundIdF);
            } else {
                if (key.equals(Sensor.SENSOR_STRING_TYPE_TILT_DETECTOR)) {
                    playSound(mSoundIdT);
                }
            }
        }
    }

    private void playSound(int soundId) {
        mSounds.stop(mSoundStreamId);
        if (mAudioManager != null) {
            mUiSoundsStreamType = mAudioManager.getUiSoundsStreamType();
        }
        // If the stream is muted, don't play the sound
        if (mAudioManager.isStreamMute(mUiSoundsStreamType))
            return;
        mSoundStreamId = mSounds.play(soundId, 1, 1, 1/* priortiy */, 0/* loop */, 1.0f/* rate */);
    }

    private void unRegisterAllSensors() {
        mSensorManager.unregisterListener(this);
        mSensorManager.unregisterListener(mStressTestEventListener);
    }

    public void recordLogs(boolean isRecord) {
        if (isRecord) {
            Log.d(TAG, "sensor algorithm test start");
            Utils.initRecordFileName();
        } else {
            Log.d(TAG, "sensor algorithm test end");
            if (!TextUtils.isEmpty(mRecordStr)) {
                Utils.recordToSdcard(mRecordStr.getBytes());
            }
        }
        mRecordStr = "";
        return;
    }

    // Add for Stress test @{
    private SensorEventListener mStressTestEventListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            String sensorType = event.sensor.getStringType();
            onSensorChangedListenerMap.get(SensorStressTestActivity.TAG).onSensorChanged(sensorType,
                    event.values, event.timestamp, true);
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            String sensorType = sensor.getStringType();
            onSensorChangedListenerMap.get(SensorStressTestActivity.TAG)
                .onAccuracyChanged(sensorType, accuracy);
        }
    };

    private TriggerEventListener mStressTestTriggerListener = new TriggerEventListener() {

        public void onTrigger(TriggerEvent event) {
            String sensorType = event.sensor.getStringType();
            onSensorChangedListenerMap.get(SensorStressTestActivity.TAG).onSensorChanged(sensorType,
                    event.values, event.timestamp, true);
        }
    };

    private final int[] SENSOR_DELAY_ARRAY = { SensorManager.SENSOR_DELAY_NORMAL,
            SensorManager.SENSOR_DELAY_GAME, SensorManager.SENSOR_DELAY_FASTEST,
            SensorManager.SENSOR_DELAY_UI };

    private final HashMap<String, Sensor> mTirrgeSensors = new HashMap<>();

    public void stressTestRegisterSensor(Sensor sensor, boolean isRegister, int rateIndex) {
        boolean isTriggerSensor = (sensor.getReportingMode() == Sensor.REPORTING_MODE_ONE_SHOT);
        if (isTriggerSensor) {
            if (isRegister) {
                mSensorManager.requestTriggerSensor(mStressTestTriggerListener, sensor);
                mTirrgeSensors.put(sensor.getStringType(), sensor);
            } else {
                mSensorManager.cancelTriggerSensor(mStressTestTriggerListener, sensor);
                mTirrgeSensors.remove(sensor.getStringType());
            }
        } else {
            if (isRegister) {
                mSensorManager.registerListener(mStressTestEventListener, sensor,
                        SENSOR_DELAY_ARRAY[rateIndex]);
            } else {
                mSensorManager.unregisterListener(mStressTestEventListener, sensor);
                // Can't receive sensor data after unregister listener,
                // so fake a float array data a for updating UI summary
                float[] a = {1f};
                onSensorChangedListenerMap.get(SensorStressTestActivity.TAG).onSensorChanged(
                        sensor.getStringType(), a, 1l, false);
            }
        }
    }

    public void unRegisterStressTestSensor() {
        Log.d("SensorStressTestLog", "unRegisterStressTestSensor ");
        mSensorManager.unregisterListener(mStressTestEventListener);
        for (Sensor sensor: mTirrgeSensors.values()) {
            Log.d("SensorStressTestLog", "unRegisterStressTestSensor sensor : " + sensor.getName());
            mSensorManager.cancelTriggerSensor(mStressTestTriggerListener, sensor);
        }
    }
    // @}
}
