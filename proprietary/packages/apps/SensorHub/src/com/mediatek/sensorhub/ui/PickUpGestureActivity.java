package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

public class PickUpGestureActivity extends BaseGestureActivity {
    private static final String TAPE = Sensor.STRING_TYPE_PICK_UP_GESTURE;

    public PickUpGestureActivity() {
        super(TAPE);
    }
}
