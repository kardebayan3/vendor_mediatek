package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class GameRotationSensorActivity extends CustomerSensorBaseActivity {

    public GameRotationSensorActivity() {
        super(Sensor.STRING_TYPE_GAME_ROTATION_VECTOR);
    }
}
