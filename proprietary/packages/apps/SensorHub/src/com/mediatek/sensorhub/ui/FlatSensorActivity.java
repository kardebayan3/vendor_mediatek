package com.mediatek.sensorhub.ui;

import com.mediatek.sensorhub.settings.MtkSensor;
import com.mediatek.sensorhub.settings.Utils;

public class FlatSensorActivity extends BaseTriggerSensorActivity {

    public FlatSensorActivity() {
        super(MtkSensor.STRING_TYPE_FLAT);
    }
}
