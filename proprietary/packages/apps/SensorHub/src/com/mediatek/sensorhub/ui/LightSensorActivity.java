package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class LightSensorActivity extends CustomerSensorBaseActivity {

    public LightSensorActivity() {
        super(Sensor.STRING_TYPE_LIGHT);
    }
}
