package com.mediatek.sensorhub.ui;

import com.mediatek.sensorhub.settings.MtkSensor;
import com.mediatek.sensorhub.settings.Utils;

public class AnswerCallSensorActivity extends BaseTriggerSensorActivity {

    public AnswerCallSensorActivity() {
        super(MtkSensor.STRING_TYPE_ANSWERCALL);
    }
}
