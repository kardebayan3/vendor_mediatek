package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

public class WakeGestureActivity extends BaseGestureActivity {
    private static final String TAPE = Sensor.STRING_TYPE_WAKE_GESTURE;

    public WakeGestureActivity() {
        super(TAPE);
    }
}
