package com.mediatek.lbs.em2.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.GnssStatus;
import android.location.GpsStatus;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioButton;

import com.mediatek.lbs.em2.ui.NmeaParser.NmeaUpdateViewListener;

public class LbsView extends Activity {
    protected RadioButton mRadioButtonGNSS;
    protected RadioButton mRadioButtonGPS;
    protected RadioButton mRadioButtonNMEA;

    private static final int SHOWING_VIEW_GNSS = 1;
    private static final int SHOWING_VIEW_GPS = 2;
    private static final int SHOWING_VIEW_NMEA = 3;
    private ViewGnss mViewGnss;
    private ViewGps mViewGps;
    private ViewNmea mViewNmea;
    private LocationManager mLocationManager = null;

    private NmeaUpdateViewListener mNmeaListener = null;
    private NmeaParser nmeaInd = NmeaParser.getNMEAParser();
    private int mShowingView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view);

        mViewGnss = (ViewGnss) findViewById(R.id.viewgnss);
        mViewGps = (ViewGps) findViewById(R.id.viewgps);
        mViewNmea = (ViewNmea) findViewById(R.id.viewnmea);
        mRadioButtonGNSS= (RadioButton) findViewById(R.id.RadioButton_GNSS);
        mRadioButtonGPS= (RadioButton) findViewById(R.id.RadioButton_GPS);
        mRadioButtonNMEA= (RadioButton) findViewById(R.id.RadioButton_NMEA);

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (mLocationManager == null) {
            log("ERR: mLocationManager is null");
        }

        mRadioButtonGNSS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mShowingView = SHOWING_VIEW_GNSS;
                showView();
            }
        });
        mRadioButtonGPS.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mShowingView = SHOWING_VIEW_GPS;
                showView();
            }
        });
        mRadioButtonNMEA.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mShowingView = SHOWING_VIEW_NMEA;
                showView();
            }
        });

        ///// GNSS status callback
        mLocationManager.registerGnssStatusCallback(mGnssStatusCallback);

        ///// GPS staus callback
        mLocationManager.addGpsStatusListener(mGpsStatusListener);

        ///// MNEA status callback
        mNmeaListener = new NmeaStatusListener();
        nmeaInd.addSVUpdateListener(mNmeaListener);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.location.GPS_ENABLED_CHANGE");
        registerReceiver(mBroadcastReceiver, intentFilter);

        mShowingView = SHOWING_VIEW_GNSS;
        mRadioButtonGNSS.setChecked(true);
    }

    /// GNSS
    private GnssStatus.Callback mGnssStatusCallback = new GnssStatus.Callback() {
        @Override
        public void onStarted() {
            mViewGnss.resetGnssView();
        }

        @Override
        public void onSatelliteStatusChanged(GnssStatus status) {
            try {
                mViewGnss.setGnssStatus(status);
            } catch (Exception e) {
                Log.e("LocationEM", "Error encountered on setGnssStatus() ", e);
            }
        }
    };

    /// GPS
    private GpsStatus.Listener mGpsStatusListener = new GpsStatus.Listener() {
        @Override
        public void onGpsStatusChanged(int event) {
            if (event == GpsStatus.GPS_EVENT_STARTED) {
                mViewGps.resetGpsView();
            } else if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
                try {
                    mViewGps.setGpsStatus(mLocationManager.getGpsStatus(null));
                } catch (Exception e) {
                    Log.e("LocationEM", "Error encountered on getGpsStatus() ", e);
                }
            }
        }
    };

    /// NMEA
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals("android.location.GPS_ENABLED_CHANGE")) {
                boolean gpsStatus = intent.getBooleanExtra("enabled", false);
                if (gpsStatus) {
                    mViewNmea.resetNmeaView();
                    nmeaInd.clearSatelliteList();
                }
            }
        }
    };
    private class NmeaStatusListener implements NmeaParser.NmeaUpdateViewListener {
        @Override
        public void onViewupdateNotify() {
            mViewNmea.setGpsStatus(nmeaInd.getSatelliteList());
        }
    }

    private void showView() {
        log("showView mShowingView=" + mShowingView);

        mViewGnss.setVisibility(View.GONE);
        mViewGps.setVisibility(View.GONE);
        mViewNmea.setVisibility(View.GONE);

        switch(mShowingView) {
            case SHOWING_VIEW_GNSS:
                mViewGnss.setVisibility(View.VISIBLE);
                break;
            case SHOWING_VIEW_GPS:
                mViewGps.setVisibility(View.VISIBLE);
                break;
            case SHOWING_VIEW_NMEA:
                mViewNmea.setVisibility(View.VISIBLE);
                break;
        }
    }


    //=================== basic utility ========================\\
    @Override
    public void onResume() {
        super.onResume();
        showView();
    }

    protected void onDestroy() {
        super.onDestroy();
        /// GNSS
        mLocationManager.unregisterGnssStatusCallback(mGnssStatusCallback);

        /// GPS
        mLocationManager.removeGpsStatusListener(mGpsStatusListener);

        /// NMEA
        nmeaInd.removeSVUpdateListener(mNmeaListener);
        unregisterReceiver(mBroadcastReceiver);
    }

    private void log(String msg) {
        Log.d("LocationEM", msg);
    }
}
