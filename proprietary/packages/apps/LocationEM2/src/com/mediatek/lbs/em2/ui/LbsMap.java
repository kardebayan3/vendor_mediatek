package com.mediatek.lbs.em2.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.Toast;

public class LbsMap extends Activity implements LocationListener {

    private static final String MAP_URL = "file:///android_asset/demo.html";
    public final static String LOAD_NMEA = "com.mediatek.lbs.em2.action.load_nmea";
    public final static String FIX_LOCATION = "com.mediatek.lbs.em2.action.fix_location";
    private WebView         mWebView;
    private Location        mRecentLocation;
    private Handler         mHandler = null;
    private LocationManager mLocationManager;
    private boolean         mIsEnable = false;
    private Button          mButtonMenu;
    private PopupMenu       mMenuPopup;

    LocationInfo[] mLocationInfo = new LocationInfo[] {
            new LocationInfo("Taipei", 25.09107, 121.5598),
            new LocationInfo("China", 35.86166, 104.1953),
            new LocationInfo("Beijing", 39.90421, 116.4074),
            new LocationInfo("USA", 37.09024, -95.7128)};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mWebView    = (WebView) findViewById(R.id.webview2);
        mButtonMenu = (Button) findViewById(R.id.ButtonMenu);

        mMenuPopup = new PopupMenu(this, mButtonMenu);
        mMenuPopup.getMenu().add(0, 0, Menu.NONE, "My Position");
        mMenuPopup.getMenu().add(0, 1, Menu.NONE, "Move to lat/lng");
        mMenuPopup.getMenu().add(0, 2, Menu.NONE, "Go to Country or City");
        mMenuPopup.getMenu().add(0, 3, Menu.NONE, "TAG - Enable").setCheckable(true).setChecked(false);
        mMenuPopup.getMenu().add(0, 4, Menu.NONE, "NMEA - Load");
        mMenuPopup.getMenu().add(1, 5, Menu.NONE, "NMEA - Remove");
        mMenuPopup.getMenu().add(1, 6, Menu.NONE, "NMEA - Show Path").setCheckable(true).setChecked(true);
        mMenuPopup.getMenu().add(1, 7, Menu.NONE, "NMEA - Show Marker").setCheckable(true).setChecked(true);
        mMenuPopup.getMenu().add(0, 8, Menu.NONE, "TRACKING - Enable").setCheckable(true).setChecked(false);
        mMenuPopup.getMenu().setGroupEnabled(1, false);

        mMenuPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getGroupId() == 0 && item.getItemId() == 0) {
                    updateMyPosition();
                } else if (item.getGroupId() == 0 && item.getItemId() == 1) {
                    openDialogLatlng(false);
                } else if (item.getGroupId() == 0 && item.getItemId() == 2) {
                    openDialogAddr();
                } else if (item.getGroupId() == 0 && item.getItemId() == 3) {
                    item.setChecked(!item.isChecked());
                    mWebView.loadUrl("javascript:enableTag(" + item.isChecked() + ")");
                } else if (item.getGroupId() == 0 && item.getItemId() == 4) {
                    Intent intent = new Intent(getApplicationContext(), FileBrowser.class);
                    startActivity(intent);
                } else if (item.getGroupId() == 1 && item.getItemId() == 5) {
                    mWebView.loadUrl("javascript:removeNmeaPoint()");
                    mMenuPopup.getMenu().setGroupEnabled(1, false);
                } else if (item.getGroupId() == 1 && item.getItemId() == 6) {
                    item.setChecked(!item.isChecked());
                    if (item.isChecked()) {
                        mWebView.loadUrl("javascript:showNmeaPath()");
                    } else {
                        mWebView.loadUrl("javascript:hideNmeaPath()");
                    }
                } else if (item.getGroupId() == 1 && item.getItemId() == 7) {
                    item.setChecked(!item.isChecked());
                    if (item.isChecked()) {
                        mWebView.loadUrl("javascript:showNmeaMarker()");
                    } else {
                        mWebView.loadUrl("javascript:hideNmeaMarker()");
                    }
                } else if (item.getGroupId() == 0 && item.getItemId() == 8) {
                    item.setChecked(!item.isChecked());
                    if (item.isChecked()) {
                        mWebView.loadUrl("javascript:enableGpsTrackingPoint(true)");
                    } else {
                        mWebView.loadUrl("javascript:enableGpsTrackingPoint(false)");
                    }
                }
                return true;
            }
        });

        mButtonMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mMenuPopup.show();
            }
        });

        if (isNetworkAvailable()) {
            setupWebView();
        }

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(LOAD_NMEA);
        intentFilter.addAction(FIX_LOCATION);
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(LOAD_NMEA)) {
                String nmeaFile = intent.getExtras().getString("nmeaFile");
                loadNmea(nmeaFile);
            } else if (action.equals(FIX_LOCATION)) {
                Bundle bundle = intent.getExtras();
                double lat = bundle.getDouble("lat");
                double lng = bundle.getDouble("lng");
                boolean hasAcc = bundle.getBoolean("hasAcc");
                float acc = bundle.getFloat("acc");
                boolean needGoTo = bundle.getBoolean("needGoTo", false);

                if (hasAcc == false)
                    acc = -1;

                mWebView.loadUrl("javascript:showMarkerOnly(" + lat +
                        "," + lng + "," + acc + ")");

                if (needGoTo) {
                    mWebView.loadUrl("javascript:setEnableTracking(false)");
                    mWebView.loadUrl("javascript:moveTo(" + lat + "," + lng + ")");
                    mWebView.loadUrl("javascript:setProperZoom(" + acc + ")");
                }
            } else if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (mIsEnable == false && isNetworkAvailable()) {
                    setupWebView();
                }
            }
        }
    };

    @SuppressLint("JavascriptInterface")
    private void setupWebView() {
        mIsEnable = true;
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                updateMyPosition();
                mWebView.loadUrl("javascript:enableTag(false)");
            }
        });
        mWebView.loadUrl(MAP_URL);
        mWebView.addJavascriptInterface(new JavaScriptCallback(), "android");
    }

    @SuppressWarnings(value = {"unused" })
    private class JavaScriptCallback {
        @JavascriptInterface
        public double getLatitude() {
            return mRecentLocation.getLatitude();
        }

        @JavascriptInterface
        public double getLongitude() {
            return mRecentLocation.getLongitude();
        }

        @JavascriptInterface
        public void log(String msg) {
            LbsMap.log(msg);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    @Override
    public void onLocationChanged(Location arg0) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    private static void log(String msg) {
        Log.d("LocationEM", msg);
    }


//=================== finished ========================

    private void updateMyPosition() {
        mRecentLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (mRecentLocation == null) {
            mRecentLocation = new Location(LocationManager.GPS_PROVIDER);
            mRecentLocation.setLatitude(25.084);
            mRecentLocation.setLongitude(121.56);
        }
        int acc;
        if (mRecentLocation.hasAccuracy()) {
            acc = (int) mRecentLocation.getAccuracy();
        } else {
            acc = -1;
        }
        mWebView.loadUrl("javascript:enableTracking(true)");
        mWebView.loadUrl("javascript:showMarkerOnly(" + mRecentLocation.getLatitude() +
                "," + mRecentLocation.getLongitude() + "," + acc + ")");
    }

    private void openDialogAddr() {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Go to somewhere");
        dialog.setContentView(R.layout.map_addr);

        final EditText editText_addr    = (EditText) dialog.findViewById(R.id.EditText_addr);
        final Spinner spinner            = (Spinner) dialog.findViewById(R.id.spinner_locList);
        Button button_ok                = (Button) dialog.findViewById(R.id.Button_ok);
        Button button_cancel            = (Button) dialog.findViewById(R.id.Button_cancel);

        List<String> countries = new ArrayList<String>();
        for (LocationInfo info: mLocationInfo) {
            countries.add(info.country);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, countries);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new ListView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                String country = mLocationInfo[spinner.getSelectedItemPosition()].country;
                editText_addr.setText(country);
            }
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        button_ok.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mWebView.loadUrl("javascript:showMarkerInfoByAddr('" + editText_addr.getText().toString() + "')");
                dialog.dismiss();
            }
        });
        button_cancel.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void openDialogLatlng(boolean warning) {
        final Dialog dialog = new Dialog(this);
        dialog.setTitle("Go to lat/lng");
        dialog.setContentView(R.layout.map_latlng);

        final EditText editText_lat = (EditText) dialog.findViewById(R.id.EditText_lat);
        final EditText editText_lng = (EditText) dialog.findViewById(R.id.EditText_lng);
        Button button_ok            = (Button) dialog.findViewById(R.id.Button_ok);
        Button button_cancel        = (Button) dialog.findViewById(R.id.Button_cancel);

        if (mRecentLocation == null) {
            mRecentLocation = new Location(LocationManager.GPS_PROVIDER);
            mRecentLocation.setLatitude(25.084);
            mRecentLocation.setLongitude(121.56);
        }

        editText_lat.setText(String.valueOf(mRecentLocation.getLatitude()));
        editText_lat.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
        editText_lng.setText(String.valueOf(mRecentLocation.getLongitude()));
        editText_lng.setKeyListener(DigitsKeyListener.getInstance("0123456789."));

        button_ok.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    double lat = Double.valueOf(editText_lat.getText().toString());
                    double lng = Double.valueOf(editText_lng.getText().toString());
                    mWebView.loadUrl("javascript:showMarkerInfoByLatlng(" + lat + "," + lng + ")");
                } catch (NumberFormatException e) {
                    openDialogLatlng(true);
                }
                dialog.dismiss();
            }
        });
        button_cancel.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        if (warning == true) {
            Button button = new Button(LbsMap.this);
            final Dialog dialog1 = new Dialog(LbsMap.this);
            button.setText("OK");
            dialog1.setTitle("Input lat/lng are incorrect");
            dialog1.setContentView(button);
            dialog1.show();
            button.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dialog1.dismiss();
                }
            });
        }
    }

    private void sendMessage(int type) {
        if (mHandler != null) {
            Message m = new Message();
            m.what = type;
            mHandler.sendMessage(m);
        }
    }
    private void msleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
        sendMessage(2);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isNetworkAvailable() == false)
            Toast.makeText(getApplicationContext(), "No Network connection available!!", Toast.LENGTH_SHORT).show();
    }

    class LatLng {
        public double lat = 0;
        public double lng = 0;
    }

    private int strstr(String str1, String str2) {
        if (str1 == null || str2 == null)
            return -1;

        int len = str1.length();
        for (int i = 0; i < len; i++) {
            if (str1.regionMatches(i, str2, 0, str2.length())) {
                return i;
            }
        }
        return -1;
    }

    private void loadNmea(String nmeaFile) {

        mWebView.loadUrl("javascript:removeNmeaPoint()");

        long fileLen = new File(nmeaFile).length();
        log("file=" + nmeaFile + " len=" + fileLen);

        //===== limitation of file size is 20 MB =====
        if (fileLen > 20000000) {
            log("ERR: the file is too huge!!");
            Toast.makeText(getApplicationContext(),
                    "ERR: The file is too huge", Toast.LENGTH_LONG).show();
            return;
        }

        LatLng latlng = new LatLng();

        int numOfFix = countNumOfRMC(nmeaFile);
        int interval = 1;

        log("The number of fix=" + numOfFix);
        if (numOfFix == 0) {
            Toast.makeText(getApplicationContext(), "ERR: No NMEA data", Toast.LENGTH_LONG).show();
            return;
        }

        //==== set the interval if number of fix is more than 100 ===
        if (numOfFix > 100) {
            interval = numOfFix / 100;
            Toast.makeText(getApplicationContext(),
                    "The number of fix is " + numOfFix + ", only show the fix per " + interval + " sec", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "The number of fix is " + numOfFix, Toast.LENGTH_LONG).show();
        }

        try {
            BufferedReader br  = new BufferedReader(new FileReader(nmeaFile));
            String line;
            int count = 0;
            boolean first = true;

            mWebView.loadUrl("javascript:setZoom(14)");

            while ((line = br.readLine()) != null) {
                if (line.contains("$GPRMC")) {
                    //jump to $GPRMC
                    line = line.substring(strstr(line, "$GPRMC"));
                    if (isRmcValid(line, latlng)) {

                        if (first) {
                            first = false;
                            mWebView.loadUrl("javascript:addNmeaInfo(" + latlng.lat + "," + latlng.lng + ",'Start Point')");
                        }
                        if (count % interval == 0) {
                            mWebView.loadUrl("javascript:addNmeaPoint(" + latlng.lat + "," + latlng.lng + ")");
                        }
                        count++;
                    }
                }
            }

            //mWebView.loadUrl("javascript:drawPath()");
            mMenuPopup.getMenu().setGroupEnabled(1, true);
            mMenuPopup.getMenu().getItem(6).setChecked(true);
            mMenuPopup.getMenu().getItem(7).setChecked(true);

        } catch (FileNotFoundException e) {
            log("ERR: FileNotFoundException");
            e.printStackTrace();
        } catch (IOException e) {
            log("ERR: IOException");
            e.printStackTrace();
        }
    }

    //TODO countNumOfRMC needs to check 'V' or 'A'
    private int countNumOfRMC(String nmeaFile) {
        int numOfFix = 0;
        try {
            BufferedReader br  = new BufferedReader(new FileReader(nmeaFile));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.contains("$GPRMC")) {
                    numOfFix++;
                }
            }
        } catch (FileNotFoundException e) {
            log("ERR: FileNotFoundException");
            e.printStackTrace();
        } catch (IOException e) {
            log("ERR: IOException");
            e.printStackTrace();
        }
        return numOfFix;
    }

    private boolean isRmcValid(String rmc, LatLng latlng) {
        if (rmc == null)
            return false;

        String tmp1 = rmc;
        String tmp2;
        int nextComma;
        try {
            @SuppressWarnings(value = {"unused" })
            double utc = 0;
            String status = new String();
            double lat = 0;
            String latDirection = new String();
            double lng = 0;
            String lngDirection = new String();

            //1. UTC Time
            tmp1 = tmp1.substring(strstr(tmp1, ",") + 1);
            nextComma = strstr(tmp1, ",");
            tmp2 = tmp1.substring(0, nextComma);
            if (nextComma > 0) {
                utc = Double.valueOf(tmp2);
            }

            //2. status, A or V
            tmp1 = tmp1.substring(strstr(tmp1, ",") + 1);
            nextComma = strstr(tmp1, ",");
            tmp2 = tmp1.substring(0, nextComma);
            if (nextComma > 0) {
                status = tmp2;
            }

            //3. latitude
            tmp1 = tmp1.substring(strstr(tmp1, ",") + 1);
            nextComma = strstr(tmp1, ",");
            tmp2 = tmp1.substring(0, nextComma);
            if (nextComma > 0) {
                lat = Double.valueOf(tmp2);
            }

            //4. latitude direction, "N" or "S"
            tmp1 = tmp1.substring(strstr(tmp1, ",") + 1);
            nextComma = strstr(tmp1, ",");
            tmp2 = tmp1.substring(0, nextComma);
            if (nextComma > 0) {
                latDirection = tmp2;
            }

            //5. longitude
            tmp1 = tmp1.substring(strstr(tmp1, ",") + 1);
            nextComma = strstr(tmp1, ",");
            tmp2 = tmp1.substring(0, nextComma);
            if (nextComma > 0) {
                lng = Double.valueOf(tmp2);
            }

            //6. longitude direction, "E" or "W"
            tmp1 = tmp1.substring(strstr(tmp1, ",") + 1);
            nextComma = strstr(tmp1, ",");
            tmp2 = tmp1.substring(0, nextComma);
            if (nextComma > 0) {
                lngDirection = tmp2;
            }

            lat = lat / 100;
            int mm = (int) lat;
            lat = lat - mm;
            lat = lat / 6 * 10;
            lat += mm;
            if (!latDirection.equals("N")) {
                lat = -lat;
            }

            lng = lng / 100;
            mm = (int) lng;
            lng = lng - mm;
            lng = lng / 6 * 10;
            lng += mm;
            if (!lngDirection.equals("E")) {
                lng = -lng;
            }

            latlng.lat = lat;
            latlng.lng = lng;

            return status.equals("A") ? true : false;
        } catch (Exception e) {
            log("ERR: isRmcValid, bad string=" + rmc);
        }

        return false;
    }

}


class LocationInfo {
    String country = null;
    double lat = 0;
    double lng = 0;

    public LocationInfo(String c, double lat, double lng) {
        country = c;
        this.lat = lat;
        this.lng = lng;
    }
    public String toString() {
        return "LocationInfo country=" + country + " lat=" + lat + " lng=" + lng;
    }
}
