/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License
 */

package com.mediatek.faceid;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.Log;

import com.android.keyguard.KeyguardConstants;
import com.android.keyguard.KeyguardUpdateMonitor;
import com.android.keyguard.KeyguardUpdateMonitorCallback;

import java.io.PrintWriter;

/**
 * MTK FaceUnlock
 * Controller which coordinates all the face unlock actions with the UI.
 */
public class FaceUnlockController extends KeyguardUpdateMonitorCallback {

    private static final String TAG = "FaceUnlockController";
    private static final boolean DEBUG_FP_WAKELOCK = KeyguardConstants.DEBUG_FP_WAKELOCK;
    private static final long FR_WAKELOCK_TIMEOUT_MS = 5 * 1000;
    private static final long FR_COMPARE_TIMEOUT_MS = 5 * 1000;
    private static final String FR_WAKE_LOCK_NAME = "wake-and-unlock wakelock";
    //TO_DO need to move to settings provider
    public static final String FACEID_ENABLED = "faceid_enable";

    //MSG to stop FR handler
    private static final int MSG_STOP_FR_SERVICE = 1;
    private static final int MSG_CANCEL_FR_COMPARE = 2;
    private static final int MSG_FACE_UNLOCK_SUCCESS = 3;
    private static final int MSG_FACE_UNLOCK_FAIL = 4;

    /** FR state: Handler not running.*/
    public static final int FR_STATE_STOPPED = 0;
    /** FR state: Comparing.*/
    public static final int FR_STATE_RUNNING = 1;
    /**FR state: Cancel compare but handler still working.*/
    public static final int FR_STATE_CANCELLING = 2;

    private static int mFRRunningState = FR_STATE_STOPPED;

    private static final long STOP_FR_DELAY_MS = 10 * 1000;

    //Compare error code
    public static final int COMPARE_START_FAILED = 1;
    public static final int UNLOCK_FAIL_TIMEOUT = 2;

    //feature option
    private static final boolean mIsCAMSecuritySupport =
            SystemProperties.get("ro.vendor.mtk_cam_security", "0").equals("1");
    private static FaceUnlockController mControllerInstance = null;

    public static FaceUnlockController getControllerInstance(Context context) {
        if (mIsCAMSecuritySupport && mControllerInstance == null) {
            mControllerInstance = new FaceUnlockController(context);
        }
        return mControllerInstance;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_STOP_FR_SERVICE:
                    stopFRService();
                    break;
                case MSG_CANCEL_FR_COMPARE:
                    cancelCompare(true);
                    break;
                case MSG_FACE_UNLOCK_SUCCESS:
                    onFaceCompareSuccess();
                    break;
                case MSG_FACE_UNLOCK_FAIL:
                    onFaceCompareFailed(msg.arg1);
                    break;
                default:
                    Log.e(TAG, "Unknown message:" + msg);
            }
        }
    };

    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;
    private KeyguardUpdateMonitor mUpdateMonitor;
    private final Context mContext;
    private FaceIdManager mFaceIdManager;
    private FRHandlerCallback mFRHandlerCallback;

    public FaceUnlockController(Context context) {
        mContext = context;
        mPowerManager = context.getSystemService(PowerManager.class);
        mUpdateMonitor = KeyguardUpdateMonitor.getInstance(context);
        mUpdateMonitor.registerCallback(this);
        mFaceIdManager = FaceIdManager.getInstance();
        mFRHandlerCallback = new FRHandlerCallback();
    }

    private final Runnable mReleaseFRWakeLockRunnable = new Runnable() {
        @Override
        public void run() {
            if (DEBUG_FP_WAKELOCK) {
                Log.i(TAG, "fp wakelock: TIMEOUT!!");
            }
            releaseFRWakeLock();
        }
    };

    private void releaseFRWakeLock() {
        if (mWakeLock != null) {
            mHandler.removeCallbacks(mReleaseFRWakeLockRunnable);
            if (DEBUG_FP_WAKELOCK) {
                Log.i(TAG, "releasing fp wakelock");
            }
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    @Override
    public void onFRStateChanged() {
        boolean shouldListenForFR = mUpdateMonitor.shouldListenForFR();
        if (mFRRunningState == FR_STATE_RUNNING && !shouldListenForFR) {
            Log.d(TAG, "onFRStateChanged will call to stopListeningForFR(false)");
            stopListeningForFR(false);
        } else if (mFRRunningState != FR_STATE_RUNNING && shouldListenForFR) {
            Log.d(TAG, "onFRStateChanged will call to startListeningForFR()");
            startListeningForFR();
        }
    }

    private void stopListeningForFR(boolean stopImmediate) {
        Log.d(TAG, "stopListeningForFR " + stopImmediate);
        //cancelCompare();
        if (stopImmediate) {
            stopFRService();
        } else {
            cancelCompare(false);
            mHandler.removeMessages(MSG_STOP_FR_SERVICE);
            mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_STOP_FR_SERVICE),
                    STOP_FR_DELAY_MS);
        }

    }

    private void startListeningForFR() {
        mHandler.removeMessages(MSG_STOP_FR_SERVICE);
        if (mFRRunningState == FR_STATE_STOPPED) {
            //TO-DO: Need to do it use asynchronous way
            //Error code: 0 means success
            Log.i(TAG, "startListeningForFR will startFRService...");
            int startCommandResult = startFRService();
            if (startCommandResult != 0) {
                Log.e(TAG, "Failed to call start to FR handler: " + startCommandResult);
                return;
            }
        }
        Log.i(TAG, "startListeningForFR will set state to FR_STATE_RUNNING and start to compare");
        //To-Do Do we need to wait for init success before start compare???
        startCompare();
    }

    private void stopFRService() {
        Log.i(TAG, "stopFRService will set state to FR_STATE_STOPPED...");
        mHandler.removeMessages(MSG_CANCEL_FR_COMPARE);
        setFRState(FR_STATE_STOPPED);
        int stopCommandResult = mFaceIdManager.release();
        if (stopCommandResult != 0) {
            Log.e(TAG, "Failed to call stop to FR handler: " + stopCommandResult);
        }
    }

    private int startFRService() {
        Log.i(TAG, "startFRService with user + " + getCurrentUserId());
        int initCommandResult = mFaceIdManager.init(102/*config*/, mFRHandlerCallback);
        return initCommandResult;
    }

    private void startCompare() {
        mHandler.removeMessages(MSG_CANCEL_FR_COMPARE);
        setFRState(FR_STATE_RUNNING);
        //To-Do need to use User name???
        Log.i(TAG, "Need to start compare...");
        int compareCommandResult = mFaceIdManager.startCompareFeature(String.valueOf(mUpdateMonitor.getCurrentUser()));
        if (compareCommandResult != 0) {
            //mUpdateMonitor.onFaceUnlockFailed(getCurrentUserId(), COMPARE_START_FAILED);
            Log.e(TAG, "Failed to call compare to FR handler:" + compareCommandResult);
            stopFRService();
            return;
        }
        Log.i(TAG, "Compare started and will acquire wake lock...");
        releaseFRWakeLock();
        //Do we need to use bright wake lock??
        mWakeLock = mPowerManager.newWakeLock(
                PowerManager.SCREEN_DIM_WAKE_LOCK, FR_WAKE_LOCK_NAME);
        Trace.beginSection("acquiring wake-and-unlock");
        mWakeLock.acquire();
        Trace.endSection();
        if (DEBUG_FP_WAKELOCK) {
            Log.i(TAG, "FR acquired, grabbing fp wakelock");
        }
        mHandler.postDelayed(mReleaseFRWakeLockRunnable,
                FR_WAKELOCK_TIMEOUT_MS);
        mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_CANCEL_FR_COMPARE),
                FR_COMPARE_TIMEOUT_MS);
    }

    private void cancelCompare(boolean timeOut) {
        releaseFRWakeLock();
        Log.i(TAG, "Cancel compare due to timeout: " + timeOut);
        mHandler.removeMessages(MSG_CANCEL_FR_COMPARE);
        setFRState(FR_STATE_CANCELLING);
        int cancelCommandResult = mFaceIdManager.stopCompareFeature();
        if (cancelCommandResult != 0) {
            Log.e(TAG, "Failed to call cancel to FR handler:" + cancelCommandResult);
        }
        if(timeOut) {
            onFaceCompareFailed(UNLOCK_FAIL_TIMEOUT);
        }
    }

    private void setFRState(int state) {
        mFRRunningState = state;
    }

    public static int getFaceCompareState() {
        return mFRRunningState;
    }

    private int getCurrentUserId() {
        return ActivityManager.getCurrentUser();
        //return mUpdateMonitor.getCurrentUser();
    }

    private class FRHandlerCallback implements FaceIdManager.InfoListener {
        @Override
        public void onInfo(int cmd, int errorCode) {
            Log.d(TAG, "FRHandlerCallback onInfo called with cmd=" + cmd + ", errorCode=" + errorCode);
            if (mFRRunningState != FR_STATE_RUNNING) {
                Log.w(TAG, "Face unlock is not running, cannot handle callback");
                return;
            }
            switch (cmd) {
                case FaceIdManager.CMD_START_COMPARE_FACE:
                    Log.i(TAG, "Compare callback received with result:" + errorCode);
                    if(errorCode == 0) {
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_FACE_UNLOCK_SUCCESS));
                    }else {
                        if(mHandler.hasMessages(MSG_FACE_UNLOCK_FAIL)) {
                            mHandler.removeMessages(MSG_FACE_UNLOCK_FAIL);
                        }
                        Message msg = mHandler.obtainMessage(MSG_FACE_UNLOCK_FAIL);
                        msg.arg1 = errorCode;
                        mHandler.sendMessage(msg);
                    }
                    break;
                default:
                    Log.w(TAG, "Command invalid for unlock:" + cmd);
            }
        }
    }

    public void onFaceCompareSuccess() {
        releaseFRWakeLock();
        int userId = getCurrentUserId();
        Log.d(TAG, "onFaceCompareSuccess user:" + userId);
        cancelCompare(false);
        mUpdateMonitor.onFaceUnlockSuccess(userId);
    }

    public void onFaceCompareFailed(int error) {
        //releaseFRWakeLock();
        int userId = getCurrentUserId();
        Log.d(TAG, "onFaceCompareFailed user:" + userId + ",error:" + error);
        mUpdateMonitor.onFaceUnlockFailed(userId, error);
    }

    public void stopFaceUnlockIfNeed() {
        if(mFRRunningState != FR_STATE_STOPPED) {
            stopListeningForFR(true);
        }
    }

}
