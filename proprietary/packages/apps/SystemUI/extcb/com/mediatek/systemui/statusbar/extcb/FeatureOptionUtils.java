package com.mediatek.systemui.statusbar.extcb;

import android.os.SystemProperties;

/**
 * M: The utilities class of feature option definitions.
 */
public class FeatureOptionUtils {

    // Feature support.
    public static final String SUPPORT_YES = "1";

    // Build Type
    public static final String BUILD_TYPE = "ro.build.type";
    public static final String BUILD_TYPE_ENG = "eng";
    public static final String BUILD_TYPE_USER = "user";

    /**
     * Check if CDMA AP IRAT feature is supported.
     * @return True if AP IRAT feature is supported.
     */
    public static final boolean isCdmaApIratSupport() {
        return isCdmaIratSupport() && !isCdmaMdIratSupport();
    }

    /**
     * Whether is User Load.
     * @return True if is User Load.
     */
    public static final boolean isUserLoad() {
        return SystemProperties.get(BUILD_TYPE).equals(BUILD_TYPE_USER);
    }

    // Important!!! the SystemProperties key's length must less than 31 , or will have JE
    /* Whether is support the key's value */
    private static final boolean isSupport(String key) {
        return SystemProperties.get(key).equals(SUPPORT_YES);
    }
}
