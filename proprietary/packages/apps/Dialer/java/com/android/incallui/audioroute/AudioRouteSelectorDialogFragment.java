/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.incallui.audioroute;

import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff.Mode;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.telecom.CallAudioState;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import com.android.dialer.common.FragmentUtils;
import com.android.dialer.common.LogUtil;

import java.util.Collection;

/** Shows picker for audio routes */
public class AudioRouteSelectorDialogFragment extends BottomSheetDialogFragment {

  public static final String TAG = "AudioRouteSelectorDialogFragment";
  private static final String ARG_AUDIO_STATE = "audio_state";

  /** Called when an audio route is picked */
  public interface AudioRouteSelectorPresenter {
    void onAudioRouteSelected(int audioRoute);

    void onAudioRouteSelectorDismiss();

    /// M: ALPS03950201. Add audio route of bluetooth device. @{
    void onBluetoothDeviceSelected(BluetoothDevice device);
    /// @}
  }

  public static AudioRouteSelectorDialogFragment newInstance(CallAudioState audioState) {
    AudioRouteSelectorDialogFragment fragment = new AudioRouteSelectorDialogFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARG_AUDIO_STATE, audioState);
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    FragmentUtils.checkParent(this, AudioRouteSelectorPresenter.class);
  }

  @Override
  public Dialog onCreateDialog(final Bundle savedInstanceState) {
    LogUtil.i("AudioRouteSelectorDialogFragment.onCreateDialog", null);
    Dialog dialog = super.onCreateDialog(savedInstanceState);
    dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
    return dialog;
  }

  @Nullable
  @Override
  public View onCreateView(
      LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
    View view = layoutInflater.inflate(R.layout.audioroute_selector, viewGroup, false);
    CallAudioState audioState = getArguments().getParcelable(ARG_AUDIO_STATE);

    /// M: ALPS03950201 Use bluetooth device to init items. @{
    /// Google code:
    /// initItem(
    ///     (TextView) view.findViewById(R.id.audioroute_bluetooth),
    ///     CallAudioState.ROUTE_BLUETOOTH,
    ///     audioState);
    if (!initBluetoothDevice(view, CallAudioState.ROUTE_BLUETOOTH, audioState)) {
      initItem(
          (TextView) view.findViewById(R.id.audioroute_bluetooth),
          CallAudioState.ROUTE_BLUETOOTH,
          audioState);
    }
    /// @}

    initItem(
        (TextView) view.findViewById(R.id.audioroute_speaker),
        CallAudioState.ROUTE_SPEAKER,
        audioState);
    initItem(
        (TextView) view.findViewById(R.id.audioroute_headset),
        CallAudioState.ROUTE_WIRED_HEADSET,
        audioState);
    initItem(
        (TextView) view.findViewById(R.id.audioroute_earpiece),
        CallAudioState.ROUTE_EARPIECE,
        audioState);

    // TODO(a bug): set peak height correctly to fully expand it in landscape mode.
    return view;
  }

  @Override
  public void onCancel(DialogInterface dialogInterface) {
    super.onCancel(dialogInterface);
    FragmentUtils.getParentUnsafe(
            AudioRouteSelectorDialogFragment.this, AudioRouteSelectorPresenter.class)
        .onAudioRouteSelectorDismiss();
  }

  private void initItem(TextView item, final int itemRoute, CallAudioState audioState) {
    int selectedColor = getResources().getColor(R.color.dialer_theme_color);
    if ((audioState.getSupportedRouteMask() & itemRoute) == 0) {
      item.setVisibility(View.GONE);
    } else if (audioState.getRoute() == itemRoute) {
      item.setTextColor(selectedColor);
      item.setCompoundDrawableTintList(ColorStateList.valueOf(selectedColor));
      item.setCompoundDrawableTintMode(Mode.SRC_ATOP);
    }
    item.setOnClickListener(
        (v) -> {
          dismiss();
          FragmentUtils.getParentUnsafe(
                  AudioRouteSelectorDialogFragment.this, AudioRouteSelectorPresenter.class)
              .onAudioRouteSelected(itemRoute);
        });
  }

  /// M: -------------- Mediatek features-----------------------------------------------------

  /// M: ALPS03950201 Add audio route of bluetooth device. @{
  private static final int MAX_DEVICE_COUNT = 5;
  /// @}

  /**
   * M: ALPS03950201 Add bluetooth device support for audio route.
   * @param parentView ViewGroup
   * @param route  the audio route
   * @param audioState CallAudioState
   * @return result of init bluetooth
   */
  private boolean initBluetoothDevice(View parentView, int route, CallAudioState audioState) {
    boolean result = false;
    boolean highlight = false;
    int index = 0;

    Collection<BluetoothDevice> list = audioState.getSupportedBluetoothDevices();

    TextView[] viewList = {
      (TextView) parentView.findViewById(R.id.bluetooth_device_one),
      (TextView) parentView.findViewById(R.id.bluetooth_device_two),
      (TextView) parentView.findViewById(R.id.bluetooth_device_three),
      (TextView) parentView.findViewById(R.id.bluetooth_device_four),
      (TextView) parentView.findViewById(R.id.bluetooth_device_five)};

    BluetoothDevice activeDevice = audioState.getActiveBluetoothDevice();
    for (BluetoothDevice device : list) {
      if (index >= MAX_DEVICE_COUNT) {
        LogUtil.d("AudioRouteSelectorDialogFragment.initBluetoothDevice", "Got max devices.");
        break;
      }

      if (activeDevice != null
          && device != null
          && device.equals(activeDevice)
          && route == audioState.getRoute()) {
        highlight = true;
      } else {
        highlight = false;
      }

      result = result | addBluetoothItem(viewList[index], device, highlight);
      index++;
    }

    for (int i = index; i < MAX_DEVICE_COUNT; i++) {
      viewList[i].setVisibility(View.GONE);
    }

    if (result) {
      TextView textView = (TextView) parentView.findViewById(R.id.audioroute_bluetooth);
      textView.setVisibility(View.GONE);
    }
    return result;
  }

  /**
   * M: ALPS03950201 Add bluetooth device support for audio route.
   * @param textView TextView
   * @param device  the BluetoothDevice
   * @param highlight should show text color
   * @return result of add bluetooth device
   */
  private boolean addBluetoothItem(TextView textView,
      final BluetoothDevice device, boolean highlight) {
    if (device == null) {
      LogUtil.d("AudioRouteSelectorDialogFragment.addBluetoothItem", "Device is null");
      textView.setVisibility(View.GONE);
      return false;
    }

    String bluetoothName = device.getAliasName();
    LogUtil.d("AudioRouteSelectorDialogFragment.addBluetoothItem", "Name = " + bluetoothName);

    textView.setText(bluetoothName);
    textView.setSingleLine(true);
    textView.setEllipsize(TextUtils.TruncateAt.valueOf("END"));

    if (highlight) {
      int selectedColor = getResources().getColor(R.color.dialer_theme_color);
      textView.setTextColor(selectedColor);
      textView.setCompoundDrawableTintList(ColorStateList.valueOf(selectedColor));
      textView.setCompoundDrawableTintMode(Mode.SRC_ATOP);
    }

    textView.setOnClickListener(
        (v) -> {
          dismiss();
          FragmentUtils.getParentUnsafe(
                  AudioRouteSelectorDialogFragment.this, AudioRouteSelectorPresenter.class)
              .onBluetoothDeviceSelected(device);
        });

    return true;
  }
}
