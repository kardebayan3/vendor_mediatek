/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2011. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.incallui.frameworks;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureLayer;

/**
 * TextureView.
 * ALPS04004170, avoid local buffer size to be modified by TextureView
 * android.view.TextureView and TextureLayer need to be exported.
 */
public class TextureView extends android.view.TextureView {

  private int mBufWidth = -1;
  private int mBufHight = -1;

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   */
  public TextureView(Context context) {
      super(context);
  }

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   * @param attrs The attributes of the XML tag that is inflating the view.
   */
  public TextureView(Context context, AttributeSet attrs) {
      super(context, attrs);
  }

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a
   *        reference to a style resource that supplies default values for
   *        the view. Can be 0 to not look for defaults.
   */
  public TextureView(Context context, AttributeSet attrs, int defStyleAttr) {
      super(context, attrs, defStyleAttr);
  }

  /**
   * Creates a new TextureView.
   *
   * @param context The context to associate this view with.
   * @param attrs The attributes of the XML tag that is inflating the view.
   * @param defStyleAttr An attribute in the current theme that contains a
   *        reference to a style resource that supplies default values for
   *        the view. Can be 0 to not look for defaults.
   * @param defStyleRes A resource identifier of a style resource that
   *        supplies default values for the view, used only if
   *        defStyleAttr is 0 or can not be found in the theme. Can be 0
   *        to not look for defaults.
   */
  public TextureView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
      super(context, attrs, defStyleAttr, defStyleRes);
  }

  protected TextureLayer getTextureLayer() {
    TextureLayer layer = super.getTextureLayer();
    if (mBufWidth > 0 && mBufHight > 0) {
      getSurfaceTexture().setDefaultBufferSize(mBufWidth, mBufHight);
      //check log: BufferQueueConsumer...setDefaultBUfferSize
    }
    return layer;
  }

  /**
   * setSurfaceBufferSize.
   * @param width the width of surface buffer
   * @param height the height of surface buffer
   */
  public void setSurfaceBufferSize(int width, int height) {
    mBufWidth = width;
    mBufHight = height;
  }
}
