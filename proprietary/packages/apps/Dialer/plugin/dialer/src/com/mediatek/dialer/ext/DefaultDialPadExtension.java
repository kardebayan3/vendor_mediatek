/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.dialer.ext;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import java.util.List;

public class DefaultDialPadExtension implements IDialPadExtension {

    private static final String TAG = "DefaultDialPadExtension";

    @Override
    public void onCreate(Activity activity, Bundle bundle) {
        log("onCreate");
    }

    @Override
    public void onDestroy(Activity activity) {
        log("onDestroy");
    }

    @Override
    public void onCreate(Context context, Fragment fragment, DialpadExtensionAction action) {
        log("onCreate");
    }

    @Override
    public boolean handleChars(Context context, String input) {
        log("handleChars");
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState,
            View resultView) {
        log("onCreateView");
        return resultView;
    }

    @Override
    public void onDestroy() {
        log("onDestroy");
    }

    @Override
    public void onHiddenChanged(boolean scaleIn, int delayMs) {
        log("onHiddenChanged");
    }

    @Override
    public void showDialpadChooser(boolean enabled) {
        log("showDialpadChooser");
    }

    @Override
    public String handleSecretCode(String input) {
        log("handleSecretCode");
        return input;
    }

    @Override
    public void constructPopupMenu(PopupMenu popupMenu, View anchorView, Menu menu) {
        log("constructPopupMenu");
    }

    @Override
    public void buildOptionsMenu(Activity activity, Menu menu) {
        log("buildOptionsMenu");
    }

    @Override
    public void onViewCreated(Activity activity, View view) {
        log("onViewCreated");
    }

    @Override
    public List<String> getSingleIMEI(List<String> list) {
        log("getSingleIMEI");
        return list;
    }

    @Override
    public boolean isVideoButtonEnabled(boolean hostVideoEnabled, Object...params) {
        return hostVideoEnabled;
    }

    @Override
    public boolean checkVideoSetting(Context appContext, Intent intent) {
        log("checkVideoSetting");
        return false;
    }

    @Override
     public void customizeDefaultTAB(Object object) {
         log("customizeDefaultTAB");
    }

    private void log(String msg) {
        Log.d(TAG, msg + " default");
    }

    @Override
    public void onSetQueryString(String number) {

    }

    @Override
    public void customizeDialerOptions(View view, int type, String number) {
        log("customizeDialerOptions");
    }

    @Override
    public void customizeContactListItemView(View view) {
        Log.d(TAG, "customizeContactListItemView");
    }

    @Override
    public boolean enableVideoButton() {
        log("enableVideoButton");
        return false;
    }

    @Override
    public boolean isSupportVideoCallIcon(boolean support) {
        return support;
    }

    @Override
    public boolean handleDeviceIdDisplay(
            ViewGroup viewGroup, boolean showDecimal, boolean showBarcode) {
        return false;
    }
}
