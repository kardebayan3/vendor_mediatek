/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.dialer.ext;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.telecom.PhoneAccountHandle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class DefaultCallLogExtension implements ICallLogExtension {
    private static final String TAG = "DefaultCallLogExtension";

    @Override
    public void appendQuerySelection(int typeFiler, StringBuilder builder,
            List<String> selectionArgs) {
        log("appendQuerySelection");
    }

    @Override
    public void createCallLogMenu(Activity activity, Menu menu, HorizontalScrollView tabs,
            ICallLogAction callLogAction) {
        log("createCallLogMenu");
    }

    @Override
    public void prepareCallLogMenu(Activity activity, Menu menu,
            Fragment fragment, MenuItem itemDeleteAll, int adapterCount) {
        log("prepareCallLogMenu");
    }

    @Override
    public void onSaveInstanceState(Activity activity, Bundle outState) {
        log("onSaveInstanceState");
    }
    private void log(String msg) {
//        Log.d(TAG, msg + " default");
    }

    @Override
    public void onDestroy(Activity activity) {
        //default do nothing
    }

    @Override
    public void onDestroy(Fragment fragment) {
        //default do nothing
    }

    @Override
    public void onCreate(Activity activity, Bundle bundle) {
        //default do nothing
    }

    @Override
    public void onCreate(Fragment fragment, Bundle bundle) {
        //default do nothing
    }

    @Override
    public void addResourceForDialerSearch(HashMap<Integer, Drawable>
            callTypeDrawable, boolean useLargeIcons, Context context) {
        //default do nothing
    }

    @Override
    public void drawWifiVolteCallIcon(int scaledHeight) {
        log("drawWifiVolteCallIcon");
    }

    @Override
    public void drawWifiVolteCanvas(int left, Canvas canvas,
                     Object callTypeIconViewObj) {
        log("drawWifiVolteCanvas");
    }

    @Override
    public void setShowVolteWifi(Object object, int features) {
        log("setShowVolteWifi");
    }


    @Override
    public boolean sameCallFeature(Cursor cursor) {
        log("sameCallFeature");
        return true;
    }

    @Override
    public void customizeBindActionButtons(Object object) {
        log("customizeBindActionButtons");
    }

    @Override
    public void onExpandViewHolderActions(String number) {

    }

    @Override
    public void showActions(Object obj, boolean show) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //default do nothing
    }

    @Override
    public void updateNotice(Fragment fragment) {
        //default do nothing
    }


    @Override
    public void onViewCreated(Fragment fragment, View view) {
        //default do nothing
    }

   @Override
   public int getTabIndexCount() {
       Log.d(TAG, "getTabIndexCount");
       return 2;
   }

    @Override
    public void initCallLogTab(CharSequence[] tabTitles, ViewPager viewPager) {
        Log.d(TAG, "initCallLogTab");
    }

    @Override
    public void setCurrentCallLogFragment(Activity activity, int position,
                          Fragment fragment) {
        Log.d(TAG, "setCurrentCallLogFragment");
    }

    @Override
    public Fragment getCallLogFragmentItem(int position) {
        Log.d(TAG, "getCallLogItem");
        return null;
    }

    @Override
    public Object instantiateCallLogFragmentItem(Activity activity, int position,
                               Fragment fragment) {
        Log.d(TAG, "instantiateCallLogItem");
        return null;
    }

    @Override
    public void restoreFragments(Bundle bundle, Context context,
        FragmentPagerAdapter pagerAdapter, HorizontalScrollView tabs) {
        log("restoreFragments");
    }

    @Override
    public Fragment getCurrentFragment(int pos) {
        return null;
    }

    @Override
    public int getCurrentCallLogFilteType(int pos) {
        return -1;
    }

    @Override
    public void onResume(Activity activity) {
        //default do nothing
    }

    @Override
    public boolean updateMissedCalls(int index) {
        return false;
    }

    @Override
    public boolean updateEmptyMessage(Fragment fragment, int filter) {
        return false;
    }

    @Override
    public void onAttachedToWindow(Fragment fragment, Object obj) {
        //default do nothing
    }

    @Override
    public void onDetachedFromWindow(Object obj) {
        //default do nothing
    }

    /**
     * Plug-in get call type icon.
     * @param callType int
     * @param res Object
     * @return return the call type icon or null
     */
    @Override
    public Drawable getCallTypeDrawable(int callType, Object res) {
        Log.d(TAG, "getCallTypeDrawable");
        return null;
    }

   /**
     * for op01.
     * whether need pre show primary action view or not. op01 need update callback button,
     * there will be flickering phenomena if callback button pre show.
     */
    @Override
    public boolean isNeedPreShowPrimaryUi() {
        return true;
    }
}
