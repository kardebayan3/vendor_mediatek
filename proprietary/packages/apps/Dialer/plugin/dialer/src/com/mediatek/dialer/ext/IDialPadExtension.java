/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.dialer.ext;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import java.util.List;

public interface IDialPadExtension {

    /**
     * for OP01.
     * called when DialtactsActivity onCreate, plug-in should reference the activity
     * @param activity Activity
     * @param bundle Bundle
     */
    void onCreate(Activity activity, Bundle bundle);

    /**
     * for OP01.
     * called when DialtactsActivity onDestroy, plug-in should clear the activity
     * @param activity Activity
     */
    void onDestroy(Activity activity);

    /**
     * for OP09.
     * called when DialPadFragment onCreate, plug-in should do init work here if needed
     * @param context Context
     * @param fragment dialpadFragment
     * @param action the actions host provided
     * @internal
     */
    void onCreate(Context context, Fragment fragment, DialpadExtensionAction action);

    /**
     * for OP09.
     * called when handle special chars, plug-in should do customize chars handling here
     * @param context Context
     * @param input the input digits (text)
     * @return whether plug-in handled the chars
     * @internal
     */
    boolean handleChars(Context context, String input);

    /**
     * for OP09.
     * called when dialpad create view, plug-in should customize view here
     * @param inflater the host inflater
     * @param container parent ViewGroup
     * @param savedState previous state saved for create view
     * @param resultView the host view created
     * @return customized view by plug-in
     * @internal
     */
    View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState,
            View resultView);

    /**
     * for OP09.
     * called when dialpad destroy
     * @internal
     */
    void onDestroy();

    /**
     * for OP09.
     * the dialpad hidden changed
     * @param scaleIn If true, scaleIn If false
     * @param delayMs delay time
     * @internal
     */
    void onHiddenChanged(boolean scaleIn, int delayMs);

    /**
     * for OP09.
     * called when to show dialpadChooser
     * @param enabled true to be enabled
     * @internal
     */
    void showDialpadChooser(boolean enabled);

    /**
     * for OP03 OP09.
     * called when dialpad fragment construct popup menu, plug-in should construct own menu here
     * @param popupMenu the created popupMenu object
     * @param anchorView the anchor for the menu to popup
     * @param menu the menu item of the popupmenu
     * @internal
     */
    void constructPopupMenu(PopupMenu popupMenu, View anchorView, Menu menu);

    /**
     * for op09.
     * called when handle handle secret code in dialpad, plug-in should customize the input string
     * @param input the string to be handled
     * @return The result of the "input" be handled.
     * @internal
     */
    String handleSecretCode(String input);

    /**
     * for OP01.
     * called when init option menu
     * @param activity the activity who call this func
     * @param menu the activity option menu
     * @internal
     */
    void buildOptionsMenu(Activity activity, Menu menu);

    /**
     * for OP01.
     * called onViewCreated
     * @param activity the activity who call this func
     * @param view the dialpad view
     * @internal
     */
    void onViewCreated(Activity activity, View view);

    /**
     * for OP01.
     * called when user input *#06#, single IMEI meaning
     * gemini project will only show one imei
     * @param list List<String>, the IMEI input
     * @internal
     */
    List<String> getSingleIMEI(List<String> list);

    /** Define all the parameters order that pass to plugin. */
    public static final int VIDEO_BUTTON_PARAMS_INDEX_QUERY_STRING = 0;
    public static final int VIDEO_BUTTON_PARAMS_INDEX_CONTACT_LOOKUP_URI = 1;

    /**
     * For all plug-in to override the video button visibility on Dialpad.
     * @param hostVideoEnabled boolean
     * @param params Object
     * @return AOSP value for default, customization for operators
     */
    public boolean isVideoButtonEnabled(boolean hostVideoEnabled, Object...params);

   /**
     * for OP07.
     *@param appContext Context
     *@param intent Intent
     * @return false or true
     */
    boolean checkVideoSetting(Context appContext, Intent intent);

   /**
     * for OP18.
     *@param object Object
     */
    void customizeDefaultTAB(Object object);

    /**
     * Request capability when input number in Dialer.
     * @param number the query number
     */
    void onSetQueryString(String number);

    /**
     * for OP01.
     * Called when create expand view in calllist item
     * @param view the host item view
     * @param type the view type
     * @param number the call number
     */
    void customizeDialerOptions(View view, int type, String number);

   /**
    * for OP12.
    *@param view View
    */
    void customizeContactListItemView(View view);

   /**
    * for OP07 for OP12.
    * @return false
    */
    boolean enableVideoButton();

    /**
     * for OP01.
     * Called when create contact list item view
     * @param support support video call icon
     * @return if support make video icon
     */
    boolean isSupportVideoCallIcon(boolean support);

    /**
     * for OP01.
     * Called when create device info list item view
     * @param viewGroup the view holder
     * @param showDecimal if need to show Decimal
     * @param showBarcode if need to show Barcode
     * @return if handled in plugin
     */
    public boolean handleDeviceIdDisplay(
            ViewGroup viewGroup, boolean showDecimal, boolean showBarcode);
}
