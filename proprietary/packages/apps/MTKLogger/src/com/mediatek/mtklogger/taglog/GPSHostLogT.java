package com.mediatek.mtklogger.taglog;

import com.mediatek.mtklogger.controller.LogControllerUtils;
import com.mediatek.mtklogger.framework.MTKLoggerServiceManager;
import com.mediatek.mtklogger.framework.MTKLoggerServiceManager.ServiceNullException;
import com.mediatek.mtklogger.utils.Utils;

/**
 * @author MTK81255
 *
 */
public class GPSHostLogT extends LogInstanceForTaglog {

    /**
     * @param logType int
     * @param tagLogInformation TagLogInformation
     */
    public GPSHostLogT(int logType, TagLogInformation tagLogInformation) {
        super(logType, tagLogInformation);
    }

    @Override
    public boolean isNeedDoTag() {

        if (!LogControllerUtils.isTypeLogRunning(mLogType)) {
            Utils.logi(TAG, "GPS Log is not running, no need do tag.");
            Utils.logd(TAG, "isNeedDoTag ? false. mLogType = " + mLogType);
            return false;
        }
        return super.isNeedDoTag();
    }

}