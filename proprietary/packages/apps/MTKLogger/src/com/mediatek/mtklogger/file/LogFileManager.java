package com.mediatek.mtklogger.file;

import com.mediatek.mtklogger.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author MTK81255
 *
 */
public class LogFileManager {
    private static final String TAG = Utils.TAG + "/LogFileManager";

    /**
     * @param file
     *            File
     * @return boolean
     * @throws IOException
     *             ioException
     */
    public static boolean createNewFile(File file) throws IOException {
        if (isInExternalStorage(file)) {
            return ExternalStorageFileManager.createNewFile(file);
        }
        try {
            return file.createNewFile();
        } catch (IOException e) {
            Utils.logw(TAG, "file.createNewFile() error!");
            return false;
        }
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean delete(File file) {
        if (isInExternalStorage(file)) {
            return ExternalStorageFileManager.delete(file);
        }
        return file.delete();
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean mkdir(File file) {
        if (isInExternalStorage(file)) {
            return ExternalStorageFileManager.mkdir(file);
        }
        return file.mkdir();
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean mkdirs(File file) {
        if (isInExternalStorage(file)) {
            return ExternalStorageFileManager.mkdirs(file);
        }
        return file.mkdirs();
    }

    /**
     * @param file
     *            File
     * @param dest
     *            File
     * @return boolean
     */
    public static boolean renameTo(File file, File dest) {
        if (isInExternalStorage(file)) {
            if (file.getParentFile().getAbsoluteFile()
                    .equals(dest.getParentFile().getAbsoluteFile())) {
                return ExternalStorageFileManager.rename(file, dest);
            } else {
                return ExternalStorageFileManager.move(file, dest);
            }
        }
        return file.renameTo(dest);
    }

    /**
     * @param file
     *            File
     * @return FileOutputStream
     */
    public static FileOutputStream getFileOutputStream(File file) {
        if (isInExternalStorage(file)) {
            return ExternalStorageFileManager.getFileOutputStream(file);
        }
        try {
            return new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            Utils.logw(TAG, "getFileOutputStream() error!");
        }
        return null;
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean isInExternalStorage(File file) {
        return false;
        // String externalStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        // boolean isInExternalStorage = Environment.isExternalStorageRemovable(file);
        // Utils.logi(TAG, "isInExternalStorage(), file.getAbsolutePath() = " +
        // file.getAbsolutePath()
        // + ", externalStoragePath = " + externalStoragePath + ", isInExternalStorage = "
        // + isInExternalStorage);
        // return isInExternalStorage;
    }

}
