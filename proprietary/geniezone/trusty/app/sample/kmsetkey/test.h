/*
 * Copyright (C) 2016 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#ifndef __TEST_H__
#define __TEST_H__

int32_t generate_rsa_key_test(void);
int32_t generate_rsa_signature_test(void);
int32_t verify_rsa_signature_test(void);
int32_t generate_ecc_key_test(void);
int32_t generate_ecc_signature_test(void);
int32_t verify_ecc_signature_test(void);
int32_t perform_aes_128_enc_test(void);
int32_t perform_aes_128_dec_test(void);
int32_t perform_aes_256_enc_test(void);
int32_t perform_aes_256_dec_test(void);
int32_t perform_sha_hash_test(void);

#endif
