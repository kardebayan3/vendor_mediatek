package mtkCc

import (
	"android/soong/android"
	"android/soong/cc"
	"android/soong/android/mediatek"
)

func init() {
	android.RegisterModuleType("mtk_cc_library", libraryFactory)
	android.RegisterModuleType("mtk_cc_library_static", libraryStaticFactory)
	android.RegisterModuleType("mtk_cc_library_shared", librarySharedFactory)
}

func libraryFactory() android.Module {
	m, _ := cc.NewLibrary(android.HostAndDeviceSupported)
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func libraryStaticFactory() android.Module {
	m, library := cc.NewLibrary(android.HostAndDeviceSupported)
	library.BuildOnlyStatic()
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}

func librarySharedFactory() android.Module {
	m, library := cc.NewLibrary(android.HostAndDeviceSupported)
	library.BuildOnlyShared()
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}
