package mtkGenrule

import (
	"os"
	"path/filepath"

	"android/soong/android"
)

type mtkDumpSingleton struct{}

func init() {
	android.RegisterSingletonType("mtkdump", MtkDumpSingleton)
}

func MtkDumpSingleton() android.Singleton {
	return &mtkDumpSingleton{}
}

func (c *mtkDumpSingleton) GenerateBuildActions(ctx android.SingletonContext) {
	config := ctx.Config()
	if !config.EmbeddedInMake() {
		return
	}
	variablesFileName := filepath.Join(config.BuildDir(), "mtk_soong.config")
	if _, err := os.Stat(variablesFileName); err == nil {
		ctx.AddNinjaFileDeps(variablesFileName)
	}
}
