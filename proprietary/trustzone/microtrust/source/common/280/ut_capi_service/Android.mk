LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := UtClientApiService
LOCAL_MODULE_TAGS := optional
LOCAL_CERTIFICATE := platform
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)

LOCAL_SRC_FILES := ut_capi_service.apk
#LOCAL_PROGUARD_ENABLED := disabled
LOCAL_PROGUARD_FLAG_FILES := proguard.cfg
LOCAL_PROPRIETARY_MODULE := true

include $(BUILD_PREBUILT)


#----------------------------------------------------------------------------
include $(CLEAR_VARS)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE := lib_ut_capi_adapter
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
LOCAL_STATIC_JAVA_LIBRARIES := lib_ut_capi_adapter_classes
LOCAL_PROPRIETARY_MODULE := true
include $(BUILD_JAVA_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := lib_ut_capi_adapter_classes:lib_ut_capi_adapter.jar
include $(BUILD_MULTI_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE = com.beanpod.capi.xml
LOCAL_MODULE_CLASS = ETC
LOCAL_MODULE_PATH = $(TARGET_OUT_VENDOR)/etc/permissions
LOCAL_PROPRIETARY_MODULE = true
LOCAL_MODULE_TAGS = optional
LOCAL_SRC_FILES = $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

#----------------------------------------------------------------------------
include $(CLEAR_VARS)
LOCAL_MODULE := libcapi_adapter
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES_64 := libs/arm64-v8a/libcapi_adapter.so
LOCAL_SRC_FILES_32 := libs/armeabi-v7a/libcapi_adapter.so
LOCAL_MULTILIB := both
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
PLATFORM_VERSION_MAJOR := $(word 1,$(subst .,$(space),$(PLATFORM_VERSION)))
ifneq ($(PLATFORM_VERSION_MAJOR), 6)
LOCAL_PROPRIETARY_MODULE := true
endif
include $(BUILD_PREBUILT)
