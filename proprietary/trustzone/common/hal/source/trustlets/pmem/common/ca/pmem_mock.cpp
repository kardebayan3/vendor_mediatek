/*
 * Copyright (c) 2018 MediaTek Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define LOG_TAG "PMEM_GZ"
/* LOG_TAG need to be defined before include log.h */
#include <android/log.h>

#include "pmem_dbg.h"
#include "pmem_share.h"

extern int tee_mock_write(int mem_handle, int mem_size, unsigned int pattern,
                          bool check_zero);
extern int tee_mock_read(int mem_handle, int mem_size, unsigned int pattern);

int gz_open()
{
    return PMEM_SUCCESS;
}

int gz_close()
{
    return PMEM_SUCCESS;
}

int gz_write(int mem_handle, int mem_size, unsigned int pattern, bool check_zero)
{
    return tee_mock_write(mem_handle, mem_size, pattern, check_zero);
}

int gz_read(int mem_handle, int mem_size, unsigned int pattern)
{
    return tee_mock_read(mem_handle, mem_size, pattern);
}
