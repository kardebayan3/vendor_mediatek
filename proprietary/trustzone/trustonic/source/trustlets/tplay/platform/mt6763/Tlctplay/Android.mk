# =============================================================================
#
# Makefile responsible for:
# - building a test binary - tlctplay
# - building the TLC library - libtlctplayLib.so
#
# =============================================================================
$(info <t-play TRUSTONIC_TEE_SUPPORT=$(TRUSTONIC_TEE_SUPPORT)) 

ifeq ($(TRUSTONIC_TEE_SUPPORT),yes)

# Do not remove this - Android build needs the definition
LOCAL_PATH	:= $(call my-dir)

ifeq ($(MTK_K64_SUPPORT), yes)
MOBICORE_LIB_PATH +=\
    $(LOCAL_PATH)/../../../../../source/external/mobicore/common/302c/MobiCoreDriverLib/ClientLib/public \
    $(LOCAL_PATH)/../../../../../source/external/mobicore/common/302c/common/MobiCore/inc
else
MOBICORE_LIB_PATH +=\
    $(LOCAL_PATH)/../../../mobicore/MobiCoreDriverLib/ClientLib/public \
    $(LOCAL_PATH)/../../../mobicore/common/MobiCore/inc
endif

TLTPLAY_HEADER_PATH +=\
    $(LOCAL_PATH)/../../../Tltplay/Locals/Code/public

# =============================================================================
# library tlctplayLib

include $(CLEAR_VARS)
LOCAL_MODULE := libtplay
# Output to system/vendor/lib/libtplay.so and system/vendor/lib64/libtplay.so
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_CLASS = SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX = .so
LOCAL_SRC_FILES_32 := lib/libtplay.so
LOCAL_SRC_FILES_64 := lib64/libtplay.so
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)


# =============================================================================
# binary tlctplay
ifeq ($(strip $(TARGET_BUILD_VARIANT)), eng)
include $(CLEAR_VARS)

# Module name
LOCAL_MODULE	:= tlctplay
LOCAL_MODULE_TAGS := optional
# Output to system/vendor/bin/tlctplay
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \
    $(TLTPLAY_HEADER_PATH) \
    $(MOBICORE_LIB_PATH)

# Add your source files here
LOCAL_SRC_FILES	+= main.cpp

LOCAL_SHARED_LIBRARIES := libMcClient
#LOCAL_SHARED_LIBRARIES += tlctplayLib
LOCAL_SHARED_LIBRARIES += libtplay
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_EXECUTABLE)
endif

# =============================================================================

endif
