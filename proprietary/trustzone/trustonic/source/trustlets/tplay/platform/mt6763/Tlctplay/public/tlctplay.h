 /*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef __TLC_TPLAY_H__
#define __TLC_TPLAY_H__

#include "MobiCoreDriverApi.h"

mcResult_t tlcOpen(void);

//mcResult_t getDevinfo(uint32_t *index, uint32_t *result);

mcResult_t testTplay(uint32_t *result);
mcResult_t setTplayHandleAddr(uint32_t *result, uint32_t high_addr, uint32_t low_addr);

void tlcClose(void);

#endif // __TLC_TPLAY_H__
