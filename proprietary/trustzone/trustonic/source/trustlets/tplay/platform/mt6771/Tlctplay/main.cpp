 /*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#include <stdlib.h>

#include "tlctplay.h"

#define LOG_TAG "TLC TPLAY"
#include "tlctplay_log.h"

int main(int argc, char *argv[])
{

    mcResult_t ret;
    unsigned int index =20;
    unsigned int result = 0;

    LOG_I("Copyright ?Trustonic Limited 2016");
	LOG_I("");
	LOG_I("TPLAY TLC called");

    ret = tlcOpen();

    if (MC_DRV_OK != ret)
    {
        printf("open TL session failed!");
        return ret;
    }
    // ret = getDevinfo(&index,&result);

    // Set <t-play Handled Address
    ret = setTplayHandleAddr(&result, 0x0, 0x5555);
    LOG_I("<t-play Set Handle Address result is %d", result);
    printf("<t-play Set Handle Address result is %d\n", result);

    // Verify <t-play
    ret = testTplay(&result);
    LOG_I("<t-play verification result is %d", result);
    printf("<t-play verification result is %d\n", result);

    // while(1);

exit:
    tlcClose();
    return ret;
}
