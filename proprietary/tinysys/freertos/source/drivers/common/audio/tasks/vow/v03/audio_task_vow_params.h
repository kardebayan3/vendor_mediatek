/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*/
/* MediaTek Inc. (C) 2015. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*/
#ifndef AUDIO_TASK_VOW_PARAMS_H
#define AUDIO_TASK_VOW_PARAMS_H


/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Header Files
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
#include "vow_hw_reg.h"
/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Options
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

#define VOW_HAS_SWIP (1)
#define TIMES_DEBUG (0)


#define __VOW_GOOGLE_SUPPORT__              (0)
#define __VOW_MTK_SUPPORT__                 (1)
#define SMART_AAD_MIC_SUPPORT               (0)


#define CHECK_RECOG_TIME

#if PMIC_6358_SUPPORT
#define BLISRC_USE
#endif  // #if PMIC_6358_SUPPORT
#ifndef DRV_WriteReg32
#define DRV_WriteReg32(addr,data)   ((*(volatile unsigned int *)(addr)) = (unsigned int)data)
#endif
#ifndef DRV_Reg32
#define DRV_Reg32(addr)             (*(volatile unsigned int *)(addr))
#endif

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Definitions
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
// #define LOCAL_TASK_STACK_SIZE (384)
#define LOCAL_TASK_STACK_SIZE (768)
#define LOCAL_TASK_NAME "vow"

#define MAX_MSG_QUEUE_SIZE (20)


#ifndef false
#define false   0
#endif

#ifndef true
#define true    1
#endif

#ifndef NULL
#define NULL    0
#endif

#ifndef bool
typedef unsigned char  bool;
#endif

// #define VOW_MAX_SPEAKER_MODEL             0xA
#define VOW_LENGTH_INIT                   (0x2)  // 0x3800
#define VOW_VOICEDATA_OFFSET              (0xDC00) //UBM_V1: 0xA000,UBM_V2: 0xDC00
#if (__VOW_GOOGLE_SUPPORT__)
#define VOW_LENGTH_SPEAKER                (0x5000)  // 0x9E54 / 2 = 4F2A
#else
#define VOW_LENGTH_SPEAKER                (0x56C0) //UBM_V1: 0x3AC0,UBM_V2: 0x56C0
#endif

#define SEAMLESS_RECORD_LENGTH            (0x7D00)
#if defined(CFG_MTK_VOW_SEAMLESS_SUPPORT) && !defined(BLISRC_USE)
#define VOW_LENGTH_VOICE                  (SEAMLESS_RECORD_LENGTH)  // 0x7D00 => 32000 / 16kSampleRate = 2sec
#else  // #if defined(CFG_MTK_VOW_SEAMLESS_SUPPORT) && !defined(BLISRC_USE)
#ifdef BLISRC_USE
#define VOW_LENGTH_VOICE                  (0x2A8)  // 0x1FE => 510 / 16.93kSampleRate = 30.124msec, 3*170=510
#else  // #ifdef BLISRC_USE
#define VOW_LENGTH_VOICE                  (0x280)  // 0x280 => 640 / 16kSampleRate = 40msec, 4*160=640
#endif  // #ifdef BLISRC_USE
#endif  // #if defined(CFG_MTK_VOW_SEAMLESS_SUPPORT) && !defined(BLISRC_USE)

#ifdef CFG_MTK_VOW_SEAMLESS_SUPPORT
#define SEAMLESS_RECORD_BUF_SIZE            (0x7D00)
#endif // #ifdef CFG_MTK_VOW_SEAMLESS_SUPPORT

#define VOW_DROP_FRAME_FOR_DC             (0x1E)  // wait for dc stable in 30(0x1E) * 10msec = 300msec

// In MT6758, Sampling rate is 16.93kHz, we choose 10msec sample(170 word) to be FIFO IRQ threshold
#ifdef BLISRC_USE
#define VOW_SAMPLE_NUM_IRQ                (0xAA)
#else  // #ifdef BLISRC_USE
#define VOW_SAMPLE_NUM_IRQ                (0xA0)
#endif  // #ifdef BLISRC_USE

#define VOW_SAMPLE_NUM_FRAME              (160)

#define VOW_RESULT_NOTHING                (0x0)
#define VOW_RESULT_PASS                   (0x1)
#define VOW_RESULT_FAIL                   (0x2)
#define VOW_RESULT_CHANGE_MODEL           (0x3)

// After receiving SEAMLESS_RECORD flag, let accumulate 20msec
// then start send voice after receiving SEAMLESS_RECORD flag
#define SEAMLESS_DELAY                    (0)//(2)

#define PCMDUMP_BUF_CNT                   (16)
#define WORD_H                            (8)
#define WORD_L                            (0)

/* For BLISRC */
#ifdef BLISRC_USE
#define BLISRC_INTERNAL_BUFFER_WORD_SIZE            (528)
#define BLISRC_ACCURATE_INTERNAL_BUFFER_WORD_SIZE   (529)
#define BLISRC_INPUT_BUFFER_SIZE_IN_HALF_WORD       (170)
#define BLISRC_OUTPUT_BUFFER_SIZE_IN_HALF_WORD      (160)  // 10msec voice data
#define BLISRC_INPUT_SAMPLING_RATE_IN_HZ            (18000)
#define BLISRC_OUTPUT_SAMPLING_RATE_IN_HZ           (17000)
#define BLISRC_ACCURATE_INPUT_SAMPLING_RATE_IN_HZ   (1625)
#define BLISRC_ACCURATE_OUTPUT_SAMPLING_RATE_IN_HZ  (1536)
#define BLISRC_INPUT_CHANNEL_NUBMER                 (1)
#define BLISRC_OUTPUT_CHANNEL_NUBMER                (1)
#define BLISRC_OUTPUT_TEMP_BUFFER               (BLISRC_OUTPUT_BUFFER_SIZE_IN_HALF_WORD * 2)

#endif  // #ifdef BLISRC_USE
/* For BARGE IN*/
#ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
#define ECHO_REF_BUF_LEN                (0xA0 * 4)   // 0x280    => 640 / 16kSampleRate = 40msec, 4*160=640
#define ECHO_REF_FRM_LEN                (0xA0)       // 0xA0     => 160 samples
#define ECHO_REF_FRM_BYTE_LEN           (0xA0 * 2)   // 0xA0 * sizeof(int16_t) => 320 bytes
#define EC_SUCCESS                      (1)
#define EC_MIC_NUMBER                   (1)
#define EC_REF_NUMBER                   (1)
#define IRQ4_SOURCE                     (0x10)
#define IRQ6_SOURCE                     (0x40)
#define IRQ7_SOURCE                     (0x80)
#define VOICE_DELAY_FRM                 (3)
#define ECHO_DELAY_FRM                  (2)
#define BIT_BARGEIN_PERIOD              (0x1)
#define BIT_VOW_PERIOD                  (0x2)
#define BIT_VOICE_R_IDX_UPDATE          (0x4)
#define ACC_SAMPLE_FACTOR               (0.7323) // (170 * 70) / (1625 * 10)

#endif // #ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Enumrations & Data Structures
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
typedef struct {
    void   (*func)(void);
    int    addr;
    int    size;
    short  *data;
    bool   flag;
} vow_model_t;

typedef struct {
    int     id;
    int     addr;
    int     size;
    bool    enable;
} vow_key_model_t;

typedef struct {
    unsigned int     dump_buf_offset;
    unsigned int     dump_size;
} vow_pcmdump_info_t;

typedef enum vow_event_info_t {
    VOW_MODEL_INIT = 0,
    VOW_MODEL_SPEAKER,
    VOW_MODEL_NOISE,
    VOW_MODEL_FIR,
    // Add before this
    VOW_MODEL_SIZE
} vow_event_info_t;

typedef enum vow_status_t {
    VOW_STATUS_IDLE = 0,
    VOW_STATUS_SCP_REG,
    VOW_STATUS_AP_REG
} vow_status_t;

typedef enum vow_mode_t {
    VOW_MODE_SCP_VOW = 0,
    VOW_MODE_VOICECOMMAND,
    VOW_MODE_MULTIPLE_KEY,
    VOW_MODE_MULTIPLE_KEY_VOICECOMMAND
} vow_mode_t;

typedef enum pmic_status_t {
    PMIC_STATUS_OFF = 0,
    PMIC_STATUS_PHASE1,
    PMIC_STATUS_PHASE2
} pmic_status_t;

typedef enum vow_flag_t {
    VOW_FLAG_DEBUG = 0,
    VOW_FLAG_PRE_LEARN,
    VOW_FLAG_DMIC_LOWPOWER,
    VOW_FLAG_PERIODIC_ENABLE,
    VOW_FLAG_FORCE_PHASE1_DEBUG,
    VOW_FLAG_FORCE_PHASE2_DEBUG,
    VOW_FLAG_SWIP_LOG_PRINT,
    VOW_FLAG_MTKIF_TYPE,
    VOW_FLAG_SEAMLESS,
} vow_flag_t;

typedef enum samplerate_t {
    VOW_SAMPLERATE_16K = 0,
    VOW_SAMPLERATE_32K
} samplerate_t;

typedef enum vow_dvfs_mode_t {
    VOW_DVFS_HW_MODE = 0,
    VOW_DVFS_SW_MODE,
    VOW_DVFS_SW_MODE_DONE
} vow_dvfs_mode_t;

typedef enum vow_ipi_result {
    VOW_IPI_SUCCESS = 0,
    VOW_IPI_CLR_SMODEL_ID_NOTMATCH,
    VOW_IPI_SET_SMODEL_NO_FREE_SLOT,
} vow_ipi_result;

typedef enum vow_ipi_msgid_t {
    IPIMSG_VOW_ENABLE = 0,
    IPIMSG_VOW_DISABLE = 1,
    IPIMSG_VOW_SETMODE = 2,
    IPIMSG_VOW_APREGDATA_ADDR = 3,
    IPIMSG_VOW_SET_MODEL = 4,
    IPIMSG_VOW_SET_FLAG = 5,
    IPIMSG_VOW_SET_SMART_DEVICE = 6,
    IPIMSG_VOW_DATAREADY_ACK = 7,

    IPIMSG_VOW_DATAREADY = 8,
    IPIMSG_VOW_RECOGNIZE_OK = 9,
#ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
    IPIMSG_VOW_SET_BARGEIN_ON = 10,
    IPIMSG_VOW_SET_BARGEIN_OFF = 11,
    IPIMSG_VOW_BARGEIN_DUMP_ON = 12,
    IPIMSG_VOW_BARGEIN_DUMP_OFF = 13,
    IPIMSG_VOW_BARGEIN_PCMDUMP_OK = 14,
#endif // #ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT

    VOW_RUN = 15,
#ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
    IPIMSG_VOW_BARGEIN_DUMP_INFO = 16,
#endif // #ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
} vow_ipi_msgid_t;

typedef enum vow_scplock_t {
    SCP_UNLOCK = 0,
    SCP_LOCK,
} vow_scplock_t;

typedef enum vow_force_phase {
    NO_FORCE = 0,
    FORCE_PHASE1,
    FORCE_PHASE2,
} vow_force_phase;

typedef enum vow_swip_log {
    VOW_SWIP_NO_LOG = 0,
    VOW_SWIP_HAS_LOG,
} vow_swip_log;

typedef enum vow_pmic_low_power_t {
    VOW_PMIC_LOW_POWER_INIT = 0,
    VOW_PMIC_LOW_POWER_ENTRY,
    VOW_PMIC_LOW_POWER_EXIT,
} vow_pmic_low_power_t;

typedef enum vow_mtkif_type {
    VOW_MTKIF_NONE = 0,
    VOW_MTKIF_AMIC,
    VOW_MTKIF_DMIC,
    VOW_MTKIF_DMIC_LP,
} vow_mtkif_type;

typedef struct vowserv_t {
    vow_status_t             vow_status;
    short                    read_idx;
    short                    write_idx;
    bool                     pre_learn;
    bool                     record_flag;
    bool                     debug_P1_flag;
    bool                     debug_P2_flag;
    bool                     swip_log_anable;
    bool                     dmic_low_power;
    bool                     dc_removal_enable;
    char                     dc_removal_order;
    bool                     can_unlock_scp_flag;
    vow_scplock_t            scpwakelock;
#if SW_FIX_METHOD
    bool                     checkDataClean;
#endif  // #if SW_FIX_METHOD
#if PMIC_6337_SUPPORT
    bool                     flr_enable;
    bool                     periodic_need_enable;
    bool                     open_periodic_flag;
#endif  // #if PMIC_6337_SUPPORT
    char                     dvfs_mode;
    int                      drop_count;
    int                      drop_frame;
    int                      apreg_addr;
    int                      samplerate;
    int                      data_length;
    int                      noKeywordCount;
#if SMART_AAD_MIC_SUPPORT
    bool                     smart_device_need_enable;
    bool                     smart_device_flag;
    int                      smart_device_eint_id;
#endif
#ifdef CFG_MTK_VOW_SEAMLESS_SUPPORT
    short                    read_buf_idx;
    bool                     seamless_record;
    bool                     sync_data_ready;
    short                    seamless_buf_w_ptr;
#endif  // #ifdef CFG_MTK_VOW_SEAMLESS_SUPPORT
    int                      pcmdump_cnt;
    vow_model_t              vow_model_event;
#if PMIC_6358_SUPPORT
    bool                     first_irq_flag;
#endif  // #if PMIC_6358_SUPPORT
    char                     mtkif_type;
#ifdef BLISRC_USE
    short                    blisrc_w_idx;
    short                    blisrc_r_idx;
#endif  // #ifdef BLISRC_USE
    uint32_t                 frm_cnt;
#ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
    uint32_t                 echo_w_idx;
    uint32_t                 echo_r_idx;
    uint32_t                 echoRead;
    uint32_t                 echoReadNowInIRQ;
    uint32_t                 isAduioFirstIrq;
    uint8_t                  echoFirstRamReadDone;
    uint8_t                  bargeInStatus;
    uint8_t                  vowTimeFlag;
    bool                     bargeInDumpFlag;
    uint32_t                 echo_data_len;
    int                      blisrc_internal_samples;
    uint32_t                 bargein_dump_frm;
    uint32_t                 dump_dram_addr;
#endif  // #ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
} vowserv_t;

#ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT
typedef enum dump_data_t{
    DUMP_VOW_VOICE = 0,
    DUMP_ECHO_REF = 1,
    NUM_DUMP_DATA = 2,
} dump_data_t;

typedef enum bargein_status_t{
    BARGEIN_OFF = 0,
    BARGEIN_START = 1,
    BARGEIN_WORKING1 = 2,
    BARGEIN_WORKING2 = 3,
} bargein_status_t;

typedef struct {
	dump_data_t data_type;
	uint32_t mic_dump_size;
	uint32_t mic_offset;
	uint32_t echo_dump_size;
	uint32_t echo_offset;
} bargein_pcmdump_info_t;
#endif // #ifdef CFG_MTK_VOW_BARGE_IN_SUPPORT

typedef struct {
    vow_status_t             vow_status;
    bool                     debug_P1_flag;
    bool                     debug_P2_flag;
    bool                     swip_log_anable;
    int                      apreg_addr;
    vow_model_t              vow_model_event;
} do_save_vow_t;

#endif  // end of AUDIO_TASK_VOW_PARAMS_H
