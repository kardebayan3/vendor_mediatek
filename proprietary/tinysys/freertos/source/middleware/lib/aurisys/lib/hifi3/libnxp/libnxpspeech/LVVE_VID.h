//**********************************************************************
// Copyright (c) 2004-2018 NXP Software. All rights are reserved.
// Reproduction in whole or in part is prohibited without the prior
// written consent of the copyright owner.
//
// This software and any compilation or derivative thereof is and
// shall remain the proprietary information of NXP Software and is
// highly confidential in nature. Any and all use hereof is restricted
// and is subject to the terms and conditions set forth in the
// software license agreement concluded with NXP Software.
//
// Under no circumstances is this software or any derivative thereof
// to be combined with any Open Source Software in any way or
// licensed under any Open License Terms without the express prior
// written  permission of NXP Software.
//
// For the purpose of this clause, the term Open Source Software means
// any software that is licensed under Open License Terms. Open
// License Terms means terms in any license that require as a
// condition of use, modification and/or distribution of a work
//
//          1. the making available of source code or other materials
//             preferred for modification, or
//
//          2. the granting of permission for creating derivative
//             works, or
//
//          3. the reproduction of certain notices or license terms
//             in derivative works or accompanying documentation, or
//
//          4. the granting of a royalty-free license to any party
//             under Intellectual Property Rights
//
// regarding the work and/or any work that contains, is combined with,
// requires or otherwise is based on the work.
//
// This software is provided for ease of recompilation only.
// Modification and reverse engineering of this software are strictly
// prohibited.
//**********************************************************************

/** @file
 *  Header file for the application layer interface of the REL_LVVE_DEMO_NV_AVC_AGC_RX_DRC_RX_SWB module
 *
 *  The suffix VID in header file stands for Voice Interface Definition
 *  This files includes all definitions, types, and structures required by the calling
 *  layer. Public functions are defined in the protoypes header files.
 *  All other types, structures and functions are private.
 */

#ifndef __REL_LVVE_DEMO_NV_AVC_AGC_RX_DRC_RX_SWB_VIDPP_DEMO_H__
#define __REL_LVVE_DEMO_NV_AVC_AGC_RX_DRC_RX_SWB_VIDPP_DEMO_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/
#include "LVC_Types.h"

/****************************************************************************************/
/*                                                                                      */
/*  Definitions                                                                         */
/*                                                                                      */
/****************************************************************************************/
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVFENS_MASK1</td>
        <td>1</td>
        <td>Far End Noise Suppression is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_HPF_MASK1</td>
        <td>4</td>
        <td>High Pass Filter is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVNG_MASK1</td>
        <td>16</td>
        <td>Noise Gate is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_NF_MASK1</td>
        <td>32</td>
        <td>Notch Filter is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_EQ_MASK1</td>
        <td>64</td>
        <td>Equalizer is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_AGC_MASK1</td>
        <td>128</td>
        <td>Automatic Gain Control is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_DRC_MASK1</td>
        <td>256</td>
        <td>Dynamic Range Control is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_VOL_MASK1</td>
        <td>512</td>
        <td>Volume Control is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_CNG_MASK1</td>
        <td>2048</td>
        <td>Comfort Noise Generator is present in the LVVE_Rx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_RX_LVAVC_MASK1</td>
        <td>8192</td>
        <td>ActiveVoiceContrast is present in the LVVE_Rx</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVVIDHeader_LVVE_Rx_Mask1_bm;
/**
* @def LVVIDHeader_LVVE_Rx_LVFENS_MASK1
* Far End Noise Suppression is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVFENS_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)1)
/**
* @def LVVIDHeader_LVVE_Rx_HPF_MASK1
* High Pass Filter is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_HPF_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)4)
/**
* @def LVVIDHeader_LVVE_Rx_LVNG_MASK1
* Noise Gate is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVNG_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)16)
/**
* @def LVVIDHeader_LVVE_Rx_NF_MASK1
* Notch Filter is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_NF_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)32)
/**
* @def LVVIDHeader_LVVE_Rx_EQ_MASK1
* Equalizer is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_EQ_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)64)
/**
* @def LVVIDHeader_LVVE_Rx_AGC_MASK1
* Automatic Gain Control is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_AGC_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)128)
/**
* @def LVVIDHeader_LVVE_Rx_DRC_MASK1
* Dynamic Range Control is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_DRC_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)256)
/**
* @def LVVIDHeader_LVVE_Rx_VOL_MASK1
* Volume Control is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_VOL_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)512)
/**
* @def LVVIDHeader_LVVE_Rx_CNG_MASK1
* Comfort Noise Generator is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_CNG_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)2048)
/**
* @def LVVIDHeader_LVVE_Rx_LVAVC_MASK1
* ActiveVoiceContrast is present in the LVVE_Rx
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_LVAVC_MASK1 ((LVVIDHeader_LVVE_Rx_Mask1_bm)8192)
/**
* @def LVVIDHEADER_LVVE_RX_MASK1_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Rx_Mask1_bm
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK1_BM_MIN ((LVVIDHeader_LVVE_Rx_Mask1_bm)0)
/**
* @def LVVIDHEADER_LVVE_RX_MASK1_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Rx_Mask1_bm
* @see LVVIDHeader_LVVE_Rx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK1_BM_MAX ((LVVIDHeader_LVVE_Rx_Mask1_bm)((1) | (4) | (16) | (32) | (64) | (128) | (256) | (512) | (2048) | (8192) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVVIDHeader_LVVE_Rx_Mask2_bm;
/**
* @def LVVIDHEADER_LVVE_RX_MASK2_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Rx_Mask2_bm
* @see LVVIDHeader_LVVE_Rx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK2_BM_MIN ((LVVIDHeader_LVVE_Rx_Mask2_bm)0)
/**
* @def LVVIDHEADER_LVVE_RX_MASK2_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Rx_Mask2_bm
* @see LVVIDHeader_LVVE_Rx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_RX_MASK2_BM_MAX ((LVVIDHeader_LVVE_Rx_Mask2_bm)((0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_EQ_MASK1</td>
        <td>4</td>
        <td>Equalizer is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LVWM_MASK1</td>
        <td>8</td>
        <td>WhisperMode is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_DRC_MASK1</td>
        <td>16</td>
        <td>Dynamic Range Control is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_VOL_MASK1</td>
        <td>32</td>
        <td>Volume Control is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_HPFREF_MASK1</td>
        <td>64</td>
        <td>HPFREF is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_HPF_MASK1</td>
        <td>128</td>
        <td>HPF is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LPF_MASK1</td>
        <td>256</td>
        <td>LPF is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_LVNV_04_MASK1</td>
        <td>4096</td>
        <td>NoiseVoid is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_CNG_MASK1</td>
        <td>8192</td>
        <td>Comfort Noise Generator is present in the LVVE_Tx</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_LVVE_TX_WNS_MASK1</td>
        <td>16384</td>
        <td>NoiseVoid with WindNoise Suppression is present in the LVVE_Tx</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVVIDHeader_LVVE_Tx_Mask1_bm;
/**
* @def LVVIDHeader_LVVE_Tx_EQ_MASK1
* Equalizer is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_EQ_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)4)
/**
* @def LVVIDHeader_LVVE_Tx_LVWM_MASK1
* WhisperMode is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LVWM_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)8)
/**
* @def LVVIDHeader_LVVE_Tx_DRC_MASK1
* Dynamic Range Control is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_DRC_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)16)
/**
* @def LVVIDHeader_LVVE_Tx_VOL_MASK1
* Volume Control is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_VOL_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)32)
/**
* @def LVVIDHeader_LVVE_Tx_HPFREF_MASK1
* HPFREF is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_HPFREF_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)64)
/**
* @def LVVIDHeader_LVVE_Tx_HPF_MASK1
* HPF is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_HPF_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)128)
/**
* @def LVVIDHeader_LVVE_Tx_LPF_MASK1
* LPF is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LPF_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)256)
/**
* @def LVVIDHeader_LVVE_Tx_LVNV_04_MASK1
* NoiseVoid is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_LVNV_04_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)4096)
/**
* @def LVVIDHeader_LVVE_Tx_CNG_MASK1
* Comfort Noise Generator is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_CNG_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)8192)
/**
* @def LVVIDHeader_LVVE_Tx_WNS_MASK1
* NoiseVoid with WindNoise Suppression is present in the LVVE_Tx
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_WNS_MASK1 ((LVVIDHeader_LVVE_Tx_Mask1_bm)16384)
/**
* @def LVVIDHEADER_LVVE_TX_MASK1_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Tx_Mask1_bm
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK1_BM_MIN ((LVVIDHeader_LVVE_Tx_Mask1_bm)0)
/**
* @def LVVIDHEADER_LVVE_TX_MASK1_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Tx_Mask1_bm
* @see LVVIDHeader_LVVE_Tx_Mask1_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK1_BM_MAX ((LVVIDHeader_LVVE_Tx_Mask1_bm)((4) | (8) | (16) | (32) | (64) | (128) | (256) | (4096) | (8192) | (16384) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVVIDHeader_LVVE_Tx_Mask2_bm;
/**
* @def LVVIDHEADER_LVVE_TX_MASK2_BM_MIN
* Minimal value for LVVIDHeader_LVVE_Tx_Mask2_bm
* @see LVVIDHeader_LVVE_Tx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK2_BM_MIN ((LVVIDHeader_LVVE_Tx_Mask2_bm)0)
/**
* @def LVVIDHEADER_LVVE_TX_MASK2_BM_MAX
* Maximal value for LVVIDHeader_LVVE_Tx_Mask2_bm
* @see LVVIDHeader_LVVE_Tx_Mask2_bm
*/
#define LVVIDHEADER_LVVE_TX_MASK2_BM_MAX ((LVVIDHeader_LVVE_Tx_Mask2_bm)((0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_CONFIGURATIONS_SUPERWIDEBAND_MASK1</td>
        <td>4</td>
        <td>Full Band configuration is present in the LVVE</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_CONFIGURATIONS_AEC_FULLBAND_MASK1</td>
        <td>32</td>
        <td>Full Band AEC configuration is present in the LVVE</td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_CONFIGURATIONS_ANTIHOWLING_MASK1</td>
        <td>64</td>
        <td>NoiseVoid with Howling Suppression is present in the LVVE_Tx</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVVIDHeader_Configurations_Mask1_bm;
/**
* @def LVVIDHEADER_CONFIGURATIONS_SUPERWIDEBAND_MASK1
* Full Band configuration is present in the LVVE
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_SUPERWIDEBAND_MASK1 ((LVVIDHeader_Configurations_Mask1_bm)4)
/**
* @def LVVIDHEADER_CONFIGURATIONS_AEC_FULLBAND_MASK1
* Full Band AEC configuration is present in the LVVE
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_AEC_FULLBAND_MASK1 ((LVVIDHeader_Configurations_Mask1_bm)32)
/**
* @def LVVIDHEADER_CONFIGURATIONS_ANTIHOWLING_MASK1
* NoiseVoid with Howling Suppression is present in the LVVE_Tx
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_ANTIHOWLING_MASK1 ((LVVIDHeader_Configurations_Mask1_bm)64)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MIN
* Minimal value for LVVIDHeader_Configurations_Mask1_bm
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MIN ((LVVIDHeader_Configurations_Mask1_bm)0)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MAX
* Maximal value for LVVIDHeader_Configurations_Mask1_bm
* @see LVVIDHeader_Configurations_Mask1_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MAX ((LVVIDHeader_Configurations_Mask1_bm)((4) | (32) | (64) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVVIDHEADER_CONFIGURATIONS_VOICEEXPERIENCE_MASK1</td>
        <td>1</td>
        <td>Product is VoiceExperience</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVVIDHeader_Configurations_Mask2_bm;
/**
* @def LVVIDHEADER_CONFIGURATIONS_VOICEEXPERIENCE_MASK1
* Product is VoiceExperience
* @see LVVIDHeader_Configurations_Mask2_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_VOICEEXPERIENCE_MASK1 ((LVVIDHeader_Configurations_Mask2_bm)1)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MIN
* Minimal value for LVVIDHeader_Configurations_Mask2_bm
* @see LVVIDHeader_Configurations_Mask2_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MIN ((LVVIDHeader_Configurations_Mask2_bm)0)
/**
* @def LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MAX
* Maximal value for LVVIDHeader_Configurations_Mask2_bm
* @see LVVIDHeader_Configurations_Mask2_bm
*/
#define LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MAX ((LVVIDHeader_Configurations_Mask2_bm)((1) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL</td>
        <td>1</td>
        <td>Enable the NASEL functionality</td>
    </tr>
    <tr>
        <td>@ref LVAVC_FU</td>
        <td>2</td>
        <td>Enable the FU functionality</td>
    </tr>
    <tr>
        <td>@ref LVAVC_RMS_ADAPTATION</td>
        <td>4</td>
        <td>Enable the RMS adaptation functionality</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_SOUNDCLASS</td>
        <td>16</td>
        <td>Enable Sound Class functionality of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_SOUNDCLASS_GWCONFIG</td>
        <td>32</td>
        <td>Enable GW 2nd configuration of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_SOUNDCLASS_GWCONFIG2</td>
        <td>64</td>
        <td>Enable GW 3rd and 4th configurations of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_NASEL_NOISEBANDGAIN</td>
        <td>128</td>
        <td>Enable Noise level dependency for the gain per Band of NASEL module</td>
    </tr>
    <tr>
        <td>@ref LVAVC_ONEINSTANCE_CONFIG</td>
        <td>4096</td>
        <td>Enable 1 instance mode in case there is no external processing done between the 2 instances of LVAVC</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVAVC_ModeWord_bm;
/**
* @def LVAVC_NASEL
* Enable the NASEL functionality
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL ((LVAVC_ModeWord_bm)1)
/**
* @def LVAVC_FU
* Enable the FU functionality
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_FU ((LVAVC_ModeWord_bm)2)
/**
* @def LVAVC_RMS_ADAPTATION
* Enable the RMS adaptation functionality
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_RMS_ADAPTATION ((LVAVC_ModeWord_bm)4)
/**
* @def LVAVC_NASEL_SOUNDCLASS
* Enable Sound Class functionality of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_SOUNDCLASS ((LVAVC_ModeWord_bm)16)
/**
* @def LVAVC_NASEL_SOUNDCLASS_GWCONFIG
* Enable GW 2nd configuration of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_SOUNDCLASS_GWCONFIG ((LVAVC_ModeWord_bm)32)
/**
* @def LVAVC_NASEL_SOUNDCLASS_GWCONFIG2
* Enable GW 3rd and 4th configurations of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_SOUNDCLASS_GWCONFIG2 ((LVAVC_ModeWord_bm)64)
/**
* @def LVAVC_NASEL_NOISEBANDGAIN
* Enable Noise level dependency for the gain per Band of NASEL module
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_NASEL_NOISEBANDGAIN ((LVAVC_ModeWord_bm)128)
/**
* @def LVAVC_ONEINSTANCE_CONFIG
* Enable 1 instance mode in case there is no external processing done between the 2 instances of LVAVC
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_ONEINSTANCE_CONFIG ((LVAVC_ModeWord_bm)4096)
/**
* @def LVAVC_MODEWORD_BM_MIN
* Minimal value for LVAVC_ModeWord_bm
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_MODEWORD_BM_MIN ((LVAVC_ModeWord_bm)0)
/**
* @def LVAVC_MODEWORD_BM_MAX
* Maximal value for LVAVC_ModeWord_bm
* @see LVAVC_ModeWord_bm
*/
#define LVAVC_MODEWORD_BM_MAX ((LVAVC_ModeWord_bm)((1) | (2) | (4) | (16) | (32) | (64) | (128) | (4096) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVFENS_MODE_WNS</td>
        <td>1</td>
        <td>Wind Noise Suppression functionality</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVFENS_ModeWord_bm;
/**
* @def LVFENS_MODE_WNS
* Wind Noise Suppression functionality
* @see LVFENS_ModeWord_bm
*/
#define LVFENS_MODE_WNS ((LVFENS_ModeWord_bm)1)
/**
* @def LVFENS_MODEWORD_BM_MIN
* Minimal value for LVFENS_ModeWord_bm
* @see LVFENS_ModeWord_bm
*/
#define LVFENS_MODEWORD_BM_MIN ((LVFENS_ModeWord_bm)0)
/**
* @def LVFENS_MODEWORD_BM_MAX
* Maximal value for LVFENS_ModeWord_bm
* @see LVFENS_ModeWord_bm
*/
#define LVFENS_MODEWORD_BM_MAX ((LVFENS_ModeWord_bm)((1) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_AVL</td>
        <td>1</td>
        <td>Turns on or off the AVL. If off, both AGC and Limiter are bypassed</td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_SPDETECT</td>
        <td>2</td>
        <td>Allows disabling speech detection in the AVL. When turned off, the AGC adapts its gain all the time.</td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_LIMITER</td>
        <td>4</td>
        <td>Turns on or off the limiter.</td>
    </tr>
    <tr>
        <td>@ref LVWM_MODE_EXT_SPDETECT</td>
        <td>8</td>
        <td>Used to tell the WhisperMode module if an external speech detection is available (If 0, there is no external speech detector).</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVWM_ModeWord_bm;
/**
* @def LVWM_MODE_AVL
* Turns on or off the AVL. If off, both AGC and Limiter are bypassed
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_AVL ((LVWM_ModeWord_bm)1)
/**
* @def LVWM_MODE_SPDETECT
* Allows disabling speech detection in the AVL. When turned off, the AGC adapts its gain all the time.
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_SPDETECT ((LVWM_ModeWord_bm)2)
/**
* @def LVWM_MODE_LIMITER
* Turns on or off the limiter.
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_LIMITER ((LVWM_ModeWord_bm)4)
/**
* @def LVWM_MODE_EXT_SPDETECT
* Used to tell the WhisperMode module if an external speech detection is available (If 0, there is no external speech detector).
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODE_EXT_SPDETECT ((LVWM_ModeWord_bm)8)
/**
* @def LVWM_MODEWORD_BM_MIN
* Minimal value for LVWM_ModeWord_bm
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODEWORD_BM_MIN ((LVWM_ModeWord_bm)0)
/**
* @def LVWM_MODEWORD_BM_MAX
* Maximal value for LVWM_ModeWord_bm
* @see LVWM_ModeWord_bm
*/
#define LVWM_MODEWORD_BM_MAX ((LVWM_ModeWord_bm)((1) | (2) | (4) | (8) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AEC</td>
        <td>1</td>
        <td>AEC NLMS or PBFDAF functionality applied on all microphones</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_FSB</td>
        <td>4</td>
        <td>Beamformer functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PCD</td>
        <td>8</td>
        <td>Path Change Detection functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_GSC</td>
        <td>16</td>
        <td>Generalized Sidelobe Canceller functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_DNNS</td>
        <td>32</td>
        <td>Dynamic Non-stationary Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_DES</td>
        <td>64</td>
        <td>Dynamic Echo Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NS</td>
        <td>128</td>
        <td>Stationary Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NS_N</td>
        <td>256</td>
        <td>Non-stationary Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PREEMPH</td>
        <td>512</td>
        <td>Pre- and de-emphasis functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_DNNS_NGS</td>
        <td>1024</td>
        <td>Natural Gain Smoothing functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_CAL</td>
        <td>2048</td>
        <td>MICPOW Calibration functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PCD_DT</td>
        <td>4096</td>
        <td>Preserve Double Talk after PCD in speakerphone config.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_BODYNOISE_REM</td>
        <td>8192</td>
        <td>Body Noise Removal functionality. Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_CLIPPED_ECHO</td>
        <td>16384</td>
        <td>Clipped echo support</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_IMCOHFASTFLOOR</td>
        <td>32768</td>
        <td>Imcoh Fast Floor functionality. Should be disabled by default.</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVNV_ModeWord_bm;
/**
* @def LVNV_MODE_AEC
* AEC NLMS or PBFDAF functionality applied on all microphones
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_AEC ((LVNV_ModeWord_bm)1)
/**
* @def LVNV_MODE_FSB
* Beamformer functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_FSB ((LVNV_ModeWord_bm)4)
/**
* @def LVNV_MODE_PCD
* Path Change Detection functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_PCD ((LVNV_ModeWord_bm)8)
/**
* @def LVNV_MODE_GSC
* Generalized Sidelobe Canceller functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_GSC ((LVNV_ModeWord_bm)16)
/**
* @def LVNV_MODE_DNNS
* Dynamic Non-stationary Noise Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_DNNS ((LVNV_ModeWord_bm)32)
/**
* @def LVNV_MODE_DES
* Dynamic Echo Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_DES ((LVNV_ModeWord_bm)64)
/**
* @def LVNV_MODE_NS
* Stationary Noise Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_NS ((LVNV_ModeWord_bm)128)
/**
* @def LVNV_MODE_NS_N
* Non-stationary Noise Suppression functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_NS_N ((LVNV_ModeWord_bm)256)
/**
* @def LVNV_MODE_PREEMPH
* Pre- and de-emphasis functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_PREEMPH ((LVNV_ModeWord_bm)512)
/**
* @def LVNV_MODE_DNNS_NGS
* Natural Gain Smoothing functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_DNNS_NGS ((LVNV_ModeWord_bm)1024)
/**
* @def LVNV_MODE_CAL
* MICPOW Calibration functionality
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_CAL ((LVNV_ModeWord_bm)2048)
/**
* @def LVNV_MODE_PCD_DT
* Preserve Double Talk after PCD in speakerphone config.
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_PCD_DT ((LVNV_ModeWord_bm)4096)
/**
* @def LVNV_MODE_BODYNOISE_REM
* Body Noise Removal functionality. Should be disabled by default.
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_BODYNOISE_REM ((LVNV_ModeWord_bm)8192)
/**
* @def LVNV_MODE_CLIPPED_ECHO
* Clipped echo support
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_CLIPPED_ECHO ((LVNV_ModeWord_bm)16384)
/**
* @def LVNV_MODE_IMCOHFASTFLOOR
* Imcoh Fast Floor functionality. Should be disabled by default.
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODE_IMCOHFASTFLOOR ((LVNV_ModeWord_bm)32768)
/**
* @def LVNV_MODEWORD_BM_MIN
* Minimal value for LVNV_ModeWord_bm
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODEWORD_BM_MIN ((LVNV_ModeWord_bm)0)
/**
* @def LVNV_MODEWORD_BM_MAX
* Maximal value for LVNV_ModeWord_bm
* @see LVNV_ModeWord_bm
*/
#define LVNV_MODEWORD_BM_MAX ((LVNV_ModeWord_bm)((1) | (4) | (8) | (16) | (32) | (64) | (128) | (256) | (512) | (1024) | (2048) | (4096) | (8192) | (16384) | (32768) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_WNS</td>
        <td>1</td>
        <td>Wind Noise Suppression functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_OUTPUT_HPF</td>
        <td>2</td>
        <td>High-pass filter at output functionality. Should be enabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_FSB_HPF</td>
        <td>4</td>
        <td>FSB High-pass filter functionality. Handset config only. Should be enabled by default)</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_SSRC_HEADROOM</td>
        <td>8</td>
        <td>SSRC headroom functionality. For sampling frequencies > 16kHz. Should be enabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_TONEPROTECT</td>
        <td>16</td>
        <td>Tone protection functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_COHERENT_NOISE</td>
        <td>32</td>
        <td>Support for Stationary Coherent noise fields. Should be enabled by default</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NOISE_GATING</td>
        <td>64</td>
        <td>Noise gating functionality to give slightly better SNRi. Should be enabled by default in handset config.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_FIXED_BMF</td>
        <td>128</td>
        <td>Fixed coefficients for beamformer. Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PROTECT_DOWN_POSITION</td>
        <td>256</td>
        <td>Protects clean speech during down position in handset config. Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_PITCH</td>
        <td>512</td>
        <td>Pitch enhancement functionality. Should be enabled by default. When enabled, NGS is also enabled (overrules previous settings).</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_POSITION_CHANGE</td>
        <td>1024</td>
        <td>Improved performance during position change in handset config. Should be enabled by default.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_FARTALK</td>
        <td>2048</td>
        <td>Support for far distance speech.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_NEDET</td>
        <td>4096</td>
        <td>Nearend detection during double talk.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_SCNE_HNDST</td>
        <td>8192</td>
        <td>Improved single channel noise estimation for handset configuration.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_STEREOAEC_PROXIMITY</td>
        <td>16384</td>
        <td>Improved stereo AEC performance for proximity based receiver mute</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVNV_ModeWord2_bm;
/**
* @def LVNV_MODE_WNS
* Wind Noise Suppression functionality
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_WNS ((LVNV_ModeWord2_bm)1)
/**
* @def LVNV_MODE_OUTPUT_HPF
* High-pass filter at output functionality. Should be enabled by default.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_OUTPUT_HPF ((LVNV_ModeWord2_bm)2)
/**
* @def LVNV_MODE_FSB_HPF
* FSB High-pass filter functionality. Handset config only. Should be enabled by default)
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_FSB_HPF ((LVNV_ModeWord2_bm)4)
/**
* @def LVNV_MODE_SSRC_HEADROOM
* SSRC headroom functionality. For sampling frequencies > 16kHz. Should be enabled by default.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_SSRC_HEADROOM ((LVNV_ModeWord2_bm)8)
/**
* @def LVNV_MODE_TONEPROTECT
* Tone protection functionality
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_TONEPROTECT ((LVNV_ModeWord2_bm)16)
/**
* @def LVNV_MODE_COHERENT_NOISE
* Support for Stationary Coherent noise fields. Should be enabled by default
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_COHERENT_NOISE ((LVNV_ModeWord2_bm)32)
/**
* @def LVNV_MODE_NOISE_GATING
* Noise gating functionality to give slightly better SNRi. Should be enabled by default in handset config.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_NOISE_GATING ((LVNV_ModeWord2_bm)64)
/**
* @def LVNV_MODE_FIXED_BMF
* Fixed coefficients for beamformer. Should be disabled by default.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_FIXED_BMF ((LVNV_ModeWord2_bm)128)
/**
* @def LVNV_MODE_PROTECT_DOWN_POSITION
* Protects clean speech during down position in handset config. Should be disabled by default.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_PROTECT_DOWN_POSITION ((LVNV_ModeWord2_bm)256)
/**
* @def LVNV_MODE_PITCH
* Pitch enhancement functionality. Should be enabled by default. When enabled, NGS is also enabled (overrules previous settings).
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_PITCH ((LVNV_ModeWord2_bm)512)
/**
* @def LVNV_MODE_POSITION_CHANGE
* Improved performance during position change in handset config. Should be enabled by default.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_POSITION_CHANGE ((LVNV_ModeWord2_bm)1024)
/**
* @def LVNV_MODE_FARTALK
* Support for far distance speech.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_FARTALK ((LVNV_ModeWord2_bm)2048)
/**
* @def LVNV_MODE_NEDET
* Nearend detection during double talk.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_NEDET ((LVNV_ModeWord2_bm)4096)
/**
* @def LVNV_MODE_SCNE_HNDST
* Improved single channel noise estimation for handset configuration.
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_SCNE_HNDST ((LVNV_ModeWord2_bm)8192)
/**
* @def LVNV_MODE_STEREOAEC_PROXIMITY
* Improved stereo AEC performance for proximity based receiver mute
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODE_STEREOAEC_PROXIMITY ((LVNV_ModeWord2_bm)16384)
/**
* @def LVNV_MODEWORD2_BM_MIN
* Minimal value for LVNV_ModeWord2_bm
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODEWORD2_BM_MIN ((LVNV_ModeWord2_bm)0)
/**
* @def LVNV_MODEWORD2_BM_MAX
* Maximal value for LVNV_ModeWord2_bm
* @see LVNV_ModeWord2_bm
*/
#define LVNV_MODEWORD2_BM_MAX ((LVNV_ModeWord2_bm)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (128) | (256) | (512) | (1024) | (2048) | (4096) | (8192) | (16384) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_FBE</td>
        <td>1</td>
        <td>AntiHowling Feedback Estimation functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_SHR</td>
        <td>2</td>
        <td>Spectral Howling Reduction functionality.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_EXTREF</td>
        <td>4</td>
        <td>Use delayed external echo ref as reference instead of delayed txout.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_NLMS_FAIL_DETECTION</td>
        <td>8</td>
        <td>NLMS failure detection functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_VOICEDSPEECH_PROTECTION</td>
        <td>16</td>
        <td>Voiced Speech protection functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_NOISE_ADAPTIVE_MASK</td>
        <td>32</td>
        <td>Adaptive noise mask functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_PEAK_DETECTION</td>
        <td>64</td>
        <td>Peak detection functionality</td>
    </tr>
    <tr>
        <td>@ref LVNV_MODE_AH_DYNAMIC_HOWLING_SUPPRESSION</td>
        <td>128</td>
        <td>Dynamic howling suppression functionality</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVNV_ModeWord3_bm;
/**
* @def LVNV_MODE_AH_FBE
* AntiHowling Feedback Estimation functionality
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_FBE ((LVNV_ModeWord3_bm)1)
/**
* @def LVNV_MODE_AH_SHR
* Spectral Howling Reduction functionality.
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_SHR ((LVNV_ModeWord3_bm)2)
/**
* @def LVNV_MODE_AH_EXTREF
* Use delayed external echo ref as reference instead of delayed txout.
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_EXTREF ((LVNV_ModeWord3_bm)4)
/**
* @def LVNV_MODE_AH_NLMS_FAIL_DETECTION
* NLMS failure detection functionality
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_NLMS_FAIL_DETECTION ((LVNV_ModeWord3_bm)8)
/**
* @def LVNV_MODE_AH_VOICEDSPEECH_PROTECTION
* Voiced Speech protection functionality
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_VOICEDSPEECH_PROTECTION ((LVNV_ModeWord3_bm)16)
/**
* @def LVNV_MODE_AH_NOISE_ADAPTIVE_MASK
* Adaptive noise mask functionality
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_NOISE_ADAPTIVE_MASK ((LVNV_ModeWord3_bm)32)
/**
* @def LVNV_MODE_AH_PEAK_DETECTION
* Peak detection functionality
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_PEAK_DETECTION ((LVNV_ModeWord3_bm)64)
/**
* @def LVNV_MODE_AH_DYNAMIC_HOWLING_SUPPRESSION
* Dynamic howling suppression functionality
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODE_AH_DYNAMIC_HOWLING_SUPPRESSION ((LVNV_ModeWord3_bm)128)
/**
* @def LVNV_MODEWORD3_BM_MIN
* Minimal value for LVNV_ModeWord3_bm
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODEWORD3_BM_MIN ((LVNV_ModeWord3_bm)0)
/**
* @def LVNV_MODEWORD3_BM_MAX
* Maximal value for LVNV_ModeWord3_bm
* @see LVNV_ModeWord3_bm
*/
#define LVNV_MODEWORD3_BM_MAX ((LVNV_ModeWord3_bm)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (128) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_NLMS1</td>
        <td>1</td>
        <td>NLMS residual signal of second microphone (wideband synthesis is applied on the lowband with highband for 16 kHz)</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_BMF_REF</td>
        <td>2</td>
        <td>Beamformer noise reference signal (wideband synthesis is applied on the lowband with highband for 16 kHz)</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_FSB_OUT</td>
        <td>4</td>
        <td>FSB output signal (before wideband synthesis the highband is muted)</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_SPDET_NE</td>
        <td>8</td>
        <td>Tuning beep when near end speech detector results in FSB updates</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_PCD</td>
        <td>32</td>
        <td>Tuning beep when when path change is detected</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_CAL_MICPOW</td>
        <td>64</td>
        <td>Tuning beep when MicPow is lower than Cal_micPowFloorMin</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_MONODET</td>
        <td>128</td>
        <td>Tuning beep during earbud removal detection in stereo headset config or microphone coverage in handset/speakerphone config</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_INITCVGCONTROL</td>
        <td>256</td>
        <td>Disable Initial Convergence Control for AEC and DNNS</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_HB_MUTE</td>
        <td>512</td>
        <td>Mute high band (4kHz-8kHz). Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_TB_MUTE</td>
        <td>1024</td>
        <td>Mute top band (above 8kHz). Should be disabled by default.</td>
    </tr>
    <tr>
        <td>@ref TUNING_MODE_SPLITBAND_OFF</td>
        <td>2048</td>
        <td>Disable effect of LF parameters for echo subtraction</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVNV_TuningModeWord_bm;
/**
* @def TUNING_MODE_NLMS1
* NLMS residual signal of second microphone (wideband synthesis is applied on the lowband with highband for 16 kHz)
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_NLMS1 ((LVNV_TuningModeWord_bm)1)
/**
* @def TUNING_MODE_BMF_REF
* Beamformer noise reference signal (wideband synthesis is applied on the lowband with highband for 16 kHz)
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_BMF_REF ((LVNV_TuningModeWord_bm)2)
/**
* @def TUNING_MODE_FSB_OUT
* FSB output signal (before wideband synthesis the highband is muted)
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_FSB_OUT ((LVNV_TuningModeWord_bm)4)
/**
* @def TUNING_MODE_SPDET_NE
* Tuning beep when near end speech detector results in FSB updates
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_SPDET_NE ((LVNV_TuningModeWord_bm)8)
/**
* @def TUNING_MODE_PCD
* Tuning beep when when path change is detected
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_PCD ((LVNV_TuningModeWord_bm)32)
/**
* @def TUNING_MODE_CAL_MICPOW
* Tuning beep when MicPow is lower than Cal_micPowFloorMin
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_CAL_MICPOW ((LVNV_TuningModeWord_bm)64)
/**
* @def TUNING_MODE_MONODET
* Tuning beep during earbud removal detection in stereo headset config or microphone coverage in handset/speakerphone config
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_MONODET ((LVNV_TuningModeWord_bm)128)
/**
* @def TUNING_MODE_INITCVGCONTROL
* Disable Initial Convergence Control for AEC and DNNS
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_INITCVGCONTROL ((LVNV_TuningModeWord_bm)256)
/**
* @def TUNING_MODE_HB_MUTE
* Mute high band (4kHz-8kHz). Should be disabled by default.
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_HB_MUTE ((LVNV_TuningModeWord_bm)512)
/**
* @def TUNING_MODE_TB_MUTE
* Mute top band (above 8kHz). Should be disabled by default.
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_TB_MUTE ((LVNV_TuningModeWord_bm)1024)
/**
* @def TUNING_MODE_SPLITBAND_OFF
* Disable effect of LF parameters for echo subtraction
* @see LVNV_TuningModeWord_bm
*/
#define TUNING_MODE_SPLITBAND_OFF ((LVNV_TuningModeWord_bm)2048)
/**
* @def LVNV_TUNINGMODEWORD_BM_MIN
* Minimal value for LVNV_TuningModeWord_bm
* @see LVNV_TuningModeWord_bm
*/
#define LVNV_TUNINGMODEWORD_BM_MIN ((LVNV_TuningModeWord_bm)0)
/**
* @def LVNV_TUNINGMODEWORD_BM_MAX
* Maximal value for LVNV_TuningModeWord_bm
* @see LVNV_TuningModeWord_bm
*/
#define LVNV_TUNINGMODEWORD_BM_MAX ((LVNV_TuningModeWord_bm)((1) | (2) | (4) | (8) | (32) | (64) | (128) | (256) | (512) | (1024) | (2048) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_WNSINPUT</td>
        <td>1</td>
        <td>Use this microphone as input to WNS</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_FSBOUT</td>
        <td>2</td>
        <td>This microphone contributes to FSB Output</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_FSBNOISEREF</td>
        <td>8</td>
        <td>Use this microphone FSB noise reference as input to the GSC</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_GSCINPUT</td>
        <td>16</td>
        <td>Use this microphone as GSC speech reference input (only supported for primary microphone)</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_SDET</td>
        <td>64</td>
        <td>Use this microphone in speech detector</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_GAINETA</td>
        <td>512</td>
        <td>Use this channel's echo estimate in the GainEta mechanism.</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW</td>
        <td>4096</td>
        <td>Order of preference when microphone(s) are covered. (LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH,LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW)=preference when microphone is covered. Higher value means higher preference</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH</td>
        <td>8192</td>
        <td>Order of preference when microphone(s) are covered. (LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH,LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW)=preference when microphone is covered. Higher value means higher preference</td>
    </tr>
    <tr>
        <td>@ref LVNV_MICROPHONE_CONTROL_AEC_LOWCOST</td>
        <td>16384</td>
        <td>Enable low-cost AEC for this microphone. It is strongly adviced NOT to enable this for primary microphone.</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVNV_MicrophoneControlWord_bm;
/**
* @def LVNV_MICROPHONE_CONTROL_WNSINPUT
* Use this microphone as input to WNS
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_WNSINPUT ((LVNV_MicrophoneControlWord_bm)1)
/**
* @def LVNV_MICROPHONE_CONTROL_FSBOUT
* This microphone contributes to FSB Output
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_FSBOUT ((LVNV_MicrophoneControlWord_bm)2)
/**
* @def LVNV_MICROPHONE_CONTROL_FSBNOISEREF
* Use this microphone FSB noise reference as input to the GSC
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_FSBNOISEREF ((LVNV_MicrophoneControlWord_bm)8)
/**
* @def LVNV_MICROPHONE_CONTROL_GSCINPUT
* Use this microphone as GSC speech reference input (only supported for primary microphone)
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_GSCINPUT ((LVNV_MicrophoneControlWord_bm)16)
/**
* @def LVNV_MICROPHONE_CONTROL_SDET
* Use this microphone in speech detector
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_SDET ((LVNV_MicrophoneControlWord_bm)64)
/**
* @def LVNV_MICROPHONE_CONTROL_GAINETA
* Use this channel's echo estimate in the GainEta mechanism.
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_GAINETA ((LVNV_MicrophoneControlWord_bm)512)
/**
* @def LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW
* Order of preference when microphone(s) are covered. (LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH,LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW)=preference when microphone is covered. Higher value means higher preference
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW ((LVNV_MicrophoneControlWord_bm)4096)
/**
* @def LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH
* Order of preference when microphone(s) are covered. (LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH,LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_LOW)=preference when microphone is covered. Higher value means higher preference
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_MICCOV_PREFERENCE_HIGH ((LVNV_MicrophoneControlWord_bm)8192)
/**
* @def LVNV_MICROPHONE_CONTROL_AEC_LOWCOST
* Enable low-cost AEC for this microphone. It is strongly adviced NOT to enable this for primary microphone.
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONE_CONTROL_AEC_LOWCOST ((LVNV_MicrophoneControlWord_bm)16384)
/**
* @def LVNV_MICROPHONECONTROLWORD_BM_MIN
* Minimal value for LVNV_MicrophoneControlWord_bm
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONECONTROLWORD_BM_MIN ((LVNV_MicrophoneControlWord_bm)0)
/**
* @def LVNV_MICROPHONECONTROLWORD_BM_MAX
* Maximal value for LVNV_MicrophoneControlWord_bm
* @see LVNV_MicrophoneControlWord_bm
*/
#define LVNV_MICROPHONECONTROLWORD_BM_MAX ((LVNV_MicrophoneControlWord_bm)((1) | (2) | (8) | (16) | (64) | (512) | (4096) | (8192) | (16384) | (0)))
/**
<table border>
    <tr>
        <td><b>Name</b></td>
        <td><b>Value</b></td>
        <td><b>Description</b></td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_DNNS_ACTIVITY</td>
        <td>1</td>
        <td>DNNS activity flag</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_MICPOWABOVEINST</td>
        <td>2</td>
        <td>MicPow above instrumental noise detection</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_CALREADY</td>
        <td>4</td>
        <td>Flag to indicate Calibration is ready</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_PCD</td>
        <td>8</td>
        <td>Flag to indicate path change detection</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_AEC_TDFARSPEAKS</td>
        <td>16</td>
        <td>Flag to indicate farend speech detection</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_MONAURALDET</td>
        <td>32</td>
        <td>Flag to indicate miccov detection in handset config or monaural detection in stereo headset config</td>
    </tr>
    <tr>
        <td>@ref LVNV_STATUS_MONAURALDET_MIC</td>
        <td>64</td>
        <td>Flag to indicate which microphone is covered</td>
    </tr>
</table>
 */
typedef LVM_UINT16 LVNV_StatusWord_bm;
/**
* @def LVNV_STATUS_DNNS_ACTIVITY
* DNNS activity flag
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_DNNS_ACTIVITY ((LVNV_StatusWord_bm)1)
/**
* @def LVNV_STATUS_MICPOWABOVEINST
* MicPow above instrumental noise detection
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_MICPOWABOVEINST ((LVNV_StatusWord_bm)2)
/**
* @def LVNV_STATUS_CALREADY
* Flag to indicate Calibration is ready
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_CALREADY ((LVNV_StatusWord_bm)4)
/**
* @def LVNV_STATUS_PCD
* Flag to indicate path change detection
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_PCD ((LVNV_StatusWord_bm)8)
/**
* @def LVNV_STATUS_AEC_TDFARSPEAKS
* Flag to indicate farend speech detection
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_AEC_TDFARSPEAKS ((LVNV_StatusWord_bm)16)
/**
* @def LVNV_STATUS_MONAURALDET
* Flag to indicate miccov detection in handset config or monaural detection in stereo headset config
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_MONAURALDET ((LVNV_StatusWord_bm)32)
/**
* @def LVNV_STATUS_MONAURALDET_MIC
* Flag to indicate which microphone is covered
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUS_MONAURALDET_MIC ((LVNV_StatusWord_bm)64)
/**
* @def LVNV_STATUSWORD_BM_MIN
* Minimal value for LVNV_StatusWord_bm
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUSWORD_BM_MIN ((LVNV_StatusWord_bm)0)
/**
* @def LVNV_STATUSWORD_BM_MAX
* Maximal value for LVNV_StatusWord_bm
* @see LVNV_StatusWord_bm
*/
#define LVNV_STATUSWORD_BM_MAX ((LVNV_StatusWord_bm)((1) | (2) | (4) | (8) | (16) | (32) | (64) | (0)))

#define LVVIDHEADER_LVVE_RX_MASK1_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Rx_Mask1_bm in LVWireFormat.

#define LVVIDHEADER_LVVE_RX_MASK2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Rx_Mask2_bm in LVWireFormat.

#define LVVIDHEADER_LVVE_TX_MASK1_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Tx_Mask1_bm in LVWireFormat.

#define LVVIDHEADER_LVVE_TX_MASK2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_LVVE_Tx_Mask2_bm in LVWireFormat.

#define LVVIDHEADER_CONFIGURATIONS_MASK1_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_Configurations_Mask1_bm in LVWireFormat.

#define LVVIDHEADER_CONFIGURATIONS_MASK2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVVIDHeader_Configurations_Mask2_bm in LVWireFormat.

#define LVAVC_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVAVC_ModeWord_bm in LVWireFormat.

#define LVFENS_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVFENS_ModeWord_bm in LVWireFormat.

#define LVWM_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVWM_ModeWord_bm in LVWireFormat.

#define LVNV_MODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_ModeWord_bm in LVWireFormat.

#define LVNV_MODEWORD2_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_ModeWord2_bm in LVWireFormat.

#define LVNV_MODEWORD3_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_ModeWord3_bm in LVWireFormat.

#define LVNV_TUNINGMODEWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_TuningModeWord_bm in LVWireFormat.

#define LVNV_MICROPHONECONTROLWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_MicrophoneControlWord_bm in LVWireFormat.

#define LVNV_STATUSWORD_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode @ref LVNV_StatusWord_bm in LVWireFormat.

#define LVM_MODE_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Mode_en in LVWireFormat.

#define LVM_CONFIG_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Config_en in LVWireFormat.

#define LVM_CHANNELTYPE_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_ChannelType_en in LVWireFormat.

#define LVM_VOLUMEDEPENDENCE_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_VolumeDependence_en in LVWireFormat.

#define LVM_FS_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Fs_en in LVWireFormat.

#define LVM_TUNABILITY_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVM_Tunability_en in LVWireFormat.

#define LVVIDHEADER_MESSAGEID_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVIDHeader_MessageID_en in LVWireFormat.

#define LVVIDHEADER_RETURNSTATUS_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVIDHeader_ReturnStatus_en in LVWireFormat.

#define LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVBuffer_Channel_en in LVWireFormat.

#define LVVE_TX_REFERENCECHANNEL_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVE_Tx_ReferenceChannel_en in LVWireFormat.

#define LVVE_TX_OUTPUTCHANNEL_LVWIREFORMAT_LENGTH (4) ///< Number of bytes to encode @ref LVVE_Tx_OutputChannel_en in LVWireFormat.

/// List of the volume dependence flag of each parameter in LVVIDHeader ControlParameters.
#define LVVIDHEADER_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVINPUTMIXER ControlParameters.
#define LVINPUTMIXER_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVAVC ControlParameters.
#define LVAVC_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, \
    LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, \
    LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT

/// List of the volume dependence flag of each parameter in LVFENS ControlParameters.
#define LVFENS_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVWM ControlParameters.
#define LVWM_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT

/// List of the volume dependence flag of each parameter in LVDRC ControlParameters.
#define LVDRC_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, \
    LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT

/// List of the volume dependence flag of each parameter in LVCNG ControlParameters.
#define LVCNG_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_DEPENDENT

/// List of the volume dependence flag of each parameter in LVNG ControlParameters.
#define LVNG_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVNF ControlParameters.
#define LVNF_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVEQ ControlParameters.
#define LVEQ_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVVOL ControlParameters.
#define LVVOL_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT

/// List of the volume dependence flag of each parameter in LVHPF ControlParameters.
#define LVHPF_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT

/// List of the volume dependence flag of each parameter in LVMUTE ControlParameters.
#define LVMUTE_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVVE_Rx ControlParameters.
#define LVVE_RX_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVINPUTMIXER_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVFENS_CONTROLPARAMS_VOLUME_DEPENDENCES, LVAVC_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT, LVEQ_CONTROLPARAMS_VOLUME_DEPENDENCES, LVDRC_CONTROLPARAMS_VOLUME_DEPENDENCES, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVM_VOLUME_INDEPENDENT, LVCNG_CONTROLPARAMS_VOLUME_DEPENDENCES, LVWM_CONTROLPARAMS_VOLUME_DEPENDENCES, LVNG_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT, LVNF_CONTROLPARAMS_VOLUME_DEPENDENCES

/// List of the volume dependence flag of each parameter in AcousticEchoCanceller ControlParameters.
#define ACOUSTICECHOCANCELLER_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVNV_FsbChannel ControlParameters.
#define LVNV_FSBCHANNEL_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in Beamformer ControlParameters.
#define BEAMFORMER_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVNV_FSBCHANNEL_CONTROLPARAMS_VOLUME_DEPENDENCES, LVNV_FSBCHANNEL_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in EchoSuppression ControlParameters.
#define ECHOSUPPRESSION_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in GeneralSidelobeCanceller ControlParameters.
#define GENERALSIDELOBECANCELLER_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in NoiseSuppression ControlParameters.
#define NOISESUPPRESSION_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in WindNoiseSuppression ControlParameters.
#define WINDNOISESUPPRESSION_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in PathChangeDetector ControlParameters.
#define PATHCHANGEDETECTOR_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in SpeechDetector ControlParameters.
#define SPEECHDETECTOR_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVNV_RsvParameters ControlParameters.
#define LVNV_RSVPARAMETERS_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in Pbfdaf ControlParameters.
#define PBFDAF_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in AntiHowling ControlParameters.
#define ANTIHOWLING_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVNV ControlParameters.
#define LVNV_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, \
    ACOUSTICECHOCANCELLER_CONTROLPARAMS_VOLUME_DEPENDENCES, ACOUSTICECHOCANCELLER_CONTROLPARAMS_VOLUME_DEPENDENCES, SPEECHDETECTOR_CONTROLPARAMS_VOLUME_DEPENDENCES, BEAMFORMER_CONTROLPARAMS_VOLUME_DEPENDENCES, GENERALSIDELOBECANCELLER_CONTROLPARAMS_VOLUME_DEPENDENCES, ECHOSUPPRESSION_CONTROLPARAMS_VOLUME_DEPENDENCES, NOISESUPPRESSION_CONTROLPARAMS_VOLUME_DEPENDENCES, WINDNOISESUPPRESSION_CONTROLPARAMS_VOLUME_DEPENDENCES, PATHCHANGEDETECTOR_CONTROLPARAMS_VOLUME_DEPENDENCES, PBFDAF_CONTROLPARAMS_VOLUME_DEPENDENCES, \
    ANTIHOWLING_CONTROLPARAMS_VOLUME_DEPENDENCES, LVNV_RSVPARAMETERS_CONTROLPARAMS_VOLUME_DEPENDENCES

/// List of the volume dependence flag of each parameter in LVBD ControlParameters.
#define LVBD_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVINPUTROUTING_Tx ControlParameters.
#define LVINPUTROUTING_TX_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVREFERENCEROUTING_Tx ControlParameters.
#define LVREFERENCEROUTING_TX_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVOUTPUTROUTING_Tx ControlParameters.
#define LVOUTPUTROUTING_TX_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVLPF ControlParameters.
#define LVLPF_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVHPFREF ControlParameters.
#define LVHPFREF_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT

/// List of the volume dependence flag of each parameter in LVVE_Tx ControlParameters.
#define LVVE_TX_CONTROLPARAMS_VOLUME_DEPENDENCES \
    LVINPUTROUTING_TX_CONTROLPARAMS_VOLUME_DEPENDENCES, LVREFERENCEROUTING_TX_CONTROLPARAMS_VOLUME_DEPENDENCES, LVOUTPUTROUTING_TX_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, \
    LVM_VOLUME_INDEPENDENT, LVM_VOLUME_DEPENDENT, LVHPFREF_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT, LVM_VOLUME_INDEPENDENT, LVNV_CONTROLPARAMS_VOLUME_DEPENDENCES, LVWM_CONTROLPARAMS_VOLUME_DEPENDENCES, LVM_VOLUME_INDEPENDENT, LVEQ_CONTROLPARAMS_VOLUME_DEPENDENCES, LVDRC_CONTROLPARAMS_VOLUME_DEPENDENCES, \
    LVM_VOLUME_INDEPENDENT, LVCNG_CONTROLPARAMS_VOLUME_DEPENDENCES

/// List of the tunability flag of each parameter in LVVIDHeader ControlParameters.
#define LVVIDHEADER_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVINPUTMIXER ControlParameters.
#define LVINPUTMIXER_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVAVC ControlParameters.
#define LVAVC_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVFENS ControlParameters.
#define LVFENS_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVWM ControlParameters.
#define LVWM_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVDRC ControlParameters.
#define LVDRC_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVCNG ControlParameters.
#define LVCNG_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVNG ControlParameters.
#define LVNG_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVNF ControlParameters.
#define LVNF_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVEQ ControlParameters.
#define LVEQ_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVVOL ControlParameters.
#define LVVOL_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVHPF ControlParameters.
#define LVHPF_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVMUTE ControlParameters.
#define LVMUTE_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVVE_Rx ControlParameters.
#define LVVE_RX_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVINPUTMIXER_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVFENS_CONTROLPARAMS_TUNABILITIES, LVAVC_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC, LVEQ_CONTROLPARAMS_TUNABILITIES, LVDRC_CONTROLPARAMS_TUNABILITIES, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVCNG_CONTROLPARAMS_TUNABILITIES, LVWM_CONTROLPARAMS_TUNABILITIES, LVNG_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC, LVNF_CONTROLPARAMS_TUNABILITIES

/// List of the tunability flag of each parameter in AcousticEchoCanceller ControlParameters.
#define ACOUSTICECHOCANCELLER_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in LVNV_FsbChannel ControlParameters.
#define LVNV_FSBCHANNEL_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in Beamformer ControlParameters.
#define BEAMFORMER_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVNV_FSBCHANNEL_CONTROLPARAMS_TUNABILITIES, LVNV_FSBCHANNEL_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in EchoSuppression ControlParameters.
#define ECHOSUPPRESSION_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in GeneralSidelobeCanceller ControlParameters.
#define GENERALSIDELOBECANCELLER_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in NoiseSuppression ControlParameters.
#define NOISESUPPRESSION_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in WindNoiseSuppression ControlParameters.
#define WINDNOISESUPPRESSION_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in PathChangeDetector ControlParameters.
#define PATHCHANGEDETECTOR_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in SpeechDetector ControlParameters.
#define SPEECHDETECTOR_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in LVNV_RsvParameters ControlParameters.
#define LVNV_RSVPARAMETERS_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, \
    LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_RESERVED, \
    LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, \
    LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED, LVM_TUNABILITY_RESERVED

/// List of the tunability flag of each parameter in Pbfdaf ControlParameters.
#define PBFDAF_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in AntiHowling ControlParameters.
#define ANTIHOWLING_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED

/// List of the tunability flag of each parameter in LVNV ControlParameters.
#define LVNV_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_ADVANCED, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_ADVANCED, \
    ACOUSTICECHOCANCELLER_CONTROLPARAMS_TUNABILITIES, ACOUSTICECHOCANCELLER_CONTROLPARAMS_TUNABILITIES, SPEECHDETECTOR_CONTROLPARAMS_TUNABILITIES, BEAMFORMER_CONTROLPARAMS_TUNABILITIES, GENERALSIDELOBECANCELLER_CONTROLPARAMS_TUNABILITIES, ECHOSUPPRESSION_CONTROLPARAMS_TUNABILITIES, NOISESUPPRESSION_CONTROLPARAMS_TUNABILITIES, WINDNOISESUPPRESSION_CONTROLPARAMS_TUNABILITIES, PATHCHANGEDETECTOR_CONTROLPARAMS_TUNABILITIES, PBFDAF_CONTROLPARAMS_TUNABILITIES, \
    ANTIHOWLING_CONTROLPARAMS_TUNABILITIES, LVNV_RSVPARAMETERS_CONTROLPARAMS_TUNABILITIES

/// List of the tunability flag of each parameter in LVBD ControlParameters.
#define LVBD_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVINPUTROUTING_Tx ControlParameters.
#define LVINPUTROUTING_TX_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVREFERENCEROUTING_Tx ControlParameters.
#define LVREFERENCEROUTING_TX_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVOUTPUTROUTING_Tx ControlParameters.
#define LVOUTPUTROUTING_TX_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVLPF ControlParameters.
#define LVLPF_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVHPFREF ControlParameters.
#define LVHPFREF_CONTROLPARAMS_TUNABILITIES \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC

/// List of the tunability flag of each parameter in LVVE_Tx ControlParameters.
#define LVVE_TX_CONTROLPARAMS_TUNABILITIES \
    LVINPUTROUTING_TX_CONTROLPARAMS_TUNABILITIES, LVREFERENCEROUTING_TX_CONTROLPARAMS_TUNABILITIES, LVOUTPUTROUTING_TX_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, \
    LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVHPFREF_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC, LVM_TUNABILITY_BASIC, LVNV_CONTROLPARAMS_TUNABILITIES, LVWM_CONTROLPARAMS_TUNABILITIES, LVM_TUNABILITY_BASIC, LVEQ_CONTROLPARAMS_TUNABILITIES, LVDRC_CONTROLPARAMS_TUNABILITIES, \
    LVM_TUNABILITY_BASIC, LVCNG_CONTROLPARAMS_TUNABILITIES

/// List of the LVWireFormat length of each parameter in LVVIDHeader ControlParameters.
#define LVVIDHEADER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 1, 1, 1, 1, LVVIDHEADER_LVVE_RX_MASK1_LVWIREFORMAT_LENGTH, LVVIDHEADER_LVVE_RX_MASK2_LVWIREFORMAT_LENGTH, LVVIDHEADER_LVVE_TX_MASK1_LVWIREFORMAT_LENGTH, LVVIDHEADER_LVVE_TX_MASK2_LVWIREFORMAT_LENGTH, LVVIDHEADER_CONFIGURATIONS_MASK1_LVWIREFORMAT_LENGTH, LVVIDHEADER_CONFIGURATIONS_MASK2_LVWIREFORMAT_LENGTH, LVVIDHEADER_MESSAGEID_LVWIREFORMAT_LENGTH, LVM_FS_LVWIREFORMAT_LENGTH, 1, 1

/// List of the LVWireFormat length of each parameter in LVINPUTMIXER ControlParameters.
#define LVINPUTMIXER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2

/// List of the LVWireFormat length of each parameter in LVAVC ControlParameters.
#define LVAVC_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, LVAVC_MODEWORD_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVFENS ControlParameters.
#define LVFENS_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, LVFENS_MODEWORD_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVWM ControlParameters.
#define LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, LVWM_MODEWORD_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVDRC ControlParameters.
#define LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, LVM_MODE_LVWIREFORMAT_LENGTH, 2

/// List of the LVWireFormat length of each parameter in LVCNG ControlParameters.
#define LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2

/// List of the LVWireFormat length of each parameter in LVNG ControlParameters.
#define LVNG_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVNF ControlParameters.
#define LVNF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2

/// List of the LVWireFormat length of each parameter in LVEQ ControlParameters.
#define LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVVOL ControlParameters.
#define LVVOL_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2

/// List of the LVWireFormat length of each parameter in LVHPF ControlParameters.
#define LVHPF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2

/// List of the LVWireFormat length of each parameter in LVMUTE ControlParameters.
#define LVMUTE_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH

/// List of the LVWireFormat length of each parameter in LVVE_Rx ControlParameters.
#define LVVE_RX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, LVINPUTMIXER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, LVM_MODE_LVWIREFORMAT_LENGTH, 2, LVFENS_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVAVC_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, 2, LVM_MODE_LVWIREFORMAT_LENGTH, LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVNG_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, LVNF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS

/// List of the LVWireFormat length of each parameter in AcousticEchoCanceller ControlParameters.
#define ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2

/// List of the LVWireFormat length of each parameter in LVNV_FsbChannel ControlParameters.
#define LVNV_FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in Beamformer ControlParameters.
#define BEAMFORMER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, LVNV_FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVNV_FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, 2

/// List of the LVWireFormat length of each parameter in EchoSuppression ControlParameters.
#define ECHOSUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in GeneralSidelobeCanceller ControlParameters.
#define GENERALSIDELOBECANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2

/// List of the LVWireFormat length of each parameter in NoiseSuppression ControlParameters.
#define NOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in WindNoiseSuppression ControlParameters.
#define WINDNOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in PathChangeDetector ControlParameters.
#define PATHCHANGEDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in SpeechDetector ControlParameters.
#define SPEECHDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVNV_RsvParameters ControlParameters.
#define LVNV_RSVPARAMETERS_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in Pbfdaf ControlParameters.
#define PBFDAF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in AntiHowling ControlParameters.
#define ANTIHOWLING_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVNV ControlParameters.
#define LVNV_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, LVM_CONFIG_LVWIREFORMAT_LENGTH, LVNV_MODEWORD_LVWIREFORMAT_LENGTH, LVNV_MODEWORD2_LVWIREFORMAT_LENGTH, LVNV_MODEWORD3_LVWIREFORMAT_LENGTH, LVNV_TUNINGMODEWORD_LVWIREFORMAT_LENGTH, LVNV_MICROPHONECONTROLWORD_LVWIREFORMAT_LENGTH, LVNV_MICROPHONECONTROLWORD_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, LVM_CHANNELTYPE_LVWIREFORMAT_LENGTH, LVM_CHANNELTYPE_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, SPEECHDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, BEAMFORMER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, GENERALSIDELOBECANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, ECHOSUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, NOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, WINDNOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, PATHCHANGEDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, PBFDAF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, ANTIHOWLING_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVNV_RSVPARAMETERS_CONTROLPARAMS_LVWIREFORMAT_LENGTHS

/// List of the LVWireFormat length of each parameter in LVBD ControlParameters.
#define LVBD_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2, 2

/// List of the LVWireFormat length of each parameter in LVINPUTROUTING_Tx ControlParameters.
#define LVINPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH, LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH, LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH, LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH

/// List of the LVWireFormat length of each parameter in LVREFERENCEROUTING_Tx ControlParameters.
#define LVREFERENCEROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVVE_TX_REFERENCECHANNEL_LVWIREFORMAT_LENGTH, LVVE_TX_REFERENCECHANNEL_LVWIREFORMAT_LENGTH

/// List of the LVWireFormat length of each parameter in LVOUTPUTROUTING_Tx ControlParameters.
#define LVOUTPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVVE_TX_OUTPUTCHANNEL_LVWIREFORMAT_LENGTH

/// List of the LVWireFormat length of each parameter in LVLPF ControlParameters.
#define LVLPF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2

/// List of the LVWireFormat length of each parameter in LVHPFREF ControlParameters.
#define LVHPFREF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVM_MODE_LVWIREFORMAT_LENGTH, 2

/// List of the LVWireFormat length of each parameter in LVVE_Tx ControlParameters.
#define LVVE_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS \
    LVINPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVREFERENCEROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVOUTPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, LVM_MODE_LVWIREFORMAT_LENGTH, LVM_MODE_LVWIREFORMAT_LENGTH, 2, 2, LVM_MODE_LVWIREFORMAT_LENGTH, 2, LVM_MODE_LVWIREFORMAT_LENGTH, 2, LVHPFREF_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, 2, LVNV_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTHS, LVM_MODE_LVWIREFORMAT_LENGTH, LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTHS

/// List of the LVWireFormat length of each parameter in LVAVC StatusParameters.
#define LVAVC_STATUSPARAMS_LVWIREFORMAT_LENGTHS \
    LVAVC_MODEWORD_LVWIREFORMAT_LENGTH, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2

/// List of the LVWireFormat length of each parameter in LVWM StatusParameters.
#define LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTHS \
    2

/// List of the LVWireFormat length of each parameter in LVVE_Rx StatusParameters.
#define LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTHS \
    LVAVC_STATUSPARAMS_LVWIREFORMAT_LENGTHS, LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTHS, 1

/// List of the LVWireFormat length of each parameter in Microphone StatusParameters.
#define MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTHS \
    4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4

/// List of the LVWireFormat length of each parameter in LVNV StatusParameters.
#define LVNV_STATUSPARAMS_LVWIREFORMAT_LENGTHS \
    LVNV_STATUSWORD_LVWIREFORMAT_LENGTH, 2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTHS, MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTHS, 4

/// List of the LVWireFormat length of each parameter in LVVE_Tx StatusParameters.
#define LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTHS \
    LVNV_STATUSPARAMS_LVWIREFORMAT_LENGTHS, LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTHS, 1

#define LVVIDHEADER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (8 \
 + LVVIDHEADER_LVVE_RX_MASK1_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_LVVE_RX_MASK2_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_LVVE_TX_MASK1_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_LVVE_TX_MASK2_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_CONFIGURATIONS_MASK1_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_CONFIGURATIONS_MASK2_LVWIREFORMAT_LENGTH \
 + LVVIDHEADER_MESSAGEID_LVWIREFORMAT_LENGTH \
 + LVM_FS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVIDHeader ControlParameters in LVWireFormat.

#define LVINPUTMIXER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode LVINPUTMIXER ControlParameters in LVWireFormat.

#define LVAVC_CONTROLPARAMS_LVWIREFORMAT_LENGTH (44 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVAVC_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVAVC ControlParameters in LVWireFormat.

#define LVFENS_CONTROLPARAMS_LVWIREFORMAT_LENGTH (16 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVFENS_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVFENS ControlParameters in LVWireFormat.

#define LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTH (14 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVWM_MODEWORD_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVWM ControlParameters in LVWireFormat.

#define LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTH (8 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + 2*5 \
 + 2*5 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVDRC ControlParameters in LVWireFormat.

#define LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode LVCNG ControlParameters in LVWireFormat.

#define LVNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH (6 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + 2*5 \
 + 2*5) ///< Number of bytes to encode LVNG ControlParameters in LVWireFormat.

#define LVNF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (6) ///< Number of bytes to encode LVNF ControlParameters in LVWireFormat.

#define LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + 2*192) ///< Number of bytes to encode LVEQ ControlParameters in LVWireFormat.

#define LVVOL_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVOL ControlParameters in LVWireFormat.

#define LVHPF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVHPF ControlParameters in LVWireFormat.

#define LVMUTE_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVMUTE ControlParameters in LVWireFormat.

#define LVVE_RX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVINPUTMIXER_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVFENS_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVAVC_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVNF_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Rx ControlParameters in LVWireFormat.

#define ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode AcousticEchoCanceller ControlParameters in LVWireFormat.

#define LVNV_FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2*8 \
 + 2*8) ///< Number of bytes to encode LVNV_FsbChannel ControlParameters in LVWireFormat.

#define BEAMFORMER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4 \
 + LVNV_FSBCHANNEL_CONTROLPARAMS_LVWIREFORMAT_LENGTH*2) ///< Number of bytes to encode Beamformer ControlParameters in LVWireFormat.

#define ECHOSUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH (84) ///< Number of bytes to encode EchoSuppression ControlParameters in LVWireFormat.

#define GENERALSIDELOBECANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH (6) ///< Number of bytes to encode GeneralSidelobeCanceller ControlParameters in LVWireFormat.

#define NOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH (72 \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*3) ///< Number of bytes to encode NoiseSuppression ControlParameters in LVWireFormat.

#define WINDNOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH (22 \
 + 2*2 \
 + 2*2) ///< Number of bytes to encode WindNoiseSuppression ControlParameters in LVWireFormat.

#define PATHCHANGEDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH (12) ///< Number of bytes to encode PathChangeDetector ControlParameters in LVWireFormat.

#define SPEECHDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH (28 \
 + 2*2) ///< Number of bytes to encode SpeechDetector ControlParameters in LVWireFormat.

#define LVNV_RSVPARAMETERS_CONTROLPARAMS_LVWIREFORMAT_LENGTH (70) ///< Number of bytes to encode LVNV_RsvParameters ControlParameters in LVWireFormat.

#define PBFDAF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (12 \
 + 2*2 \
 + 2*2 \
 + 2*2 \
 + 2*2 \
 + 2*2 \
 + 2*2 \
 + 2*2) ///< Number of bytes to encode Pbfdaf ControlParameters in LVWireFormat.

#define ANTIHOWLING_CONTROLPARAMS_LVWIREFORMAT_LENGTH (28) ///< Number of bytes to encode AntiHowling ControlParameters in LVWireFormat.

#define LVNV_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4 \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_CONFIG_LVWIREFORMAT_LENGTH \
 + LVNV_MODEWORD_LVWIREFORMAT_LENGTH \
 + LVNV_MODEWORD2_LVWIREFORMAT_LENGTH \
 + LVNV_MODEWORD3_LVWIREFORMAT_LENGTH \
 + LVNV_TUNINGMODEWORD_LVWIREFORMAT_LENGTH \
 + LVNV_MICROPHONECONTROLWORD_LVWIREFORMAT_LENGTH*2 \
 + 2*2 \
 + 2*2 \
 + 2*2 \
 + LVM_CHANNELTYPE_LVWIREFORMAT_LENGTH*2 \
 + 2*2 \
 + ACOUSTICECHOCANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH*2 \
 + SPEECHDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + BEAMFORMER_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + GENERALSIDELOBECANCELLER_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + ECHOSUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + NOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + WINDNOISESUPPRESSION_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + PATHCHANGEDETECTOR_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + PBFDAF_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + ANTIHOWLING_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVNV_RSVPARAMETERS_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVNV ControlParameters in LVWireFormat.

#define LVBD_CONTROLPARAMS_LVWIREFORMAT_LENGTH (4 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVBD ControlParameters in LVWireFormat.

#define LVINPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH \
 + LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH \
 + LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH \
 + LVBUFFER_CHANNEL_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVINPUTROUTING_Tx ControlParameters in LVWireFormat.

#define LVREFERENCEROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVVE_TX_REFERENCECHANNEL_LVWIREFORMAT_LENGTH \
 + LVVE_TX_REFERENCECHANNEL_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVREFERENCEROUTING_Tx ControlParameters in LVWireFormat.

#define LVOUTPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (LVVE_TX_OUTPUTCHANNEL_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVOUTPUTROUTING_Tx ControlParameters in LVWireFormat.

#define LVLPF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVLPF ControlParameters in LVWireFormat.

#define LVHPFREF_CONTROLPARAMS_LVWIREFORMAT_LENGTH (2 \
 + LVM_MODE_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVHPFREF ControlParameters in LVWireFormat.

#define LVVE_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH (10 \
 + LVINPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVREFERENCEROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVOUTPUTROUTING_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVHPFREF_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVNV_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVEQ_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVDRC_CONTROLPARAMS_LVWIREFORMAT_LENGTH \
 + LVM_MODE_LVWIREFORMAT_LENGTH \
 + LVCNG_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Tx ControlParameters in LVWireFormat.

#define LVAVC_STATUSPARAMS_LVWIREFORMAT_LENGTH (6 \
 + LVAVC_MODEWORD_LVWIREFORMAT_LENGTH \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*3 \
 + 2*8 \
 + 2*8) ///< Number of bytes to encode LVAVC StatusParameters in LVWireFormat.

#define LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTH (2) ///< Number of bytes to encode LVWM StatusParameters in LVWireFormat.

#define LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTH (1 \
 + LVAVC_STATUSPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Rx StatusParameters in LVWireFormat.

#define MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTH (8 \
 + 4*128 \
 + 4*128 \
 + 2*24) ///< Number of bytes to encode Microphone StatusParameters in LVWireFormat.

#define LVNV_STATUSPARAMS_LVWIREFORMAT_LENGTH (22 \
 + LVNV_STATUSWORD_LVWIREFORMAT_LENGTH \
 + 4*64 \
 + 2*2 \
 + 2*2 \
 + MICROPHONE_STATUSPARAMS_LVWIREFORMAT_LENGTH*2) ///< Number of bytes to encode LVNV StatusParameters in LVWireFormat.

#define LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTH (1 \
 + LVNV_STATUSPARAMS_LVWIREFORMAT_LENGTH \
 + LVWM_STATUSPARAMS_LVWIREFORMAT_LENGTH) ///< Number of bytes to encode LVVE_Tx StatusParameters in LVWireFormat.

/**
* @def LVVIDHEADER_HEADERVERSION_DEFAULT
* Default of HeaderVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_HEADERVERSION_DEFAULT (4)
/**
* @def LVVIDHEADER_HEADERVERSION_MIN
* MinValue of HeaderVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_HEADERVERSION_MIN (0)
/**
* @def LVVIDHEADER_HEADERVERSION_MAX
* MaxValue of HeaderVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_HEADERVERSION_MAX (32767)

/**
* @def LVVIDHEADER_MAJORBASELINEVERSION_DEFAULT
* Default of MajorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MAJORBASELINEVERSION_DEFAULT (12)
/**
* @def LVVIDHEADER_MAJORBASELINEVERSION_MIN
* MinValue of MajorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MAJORBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_MAJORBASELINEVERSION_MAX
* MaxValue of MajorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MAJORBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_MINORBASELINEVERSION_DEFAULT
* Default of MinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORBASELINEVERSION_DEFAULT (5)
/**
* @def LVVIDHEADER_MINORBASELINEVERSION_MIN
* MinValue of MinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_MINORBASELINEVERSION_MAX
* MaxValue of MinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_MINORMINORBASELINEVERSION_DEFAULT
* Default of MinorMinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORMINORBASELINEVERSION_DEFAULT (1)
/**
* @def LVVIDHEADER_MINORMINORBASELINEVERSION_MIN
* MinValue of MinorMinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORMINORBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_MINORMINORBASELINEVERSION_MAX
* MaxValue of MinorMinorBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MINORMINORBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_PATCHBASELINEVERSION_DEFAULT
* Default of PatchBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_PATCHBASELINEVERSION_DEFAULT (1)
/**
* @def LVVIDHEADER_PATCHBASELINEVERSION_MIN
* MinValue of PatchBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_PATCHBASELINEVERSION_MIN (0)
/**
* @def LVVIDHEADER_PATCHBASELINEVERSION_MAX
* MaxValue of PatchBaselineVersion
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_PATCHBASELINEVERSION_MAX (255)

/**
* @def LVVIDHEADER_LVVE_RX_ALGOMASK1_DEFAULT
* Default of LVVE_Rx_AlgoMask1
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_RX_ALGOMASK1_DEFAULT (LVVIDHEADER_LVVE_RX_MASK1_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_RX_ALGOMASK2_DEFAULT
* Default of LVVE_Rx_AlgoMask2
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_RX_ALGOMASK2_DEFAULT (LVVIDHEADER_LVVE_RX_MASK2_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_TX_ALGOMASK1_DEFAULT
* Default of LVVE_Tx_AlgoMask1
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_TX_ALGOMASK1_DEFAULT (LVVIDHEADER_LVVE_TX_MASK1_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_TX_ALGOMASK2_DEFAULT
* Default of LVVE_Tx_AlgoMask2
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_TX_ALGOMASK2_DEFAULT (LVVIDHEADER_LVVE_TX_MASK2_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_CONFIG_ALGOMASK1_DEFAULT
* Default of LVVE_Config_AlgoMask1
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_CONFIG_ALGOMASK1_DEFAULT (LVVIDHEADER_CONFIGURATIONS_MASK1_BM_MAX)

/**
* @def LVVIDHEADER_LVVE_CONFIG_ALGOMASK2_DEFAULT
* Default of LVVE_Config_AlgoMask2
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_LVVE_CONFIG_ALGOMASK2_DEFAULT (LVVIDHEADER_CONFIGURATIONS_MASK2_BM_MAX)

/**
* @def LVVIDHEADER_MESSAGEID_DEFAULT
* Default of MessageID
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_MESSAGEID_DEFAULT (LVVE_RX_PRESET)

/**
* @def LVVIDHEADER_SAMPLERATE_DEFAULT
* Default of SampleRate
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_SAMPLERATE_DEFAULT (LVM_FS_8000)

/**
* @def LVVIDHEADER_VOLUMEINDEX_DEFAULT
* Default of VolumeIndex
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_VOLUMEINDEX_DEFAULT (0)
/**
* @def LVVIDHEADER_VOLUMEINDEX_MIN
* MinValue of VolumeIndex
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_VOLUMEINDEX_MIN (0)
/**
* @def LVVIDHEADER_VOLUMEINDEX_MAX
* MaxValue of VolumeIndex
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_VOLUMEINDEX_MAX (255)

/**
* @def LVVIDHEADER_NUMVOLUMES_DEFAULT
* Default of NumVolumes
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_NUMVOLUMES_DEFAULT (1)
/**
* @def LVVIDHEADER_NUMVOLUMES_MIN
* MinValue of NumVolumes
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_NUMVOLUMES_MIN (0)
/**
* @def LVVIDHEADER_NUMVOLUMES_MAX
* MaxValue of NumVolumes
* @see LVVIDHeader_ControlParams_st
*/
#define LVVIDHEADER_NUMVOLUMES_MAX (255)

/**
* @def LVINPUTMIXER_INPUTGAIN_DEFAULT
* Default of InputGain
* @see LVINPUTMIXER_ControlParams_st
*/
#define LVINPUTMIXER_INPUTGAIN_DEFAULT (0)
/**
* @def LVINPUTMIXER_INPUTGAIN_MIN
* MinValue of InputGain
* @see LVINPUTMIXER_ControlParams_st
*/
#define LVINPUTMIXER_INPUTGAIN_MIN (-96)
/**
* @def LVINPUTMIXER_INPUTGAIN_MAX
* MaxValue of InputGain
* @see LVINPUTMIXER_ControlParams_st
*/
#define LVINPUTMIXER_INPUTGAIN_MAX (24)

/**
* @def LVAVC_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVAVC_MODE_DEFAULT
* Default of Mode
* @see LVAVC_ControlParams_st
*/
#define LVAVC_MODE_DEFAULT (17)

/**
* @def LVAVC_GAIN_DEFAULT
* Default of Gain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_DEFAULT (10)
/**
* @def LVAVC_GAIN_MIN
* MinValue of Gain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_MIN (0)
/**
* @def LVAVC_GAIN_MAX
* MaxValue of Gain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_MAX (30)

/**
* @def LVAVC_NOISEADAPTTH0_DEFAULT
* Default of NoiseAdaptTh0
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH0_DEFAULT (0)
/**
* @def LVAVC_NOISEADAPTTH0_MIN
* MinValue of NoiseAdaptTh0
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH0_MIN (0)
/**
* @def LVAVC_NOISEADAPTTH0_MAX
* MaxValue of NoiseAdaptTh0
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH0_MAX (90)

/**
* @def LVAVC_NOISEADAPTTH1_DEFAULT
* Default of NoiseAdaptTh1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH1_DEFAULT (40)
/**
* @def LVAVC_NOISEADAPTTH1_MIN
* MinValue of NoiseAdaptTh1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH1_MIN (0)
/**
* @def LVAVC_NOISEADAPTTH1_MAX
* MaxValue of NoiseAdaptTh1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH1_MAX (90)

/**
* @def LVAVC_NOISEADAPTTH2_DEFAULT
* Default of NoiseAdaptTh2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH2_DEFAULT (60)
/**
* @def LVAVC_NOISEADAPTTH2_MIN
* MinValue of NoiseAdaptTh2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH2_MIN (0)
/**
* @def LVAVC_NOISEADAPTTH2_MAX
* MaxValue of NoiseAdaptTh2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTTH2_MAX (90)

/**
* @def LVAVC_NASEL_NOISEADAPTGMIN1_DEFAULT
* Default of NASEL_NoiseAdaptGmin1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN1_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN1_MIN
* MinValue of NASEL_NoiseAdaptGmin1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN1_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN1_MAX
* MaxValue of NASEL_NoiseAdaptGmin1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN1_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMIN2_DEFAULT
* Default of NASEL_NoiseAdaptGmin2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN2_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN2_MIN
* MinValue of NASEL_NoiseAdaptGmin2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN2_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN2_MAX
* MaxValue of NASEL_NoiseAdaptGmin2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN2_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMIN3_DEFAULT
* Default of NASEL_NoiseAdaptGmin3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN3_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN3_MIN
* MinValue of NASEL_NoiseAdaptGmin3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN3_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMIN3_MAX
* MaxValue of NASEL_NoiseAdaptGmin3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMIN3_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMAX1_DEFAULT
* Default of NASEL_NoiseAdaptGmax1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX1_DEFAULT (6)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX1_MIN
* MinValue of NASEL_NoiseAdaptGmax1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX1_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX1_MAX
* MaxValue of NASEL_NoiseAdaptGmax1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX1_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMAX2_DEFAULT
* Default of NASEL_NoiseAdaptGmax2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX2_DEFAULT (6)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX2_MIN
* MinValue of NASEL_NoiseAdaptGmax2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX2_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX2_MAX
* MaxValue of NASEL_NoiseAdaptGmax2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX2_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTGMAX3_DEFAULT
* Default of NASEL_NoiseAdaptGmax3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX3_DEFAULT (6)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX3_MIN
* MinValue of NASEL_NoiseAdaptGmax3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX3_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTGMAX3_MAX
* MaxValue of NASEL_NoiseAdaptGmax3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTGMAX3_MAX (30)

/**
* @def LVAVC_NASEL_NOISEADAPTATTACK_DEFAULT
* Default of NASEL_NoiseAdaptAttack
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTATTACK_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTATTACK_MIN
* MinValue of NASEL_NoiseAdaptAttack
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTATTACK_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTATTACK_MAX
* MaxValue of NASEL_NoiseAdaptAttack
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTATTACK_MAX (100)

/**
* @def LVAVC_NASEL_NOISEADAPTRELEASE_DEFAULT
* Default of NASEL_NoiseAdaptRelease
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTRELEASE_DEFAULT (0)
/**
* @def LVAVC_NASEL_NOISEADAPTRELEASE_MIN
* MinValue of NASEL_NoiseAdaptRelease
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTRELEASE_MIN (0)
/**
* @def LVAVC_NASEL_NOISEADAPTRELEASE_MAX
* MaxValue of NASEL_NoiseAdaptRelease
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_NOISEADAPTRELEASE_MAX (100)

/**
* @def LVAVC_NASEL_COMPKNEE1_DEFAULT
* Default of NASEL_CompKnee1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE1_DEFAULT (60)
/**
* @def LVAVC_NASEL_COMPKNEE1_MIN
* MinValue of NASEL_CompKnee1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE1_MIN (0)
/**
* @def LVAVC_NASEL_COMPKNEE1_MAX
* MaxValue of NASEL_CompKnee1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE1_MAX (90)

/**
* @def LVAVC_NASEL_COMPKNEE2_DEFAULT
* Default of NASEL_CompKnee2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE2_DEFAULT (55)
/**
* @def LVAVC_NASEL_COMPKNEE2_MIN
* MinValue of NASEL_CompKnee2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE2_MIN (0)
/**
* @def LVAVC_NASEL_COMPKNEE2_MAX
* MaxValue of NASEL_CompKnee2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE2_MAX (90)

/**
* @def LVAVC_NASEL_COMPKNEE3_DEFAULT
* Default of NASEL_CompKnee3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE3_DEFAULT (50)
/**
* @def LVAVC_NASEL_COMPKNEE3_MIN
* MinValue of NASEL_CompKnee3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE3_MIN (0)
/**
* @def LVAVC_NASEL_COMPKNEE3_MAX
* MaxValue of NASEL_CompKnee3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPKNEE3_MAX (90)

/**
* @def LVAVC_NASEL_COMPRATIO1_DEFAULT
* Default of NASEL_CompRatio1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO1_DEFAULT (32767)
/**
* @def LVAVC_NASEL_COMPRATIO1_MIN
* MinValue of NASEL_CompRatio1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO1_MIN (2)
/**
* @def LVAVC_NASEL_COMPRATIO1_MAX
* MaxValue of NASEL_CompRatio1
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO1_MAX (32767)

/**
* @def LVAVC_NASEL_COMPRATIO2_DEFAULT
* Default of NASEL_CompRatio2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO2_DEFAULT (32767)
/**
* @def LVAVC_NASEL_COMPRATIO2_MIN
* MinValue of NASEL_CompRatio2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO2_MIN (2)
/**
* @def LVAVC_NASEL_COMPRATIO2_MAX
* MaxValue of NASEL_CompRatio2
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO2_MAX (32767)

/**
* @def LVAVC_NASEL_COMPRATIO3_DEFAULT
* Default of NASEL_CompRatio3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO3_DEFAULT (32767)
/**
* @def LVAVC_NASEL_COMPRATIO3_MIN
* MinValue of NASEL_CompRatio3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO3_MIN (2)
/**
* @def LVAVC_NASEL_COMPRATIO3_MAX
* MaxValue of NASEL_CompRatio3
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NASEL_COMPRATIO3_MAX (32767)

/**
* @def LVAVC_FU_MINGAIN_DEFAULT
* Default of FU_MinGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MINGAIN_DEFAULT (0)
/**
* @def LVAVC_FU_MINGAIN_MIN
* MinValue of FU_MinGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MINGAIN_MIN (0)
/**
* @def LVAVC_FU_MINGAIN_MAX
* MaxValue of FU_MinGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MINGAIN_MAX (54)

/**
* @def LVAVC_FU_MAXGAIN_DEFAULT
* Default of FU_MaxGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MAXGAIN_DEFAULT (10)
/**
* @def LVAVC_FU_MAXGAIN_MIN
* MinValue of FU_MaxGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MAXGAIN_MIN (0)
/**
* @def LVAVC_FU_MAXGAIN_MAX
* MaxValue of FU_MaxGain
* @see LVAVC_ControlParams_st
*/
#define LVAVC_FU_MAXGAIN_MAX (54)

/**
* @def LVAVC_LIMITER_THR_DEFAULT
* Default of Limiter_Thr
* @see LVAVC_ControlParams_st
*/
#define LVAVC_LIMITER_THR_DEFAULT (32767)
/**
* @def LVAVC_LIMITER_THR_MIN
* MinValue of Limiter_Thr
* @see LVAVC_ControlParams_st
*/
#define LVAVC_LIMITER_THR_MIN (0)
/**
* @def LVAVC_LIMITER_THR_MAX
* MaxValue of Limiter_Thr
* @see LVAVC_ControlParams_st
*/
#define LVAVC_LIMITER_THR_MAX (32767)

/**
* @def LVAVC_ADVANCED_CONFIG_DEFAULT
* Default of Advanced_Config
* @see LVAVC_ControlParams_st
*/
#define LVAVC_ADVANCED_CONFIG_DEFAULT (0)
/**
* @def LVAVC_ADVANCED_CONFIG_MIN
* MinValue of Advanced_Config
* @see LVAVC_ControlParams_st
*/
#define LVAVC_ADVANCED_CONFIG_MIN (0)
/**
* @def LVAVC_ADVANCED_CONFIG_MAX
* MaxValue of Advanced_Config
* @see LVAVC_ControlParams_st
*/
#define LVAVC_ADVANCED_CONFIG_MAX (32767)

/**
* @def LVFENS_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVFENS_ControlParams_st
*/
#define LVFENS_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVFENS_MODE_DEFAULT
* Default of Mode
* @see LVFENS_ControlParams_st
*/
#define LVFENS_MODE_DEFAULT (1)

/**
* @def LVFENS_FENS_LIMIT_NS_DEFAULT
* Default of FENS_limit_NS
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_LIMIT_NS_DEFAULT (10976)
/**
* @def LVFENS_FENS_LIMIT_NS_MIN
* MinValue of FENS_limit_NS
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_LIMIT_NS_MIN (0)
/**
* @def LVFENS_FENS_LIMIT_NS_MAX
* MaxValue of FENS_limit_NS
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_LIMIT_NS_MAX (32767)

/**
* @def LVFENS_FENS_NOISETHRESHOLD_DEFAULT
* Default of FENS_NoiseThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_NOISETHRESHOLD_DEFAULT (96)
/**
* @def LVFENS_FENS_NOISETHRESHOLD_MIN
* MinValue of FENS_NoiseThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_NOISETHRESHOLD_MIN (0)
/**
* @def LVFENS_FENS_NOISETHRESHOLD_MAX
* MaxValue of FENS_NoiseThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_FENS_NOISETHRESHOLD_MAX (32767)

/**
* @def LVFENS_WNS_AGGRESSIVENESS_DEFAULT
* Default of WNS_Aggressiveness
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_AGGRESSIVENESS_DEFAULT (256)
/**
* @def LVFENS_WNS_AGGRESSIVENESS_MIN
* MinValue of WNS_Aggressiveness
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_AGGRESSIVENESS_MIN (0)
/**
* @def LVFENS_WNS_AGGRESSIVENESS_MAX
* MaxValue of WNS_Aggressiveness
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_AGGRESSIVENESS_MAX (32767)

/**
* @def LVFENS_WNS_POWERSCALEFACTOR_DEFAULT
* Default of WNS_PowerScaleFactor
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_POWERSCALEFACTOR_DEFAULT (0)
/**
* @def LVFENS_WNS_POWERSCALEFACTOR_MIN
* MinValue of WNS_PowerScaleFactor
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_POWERSCALEFACTOR_MIN (-96)
/**
* @def LVFENS_WNS_POWERSCALEFACTOR_MAX
* MaxValue of WNS_PowerScaleFactor
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_POWERSCALEFACTOR_MAX (32)

/**
* @def LVFENS_WNS_MAXATTENUATION_DEFAULT
* Default of WNS_MaxAttenuation
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_MAXATTENUATION_DEFAULT (8231)
/**
* @def LVFENS_WNS_MAXATTENUATION_MIN
* MinValue of WNS_MaxAttenuation
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_MAXATTENUATION_MIN (0)
/**
* @def LVFENS_WNS_MAXATTENUATION_MAX
* MaxValue of WNS_MaxAttenuation
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_MAXATTENUATION_MAX (32767)

/**
* @def LVFENS_WNS_HFSLOPE_DEFAULT
* Default of WNS_HFSlope
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HFSLOPE_DEFAULT (-6)
/**
* @def LVFENS_WNS_HFSLOPE_MIN
* MinValue of WNS_HFSlope
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HFSLOPE_MIN (-20)
/**
* @def LVFENS_WNS_HFSLOPE_MAX
* MaxValue of WNS_HFSlope
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HFSLOPE_MAX (0)

/**
* @def LVFENS_WNS_GUSHTHRESHOLD_DEFAULT
* Default of WNS_GushThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_GUSHTHRESHOLD_DEFAULT (9830)
/**
* @def LVFENS_WNS_GUSHTHRESHOLD_MIN
* MinValue of WNS_GushThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_GUSHTHRESHOLD_MIN (0)
/**
* @def LVFENS_WNS_GUSHTHRESHOLD_MAX
* MaxValue of WNS_GushThreshold
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_GUSHTHRESHOLD_MAX (32767)

/**
* @def LVFENS_WNS_HPFCORNERFREQ_DEFAULT
* Default of WNS_HPFCornerFreq
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HPFCORNERFREQ_DEFAULT (50)
/**
* @def LVFENS_WNS_HPFCORNERFREQ_MIN
* MinValue of WNS_HPFCornerFreq
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HPFCORNERFREQ_MIN (0)
/**
* @def LVFENS_WNS_HPFCORNERFREQ_MAX
* MaxValue of WNS_HPFCornerFreq
* @see LVFENS_ControlParams_st
*/
#define LVFENS_WNS_HPFCORNERFREQ_MAX (1500)

/**
* @def LVWM_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVWM_ControlParams_st
*/
#define LVWM_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVWM_MODE_DEFAULT
* Default of mode
* @see LVWM_ControlParams_st
*/
#define LVWM_MODE_DEFAULT (7)

/**
* @def LVWM_AVL_TARGET_LEVEL_LIN_DEFAULT
* Default of AVL_Target_level_lin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_TARGET_LEVEL_LIN_DEFAULT (16384)
/**
* @def LVWM_AVL_TARGET_LEVEL_LIN_MIN
* MinValue of AVL_Target_level_lin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_TARGET_LEVEL_LIN_MIN (0)
/**
* @def LVWM_AVL_TARGET_LEVEL_LIN_MAX
* MaxValue of AVL_Target_level_lin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_TARGET_LEVEL_LIN_MAX (32767)

/**
* @def LVWM_AVL_MINGAINLIN_DEFAULT
* Default of AVL_MinGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MINGAINLIN_DEFAULT (128)
/**
* @def LVWM_AVL_MINGAINLIN_MIN
* MinValue of AVL_MinGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MINGAINLIN_MIN (0)
/**
* @def LVWM_AVL_MINGAINLIN_MAX
* MaxValue of AVL_MinGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MINGAINLIN_MAX (512)

/**
* @def LVWM_AVL_MAXGAINLIN_DEFAULT
* Default of AVL_MaxGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MAXGAINLIN_DEFAULT (8189)
/**
* @def LVWM_AVL_MAXGAINLIN_MIN
* MinValue of AVL_MaxGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MAXGAINLIN_MIN (512)
/**
* @def LVWM_AVL_MAXGAINLIN_MAX
* MaxValue of AVL_MaxGainLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_MAXGAINLIN_MAX (32767)

/**
* @def LVWM_AVL_ATTACK_DEFAULT
* Default of AVL_Attack
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_ATTACK_DEFAULT (25520)
/**
* @def LVWM_AVL_ATTACK_MIN
* MinValue of AVL_Attack
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_ATTACK_MIN (0)
/**
* @def LVWM_AVL_ATTACK_MAX
* MaxValue of AVL_Attack
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_ATTACK_MAX (32767)

/**
* @def LVWM_AVL_RELEASE_DEFAULT
* Default of AVL_Release
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_RELEASE_DEFAULT (32685)
/**
* @def LVWM_AVL_RELEASE_MIN
* MinValue of AVL_Release
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_RELEASE_MIN (0)
/**
* @def LVWM_AVL_RELEASE_MAX
* MaxValue of AVL_Release
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_RELEASE_MAX (32767)

/**
* @def LVWM_AVL_LIMIT_MAXOUTPUTLIN_DEFAULT
* Default of AVL_Limit_MaxOutputLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_LIMIT_MAXOUTPUTLIN_DEFAULT (23197)
/**
* @def LVWM_AVL_LIMIT_MAXOUTPUTLIN_MIN
* MinValue of AVL_Limit_MaxOutputLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_LIMIT_MAXOUTPUTLIN_MIN (0)
/**
* @def LVWM_AVL_LIMIT_MAXOUTPUTLIN_MAX
* MaxValue of AVL_Limit_MaxOutputLin
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_LIMIT_MAXOUTPUTLIN_MAX (32767)

/**
* @def LVWM_SPDETECT_THRESHOLD_DEFAULT
* Default of SpDetect_Threshold
* @see LVWM_ControlParams_st
*/
#define LVWM_SPDETECT_THRESHOLD_DEFAULT (9216)
/**
* @def LVWM_SPDETECT_THRESHOLD_MIN
* MinValue of SpDetect_Threshold
* @see LVWM_ControlParams_st
*/
#define LVWM_SPDETECT_THRESHOLD_MIN (0)
/**
* @def LVWM_SPDETECT_THRESHOLD_MAX
* MaxValue of SpDetect_Threshold
* @see LVWM_ControlParams_st
*/
#define LVWM_SPDETECT_THRESHOLD_MAX (32767)

/**
* @def LVDRC_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVDRC_ControlParams_st
*/
#define LVDRC_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVDRC_NUMKNEES_DEFAULT
* Default of NumKnees
* @see LVDRC_ControlParams_st
*/
#define LVDRC_NUMKNEES_DEFAULT (5)
/**
* @def LVDRC_NUMKNEES_MIN
* MinValue of NumKnees
* @see LVDRC_ControlParams_st
*/
#define LVDRC_NUMKNEES_MIN (1)
/**
* @def LVDRC_NUMKNEES_MAX
* MaxValue of NumKnees
* @see LVDRC_ControlParams_st
*/
#define LVDRC_NUMKNEES_MAX (5)

/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_DEFAULT
* Default of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-96,-70,-50,-24,0}
/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVDRC_COMPRESSORCURVEINPUTLEVELS_LENGTH
* Length of CompressorCurveInputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEINPUTLEVELS_LENGTH (5)

/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT
* Default of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-70,-38,-14,0}
/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVDRC_COMPRESSORCURVEOUTPUTLEVELS_LENGTH
* Length of CompressorCurveOutputLevels
* @see LVDRC_ControlParams_st
*/
#define LVDRC_COMPRESSORCURVEOUTPUTLEVELS_LENGTH (5)

/**
* @def LVDRC_ATTACKTIME_DEFAULT
* Default of AttackTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_ATTACKTIME_DEFAULT (10)
/**
* @def LVDRC_ATTACKTIME_MIN
* MinValue of AttackTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_ATTACKTIME_MIN (0)
/**
* @def LVDRC_ATTACKTIME_MAX
* MaxValue of AttackTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_ATTACKTIME_MAX (32767)

/**
* @def LVDRC_RELEASETIME_DEFAULT
* Default of ReleaseTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_RELEASETIME_DEFAULT (2500)
/**
* @def LVDRC_RELEASETIME_MIN
* MinValue of ReleaseTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_RELEASETIME_MIN (0)
/**
* @def LVDRC_RELEASETIME_MAX
* MaxValue of ReleaseTime
* @see LVDRC_ControlParams_st
*/
#define LVDRC_RELEASETIME_MAX (32767)

/**
* @def LVDRC_LIMITEROPERATINGMODE_DEFAULT
* Default of LimiterOperatingMode
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITEROPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVDRC_LIMITLEVEL_DEFAULT
* Default of LimitLevel
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITLEVEL_DEFAULT (0)
/**
* @def LVDRC_LIMITLEVEL_MIN
* MinValue of LimitLevel
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITLEVEL_MIN (-96)
/**
* @def LVDRC_LIMITLEVEL_MAX
* MaxValue of LimitLevel
* @see LVDRC_ControlParams_st
*/
#define LVDRC_LIMITLEVEL_MAX (0)

/**
* @def LVCNG_CNG_VOLUME_DEFAULT
* Default of CNG_Volume
* @see LVCNG_ControlParams_st
*/
#define LVCNG_CNG_VOLUME_DEFAULT (-90)
/**
* @def LVCNG_CNG_VOLUME_MIN
* MinValue of CNG_Volume
* @see LVCNG_ControlParams_st
*/
#define LVCNG_CNG_VOLUME_MIN (-96)
/**
* @def LVCNG_CNG_VOLUME_MAX
* MaxValue of CNG_Volume
* @see LVCNG_ControlParams_st
*/
#define LVCNG_CNG_VOLUME_MAX (-60)

/**
* @def LVNG_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVNG_ControlParams_st
*/
#define LVNG_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVNG_NUMKNEES_DEFAULT
* Default of NumKnees
* @see LVNG_ControlParams_st
*/
#define LVNG_NUMKNEES_DEFAULT (5)
/**
* @def LVNG_NUMKNEES_MIN
* MinValue of NumKnees
* @see LVNG_ControlParams_st
*/
#define LVNG_NUMKNEES_MIN (1)
/**
* @def LVNG_NUMKNEES_MAX
* MaxValue of NumKnees
* @see LVNG_ControlParams_st
*/
#define LVNG_NUMKNEES_MAX (5)

/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_DEFAULT
* Default of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-80,-70,-50,-24,0}
/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVNG_COMPRESSORCURVEINPUTLEVELS_LENGTH
* Length of CompressorCurveInputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEINPUTLEVELS_LENGTH (5)

/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT
* Default of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-80,-50,-24,0}
/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN
* MinValue of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}
/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX
* MaxValue of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}
/**
* @def LVNG_COMPRESSORCURVEOUTPUTLEVELS_LENGTH
* Length of CompressorCurveOutputLevels
* @see LVNG_ControlParams_st
*/
#define LVNG_COMPRESSORCURVEOUTPUTLEVELS_LENGTH (5)

/**
* @def LVNG_ATTACKTIME_DEFAULT
* Default of AttackTime
* @see LVNG_ControlParams_st
*/
#define LVNG_ATTACKTIME_DEFAULT (50)
/**
* @def LVNG_ATTACKTIME_MIN
* MinValue of AttackTime
* @see LVNG_ControlParams_st
*/
#define LVNG_ATTACKTIME_MIN (0)
/**
* @def LVNG_ATTACKTIME_MAX
* MaxValue of AttackTime
* @see LVNG_ControlParams_st
*/
#define LVNG_ATTACKTIME_MAX (32767)

/**
* @def LVNG_RELEASETIME_DEFAULT
* Default of ReleaseTime
* @see LVNG_ControlParams_st
*/
#define LVNG_RELEASETIME_DEFAULT (50)
/**
* @def LVNG_RELEASETIME_MIN
* MinValue of ReleaseTime
* @see LVNG_ControlParams_st
*/
#define LVNG_RELEASETIME_MIN (0)
/**
* @def LVNG_RELEASETIME_MAX
* MaxValue of ReleaseTime
* @see LVNG_ControlParams_st
*/
#define LVNG_RELEASETIME_MAX (32767)

/**
* @def LVNF_NF_ATTENUATION_DEFAULT
* Default of NF_Attenuation
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_ATTENUATION_DEFAULT (10)
/**
* @def LVNF_NF_ATTENUATION_MIN
* MinValue of NF_Attenuation
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_ATTENUATION_MIN (0)
/**
* @def LVNF_NF_ATTENUATION_MAX
* MaxValue of NF_Attenuation
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_ATTENUATION_MAX (24)

/**
* @def LVNF_NF_FREQUENCY_DEFAULT
* Default of NF_Frequency
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_FREQUENCY_DEFAULT (800)
/**
* @def LVNF_NF_FREQUENCY_MIN
* MinValue of NF_Frequency
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_FREQUENCY_MIN (400)
/**
* @def LVNF_NF_FREQUENCY_MAX
* MaxValue of NF_Frequency
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_FREQUENCY_MAX (1500)

/**
* @def LVNF_NF_QFACTOR_DEFAULT
* Default of NF_QFactor
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_QFACTOR_DEFAULT (120)
/**
* @def LVNF_NF_QFACTOR_MIN
* MinValue of NF_QFactor
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_QFACTOR_MIN (50)
/**
* @def LVNF_NF_QFACTOR_MAX
* MaxValue of NF_QFactor
* @see LVNF_ControlParams_st
*/
#define LVNF_NF_QFACTOR_MAX (500)

/**
* @def LVEQ_EQ_LENGTH_DEFAULT
* Default of EQ_Length
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_LENGTH_DEFAULT (192)
/**
* @def LVEQ_EQ_LENGTH_MIN
* MinValue of EQ_Length
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_LENGTH_MIN (8)
/**
* @def LVEQ_EQ_LENGTH_MAX
* MaxValue of EQ_Length
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_LENGTH_MAX (192)

/**
* @def LVEQ_EQ_COEFS_DEFAULT
* Default of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_DEFAULT {4096,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVEQ_EQ_COEFS_ARRAYMIN
* MinValue of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def LVEQ_EQ_COEFS_ARRAYMAX
* MaxValue of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVEQ_EQ_COEFS_LENGTH
* Length of EQ_Coefs
* @see LVEQ_ControlParams_st
*/
#define LVEQ_EQ_COEFS_LENGTH (192)

/**
* @def LVVOL_VOL_OPERATINGMODE_DEFAULT
* Default of VOL_OperatingMode
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVOL_VOL_GAIN_DEFAULT
* Default of VOL_Gain
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_GAIN_DEFAULT (0)
/**
* @def LVVOL_VOL_GAIN_MIN
* MinValue of VOL_Gain
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_GAIN_MIN (-96)
/**
* @def LVVOL_VOL_GAIN_MAX
* MaxValue of VOL_Gain
* @see LVVOL_ControlParams_st
*/
#define LVVOL_VOL_GAIN_MAX (24)

/**
* @def LVHPF_HPF_OPERATINGMODE_DEFAULT
* Default of HPF_OperatingMode
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVHPF_HPF_CORNERFREQ_DEFAULT
* Default of HPF_CornerFreq
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVHPF_HPF_CORNERFREQ_MIN
* MinValue of HPF_CornerFreq
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_CORNERFREQ_MIN (50)
/**
* @def LVHPF_HPF_CORNERFREQ_MAX
* MaxValue of HPF_CornerFreq
* @see LVHPF_ControlParams_st
*/
#define LVHPF_HPF_CORNERFREQ_MAX (1500)

/**
* @def LVMUTE_MUTE_DEFAULT
* Default of Mute
* @see LVMUTE_ControlParams_st
*/
#define LVMUTE_MUTE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_OPERATINGMODE_DEFAULT (LVM_MODE_ON)


/**
* @def LVVE_RX_MUTE_DEFAULT
* Default of Mute
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_MUTE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_VOL_OPERATINGMODE_DEFAULT
* Default of VOL_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_VOL_GAIN_DEFAULT
* Default of VOL_Gain
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_GAIN_DEFAULT (0)
/**
* @def LVVE_RX_VOL_GAIN_MIN
* MinValue of VOL_Gain
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_GAIN_MIN (-96)
/**
* @def LVVE_RX_VOL_GAIN_MAX
* MaxValue of VOL_Gain
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_VOL_GAIN_MAX (24)



/**
* @def LVVE_RX_EQ_OPERATINGMODE_DEFAULT
* Default of EQ_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_EQ_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)



/**
* @def LVVE_RX_HPF_OPERATINGMODE_DEFAULT
* Default of HPF_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_RX_HPF_CORNERFREQ_DEFAULT
* Default of HPF_CornerFreq
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVVE_RX_HPF_CORNERFREQ_MIN
* MinValue of HPF_CornerFreq
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_CORNERFREQ_MIN (50)
/**
* @def LVVE_RX_HPF_CORNERFREQ_MAX
* MaxValue of HPF_CornerFreq
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_HPF_CORNERFREQ_MAX (1500)

/**
* @def LVVE_RX_CNG_OPERATINGMODE_DEFAULT
* Default of CNG_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_CNG_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)




/**
* @def LVVE_RX_NF_OPERATINGMODE_DEFAULT
* Default of NF_OperatingMode
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_NF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)


/**
* @def ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_DEFAULT
* Default of MicrophoneGainComp
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_DEFAULT (0)
/**
* @def ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MIN
* MinValue of MicrophoneGainComp
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MIN (-48)
/**
* @def ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MAX
* MaxValue of MicrophoneGainComp
* @see AcousticEchoCanceller_ControlParams_st
*/
#define ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MAX (24)

/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLE_DEFAULT
* Default of FSB_InitTable
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLE_DEFAULT {0,32767,0,0,0,0,0,0}
/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLE_ARRAYMIN
* MinValue of FSB_InitTable
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLE_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLE_ARRAYMAX
* MaxValue of FSB_InitTable
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLE_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLE_LENGTH
* Length of FSB_InitTable
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLE_LENGTH (8)

/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLEHB_DEFAULT
* Default of FSB_InitTableHB
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLEHB_DEFAULT {0,32767,0,0,0,0,0,0}
/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLEHB_ARRAYMIN
* MinValue of FSB_InitTableHB
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLEHB_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLEHB_ARRAYMAX
* MaxValue of FSB_InitTableHB
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLEHB_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVNV_FSBCHANNEL_FSB_INITTABLEHB_LENGTH
* Length of FSB_InitTableHB
* @see LVNV_FsbChannel_ControlParams_st
*/
#define LVNV_FSBCHANNEL_FSB_INITTABLEHB_LENGTH (8)

/**
* @def BEAMFORMER_FSB_TAPS_DEFAULT
* Default of FSB_taps
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSB_TAPS_DEFAULT (16)
/**
* @def BEAMFORMER_FSB_TAPS_MIN
* MinValue of FSB_taps
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSB_TAPS_MIN (8)
/**
* @def BEAMFORMER_FSB_TAPS_MAX
* MaxValue of FSB_taps
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSB_TAPS_MAX (24)

/**
* @def BEAMFORMER_FSBCHANNEL_LENGTH
* Length of FsbChannel
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_FSBCHANNEL_LENGTH (2)

/**
* @def BEAMFORMER_NOISEREFERENCESGAIN_DEFAULT
* Default of NoiseReferencesGain
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_NOISEREFERENCESGAIN_DEFAULT (6)
/**
* @def BEAMFORMER_NOISEREFERENCESGAIN_MIN
* MinValue of NoiseReferencesGain
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_NOISEREFERENCESGAIN_MIN (-24)
/**
* @def BEAMFORMER_NOISEREFERENCESGAIN_MAX
* MaxValue of NoiseReferencesGain
* @see Beamformer_ControlParams_st
*/
#define BEAMFORMER_NOISEREFERENCESGAIN_MAX (24)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_DEFAULT
* Default of DNNS_EchoGammaDetect_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_DEFAULT (281)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_MIN
* MinValue of DNNS_EchoGammaDetect_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_MAX
* MaxValue of DNNS_EchoGammaDetect_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_DEFAULT
* Default of DNNS_EchoGammaHi_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_DEFAULT (450)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MIN
* MinValue of DNNS_EchoGammaHi_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MAX
* MaxValue of DNNS_EchoGammaHi_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_DEFAULT
* Default of DNNS_EchoGammaLo_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MIN
* MinValue of DNNS_EchoGammaLo_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MAX
* MaxValue of DNNS_EchoGammaLo_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_DEFAULT
* Default of DNNS_EchoGammaDt_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MIN
* MinValue of DNNS_EchoGammaDt_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MAX
* MaxValue of DNNS_EchoGammaDt_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_DEFAULT
* Default of DNNS_EchoCutoff_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_DEFAULT (600)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MIN
* MinValue of DNNS_EchoCutoff_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MAX
* MaxValue of DNNS_EchoCutoff_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MAX (4000)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTENLOW_DEFAULT
* Default of DNNS_NlAttenLow
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENLOW_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTENLOW_MIN
* MinValue of DNNS_NlAttenLow
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENLOW_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTENLOW_MAX
* MaxValue of DNNS_NlAttenLow
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENLOW_MAX (2048)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_DEFAULT
* Default of DNNS_EchoGammaDetect_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_DEFAULT (281)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_MIN
* MinValue of DNNS_EchoGammaDetect_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_MAX
* MaxValue of DNNS_EchoGammaDetect_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_DEFAULT
* Default of DNNS_LinEchoFractionNeDetect_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_DEFAULT (6554)
/**
* @def ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_MIN
* MinValue of DNNS_LinEchoFractionNeDetect_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_MAX
* MaxValue of DNNS_LinEchoFractionNeDetect_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_DEFAULT
* Default of DNNS_EchoGammaHi_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_DEFAULT (450)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MIN
* MinValue of DNNS_EchoGammaHi_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MAX
* MaxValue of DNNS_EchoGammaHi_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_DEFAULT
* Default of DNNS_EchoGammaLo_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MIN
* MinValue of DNNS_EchoGammaLo_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MAX
* MaxValue of DNNS_EchoGammaLo_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_DEFAULT
* Default of DNNS_EchoGammaDt_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MIN
* MinValue of DNNS_EchoGammaDt_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MAX
* MaxValue of DNNS_EchoGammaDt_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_DEFAULT
* Default of DNNS_EchoAlphaRev_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_DEFAULT (10000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MIN
* MinValue of DNNS_EchoAlphaRev_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MAX
* MaxValue of DNNS_EchoAlphaRev_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_DEFAULT
* Default of DNNS_EchoTailPortion_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_DEFAULT (8000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MIN
* MinValue of DNNS_EchoTailPortion_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MAX
* MaxValue of DNNS_EchoTailPortion_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_LB_DEFAULT
* Default of DNNS_NlAtten_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_LB_DEFAULT (128)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_LB_MIN
* MinValue of DNNS_NlAtten_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_LB_MAX
* MaxValue of DNNS_NlAtten_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_LB_MAX (2048)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_DEFAULT
* Default of DNNS_EchoGammaDetect_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_MIN
* MinValue of DNNS_EchoGammaDetect_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_MAX
* MaxValue of DNNS_EchoGammaDetect_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_DEFAULT
* Default of DNNS_LinEchoFractionNeDetect_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_DEFAULT (6554)
/**
* @def ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_MIN
* MinValue of DNNS_LinEchoFractionNeDetect_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_MAX
* MaxValue of DNNS_LinEchoFractionNeDetect_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_DEFAULT
* Default of DNNS_EchoGammaHi_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_DEFAULT (300)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MIN
* MinValue of DNNS_EchoGammaHi_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MAX
* MaxValue of DNNS_EchoGammaHi_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_DEFAULT
* Default of DNNS_EchoGammaLo_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MIN
* MinValue of DNNS_EchoGammaLo_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MAX
* MaxValue of DNNS_EchoGammaLo_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_DEFAULT
* Default of DNNS_EchoGammaDt_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MIN
* MinValue of DNNS_EchoGammaDt_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MAX
* MaxValue of DNNS_EchoGammaDt_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_DEFAULT
* Default of DNNS_EchoAlphaRev_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_DEFAULT (12000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MIN
* MinValue of DNNS_EchoAlphaRev_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MAX
* MaxValue of DNNS_EchoAlphaRev_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_DEFAULT
* Default of DNNS_EchoTailPortion_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_DEFAULT (12000)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MIN
* MinValue of DNNS_EchoTailPortion_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MAX
* MaxValue of DNNS_EchoTailPortion_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_HB_DEFAULT
* Default of DNNS_NlAtten_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_HB_DEFAULT (64)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_HB_MIN
* MinValue of DNNS_NlAtten_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTEN_HB_MAX
* MaxValue of DNNS_NlAtten_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTEN_HB_MAX (2048)

/**
* @def ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_DEFAULT
* Default of DNNS_MaxSuppression_EchoTopband
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_MIN
* MinValue of DNNS_MaxSuppression_EchoTopband
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_MAX
* MaxValue of DNNS_MaxSuppression_EchoTopband
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_MAX (48)

/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_DEFAULT
* Default of DNNS_EchoRelax_NEThreshold_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MIN
* MinValue of DNNS_EchoRelax_NEThreshold_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MAX
* MaxValue of DNNS_EchoRelax_NEThreshold_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_DEFAULT
* Default of DNNS_EchoRelax_NEThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_DEFAULT (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MIN
* MinValue of DNNS_EchoRelax_NEThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MAX
* MaxValue of DNNS_EchoRelax_NEThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_DEFAULT
* Default of DNNS_EchoClippingLevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_DEFAULT (32767)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MIN
* MinValue of DNNS_EchoClippingLevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MAX
* MaxValue of DNNS_EchoClippingLevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_LF_DEFAULT
* Default of DNNS_GainEta_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_LF_DEFAULT (16384)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_LF_MIN
* MinValue of DNNS_GainEta_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_LF_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_LF_MAX
* MaxValue of DNNS_GainEta_LF
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_LF_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_LB_DEFAULT
* Default of DNNS_GainEta_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_LB_DEFAULT (16384)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_LB_MIN
* MinValue of DNNS_GainEta_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_LB_MAX
* MaxValue of DNNS_GainEta_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_HB_DEFAULT
* Default of DNNS_GainEta_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_HB_DEFAULT (16384)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_HB_MIN
* MinValue of DNNS_GainEta_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_HB_MAX
* MaxValue of DNNS_GainEta_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_DEFAULT
* Default of DNNS_GainEta_Sensitivity
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_DEFAULT (32767)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_MIN
* MinValue of DNNS_GainEta_Sensitivity
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_MAX
* MaxValue of DNNS_GainEta_Sensitivity
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_DEFAULT
* Default of DNNS_MSR_EchoGammaVoiced
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_MIN
* MinValue of DNNS_MSR_EchoGammaVoiced
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_MAX
* MaxValue of DNNS_MSR_EchoGammaVoiced
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_DEFAULT
* Default of DNNS_MSR_EchoGammaUnvoiced
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_DEFAULT (256)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_MIN
* MinValue of DNNS_MSR_EchoGammaUnvoiced
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_MAX
* MaxValue of DNNS_MSR_EchoGammaUnvoiced
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_DEFAULT
* Default of DNNS_MSR_NonLinEchoFraction_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_DEFAULT (8192)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_MIN
* MinValue of DNNS_MSR_NonLinEchoFraction_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_MAX
* MaxValue of DNNS_MSR_NonLinEchoFraction_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_DEFAULT
* Default of DNNS_MSR_NonLinEchoFraction_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_DEFAULT (8192)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_MIN
* MinValue of DNNS_MSR_NonLinEchoFraction_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_MAX
* MaxValue of DNNS_MSR_NonLinEchoFraction_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_DEFAULT
* Default of DNNS_MSR_LinEchoFraction_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_DEFAULT (1638)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_MIN
* MinValue of DNNS_MSR_LinEchoFraction_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_MAX
* MaxValue of DNNS_MSR_LinEchoFraction_LB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_DEFAULT
* Default of DNNS_MSR_LinEchoFraction_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_DEFAULT (1638)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_MIN
* MinValue of DNNS_MSR_LinEchoFraction_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_MAX
* MaxValue of DNNS_MSR_LinEchoFraction_HB
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_DEFAULT
* Default of DNNS_MSR_VoicedPreservationThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_DEFAULT (263)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_MIN
* MinValue of DNNS_MSR_VoicedPreservationThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_MAX
* MaxValue of DNNS_MSR_VoicedPreservationThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_DEFAULT
* Default of DNNS_SPDET_NearThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_DEFAULT (512)
/**
* @def ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MIN
* MinValue of DNNS_SPDET_NearThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MAX
* MaxValue of DNNS_SPDET_NearThreshold
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_CNILEVEL_DEFAULT
* Default of DNNS_CNILevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_CNILEVEL_DEFAULT (12000)
/**
* @def ECHOSUPPRESSION_DNNS_CNILEVEL_MIN
* MinValue of DNNS_CNILevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_CNILEVEL_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_CNILEVEL_MAX
* MaxValue of DNNS_CNILevel
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_CNILEVEL_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_DEFAULT
* Default of DNNS_EchoGammaInit
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_DEFAULT (1024)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_MIN
* MinValue of DNNS_EchoGammaInit
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_MAX
* MaxValue of DNNS_EchoGammaInit
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_DEFAULT
* Default of DNNS_EchoGammaScaling_MicCoverage
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_DEFAULT (512)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_MIN
* MinValue of DNNS_EchoGammaScaling_MicCoverage
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_MAX
* MaxValue of DNNS_EchoGammaScaling_MicCoverage
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_MAX (32767)

/**
* @def ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_DEFAULT
* Default of DNNS_NlAttenScaling_MicCoverage
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_DEFAULT (512)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_MIN
* MinValue of DNNS_NlAttenScaling_MicCoverage
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_MIN (0)
/**
* @def ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_MAX
* MaxValue of DNNS_NlAttenScaling_MicCoverage
* @see EchoSuppression_ControlParams_st
*/
#define ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_MAX (32767)

/**
* @def GENERALSIDELOBECANCELLER_GSC_TAPS_DEFAULT
* Default of GSC_taps
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_TAPS_DEFAULT (16)
/**
* @def GENERALSIDELOBECANCELLER_GSC_TAPS_MIN
* MinValue of GSC_taps
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_TAPS_MIN (8)
/**
* @def GENERALSIDELOBECANCELLER_GSC_TAPS_MAX
* MaxValue of GSC_taps
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_TAPS_MAX (16)

/**
* @def GENERALSIDELOBECANCELLER_GSC_ERL_DEFAULT
* Default of GSC_erl
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_ERL_DEFAULT (256)
/**
* @def GENERALSIDELOBECANCELLER_GSC_ERL_MIN
* MinValue of GSC_erl
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_ERL_MIN (64)
/**
* @def GENERALSIDELOBECANCELLER_GSC_ERL_MAX
* MaxValue of GSC_erl
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_ERL_MAX (32767)

/**
* @def GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_DEFAULT
* Default of GSC_SpeechRecovery_Sensitivity
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_DEFAULT (32767)
/**
* @def GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_MIN
* MinValue of GSC_SpeechRecovery_Sensitivity
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_MIN (0)
/**
* @def GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_MAX
* MaxValue of GSC_SpeechRecovery_Sensitivity
* @see GeneralSidelobeCanceller_ControlParams_st
*/
#define GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_DEFAULT
* Default of DNNS_Gamma_nn_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_DEFAULT (8192)
/**
* @def NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MIN
* MinValue of DNNS_Gamma_nn_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MAX
* MaxValue of DNNS_Gamma_nn_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_DEFAULT
* Default of DNNS_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MIN
* MinValue of DNNS_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MAX
* MaxValue of DNNS_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_DEFAULT
* Default of DNNS_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MIN
* MinValue of DNNS_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MAX
* MaxValue of DNNS_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_DEFAULT
* Default of DNNS_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MIN
* MinValue of DNNS_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MAX
* MaxValue of DNNS_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_DEFAULT
* Default of DNNS_MinGain_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_DEFAULT {200,4000,8000}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_ARRAYMIN
* MinValue of DNNS_MinGain_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_ARRAYMIN {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_ARRAYMAX
* MaxValue of DNNS_MinGain_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_ARRAYMAX {8000,8000,8000}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_LENGTH
* Length of DNNS_MinGain_SNR_freq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_DEFAULT
* Default of DNNS_MinGain_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_DEFAULT {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_ARRAYMIN
* MinValue of DNNS_MinGain_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_ARRAYMIN {-5,-5,-5}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_ARRAYMAX
* MaxValue of DNNS_MinGain_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_ARRAYMAX {63,63,63}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_LENGTH
* Length of DNNS_MinGain_SNR_min
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_DEFAULT
* Default of DNNS_MinGain_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_DEFAULT {20,20,20}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_ARRAYMIN
* MinValue of DNNS_MinGain_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_ARRAYMIN {-5,-5,-5}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_ARRAYMAX
* MaxValue of DNNS_MinGain_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_ARRAYMAX {63,63,63}
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_LENGTH
* Length of DNNS_MinGain_SNR_max
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_DEFAULT
* Default of DNNS_MaxSuppression_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_DEFAULT {15,15,15}
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_ARRAYMIN
* MinValue of DNNS_MaxSuppression_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_ARRAYMIN {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_ARRAYMAX
* MaxValue of DNNS_MaxSuppression_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_ARRAYMAX {48,48,48}
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_LENGTH
* Length of DNNS_MaxSuppression_highSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_DEFAULT
* Default of DNNS_MaxSuppression_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_DEFAULT {15,15,15}
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_ARRAYMIN
* MinValue of DNNS_MaxSuppression_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_ARRAYMIN {0,0,0}
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_ARRAYMAX
* MaxValue of DNNS_MaxSuppression_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_ARRAYMAX {48,48,48}
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_LENGTH
* Length of DNNS_MaxSuppression_lowSNR
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_LENGTH (3)

/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_DEFAULT
* Default of DNNS_MaxSuppressionRelax_Speech
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_DEFAULT (1)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_MIN
* MinValue of DNNS_MaxSuppressionRelax_Speech
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_MAX
* MaxValue of DNNS_MaxSuppressionRelax_Speech
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_MAX (6)

/**
* @def NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_DEFAULT
* Default of DNNS_MinSuppression_Speech
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_DEFAULT (6)
/**
* @def NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_MIN
* MinValue of DNNS_MinSuppression_Speech
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_MAX
* MaxValue of DNNS_MinSuppression_Speech
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_MAX (48)

/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_DEFAULT
* Default of DNNS_MaxSuppressionRel_NonStationary
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_DEFAULT (0)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_MIN
* MinValue of DNNS_MaxSuppressionRel_NonStationary
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_MAX
* MaxValue of DNNS_MaxSuppressionRel_NonStationary
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_MAX (36)

/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_DEFAULT
* Default of DNNS_MaxSuppressionRel_NonStatBroadSide
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_DEFAULT (0)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_MIN
* MinValue of DNNS_MaxSuppressionRel_NonStatBroadSide
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_MAX
* MaxValue of DNNS_MaxSuppressionRel_NonStatBroadSide
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_MAX (36)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_DEFAULT
* Default of DNNS_MinGain_RelaxPosChange
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_DEFAULT (2048)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_MIN
* MinValue of DNNS_MinGain_RelaxPosChange
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_MIN (2048)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_MAX
* MaxValue of DNNS_MinGain_RelaxPosChange
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_DEFAULT
* Default of DNNS_MinGain_RelaxMax
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_DEFAULT (5)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_MIN
* MinValue of DNNS_MinGain_RelaxMax
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_MAX
* MaxValue of DNNS_MinGain_RelaxMax
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_MAX (12)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_DEFAULT
* Default of DNNS_MinGain_RelaxSensitivity_LB
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_DEFAULT (16384)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_MIN
* MinValue of DNNS_MinGain_RelaxSensitivity_LB
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_MAX
* MaxValue of DNNS_MinGain_RelaxSensitivity_LB
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_DEFAULT
* Default of DNNS_MinGain_RelaxSensitivity_HB
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_DEFAULT (16384)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_MIN
* MinValue of DNNS_MinGain_RelaxSensitivity_HB
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_MAX
* MaxValue of DNNS_MinGain_RelaxSensitivity_HB
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_DEFAULT
* Default of DNNS_MaxSuppression_Topband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_DEFAULT (15)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_MIN
* MinValue of DNNS_MaxSuppression_Topband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_MAX
* MaxValue of DNNS_MaxSuppression_Topband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_MAX (48)

/**
* @def NOISESUPPRESSION_DNNS_SCNESUBFACTOR_DEFAULT
* Default of DNNS_ScneSubFactor
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNESUBFACTOR_DEFAULT (1024)
/**
* @def NOISESUPPRESSION_DNNS_SCNESUBFACTOR_MIN
* MinValue of DNNS_ScneSubFactor
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNESUBFACTOR_MIN (1024)
/**
* @def NOISESUPPRESSION_DNNS_SCNESUBFACTOR_MAX
* MaxValue of DNNS_ScneSubFactor
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNESUBFACTOR_MAX (3072)

/**
* @def NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_DEFAULT
* Default of DNNS_ScneMinThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_DEFAULT (4096)
/**
* @def NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_MIN
* MinValue of DNNS_ScneMinThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_MAX
* MaxValue of DNNS_ScneMinThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_DEFAULT
* Default of DNNS_ScneMaxThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_DEFAULT (12288)
/**
* @def NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_MIN
* MinValue of DNNS_ScneMaxThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_MAX
* MaxValue of DNNS_ScneMaxThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_SCNEMINSCALING_DEFAULT
* Default of DNNS_ScneMinScaling
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMINSCALING_DEFAULT (16384)
/**
* @def NOISESUPPRESSION_DNNS_SCNEMINSCALING_MIN
* MinValue of DNNS_ScneMinScaling
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMINSCALING_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_SCNEMINSCALING_MAX
* MaxValue of DNNS_ScneMinScaling
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SCNEMINSCALING_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_NGS_SMOOTHING_DEFAULT
* Default of DNNS_NGS_Smoothing
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NGS_SMOOTHING_DEFAULT (819)
/**
* @def NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MIN
* MinValue of DNNS_NGS_Smoothing
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MAX
* MaxValue of DNNS_NGS_Smoothing
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MAX (1024)

/**
* @def NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_DEFAULT
* Default of DNNS_MusicalTone_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_DEFAULT (0)
/**
* @def NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MIN
* MinValue of DNNS_MusicalTone_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MIN (-1024)
/**
* @def NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MAX
* MaxValue of DNNS_MusicalTone_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MAX (1024)

/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_DEFAULT
* Default of DNNS_SpeechRecovery_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_DEFAULT (32767)
/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_MIN
* MinValue of DNNS_SpeechRecovery_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_MAX
* MaxValue of DNNS_SpeechRecovery_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_DEFAULT
* Default of DNNS_SpeechRecovery_Factor
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_DEFAULT (8192)
/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_MIN
* MinValue of DNNS_SpeechRecovery_Factor
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_MAX
* MaxValue of DNNS_SpeechRecovery_Factor
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_DEFAULT
* Default of DNNS_NoiseDetect_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_DEFAULT (32767)
/**
* @def NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_MIN
* MinValue of DNNS_NoiseDetect_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_MAX
* MaxValue of DNNS_NoiseDetect_Sensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_NOISEPULLDOWN_DEFAULT
* Default of DNNS_NoisePullDown
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NOISEPULLDOWN_DEFAULT (0)
/**
* @def NOISESUPPRESSION_DNNS_NOISEPULLDOWN_MIN
* MinValue of DNNS_NoisePullDown
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NOISEPULLDOWN_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_NOISEPULLDOWN_MAX
* MaxValue of DNNS_NoisePullDown
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_NOISEPULLDOWN_MAX (12)

/**
* @def NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_DEFAULT
* Default of DNNS_MVAD_Sensitivity_Noise
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_DEFAULT (16384)
/**
* @def NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_MIN
* MinValue of DNNS_MVAD_Sensitivity_Noise
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_MAX
* MaxValue of DNNS_MVAD_Sensitivity_Noise
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_DEFAULT
* Default of DNNS_VoicingThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_DEFAULT (60)
/**
* @def NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_MIN
* MinValue of DNNS_VoicingThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_MAX
* MaxValue of DNNS_VoicingThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_MAX (512)

/**
* @def NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_DEFAULT
* Default of DNNS_PitchEnhancement
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_DEFAULT (10)
/**
* @def NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_MIN
* MinValue of DNNS_PitchEnhancement
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_MAX
* MaxValue of DNNS_PitchEnhancement
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_MAX (24)

/**
* @def NOISESUPPRESSION_DNNS_PITCHMAXFREQ_DEFAULT
* Default of DNNS_PitchMaxFreq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_PITCHMAXFREQ_DEFAULT (300)
/**
* @def NOISESUPPRESSION_DNNS_PITCHMAXFREQ_MIN
* MinValue of DNNS_PitchMaxFreq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_PITCHMAXFREQ_MIN (100)
/**
* @def NOISESUPPRESSION_DNNS_PITCHMAXFREQ_MAX
* MaxValue of DNNS_PitchMaxFreq
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_PITCHMAXFREQ_MAX (500)

/**
* @def NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_DEFAULT
* Default of DNNS_MSR_VoicingThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_DEFAULT (60)
/**
* @def NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_MIN
* MinValue of DNNS_MSR_VoicingThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_MAX
* MaxValue of DNNS_MSR_VoicingThreshold
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_MAX (512)

/**
* @def NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_DEFAULT
* Default of DNNS_MSR_VoicedSensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_DEFAULT (32767)
/**
* @def NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_MIN
* MinValue of DNNS_MSR_VoicedSensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_MAX
* MaxValue of DNNS_MSR_VoicedSensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_DEFAULT
* Default of DNNS_MSR_UnvoicedSensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_DEFAULT (32767)
/**
* @def NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_MIN
* MinValue of DNNS_MSR_UnvoicedSensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_MAX
* MaxValue of DNNS_MSR_UnvoicedSensitivity
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_DEFAULT
* Default of DNNS_MSR_NoiseGammaVoiced
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_DEFAULT (256)
/**
* @def NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_MIN
* MinValue of DNNS_MSR_NoiseGammaVoiced
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_MAX
* MaxValue of DNNS_MSR_NoiseGammaVoiced
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_DEFAULT
* Default of DNNS_MSR_NoiseGammaUnvoiced
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_DEFAULT (128)
/**
* @def NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_MIN
* MinValue of DNNS_MSR_NoiseGammaUnvoiced
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_MIN (0)
/**
* @def NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_MAX
* MaxValue of DNNS_MSR_NoiseGammaUnvoiced
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_DEFAULT
* Default of DNNS_VAD_hysteresis
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_DEFAULT (32113)
/**
* @def NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_MIN
* MinValue of DNNS_VAD_hysteresis
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_MIN (16384)
/**
* @def NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_MAX
* MaxValue of DNNS_VAD_hysteresis
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_MAX (32440)

/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_DEFAULT
* Default of DNNS_PosChange_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_DEFAULT (512)
/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_MIN
* MinValue of DNNS_PosChange_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_MAX
* MaxValue of DNNS_PosChange_Constrained_nn_lo
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_DEFAULT
* Default of DNNS_PosChange_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_DEFAULT (768)
/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_MIN
* MinValue of DNNS_PosChange_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_MAX
* MaxValue of DNNS_PosChange_Constrained_nn_hi
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_MAX (32767)

/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_DEFAULT
* Default of DNNS_PosChange_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_DEFAULT (768)
/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_MIN
* MinValue of DNNS_PosChange_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_MIN (256)
/**
* @def NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_MAX
* MaxValue of DNNS_PosChange_Constrained_nn_highband
* @see NoiseSuppression_ControlParams_st
*/
#define NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_DEFAULT
* Default of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_DEFAULT (12)
/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_MIN
* MinValue of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_MAX
* MaxValue of WNS_MicSpeechLevelDiff
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_MAX (20)

/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_DEFAULT
* Default of WNS_DetectorThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_DEFAULT (6554)
/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MIN
* MinValue of WNS_DetectorThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MAX
* MaxValue of WNS_DetectorThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_DEFAULT
* Default of WNS_HPFCornerFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_DEFAULT (50)
/**
* @def WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MIN
* MinValue of WNS_HPFCornerFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MAX
* MaxValue of WNS_HPFCornerFreq
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MAX (1500)

/**
* @def WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_DEFAULT
* Default of WNS_CutOff_MinEnergyLevel
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_DEFAULT (36)
/**
* @def WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_MIN
* MinValue of WNS_CutOff_MinEnergyLevel
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_MAX
* MaxValue of WNS_CutOff_MinEnergyLevel
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_MAX (196)

/**
* @def WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_DEFAULT
* Default of WNS_CutOff_MaxEnergyLevel
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_DEFAULT (106)
/**
* @def WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_MIN
* MinValue of WNS_CutOff_MaxEnergyLevel
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_MIN (20)
/**
* @def WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_MAX
* MaxValue of WNS_CutOff_MaxEnergyLevel
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_MAX (196)

/**
* @def WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_DEFAULT
* Default of WNS_GustPostProc_Sensitivity
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_DEFAULT (0)
/**
* @def WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_MIN
* MinValue of WNS_GustPostProc_Sensitivity
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_MAX
* MaxValue of WNS_GustPostProc_Sensitivity
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_DEFAULT
* Default of WNS_NMic_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_DEFAULT {32767,32767}
/**
* @def WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_ARRAYMIN
* MinValue of WNS_NMic_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_ARRAYMIN {0,0}
/**
* @def WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_ARRAYMAX
* MaxValue of WNS_NMic_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_ARRAYMAX {32767,32767}
/**
* @def WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_LENGTH
* Length of WNS_NMic_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_LENGTH (2)

/**
* @def WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_DEFAULT
* Default of WNS_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_DEFAULT (256)
/**
* @def WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MIN
* MinValue of WNS_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MAX
* MaxValue of WNS_Aggressiveness
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_DEFAULT
* Default of WNS_MaxSuppression
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_DEFAULT (12)
/**
* @def WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_MIN
* MinValue of WNS_MaxSuppression
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_MAX
* MaxValue of WNS_MaxSuppression
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_MAX (48)

/**
* @def WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_DEFAULT
* Default of WNS_MaxExtraGustSuppression
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_DEFAULT (48)
/**
* @def WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_MIN
* MinValue of WNS_MaxExtraGustSuppression
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_MAX
* MaxValue of WNS_MaxExtraGustSuppression
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_MAX (48)

/**
* @def WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_DEFAULT
* Default of WNS_GushThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_DEFAULT (9830)
/**
* @def WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MIN
* MinValue of WNS_GushThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MIN (0)
/**
* @def WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MAX
* MaxValue of WNS_GushThreshold
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MAX (32767)

/**
* @def WINDNOISESUPPRESSION_WNS_HFSLOPE_DEFAULT
* Default of WNS_HFSlope
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HFSLOPE_DEFAULT (-6)
/**
* @def WINDNOISESUPPRESSION_WNS_HFSLOPE_MIN
* MinValue of WNS_HFSlope
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HFSLOPE_MIN (-20)
/**
* @def WINDNOISESUPPRESSION_WNS_HFSLOPE_MAX
* MaxValue of WNS_HFSlope
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_HFSLOPE_MAX (0)

/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_DEFAULT
* Default of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_DEFAULT {0,0}
/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMIN
* MinValue of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMIN {-96,-96}
/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMAX
* MaxValue of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMAX {32,32}
/**
* @def WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_LENGTH
* Length of WNS_EnergyScaleFactor
* @see WindNoiseSuppression_ControlParams_st
*/
#define WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_LENGTH (2)

/**
* @def PATHCHANGEDETECTOR_PCD_TAPS_DEFAULT
* Default of PCD_Taps
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_TAPS_DEFAULT (32)
/**
* @def PATHCHANGEDETECTOR_PCD_TAPS_MIN
* MinValue of PCD_Taps
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_TAPS_MIN (16)
/**
* @def PATHCHANGEDETECTOR_PCD_TAPS_MAX
* MaxValue of PCD_Taps
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_TAPS_MAX (64)

/**
* @def PATHCHANGEDETECTOR_PCD_ERL_DEFAULT
* Default of PCD_Erl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERL_DEFAULT (64)
/**
* @def PATHCHANGEDETECTOR_PCD_ERL_MIN
* MinValue of PCD_Erl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERL_MIN (64)
/**
* @def PATHCHANGEDETECTOR_PCD_ERL_MAX
* MaxValue of PCD_Erl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERL_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_THRESHOLD_DEFAULT
* Default of PCD_Threshold
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_THRESHOLD_DEFAULT (5000)
/**
* @def PATHCHANGEDETECTOR_PCD_THRESHOLD_MIN
* MinValue of PCD_Threshold
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_THRESHOLD_MIN (0)
/**
* @def PATHCHANGEDETECTOR_PCD_THRESHOLD_MAX
* MaxValue of PCD_Threshold
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_THRESHOLD_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_MINIMUMERL_DEFAULT
* Default of PCD_MinimumErl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_MINIMUMERL_DEFAULT (64)
/**
* @def PATHCHANGEDETECTOR_PCD_MINIMUMERL_MIN
* MinValue of PCD_MinimumErl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_MINIMUMERL_MIN (64)
/**
* @def PATHCHANGEDETECTOR_PCD_MINIMUMERL_MAX
* MaxValue of PCD_MinimumErl
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_MINIMUMERL_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_ERLSTEP_DEFAULT
* Default of PCD_ErlStep
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERLSTEP_DEFAULT (16800)
/**
* @def PATHCHANGEDETECTOR_PCD_ERLSTEP_MIN
* MinValue of PCD_ErlStep
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERLSTEP_MIN (16384)
/**
* @def PATHCHANGEDETECTOR_PCD_ERLSTEP_MAX
* MaxValue of PCD_ErlStep
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_ERLSTEP_MAX (32767)

/**
* @def PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_DEFAULT
* Default of PCD_GammaERescue
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_DEFAULT (2048)
/**
* @def PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MIN
* MinValue of PCD_GammaERescue
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MIN (0)
/**
* @def PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MAX
* MaxValue of PCD_GammaERescue
* @see PathChangeDetector_ControlParams_st
*/
#define PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_DEFAULT
* Default of SDET_FarThreshold_LB
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_DEFAULT (16384)
/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_MIN
* MinValue of SDET_FarThreshold_LB
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_MAX
* MaxValue of SDET_FarThreshold_LB
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_DEFAULT
* Default of SDET_FarThreshold_HB
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_DEFAULT (24576)
/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_MIN
* MinValue of SDET_FarThreshold_HB
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_MAX
* MaxValue of SDET_FarThreshold_HB
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_DEFAULT
* Default of SDET_CAL_MicPowFloorMin
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_DEFAULT (29)
/**
* @def SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MIN
* MinValue of SDET_CAL_MicPowFloorMin
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MAX
* MaxValue of SDET_CAL_MicPowFloorMin
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_DEFAULT
* Default of SDET_IMCOH_BroadsideLevelScale
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_DEFAULT (11470)
/**
* @def SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MIN
* MinValue of SDET_IMCOH_BroadsideLevelScale
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MIN (6554)
/**
* @def SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MAX
* MaxValue of SDET_IMCOH_BroadsideLevelScale
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MAX (19661)

/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_DEFAULT
* Default of SDET_PosChange_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_DEFAULT (32767)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MIN
* MinValue of SDET_PosChange_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MAX
* MaxValue of SDET_PosChange_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_DEFAULT
* Default of SDET_PosChange_ProtectPeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_DEFAULT (60)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MIN
* MinValue of SDET_PosChange_ProtectPeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MAX
* MaxValue of SDET_PosChange_ProtectPeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_DEFAULT
* Default of SDET_PosChange_UpdateThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_DEFAULT (16384)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_MIN
* MinValue of SDET_PosChange_UpdateThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_MAX
* MaxValue of SDET_PosChange_UpdateThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_DEFAULT
* Default of SDET_MaxConvergencePeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_DEFAULT (0)
/**
* @def SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MIN
* MinValue of SDET_MaxConvergencePeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MAX
* MaxValue of SDET_MaxConvergencePeriod
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_DEFAULT
* Default of SDET_NoiseChangeFloor
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_DEFAULT (41)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MIN
* MinValue of SDET_NoiseChangeFloor
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MAX
* MaxValue of SDET_NoiseChangeFloor
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_DEFAULT
* Default of SDET_NoiseChangeThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_DEFAULT (3840)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MIN
* MinValue of SDET_NoiseChangeThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MAX
* MaxValue of SDET_NoiseChangeThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_DEFAULT
* Default of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_DEFAULT {96,96}
/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMIN
* MinValue of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMIN {0,0}
/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMAX
* MaxValue of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMAX {96,96}
/**
* @def SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_LENGTH
* Length of SDET_MicCovThreshold
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_LENGTH (2)

/**
* @def SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_DEFAULT
* Default of SDET_CoherentNoiseDetect_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_DEFAULT (26213)
/**
* @def SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_MIN
* MinValue of SDET_CoherentNoiseDetect_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_MAX
* MaxValue of SDET_CoherentNoiseDetect_Sensitivity
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_DEFAULT
* Default of SDET_CoherentNoise_Aggressiveness
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_DEFAULT (2048)
/**
* @def SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_MIN
* MinValue of SDET_CoherentNoise_Aggressiveness
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_MIN (2048)
/**
* @def SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_MAX
* MaxValue of SDET_CoherentNoise_Aggressiveness
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_MAX (32767)

/**
* @def SPEECHDETECTOR_SDET_SOFTSPEECH_DEFAULT
* Default of SDET_SoftSpeech
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_SOFTSPEECH_DEFAULT (10)
/**
* @def SPEECHDETECTOR_SDET_SOFTSPEECH_MIN
* MinValue of SDET_SoftSpeech
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_SOFTSPEECH_MIN (3)
/**
* @def SPEECHDETECTOR_SDET_SOFTSPEECH_MAX
* MaxValue of SDET_SoftSpeech
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_SOFTSPEECH_MAX (100)

/**
* @def SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_DEFAULT
* Default of SDET_CardioidLeakage
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_DEFAULT (19661)
/**
* @def SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_MIN
* MinValue of SDET_CardioidLeakage
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_MIN (0)
/**
* @def SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_MAX
* MaxValue of SDET_CardioidLeakage
* @see SpeechDetector_ControlParams_st
*/
#define SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_0_DEFAULT
* Default of RsvParam_0
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_0_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_0_MIN
* MinValue of RsvParam_0
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_0_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_0_MAX
* MaxValue of RsvParam_0
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_0_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_1_DEFAULT
* Default of RsvParam_1
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_1_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_1_MIN
* MinValue of RsvParam_1
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_1_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_1_MAX
* MaxValue of RsvParam_1
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_1_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_2_DEFAULT
* Default of RsvParam_2
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_2_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_2_MIN
* MinValue of RsvParam_2
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_2_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_2_MAX
* MaxValue of RsvParam_2
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_2_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_3_DEFAULT
* Default of RsvParam_3
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_3_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_3_MIN
* MinValue of RsvParam_3
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_3_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_3_MAX
* MaxValue of RsvParam_3
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_3_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_4_DEFAULT
* Default of RsvParam_4
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_4_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_4_MIN
* MinValue of RsvParam_4
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_4_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_4_MAX
* MaxValue of RsvParam_4
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_4_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_5_DEFAULT
* Default of RsvParam_5
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_5_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_5_MIN
* MinValue of RsvParam_5
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_5_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_5_MAX
* MaxValue of RsvParam_5
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_5_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_6_DEFAULT
* Default of RsvParam_6
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_6_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_6_MIN
* MinValue of RsvParam_6
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_6_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_6_MAX
* MaxValue of RsvParam_6
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_6_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_7_DEFAULT
* Default of RsvParam_7
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_7_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_7_MIN
* MinValue of RsvParam_7
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_7_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_7_MAX
* MaxValue of RsvParam_7
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_7_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_8_DEFAULT
* Default of RsvParam_8
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_8_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_8_MIN
* MinValue of RsvParam_8
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_8_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_8_MAX
* MaxValue of RsvParam_8
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_8_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_9_DEFAULT
* Default of RsvParam_9
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_9_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_9_MIN
* MinValue of RsvParam_9
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_9_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_9_MAX
* MaxValue of RsvParam_9
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_9_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_10_DEFAULT
* Default of RsvParam_10
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_10_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_10_MIN
* MinValue of RsvParam_10
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_10_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_10_MAX
* MaxValue of RsvParam_10
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_10_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_11_DEFAULT
* Default of RsvParam_11
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_11_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_11_MIN
* MinValue of RsvParam_11
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_11_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_11_MAX
* MaxValue of RsvParam_11
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_11_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_12_DEFAULT
* Default of RsvParam_12
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_12_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_12_MIN
* MinValue of RsvParam_12
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_12_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_12_MAX
* MaxValue of RsvParam_12
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_12_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_13_DEFAULT
* Default of RsvParam_13
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_13_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_13_MIN
* MinValue of RsvParam_13
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_13_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_13_MAX
* MaxValue of RsvParam_13
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_13_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_14_DEFAULT
* Default of RsvParam_14
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_14_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_14_MIN
* MinValue of RsvParam_14
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_14_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_14_MAX
* MaxValue of RsvParam_14
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_14_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_15_DEFAULT
* Default of RsvParam_15
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_15_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_15_MIN
* MinValue of RsvParam_15
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_15_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_15_MAX
* MaxValue of RsvParam_15
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_15_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_16_DEFAULT
* Default of RsvParam_16
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_16_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_16_MIN
* MinValue of RsvParam_16
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_16_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_16_MAX
* MaxValue of RsvParam_16
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_16_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_17_DEFAULT
* Default of RsvParam_17
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_17_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_17_MIN
* MinValue of RsvParam_17
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_17_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_17_MAX
* MaxValue of RsvParam_17
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_17_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_18_DEFAULT
* Default of RsvParam_18
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_18_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_18_MIN
* MinValue of RsvParam_18
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_18_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_18_MAX
* MaxValue of RsvParam_18
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_18_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_19_DEFAULT
* Default of RsvParam_19
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_19_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_19_MIN
* MinValue of RsvParam_19
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_19_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_19_MAX
* MaxValue of RsvParam_19
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_19_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_20_DEFAULT
* Default of RsvParam_20
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_20_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_20_MIN
* MinValue of RsvParam_20
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_20_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_20_MAX
* MaxValue of RsvParam_20
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_20_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_21_DEFAULT
* Default of RsvParam_21
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_21_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_21_MIN
* MinValue of RsvParam_21
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_21_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_21_MAX
* MaxValue of RsvParam_21
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_21_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_22_DEFAULT
* Default of RsvParam_22
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_22_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_22_MIN
* MinValue of RsvParam_22
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_22_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_22_MAX
* MaxValue of RsvParam_22
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_22_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_23_DEFAULT
* Default of RsvParam_23
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_23_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_23_MIN
* MinValue of RsvParam_23
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_23_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_23_MAX
* MaxValue of RsvParam_23
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_23_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_24_DEFAULT
* Default of RsvParam_24
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_24_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_24_MIN
* MinValue of RsvParam_24
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_24_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_24_MAX
* MaxValue of RsvParam_24
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_24_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_25_DEFAULT
* Default of RsvParam_25
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_25_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_25_MIN
* MinValue of RsvParam_25
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_25_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_25_MAX
* MaxValue of RsvParam_25
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_25_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_26_DEFAULT
* Default of RsvParam_26
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_26_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_26_MIN
* MinValue of RsvParam_26
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_26_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_26_MAX
* MaxValue of RsvParam_26
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_26_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_27_DEFAULT
* Default of RsvParam_27
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_27_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_27_MIN
* MinValue of RsvParam_27
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_27_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_27_MAX
* MaxValue of RsvParam_27
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_27_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_28_DEFAULT
* Default of RsvParam_28
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_28_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_28_MIN
* MinValue of RsvParam_28
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_28_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_28_MAX
* MaxValue of RsvParam_28
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_28_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_29_DEFAULT
* Default of RsvParam_29
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_29_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_29_MIN
* MinValue of RsvParam_29
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_29_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_29_MAX
* MaxValue of RsvParam_29
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_29_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_30_DEFAULT
* Default of RsvParam_30
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_30_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_30_MIN
* MinValue of RsvParam_30
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_30_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_30_MAX
* MaxValue of RsvParam_30
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_30_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_31_DEFAULT
* Default of RsvParam_31
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_31_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_31_MIN
* MinValue of RsvParam_31
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_31_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_31_MAX
* MaxValue of RsvParam_31
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_31_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_32_DEFAULT
* Default of RsvParam_32
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_32_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_32_MIN
* MinValue of RsvParam_32
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_32_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_32_MAX
* MaxValue of RsvParam_32
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_32_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_33_DEFAULT
* Default of RsvParam_33
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_33_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_33_MIN
* MinValue of RsvParam_33
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_33_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_33_MAX
* MaxValue of RsvParam_33
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_33_MAX (32767)

/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_34_DEFAULT
* Default of RsvParam_34
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_34_DEFAULT (6826)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_34_MIN
* MinValue of RsvParam_34
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_34_MIN (-32768)
/**
* @def LVNV_RSVPARAMETERS_RSVPARAM_34_MAX
* MaxValue of RsvParam_34
* @see LVNV_RsvParameters_ControlParams_st
*/
#define LVNV_RSVPARAMETERS_RSVPARAM_34_MAX (32767)

/**
* @def PBFDAF_PBFDAFGAMMAUP_DEFAULT
* Default of PbfdafGammaUp
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFGAMMAUP_DEFAULT (31129)
/**
* @def PBFDAF_PBFDAFGAMMAUP_MIN
* MinValue of PbfdafGammaUp
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFGAMMAUP_MIN (0)
/**
* @def PBFDAF_PBFDAFGAMMAUP_MAX
* MaxValue of PbfdafGammaUp
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFGAMMAUP_MAX (32767)

/**
* @def PBFDAF_PBFDAFGAMMADOWN_DEFAULT
* Default of PbfdafGammaDown
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFGAMMADOWN_DEFAULT (31129)
/**
* @def PBFDAF_PBFDAFGAMMADOWN_MIN
* MinValue of PbfdafGammaDown
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFGAMMADOWN_MIN (0)
/**
* @def PBFDAF_PBFDAFGAMMADOWN_MAX
* MaxValue of PbfdafGammaDown
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFGAMMADOWN_MAX (32767)

/**
* @def PBFDAF_PBFDAFFILTERLENGTHREF0_DEFAULT
* Default of PbfdafFilterLengthRef0
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFFILTERLENGTHREF0_DEFAULT {320,320}
/**
* @def PBFDAF_PBFDAFFILTERLENGTHREF0_ARRAYMIN
* MinValue of PbfdafFilterLengthRef0
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFFILTERLENGTHREF0_ARRAYMIN {160,160}
/**
* @def PBFDAF_PBFDAFFILTERLENGTHREF0_ARRAYMAX
* MaxValue of PbfdafFilterLengthRef0
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFFILTERLENGTHREF0_ARRAYMAX {640,640}
/**
* @def PBFDAF_PBFDAFFILTERLENGTHREF0_LENGTH
* Length of PbfdafFilterLengthRef0
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFFILTERLENGTHREF0_LENGTH (2)

/**
* @def PBFDAF_PBFDAFABSPOWMIN_DEFAULT
* Default of PbfdafAbsPowMin
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFABSPOWMIN_DEFAULT (2000)
/**
* @def PBFDAF_PBFDAFABSPOWMIN_MIN
* MinValue of PbfdafAbsPowMin
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFABSPOWMIN_MIN (0)
/**
* @def PBFDAF_PBFDAFABSPOWMIN_MAX
* MaxValue of PbfdafAbsPowMin
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFABSPOWMIN_MAX (32767)

/**
* @def PBFDAF_PBFDAFERL_LF_DEFAULT
* Default of PbfdafErl_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LF_DEFAULT {3200,3200}
/**
* @def PBFDAF_PBFDAFERL_LF_ARRAYMIN
* MinValue of PbfdafErl_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LF_ARRAYMIN {32,32}
/**
* @def PBFDAF_PBFDAFERL_LF_ARRAYMAX
* MaxValue of PbfdafErl_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LF_ARRAYMAX {32767,32767}
/**
* @def PBFDAF_PBFDAFERL_LF_LENGTH
* Length of PbfdafErl_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LF_LENGTH (2)

/**
* @def PBFDAF_PBFDAFERL_LB_DEFAULT
* Default of PbfdafErl_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LB_DEFAULT {3200,3200}
/**
* @def PBFDAF_PBFDAFERL_LB_ARRAYMIN
* MinValue of PbfdafErl_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LB_ARRAYMIN {32,32}
/**
* @def PBFDAF_PBFDAFERL_LB_ARRAYMAX
* MaxValue of PbfdafErl_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LB_ARRAYMAX {32767,32767}
/**
* @def PBFDAF_PBFDAFERL_LB_LENGTH
* Length of PbfdafErl_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_LB_LENGTH (2)

/**
* @def PBFDAF_PBFDAFERL_HB_DEFAULT
* Default of PbfdafErl_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_HB_DEFAULT {3200,3200}
/**
* @def PBFDAF_PBFDAFERL_HB_ARRAYMIN
* MinValue of PbfdafErl_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_HB_ARRAYMIN {32,32}
/**
* @def PBFDAF_PBFDAFERL_HB_ARRAYMAX
* MaxValue of PbfdafErl_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_HB_ARRAYMAX {32767,32767}
/**
* @def PBFDAF_PBFDAFERL_HB_LENGTH
* Length of PbfdafErl_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFERL_HB_LENGTH (2)

/**
* @def PBFDAF_PBFDAFINITIALERL_DEFAULT
* Default of PbfdafInitialErl
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFINITIALERL_DEFAULT (32)
/**
* @def PBFDAF_PBFDAFINITIALERL_MIN
* MinValue of PbfdafInitialErl
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFINITIALERL_MIN (32)
/**
* @def PBFDAF_PBFDAFINITIALERL_MAX
* MaxValue of PbfdafInitialErl
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFINITIALERL_MAX (32767)

/**
* @def PBFDAF_PBFDAFALPHA_LF_DEFAULT
* Default of PbfdafAlpha_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LF_DEFAULT {1638,1638}
/**
* @def PBFDAF_PBFDAFALPHA_LF_ARRAYMIN
* MinValue of PbfdafAlpha_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LF_ARRAYMIN {0,0}
/**
* @def PBFDAF_PBFDAFALPHA_LF_ARRAYMAX
* MaxValue of PbfdafAlpha_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LF_ARRAYMAX {32767,32767}
/**
* @def PBFDAF_PBFDAFALPHA_LF_LENGTH
* Length of PbfdafAlpha_LF
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LF_LENGTH (2)

/**
* @def PBFDAF_PBFDAFALPHA_LB_DEFAULT
* Default of PbfdafAlpha_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LB_DEFAULT {1638,1638}
/**
* @def PBFDAF_PBFDAFALPHA_LB_ARRAYMIN
* MinValue of PbfdafAlpha_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LB_ARRAYMIN {0,0}
/**
* @def PBFDAF_PBFDAFALPHA_LB_ARRAYMAX
* MaxValue of PbfdafAlpha_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LB_ARRAYMAX {32767,32767}
/**
* @def PBFDAF_PBFDAFALPHA_LB_LENGTH
* Length of PbfdafAlpha_LB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_LB_LENGTH (2)

/**
* @def PBFDAF_PBFDAFALPHA_HB_DEFAULT
* Default of PbfdafAlpha_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_HB_DEFAULT {1638,1638}
/**
* @def PBFDAF_PBFDAFALPHA_HB_ARRAYMIN
* MinValue of PbfdafAlpha_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_HB_ARRAYMIN {0,0}
/**
* @def PBFDAF_PBFDAFALPHA_HB_ARRAYMAX
* MaxValue of PbfdafAlpha_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_HB_ARRAYMAX {32767,32767}
/**
* @def PBFDAF_PBFDAFALPHA_HB_LENGTH
* Length of PbfdafAlpha_HB
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAFALPHA_HB_LENGTH (2)

/**
* @def PBFDAF_PBFDAF_LFCUTOFF_DEFAULT
* Default of Pbfdaf_LfCutoff
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAF_LFCUTOFF_DEFAULT (1000)
/**
* @def PBFDAF_PBFDAF_LFCUTOFF_MIN
* MinValue of Pbfdaf_LfCutoff
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAF_LFCUTOFF_MIN (0)
/**
* @def PBFDAF_PBFDAF_LFCUTOFF_MAX
* MaxValue of Pbfdaf_LfCutoff
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAF_LFCUTOFF_MAX (8000)

/**
* @def PBFDAF_PBFDAF_LBCUTOFF_DEFAULT
* Default of Pbfdaf_LbCutoff
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAF_LBCUTOFF_DEFAULT (4000)
/**
* @def PBFDAF_PBFDAF_LBCUTOFF_MIN
* MinValue of Pbfdaf_LbCutoff
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAF_LBCUTOFF_MIN (0)
/**
* @def PBFDAF_PBFDAF_LBCUTOFF_MAX
* MaxValue of Pbfdaf_LbCutoff
* @see Pbfdaf_ControlParams_st
*/
#define PBFDAF_PBFDAF_LBCUTOFF_MAX (8000)

/**
* @def ANTIHOWLING_FEEDBACKDELAY_DEFAULT
* Default of FeedbackDelay
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_FEEDBACKDELAY_DEFAULT (0)
/**
* @def ANTIHOWLING_FEEDBACKDELAY_MIN
* MinValue of FeedbackDelay
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_FEEDBACKDELAY_MIN (0)
/**
* @def ANTIHOWLING_FEEDBACKDELAY_MAX
* MaxValue of FeedbackDelay
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_FEEDBACKDELAY_MAX (19200)

/**
* @def ANTIHOWLING_AH_REF_HPF_DEFAULT
* Default of AH_REF_Hpf
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_AH_REF_HPF_DEFAULT (800)
/**
* @def ANTIHOWLING_AH_REF_HPF_MIN
* MinValue of AH_REF_Hpf
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_AH_REF_HPF_MIN (50)
/**
* @def ANTIHOWLING_AH_REF_HPF_MAX
* MaxValue of AH_REF_Hpf
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_AH_REF_HPF_MAX (1500)

/**
* @def ANTIHOWLING_SHR_MIN_GAIN_DEFAULT
* Default of SHR_Min_Gain
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_MIN_GAIN_DEFAULT (33)
/**
* @def ANTIHOWLING_SHR_MIN_GAIN_MIN
* MinValue of SHR_Min_Gain
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_MIN_GAIN_MIN (0)
/**
* @def ANTIHOWLING_SHR_MIN_GAIN_MAX
* MaxValue of SHR_Min_Gain
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_MIN_GAIN_MAX (32767)

/**
* @def ANTIHOWLING_SHR_GAMMA_MASK_DEFAULT
* Default of SHR_Gamma_Mask
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_MASK_DEFAULT (768)
/**
* @def ANTIHOWLING_SHR_GAMMA_MASK_MIN
* MinValue of SHR_Gamma_Mask
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_MASK_MIN (0)
/**
* @def ANTIHOWLING_SHR_GAMMA_MASK_MAX
* MaxValue of SHR_Gamma_Mask
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_MASK_MAX (32767)

/**
* @def ANTIHOWLING_SHR_GAMMA_STD_DEFAULT
* Default of SHR_Gamma_Std
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_STD_DEFAULT (256)
/**
* @def ANTIHOWLING_SHR_GAMMA_STD_MIN
* MinValue of SHR_Gamma_Std
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_STD_MIN (0)
/**
* @def ANTIHOWLING_SHR_GAMMA_STD_MAX
* MaxValue of SHR_Gamma_Std
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_STD_MAX (32767)

/**
* @def ANTIHOWLING_SHR_ENERGY_PERCENTILE_DEFAULT
* Default of SHR_Energy_Percentile
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_ENERGY_PERCENTILE_DEFAULT (50)
/**
* @def ANTIHOWLING_SHR_ENERGY_PERCENTILE_MIN
* MinValue of SHR_Energy_Percentile
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_ENERGY_PERCENTILE_MIN (5)
/**
* @def ANTIHOWLING_SHR_ENERGY_PERCENTILE_MAX
* MaxValue of SHR_Energy_Percentile
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_ENERGY_PERCENTILE_MAX (95)

/**
* @def ANTIHOWLING_SHR_LOWER_LIMIT_DEFAULT
* Default of SHR_Lower_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_LOWER_LIMIT_DEFAULT (1024)
/**
* @def ANTIHOWLING_SHR_LOWER_LIMIT_MIN
* MinValue of SHR_Lower_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_LOWER_LIMIT_MIN (0)
/**
* @def ANTIHOWLING_SHR_LOWER_LIMIT_MAX
* MaxValue of SHR_Lower_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_LOWER_LIMIT_MAX (32767)

/**
* @def ANTIHOWLING_SHR_UPPER_LIMIT_DEFAULT
* Default of SHR_Upper_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_UPPER_LIMIT_DEFAULT (2048)
/**
* @def ANTIHOWLING_SHR_UPPER_LIMIT_MIN
* MinValue of SHR_Upper_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_UPPER_LIMIT_MIN (0)
/**
* @def ANTIHOWLING_SHR_UPPER_LIMIT_MAX
* MaxValue of SHR_Upper_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_UPPER_LIMIT_MAX (32767)

/**
* @def ANTIHOWLING_SHR_GAMMA_PERSISTENCE_DEFAULT
* Default of SHR_Gamma_Persistence
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_PERSISTENCE_DEFAULT (384)
/**
* @def ANTIHOWLING_SHR_GAMMA_PERSISTENCE_MIN
* MinValue of SHR_Gamma_Persistence
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_PERSISTENCE_MIN (0)
/**
* @def ANTIHOWLING_SHR_GAMMA_PERSISTENCE_MAX
* MaxValue of SHR_Gamma_Persistence
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAMMA_PERSISTENCE_MAX (32767)

/**
* @def ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_DEFAULT
* Default of SHR_SpeechProtect_Freq_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_DEFAULT (2000)
/**
* @def ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_MIN
* MinValue of SHR_SpeechProtect_Freq_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_MIN (0)
/**
* @def ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_MAX
* MaxValue of SHR_SpeechProtect_Freq_Limit
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_MAX (4000)

/**
* @def ANTIHOWLING_AH_FBE_ERL_DEFAULT
* Default of AH_FBE_Erl
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_AH_FBE_ERL_DEFAULT (64)
/**
* @def ANTIHOWLING_AH_FBE_ERL_MIN
* MinValue of AH_FBE_Erl
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_AH_FBE_ERL_MIN (64)
/**
* @def ANTIHOWLING_AH_FBE_ERL_MAX
* MaxValue of AH_FBE_Erl
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_AH_FBE_ERL_MAX (32767)

/**
* @def ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_DEFAULT
* Default of SHR_Gain_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_DEFAULT (16384)
/**
* @def ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_MIN
* MinValue of SHR_Gain_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_MIN (0)
/**
* @def ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_MAX
* MaxValue of SHR_Gain_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_MAX (32767)

/**
* @def ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_DEFAULT
* Default of SHR_Energy_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_DEFAULT (33)
/**
* @def ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_MIN
* MinValue of SHR_Energy_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_MIN (0)
/**
* @def ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_MAX
* MaxValue of SHR_Energy_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_MAX (32767)

/**
* @def ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_DEFAULT
* Default of SHR_Candidates_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_DEFAULT (164)
/**
* @def ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_MIN
* MinValue of SHR_Candidates_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_MIN (0)
/**
* @def ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_MAX
* MaxValue of SHR_Candidates_Smoothing_Factor
* @see AntiHowling_ControlParams_st
*/
#define ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_MAX (32767)

/**
* @def LVNV_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVNV_ControlParams_st
*/
#define LVNV_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVNV_CONFIG_DEFAULT
* Default of Config
* @see LVNV_ControlParams_st
*/
#define LVNV_CONFIG_DEFAULT (LVM_CONFIG_HANDSET)

/**
* @def LVNV_MODE_DEFAULT
* Default of Mode
* @see LVNV_ControlParams_st
*/
#define LVNV_MODE_DEFAULT (8191)

/**
* @def LVNV_MODE2_DEFAULT
* Default of Mode2
* @see LVNV_ControlParams_st
*/
#define LVNV_MODE2_DEFAULT (1646)

/**
* @def LVNV_MODE3_DEFAULT
* Default of Mode3
* @see LVNV_ControlParams_st
*/
#define LVNV_MODE3_DEFAULT (0)

/**
* @def LVNV_TUNINGMODE_DEFAULT
* Default of TuningMode
* @see LVNV_ControlParams_st
*/
#define LVNV_TUNINGMODE_DEFAULT (0)

/**
* @def LVNV_MICROPHONEMODE_DEFAULT
* Default of MicrophoneMode
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEMODE_DEFAULT {83,75}
/**
* @def LVNV_MICROPHONEMODE_LENGTH
* Length of MicrophoneMode
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEMODE_LENGTH (2)

/**
* @def LVNV_MICROPHONEPOSITIONX_DEFAULT
* Default of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_DEFAULT {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONX_ARRAYMIN
* MinValue of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONX_ARRAYMAX
* MaxValue of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_ARRAYMAX {400,400}
/**
* @def LVNV_MICROPHONEPOSITIONX_LENGTH
* Length of MicrophonePositionX
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONX_LENGTH (2)

/**
* @def LVNV_MICROPHONEPOSITIONY_DEFAULT
* Default of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_DEFAULT {0,90}
/**
* @def LVNV_MICROPHONEPOSITIONY_ARRAYMIN
* MinValue of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONY_ARRAYMAX
* MaxValue of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_ARRAYMAX {400,400}
/**
* @def LVNV_MICROPHONEPOSITIONY_LENGTH
* Length of MicrophonePositionY
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONY_LENGTH (2)

/**
* @def LVNV_MICROPHONEPOSITIONZ_DEFAULT
* Default of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_DEFAULT {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONZ_ARRAYMIN
* MinValue of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_ARRAYMIN {0,0}
/**
* @def LVNV_MICROPHONEPOSITIONZ_ARRAYMAX
* MaxValue of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_ARRAYMAX {400,400}
/**
* @def LVNV_MICROPHONEPOSITIONZ_LENGTH
* Length of MicrophonePositionZ
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEPOSITIONZ_LENGTH (2)

/**
* @def LVNV_CHANNELTYPE_DEFAULT
* Default of ChannelType
* @see LVNV_ControlParams_st
*/
#define LVNV_CHANNELTYPE_DEFAULT {LVM_MICROPHONE_CHANNEL,LVM_MICROPHONE_CHANNEL}
/**
* @def LVNV_CHANNELTYPE_LENGTH
* Length of ChannelType
* @see LVNV_ControlParams_st
*/
#define LVNV_CHANNELTYPE_LENGTH (2)

/**
* @def LVNV_MICROPHONEINPUTGAIN_DEFAULT
* Default of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_DEFAULT {0,0}
/**
* @def LVNV_MICROPHONEINPUTGAIN_ARRAYMIN
* MinValue of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_ARRAYMIN {-48,-48}
/**
* @def LVNV_MICROPHONEINPUTGAIN_ARRAYMAX
* MaxValue of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_ARRAYMAX {12,12}
/**
* @def LVNV_MICROPHONEINPUTGAIN_LENGTH
* Length of MicrophoneInputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONEINPUTGAIN_LENGTH (2)

/**
* @def LVNV_OUTPUTGAIN_DEFAULT
* Default of OutputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_OUTPUTGAIN_DEFAULT (0)
/**
* @def LVNV_OUTPUTGAIN_MIN
* MinValue of OutputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_OUTPUTGAIN_MIN (-48)
/**
* @def LVNV_OUTPUTGAIN_MAX
* MaxValue of OutputGain
* @see LVNV_ControlParams_st
*/
#define LVNV_OUTPUTGAIN_MAX (24)

/**
* @def LVNV_PRIMMICCOVGAIN_DEFAULT
* Default of PrimMicCovGain
* @see LVNV_ControlParams_st
*/
#define LVNV_PRIMMICCOVGAIN_DEFAULT (10)
/**
* @def LVNV_PRIMMICCOVGAIN_MIN
* MinValue of PrimMicCovGain
* @see LVNV_ControlParams_st
*/
#define LVNV_PRIMMICCOVGAIN_MIN (-24)
/**
* @def LVNV_PRIMMICCOVGAIN_MAX
* MaxValue of PrimMicCovGain
* @see LVNV_ControlParams_st
*/
#define LVNV_PRIMMICCOVGAIN_MAX (24)

/**
* @def LVNV_AECPARAMS_LENGTH
* Length of AECParams
* @see LVNV_ControlParams_st
*/
#define LVNV_AECPARAMS_LENGTH (2)











/**
* @def LVBD_BD_OPERATINGMODE_DEFAULT
* Default of BD_OperatingMode
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVBD_BULKDELAY_DEFAULT
* Default of BulkDelay
* @see LVBD_ControlParams_st
*/
#define LVBD_BULKDELAY_DEFAULT (0)
/**
* @def LVBD_BULKDELAY_MIN
* MinValue of BulkDelay
* @see LVBD_ControlParams_st
*/
#define LVBD_BULKDELAY_MIN (0)
/**
* @def LVBD_BULKDELAY_MAX
* MaxValue of BulkDelay
* @see LVBD_ControlParams_st
*/
#define LVBD_BULKDELAY_MAX (6400)

/**
* @def LVBD_BD_GAIN_DEFAULT
* Default of BD_Gain
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_GAIN_DEFAULT (8192)
/**
* @def LVBD_BD_GAIN_MIN
* MinValue of BD_Gain
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_GAIN_MIN (0)
/**
* @def LVBD_BD_GAIN_MAX
* MaxValue of BD_Gain
* @see LVBD_ControlParams_st
*/
#define LVBD_BD_GAIN_MAX (32767)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_0_DEFAULT
* Default of MicrophoneChannel_0
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_0_DEFAULT (LVBUFFER_CHANNEL_0)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_1_DEFAULT
* Default of MicrophoneChannel_1
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_1_DEFAULT (LVBUFFER_CHANNEL_1)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_2_DEFAULT
* Default of MicrophoneChannel_2
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_2_DEFAULT (LVBUFFER_CHANNEL_2)

/**
* @def LVINPUTROUTING_TX_MICROPHONECHANNEL_3_DEFAULT
* Default of MicrophoneChannel_3
* @see LVINPUTROUTING_Tx_ControlParams_st
*/
#define LVINPUTROUTING_TX_MICROPHONECHANNEL_3_DEFAULT (LVBUFFER_CHANNEL_3)

/**
* @def LVREFERENCEROUTING_TX_REFERENCECHANNEL_0_DEFAULT
* Default of ReferenceChannel_0
* @see LVREFERENCEROUTING_Tx_ControlParams_st
*/
#define LVREFERENCEROUTING_TX_REFERENCECHANNEL_0_DEFAULT (LVVE_TX_REFERENCE_CHANNEL_0)

/**
* @def LVREFERENCEROUTING_TX_REFERENCECHANNEL_1_DEFAULT
* Default of ReferenceChannel_1
* @see LVREFERENCEROUTING_Tx_ControlParams_st
*/
#define LVREFERENCEROUTING_TX_REFERENCECHANNEL_1_DEFAULT (LVVE_TX_REFERENCE_CHANNEL_1)

/**
* @def LVOUTPUTROUTING_TX_OUTPUTCHANNEL_0_DEFAULT
* Default of OutputChannel_0
* @see LVOUTPUTROUTING_Tx_ControlParams_st
*/
#define LVOUTPUTROUTING_TX_OUTPUTCHANNEL_0_DEFAULT (LVVE_TX_OUTPUT_CHANNEL_0)

/**
* @def LVLPF_LPF_OPERATINGMODE_DEFAULT
* Default of LPF_OperatingMode
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVLPF_LPF_CORNERFREQ_DEFAULT
* Default of LPF_CornerFreq
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_CORNERFREQ_DEFAULT (3700)
/**
* @def LVLPF_LPF_CORNERFREQ_MIN
* MinValue of LPF_CornerFreq
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_CORNERFREQ_MIN (3500)
/**
* @def LVLPF_LPF_CORNERFREQ_MAX
* MaxValue of LPF_CornerFreq
* @see LVLPF_ControlParams_st
*/
#define LVLPF_LPF_CORNERFREQ_MAX (23400)

/**
* @def LVHPFREF_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVHPFREF_ControlParams_st
*/
#define LVHPFREF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVHPFREF_CORNERFREQ_DEFAULT
* Default of CornerFreq
* @see LVHPFREF_ControlParams_st
*/
#define LVHPFREF_CORNERFREQ_DEFAULT (700)
/**
* @def LVHPFREF_CORNERFREQ_MIN
* MinValue of CornerFreq
* @see LVHPFREF_ControlParams_st
*/
#define LVHPFREF_CORNERFREQ_MIN (100)
/**
* @def LVHPFREF_CORNERFREQ_MAX
* MaxValue of CornerFreq
* @see LVHPFREF_ControlParams_st
*/
#define LVHPFREF_CORNERFREQ_MAX (2000)




/**
* @def LVVE_TX_OPERATINGMODE_DEFAULT
* Default of OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_OPERATINGMODE_DEFAULT (LVM_MODE_ON)

/**
* @def LVVE_TX_MUTE_DEFAULT
* Default of Mute
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MUTE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_BD_OPERATINGMODE_DEFAULT
* Default of BD_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_BULKDELAY_DEFAULT
* Default of BulkDelay
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BULKDELAY_DEFAULT (0)
/**
* @def LVVE_TX_BULKDELAY_MIN
* MinValue of BulkDelay
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BULKDELAY_MIN (0)
/**
* @def LVVE_TX_BULKDELAY_MAX
* MaxValue of BulkDelay
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BULKDELAY_MAX (6400)

/**
* @def LVVE_TX_BD_GAIN_DEFAULT
* Default of BD_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_GAIN_DEFAULT (8192)
/**
* @def LVVE_TX_BD_GAIN_MIN
* MinValue of BD_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_GAIN_MIN (0)
/**
* @def LVVE_TX_BD_GAIN_MAX
* MaxValue of BD_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_BD_GAIN_MAX (32767)

/**
* @def LVVE_TX_VOL_OPERATINGMODE_DEFAULT
* Default of VOL_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_VOL_GAIN_DEFAULT
* Default of VOL_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_GAIN_DEFAULT (0)
/**
* @def LVVE_TX_VOL_GAIN_MIN
* MinValue of VOL_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_GAIN_MIN (-96)
/**
* @def LVVE_TX_VOL_GAIN_MAX
* MaxValue of VOL_Gain
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_VOL_GAIN_MAX (24)

/**
* @def LVVE_TX_HPF_OPERATINGMODE_DEFAULT
* Default of HPF_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_HPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_MIC_HPF_CORNERFREQ_DEFAULT
* Default of MIC_HPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_HPF_CORNERFREQ_DEFAULT (50)
/**
* @def LVVE_TX_MIC_HPF_CORNERFREQ_MIN
* MinValue of MIC_HPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_HPF_CORNERFREQ_MIN (50)
/**
* @def LVVE_TX_MIC_HPF_CORNERFREQ_MAX
* MaxValue of MIC_HPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_HPF_CORNERFREQ_MAX (1500)


/**
* @def LVVE_TX_LPF_OPERATINGMODE_DEFAULT
* Default of LPF_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_LPF_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)

/**
* @def LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT
* Default of MIC_LPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT (3700)
/**
* @def LVVE_TX_MIC_LPF_CORNERFREQ_MIN
* MinValue of MIC_LPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_LPF_CORNERFREQ_MIN (3500)
/**
* @def LVVE_TX_MIC_LPF_CORNERFREQ_MAX
* MaxValue of MIC_LPF_CornerFreq
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_MIC_LPF_CORNERFREQ_MAX (23400)



/**
* @def LVVE_TX_EQ_OPERATINGMODE_DEFAULT
* Default of EQ_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_EQ_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)



/**
* @def LVVE_TX_CNG_OPERATINGMODE_DEFAULT
* Default of CNG_OperatingMode
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_CNG_OPERATINGMODE_DEFAULT (LVM_MODE_OFF)


/**
* @def LVAVC_MODULESTATUS_DEFAULT
* Default of ModuleStatus
* @see LVAVC_ControlParams_st
*/
#define LVAVC_MODULESTATUS_DEFAULT (0)

/**
* @def LVAVC_VADDECISION_DEFAULT
* Default of VadDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VADDECISION_DEFAULT (0)
/**
* @def LVAVC_VADDECISION_MIN
* MinValue of VadDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VADDECISION_MIN (0)
/**
* @def LVAVC_VADDECISION_MAX
* MaxValue of VadDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VADDECISION_MAX (1)

/**
* @def LVAVC_VOICINGPROBABILITYDECISION_DEFAULT
* Default of VoicingProbabilityDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VOICINGPROBABILITYDECISION_DEFAULT (0)
/**
* @def LVAVC_VOICINGPROBABILITYDECISION_MIN
* MinValue of VoicingProbabilityDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VOICINGPROBABILITYDECISION_MIN (0)
/**
* @def LVAVC_VOICINGPROBABILITYDECISION_MAX
* MaxValue of VoicingProbabilityDecision
* @see LVAVC_ControlParams_st
*/
#define LVAVC_VOICINGPROBABILITYDECISION_MAX (3)

/**
* @def LVAVC_NOISEADAPTIVEGAINS_DEFAULT
* Default of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_DEFAULT {0,0,0}
/**
* @def LVAVC_NOISEADAPTIVEGAINS_ARRAYMIN
* MinValue of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_NOISEADAPTIVEGAINS_ARRAYMAX
* MaxValue of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_NOISEADAPTIVEGAINS_LENGTH
* Length of NoiseAdaptiveGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_NOISEADAPTIVEGAINS_LENGTH (3)

/**
* @def LVAVC_BANDWEIGHTINGS_DEFAULT
* Default of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_DEFAULT {32767,32767,32767}
/**
* @def LVAVC_BANDWEIGHTINGS_ARRAYMIN
* MinValue of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_BANDWEIGHTINGS_ARRAYMAX
* MaxValue of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_BANDWEIGHTINGS_LENGTH
* Length of BandWeightings
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BANDWEIGHTINGS_LENGTH (3)

/**
* @def LVAVC_COMPRESSIONGAINS_DEFAULT
* Default of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_DEFAULT {0,0,0}
/**
* @def LVAVC_COMPRESSIONGAINS_ARRAYMIN
* MinValue of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_COMPRESSIONGAINS_ARRAYMAX
* MaxValue of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_COMPRESSIONGAINS_LENGTH
* Length of CompressionGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_COMPRESSIONGAINS_LENGTH (3)

/**
* @def LVAVC_OUTPUTGAINS_DEFAULT
* Default of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_DEFAULT {0,0,0}
/**
* @def LVAVC_OUTPUTGAINS_ARRAYMIN
* MinValue of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_ARRAYMIN {0,0,0}
/**
* @def LVAVC_OUTPUTGAINS_ARRAYMAX
* MaxValue of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_ARRAYMAX {32767,32767,32767}
/**
* @def LVAVC_OUTPUTGAINS_LENGTH
* Length of OutputGains
* @see LVAVC_ControlParams_st
*/
#define LVAVC_OUTPUTGAINS_LENGTH (3)

/**
* @def LVAVC_N_FORMANTS_DEFAULT
* Default of N_formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_N_FORMANTS_DEFAULT (0)
/**
* @def LVAVC_N_FORMANTS_MIN
* MinValue of N_formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_N_FORMANTS_MIN (0)
/**
* @def LVAVC_N_FORMANTS_MAX
* MaxValue of N_formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_N_FORMANTS_MAX (8)

/**
* @def LVAVC_GAIN_FORMANTS_DEFAULT
* Default of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_GAIN_FORMANTS_ARRAYMIN
* MinValue of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_GAIN_FORMANTS_ARRAYMAX
* MaxValue of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVAVC_GAIN_FORMANTS_LENGTH
* Length of Gain_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_GAIN_FORMANTS_LENGTH (8)

/**
* @def LVAVC_BIN_FORMANTS_DEFAULT
* Default of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_BIN_FORMANTS_ARRAYMIN
* MinValue of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}
/**
* @def LVAVC_BIN_FORMANTS_ARRAYMAX
* MaxValue of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def LVAVC_BIN_FORMANTS_LENGTH
* Length of Bin_Formants
* @see LVAVC_ControlParams_st
*/
#define LVAVC_BIN_FORMANTS_LENGTH (8)

/**
* @def LVWM_AVL_GAIN_DEFAULT
* Default of AVL_Gain
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_GAIN_DEFAULT (0)
/**
* @def LVWM_AVL_GAIN_MIN
* MinValue of AVL_Gain
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_GAIN_MIN (0)
/**
* @def LVWM_AVL_GAIN_MAX
* MaxValue of AVL_Gain
* @see LVWM_ControlParams_st
*/
#define LVWM_AVL_GAIN_MAX (32767)



/**
* @def LVVE_RX_DUMMY_DEFAULT
* Default of dummy
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_DUMMY_DEFAULT (0)
/**
* @def LVVE_RX_DUMMY_MIN
* MinValue of dummy
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_DUMMY_MIN (-128)
/**
* @def LVVE_RX_DUMMY_MAX
* MaxValue of dummy
* @see LVVE_Rx_ControlParams_st
*/
#define LVVE_RX_DUMMY_MAX (127)

/**
* @def MICROPHONE_NLMS_LB_COEFF_DEFAULT
* Default of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def MICROPHONE_NLMS_LB_COEFF_ARRAYMIN
* MinValue of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}
/**
* @def MICROPHONE_NLMS_LB_COEFF_ARRAYMAX
* MaxValue of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def MICROPHONE_NLMS_LB_COEFF_LENGTH
* Length of NLMS_LB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_LB_COEFF_LENGTH (128)

/**
* @def MICROPHONE_NLMS_HB_COEFF_DEFAULT
* Default of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def MICROPHONE_NLMS_HB_COEFF_ARRAYMIN
* MinValue of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}
/**
* @def MICROPHONE_NLMS_HB_COEFF_ARRAYMAX
* MaxValue of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def MICROPHONE_NLMS_HB_COEFF_LENGTH
* Length of NLMS_HB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_NLMS_HB_COEFF_LENGTH (128)

/**
* @def MICROPHONE_FSB_COEFF_DEFAULT
* Default of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def MICROPHONE_FSB_COEFF_ARRAYMIN
* MinValue of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}
/**
* @def MICROPHONE_FSB_COEFF_ARRAYMAX
* MaxValue of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}
/**
* @def MICROPHONE_FSB_COEFF_LENGTH
* Length of FSB_Coeff
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_FSB_COEFF_LENGTH (24)

/**
* @def MICROPHONE_LOWFREQUENCYENERGY_DEFAULT
* Default of LowFrequencyEnergy
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_LOWFREQUENCYENERGY_DEFAULT (0)
/**
* @def MICROPHONE_LOWFREQUENCYENERGY_MIN
* MinValue of LowFrequencyEnergy
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_LOWFREQUENCYENERGY_MIN (0)
/**
* @def MICROPHONE_LOWFREQUENCYENERGY_MAX
* MaxValue of LowFrequencyEnergy
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_LOWFREQUENCYENERGY_MAX (32767)

/**
* @def MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_DEFAULT
* Default of WNS_Cutoff_EnergyLevel
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_DEFAULT (0)
/**
* @def MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_MIN
* MinValue of WNS_Cutoff_EnergyLevel
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_MIN (-32768)
/**
* @def MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_MAX
* MaxValue of WNS_Cutoff_EnergyLevel
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_MAX (32767)

/**
* @def MICROPHONE_AMS_COVEREDFRAMES_DEFAULT
* Default of AMS_CoveredFrames
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_AMS_COVEREDFRAMES_DEFAULT (0)
/**
* @def MICROPHONE_AMS_COVEREDFRAMES_MIN
* MinValue of AMS_CoveredFrames
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_AMS_COVEREDFRAMES_MIN (0)
/**
* @def MICROPHONE_AMS_COVEREDFRAMES_MAX
* MaxValue of AMS_CoveredFrames
* @see Microphone_ControlParams_st
*/
#define MICROPHONE_AMS_COVEREDFRAMES_MAX (4294967295)

/**
* @def LVNV_STATUS_DEFAULT
* Default of Status
* @see LVNV_ControlParams_st
*/
#define LVNV_STATUS_DEFAULT (0)

/**
* @def LVNV_BROADSIDE_ICC_DEFAULT
* Default of Broadside_ICC
* @see LVNV_ControlParams_st
*/
#define LVNV_BROADSIDE_ICC_DEFAULT (0)
/**
* @def LVNV_BROADSIDE_ICC_MIN
* MinValue of Broadside_ICC
* @see LVNV_ControlParams_st
*/
#define LVNV_BROADSIDE_ICC_MIN (0)
/**
* @def LVNV_BROADSIDE_ICC_MAX
* MaxValue of Broadside_ICC
* @see LVNV_ControlParams_st
*/
#define LVNV_BROADSIDE_ICC_MAX (32767)

/**
* @def LVNV_PCD_COEFF_DEFAULT
* Default of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}
/**
* @def LVNV_PCD_COEFF_ARRAYMIN
* MinValue of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}
/**
* @def LVNV_PCD_COEFF_ARRAYMAX
* MaxValue of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}
/**
* @def LVNV_PCD_COEFF_LENGTH
* Length of PCD_Coeff
* @see LVNV_ControlParams_st
*/
#define LVNV_PCD_COEFF_LENGTH (64)

/**
* @def LVNV_SDET_FAREND_DETECT_DEFAULT
* Default of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_DEFAULT {0,0}
/**
* @def LVNV_SDET_FAREND_DETECT_ARRAYMIN
* MinValue of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_ARRAYMIN {0,0}
/**
* @def LVNV_SDET_FAREND_DETECT_ARRAYMAX
* MaxValue of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_ARRAYMAX {32767,32767}
/**
* @def LVNV_SDET_FAREND_DETECT_LENGTH
* Length of SDET_FarEnd_Detect
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_FAREND_DETECT_LENGTH (2)

/**
* @def LVNV_DNNS_GAMMAE_DEFAULT
* Default of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_DEFAULT {0,0}
/**
* @def LVNV_DNNS_GAMMAE_ARRAYMIN
* MinValue of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_ARRAYMIN {0,0}
/**
* @def LVNV_DNNS_GAMMAE_ARRAYMAX
* MaxValue of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_ARRAYMAX {32767,32767}
/**
* @def LVNV_DNNS_GAMMAE_LENGTH
* Length of DNNS_GammaE
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_GAMMAE_LENGTH (2)

/**
* @def LVNV_SDET_COH_DEFAULT
* Default of SDET_Coh
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_COH_DEFAULT (0)
/**
* @def LVNV_SDET_COH_MIN
* MinValue of SDET_Coh
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_COH_MIN (0)
/**
* @def LVNV_SDET_COH_MAX
* MaxValue of SDET_Coh
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_COH_MAX (32767)

/**
* @def LVNV_SDET_IMCOH_FLOORCOMP_DEFAULT
* Default of SDET_IMCoh_floorcomp
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_IMCOH_FLOORCOMP_DEFAULT (0)
/**
* @def LVNV_SDET_IMCOH_FLOORCOMP_MIN
* MinValue of SDET_IMCoh_floorcomp
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_IMCOH_FLOORCOMP_MIN (0)
/**
* @def LVNV_SDET_IMCOH_FLOORCOMP_MAX
* MaxValue of SDET_IMCoh_floorcomp
* @see LVNV_ControlParams_st
*/
#define LVNV_SDET_IMCOH_FLOORCOMP_MAX (32767)

/**
* @def LVNV_DNNS_SPPRESPROBBROADBAND_DEFAULT
* Default of DNNS_SpPresProbBroadband
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_SPPRESPROBBROADBAND_DEFAULT (0)
/**
* @def LVNV_DNNS_SPPRESPROBBROADBAND_MIN
* MinValue of DNNS_SpPresProbBroadband
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_SPPRESPROBBROADBAND_MIN (0)
/**
* @def LVNV_DNNS_SPPRESPROBBROADBAND_MAX
* MaxValue of DNNS_SpPresProbBroadband
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_SPPRESPROBBROADBAND_MAX (32767)

/**
* @def LVNV_DNNS_MEANSNRLONG_DEFAULT
* Default of DNNS_meanSNRlong
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_MEANSNRLONG_DEFAULT (0)
/**
* @def LVNV_DNNS_MEANSNRLONG_MIN
* MinValue of DNNS_meanSNRlong
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_MEANSNRLONG_MIN (-32768)
/**
* @def LVNV_DNNS_MEANSNRLONG_MAX
* MaxValue of DNNS_meanSNRlong
* @see LVNV_ControlParams_st
*/
#define LVNV_DNNS_MEANSNRLONG_MAX (32767)

/**
* @def LVNV_WNS_WINDPROBABILITY_DEFAULT
* Default of WNS_WindProbability
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_WINDPROBABILITY_DEFAULT (0)
/**
* @def LVNV_WNS_WINDPROBABILITY_MIN
* MinValue of WNS_WindProbability
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_WINDPROBABILITY_MIN (0)
/**
* @def LVNV_WNS_WINDPROBABILITY_MAX
* MaxValue of WNS_WindProbability
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_WINDPROBABILITY_MAX (32767)

/**
* @def LVNV_WNS_CUTOFFFREQ_DEFAULT
* Default of WNS_CutoffFreq
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_CUTOFFFREQ_DEFAULT (0)
/**
* @def LVNV_WNS_CUTOFFFREQ_MIN
* MinValue of WNS_CutoffFreq
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_CUTOFFFREQ_MIN (0)
/**
* @def LVNV_WNS_CUTOFFFREQ_MAX
* MaxValue of WNS_CutoffFreq
* @see LVNV_ControlParams_st
*/
#define LVNV_WNS_CUTOFFFREQ_MAX (8000)

/**
* @def LVNV_AMS_STATUS_DEFAULT
* Default of AMS_Status
* @see LVNV_ControlParams_st
*/
#define LVNV_AMS_STATUS_DEFAULT (0)
/**
* @def LVNV_AMS_STATUS_MIN
* MinValue of AMS_Status
* @see LVNV_ControlParams_st
*/
#define LVNV_AMS_STATUS_MIN (0)
/**
* @def LVNV_AMS_STATUS_MAX
* MaxValue of AMS_Status
* @see LVNV_ControlParams_st
*/
#define LVNV_AMS_STATUS_MAX (4)

/**
* @def LVNV_DUMMY_DEFAULT
* Default of dummy
* @see LVNV_ControlParams_st
*/
#define LVNV_DUMMY_DEFAULT (0)
/**
* @def LVNV_DUMMY_MIN
* MinValue of dummy
* @see LVNV_ControlParams_st
*/
#define LVNV_DUMMY_MIN (-32768)
/**
* @def LVNV_DUMMY_MAX
* MaxValue of dummy
* @see LVNV_ControlParams_st
*/
#define LVNV_DUMMY_MAX (32767)

/**
* @def LVNV_MICROPHONESTATUS_LENGTH
* Length of MicrophoneStatus
* @see LVNV_ControlParams_st
*/
#define LVNV_MICROPHONESTATUS_LENGTH (2)

/**
* @def LVNV_AH_DELTRACK_DEFAULT
* Default of AH_DelTrack
* @see LVNV_ControlParams_st
*/
#define LVNV_AH_DELTRACK_DEFAULT (0)
/**
* @def LVNV_AH_DELTRACK_MIN
* MinValue of AH_DelTrack
* @see LVNV_ControlParams_st
*/
#define LVNV_AH_DELTRACK_MIN (0)
/**
* @def LVNV_AH_DELTRACK_MAX
* MaxValue of AH_DelTrack
* @see LVNV_ControlParams_st
*/
#define LVNV_AH_DELTRACK_MAX (32767)



/**
* @def LVVE_TX_DUMMY_DEFAULT
* Default of dummy
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_DUMMY_DEFAULT (0)
/**
* @def LVVE_TX_DUMMY_MIN
* MinValue of dummy
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_DUMMY_MIN (-128)
/**
* @def LVVE_TX_DUMMY_MAX
* MaxValue of dummy
* @see LVVE_Tx_ControlParams_st
*/
#define LVVE_TX_DUMMY_MAX (127)

/**
* Enum type for LVVIDHeader_MessageID_en
*/
typedef enum
{
    LVVE_RX_PRESET = 0, ///< VID header Message ID for LVVE_Rx presets
    LVVE_TX_PRESET = 1, ///< VID header Message ID for LVVE_Tx Presets
    LVVIDHEADER_MESSAGEID_EN_DUMMY = LVM_MAXENUM
} LVVIDHeader_MessageID_en;

/**
* Enum type for LVVIDHeader_ReturnStatus_en
*/
typedef enum
{
    LVVIDHEADER_NULLADDRESS = 0, ///< LVVIDHeader module returns NULL address error
    LVVIDHEADER_SUCCESS = 1, ///< LVVIDHeader module returns with success
    LVVIDHEADER_RETURNSTATUS_EN_DUMMY = LVM_MAXENUM
} LVVIDHeader_ReturnStatus_en;

/**
* Enum type for LVBuffer_Channel_en
*/
typedef enum
{
    LVBUFFER_CHANNEL_0 = 0, ///< First channel of the audio buffer
    LVBUFFER_CHANNEL_1 = 1, ///< Second channel of the audio buffer
    LVBUFFER_CHANNEL_2 = 2, ///< Third channel of the audio buffer
    LVBUFFER_CHANNEL_3 = 3, ///< Fourth channel of the audio buffer
    LVBUFFER_CHANNEL_EN_DUMMY = LVM_MAXENUM
} LVBuffer_Channel_en;

/**
* Enum type for LVVE_Tx_ReferenceChannel_en
*/
typedef enum
{
    LVVE_TX_REFERENCE_CHANNEL_0 = 0, ///< First channel of the echo reference audio buffer from VoiceExperience
    LVVE_TX_REFERENCE_CHANNEL_1 = 1, ///< Second channel of the echo reference audio buffer from VoiceExperience
    LVVE_TX_REFERENCE_CHANNEL_DOWNMIX = 100, ///< Sum of all channels of the reference audio buffer from VoiceExperience.
    LVVE_TX_REFERENCE_CHANNEL_ANTIHOWLING_FEEDBACK = 200, ///< External AntiHowling feedback audio buffer from the end of Tx audio chain
    LVVE_TX_REFERENCECHANNEL_EN_DUMMY = LVM_MAXENUM
} LVVE_Tx_ReferenceChannel_en;

/**
* Enum type for LVVE_Tx_OutputChannel_en
*/
typedef enum
{
    LVVE_TX_OUTPUT_CHANNEL_0 = 0, ///< First channel of the processed audio buffer from VoiceExperience
    LVVE_TX_OUTPUT_REFERENCE_CHANNEL_0 = 100, ///< First channel of the audio buffer returned by the AEC Reference FIFO, processed with the LVBD module.
    LVVE_TX_OUTPUTCHANNEL_EN_DUMMY = LVM_MAXENUM
} LVVE_Tx_OutputChannel_en;

/****************************************************************************************/
/*                                                                                      */
/*  Structures                                                                          */
/*                                                                                      */
/****************************************************************************************/
/**
* The status parameters are used to retrieve the status of the module.
* @see LVAVC_GetStatusParameters
*/
typedef struct
{
    /**
    Status of the LVAVC module: Current Operating mode and ModeWord combined.<br>
    */
    LVAVC_ModeWord_bm ModuleStatus;          ///< Module status

    /**
    Current VAD decision.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_VADDECISION_MIN (0)</td>
        <td>@ref LVAVC_VADDECISION_DEFAULT (0)</td>
        <td>@ref LVAVC_VADDECISION_MAX (1)</td>
    </tr>
    </table>
    */
    LVM_INT16 VadDecision;          ///< VAD decision

    /**
    Current Voicing Probability and SNR decisions.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_VOICINGPROBABILITYDECISION_MIN (0)</td>
        <td>@ref LVAVC_VOICINGPROBABILITYDECISION_DEFAULT (0)</td>
        <td>@ref LVAVC_VOICINGPROBABILITYDECISION_MAX (3)</td>
    </tr>
    </table>
    */
    LVM_INT16 VoicingProbabilityDecision;          ///< Voicing Probability and SNR decisions

    /**
    Gain of Noise Adaptive Downlink modules.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref LVAVC_NOISEADAPTIVEGAINS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_NOISEADAPTIVEGAINS_DEFAULT {0,0,0}</td>
        <td>@ref LVAVC_NOISEADAPTIVEGAINS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 NoiseAdaptiveGains[LVAVC_NOISEADAPTIVEGAINS_LENGTH];          ///< Noise Adaptive Gains

    /**
    Band Weightings.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVAVC_BANDWEIGHTINGS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_BANDWEIGHTINGS_DEFAULT {32767,32767,32767}</td>
        <td>@ref LVAVC_BANDWEIGHTINGS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 BandWeightings[LVAVC_BANDWEIGHTINGS_LENGTH];          ///< Band Weightings

    /**
    Compression Gains.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVAVC_COMPRESSIONGAINS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_COMPRESSIONGAINS_DEFAULT {0,0,0}</td>
        <td>@ref LVAVC_COMPRESSIONGAINS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 CompressionGains[LVAVC_COMPRESSIONGAINS_LENGTH];          ///< Compression Gains

    /**
    Otput Gains.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref LVAVC_OUTPUTGAINS_ARRAYMIN {0,0,0}</td>
        <td>@ref LVAVC_OUTPUTGAINS_DEFAULT {0,0,0}</td>
        <td>@ref LVAVC_OUTPUTGAINS_ARRAYMAX {32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 OutputGains[LVAVC_OUTPUTGAINS_LENGTH];          ///< Output Gains

    /**
    Number of formants.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_N_FORMANTS_MIN (0)</td>
        <td>@ref LVAVC_N_FORMANTS_DEFAULT (0)</td>
        <td>@ref LVAVC_N_FORMANTS_MAX (8)</td>
    </tr>
    </table>
    */
    LVM_INT16 N_formants;          ///< Number of formants

    /**
    Gain of Formants.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVAVC_GAIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_GAIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_GAIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 Gain_Formants[LVAVC_GAIN_FORMANTS_LENGTH];          ///< Formants Gain

    /**
    Bin of Formants.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_BIN_FORMANTS_ARRAYMIN {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_BIN_FORMANTS_DEFAULT {0,0,0,0,0,0,0,0}</td>
        <td>@ref LVAVC_BIN_FORMANTS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 Bin_Formants[LVAVC_BIN_FORMANTS_LENGTH];          ///< Formants Bin

} LVAVC_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVWM_GetStatusParameters
*/
typedef struct
{
    /**
    The internal gain setting used by WhisperMode in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_AVL_GAIN_MIN (0)</td>
        <td>@ref LVWM_AVL_GAIN_DEFAULT (0)</td>
        <td>@ref LVWM_AVL_GAIN_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 AVL_Gain;          ///< gain (dB)

} LVWM_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVVE_Rx_GetStatusParameters
*/
typedef struct
{
    /**
    Active Voice Contrast Status Parameter Structure.<br>
    */
    LVAVC_StatusParams_st AVC_StatusParams;          ///< Active Voice Contrast Status Parameter Structure

    /**
    Whisper Mode Status Parameter Structure.<br>
    */
    LVWM_StatusParams_st WM_StatusParams;          ///< Whisper Mode Status Parameter Structure

    /**
    Dummy Param to avoid an empty struct which is not allowed in C in some LVVE
    build configurations.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.7</td>
        <td>@ref LVVE_RX_DUMMY_MIN (-128)</td>
        <td>@ref LVVE_RX_DUMMY_DEFAULT (0)</td>
        <td>@ref LVVE_RX_DUMMY_MAX (127)</td>
    </tr>
    </table>
    */
    LVM_INT8 dummy;          ///< Dummy Param

} LVVE_Rx_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see Microphone_GetStatusParameters
*/
typedef struct
{
    /**
    Table of coefficients of the low-band NLMS filter for this microphone.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref MICROPHONE_NLMS_LB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}</td>
        <td>@ref MICROPHONE_NLMS_LB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref MICROPHONE_NLMS_LB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table>
    */
    LVM_INT32 NLMS_LB_Coeff[MICROPHONE_NLMS_LB_COEFF_LENGTH];          ///< NLMS low-band coefficients.

    /**
    Table of coefficients of the high-band NLMS filter for this microphone.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.30</td>
        <td>@ref MICROPHONE_NLMS_HB_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}</td>
        <td>@ref MICROPHONE_NLMS_HB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref MICROPHONE_NLMS_HB_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table>
    */
    LVM_INT32 NLMS_HB_Coeff[MICROPHONE_NLMS_HB_COEFF_LENGTH];          ///< NLMS high-band coefficients.

    /**
    Table of coefficients of FSB for this microphone.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref MICROPHONE_FSB_COEFF_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref MICROPHONE_FSB_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref MICROPHONE_FSB_COEFF_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 FSB_Coeff[MICROPHONE_FSB_COEFF_LENGTH];          ///< FSB coefficients.

    /**
    Low Frequency Energy used for wind noise suppression for this microphone. Can
    be used to tune WNS_EnergyScaleFactor. During clean speech-only segments its
    value should be around 819 (or 0.05 float) for each channel<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref MICROPHONE_LOWFREQUENCYENERGY_MIN (0)</td>
        <td>@ref MICROPHONE_LOWFREQUENCYENERGY_DEFAULT (0)</td>
        <td>@ref MICROPHONE_LOWFREQUENCYENERGY_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 LowFrequencyEnergy;          ///< Low Frequency Energy.

    /**
    Energy level for this microphone which can be used to tune
    WNS_CutOff_MinEnergyLevel and WNS_CutOff_MaxEnergyLevel for wind noise
    suppression. Unit in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_MIN (-32768)</td>
        <td>@ref MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_DEFAULT (0)</td>
        <td>@ref MICROPHONE_WNS_CUTOFF_ENERGYLEVEL_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 WNS_Cutoff_EnergyLevel;          ///< Energy level used for wind noise cut-off frequency.

    /**
    Counter indicating the amount of frames with coverage detection for this
    microphone.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>31.0</td>
        <td>@ref MICROPHONE_AMS_COVEREDFRAMES_MIN (0)</td>
        <td>@ref MICROPHONE_AMS_COVEREDFRAMES_DEFAULT (0)</td>
        <td>@ref MICROPHONE_AMS_COVEREDFRAMES_MAX (4294967295)</td>
    </tr>
    </table>
    */
    LVM_UINT32 AMS_CoveredFrames;          ///< Frame counter for mic coverage

} Microphone_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVNV_GetStatusParameters
*/
typedef struct
{
    /**
    Bit mask describing current state of LVNV, used for debugging.<br>
    */
    LVNV_StatusWord_bm Status;          ///< Status word

    /**
    Value of the broadside detector (soft detection).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_BROADSIDE_ICC_MIN (0)</td>
        <td>@ref LVNV_BROADSIDE_ICC_DEFAULT (0)</td>
        <td>@ref LVNV_BROADSIDE_ICC_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 Broadside_ICC;          ///< Broadside detection

    /**
    Table of coefficients of PCD filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.26</td>
        <td>@ref LVNV_PCD_COEFF_ARRAYMIN {-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647,-2147483647}</td>
        <td>@ref LVNV_PCD_COEFF_DEFAULT {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVNV_PCD_COEFF_ARRAYMAX {2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647,2147483647}</td>
    </tr>
    </table>
    */
    LVM_INT32 PCD_Coeff[LVNV_PCD_COEFF_LENGTH];          ///< PCD coefficients.

    /**
    Probability of far-end being present (soft detection) for each band.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SDET_FAREND_DETECT_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_SDET_FAREND_DETECT_DEFAULT {0,0}</td>
        <td>@ref LVNV_SDET_FAREND_DETECT_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 SDET_FarEnd_Detect[LVNV_SDET_FAREND_DETECT_LENGTH];          ///< Far-end detection for each band.

    /**
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVNV_DNNS_GAMMAE_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_DNNS_GAMMAE_DEFAULT {0,0}</td>
        <td>@ref LVNV_DNNS_GAMMAE_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    */
    LVM_INT16 DNNS_GammaE[LVNV_DNNS_GAMMAE_LENGTH];          ///< Oversubtraction factor for echo for each band.

    /**
    Inter-microphone coherence detection (soft detection).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SDET_COH_MIN (0)</td>
        <td>@ref LVNV_SDET_COH_DEFAULT (0)</td>
        <td>@ref LVNV_SDET_COH_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 SDET_Coh;          ///< Inter-microphone coherence.

    /**
    Floor compensation for inter-microphone coherence detection (soft detection) in
    low-band.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_SDET_IMCOH_FLOORCOMP_MIN (0)</td>
        <td>@ref LVNV_SDET_IMCOH_FLOORCOMP_DEFAULT (0)</td>
        <td>@ref LVNV_SDET_IMCOH_FLOORCOMP_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 SDET_IMCoh_floorcomp;          ///< Floor compensation for inter-microphone coherence

    /**
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_DNNS_SPPRESPROBBROADBAND_MIN (0)</td>
        <td>@ref LVNV_DNNS_SPPRESPROBBROADBAND_DEFAULT (0)</td>
        <td>@ref LVNV_DNNS_SPPRESPROBBROADBAND_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 DNNS_SpPresProbBroadband;          ///< Broadband speech presence probability for low-band.

    /**
    Long-term SNR-level averaged over frequency bins.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVNV_DNNS_MEANSNRLONG_MIN (-32768)</td>
        <td>@ref LVNV_DNNS_MEANSNRLONG_DEFAULT (0)</td>
        <td>@ref LVNV_DNNS_MEANSNRLONG_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 DNNS_meanSNRlong;          ///< Mean long-term SNR-level.

    /**
    Probability of wind noise presence. Can be used to tune WNS_DetectorThreshold.
    This status parameter is only valid when at least 2 input channels are
    provided.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_WNS_WINDPROBABILITY_MIN (0)</td>
        <td>@ref LVNV_WNS_WINDPROBABILITY_DEFAULT (0)</td>
        <td>@ref LVNV_WNS_WINDPROBABILITY_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 WNS_WindProbability;          ///< Probability of wind noise presence.

    /**
    Cut-off frequency for wind noise suppression. Unit in Hz. This can be used to
    validate the tuning of WNS_CutOff_MinEnergyLevel and WNS_CutOff_MaxEnergyLevel.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_WNS_CUTOFFFREQ_MIN (0)</td>
        <td>@ref LVNV_WNS_CUTOFFFREQ_DEFAULT (0)</td>
        <td>@ref LVNV_WNS_CUTOFFFREQ_MAX (8000)</td>
    </tr>
    </table>
    */
    LVM_INT16 WNS_CutoffFreq;          ///< Cut-off frequency for wind noise suppression.

    /**
    Indicates which microphone is covered if any; 0=uncovered; 1=primary;
    2=secondary .<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_AMS_STATUS_MIN (0)</td>
        <td>@ref LVNV_AMS_STATUS_DEFAULT (0)</td>
        <td>@ref LVNV_AMS_STATUS_MAX (4)</td>
    </tr>
    </table>
    */
    LVM_INT16 AMS_Status;          ///< Index of covered mic.

    /**
    dummy for 32-bit memory alignment.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_DUMMY_MIN (-32768)</td>
        <td>@ref LVNV_DUMMY_DEFAULT (0)</td>
        <td>@ref LVNV_DUMMY_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT16 dummy;          ///< dummy.

    /**
    Array of Microphone status. One status structure per microphone.<br>
    */
    Microphone_StatusParams_st MicrophoneStatus[LVNV_MICROPHONESTATUS_LENGTH];          ///< Microphone status.

    /**
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.15</td>
        <td>@ref LVNV_AH_DELTRACK_MIN (0)</td>
        <td>@ref LVNV_AH_DELTRACK_DEFAULT (0)</td>
        <td>@ref LVNV_AH_DELTRACK_MAX (32767)</td>
    </tr>
    </table>
    */
    LVM_INT32 AH_DelTrack;          ///< Tracked delay for Antihowling reference.

} LVNV_StatusParams_st;

/**
* The status parameters are used to retrieve the status of the module.
* @see LVVE_Tx_GetStatusParameters
*/
typedef struct
{
    /**
    NoiseVoid Status Parameter Structure.<br>
    */
    LVNV_StatusParams_st NV_StatusParams;          ///< NoiseVoid Status Parameters Structrue

    /**
    WhisperMode Status Parameter Structure.<br>
    */
    LVWM_StatusParams_st WM_StatusParams;          ///< WhisperMode Status Parameter Structrue

    /**
    Dummy Param to avoid an empty struct which is not allowed in C in some LVVE
    build configurations.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.7</td>
        <td>@ref LVVE_TX_DUMMY_MIN (-128)</td>
        <td>@ref LVVE_TX_DUMMY_DEFAULT (0)</td>
        <td>@ref LVVE_TX_DUMMY_MAX (127)</td>
    </tr>
    </table>
    */
    LVM_INT8 dummy;          ///< Dummy Param

} LVVE_Tx_StatusParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVIDHeader_SetControlParameters function but they
* will not take effect until the next call to the LVVIDHeader_Process function.
*
* @see LVVIDHeader_SetControlParameters
* @see LVVIDHeader_Process
*/
typedef struct
{
    /**
    This number always increments by one when there is change in the header format.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVIDHEADER_HEADERVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_HEADERVERSION_DEFAULT (4)</td>
        <td>@ref LVVIDHEADER_HEADERVERSION_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 HeaderVersion;          ///< Header Format Version

    /**
    This variable holds value of the Major baseline version.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_MAJORBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_MAJORBASELINEVERSION_DEFAULT (12)</td>
        <td>@ref LVVIDHEADER_MAJORBASELINEVERSION_MAX (255)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT8 MajorBaselineVersion;          ///< Major baseline version of the product

    /**
    This variable holds value of the Minor baseline version.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_MINORBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_MINORBASELINEVERSION_DEFAULT (5)</td>
        <td>@ref LVVIDHEADER_MINORBASELINEVERSION_MAX (255)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT8 MinorBaselineVersion;          ///< Minor baseline version of the product

    /**
    This variable holds value of the MinorMinor baseline version.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_MINORMINORBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_MINORMINORBASELINEVERSION_DEFAULT (1)</td>
        <td>@ref LVVIDHEADER_MINORMINORBASELINEVERSION_MAX (255)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT8 MinorMinorBaselineVersion;          ///< MinorMinor baseline version of the product

    /**
    This variable holds value of the patch baseline version.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_PATCHBASELINEVERSION_MIN (0)</td>
        <td>@ref LVVIDHEADER_PATCHBASELINEVERSION_DEFAULT (1)</td>
        <td>@ref LVVIDHEADER_PATCHBASELINEVERSION_MAX (255)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT8 PatchBaselineVersion;          ///< Patch baseline version of the product

    /**
    First LVVE_Rx algorithm mask which holds information of which LVVE_Rx
    algorithms are present.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_LVVE_Rx_Mask1_bm LVVE_Rx_AlgoMask1;          ///< This variable forms first part of LVVE_Rx algorithm mask

    /**
    Second LVVE_Rx algorithm mask which holds information of which LVVE_Rx
    algorithms are present.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_LVVE_Rx_Mask2_bm LVVE_Rx_AlgoMask2;          ///< This variable forms second part of LVVE_Rx algorithm mask

    /**
    First LVVE_Tx algorithm mask which holds information of which LVVE_Tx
    algorithms are present.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_LVVE_Tx_Mask1_bm LVVE_Tx_AlgoMask1;          ///< This variable forms first part of LVVE_Tx mask

    /**
    Second LVVE_Tx algorithm mask which holds information of which LVVE_Tx
    algorithms are present.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_LVVE_Tx_Mask2_bm LVVE_Tx_AlgoMask2;          ///< This variable forms second part of LVVE_Tx mask

    /**
    First LVVE configuration mask which holds information of which configurations
    of LVVE_Tx and LVVE_Rx algorithm are present.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_Configurations_Mask1_bm LVVE_Config_AlgoMask1;          ///< This variable forms first part of common LVVE_Tx and LVVE_Rx configuration mask

    /**
    Second LVVE configuration mask which holds information of which configurations
    of LVVE_Tx and LVVE_Rx algorithm are present.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_Configurations_Mask2_bm LVVE_Config_AlgoMask2;          ///< This variable forms second part of common LVVE_Tx and LVVE_Rx configuration mask

    /**
    Messge ID is used to identify stream<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVIDHeader_MessageID_en MessageID;          ///< This param holds message ID of the buffer

    /**
    This parameter specifies the sample rate information of the buffer.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Fs_en SampleRate;          ///< Sample rate information of the buffer.

    /**
    This parameter specifies the volume index corresponding to the subsequent data.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_VOLUMEINDEX_MIN (0)</td>
        <td>@ref LVVIDHEADER_VOLUMEINDEX_DEFAULT (0)</td>
        <td>@ref LVVIDHEADER_VOLUMEINDEX_MAX (255)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT8 VolumeIndex;          ///< Volume index corresponding to the subsequent data.

    /**
    This parameter specifies how many volumes are present in the binary file.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>8.0</td>
        <td>@ref LVVIDHEADER_NUMVOLUMES_MIN (0)</td>
        <td>@ref LVVIDHEADER_NUMVOLUMES_DEFAULT (1)</td>
        <td>@ref LVVIDHEADER_NUMVOLUMES_MAX (255)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT8 NumVolumes;          ///< Number of volumes present in the binary file.

} LVVIDHeader_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVINPUTMIXER_SetControlParameters function but they
* will not take effect until the next call to the LVINPUTMIXER_Process function.
*
* @see LVINPUTMIXER_SetControlParameters
* @see LVINPUTMIXER_Process
*/
typedef struct
{
    /**
    Gain (dB) applied at the Rx input of LVVE.<br>
    Can be used to scale the input signal (optimize headroom) for better operation
    of the rest of the Rx modules.<br>
    Note that the gain has no soft limiter, so it should be chosen well to avoid
    clipping.<br>
    There is also no pops & clicks suppression, so value should not be changed
    during a stream.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVINPUTMIXER_INPUTGAIN_MIN (-96)</td>
        <td>@ref LVINPUTMIXER_INPUTGAIN_DEFAULT (0)</td>
        <td>@ref LVINPUTMIXER_INPUTGAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 InputGain;          ///< Apply gain (dB) to input signal

} LVINPUTMIXER_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVAVC_SetControlParameters function but they
* will not take effect until the next call to the LVAVC_Process function.
*
* @see LVAVC_SetControlParameters
* @see LVAVC_Process
*/
typedef struct
{
    /**
    OperatingMode enables/disables Active Voice Contrast.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    The mode word allows enabling or disabling certain functional blocks in Active
    Voice Contrast.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVAVC_ModeWord_bm Mode;          ///< The mode word to control the internal operation of LVAVC.

    /**
    RMS Gain applied by AVC - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_GAIN_MIN (0)</td>
        <td>@ref LVAVC_GAIN_DEFAULT (10)</td>
        <td>@ref LVAVC_GAIN_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 Gain;          ///< RMS Gain applied by AVC - dB parameter

    /**
    Estimated ambient noise energy below which AVC effect is smoothed.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NOISEADAPTTH0_MIN (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH0_DEFAULT (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH0_MAX (90)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NoiseAdaptTh0;          ///< Noise enabling Threshold

    /**
    Noise Adaptive Threshold low dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NOISEADAPTTH1_MIN (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH1_DEFAULT (40)</td>
        <td>@ref LVAVC_NOISEADAPTTH1_MAX (90)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NoiseAdaptTh1;          ///< Noise Adaptive Threshold low dB parameter

    /**
    Noise Adaptive Threshold high dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NOISEADAPTTH2_MIN (0)</td>
        <td>@ref LVAVC_NOISEADAPTTH2_DEFAULT (60)</td>
        <td>@ref LVAVC_NOISEADAPTTH2_MAX (90)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NoiseAdaptTh2;          ///< Noise Adaptive Threshold high dB parameter

    /**
    Noise Adaptive Gain Min for Band1 - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN1_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN1_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN1_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 NASEL_NoiseAdaptGmin1;          ///< Noise Adaptive Gain Min for Band1 - dB parameter

    /**
    Noise Adaptive Gain Min for Band2 - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN2_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN2_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN2_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 NASEL_NoiseAdaptGmin2;          ///< Noise Adaptive Gain Min for Band2 - dB parameter

    /**
    Noise Adaptive Gain Min for Band3 - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN3_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN3_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMIN3_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 NASEL_NoiseAdaptGmin3;          ///< Noise Adaptive Gain Min for Band3 - dB parameter

    /**
    Noise Adaptive Gain Max for Band1 - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX1_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX1_DEFAULT (6)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX1_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 NASEL_NoiseAdaptGmax1;          ///< Noise Adaptive Gain Max for Band1 - dB parameter

    /**
    Noise Adaptive Gain Max for Band2 - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX2_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX2_DEFAULT (6)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX2_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 NASEL_NoiseAdaptGmax2;          ///< Noise Adaptive Gain Max for Band2 - dB parameter

    /**
    Noise Adaptive Gain Max for Band3 - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX3_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX3_DEFAULT (6)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTGMAX3_MAX (30)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 NASEL_NoiseAdaptGmax3;          ///< Noise Adaptive Gain Max for Band3 - dB parameter

    /**
    Attack parameter for Noise Adapt module of NASEL.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTATTACK_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTATTACK_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTATTACK_MAX (100)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_NoiseAdaptAttack;          ///< Noise Adaptive Attack parameter in dB/s

    /**
    Reelase parameter for Noise Adapt module of NASEL.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTRELEASE_MIN (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTRELEASE_DEFAULT (0)</td>
        <td>@ref LVAVC_NASEL_NOISEADAPTRELEASE_MAX (100)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_NoiseAdaptRelease;          ///< Noise Adaptive Release parameter in dB/s

    /**
    Compression knee for low band of NASEL in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPKNEE1_MIN (0)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE1_DEFAULT (60)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE1_MAX (90)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_CompKnee1;          ///< Compression knee for low band of NASEL in dB

    /**
    Compression knee for mid band of NASEL in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPKNEE2_MIN (0)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE2_DEFAULT (55)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE2_MAX (90)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_CompKnee2;          ///< Compression knee for mid band of NASEL in dB

    /**
    Compression knee for high band of NASEL in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPKNEE3_MIN (0)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE3_DEFAULT (50)</td>
        <td>@ref LVAVC_NASEL_COMPKNEE3_MAX (90)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_CompKnee3;          ///< Compression knee for high band of NASEL in dB

    /**
    Compression ratio for low band of NASEL.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPRATIO1_MIN (2)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO1_DEFAULT (32767)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO1_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_CompRatio1;          ///< Compression ratio for low band of NASEL

    /**
    Compression ratio for mid band of NASEL.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPRATIO2_MIN (2)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO2_DEFAULT (32767)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO2_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_CompRatio2;          ///< Compression ratio for mid band of NASEL

    /**
    Compression ratio for high band of NASEL.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_NASEL_COMPRATIO3_MIN (2)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO3_DEFAULT (32767)</td>
        <td>@ref LVAVC_NASEL_COMPRATIO3_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 NASEL_CompRatio3;          ///< Compression ratio for high band of NASEL

    /**
    FU Minimum spectral Gain - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_FU_MINGAIN_MIN (0)</td>
        <td>@ref LVAVC_FU_MINGAIN_DEFAULT (0)</td>
        <td>@ref LVAVC_FU_MINGAIN_MAX (54)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 FU_MinGain;          ///< FU Min Gain

    /**
    FU Maximum spectral Gain - dB parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_FU_MAXGAIN_MIN (0)</td>
        <td>@ref LVAVC_FU_MAXGAIN_DEFAULT (10)</td>
        <td>@ref LVAVC_FU_MAXGAIN_MAX (54)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 FU_MaxGain;          ///< FU Max Gain

    /**
    Limiter threshold for AVC.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_LIMITER_THR_MIN (0)</td>
        <td>@ref LVAVC_LIMITER_THR_DEFAULT (32767)</td>
        <td>@ref LVAVC_LIMITER_THR_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 Limiter_Thr;          ///< Limiter threshold for AVC

    /**
    Advanced configuration for AVC.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVAVC_ADVANCED_CONFIG_MIN (0)</td>
        <td>@ref LVAVC_ADVANCED_CONFIG_DEFAULT (0)</td>
        <td>@ref LVAVC_ADVANCED_CONFIG_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 Advanced_Config;          ///< Advanced configuration for AVC

} LVAVC_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVFENS_SetControlParameters function but they
* will not take effect until the next call to the LVFENS_Process function.
*
* @see LVFENS_SetControlParameters
* @see LVFENS_Process
*/
typedef struct
{
    /**
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Operating Mode

    /**
    The mode word to enable/disable internal algorithm blocks.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVFENS_ModeWord_bm Mode;          ///< Mode word

    /**
    The maximum amount of background noise suppression. FENS_limit_ns =
    \f$32767*10^{-max noise reduction[dB]/20}\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_FENS_LIMIT_NS_MIN (0)</td>
        <td>@ref LVFENS_FENS_LIMIT_NS_DEFAULT (10976)</td>
        <td>@ref LVFENS_FENS_LIMIT_NS_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 FENS_limit_NS;          ///< FENS Noise Suppression Limit

    /**
    Additional noise suppression will be applied when noise is below the
    FENS_NoiseThreshold.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_FENS_NOISETHRESHOLD_MIN (0)</td>
        <td>@ref LVFENS_FENS_NOISETHRESHOLD_DEFAULT (96)</td>
        <td>@ref LVFENS_FENS_NOISETHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 FENS_NoiseThreshold;          ///< FENS Noise Threshold

    /**
    Factor to control the amount of wind noise suppression. A higher value means
    more wind noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref LVFENS_WNS_AGGRESSIVENESS_MIN (0)</td>
        <td>@ref LVFENS_WNS_AGGRESSIVENESS_DEFAULT (256)</td>
        <td>@ref LVFENS_WNS_AGGRESSIVENESS_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_Aggressiveness;          ///< Aggressiveness of wind noise suppression

    /**
    Scale factor of powers used for wind noise suppression. <br>
    Unit: dB<br>
    <br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVFENS_WNS_POWERSCALEFACTOR_MIN (-96)</td>
        <td>@ref LVFENS_WNS_POWERSCALEFACTOR_DEFAULT (0)</td>
        <td>@ref LVFENS_WNS_POWERSCALEFACTOR_MAX (32)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_PowerScaleFactor;          ///< Scale factor of powers used for wind noise suppression

    /**
    The maximum amount of wind noise suppression. WNS_MaxAttenuation =
    \f$32767*10^{-max noise reduction[dB]/20}\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_WNS_MAXATTENUATION_MIN (0)</td>
        <td>@ref LVFENS_WNS_MAXATTENUATION_DEFAULT (8231)</td>
        <td>@ref LVFENS_WNS_MAXATTENUATION_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_MaxAttenuation;          ///< Maximum wind noise attenuation

    /**
    High frequency slope used for wind noise suppression. <br>
    Unit: dB<br>
    A higher value allows more wind reduction. A smaller value can be used to
    protect high frequency speech components.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVFENS_WNS_HFSLOPE_MIN (-20)</td>
        <td>@ref LVFENS_WNS_HFSLOPE_DEFAULT (-6)</td>
        <td>@ref LVFENS_WNS_HFSLOPE_MAX (0)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_HFSlope;          ///< High frequency slope used for wind noise suppression

    /**
    Threshold for wind gush suppression. A higher threshold means less suppression
    of wind gushes.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVFENS_WNS_GUSHTHRESHOLD_MIN (0)</td>
        <td>@ref LVFENS_WNS_GUSHTHRESHOLD_DEFAULT (9830)</td>
        <td>@ref LVFENS_WNS_GUSHTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_GushThreshold;          ///< Threshold for wind gush suppression

    /**
    The 3dB corner frequency of the high pass filter that was applied before LVFENS
    (e.g. by the codex). This is needed by our wind noise suppression algorithm in
    order to do a correct detection. A value of 0 means no high pass filtering was
    done on the signal beforehand. <br>
    Unit: Hz<br>
    <br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVFENS_WNS_HPFCORNERFREQ_MIN (0)</td>
        <td>@ref LVFENS_WNS_HPFCORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVFENS_WNS_HPFCORNERFREQ_MAX (1500)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_HPFCornerFreq;          ///< High Pass Filter Corner Frequency in Hz

} LVFENS_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVWM_SetControlParameters function but they
* will not take effect until the next call to the LVWM_Process function.
*
* @see LVWM_SetControlParameters
* @see LVWM_Process
*/
typedef struct
{
    /**
    Control the operating mode (ON/OFF) of the algorithm.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    The mode word to enable/disable internal algorithm blocks of WhisperMode.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVWM_ModeWord_bm mode;          ///< Mode word

    /**
    The desired output level of the AGC. <br>
    AVL_Target_level_lin = \f$32767*10^{TargetLeveldB/20}\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_TARGET_LEVEL_LIN_MIN (0)</td>
        <td>@ref LVWM_AVL_TARGET_LEVEL_LIN_DEFAULT (16384)</td>
        <td>@ref LVWM_AVL_TARGET_LEVEL_LIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AVL_Target_level_lin;          ///< The desired output level of the AGC.

    /**
    The maximal attenuation of the AGC. <br>
    AVL_MinGainLin = \f$512*10^{MinGaindB/20}\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_AVL_MINGAINLIN_MIN (0)</td>
        <td>@ref LVWM_AVL_MINGAINLIN_DEFAULT (128)</td>
        <td>@ref LVWM_AVL_MINGAINLIN_MAX (512)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AVL_MinGainLin;          ///< The maximal attenuation of the AGC.

    /**
    The maximal gain of the AGC. <br>
    AVL_MaxGainLin = \f$512*10^{MaxGaindB/20}\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_AVL_MAXGAINLIN_MIN (512)</td>
        <td>@ref LVWM_AVL_MAXGAINLIN_DEFAULT (8189)</td>
        <td>@ref LVWM_AVL_MAXGAINLIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AVL_MaxGainLin;          ///< The maximal gain of the AGC.

    /**
    The gain of the AGC is smoothed across time. The time constant of the smoothing
    depends on whether the gain is increasing or decreasing. When a sudden loud
    signal is encountered (e.g. attack of speech), the attack time is used. When
    the gain is increasing (e.g. long period of whispered speech), the release time
    is used. <br>

    AVL_Attack = \f$32767*\exp(-1/(100*AttackTimeSec))\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_ATTACK_MIN (0)</td>
        <td>@ref LVWM_AVL_ATTACK_DEFAULT (25520)</td>
        <td>@ref LVWM_AVL_ATTACK_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AVL_Attack;          ///< The attack time of the AGC.

    /**
    The gain of the AGC is smoothed across time. The time constant of the smoothing
    depends on whether the gain is increasing or decreasing. When a sudden loud
    signal is encountered (e.g. attack of speech), the attack time is used. When
    the gain is increasing (e.g. long period of whispered speech), the release time
    is used. <br>

    AVL_Release = \f$32767*\exp(-1/(100*ReleasTimeSec))\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_RELEASE_MIN (0)</td>
        <td>@ref LVWM_AVL_RELEASE_DEFAULT (32685)</td>
        <td>@ref LVWM_AVL_RELEASE_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AVL_Release;          ///< The release time of the AGC.

    /**
    AVL_Limit_MaxOutputLin = \f$32767*10^{TargetLimiterLeveldB/20}\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVWM_AVL_LIMIT_MAXOUTPUTLIN_MIN (0)</td>
        <td>@ref LVWM_AVL_LIMIT_MAXOUTPUTLIN_DEFAULT (23197)</td>
        <td>@ref LVWM_AVL_LIMIT_MAXOUTPUTLIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AVL_Limit_MaxOutputLin;          ///< The level to which the signal will be limited.

    /**
    The higher the value of the threshold, the less speech detections will occur.
    <br>
    SpDetect_Threshold = \f$512 * [0,64]\f$<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref LVWM_SPDETECT_THRESHOLD_MIN (0)</td>
        <td>@ref LVWM_SPDETECT_THRESHOLD_DEFAULT (9216)</td>
        <td>@ref LVWM_SPDETECT_THRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 SpDetect_Threshold;          ///< The threshold used in the speech detector of WhisperMode.

} LVWM_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVDRC_SetControlParameters function but they
* will not take effect until the next call to the LVDRC_Process function.
*
* @see LVDRC_SetControlParameters
* @see LVDRC_Process
*/
typedef struct
{
    /**
    Operating mode for DRC<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    Sets the number of sections (knee points) in the compressor curve<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVDRC_NUMKNEES_MIN (1)</td>
        <td>@ref LVDRC_NUMKNEES_DEFAULT (5)</td>
        <td>@ref LVDRC_NUMKNEES_MAX (5)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 NumKnees;          ///< Number of Knee points in compressor curve

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. Th input level for the first knee should be equal to or
    larger than -96dB, and the input level for the last knee should be 0dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVDRC_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-96,-70,-50,-24,0}</td>
        <td>@ref LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 CompressorCurveInputLevels[LVDRC_COMPRESSORCURVEINPUTLEVELS_LENGTH];          ///< Compressor Curve Input Levels

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. The output level for the first knee should be equal to or
    larger than -96dB, and the output level for the last knee should be equal to or
    smaller than 0dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVDRC_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-70,-38,-14,0}</td>
        <td>@ref LVDRC_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 CompressorCurveOutputLevels[LVDRC_COMPRESSORCURVEOUTPUTLEVELS_LENGTH];          ///< Compressor Curve Output Levels

    /**
    The AttackTime parameter is the time constant controlling the rate of reaction
    to increase in signal level. The AttackTime is specified in increments of 100�s<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_ATTACKTIME_MIN (0)</td>
        <td>@ref LVDRC_ATTACKTIME_DEFAULT (10)</td>
        <td>@ref LVDRC_ATTACKTIME_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 AttackTime;          ///< Attack Time

    /**
    The ReleaseTime parameter is the time constant controlling the rate of reaction
    to decrease in signal level. The ReleaseTime is specified in increments of
    100�s.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_RELEASETIME_MIN (0)</td>
        <td>@ref LVDRC_RELEASETIME_DEFAULT (2500)</td>
        <td>@ref LVDRC_RELEASETIME_MAX (32767)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 ReleaseTime;          ///< Release Time

    /**
    Enable or Disable limiter with soft clipping.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en LimiterOperatingMode;          ///< Enable or Disable limiter with soft clipping

    /**
    Sets the limit level of the compressor in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVDRC_LIMITLEVEL_MIN (-96)</td>
        <td>@ref LVDRC_LIMITLEVEL_DEFAULT (0)</td>
        <td>@ref LVDRC_LIMITLEVEL_MAX (0)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 LimitLevel;          ///< Sets the limit level of the compressor in dB

} LVDRC_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVCNG_SetControlParameters function but they
* will not take effect until the next call to the LVCNG_Process function.
*
* @see LVCNG_SetControlParameters
* @see LVCNG_Process
*/
typedef struct
{
    /**
    Sets the level of the noise generated by the Comfort Noise Generator. The level
    is expressed in dBFS RMS.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVCNG_CNG_VOLUME_MIN (-96)</td>
        <td>@ref LVCNG_CNG_VOLUME_DEFAULT (-90)</td>
        <td>@ref LVCNG_CNG_VOLUME_MAX (-60)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 CNG_Volume;          ///< CNG volume level in dBFS RMS

} LVCNG_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNG_SetControlParameters function but they
* will not take effect until the next call to the LVNG_Process function.
*
* @see LVNG_SetControlParameters
* @see LVNG_Process
*/
typedef struct
{
    /**
    Operating mode for Noise Gate<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Operating mode

    /**
    Sets the number of sections (knee points) in the compressor curve<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNG_NUMKNEES_MIN (1)</td>
        <td>@ref LVNG_NUMKNEES_DEFAULT (5)</td>
        <td>@ref LVNG_NUMKNEES_MAX (5)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 NumKnees;          ///< Number of Knee points in compressor curve

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. The input level for the first knee should be equal to or
    larger than -96dB, and the input level for the last knee should be 0dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVNG_COMPRESSORCURVEINPUTLEVELS_DEFAULT {-80,-70,-50,-24,0}</td>
        <td>@ref LVNG_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 CompressorCurveInputLevels[LVNG_COMPRESSORCURVEINPUTLEVELS_LENGTH];          ///< Compressor Curve Input Levels

    /**
    An array of size 5(maximum) containing the knee points specified by the
    compression curve in dBFs. The number of knee points in use is limited by the
    NumKnees parameter. The output level for the first knee should be equal to or
    larger than -96dB, and the output level for the last knee should be equal to or
    smaller than 0dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMIN {-96,-96,-96,-96,-96}</td>
        <td>@ref LVNG_COMPRESSORCURVEOUTPUTLEVELS_DEFAULT {-96,-80,-50,-24,0}</td>
        <td>@ref LVNG_COMPRESSORCURVEOUTPUTLEVELS_ARRAYMAX {0,0,0,0,0}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 CompressorCurveOutputLevels[LVNG_COMPRESSORCURVEOUTPUTLEVELS_LENGTH];          ///< Compressor Curve Output Levels

    /**
    The AttackTime parameter is the time constant controlling the rate of reaction
    to increase in signal level. The AttackTime is specified in increments of 100�s<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_ATTACKTIME_MIN (0)</td>
        <td>@ref LVNG_ATTACKTIME_DEFAULT (50)</td>
        <td>@ref LVNG_ATTACKTIME_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 AttackTime;          ///< Attack Time

    /**
    The ReleaseTime parameter is the time constant controlling the rate of reaction
    to decrease in signal level. The ReleaseTime is specified in increments of
    100�s.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNG_RELEASETIME_MIN (0)</td>
        <td>@ref LVNG_RELEASETIME_DEFAULT (50)</td>
        <td>@ref LVNG_RELEASETIME_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 ReleaseTime;          ///< Release Time

} LVNG_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNF_SetControlParameters function but they
* will not take effect until the next call to the LVNF_Process function.
*
* @see LVNF_SetControlParameters
* @see LVNF_Process
*/
typedef struct
{
    /**
    Sets the Notch Filter attenuation in dB.(i.e. negative gain). This attenuation
    is reached at NF_Frequency.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNF_NF_ATTENUATION_MIN (0)</td>
        <td>@ref LVNF_NF_ATTENUATION_DEFAULT (10)</td>
        <td>@ref LVNF_NF_ATTENUATION_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 NF_Attenuation;          ///< Notch Filter Attenuation

    /**
    Sets the filter center frequency in Hz .<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNF_NF_FREQUENCY_MIN (400)</td>
        <td>@ref LVNF_NF_FREQUENCY_DEFAULT (800)</td>
        <td>@ref LVNF_NF_FREQUENCY_MAX (1500)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 NF_Frequency;          ///< Notch Filter Frequency

    /**
    Sets the width of the filter. <br>
    \f$NF_QFactor= 100*Q\f$ <br>
    Where Q is the usual float range of a Q factor in float. <br>
    Note: The QFactor is defined at half the attenuation in dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNF_NF_QFACTOR_MIN (50)</td>
        <td>@ref LVNF_NF_QFACTOR_DEFAULT (120)</td>
        <td>@ref LVNF_NF_QFACTOR_MAX (500)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 NF_QFactor;          ///< Notch Filter Qfactor

} LVNF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVEQ_SetControlParameters function but they
* will not take effect until the next call to the LVEQ_Process function.
*
* @see LVEQ_SetControlParameters
* @see LVEQ_Process
*/
typedef struct
{
    /**
    Sets the length of the Equalizer impulse response. This must never be more than
    the value of EQ_MaxLength set at initialization time. The EQ_Length must be a
    multiple of 8.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVEQ_EQ_LENGTH_MIN (8)</td>
        <td>@ref LVEQ_EQ_LENGTH_DEFAULT (192)</td>
        <td>@ref LVEQ_EQ_LENGTH_MAX (192)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 EQ_Length;          ///< EQ Tap Length

    /**
    Pointer to an array containing the samples of the Equalizer impulse response.
    The format of the array content is specified below. The samples of the
    Equalizer impulse response must be in Q3.12 format.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>3.12</td>
        <td>@ref LVEQ_EQ_COEFS_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref LVEQ_EQ_COEFS_DEFAULT {4096,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}</td>
        <td>@ref LVEQ_EQ_COEFS_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 *pEQ_Coefs;          ///< Equalizer impulse response

} LVEQ_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVOL_SetControlParameters function but they
* will not take effect until the next call to the LVVOL_Process function.
*
* @see LVVOL_SetControlParameters
* @see LVVOL_Process
*/
typedef struct
{
    /**
    Turns on/off VOL_Gain.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en VOL_OperatingMode;          ///< Turns on/off VOL_Gain

    /**
    The volume control gain can be used to set the overall volume level of the
    signal. The gain is set in dB with steps of 1dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVOL_VOL_GAIN_MIN (-96)</td>
        <td>@ref LVVOL_VOL_GAIN_DEFAULT (0)</td>
        <td>@ref LVVOL_VOL_GAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 VOL_Gain;          ///< Apply Gain to Input signal

} LVVOL_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVHPF_SetControlParameters function but they
* will not take effect until the next call to the LVHPF_Process function.
*
* @see LVHPF_SetControlParameters
* @see LVHPF_Process
*/
typedef struct
{
    /**
    Turns on/off High Pass filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en HPF_OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVHPF_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVHPF_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVHPF_HPF_CORNERFREQ_MAX (1500)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 HPF_CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

} LVHPF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVMUTE_SetControlParameters function but they
* will not take effect until the next call to the LVMUTE_Process function.
*
* @see LVMUTE_SetControlParameters
* @see LVMUTE_Process
*/
typedef struct
{
    /**
    This param can mute unmute the Rx/Tx engine output.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en Mute;          ///< This param can mute unmute the Rx/Tx engine output

} LVMUTE_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVE_Rx_SetControlParameters function but they
* will not take effect until the next call to the LVVE_Rx_Process function.
*
* @see LVVE_Rx_SetControlParameters
* @see LVVE_Rx_Process
*/
typedef struct
{
    /**
    This enumerated type is used to set the operating mode of the Rx path. The
    processing can be separately set to enable all processing modules (i.e., ON),
    or to disable all processing modules (i.e., OFF). When OFF, the input is copied
    to the output with the applied channel routing and the LVVE sub modules can be
    provided with invalid parameters for modules. The sub module functions are not
    executed in this scenario.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Turns on/off all VoiceExperience processing modules on Rx

    /**
    Input Mixer Control Parameter Structure.<br>
    */
    LVINPUTMIXER_ControlParams_st InputMixer;          ///< Input Mixer Control Parameter Structure

    /**
    This param can mute unmute the Rx/Tx engine output.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en Mute;          ///< This param can mute unmute the Rx/Tx engine output

    /**
    Turns on/off VOL_Gain.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en VOL_OperatingMode;          ///< Turns on/off VOL_Gain

    /**
    The volume control gain can be used to set the overall volume level of the
    signal. The gain is set in dB with steps of 1dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVE_RX_VOL_GAIN_MIN (-96)</td>
        <td>@ref LVVE_RX_VOL_GAIN_DEFAULT (0)</td>
        <td>@ref LVVE_RX_VOL_GAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 VOL_Gain;          ///< Apply Gain to Input signal

    /**
    Far End Noise Suppression Control Parameter Structure.<br>
    */
    LVFENS_ControlParams_st FENS_ControlParams;          ///< Far End Noise Suppression Control Parameter Structure

    /**
    Active Voice Constrat Parameter Structure.<br>
    */
    LVAVC_ControlParams_st AVC_ControlParams;          ///< Active Voice Contrast Parameter Structure

    /**
    Equalizer Operating mode.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en EQ_OperatingMode;          ///< Equalizer Operating mode

    /**
    Equalizer Control Parameters Structure.<br>
    */
    LVEQ_ControlParams_st EQ_ControlParams;          ///< Equalizer Control Parameters Structure

    /**
    DRC Control Parameter Strcuture.<br>
    */
    LVDRC_ControlParams_st DRC_ControlParams;          ///< DRC Control Parameter Strcuture

    /**
    Turns on/off High Pass filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en HPF_OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_RX_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVVE_RX_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVVE_RX_HPF_CORNERFREQ_MAX (1500)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 HPF_CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    Turns on/off Conformt Noise Generator Module.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en CNG_OperatingMode;          ///< Turns on/off Comfort Noise Generator Module

    /**
    Comfort Noise Generator Control Parameter Structure.<br>
    */
    LVCNG_ControlParams_st CNG_ControlParams;          ///< Comfort Noise Generator Control Parameter Structure

    /**
    Whisper Mode Control Parameter Strcuture.<br>
    */
    LVWM_ControlParams_st WM_ControlParams;          ///< Whisper Mode Control Parameter Strcuture

    /**
    Noise Gate Control Parameter Structure.<br>
    */
    LVNG_ControlParams_st NG_ControlParams;          ///< Noise Gate Control Parameter Structure

    /**
    Turns on/off Notch Filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en NF_OperatingMode;          ///< Turns on/off Notch Filter

    /**
    Notch Filter Control Parameter Structure.<br>
    */
    LVNF_ControlParams_st NF_ControlParams;          ///< Notch Filter Control Parameter Structure

} LVVE_Rx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the AcousticEchoCanceller_SetControlParameters function but they
* will not take effect until the next call to the AcousticEchoCanceller_Process function.
*
* @see AcousticEchoCanceller_SetControlParameters
* @see AcousticEchoCanceller_Process
*/
typedef struct
{
    /**
    Gain (in dB) applied to the output of the AEC residual signal.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MIN (-48)</td>
        <td>@ref ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_DEFAULT (0)</td>
        <td>@ref ACOUSTICECHOCANCELLER_MICROPHONEGAINCOMP_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 MicrophoneGainComp;          ///< Microphone Gain Compensation (in dB)

} AcousticEchoCanceller_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNV_FsbChannel_SetControlParameters function but they
* will not take effect until the next call to the LVNV_FsbChannel_Process function.
*
* @see LVNV_FsbChannel_SetControlParameters
* @see LVNV_FsbChannel_Process
*/
typedef struct
{
    /**
    Set of coefficients for the adaptive filter FSB inside the Beamformer. These
    values should be determined for the given microphone configuration<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_FSBCHANNEL_FSB_INITTABLE_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref LVNV_FSBCHANNEL_FSB_INITTABLE_DEFAULT {0,32767,0,0,0,0,0,0}</td>
        <td>@ref LVNV_FSBCHANNEL_FSB_INITTABLE_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 FSB_InitTable[LVNV_FSBCHANNEL_FSB_INITTABLE_LENGTH];          ///< FSB initial coefficients.

    /**
    Set of coefficients for the highband adaptive filter FSB inside the Beamformer.
    These values should be determined for the given microphone configuration<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_FSBCHANNEL_FSB_INITTABLEHB_ARRAYMIN {-32768,-32768,-32768,-32768,-32768,-32768,-32768,-32768}</td>
        <td>@ref LVNV_FSBCHANNEL_FSB_INITTABLEHB_DEFAULT {0,32767,0,0,0,0,0,0}</td>
        <td>@ref LVNV_FSBCHANNEL_FSB_INITTABLEHB_ARRAYMAX {32767,32767,32767,32767,32767,32767,32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 FSB_InitTableHB[LVNV_FSBCHANNEL_FSB_INITTABLEHB_LENGTH];          ///< FSB initial coefficients.

} LVNV_FsbChannel_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the Beamformer_SetControlParameters function but they
* will not take effect until the next call to the Beamformer_Process function.
*
* @see Beamformer_SetControlParameters
* @see Beamformer_Process
*/
typedef struct
{
    /**
    Length of the Beamformer filters to model the acoustical paths between the
    speech source and the microphones. The length must be a multiple of 8. It must
    be equal to 16 for the handset application.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref BEAMFORMER_FSB_TAPS_MIN (8)</td>
        <td>@ref BEAMFORMER_FSB_TAPS_DEFAULT (16)</td>
        <td>@ref BEAMFORMER_FSB_TAPS_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 FSB_taps;          ///< Number of FSB taps

    /**
    Array of microphone dependent Fsb parameters.<br>
    */
    LVNV_FsbChannel_ControlParams_st FsbChannel[BEAMFORMER_FSBCHANNEL_LENGTH];          ///< Fsb channel Params.

    /**
    Gain in dB to compensate low-frequency component loss in noise reference, only
    when using close microphones.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref BEAMFORMER_NOISEREFERENCESGAIN_MIN (-24)</td>
        <td>@ref BEAMFORMER_NOISEREFERENCESGAIN_DEFAULT (6)</td>
        <td>@ref BEAMFORMER_NOISEREFERENCESGAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 NoiseReferencesGain;          ///< Gain applied to noise references in dB

} Beamformer_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the EchoSuppression_SetControlParameters function but they
* will not take effect until the next call to the EchoSuppression_Process function.
*
* @see EchoSuppression_SetControlParameters
* @see EchoSuppression_Process
*/
typedef struct
{
    /**
    Echo overestimation factor for low frequencies that is applied for near-end
    detection. Used only for SPKPH. For other configurations, it should be set to
    0.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_DEFAULT (281)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LF_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaDetect_LF;          ///< Echo overestimation for NE detection LF

    /**
    Echo oversubtraction factor for low frequencies (below EchoCutoff_LF) that is
    applied during farend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_DEFAULT (450)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LF_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaHi_LF;          ///< High echo oversubtraction factor LF

    /**
    Echo oversubtraction factor for low frequencies (below EchoCutoff_LF) that is
    applied during nearend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LF_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaLo_LF;          ///< Low echo oversubtraction factor LF

    /**
    Echo oversubtraction factor for low frequencies (below EchoCutoff_LF) that is
    applied during double talk only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LF_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaDt_LF;          ///< Mid echo oversubtraction factor LF

    /**
    Cutoff frequency in Hz for low frequency band in low-band<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_DEFAULT (600)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCUTOFF_LF_MAX (4000)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoCutoff_LF;          ///< Cut off frequency of LF in Hz

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    nearend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENLOW_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENLOW_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENLOW_MAX (2048)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NlAttenLow;          ///< Non-linear echo subtraction factor for lowest frequencies

    /**
    Echo overestimation factor for lowband that is applied for near-end detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_DEFAULT (281)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaDetect_LB;          ///< Echo overestimation for NE detection in lowband

    /**
    Linear Echo factor factor for lowband that is applied for near-end detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_DEFAULT (6554)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_LinEchoFractionNeDetect_LB;          ///< Linear Echo factor for NE detection in lowband

    /**
    Echo oversubtraction factor for low-band that is applied during farend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_DEFAULT (450)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaHi_LB;          ///< High echo oversubtraction factor LB

    /**
    Echo oversubtraction factor for low-band that is applied during nearend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaLo_LB;          ///< Low echo oversubtraction factor LB

    /**
    Echo oversubtraction factor for low-band that is applied during double talk
    only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaDt_LB;          ///< Mid echo oversubtraction factor LB

    /**
    Parameter related to the reverberation time of the acoustical environment,
    which represents the decay in low-band energy over time of the echo tail of the
    impulse response.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_DEFAULT (10000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoAlphaRev_LB;          ///< Echo reverberation factor LB

    /**
    Parameter representing the portion of the echo tail (estimated by the low-band
    NLMS filter) that has to be extrapolated in time.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_DEFAULT (8000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoTailPortion_LB;          ///< Echo tail portion LB

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    farend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_LB_DEFAULT (128)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_LB_MAX (2048)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NlAtten_LB;          ///< Non-linear echo subtraction factor LB

    /**
    Echo overestimation factor for highband that is applied for near-end detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADETECT_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaDetect_HB;          ///< Echo overestimation for NE detection in highband

    /**
    Linear Echo factor for highband that is applied for near-end detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_DEFAULT (6554)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_LINECHOFRACTIONNEDETECT_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_LinEchoFractionNeDetect_HB;          ///< Linear Echo factor for NE detection in highband

    /**
    Echo oversubtraction factor for high-band that is applied during farend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_DEFAULT (300)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAHI_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaHi_HB;          ///< High echo oversubtraction factor HB

    /**
    Echo oversubtraction factor for high-band that is applied during nearend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMALO_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaLo_HB;          ///< Low echo oversubtraction factor HB

    /**
    Echo oversubtraction factor for high-band that is applied during double talk
    only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMADT_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaDt_HB;          ///< Mid echo oversubtraction factor HB

    /**
    Parameter related to the reverberation time of the acoustical environment,
    which represents the decay in high-band energy over time of the echo tail of
    the impulse response.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_DEFAULT (12000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOALPHAREV_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoAlphaRev_HB;          ///< Echo reverberation factor HB

    /**
    Parameter representing the portion of the echo tail (estimated by the high-band
    NLMS filter) that has to be extrapolated in time.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_DEFAULT (12000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOTAILPORTION_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoTailPortion_HB;          ///< Echo tail portion HB

    /**
    Non-linear echo subtraction factor that is applied during double talk and
    farend-only.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_HB_DEFAULT (64)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTEN_HB_MAX (2048)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NlAtten_HB;          ///< Non-linear echo subtraction factor HB

    /**
    Maximal amount of echo suppression additionally applied to the top band (above
    7kHz). The true amount applied depends on how much the echo estimate is above
    the floor. A value of 0dB means no additional attenuation is applied. A value
    of 12 would allow up to 12dB of additional echo suppression in the top band.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MAXSUPPRESSION_ECHOTOPBAND_MAX (48)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppression_EchoTopband;          ///< Echo suppression additionally applied to the top band

    /**
    Threshold to relax the echo attenuation during NearEnd dominance for
    frequenices below EchoCutoff_LF<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_LF_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoRelax_NEThreshold_LF;          ///< Echo relaxation threshold for low frequencies

    /**
    Threshold to relax the echo attenuation during NearEnd dominance for
    frequenices above EchoCutoff_LF<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_DEFAULT (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHORELAX_NETHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoRelax_NEThreshold;          ///< Echo relaxation threshold

    /**
    Level to which the echo estimate is clipped inside the post-processor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_DEFAULT (32767)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOCLIPPINGLEVEL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoClippingLevel;          ///< Clipping level for echo inside DNNS

    /**
    Echo oversubtraction factor for low frequencies applied to the estimated
    non-stationary noise floor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_LF_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_LF_DEFAULT (16384)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_LF_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_GainEta_LF;          ///< Echo oversubtraction factor for low freq.

    /**
    Echo oversubtraction factor for lowband applied to the estimated non-stationary
    noise floor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_LB_DEFAULT (16384)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_GainEta_LB;          ///< Echo oversubtraction factor for lowband.

    /**
    Echo oversubtraction factor for highband applied to the estimated
    non-stationary noise floor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_HB_DEFAULT (16384)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_GainEta_HB;          ///< Echo oversubtraction factor for highband.

    /**
    Scaling factor for Gaineta which will be applied to the estimated
    non-stationary noise floor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_DEFAULT (32767)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_GAINETA_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_GainEta_Sensitivity;          ///< Control GainEta sensitivity.

    /**
    Echo oversubtraction factor for voiced frames. The higher the value, the lower
    will be the voiced preservation during DT.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAVOICED_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_EchoGammaVoiced;          ///< Echo oversubtraction factor for voiced.

    /**
    Echo oversubtraction factor for unvoiced frames. The higher the value, the
    lower will be the unvoiced preservation during DT.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_DEFAULT (256)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_ECHOGAMMAUNVOICED_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_EchoGammaUnvoiced;          ///< Echo oversubtraction factor for unvoiced.

    /**
    Non-linear echo fraction in lowband for preservation during DT.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_DEFAULT (8192)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_NonLinEchoFraction_LB;          ///< Non-linear echo fraction in lowband.

    /**
    Non-linear echo fraction in highband for preservation during DT.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_DEFAULT (8192)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_NONLINECHOFRACTION_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_NonLinEchoFraction_HB;          ///< Non-linear echo fraction in highband.

    /**
    Linear echo fraction in lowband for preservation during DT.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_DEFAULT (1638)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_LinEchoFraction_LB;          ///< Linear echo fraction in lowband.

    /**
    Linear echo fraction in highband for preservation during DT.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_DEFAULT (1638)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_LINECHOFRACTION_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_LinEchoFraction_HB;          ///< Linear echo fraction in highband.

    /**
    Threshold for voiced preservation during noise. Default is 263, which means
    disabled.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>3.12</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_DEFAULT (263)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_MSR_VOICEDPRESERVATIONTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_VoicedPreservationThreshold;          ///< Threshold for voiced preservation during noise.

    /**
    Near-end speech detector threshold used in Dynamic Echo Suppression (DES). The
    lower the threshold, the more detections.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_DEFAULT (512)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_SPDET_NEARTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_SPDET_NearThreshold;          ///< Near-end speech detector threshold.

    /**
    The amount of comfort noise insertion.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref ECHOSUPPRESSION_DNNS_CNILEVEL_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_CNILEVEL_DEFAULT (12000)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_CNILEVEL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_CNILevel;          ///< Comfort noise insertion level.

    /**
    Oversubtraction factor for echo at initialization. Default is 1024 for Handset,
    and 5120 for Speakerphone.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_DEFAULT (1024)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMAINIT_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaInit;          ///< Oversubtraction factor for echo at initialization.

    /**
    Scaling factor for EchoGammaHi and EchoGammaDt when primary microphone is
    covered in handset mode.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_DEFAULT (512)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_ECHOGAMMASCALING_MICCOVERAGE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_EchoGammaScaling_MicCoverage;          ///< Scaling factor for EchoGammaHi and EchoGammaDt when primary microphone is covered.

    /**
    Scaling factor for NlAtten when primary microphone is covered in handset mode.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>6.9</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_MIN (0)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_DEFAULT (512)</td>
        <td>@ref ECHOSUPPRESSION_DNNS_NLATTENSCALING_MICCOVERAGE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NlAttenScaling_MicCoverage;          ///< Scaling factor for NlAtten when primary microphone is covered.

} EchoSuppression_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the GeneralSidelobeCanceller_SetControlParameters function but they
* will not take effect until the next call to the GeneralSidelobeCanceller_Process function.
*
* @see GeneralSidelobeCanceller_SetControlParameters
* @see GeneralSidelobeCanceller_Process
*/
typedef struct
{
    /**
    Length of the GSC to model the transfer function between the noise reference
    and the residual signal, in order to reduce the noise and interfering sounds
    from the desired signal. This number should be a multiple of 8.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_TAPS_MIN (8)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_TAPS_DEFAULT (16)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_TAPS_MAX (16)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 GSC_taps;          ///< Number of GSC taps for noise cancellation

    /**
    Adaptive step-size control for GSC to avoid divergence of the adaptive filter
    during desired speech. In general, GSC_erl is lower than NLMS_erl and ranges
    between 0 and 30 dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_ERL_MIN (64)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_ERL_DEFAULT (256)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_ERL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 GSC_erl;          ///< GSC erl

    /**
    This parameter controls the trade-off between the amount of speech recovery
    that is applied and the amount of residual noise during speech (especially in
    car noise environments). By tuning the parameter down, the speech can be
    slightly more attenuated, but some residual noises can be removed. This
    parameter is only active in handset configurations.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_MIN (0)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_DEFAULT (32767)</td>
        <td>@ref GENERALSIDELOBECANCELLER_GSC_SPEECHRECOVERY_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 GSC_SpeechRecovery_Sensitivity;          ///< Sensitivity of the speech recovery mechanism.

} GeneralSidelobeCanceller_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the NoiseSuppression_SetControlParameters function but they
* will not take effect until the next call to the NoiseSuppression_Process function.
*
* @see NoiseSuppression_SetControlParameters
* @see NoiseSuppression_Process
*/
typedef struct
{
    /**
    Maximum oversubtraction factor for non-stationary noise.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_DEFAULT (8192)</td>
        <td>@ref NOISESUPPRESSION_DNNS_GAMMA_NN_MAX_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_Gamma_nn_max;          ///< Maximum oversubtraction factor for non-stationary noise.

    /**
    Relates to the amount of non-stationary noise suppression for low frequencies.
    The higher the value, the more non-stationary noise is suppressed. A value of
    256 would only keep stationary NS. A value > 256 would make a trade-off between
    stationary and non-stationary NS.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_LO_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_Constrained_nn_lo;          ///< Trade-off between stationary and non-stationary NS for low-band.

    /**
    Relates to the amount of non-stationary noise suppression for mid frequencies.
    The higher the value, the more non-stationary noise is suppressed. A value of
    256 would only keep stationary NS. A value > 256 would make a trade-off between
    stationary and non-stationary NS.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HI_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_Constrained_nn_hi;          ///< Trade-off between stationary and non-stationary NS for mid-band.

    /**
    Controls the amount of non-stationary noise suppression in the high-band
    (between 4kHz and 8kHz). The higher the value, the more non-stationary noise is
    suppressed. Value of 256 means there is no non-stationary noise suppression in
    high band.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_CONSTRAINED_NN_HIGHBAND_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_Constrained_nn_highband;          ///< Trade-off between stationary and non-stationary NS for high-band.

    /**
    Upper frequency (in Hz) of region for mingain computation.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_ARRAYMIN {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_DEFAULT {200,4000,8000}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_ARRAYMAX {8000,8000,8000}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_SNR_freq[NOISESUPPRESSION_DNNS_MINGAIN_SNR_FREQ_LENGTH];          ///< Upper frequency of SNR region.

    /**
    Low SNR-level for mingain computation in [dB]. This parameter belongs to a set
    of 4 to determine a piecewise linear curve to tune the amount of noise
    suppression. Between low SNR-level and high SNR-level in [dB], the NS is
    linear. Below low SNR-level and above high SNR-level, NS is constant.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_ARRAYMIN {-5,-5,-5}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_DEFAULT {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_ARRAYMAX {63,63,63}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_SNR_min[NOISESUPPRESSION_DNNS_MINGAIN_SNR_MIN_LENGTH];          ///< Low SNR-level for mingain.

    /**
    High SNR-level for mingain computation in [dB]. Note that the value of this
    parameter must be larger than or equal to the value of DNNS_MinGain_SNR_min.
    This parameter belongs to a set of 4 to determine a piecewise linear curve to
    tune the amount of noise suppression. Between low SNR-level and high SNR-level
    in [dB], the NS is linear. Below low SNR-level and above high SNR-level, NS is
    constant.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_ARRAYMIN {-5,-5,-5}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_DEFAULT {20,20,20}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_ARRAYMAX {63,63,63}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_SNR_max[NOISESUPPRESSION_DNNS_MINGAIN_SNR_MAX_LENGTH];          ///< High SNR-level for mingain.

    /**
    Maximal amount of noise suppression at high SNR-level in [dB]. This parameter
    belongs to a set of 4 to determine a piecewise linear curve to tune the amount
    of noise suppression. Between low SNR-level and high SNR-level in [dB], the NS
    is linear. Below low SNR-level and above high SNR-level, NS is constant.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_ARRAYMIN {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_DEFAULT {15,15,15}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_ARRAYMAX {48,48,48}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppression_highSNR[NOISESUPPRESSION_DNNS_MAXSUPPRESSION_HIGHSNR_LENGTH];          ///< Maximal noise suppression at high SNR-level.

    /**
    Maximal amount of noise suppression at low SNR-level in [dB]. This parameter
    belongs to a set of 4 to determine a piecewise linear curve to tune the amount
    of noise suppression. Between low SNR-level and high SNR-level in [dB], the NS
    is linear. Below low SNR-level and above high SNR-level, NS is constant.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_ARRAYMIN {0,0,0}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_DEFAULT {15,15,15}</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_ARRAYMAX {48,48,48}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppression_lowSNR[NOISESUPPRESSION_DNNS_MAXSUPPRESSION_LOWSNR_LENGTH];          ///< Maximal noise suppression at low SNR-level.

    /**
    Relaxation (in dB) of maximum noise suppression during speech. Relaxation is
    applied to the SNR-dependent noise suppression which is controlled by the
    parameters DNNS_MaxSuppression_lowSNR and DNNS_MaxSuppression_highSNR. The
    higher the value, the better the preservation of speech at the expense of
    potentially audible noise pumping.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_DEFAULT (1)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONRELAX_SPEECH_MAX (6)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppressionRelax_Speech;          ///< Relaxation (in dB) of maximum noise suppression during speech

    /**
    Minimum amount of noise suppression during speech (in dB). This parameter
    should be set between
    min(DNNS_MaxSuppression_lowSNR,DNNS_MaxSuppression_highSNR) and 0 dB. The lower
    the parameter, the better the speech is preserved at the expense of potentially
    less noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_DEFAULT (6)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINSUPPRESSION_SPEECH_MAX (48)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinSuppression_Speech;          ///< Minimum noise suppression during speech (in dB).

    /**
    Fine-tuning on the maximum amount of suppression during non-stationary noise,
    relative to the amount during stationary noise. E.g., 12 indicates 12dB less
    non-stationary NS compared to stationary noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_DEFAULT (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATIONARY_MAX (36)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppressionRel_NonStationary;          ///< Non-stationary NS amount compared to stationary NS

    /**
    Fine-tuning on the maximum amount of suppression during non-stationary noise in
    broadside position, relative to the amount during stationary noise. E.g, 12
    indicates 12dB less non-stationary noise suppression in broadside compared to
    stationary noise suppression. NOTE: Only for handset config.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_DEFAULT (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSIONREL_NONSTATBROADSIDE_MAX (36)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppressionRel_NonStatBroadSide;          ///< Non-stationary NS amount compared to stationary NS in Broadside

    /**
    Relaxation factor for the minimum gain during recovery after position change
    (only for handset config)<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_MIN (2048)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_DEFAULT (2048)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXPOSCHANGE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_RelaxPosChange;          ///< Minimum Gain relaxation after position change

    /**
    Maximum of MinGain relaxation (in dB). The higher the value, the more speech
    can be preserved at the cost of lowering the amount of noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_DEFAULT (5)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXMAX_MAX (12)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_RelaxMax;          ///< Maximum of MinGain relaxation

    /**
    Low-band sensitivity of the MinGain relaxation mechanism. The higher the value,
    the more relaxation of MinGain during the speech.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_DEFAULT (16384)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_RelaxSensitivity_LB;          ///< Low-band sensitivity of MinGain relaxation

    /**
    High-band sensitivity of the MinGain relaxation mechanism. The higher the
    value, the more relaxation of MinGain during the speech.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_DEFAULT (16384)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MINGAIN_RELAXSENSITIVITY_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MinGain_RelaxSensitivity_HB;          ///< High-band sensitivity of MinGain relaxation

    /**
    Maximal amount of noise suppression for top band in dB. This value must always
    be set to the highest of DNNS_MaxSuppression_highSNR and
    DNNS_MaxSuppression_lowSNR, or higher. In speech segments the parameter will
    have almost no effect, while in noise or echo segments it becomes active.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_DEFAULT (15)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MAXSUPPRESSION_TOPBAND_MAX (48)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MaxSuppression_Topband;          ///< Maximal noise suppression for top band in dB

    /**
    Factor by which single channel noise estimate has to be boosted for increased
    noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNESUBFACTOR_MIN (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNESUBFACTOR_DEFAULT (1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNESUBFACTOR_MAX (3072)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_ScneSubFactor;          ///< overestimation factor for single-channel noise estimate

    /**
    Minimum threshold for SCNE weighting factor for improved single channel noise
    estimator for handset configuration.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_DEFAULT (4096)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMINTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_ScneMinThreshold;          ///< Minimum threshold for SCNE weighting factor.

    /**
    Maximum threshold for SCNE weighting factor for improved single channel noise
    estimator for handset configuration.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_DEFAULT (12288)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMAXTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_ScneMaxThreshold;          ///< Maximum threshold for SCNE weighting factor

    /**
    Minimum scaling factor for SCNE for handset configuration.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMINSCALING_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMINSCALING_DEFAULT (16384)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SCNEMINSCALING_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_ScneMinScaling;          ///< Minimum scaling factor for SCNE

    /**
    Controls the amount of smoothing. A large value makes the speech sound natural
    and continuous, but since a lot of smoothing is applied it can also introduce
    speech attenuation. A small value implies less smoothing, hence less
    naturalness of the speech, but also less attenuation. It is advised to keep the
    value of this parameter within the range [307; 819] to avoid artifacts.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NGS_SMOOTHING_DEFAULT (819)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NGS_SMOOTHING_MAX (1024)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NGS_Smoothing;          ///< Maximum oversubtraction factor for non-stationary noise.

    /**
    Controls the amount of musical tone suppression. A large value implies that
    less musical tones will be suppressed. A small value implies a more aggressive
    suppression of musical tones but possibly also some speech attenuation. It is
    advised to use a value within the range [-308; 308].<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>5.10</td>
        <td>@ref NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MIN (-1024)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_DEFAULT (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MUSICALTONE_SENSITIVITY_MAX (1024)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MusicalTone_Sensitivity;          ///< Musical tone suppression sensitivity.

    /**
    Sensitivity of the speech recovery mechanism. It controls the trade-off between
    the amount of speech recovery that is applied and the amount of residual noise
    during speech (especially in car noise environments). By tuning the parameter
    down, the speech can be slightly more attenuated, but some residual noises can
    be removed.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_DEFAULT (32767)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_SpeechRecovery_Sensitivity;          ///< Sensitivity of the speech recovery mechanism.

    /**
    Speech overestimation factor when computing speech-to-total power for speech
    recovery. Controls the trade-off between the amount of speech recovery that is
    applied and the amount of residual noise during speech. A lower value means the
    speech can be slightly more attenuated, but some residual noises can be
    removed.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>3.12</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_DEFAULT (8192)</td>
        <td>@ref NOISESUPPRESSION_DNNS_SPEECHRECOVERY_FACTOR_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_SpeechRecovery_Factor;          ///< Speech overestimation factor for speech recovery mechanism.

    /**
    Sensitivity of the noise detector to steer the gain pull-down mechanism in
    noise-only segments.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_DEFAULT (32767)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NOISEDETECT_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NoiseDetect_Sensitivity;          ///< Sensitivity of noise detector

    /**
    Parameter to control further pull-down (in dB) on MinGain during noise-only
    segments. A higher value can bring more noise suppression but it may lead to
    noise pumping.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_NOISEPULLDOWN_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NOISEPULLDOWN_DEFAULT (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_NOISEPULLDOWN_MAX (12)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_NoisePullDown;          ///< Amount of gain pull-down in noise

    /**
    Parameter to control the sensitivity of the model-based VAD for gain pull-down
    during noise.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_DEFAULT (16384)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MVAD_SENSITIVITY_NOISE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MVAD_Sensitivity_Noise;          ///< MVAD sensitivity in noise

    /**
    Threshold to detect voicing of the speech. The higher the threshold, the less
    voicing will be detected. If no voicing is detected, then pitch enhancement
    will not be active.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_DEFAULT (60)</td>
        <td>@ref NOISESUPPRESSION_DNNS_VOICINGTHRESHOLD_MAX (512)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_VoicingThreshold;          ///< Threshold for voicing

    /**
    This parameter controls the amount of pitch enhancement (in dB) during voiced
    speech.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_DEFAULT (10)</td>
        <td>@ref NOISESUPPRESSION_DNNS_PITCHENHANCEMENT_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_PitchEnhancement;          ///< Amount of pitch enhancement

    /**
    Maximum frequency (in Hz) for the pitch estimate. It is recommended to keep the
    default value.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_PITCHMAXFREQ_MIN (100)</td>
        <td>@ref NOISESUPPRESSION_DNNS_PITCHMAXFREQ_DEFAULT (300)</td>
        <td>@ref NOISESUPPRESSION_DNNS_PITCHMAXFREQ_MAX (500)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_PitchMaxFreq;          ///< Maximum pitch frequency

    /**
    Threshold to detect voicing of the speech for speech preservation. With a
    higher threshold, less voicing will be detected.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_DEFAULT (60)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_VOICINGTHRESHOLD_MAX (512)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_VoicingThreshold;          ///< Threshold for voicing

    /**
    Controls the amount of preservation in voiced speech frames.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_DEFAULT (32767)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_VOICEDSENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_VoicedSensitivity;          ///< Sensitivity for voiced speech.

    /**
    Controls the amount of preservation in unvoiced speech frames.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_DEFAULT (32767)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_UNVOICEDSENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_UnvoicedSensitivity;          ///< Sensitivity for unvoiced speech.

    /**
    Noise oversubtraction factor for voiced frames. With a higher value, there will
    be less voiced preservation.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_DEFAULT (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAVOICED_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_NoiseGammaVoiced;          ///< Noise oversubtraction factor for voiced.

    /**
    Noise oversubtraction factor for unvoiced frames. With a higher value, there
    will be less unvoiced preservation.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_MIN (0)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_DEFAULT (128)</td>
        <td>@ref NOISESUPPRESSION_DNNS_MSR_NOISEGAMMAUNVOICED_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_MSR_NoiseGammaUnvoiced;          ///< Noise oversubtraction factor for unvoiced.

    /**
    Speech hysteresis of the VAD. When tuned to minimum, no hysteresis will apply.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_MIN (16384)</td>
        <td>@ref NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_DEFAULT (32113)</td>
        <td>@ref NOISESUPPRESSION_DNNS_VAD_HYSTERESIS_MAX (32440)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_VAD_hysteresis;          ///< Hysteresis of the VAD.

    /**
    Trade-off between stationary and non-stationary NS for low-band during position
    change. The higher the value, the more non-stationary noise is suppressed. Its
    value should not exceed DNNS_Constrained_nn_lo. This parameter only has effect
    when LVNV_MODE_POSITION_CHANGE is enabled. [VE12.5] In speakerphone mode it is
    used for echo tail-alpha in LB when far-end activity stops, for improving DT in
    reverberant rooms<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_DEFAULT (512)</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_LO_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_PosChange_Constrained_nn_lo;          ///< Trade-off between stationary and non-stationary NS for low-band during position change.

    /**
    Trade-off between stationary and non-stationary NS for mid-band during position
    change. The higher the value, the more non-stationary noise is suppressed. Its
    value should not exceed DNNS_Constrained_nn_hi. This parameter only has effect
    when LVNV_MODE_POSITION_CHANGE is enabled.[VE12.5] In speakerphone mode it is
    used for echo tail-portion in LB when far-end activity stops, for improving DT
    in reverberant rooms<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_DEFAULT (768)</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HI_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_PosChange_Constrained_nn_hi;          ///< Trade-off between stationary and non-stationary NS for mid-band during position change.

    /**
    Trade-off between stationary and non-stationary NS for high-band (between 4kHz
    and 8kHz) during position change. The higher the value, the more non-stationary
    noise is suppressed. Its value should not exceed DNNS_Constrained_nn_highband.
    This parameter only has effect when LVNV_MODE_POSITION_CHANGE is
    enabled.[VE12.5] In speakerphone mode it is used for LB cutoff (default 4kHz)
    for Echo suppression<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_MIN (256)</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_DEFAULT (768)</td>
        <td>@ref NOISESUPPRESSION_DNNS_POSCHANGE_CONSTRAINED_NN_HIGHBAND_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 DNNS_PosChange_Constrained_nn_highband;          ///< Trade-off between stationary and non-stationary NS for high-band during position change.

} NoiseSuppression_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the WindNoiseSuppression_SetControlParameters function but they
* will not take effect until the next call to the WindNoiseSuppression_Process function.
*
* @see WindNoiseSuppression_SetControlParameters
* @see WindNoiseSuppression_Process
*/
typedef struct
{
    /**
    Speech level difference in dB for the 2nd microphone taking primary microphone
    as a reference. Only used in handset mode during wind noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_DEFAULT (12)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MICSPEECHLEVELDIFF_MAX (20)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_MicSpeechLevelDiff;          ///< Mic speech level difference for wind noise suppression

    /**
    Threshold on the wind probability below which wind noise suppression is
    disabled. A higher value means less wind noise detections. Not used in 1-mic.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_DEFAULT (6554)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_DETECTORTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_DetectorThreshold;          ///< Threshold for wind noise detection

    /**
    The 3dB corner frequency of the high pass filter that was applied before
    NoiseVoid. This is needed by our wind noise suppression algorithm in order to
    do a correct detection. It is recommended to set this parameter to the same
    value as LVHPF CornerFreq. <br>
    Unit: Hz<br>
    <br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_DEFAULT (50)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HPFCORNERFREQ_MAX (1500)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_HPFCornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    If energy level of all microphones is below this parameter, WNS cut-off
    frequency is 0 and hence, no wind noise suppression will be done. The energy
    level can be read from the status parameters. Unit is in dB. Not used in 1-mic.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_DEFAULT (36)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_CUTOFF_MINENERGYLEVEL_MAX (196)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_CutOff_MinEnergyLevel;          ///< Minimum energy level for WNS cut-off

    /**
    If energy level of at least one microphone exeeds this parameter, wind noise
    suppression will be done up to a cut-off frequency of 8kHz. The energy level
    can be read from the status parameters. For energy levels in between
    WNS_CutOff_MinEnergyLevel and WNS_CutOff_MaxEnergyLevel, the cut-off will be in
    between 0 and 8000Hz. The value of WNS_CutOff_MaxEnergyLevel has to be larger
    than WNS_CutOff_MinEnergyLevel. Unit is in dB. Not used in 1-mic.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_MIN (20)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_DEFAULT (106)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_CUTOFF_MAXENERGYLEVEL_MAX (196)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_CutOff_MaxEnergyLevel;          ///< Maximum energy level for WNS cut-off

    /**
    Sensitivity of wind gust post-processor. With a value of 0 (disabled), speech
    might be better preserved at the cost of less wind suppression. A value of
    32767 means fully enabled, so good wind suppression with the possibility of
    some speech attenuation. Not used in 1-mic.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_DEFAULT (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSTPOSTPROC_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_GustPostProc_Sensitivity;          ///< Sensitivity of wind gust post-processor

    /**
    Amount of multi-channel wind noise suppression applied to each microphone. A
    value of 0 means no multi-channel WNS, a value of 32767 means full
    multi-channel WNS. Not used in 1-mic.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_ARRAYMIN {0,0}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_DEFAULT {32767,32767}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_NMic_Aggressiveness[WINDNOISESUPPRESSION_WNS_NMIC_AGGRESSIVENESS_LENGTH];          ///< Aggressiveness of N-mic wind noise suppression

    /**
    Parameter to control the amount of 1-mic wind noise suppression. A higher value
    means more wind noise suppression. Not used in case of 2 or more microphone
    inputs.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_DEFAULT (256)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_AGGRESSIVENESS_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_Aggressiveness;          ///< Aggressiveness of 1-mic wind noise suppression

    /**
    The maximum amount of quasi-stationary wind noise suppression. Unit in [dB].<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_DEFAULT (12)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXSUPPRESSION_MAX (48)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_MaxSuppression;          ///< Maximum wind noise suppression

    /**
    The maximum amount of wind gust suppression additional to WNS_MaxSuppression.
    Unit in [dB].<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_DEFAULT (48)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_MAXEXTRAGUSTSUPPRESSION_MAX (48)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_MaxExtraGustSuppression;          ///< Maximum extra noise suppression during wind gusts

    /**
    Threshold for wind gust suppression. A higher threshold means less suppression
    of wind gusts. Not used in case of 2 or more microphone inputs.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MIN (0)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_DEFAULT (9830)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_GUSHTHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_GushThreshold;          ///< Threshold for wind gust suppression

    /**
    High frequency slope used for 1-mic wind noise suppression. <br>
    Unit: dB<br>
    A higher value allows more wind reduction. A smaller value (more negative) can
    be used to protect high frequency speech components. Not used in case of 2 or
    more microphone inputs.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HFSLOPE_MIN (-20)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HFSLOPE_DEFAULT (-6)</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_HFSLOPE_MAX (0)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_HFSlope;          ///< High frequency slope used for 1-mic wind noise suppression

    /**
    Scale factor of powers used for 1-mic wind noise suppression. <br>
    Unit: dB<br>
    . This scale factor should be tuned such that the value of the
    LowFrequencyEnergy status parameter during clean speech-only segments is around
    819 (or 0.05 float). Not used in case of 2 or more microphone inputs.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMIN {-96,-96}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_DEFAULT {0,0}</td>
        <td>@ref WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_ARRAYMAX {32,32}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 WNS_EnergyScaleFactor[WINDNOISESUPPRESSION_WNS_ENERGYSCALEFACTOR_LENGTH];          ///< Scale factor of powers used for 1-mic wind noise suppression

} WindNoiseSuppression_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the PathChangeDetector_SetControlParameters function but they
* will not take effect until the next call to the PathChangeDetector_Process function.
*
* @see PathChangeDetector_SetControlParameters
* @see PathChangeDetector_Process
*/
typedef struct
{
    /**
    Length of the background adaptive echo cancellation filter. The length should
    be chosen such that only the main peak of the impulse response is covered by
    the filter. The length should be a multiple of 8.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_TAPS_MIN (16)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_TAPS_DEFAULT (32)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_TAPS_MAX (64)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PCD_Taps;          ///< Number of PCD taps

    /**
    Adaptive step size control for the background adaptive echo cancellation
    filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERL_MIN (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERL_DEFAULT (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PCD_Erl;          ///< Step size control for PCD

    /**
    Sensitivity to detect acoustical path changes. The higher the value the more
    detections.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_THRESHOLD_MIN (0)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_THRESHOLD_DEFAULT (5000)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_THRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PCD_Threshold;          ///< Sensitivity of PCD

    /**
    If a path change is detected, NLMS_erl of the NLMS is set to this value, to
    ensure a fast adapting filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_MINIMUMERL_MIN (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_MINIMUMERL_DEFAULT (64)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_MINIMUMERL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PCD_MinimumErl;          ///< Step size control of NLMS in case of PCD

    /**
    After a path change, the NLMS_erl of the NLMS increases back to the nominal
    value. The speed to increase can be controlled by this parameter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERLSTEP_MIN (16384)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERLSTEP_DEFAULT (16800)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_ERLSTEP_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PCD_ErlStep;          ///< Speed with which nominal erl value is restored in case of PCD

    /**
    Gain factor applied to the echo subtraction during acoustical path change.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MIN (0)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_DEFAULT (2048)</td>
        <td>@ref PATHCHANGEDETECTOR_PCD_GAMMAERESCUE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PCD_GammaERescue;          ///< Echo oversubtraction factor in case of PCD

} PathChangeDetector_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the SpeechDetector_SetControlParameters function but they
* will not take effect until the next call to the SpeechDetector_Process function.
*
* @see SpeechDetector_SetControlParameters
* @see SpeechDetector_Process
*/
typedef struct
{
    /**
    Threshold for far-end activity detection in low band.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_DEFAULT (16384)</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_LB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_FarThreshold_LB;          ///< Far-end activity detection threshold in low band

    /**
    Threshold for far-end activity detection in high band.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_DEFAULT (24576)</td>
        <td>@ref SPEECHDETECTOR_SDET_FARTHRESHOLD_HB_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_FarThreshold_HB;          ///< Far-end activity detection threshold in high band

    /**
    A parameter representing a threshold for the detection of instrumental noise.
    The higher the value for this parameter, the lower the noise suppression
    performance.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_DEFAULT (29)</td>
        <td>@ref SPEECHDETECTOR_SDET_CAL_MICPOWFLOORMIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_CAL_MicPowFloorMin;          ///< Instrumental noise threshold

    /**
    Sensitivity parameter for broadside detection. The higher the value for this
    parameter, the more sensitive the detector.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>1.14</td>
        <td>@ref SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MIN (6554)</td>
        <td>@ref SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_DEFAULT (11470)</td>
        <td>@ref SPEECHDETECTOR_SDET_IMCOH_BROADSIDELEVELSCALE_MAX (19661)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_IMCOH_BroadsideLevelScale;          ///< Broadside sensitivity

    /**
    Sensitivity parameter for position change. The higher the value for this
    parameter, the less sensitive the detector.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_DEFAULT (32767)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_PosChange_Sensitivity;          ///< Position change sensitivity

    /**
    This parameter is used to determine the period during which speech is protected
    after handset position change. It defines the number of beamformer updates
    which must occur before the system is defined to be well adjusted to the new
    position.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_DEFAULT (60)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_PROTECTPERIOD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_PosChange_ProtectPeriod;          ///< Speech protection period after handset position change

    /**
    Position change detection threshold for handset<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_DEFAULT (16384)</td>
        <td>@ref SPEECHDETECTOR_SDET_POSCHANGE_UPDATETHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_PosChange_UpdateThreshold;          ///< Position change detection threshold

    /**
    This parameter is used to determine the period during which speech is protected
    after a change in noise characteristics. It defines the number of frames which
    must occur before noise suppression goes back after a period of silence is
    detected.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_DEFAULT (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_MAXCONVERGENCEPERIOD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_MaxConvergencePeriod;          ///< Speech protection period after silence period is detected

    /**
    This parameter is used to determine the maximum amplitude used in low-level
    noise detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_DEFAULT (41)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGEFLOOR_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_NoiseChangeFloor;          ///< Floor for idle noise detection in noise change detection

    /**
    This parameter is the threshold to determine if a noise characteristics change
    has occured.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_DEFAULT (3840)</td>
        <td>@ref SPEECHDETECTOR_SDET_NOISECHANGETHRESHOLD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_NoiseChangeThreshold;          ///< Threshold for noise change detection

    /**
    Level difference in dB for each microphone, as compared to some reference
    microphone(s), which suggests that microphone is covered and triggers special
    processing.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMIN {0,0}</td>
        <td>@ref SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_DEFAULT {96,96}</td>
        <td>@ref SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_ARRAYMAX {96,96}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_MicCovThreshold[SPEECHDETECTOR_SDET_MICCOVTHRESHOLD_LENGTH];          ///< SDET microphone coverage threshold for each microphone.

    /**
    Sensitivity of the detector for stationary coherent noise fields. The higher
    the value, the more coherent noise will be detected. This parameter is only
    active when LVNV_MODE_COHERENT_NOISE is enabled.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_DEFAULT (26213)</td>
        <td>@ref SPEECHDETECTOR_SDET_COHERENTNOISEDETECT_SENSITIVITY_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_CoherentNoiseDetect_Sensitivity;          ///< Sensitivity of the detector for stationary coherent noise fields

    /**
    Aggressiveness during stationary coherent noise fields. The higher the value,
    the more coherent noise can be suppressed at the risk of some speech
    attenuation during low SNR-levels.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_MIN (2048)</td>
        <td>@ref SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_DEFAULT (2048)</td>
        <td>@ref SPEECHDETECTOR_SDET_COHERENTNOISE_AGGRESSIVENESS_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_CoherentNoise_Aggressiveness;          ///< Aggressiveness during stationary coherent noise fields

    /**
    A smaller value can avoid low level (soft) speech attenuation, at the risk of
    instability of the filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref SPEECHDETECTOR_SDET_SOFTSPEECH_MIN (3)</td>
        <td>@ref SPEECHDETECTOR_SDET_SOFTSPEECH_DEFAULT (10)</td>
        <td>@ref SPEECHDETECTOR_SDET_SOFTSPEECH_MAX (100)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_SoftSpeech;          ///< Threshold for soft speech

    /**
    Set higher value for less false speech detection during noise when using
    cardioid detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_MIN (0)</td>
        <td>@ref SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_DEFAULT (19661)</td>
        <td>@ref SPEECHDETECTOR_SDET_CARDIOIDLEAKAGE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SDET_CardioidLeakage;          ///< Leakage tuning for Cardioid robustness

} SpeechDetector_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNV_RsvParameters_SetControlParameters function but they
* will not take effect until the next call to the LVNV_RsvParameters_Process function.
*
* @see LVNV_RsvParameters_SetControlParameters
* @see LVNV_RsvParameters_Process
*/
typedef struct
{
    /**
    Sensitivity factor to control speech quality in WNS. When enabled it could
    reduce roboticness in specific use cases. 0 means disabled. MinValue:0
    MaxValue:32767 DefaultValue:16384<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_0_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_0_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_0_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_0;          ///< Sensitivity factor to control speech quality in WNS

    /**
    Sensitivity factor to control agressiveness of transient noise suppression. 0
    Means no extra transient noise suppression. MinValue:0 MaxValue:32767
    DefaultValue:0<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_1_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_1_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_1_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_1;          ///< Sensitivity of Transient Noise Suppression

    /**
    The higher, the more speech will be protected at expense of less transient
    noise suppression / less NG. MinValue:-32768 MaxValue:32767 DefaultValue:0<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_2_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_2_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_2_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_2;          ///< Speech protection sensitivity for Transient Noise Suppression

    /**
    DOA filter convergence threshold. MinValue:0 MaxValue:32767 DefaultValue:4915<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_3_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_3_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_3_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_3;          ///< DOA filter convergence threshold

    /**
    A higher value means higher level speech will be considered Far/Soft speech in
    speakerphone mode. MinValue:0 MaxValue:32767 DefaultValue:10000 for WB or 5000
    for NB<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_4_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_4_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_4_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_4;          ///< Speech level threshold for Far/Soft Talk detection

    /**
    A higher value means position change detection is less sensitive to voicing
    detection. MinValue:0 MaxValue:32767 DefaultValue:0<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_5_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_5_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_5_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_5;          ///< Voicing offset for position change detection

    /**
    Higher the value more is hysteresis on far end detection. MinValue:0
    MaxValue:32767 DefaultValue:16384<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_6_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_6_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_6_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_6;          ///< Hysteresis on far end detection

    /**
    Scaling factor for LF Gain. Default is 8192, which means disabled.
    MinValue:4096 MaxValue:16384 DefaultValue:8192 QFormat:2.13<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_7_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_7_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_7_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_7;          ///< Gain Scaling for Car LF Enhancement

    /**
    Scale factor for fartalk use case in Q1.14; MaxValue:16384 means no effect.
    MinValue:2458. DefaultValue:8192 gives improved speech preservation. A too low
    value will degrade non-stationary noise suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_8_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_8_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_8_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_8;          ///< FarTalk scaling

    /**
    A higher value means more scratch will be detected based on Energy level
    difference. MinValue: -32768 means disabled. DefaultValue:0 QFormat:5.10<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_9_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_9_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_9_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_9;          ///< Energy level difference offset for SDET scratch detection

    /**
    Sensitivity for Low-SNR speech preservation in Q0.15; MaxValue:32767.
    MinValue:0 means disabled (no extra preservation, DefaultValue:32767.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_10_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_10_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_10_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_10;          ///< Low-SNR Speech preservation sensitivity

    /**
    NoiseGate mingain offset in dB, Q15.0 format; MaxValue:40. MinValue:0,
    DefaultValue:0. A too high value could give noise pumping.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_11_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_11_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_11_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_11;          ///< NG mingain offset

    /**
    NoiseGate threshold in dB, Q15.0 format; MaxValue:96. MinValue:3,
    DefaultValue:15. A higher threshold means less effect.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_12_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_12_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_12_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_12;          ///< NG threshold

    /**
    Controls the level of speech preservation during double-talk. Higher value
    results in better speech preservation. 0 means DT speech preservation is
    disabled. MinValue:0 DefaultValue:32767<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_13_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_13_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_13_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_13;          ///< DT speech preservation level

    /**
    Controls the sensitivity of speech preservation during double-talk. The higher
    the value, the higher number of speech segments will be preserved during
    double-talk. MinValue:0 DefaultValue:16384<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_14_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_14_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_14_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_14;          ///< DT speech preservation sensitivity factor

    /**
    Number of frames that will be muted when parameter update happens.
    MaxValue:100. MinValue:0, DefaultValue:0.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_15_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_15_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_15_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_15;          ///< Mute at param update

    /**
    Difference between ExpectedDelayMax and ExpectedDelayMin in samples. Q15.0
    format. MaxValue:8160. MinValue:0, DefaultValue:0.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_16_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_16_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_16_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_16;          ///< ExpectedDelayDelta

    /**
    Scaling factor for Howling estimate. Q7.8 format. MaxValue:32767. MinValue:0,
    DefaultValue:0.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_17_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_17_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_17_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_17;          ///< DHS_Gamma

    /**
    Sensitivity parameter for detection of howling repetition. Q0.15 format
    MaxValue:32767. MinValue:0(Off), DefaultValue:0(Off).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_18_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_18_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_18_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_18;          ///< RepetitionDetect_Sensitivity

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_19_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_19_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_19_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_19;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_20_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_20_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_20_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_20;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_21_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_21_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_21_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_21;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_22_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_22_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_22_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_22;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_23_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_23_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_23_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_23;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_24_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_24_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_24_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_24;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_25_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_25_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_25_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_25;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_26_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_26_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_26_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_26;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_27_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_27_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_27_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_27;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_28_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_28_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_28_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_28;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_29_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_29_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_29_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_29;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_30_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_30_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_30_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_30;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_31_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_31_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_31_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_31;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_32_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_32_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_32_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_32;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_33_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_33_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_33_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_33;          ///< Reserved Parameter

    /**
    Reserved Parameter in case we need it :).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_34_MIN (-32768)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_34_DEFAULT (6826)</td>
        <td>@ref LVNV_RSVPARAMETERS_RSVPARAM_34_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 RsvParam_34;          ///< Reserved Parameter

} LVNV_RsvParameters_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the Pbfdaf_SetControlParameters function but they
* will not take effect until the next call to the Pbfdaf_Process function.
*
* @see Pbfdaf_SetControlParameters
* @see Pbfdaf_Process
*/
typedef struct
{
    /**
    Power update factor for increasing power for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PBFDAF_PBFDAFGAMMAUP_MIN (0)</td>
        <td>@ref PBFDAF_PBFDAFGAMMAUP_DEFAULT (31129)</td>
        <td>@ref PBFDAF_PBFDAFGAMMAUP_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafGammaUp;          ///< Power update factor for increasing power

    /**
    Power update factor for decreasing power for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PBFDAF_PBFDAFGAMMADOWN_MIN (0)</td>
        <td>@ref PBFDAF_PBFDAFGAMMADOWN_DEFAULT (31129)</td>
        <td>@ref PBFDAF_PBFDAFGAMMADOWN_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafGammaDown;          ///< Power update factor for decreasing power

    /**
    Filter length for each adaptive filter should be multiple of 320 (160) taps at
    16kHz (8kHz).<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref PBFDAF_PBFDAFFILTERLENGTHREF0_ARRAYMIN {160,160}</td>
        <td>@ref PBFDAF_PBFDAFFILTERLENGTHREF0_DEFAULT {320,320}</td>
        <td>@ref PBFDAF_PBFDAFFILTERLENGTHREF0_ARRAYMAX {640,640}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafFilterLengthRef0[PBFDAF_PBFDAFFILTERLENGTHREF0_LENGTH];          ///< Filter length in taps for each adaptive filter using echo reference 0

    /**
    Absolute power minimum used in power update for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref PBFDAF_PBFDAFABSPOWMIN_MIN (0)</td>
        <td>@ref PBFDAF_PBFDAFABSPOWMIN_DEFAULT (2000)</td>
        <td>@ref PBFDAF_PBFDAFABSPOWMIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafAbsPowMin;          ///< Absolute power minimum

    /**
    Low frequency Erl value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PBFDAF_PBFDAFERL_LF_ARRAYMIN {32,32}</td>
        <td>@ref PBFDAF_PBFDAFERL_LF_DEFAULT {3200,3200}</td>
        <td>@ref PBFDAF_PBFDAFERL_LF_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafErl_LF[PBFDAF_PBFDAFERL_LF_LENGTH];          ///< Low frequency Erl value.

    /**
    Low band Erl value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PBFDAF_PBFDAFERL_LB_ARRAYMIN {32,32}</td>
        <td>@ref PBFDAF_PBFDAFERL_LB_DEFAULT {3200,3200}</td>
        <td>@ref PBFDAF_PBFDAFERL_LB_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafErl_LB[PBFDAF_PBFDAFERL_LB_LENGTH];          ///< Low Band Erl value.

    /**
    High band Erl value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PBFDAF_PBFDAFERL_HB_ARRAYMIN {32,32}</td>
        <td>@ref PBFDAF_PBFDAFERL_HB_DEFAULT {3200,3200}</td>
        <td>@ref PBFDAF_PBFDAFERL_HB_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafErl_HB[PBFDAF_PBFDAFERL_HB_LENGTH];          ///< High Band Erl value

    /**
    Erl initial value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref PBFDAF_PBFDAFINITIALERL_MIN (32)</td>
        <td>@ref PBFDAF_PBFDAFINITIALERL_DEFAULT (32)</td>
        <td>@ref PBFDAF_PBFDAFINITIALERL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafInitialErl;          ///< Erl initial value

    /**
    Stepsize used in low frequency value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PBFDAF_PBFDAFALPHA_LF_ARRAYMIN {0,0}</td>
        <td>@ref PBFDAF_PBFDAFALPHA_LF_DEFAULT {1638,1638}</td>
        <td>@ref PBFDAF_PBFDAFALPHA_LF_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafAlpha_LF[PBFDAF_PBFDAFALPHA_LF_LENGTH];          ///< Alpha Low frequency value

    /**
    Stepsize used in low band value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PBFDAF_PBFDAFALPHA_LB_ARRAYMIN {0,0}</td>
        <td>@ref PBFDAF_PBFDAFALPHA_LB_DEFAULT {1638,1638}</td>
        <td>@ref PBFDAF_PBFDAFALPHA_LB_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafAlpha_LB[PBFDAF_PBFDAFALPHA_LB_LENGTH];          ///< Alpha Low band value

    /**
    Stepsize used in high band value for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref PBFDAF_PBFDAFALPHA_HB_ARRAYMIN {0,0}</td>
        <td>@ref PBFDAF_PBFDAFALPHA_HB_DEFAULT {1638,1638}</td>
        <td>@ref PBFDAF_PBFDAFALPHA_HB_ARRAYMAX {32767,32767}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PbfdafAlpha_HB[PBFDAF_PBFDAFALPHA_HB_LENGTH];          ///< Alpha high band value

    /**
    Low frequency cutoff frequency (in Hz) for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref PBFDAF_PBFDAF_LFCUTOFF_MIN (0)</td>
        <td>@ref PBFDAF_PBFDAF_LFCUTOFF_DEFAULT (1000)</td>
        <td>@ref PBFDAF_PBFDAF_LFCUTOFF_MAX (8000)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 Pbfdaf_LfCutoff;          ///< Low Frequency PBFDAF cutoff frequency (in Hz)

    /**
    Low band cutoff frequency (in Hz) for PBFDAF.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref PBFDAF_PBFDAF_LBCUTOFF_MIN (0)</td>
        <td>@ref PBFDAF_PBFDAF_LBCUTOFF_DEFAULT (4000)</td>
        <td>@ref PBFDAF_PBFDAF_LBCUTOFF_MAX (8000)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 Pbfdaf_LbCutoff;          ///< Low Band PBFDAF cutoff frequency (in Hz)

} Pbfdaf_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the AntiHowling_SetControlParameters function but they
* will not take effect until the next call to the AntiHowling_Process function.
*
* @see AntiHowling_SetControlParameters
* @see AntiHowling_Process
*/
typedef struct
{
    /**
    Number of samples the TxOut has to be delayed before being fed back to NV.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref ANTIHOWLING_FEEDBACKDELAY_MIN (0)</td>
        <td>@ref ANTIHOWLING_FEEDBACKDELAY_DEFAULT (0)</td>
        <td>@ref ANTIHOWLING_FEEDBACKDELAY_MAX (19200)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 FeedbackDelay;          ///< Maximum expected delay in samples for TxOut feedback.

    /**
    Cutoff for high pass filter on Howling reference.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ANTIHOWLING_AH_REF_HPF_MIN (50)</td>
        <td>@ref ANTIHOWLING_AH_REF_HPF_DEFAULT (800)</td>
        <td>@ref ANTIHOWLING_AH_REF_HPF_MAX (1500)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 AH_REF_Hpf;          ///< High pass filter on howling reference, always on

    /**
    Minimum gain applied for howling suppression.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ANTIHOWLING_SHR_MIN_GAIN_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_MIN_GAIN_DEFAULT (33)</td>
        <td>@ref ANTIHOWLING_SHR_MIN_GAIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Min_Gain;          ///< Minimum gain threshold of SHR gain

    /**
    Oversubtraction factor for howling candidates that are detected by NLMS as
    feedback.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_MASK_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_MASK_DEFAULT (768)</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_MASK_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Gamma_Mask;          ///< Oversubtraction factor for howling mask

    /**
    Oversubtraction factor for howling candidate bins that vary little in
    amplitude.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_STD_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_STD_DEFAULT (256)</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_STD_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Gamma_Std;          ///< Oversubtraction for the std mask. Used when NLMS is not working

    /**
    Percentile of bins that determines threshold for howling candidates detection.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ANTIHOWLING_SHR_ENERGY_PERCENTILE_MIN (5)</td>
        <td>@ref ANTIHOWLING_SHR_ENERGY_PERCENTILE_DEFAULT (50)</td>
        <td>@ref ANTIHOWLING_SHR_ENERGY_PERCENTILE_MAX (95)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Energy_Percentile;          ///< SHR_Energy_Percentile

    /**
    The lower, the more lower energies are detected as howling..<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ANTIHOWLING_SHR_LOWER_LIMIT_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_LOWER_LIMIT_DEFAULT (1024)</td>
        <td>@ref ANTIHOWLING_SHR_LOWER_LIMIT_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Lower_Limit;          ///< Lower energy limit for howling reduction

    /**
    The lower, the more howling is attenuated..<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>4.11</td>
        <td>@ref ANTIHOWLING_SHR_UPPER_LIMIT_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_UPPER_LIMIT_DEFAULT (2048)</td>
        <td>@ref ANTIHOWLING_SHR_UPPER_LIMIT_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Upper_Limit;          ///< Upper energy limit for howling reduction

    /**
    Oversubtraction factor for howling candidate bins that are persistent.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>7.8</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_PERSISTENCE_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_PERSISTENCE_DEFAULT (384)</td>
        <td>@ref ANTIHOWLING_SHR_GAMMA_PERSISTENCE_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Gamma_Persistence;          ///< Oversubtraction factor for persistence estimate

    /**
    Protects speech below the specified frequency.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_DEFAULT (2000)</td>
        <td>@ref ANTIHOWLING_SHR_SPEECHPROTECT_FREQ_LIMIT_MAX (4000)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_SpeechProtect_Freq_Limit;          ///< SpeechProtection frequency upperLimit

    /**
    Step-size control for AH FBE filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>9.6</td>
        <td>@ref ANTIHOWLING_AH_FBE_ERL_MIN (64)</td>
        <td>@ref ANTIHOWLING_AH_FBE_ERL_DEFAULT (64)</td>
        <td>@ref ANTIHOWLING_AH_FBE_ERL_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 AH_FBE_Erl;          ///< AH_FBE_Erl

    /**
    Smoothing Factor for gain estimate.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_DEFAULT (16384)</td>
        <td>@ref ANTIHOWLING_SHR_GAIN_SMOOTHING_FACTOR_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Gain_Smoothing_Factor;          ///< SHR_Gain_Smoothing_Factor

    /**
    Energy smoothing factor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_DEFAULT (33)</td>
        <td>@ref ANTIHOWLING_SHR_ENERGY_SMOOTHING_FACTOR_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Energy_Smoothing_Factor;          ///< SHR_Energy_Smoothing_Factor

    /**
    Candidates smoothing factor.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>0.15</td>
        <td>@ref ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_MIN (0)</td>
        <td>@ref ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_DEFAULT (164)</td>
        <td>@ref ANTIHOWLING_SHR_CANDIDATES_SMOOTHING_FACTOR_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 SHR_Candidates_Smoothing_Factor;          ///< SHR_Candidates_Smoothing_Factor

} AntiHowling_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVNV_SetControlParameters function but they
* will not take effect until the next call to the LVNV_Process function.
*
* @see LVNV_SetControlParameters
* @see LVNV_Process
*/
typedef struct
{
    /**
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Operating Mode

    /**
    Control word to select between handset, speakerphone, etc.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Config_en Config;          ///< Configuration

    /**
    The mode word to enable/disable internal algorithm blocks of LVNV.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVNV_ModeWord_bm Mode;          ///< Mode word

    /**
    Extension of the mode word to enable/disable internal algorithm blocks of LVNV.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVNV_ModeWord2_bm Mode2;          ///< Extended Mode word

    /**
    Extension of the mode word to enable/disable internal algorithm blocks of LVNV
    related to AntiHowling.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVNV_ModeWord3_bm Mode3;          ///< Extended Mode word for AntiHowling

    /**
    When the tuning mode word is enabled, the output of the module is replaced by a
    special signal, which provides knowledge on the internal state of the
    algorithm. Only one bit is allowed to be enabled at a time.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVNV_TuningModeWord_bm TuningMode;          ///< Tuning mode word

    /**
    Sets the control for the microphone.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVNV_MicrophoneControlWord_bm MicrophoneMode[LVNV_MICROPHONEMODE_LENGTH];          ///< Microphone mode word

    /**
    The microphone position of the device in millimeters, given as X.Y.Z where X is
    the horizontal position, Y the vertical position, and Z the depth relative to
    the bottom left corner of the device.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNV_MICROPHONEPOSITIONX_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONX_DEFAULT {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONX_ARRAYMAX {400,400}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 MicrophonePositionX[LVNV_MICROPHONEPOSITIONX_LENGTH];          ///< Microphone X Position in millimeters.

    /**
    The microphone position of the device in millimeters, given as X.Y.Z where X is
    the horizontal position, Y the vertical position, and Z the depth relative to
    the bottom left corner of the device.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNV_MICROPHONEPOSITIONY_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONY_DEFAULT {0,90}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONY_ARRAYMAX {400,400}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 MicrophonePositionY[LVNV_MICROPHONEPOSITIONY_LENGTH];          ///< Microphone Y Position in millimeters.

    /**
    The microphone position of the device in millimeters, given as X.Y.Z where X is
    the horizontal position, Y the vertical position, and Z the depth relative to
    the bottom left corner of the device.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVNV_MICROPHONEPOSITIONZ_ARRAYMIN {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONZ_DEFAULT {0,0}</td>
        <td>@ref LVNV_MICROPHONEPOSITIONZ_ARRAYMAX {400,400}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 MicrophonePositionZ[LVNV_MICROPHONEPOSITIONZ_LENGTH];          ///< Microphone Z Position in millimeters.

    /**
    Type of input signal for each channel.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_ChannelType_en ChannelType[LVNV_CHANNELTYPE_LENGTH];          ///< Type of input signal for each channel.

    /**
    Gain applied (in dB) to the Microphone channel in the Tx input. Used to scale
    the microphone signal in order to give the NLMS_LB and NLMS_HB filters headroom
    for correct operation.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_MICROPHONEINPUTGAIN_ARRAYMIN {-48,-48}</td>
        <td>@ref LVNV_MICROPHONEINPUTGAIN_DEFAULT {0,0}</td>
        <td>@ref LVNV_MICROPHONEINPUTGAIN_ARRAYMAX {12,12}</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 MicrophoneInputGain[LVNV_MICROPHONEINPUTGAIN_LENGTH];          ///< The microphone input gain (in dB).

    /**
    Gain (in dB) applied to the Tx output of LVNV. Compensates for the applied
    MicrophoneInputGain in order to preserve the overall Tx gain.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_OUTPUTGAIN_MIN (-48)</td>
        <td>@ref LVNV_OUTPUTGAIN_DEFAULT (0)</td>
        <td>@ref LVNV_OUTPUTGAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 OutputGain;          ///< The output gain (in dB).

    /**
    Extra gain (in dB) applied when coverage of the primary microphone is detected.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVNV_PRIMMICCOVGAIN_MIN (-24)</td>
        <td>@ref LVNV_PRIMMICCOVGAIN_DEFAULT (10)</td>
        <td>@ref LVNV_PRIMMICCOVGAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 PrimMicCovGain;          ///< Primary Mic coverage Gain.

    /**
    Array of Microphone dependent parameters.<br>
    */
    AcousticEchoCanceller_ControlParams_st AECParams[LVNV_AECPARAMS_LENGTH];          ///< AEC Params.

    /**
    */
    SpeechDetector_ControlParams_st SDETParams;          ///< Speech Detector Params

    /**
    */
    Beamformer_ControlParams_st BMFParams;          ///< Beamformer Params

    /**
    */
    GeneralSidelobeCanceller_ControlParams_st GSCParams;          ///< General Sidelobe Canceller Params

    /**
    */
    EchoSuppression_ControlParams_st EchoSuppressionParams;          ///< Echo Suppression Params

    /**
    */
    NoiseSuppression_ControlParams_st NoiseSuppressionParams;          ///< Noise Suppression Params

    /**
    */
    WindNoiseSuppression_ControlParams_st WindNoiseSuppressionParams;          ///< WindNoise Suppression Params

    /**
    */
    PathChangeDetector_ControlParams_st PCDParams;          ///< Echo Path Change Detector Params

    /**
    */
    Pbfdaf_ControlParams_st PbfdafParams;          ///< Pbfdaf Params

    /**
    */
    AntiHowling_ControlParams_st AntiHowlingParams;          ///< AntiHowling Params

    /**
    */
    LVNV_RsvParameters_ControlParams_st LVNV_RsvParameters;          ///< Reserved Params

} LVNV_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVBD_SetControlParameters function but they
* will not take effect until the next call to the LVBD_Process function.
*
* @see LVBD_SetControlParameters
* @see LVBD_Process
*/
typedef struct
{
    /**
    Set Bulk Delay operating mode on/off.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en BD_OperatingMode;          ///< Set Bulk Delay operating mode on/off

    /**
    This parameter compensates for the fixed delay in the echo path and can be
    measured in advance. This delay is caused by the audio I/O buffering, AD/DA
    converter and propagation time between the loudspeaker and microphone. The unit
    of 'BulkDelay' is [number of samples] at the respective sampling rate.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVBD_BULKDELAY_MIN (0)</td>
        <td>@ref LVBD_BULKDELAY_DEFAULT (0)</td>
        <td>@ref LVBD_BULKDELAY_MAX (6400)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 BulkDelay;          ///< Sets Bulk Delay

    /**
    This param sets the gain for the Echo reference signal. Value 8192 belong to
    0dB<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref LVBD_BD_GAIN_MIN (0)</td>
        <td>@ref LVBD_BD_GAIN_DEFAULT (8192)</td>
        <td>@ref LVBD_BD_GAIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 BD_Gain;          ///< Apply Gain to Echo reference signal

} LVBD_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVINPUTROUTING_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVINPUTROUTING_Tx_Process function.
*
* @see LVINPUTROUTING_Tx_SetControlParameters
* @see LVINPUTROUTING_Tx_Process
*/
typedef struct
{
    /**
    Selects the input channel for the first microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVBuffer_Channel_en MicrophoneChannel_0;          ///< Channel routing for the first microphone input of the LVVE_Tx_Process function.

    /**
    Selects the input channel for the second microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVBuffer_Channel_en MicrophoneChannel_1;          ///< Channel routing for the second microphone input of the LVVE_Tx_Process function.

    /**
    Selects the input channel for the third microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVBuffer_Channel_en MicrophoneChannel_2;          ///< Channel routing for the third microphone input of the LVVE_Tx_Process function.

    /**
    Selects the input channel for the fourth microphone channel of the
    VoiceExperience module before processing. Each audio channel can be selected
    for any microphone channel, duplicates are allowed. If the selected audio
    channel is not available from the input audio stream, a muted signal will be
    passed to VoiceExperience.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVBuffer_Channel_en MicrophoneChannel_3;          ///< Channel routing for the fourth microphone input of the LVVE_Tx_Process function.

} LVINPUTROUTING_Tx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVREFERENCEROUTING_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVREFERENCEROUTING_Tx_Process function.
*
* @see LVREFERENCEROUTING_Tx_SetControlParameters
* @see LVREFERENCEROUTING_Tx_Process
*/
typedef struct
{
    /**
    Selects the input channel for the first reference channel of the
    VoiceExperience module before processing. Each channel can be selected for any
    reference channel, duplicates are allowed. If the selected channel is not
    available from the reference audio stream, a muted signal will be passed to
    VoiceExperience.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVE_Tx_ReferenceChannel_en ReferenceChannel_0;          ///< Channel routing for the first reference input of the LVVE_Tx_Process function.

    /**
    Selects the input channel for the second reference channel of the
    VoiceExperience module before processing. Each channel can be selected for any
    reference channel, duplicates are allowed. If the selected channel is not
    available from the reference audio stream, a muted signal will be passed to
    VoiceExperience.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVE_Tx_ReferenceChannel_en ReferenceChannel_1;          ///< Channel routing for the second reference input of the LVVE_Tx_Process function.

} LVREFERENCEROUTING_Tx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVOUTPUTROUTING_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVOUTPUTROUTING_Tx_Process function.
*
* @see LVOUTPUTROUTING_Tx_SetControlParameters
* @see LVOUTPUTROUTING_Tx_Process
*/
typedef struct
{
    /**
    Selects the origin for the first audio channel of the output stream. The origin
    can be selected from the processed output of the VoiceExperience processing or
    from traces of internal signals (e.g. the AEC Reference).<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVVE_Tx_OutputChannel_en OutputChannel_0;          ///< Channel routing for the output channels returned by the Process function.

} LVOUTPUTROUTING_Tx_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVLPF_SetControlParameters function but they
* will not take effect until the next call to the LVLPF_Process function.
*
* @see LVLPF_SetControlParameters
* @see LVLPF_Process
*/
typedef struct
{
    /**
    Turns on/off Low Pass filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en LPF_OperatingMode;          ///< Turns on/off Low Pass filter

    /**
    Sets the 3dB corner frequency of the low-pass filter. In case NoiseVoid is
    enabled, the same low-pass filter will be applied to the second microphone
    channel. The range of frequency is [3500 3900] for 8KHz, [3500 7800] for 16KHz,
    [3500, 11700] for 24KHz, [3500, 15600] for 32KHz and [3500, 23400] for 48KHz<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVLPF_LPF_CORNERFREQ_MIN (3500)</td>
        <td>@ref LVLPF_LPF_CORNERFREQ_DEFAULT (3700)</td>
        <td>@ref LVLPF_LPF_CORNERFREQ_MAX (23400)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 LPF_CornerFreq;          ///< Low Pass Filter Corner Frequency in Hz

} LVLPF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVHPFREF_SetControlParameters function but they
* will not take effect until the next call to the LVHPFREF_Process function.
*
* @see LVHPFREF_SetControlParameters
* @see LVHPFREF_Process
*/
typedef struct
{
    /**
    Turns on/off High Pass filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVHPFREF_CORNERFREQ_MIN (100)</td>
        <td>@ref LVHPFREF_CORNERFREQ_DEFAULT (700)</td>
        <td>@ref LVHPFREF_CORNERFREQ_MAX (2000)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

} LVHPFREF_ControlParams_st;

/**
* The control parameters are used to control the overall module behavior. These parameters may
* be changed at any time during processing using the LVVE_Tx_SetControlParameters function but they
* will not take effect until the next call to the LVVE_Tx_Process function.
*
* @see LVVE_Tx_SetControlParameters
* @see LVVE_Tx_Process
*/
typedef struct
{
    /**
    Microphone Routing Control Parameter Structure.<br>
    */
    LVINPUTROUTING_Tx_ControlParams_st MicrophoneRouting;          ///< Microphone Routing Control Parameter Structure

    /**
    Echo Reference Routing Control Parameter Structure.<br>
    */
    LVREFERENCEROUTING_Tx_ControlParams_st ReferenceRouting;          ///< Echo Reference Routing Control Parameter Structure

    /**
    Output Routing Control Parameter Structure.<br>
    */
    LVOUTPUTROUTING_Tx_ControlParams_st OutputRouting;          ///< Output Routing Control Parameter Structure

    /**
    This enumerated type is used to set the operating mode of the Tx path. The
    processing can be separately set to enable all processing modules (i.e., ON),
    or to disable all processing modules (i.e., OFF). When OFF, the input is copied
    to the output with the applied channel routing and the LVVE sub modules can be
    provided with invalid parameters for modules. The sub module functions are not
    executed in this scenario.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en OperatingMode;          ///< Turns on/off all VoiceExperience processing modules on Tx

    /**
    This param can mute unmute the Rx/Tx engine output.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en Mute;          ///< This param can mute unmute the Rx/Tx engine output

    /**
    Set Bulk Delay operating mode on/off.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en BD_OperatingMode;          ///< Set Bulk Delay operating mode on/off

    /**
    This parameter compensates for the fixed delay in the echo path and can be
    measured in advance. This delay is caused by the audio I/O buffering, AD/DA
    converter and propagation time between the loudspeaker and microphone. The unit
    of 'BulkDelay' is [number of samples] at the respective sampling rate.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_TX_BULKDELAY_MIN (0)</td>
        <td>@ref LVVE_TX_BULKDELAY_DEFAULT (0)</td>
        <td>@ref LVVE_TX_BULKDELAY_MAX (6400)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 BulkDelay;          ///< Sets Bulk Delay

    /**
    This param sets the gain for the Echo reference signal. Value 8192 belong to
    0dB<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>2.13</td>
        <td>@ref LVVE_TX_BD_GAIN_MIN (0)</td>
        <td>@ref LVVE_TX_BD_GAIN_DEFAULT (8192)</td>
        <td>@ref LVVE_TX_BD_GAIN_MAX (32767)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_INT16 BD_Gain;          ///< Apply Gain to Echo reference signal

    /**
    Turns on/off VOL_Gain.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en VOL_OperatingMode;          ///< Turns on/off VOL_Gain

    /**
    The volume control gain can be used to set the overall volume level of the
    signal. The gain is set in dB with steps of 1dB.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>15.0</td>
        <td>@ref LVVE_TX_VOL_GAIN_MIN (-96)</td>
        <td>@ref LVVE_TX_VOL_GAIN_DEFAULT (0)</td>
        <td>@ref LVVE_TX_VOL_GAIN_MAX (24)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_INT16 VOL_Gain;          ///< Apply Gain to Input signal

    /**
    Turns on/off High Pass filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en HPF_OperatingMode;          ///< Turns on/off High Pass filter

    /**
    Sets the 3dB corner frequency of the high-pass filter.<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_TX_MIC_HPF_CORNERFREQ_MIN (50)</td>
        <td>@ref LVVE_TX_MIC_HPF_CORNERFREQ_DEFAULT (50)</td>
        <td>@ref LVVE_TX_MIC_HPF_CORNERFREQ_MAX (1500)</td>
    </tr>
    </table>
    This parameter can be tuned per volume level.<br>
    */
    LVM_UINT16 MIC_HPF_CornerFreq;          ///< High Pass Filter Corner Frequency in Hz

    /**
    High Pass Filter on Echo Reference Control Parameter Structure.<br>
    */
    LVHPFREF_ControlParams_st HPFREF_ControlParams;          ///< High Pass Filter on Echo Reference Control Parameter Structure

    /**
    Turns on/off Low Pass filter.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en LPF_OperatingMode;          ///< Turns on/off Low Pass filter

    /**
    Sets the 3dB corner frequency of the low-pass filter. In case NoiseVoid is
    enabled, the same low-pass filter will be applied to the second microphone
    channel. The range of frequency is [3500 3900] for 8KHz, [3500 7800] for 16KHz,
    [3500, 11700] for 24KHz, [3500, 15600] for 32KHz and [3500, 23400] for 48KHz<br>
    <table border>
    <tr>
        <td><b>Q-format</b></td>
        <td><b>Min</b></td>
        <td><b>Default</b></td>
        <td><b>Max</b></td>
    </tr>
    <tr>
        <td>16.0</td>
        <td>@ref LVVE_TX_MIC_LPF_CORNERFREQ_MIN (3500)</td>
        <td>@ref LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT (3700)</td>
        <td>@ref LVVE_TX_MIC_LPF_CORNERFREQ_MAX (23400)</td>
    </tr>
    </table>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_UINT16 MIC_LPF_CornerFreq;          ///< Low Pass Filter Corner Frequency in Hz

    /**
    NoiseVoid Control Parameter Structure.<br>
    */
    LVNV_ControlParams_st NV_ControlParams;          ///< NoiseVoid Control Parameter Structure

    /**
    WhisperMode Control Parameter Structure.<br>
    */
    LVWM_ControlParams_st WM_ControlParams;          ///< WhisperMode Control Parameter Structure

    /**
    Equalizer Operating mode.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en EQ_OperatingMode;          ///< Equalizer Operating mode

    /**
    Equalizer Control Parameter Structure.<br>
    */
    LVEQ_ControlParams_st EQ_ControlParams;          ///< Equalizer Control Parameters Structure

    /**
    DRC Control Parameter Structure.<br>
    */
    LVDRC_ControlParams_st DRC_ControlParams;          ///< DRC Control Parameter Structure

    /**
    Turns on/off Conformt Noise Generator Module.<br>
    This parameter isn't tuned per volume level.<br>
    */
    LVM_Mode_en CNG_OperatingMode;          ///< Turns on/off Comfort Noise Generator Module

    /**
    Comfort Noise Generator Control Parameter Structure.<br>
    */
    LVCNG_ControlParams_st CNG_ControlParams;          ///< Comfort Noise Generator Control Parameter Structure

} LVVE_Tx_ControlParams_st;

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif      /* __REL_LVVE_DEMO_NV_AVC_AGC_RX_DRC_RX_SWB_VIDPP_DEMO_H__ */

/* End of file */
