/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "SeninfDrv"

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <linux/mman-proprietary.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <utils/threads.h>
#include <utils/Errors.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include <cutils/atomic.h>
#include <mtkcam/def/common.h>
#include "kd_seninf.h"
#include "seninf_drv_tee.h"
#ifndef USING_MTK_LDVT
#define LOG_MSG(fmt, arg...)    ALOGD("[%s]" fmt, __FUNCTION__, ##arg)
#define LOG_WRN(fmt, arg...)    ALOGD("[%s]Warning(%5d):" fmt, __FUNCTION__, __LINE__, ##arg)
#define LOG_ERR(fmt, arg...)    ALOGE("[%s]Err(%5d):" fmt, __FUNCTION__, __LINE__, ##arg)
#else
#include "uvvf.h"
#if 1
#define LOG_MSG(fmt, arg...)    VV_MSG("[%s]" fmt, __FUNCTION__, ##arg)
#define LOG_WRN(fmt, arg...)    VV_MSG("[%s]Warning(%5d):" fmt, __FUNCTION__, __LINE__, ##arg)
#define LOG_ERR(fmt, arg...)    VV_MSG("[%s]Err(%5d):" fmt, __FUNCTION__, __LINE__, ##arg)
#else
#define LOG_MSG(fmt, arg...)
#define LOG_WRN(fmt, arg...)
#define LOG_ERR(fmt, arg...)
#endif
#endif


/*******************************************************************************
*
********************************************************************************/
SeninfDrv*
SeninfDrv::
getInstance(MBOOL isSecure)
{
    return (isSecure) ? SeninfDrvTeeImp::createInstance() : SeninfDrvImp::createInstance();
}

/*******************************************************************************
*
********************************************************************************/
SeninfDrvTeeImp::SeninfDrvTeeImp() : SeninfDrvImp()
{
    memset(&mTeeReg, 0, sizeof(SENINF_TEE_REG));
    mUser = 0;
    mpCa = nullptr;
    seninf_ca_open_session = nullptr;
    seninf_ca_close_session = nullptr;
    seninf_ca_sync_to_pa = nullptr;
    seninf_ca_sync_to_va = nullptr;
}

/*******************************************************************************
*
********************************************************************************/
SeninfDrvTeeImp::~SeninfDrvTeeImp()
{

}

/*******************************************************************************
*
********************************************************************************/
SeninfDrv*
SeninfDrvTeeImp::
createInstance()
{
    static SeninfDrvTeeImp drv;
    return (SeninfDrv*)&drv;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::init()
{
    int ret = 0;

    Mutex::Autolock lock(mLock);

    LOG_MSG("SeninfDrvTee init %d \n", mUser);
    if (mUser) {
        android_atomic_inc(&mUser);
        return 0;
    }

    ret = SeninfDrvImp::init();

    void *pDynamicLink = dlopen(SENINF_CA_LIB, RTLD_NOW);

    if (pDynamicLink == nullptr) {
        LOG_ERR("seninf_ca lib open failed (%s)", dlerror());
        return MFALSE;
    }

    if (seninf_ca_open_session == nullptr) {
        seninf_ca_open_session = (seninf_ca_open_session_t*) dlsym(pDynamicLink, SENINF_CA_OPEN_SESSION);
        if (seninf_ca_open_session == nullptr) {
            LOG_ERR("dlsym seninf_ca_open_session failed!");
            return MFALSE;
        }
    }

    if (seninf_ca_close_session == nullptr) {
        seninf_ca_close_session = (seninf_ca_close_session_t*) dlsym(pDynamicLink, SENINF_CA_CLOSE_SESSION);
        if (seninf_ca_close_session == nullptr) {
            LOG_ERR("dlsym seninf_ca_close_session failed!");
            return MFALSE;
        }
    }

    if (seninf_ca_sync_to_pa == nullptr) {
        seninf_ca_sync_to_pa = (seninf_ca_sync_to_pa_t*) dlsym(pDynamicLink, SENINF_CA_SYNC_TO_PA);
        if (seninf_ca_sync_to_pa == nullptr) {
            LOG_ERR("dlsym seninf_ca_sync_to_pa failed!");
            return MFALSE;
        }
    }

    if (seninf_ca_sync_to_va == nullptr) {
        seninf_ca_sync_to_va = (seninf_ca_sync_to_va_t*) dlsym(pDynamicLink, SENINF_CA_SYNC_TO_VA);
        if (seninf_ca_sync_to_va == nullptr) {
            LOG_ERR("dlsym seninf_ca_write_reg failed!");
            return MFALSE;
        }
    }

    mpCa = seninf_ca_open_session();
    if (mpCa == nullptr) {
        LOG_ERR("seninf_ca_open_session failed!");
        return MFALSE;
    }

    memset(&mTeeReg, 0, sizeof(SENINF_TEE_REG));
    seninf_ca_sync_to_va(mpCa, (void *)&mTeeReg);

    android_atomic_inc(&mUser);

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::uninit()
{
    int ret = 0;

    Mutex::Autolock lock(mLock);

    LOG_MSG("SeninfDrvTee uninit %d \n", mUser);
    android_atomic_dec(&mUser);

    if (mUser) {
        return 0;
    }

    seninf_ca_close_session(mpCa);

    memset(&mTeeReg, 0, sizeof(SENINF_TEE_REG));

    mpCa = nullptr;
    seninf_ca_open_session = nullptr;
    seninf_ca_close_session = nullptr;
    seninf_ca_sync_to_pa = nullptr;
    seninf_ca_sync_to_va = nullptr;

    ret = SeninfDrvImp::uninit();

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::sendCommand(int cmd, unsigned long arg1, unsigned long arg2, unsigned long arg3)
{
    int ret = 0;

    Mutex::Autolock lock(mLock);//for uninit, some pointer will be set to NULL

    switch (cmd) {
    case CMD_SENINF_SYNC_REG_TO_PA:
        LOG_MSG("Sync registers to PA");
        seninf_ca_sync_to_pa(mpCa, (void *)&mTeeReg);
        break;

    case CMD_SENINF_DEBUG_TASK:
        ret = debug();
        break;

    default:
        ret = SeninfDrvImp::sendCommand(cmd, arg1, arg2, arg3);
        break;
    }

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::setSeninfTopMuxCtrl(SENINF_MUX_ENUM mux, SENINF_ENUM seninfSrc)
{
    int ret = 0;
    SENINF_TEE_REG *pTeeReg = (SENINF_TEE_REG *)&mTeeReg;

    SENINF_WRITE_REG(pTeeReg, SENINF_TOP_MUX_CTRL, (SENINF_READ_REG(pTeeReg, SENINF_TOP_MUX_CTRL)&(~(0xF<<(mux * 4)))|(seninfSrc&0xF)<<(mux * 4)));

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::setSeninfCamTGMuxCtrl(unsigned int targetCamTG, SENINF_MUX_ENUM muxSrc)
{
    int ret = 0;
    SENINF_TEE_REG *pTeeReg = (SENINF_TEE_REG *)&mTeeReg;

    SENINF_WRITE_REG(pTeeReg, SENINF_TOP_CAM_MUX_CTRL, (SENINF_READ_REG(pTeeReg, SENINF_TOP_CAM_MUX_CTRL)&(~(0xF<<(targetCamTG * 4)))|(muxSrc&0xF)<<(targetCamTG * 4)));

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::getSeninfTopMuxCtrl(SENINF_MUX_ENUM mux)
{
    SENINF_TEE_REG *pTeeReg = (SENINF_TEE_REG *)&mTeeReg;

    return (SENINF_READ_REG(pTeeReg, SENINF_TOP_MUX_CTRL) & (0xF<<(mux * 4)))>>(mux * 4);
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::getSeninfCamTGMuxCtrl(unsigned int targetCam)
{
    SENINF_TEE_REG *pTeeReg = (SENINF_TEE_REG *)&mTeeReg;

    return (SENINF_READ_REG(pTeeReg, SENINF_TOP_CAM_MUX_CTRL) & (0xF<<(targetCam * 4)))>>(targetCam * 4);
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::setSeninfMuxCtrl(SENINF_MUX_ENUM mux, unsigned long hsPol, unsigned long vsPol, SENINF_SOURCE_ENUM inSrcTypeSel, TG_FORMAT_ENUM inDataType, unsigned int pixelMode)
{
    int ret = 0;
    unsigned int temp = 0;
    SENINF_TEE_REG_MUX *pTeeRegMux = (SENINF_TEE_REG_MUX *)&mTeeReg.SENINF1_MUX_CTRL;
    pTeeRegMux += mux;

    SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_SRC_SEL) = inSrcTypeSel;
    SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL_EXT, SENINF_SRC_SEL_EXT) = (inSrcTypeSel == TEST_MODEL) ? 0 : 1;

    if(1 == pixelMode) { /*2 Pixel*/
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL_EXT, SENINF_PIX_SEL_EXT) = 0;
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_PIX_SEL) = 1;
    }
    else if(2 == pixelMode) { /* 4 Pixel*/
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL_EXT, SENINF_PIX_SEL_EXT) = 1;
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_PIX_SEL) = 0;
    }
    else {/* 1 pixel*/
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL_EXT, SENINF_PIX_SEL_EXT) = 0;
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_PIX_SEL) = 0;
    }

    if(JPEG_FMT != inDataType) {
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, FIFO_FULL_WR_EN) = 2;
    }
    else {
        SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, FIFO_FULL_WR_EN) = 0;
    }

    if ((CSI2 == inSrcTypeSel)||(MIPI_SENSOR <= inSrcTypeSel)) {
        if(JPEG_FMT != inDataType) {/*Need to use Default for New design*/
            SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, FIFO_FLUSH_EN) = 0x1B;
            SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, FIFO_PUSH_EN) = 0x1F;
        }
        else {
            SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, FIFO_FLUSH_EN) = 0x18;
            SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, FIFO_PUSH_EN) = 0x1E;
        }
    }
    /*Disable send fifo to cam*/
   // SENINF_BITS(pTeeRegMux, SENINF1_MUX_SPARE, SENINF_FIFO_FULL_SEL) = 0; keep default =1

    SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_HSYNC_POL) = hsPol;
    SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_VSYNC_POL) = vsPol;

    temp = SENINF_READ_REG(pTeeRegMux, SENINF1_MUX_CTRL);
    SENINF_WRITE_REG(pTeeRegMux,SENINF1_MUX_CTRL, temp|0x3);//reset
    SENINF_WRITE_REG(pTeeRegMux,SENINF1_MUX_CTRL, temp&0xFFFFFFFC);//clear reset

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
MBOOL SeninfDrvTeeImp::isMUXUsed(SENINF_MUX_ENUM mux)
{
    SENINF_TEE_REG_MUX *pTeeRegMux = (SENINF_TEE_REG_MUX *)&mTeeReg.SENINF1_MUX_CTRL;
    pTeeRegMux += mux;

    return SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_MUX_EN);
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::enableMUX(SENINF_MUX_ENUM mux)
{
    SENINF_TEE_REG_MUX *pTeeRegMux = (SENINF_TEE_REG_MUX *)&mTeeReg.SENINF1_MUX_CTRL;
    pTeeRegMux += mux;

    SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_MUX_EN) = 1;

    return 0;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::disableMUX(SENINF_MUX_ENUM mux)
{
    SENINF_TEE_REG_MUX *pTeeRegMux = (SENINF_TEE_REG_MUX *)&mTeeReg.SENINF1_MUX_CTRL;
    pTeeRegMux += mux;

    SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_MUX_EN) = 0;

    return 0;
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::debug()
{
    SENINF_TEE_REG  reg;
    volatile UINT32 *pRegPa = &reg.SENINF_TOP_MUX_CTRL.Raw;
    volatile UINT32 *pRegVa = &mTeeReg.SENINF_TOP_MUX_CTRL.Raw;

    seninf_ca_sync_to_va(mpCa, (void *)&reg);

    LOG_MSG("Seninf TEE registers (PA VA):");
    do {
        LOG_MSG("%x %x", *pRegPa, *pRegVa);
        pRegPa++;
        pRegVa++;
    } while (pRegPa != &reg.SENINF6_MUX_CTRL_EXT.Raw);

    return SeninfDrvImp::debug();
}

/*******************************************************************************
*
********************************************************************************/
int SeninfDrvTeeImp::reset(SENINF_ENUM seninf)
{
    int i;
    SENINF_TEE_REG_MUX *pTeeRegMux = (SENINF_TEE_REG_MUX *)&mTeeReg.SENINF1_MUX_CTRL;

    SeninfDrvImp::reset(seninf);

    for(i = SENINF_MUX1; i < SENINF_MUX_NUM; i++)
        if(getSeninfTopMuxCtrl((SENINF_MUX_ENUM)i) == seninf && isMUXUsed((SENINF_MUX_ENUM)i)) {
            pTeeRegMux += i;
            SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_MUX_SW_RST) = 1;
            usleep(1);
            SENINF_BITS(pTeeRegMux, SENINF1_MUX_CTRL, SENINF_MUX_SW_RST) = 0;
        }

    return 0;
}

