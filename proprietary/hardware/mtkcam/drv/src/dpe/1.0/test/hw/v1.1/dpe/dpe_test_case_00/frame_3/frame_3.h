#ifndef __DPE_TEST_CASE_00_FRAME3__
#define __DPE_TEST_CASE_00_FRAME3__

extern char frame_3_dpe_confo_l_frame_00_01[];
#define frame_3_dpe_confo_l_frame_00_01_size 38896
extern unsigned int frame_3_golden_dpe_confo_l_size;
extern char* frame_3_golden_dpe_confo_l_frame;

extern char frame_3_dpe_confo_r_frame_00_01[];
#define frame_3_dpe_confo_r_frame_00_01_size 38896
extern unsigned int frame_3_golden_dpe_confo_r_size;
extern char* frame_3_golden_dpe_confo_r_frame;

extern char frame_3_dpe_dvi_l_frame_00[];
#define frame_3_dpe_dvi_l_frame_00_size 77792
extern char frame_3_dpe_dvi_r_frame_00[];
#define frame_3_dpe_dvi_r_frame_00_size 77792
extern char frame_3_dpe_dvo_l_frame_00_01[];
#define frame_3_dpe_dvo_l_frame_00_01_size 77792
extern unsigned int frame_3_golden_dpe_dvo_l_size;
extern char* frame_3_golden_dpe_dvo_l_frame;

extern char frame_3_dpe_dvo_r_frame_00_01[];
#define frame_3_dpe_dvo_r_frame_00_01_size 77792
extern unsigned int frame_3_golden_dpe_dvo_r_size;
extern char* frame_3_golden_dpe_dvo_r_frame;

extern char frame_3_dpe_imgi_l_frame_00[];
#define frame_3_dpe_imgi_l_frame_00_size 38896
extern char frame_3_dpe_imgi_r_frame_00[];
#define frame_3_dpe_imgi_r_frame_00_size 38896
extern char frame_3_dpe_maski_l_frame_00[];
#define frame_3_dpe_maski_l_frame_00_size 38896
extern char frame_3_dpe_maski_r_frame_00[];
#define frame_3_dpe_maski_r_frame_00_size 38896
extern char frame_3_dpe_respo_l_frame_00_01[];
#define frame_3_dpe_respo_l_frame_00_01_size 38896
extern unsigned int frame_3_golden_dpe_respo_l_size;
extern char* frame_3_golden_dpe_respo_l_frame;

extern char frame_3_dpe_respo_r_frame_00_01[];
#define frame_3_dpe_respo_r_frame_00_01_size 38896
extern unsigned int frame_3_golden_dpe_respo_r_size;
extern char* frame_3_golden_dpe_respo_r_frame;

extern char frame_3_dpe_wmf_dpi_frame_00_00_0[];
#define frame_3_dpe_wmf_dpi_frame_00_00_0_size 32400
extern char* frame_3_in_dpe_wmf_dpi_frame_0;

extern char frame_3_dpe_wmf_dpi_frame_00_00_1[];
#define frame_3_dpe_wmf_dpi_frame_00_00_1_size 32400
extern char* frame_3_in_dpe_wmf_dpi_frame_1;

extern char frame_3_dpe_wmf_dpi_frame_00_00_2[];
#define frame_3_dpe_wmf_dpi_frame_00_00_2_size 32400
extern char* frame_3_in_dpe_wmf_dpi_frame_2;

extern char frame_3_dpe_wmf_dpi_frame_00_00_3[];
#define frame_3_dpe_wmf_dpi_frame_00_00_3_size 129600
extern char* frame_3_in_dpe_wmf_dpi_frame_3;

extern char frame_3_dpe_wmf_dpi_frame_00_00_4[];
#define frame_3_dpe_wmf_dpi_frame_00_00_4_size 32400
extern char* frame_3_in_dpe_wmf_dpi_frame_4;

extern char frame_3_dpe_wmf_dpo_frame_00_00_0[];
#define frame_3_dpe_wmf_dpo_frame_00_00_0_size 32400
extern unsigned int frame_3_golden_dpe_wmf_dpo_0_size;
extern char* frame_3_golden_dpe_wmf_dpo_frame_0;

extern char frame_3_dpe_wmf_dpo_frame_00_00_1[];
#define frame_3_dpe_wmf_dpo_frame_00_00_1_size 32400
extern unsigned int frame_3_golden_dpe_wmf_dpo_1_size;
extern char* frame_3_golden_dpe_wmf_dpo_frame_1;

extern char frame_3_dpe_wmf_dpo_frame_00_00_2[];
#define frame_3_dpe_wmf_dpo_frame_00_00_2_size 32400
extern unsigned int frame_3_golden_dpe_wmf_dpo_2_size;
extern char* frame_3_golden_dpe_wmf_dpo_frame_2;

extern char frame_3_dpe_wmf_dpo_frame_00_00_3[];
#define frame_3_dpe_wmf_dpo_frame_00_00_3_size 129600
extern unsigned int frame_3_golden_dpe_wmf_dpo_3_size;
extern char* frame_3_golden_dpe_wmf_dpo_frame_3;

extern char frame_3_dpe_wmf_dpo_frame_00_00_4[];
#define frame_3_dpe_wmf_dpo_frame_00_00_4_size 129600
extern unsigned int frame_3_golden_dpe_wmf_dpo_4_size;
extern char* frame_3_golden_dpe_wmf_dpo_frame_4;

extern char frame_3_dpe_wmf_imgi_frame_00_00_0[];
#define frame_3_dpe_wmf_imgi_frame_00_00_0_size 32400
extern char* frame_3_in_dpe_wmf_imgi_frame_0;

extern char frame_3_dpe_wmf_imgi_frame_00_00_1[];
#define frame_3_dpe_wmf_imgi_frame_00_00_1_size 32400
extern char* frame_3_in_dpe_wmf_imgi_frame_1;

extern char frame_3_dpe_wmf_imgi_frame_00_00_2[];
#define frame_3_dpe_wmf_imgi_frame_00_00_2_size 32400
extern char* frame_3_in_dpe_wmf_imgi_frame_2;

extern char frame_3_dpe_wmf_imgi_frame_00_00_3[];
#define frame_3_dpe_wmf_imgi_frame_00_00_3_size 129600
extern char* frame_3_in_dpe_wmf_imgi_frame_3;

extern char frame_3_dpe_wmf_imgi_frame_00_00_4[];
#define frame_3_dpe_wmf_imgi_frame_00_00_4_size 129600
extern char* frame_3_in_dpe_wmf_imgi_frame_4;

extern char frame_3_dpe_wmf_maski_frame_00_00_0[];
#define frame_3_dpe_wmf_maski_frame_00_00_0_size 32400
extern char* frame_3_in_dpe_wmf_maski_frame_0;

extern char frame_3_dpe_wmf_maski_frame_00_00_1[];
#define frame_3_dpe_wmf_maski_frame_00_00_1_size 32400
extern char* frame_3_in_dpe_wmf_maski_frame_1;

extern char frame_3_dpe_wmf_tbli_frame_00_00_0[];
#define frame_3_dpe_wmf_tbli_frame_00_00_0_size 256
extern char* frame_3_in_dpe_wmf_tbli_frame_0;

extern char frame_3_dpe_wmf_tbli_frame_00_00_1[];
#define frame_3_dpe_wmf_tbli_frame_00_00_1_size 256
extern char* frame_3_in_dpe_wmf_tbli_frame_1;

extern char frame_3_dpe_wmf_tbli_frame_00_00_2[];
#define frame_3_dpe_wmf_tbli_frame_00_00_2_size 256
extern char* frame_3_in_dpe_wmf_tbli_frame_2;

extern char frame_3_dpe_wmf_tbli_frame_00_00_3[];
#define frame_3_dpe_wmf_tbli_frame_00_00_3_size 256
extern char* frame_3_in_dpe_wmf_tbli_frame_3;

extern char frame_3_dpe_wmf_tbli_frame_00_00_4[];
#define frame_3_dpe_wmf_tbli_frame_00_00_4_size 256
extern char* frame_3_in_dpe_wmf_tbli_frame_4;

extern void getframe_3GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2,
	unsigned long* golden_dpe_wmf_dpo_frame_3,
	unsigned long* golden_dpe_wmf_dpo_frame_4
);

#endif