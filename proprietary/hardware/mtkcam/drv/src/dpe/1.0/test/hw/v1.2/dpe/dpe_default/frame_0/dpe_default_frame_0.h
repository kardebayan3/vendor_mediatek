#ifndef __DPE_DPE_DEFAULT_FRAME0__
#define __DPE_DPE_DEFAULT_FRAME0__

extern char dpe_default_frame_0_dpe_confo_l_frame_00_01[];
#define dpe_default_frame_0_dpe_confo_l_frame_00_01_size 38896
extern unsigned int dpe_default_frame_0_golden_dpe_confo_l_size;
extern char* dpe_default_frame_0_golden_dpe_confo_l_frame;

extern char dpe_default_frame_0_dpe_confo_r_frame_00_01[];
#define dpe_default_frame_0_dpe_confo_r_frame_00_01_size 38896
extern unsigned int dpe_default_frame_0_golden_dpe_confo_r_size;
extern char* dpe_default_frame_0_golden_dpe_confo_r_frame;

extern char dpe_default_frame_0_dpe_dvi_l_frame_00[];
#define dpe_default_frame_0_dpe_dvi_l_frame_00_size 77792
extern char dpe_default_frame_0_dpe_dvi_r_frame_00[];
#define dpe_default_frame_0_dpe_dvi_r_frame_00_size 77792
extern char dpe_default_frame_0_dpe_dvo_l_frame_00_01[];
#define dpe_default_frame_0_dpe_dvo_l_frame_00_01_size 77792
extern unsigned int dpe_default_frame_0_golden_dpe_dvo_l_size;
extern char* dpe_default_frame_0_golden_dpe_dvo_l_frame;

extern char dpe_default_frame_0_dpe_dvo_r_frame_00_01[];
#define dpe_default_frame_0_dpe_dvo_r_frame_00_01_size 77792
extern unsigned int dpe_default_frame_0_golden_dpe_dvo_r_size;
extern char* dpe_default_frame_0_golden_dpe_dvo_r_frame;

extern char dpe_default_frame_0_dpe_imgi_l_frame_00[];
#define dpe_default_frame_0_dpe_imgi_l_frame_00_size 38896
extern char dpe_default_frame_0_dpe_imgi_r_frame_00[];
#define dpe_default_frame_0_dpe_imgi_r_frame_00_size 38896
extern char dpe_default_frame_0_dpe_maski_l_frame_00[];
#define dpe_default_frame_0_dpe_maski_l_frame_00_size 38896
extern char dpe_default_frame_0_dpe_maski_r_frame_00[];
#define dpe_default_frame_0_dpe_maski_r_frame_00_size 38896
extern char dpe_default_frame_0_dpe_respo_l_frame_00_01[];
#define dpe_default_frame_0_dpe_respo_l_frame_00_01_size 38896
extern unsigned int dpe_default_frame_0_golden_dpe_respo_l_size;
extern char* dpe_default_frame_0_golden_dpe_respo_l_frame;

extern char dpe_default_frame_0_dpe_respo_r_frame_00_01[];
#define dpe_default_frame_0_dpe_respo_r_frame_00_01_size 38896
extern unsigned int dpe_default_frame_0_golden_dpe_respo_r_size;
extern char* dpe_default_frame_0_golden_dpe_respo_r_frame;

extern void dpe_default_getframe_0GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame
);

#endif