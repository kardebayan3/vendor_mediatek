#include "dpe_default_frame_0_dpe_setting_00.h"

/******************************************************************************
* DPE Test Case
*******************************************************************************/
//#define DVE_IS_SECURE
#define DVE_ENABLE 0x1
int dpe_default_frame_0_golden_l_start_x = 0;
int dpe_default_frame_0_golden_l_start_y = 5;
int dpe_default_frame_0_golden_l_end_x = 2172;
int dpe_default_frame_0_golden_l_end_y = 1099;
int dpe_default_frame_0_golden_r_start_x = 5;
int dpe_default_frame_0_golden_r_start_y = 5;
int dpe_default_frame_0_golden_r_end_x = 2172;
int dpe_default_frame_0_golden_r_end_y = 1099;
MUINT32 dpe_default_frame_0_golden_DVE_HORZ_SV = 0x3DF;
MUINT32 dpe_default_frame_0_golden_DVE_VERT_SV =  0x00;

int dpe_default();

MBOOL g_bDVECallback;

//typedef MVOID                   (*PFN_CALLBACK_T)(DVEParams& rParams);

MVOID DPE_DVECallback(DVEParams& rParams);

MVOID DPE_DVECallback(DVEParams& rParams)
{
    MUINT32 DVE_HORZ_SV;
    MUINT32 DVE_VERT_SV;

    printf("--- [DVE callback func]\n");

    vector<DVEConfig>::iterator iter = rParams.mDVEConfigVec.begin();
    for (;iter!= rParams.mDVEConfigVec.end();iter++)
    {
        DVE_VERT_SV = (*iter).Dve_Vert_Sv;
        DVE_HORZ_SV = (*iter).Dve_Horz_Sv;        
        printf("DVE_VERT_SV:%d, DVE_HORZ_SV:%d\n", DVE_VERT_SV, DVE_HORZ_SV);
        if ( (dpe_default_frame_0_golden_DVE_HORZ_SV == DVE_HORZ_SV) &&
             (dpe_default_frame_0_golden_DVE_VERT_SV == DVE_VERT_SV) )
        {
            //Pass
            printf("dpe DVE Statistic Result 0 bit true pass!!!\n");
        }
        else
        {
            //Erro
            printf("dpe DVE Statistic Result 0 bit true fail, DVE_HORZ_SV:(%d), DVE_VERT_SV:(%d)!!!\n", DVE_HORZ_SV, DVE_VERT_SV);
        }
    }
    g_bDVECallback = MTRUE;

}


//#include "../../imageio/test/DIP_pics/P2A_FG/imgi_640_480_10.h"
/*********************************************************************************/
int dpe_default()
{
    int ret=0;
    printf("--- [dpe_default]\n");
    
    
    //i need a varaible to switch the difference between test code and dpe_drv.cpp
    
    NSCam::NSIoPipe::NSDpe::IDpeStream* pStream;
    pStream= NSCam::NSIoPipe::NSDpe::IDpeStream::createInstance("test_dpe_default");
    pStream->init();   
    printf("--- [test_dpe_default...DpeStream init done\n");

    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    int golden_l_start_x = dpe_default_frame_0_golden_l_start_x;
    int golden_l_start_y = dpe_default_frame_0_golden_l_start_y;
    int golden_l_end_x = dpe_default_frame_0_golden_l_end_x;
    int golden_l_end_y = dpe_default_frame_0_golden_l_end_y;
    int golden_r_start_x = dpe_default_frame_0_golden_r_start_x;
    int golden_r_start_y = dpe_default_frame_0_golden_r_start_y;
    int golden_r_end_x = dpe_default_frame_0_golden_r_end_x;
    int golden_r_end_y = dpe_default_frame_0_golden_r_end_y;
    //MUINT32 golden_DVE_HORZ_SV = dpe_default_frame_0_golden_DVE_HORZ_SV; //unused
    //MUINT32 golden_DVE_VERT_SV = dpe_default_frame_0_golden_DVE_VERT_SV; //unused
    //copy the pointer of golden answer of DPE
    unsigned long golden_dpe_dvo_l_frame;
    unsigned long golden_dpe_dvo_r_frame;
    unsigned long golden_dpe_confo_l_frame;
    unsigned long golden_dpe_confo_r_frame;
    unsigned long golden_dpe_respo_l_frame;
    unsigned long golden_dpe_respo_r_frame;
    dpe_default_getframe_0GoldPointer(
    &golden_dpe_dvo_l_frame,
    &golden_dpe_dvo_r_frame,
    &golden_dpe_confo_l_frame,
    &golden_dpe_confo_r_frame,
    &golden_dpe_respo_l_frame,
    &golden_dpe_respo_r_frame
    );

    //input frame pointer
    /* unused
    char* dpe_imgi_l_frame;
    char* dpe_imgi_r_frame;
    char* dpe_dvi_l_frame;
    char* dpe_dvi_r_frame;
    char* dpe_maski_l_frame;
    char* dpe_maski_r_frame;
    */
#if DVE_ENABLE

    printf("--- dve input allocate init\n");

    IMEM_BUF_INFO buf_imgi_l_frame;
    buf_imgi_l_frame.size= dpe_default_frame_0_dpe_imgi_l_frame_00_size;
    printf("buf_imgi_l_frame.size:%d",buf_imgi_l_frame.size);

    #ifdef DVE_IS_SECURE
    if ( mpImemDrv->allocSecureBuf(&buf_imgi_l_frame) ){
        printf("[Error] imgi_l secure buf alloc fail!!");
        return false;
    }
    buf_imgi_l_frame.secHandle = (MUINT32)buf_imgi_l_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_imgi_l_frame);
    mpImemDrv->mapPhyAddr(&buf_imgi_l_frame);
    memcpy( (MUINT8*)(buf_imgi_l_frame.virtAddr), (MUINT8*)(dpe_default_frame_0_dpe_imgi_l_frame_00), buf_imgi_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_imgi_l_frame);
    #endif

    IMEM_BUF_INFO buf_imgi_r_frame;
    buf_imgi_r_frame.size=dpe_default_frame_0_dpe_imgi_r_frame_00_size;
    printf("buf_imgi_r_frame.size:%d",buf_imgi_r_frame.size);
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_imgi_r_frame) ){
            printf("[Error] imgi_r secure buf alloc fail!!");
            return false;
        }
        buf_imgi_r_frame.secHandle = (MUINT32)buf_imgi_r_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_imgi_r_frame);
    mpImemDrv->mapPhyAddr(&buf_imgi_r_frame);
    memcpy( (MUINT8*)(buf_imgi_r_frame.virtAddr), (MUINT8*)(dpe_default_frame_0_dpe_imgi_r_frame_00), buf_imgi_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_imgi_r_frame);
    #endif

    IMEM_BUF_INFO buf_dvi_l_frame;
    buf_dvi_l_frame.size=dpe_default_frame_0_dpe_dvi_l_frame_00_size;
    printf("buf_dvi_l_frame.size:%d",buf_dvi_l_frame.size);
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_dvi_l_frame) ){
            printf("[Error] dvi_l secure buf alloc fail!!");
            return false;
        }
        buf_dvi_l_frame.secHandle = (MUINT32)buf_dvi_l_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_dvi_l_frame);
    mpImemDrv->mapPhyAddr(&buf_dvi_l_frame);
    memcpy( (MUINT8*)(buf_dvi_l_frame.virtAddr), (MUINT8*)(dpe_default_frame_0_dpe_dvi_l_frame_00), buf_dvi_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvi_l_frame);
    #endif

    IMEM_BUF_INFO buf_dvi_r_frame;
    buf_dvi_r_frame.size=dpe_default_frame_0_dpe_dvi_r_frame_00_size;
    printf("buf_dvi_r_frame.size:%d",buf_dvi_r_frame.size);
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_dvi_r_frame) ){
            printf("[Error] dvi_r secure buf alloc fail!!");
            return false;
        }
        buf_dvi_r_frame.secHandle = (MUINT32)buf_dvi_r_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_dvi_r_frame);
    mpImemDrv->mapPhyAddr(&buf_dvi_r_frame);
    memcpy( (MUINT8*)(buf_dvi_r_frame.virtAddr), (MUINT8*)(dpe_default_frame_0_dpe_dvi_r_frame_00), buf_dvi_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvi_r_frame);
    #endif

    IMEM_BUF_INFO buf_maski_l_frame;
    buf_maski_l_frame.size=dpe_default_frame_0_dpe_maski_l_frame_00_size;
    printf("buf_maski_l_frame.size:%d",buf_maski_l_frame.size);
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_maski_l_frame) ){
            printf("[Error] maski_l secure buf alloc fail!!");
            return false;
        }
        buf_maski_l_frame.secHandle = (MUINT32)buf_maski_l_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_maski_l_frame);
    mpImemDrv->mapPhyAddr(&buf_maski_l_frame);
    memcpy( (MUINT8*)(buf_maski_l_frame.virtAddr), (MUINT8*)(dpe_default_frame_0_dpe_maski_l_frame_00), buf_maski_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_maski_l_frame);
    #endif

    IMEM_BUF_INFO buf_maski_r_frame;
    buf_maski_r_frame.size=dpe_default_frame_0_dpe_maski_r_frame_00_size;
    printf("buf_maski_r_frame.size:%d",buf_maski_r_frame.size);
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_maski_r_frame) ){
            printf("[Error] maski_r secure buf alloc fail!!");
            return false;
        }
        buf_maski_r_frame.secHandle = (MUINT32)buf_maski_r_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_maski_r_frame);
    mpImemDrv->mapPhyAddr(&buf_maski_r_frame);
    memcpy( (MUINT8*)(buf_maski_r_frame.virtAddr), (MUINT8*)(dpe_default_frame_0_dpe_maski_r_frame_00), buf_maski_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_maski_r_frame);
    #endif

    printf("--- dve  input  allocate done\n");

#endif

    //allocate the memory to be used the Target of DPE
    /* unused
    char* dpe_dvo_l_frame;
    char* dpe_dvo_r_frame;
    char* dpe_confo_l_frame;
    char* dpe_confo_r_frame;
    char* dpe_respo_l_frame;
    char* dpe_respo_r_frame;
    */

    printf("#########################################################\n");
    printf("###########frame_0Start to Test !!!!###########\n");
    printf("#########################################################\n");

#if DVE_ENABLE
    IMEM_BUF_INFO buf_dvo_l_frame;    
    buf_dvo_l_frame.size = dpe_default_frame_0_dpe_dvo_l_frame_00_01_size;
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_dvo_l_frame) ){
            printf("[Error] dvo_l secure buf alloc fail!!");
            return false;
        }
        buf_dvo_l_frame.secHandle = (MUINT32)buf_dvo_l_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_dvo_l_frame);
    mpImemDrv->mapPhyAddr(&buf_dvo_l_frame);
    memset((MUINT8*)buf_dvo_l_frame.virtAddr, 0xffffffff, buf_dvo_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvo_l_frame);
    #endif

    IMEM_BUF_INFO buf_dvo_r_frame;    
    buf_dvo_r_frame.size = dpe_default_frame_0_dpe_dvo_r_frame_00_01_size;
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_dvo_r_frame) ){
            printf("[Error] dvo_r secure buf alloc fail!!");
            return false;
        }
        buf_dvo_r_frame.secHandle = (MUINT32)buf_dvo_r_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_dvo_r_frame);
    mpImemDrv->mapPhyAddr(&buf_dvo_r_frame);
    memset((MUINT8*)buf_dvo_r_frame.virtAddr, 0xffffffff, buf_dvo_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_dvo_r_frame);
    #endif

    IMEM_BUF_INFO buf_confo_l_frame;    
    buf_confo_l_frame.size = dpe_default_frame_0_dpe_confo_l_frame_00_01_size;
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_confo_l_frame) ){
            printf("[Error] confo_l secure buf alloc fail!!");
            return false;
        }
        buf_confo_l_frame.secHandle = (MUINT32)buf_confo_l_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_confo_l_frame);
    mpImemDrv->mapPhyAddr(&buf_confo_l_frame);
    memset((MUINT8*)buf_confo_l_frame.virtAddr, 0xffffffff, buf_confo_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_confo_l_frame);
    #endif

    IMEM_BUF_INFO buf_confo_r_frame;    
    buf_confo_r_frame.size = dpe_default_frame_0_dpe_confo_r_frame_00_01_size;
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_confo_r_frame) ){
            printf("[Error] confo_r secure buf alloc fail!!");
            return false;
        }
        buf_confo_r_frame.secHandle = (MUINT32)buf_confo_r_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_confo_r_frame);
    mpImemDrv->mapPhyAddr(&buf_confo_r_frame);
    memset((MUINT8*)buf_confo_r_frame.virtAddr, 0xffffffff, buf_confo_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_confo_r_frame);
    #endif

    IMEM_BUF_INFO buf_respo_l_frame;    
    buf_respo_l_frame.size = dpe_default_frame_0_dpe_respo_l_frame_00_01_size;
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_respo_l_frame) ){
            printf("[Error] respo_l secure buf alloc fail!!");
            return false;
        }
        buf_respo_l_frame.secHandle = (MUINT32)buf_respo_l_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_respo_l_frame);
    mpImemDrv->mapPhyAddr(&buf_respo_l_frame);
    memset((MUINT8*)buf_respo_l_frame.virtAddr, 0xffffffff, buf_respo_l_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_respo_l_frame);
    #endif

    IMEM_BUF_INFO buf_respo_r_frame;    
    buf_respo_r_frame.size = dpe_default_frame_0_dpe_respo_r_frame_00_01_size;
    #ifdef DVE_IS_SECURE
        if ( mpImemDrv->allocSecureBuf(&buf_respo_r_frame) ){
            printf("[Error] respo_r secure buf alloc fail!!");
            return false;
        }
        buf_respo_r_frame.secHandle = (MUINT32)buf_respo_r_frame.phyAddr;
    #else
    mpImemDrv->allocVirtBuf(&buf_respo_r_frame);
    mpImemDrv->mapPhyAddr(&buf_respo_r_frame);
    memset((MUINT8*)buf_respo_r_frame.virtAddr, 0xffffffff, buf_respo_r_frame.size);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf_respo_r_frame);
    #endif
#endif

#if DVE_ENABLE
    printf("golden_dpe_dvo_l_size:%d, golden_dpe_dvo_r_size:%d\n", dpe_default_frame_0_golden_dpe_dvo_l_size, dpe_default_frame_0_golden_dpe_dvo_r_size);
    printf("golden_dpe_confo_l_size:%d, golden_dpe_confo_r_size:%d\n",dpe_default_frame_0_golden_dpe_confo_l_size , dpe_default_frame_0_golden_dpe_confo_r_size);
    printf("golden_dpe_respo_l_size:%d, golden_dpe_respo_r_size:%d\n",dpe_default_frame_0_golden_dpe_respo_l_size , dpe_default_frame_0_golden_dpe_respo_r_size);

#endif

    DVEParams rDveParams;
    DVEConfig dveconfig;
    rDveParams.mpfnCallback = DPE_DVECallback;  

    dveconfig.Dve_Skp_Pre_Dv = false;
    dveconfig.Dve_Mask_En = true;
    dveconfig.Dve_l_Bbox_En = true;
    dveconfig.Dve_r_Bbox_En = true;
    dveconfig.Dve_Horz_Ds_Mode = 0x0;
    dveconfig.Dve_Vert_Ds_Mode = 0x0;
    dveconfig.Dve_Imgi_l_Fmt = DPE_IMGI_Y_FMT;
    dveconfig.Dve_Imgi_r_Fmt = DPE_IMGI_Y_FMT;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_RIGHT = 0x880;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_LEFT = 0x0;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_BOTTOM = 0x478;
    dveconfig.Dve_Org_l_Bbox.DVE_ORG_BBOX_TOP = 0x0;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_RIGHT = 0x87c;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_LEFT = 0x5;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_BOTTOM = 0x44b;
    dveconfig.Dve_Org_r_Bbox.DVE_ORG_BBOX_TOP = 0x5;
    dveconfig.Dve_Org_Width = 0x880;
    dveconfig.Dve_Org_Height = 0x478;
    dveconfig.Dve_Org_Horz_Sr_1 = 0x80;
    dveconfig.Dve_Org_Horz_Sr_0 = 0x01ff;
    dveconfig.Dve_Org_Vert_Sr_0 = 0x18;
    dveconfig.Dve_Org_Start_Vert_Sv = 0x0;
    dveconfig.Dve_Org_Start_Horz_Sv = 0x0;
    dveconfig.Dve_Cand_Num = 0x9;


    dveconfig.Dve_Cand_0.DVE_CAND_SEL = 0xc;
    dveconfig.Dve_Cand_0.DVE_CAND_TYPE = 0x0;
    dveconfig.Dve_Cand_1.DVE_CAND_SEL = 0xb;
    dveconfig.Dve_Cand_1.DVE_CAND_TYPE = 0x1;
    dveconfig.Dve_Cand_2.DVE_CAND_SEL = 0x12;
    dveconfig.Dve_Cand_2.DVE_CAND_TYPE = 0x2;
    dveconfig.Dve_Cand_3.DVE_CAND_SEL = 0x7;
    dveconfig.Dve_Cand_3.DVE_CAND_TYPE = 0x1;

    dveconfig.Dve_Cand_4.DVE_CAND_SEL = 0x1f;
    dveconfig.Dve_Cand_4.DVE_CAND_TYPE = 0x6;
    dveconfig.Dve_Cand_5.DVE_CAND_SEL = 0x19;
    dveconfig.Dve_Cand_5.DVE_CAND_TYPE = 0x3;
    dveconfig.Dve_Cand_6.DVE_CAND_SEL = 0x1a;
    dveconfig.Dve_Cand_6.DVE_CAND_TYPE = 0x3;
    dveconfig.Dve_Cand_7.DVE_CAND_SEL = 0x1c;
    dveconfig.Dve_Cand_7.DVE_CAND_TYPE = 0x4;
    dveconfig.Dve_Rand_Lut_0 = 0x0B;
    dveconfig.Dve_Rand_Lut_1 = 0x17;
    dveconfig.Dve_Rand_Lut_2 = 0x1f;
    dveconfig.Dve_Rand_Lut_3 = 0x2b;
    dveconfig.DVE_VERT_GMV = 0x0;
    dveconfig.DVE_HORZ_GMV = 0x0;
    dveconfig.Dve_Horz_Dv_Ini = 0x0;
    dveconfig.Dve_Coft_Shift = 0x2;
    dveconfig.Dve_Corner_Th = 0x0010;
    dveconfig.Dve_Smth_Luma_Th_1 = 0x0;
    dveconfig.Dve_Smth_Luma_Th_0 = 0xC;
    dveconfig.Dve_Smth_Luma_Ada_Base = 0x0;
    dveconfig.Dve_Smth_Luma_Horz_Pnlty_Sel = 0x1;

    dveconfig.Dve_Smth_Dv_Mode = 0x1;
    dveconfig.Dve_Smth_Dv_Horz_Pnlty_Sel = 0x2;
    dveconfig.Dve_Smth_Dv_Vert_Pnlty_Sel = 0x1;
    dveconfig.Dve_Smth_Dv_Ada_Base = 0x5;
    dveconfig.Dve_Smth_Dv_Th_0 = 0xC;
    dveconfig.Dve_Smth_Dv_Th_1 = 0x0;
    dveconfig.Dve_Ord_Pnlty_Sel = 0x1;
    dveconfig.Dve_Ord_Coring = 0x4;
    dveconfig.Dve_Ord_DownSample_En = 0x0;
    dveconfig.Dve_Ord_Th = 0x100;

    dveconfig.Dve_Type_Penality_Ctrl.DVE_REFINE_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_GMV_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_PREV_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_NBR_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_RAND_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_TMPR_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_SPTL_PNLTY_SEL = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_RAND_COST = 0x20;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_GMV_COST = 0x3;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_PREV_COST = 0x2;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_NBR_COST = 0x1;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_REFINE_COST = 0x1;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_TMPR_COST = 0x3;
    dveconfig.Dve_Type_Penality_Ctrl.DVE_SPTL_COST = 0x0;

    dveconfig.Dve_Ord_As_TH = 0;
    dveconfig.Dve_Ord_As_Mask = {0x001C1C1C, 0x001C1C1C, 0x001C1C1C, 0x001C1C1C};
    dveconfig.Dve_Ord_Ref_Mask_A = {0x0, 0x007E007E, 0x007E007E, 0x0, 0x0, 0x0, 0x0};
    dveconfig.Dve_Ord_Ref_Mask_B = {0x0, 0x007E007E, 0x007E007E, 0x0, 0x0, 0x0, 0x0};
    dveconfig.Dve_Ord_Ref_Mask_C = {0x0, 0x007E007E, 0x007E007E, 0x0, 0x0, 0x0, 0x0};
    dveconfig.Dve_Ord_Ref_Mask_D = {0x0, 0x007E007E, 0x007E007E, 0x0, 0x0, 0x0, 0x0};
    
    dveconfig.Dve_Imgi_l.u4BufVA = buf_imgi_l_frame.virtAddr;
    dveconfig.Dve_Imgi_l.u4BufPA = buf_imgi_l_frame.phyAddr;
    dveconfig.Dve_Imgi_l.u4BufSize = buf_imgi_l_frame.size;
    dveconfig.Dve_Imgi_l.u4Stride = 0x110;
    dveconfig.Dve_Imgi_r.u4BufVA = buf_imgi_r_frame.virtAddr;
    dveconfig.Dve_Imgi_r.u4BufPA = buf_imgi_r_frame.phyAddr;
    dveconfig.Dve_Imgi_r.u4BufSize = buf_imgi_r_frame.size;
    dveconfig.Dve_Imgi_r.u4Stride = 0x110;

    dveconfig.Dve_Dvi_l.u4BufVA = buf_dvi_l_frame.virtAddr;
    dveconfig.Dve_Dvi_l.u4BufPA = buf_dvi_l_frame.phyAddr;
    dveconfig.Dve_Dvi_l.u4Stride = 0x220;
    dveconfig.Dve_Dvi_l.u4BufSize = buf_dvi_l_frame.size;
    dveconfig.Dve_Dvi_r.u4BufVA = buf_dvi_r_frame.virtAddr;
    dveconfig.Dve_Dvi_r.u4BufPA = buf_dvi_r_frame.phyAddr;
    dveconfig.Dve_Dvi_r.u4Stride = 0x220;
    dveconfig.Dve_Dvi_r.u4BufSize = buf_dvi_r_frame.size;

    dveconfig.Dve_Maski_l.u4BufVA = buf_maski_l_frame.virtAddr;
    dveconfig.Dve_Maski_l.u4BufPA = buf_maski_l_frame.phyAddr;
    dveconfig.Dve_Maski_l.u4BufSize = buf_maski_l_frame.size;
    dveconfig.Dve_Maski_l.u4Stride = 0x110;
    dveconfig.Dve_Maski_r.u4BufVA = buf_maski_r_frame.virtAddr;
    dveconfig.Dve_Maski_r.u4BufPA = buf_maski_r_frame.phyAddr;
    dveconfig.Dve_Maski_r.u4BufSize = buf_maski_r_frame.size;
    dveconfig.Dve_Maski_r.u4Stride = 0x110;


    dveconfig.Dve_Dvo_l.u4BufVA = buf_dvo_l_frame.virtAddr;
    dveconfig.Dve_Dvo_l.u4BufPA = buf_dvo_l_frame.phyAddr;
    dveconfig.Dve_Dvo_l.u4BufSize = buf_dvo_l_frame.size;
    dveconfig.Dve_Dvo_l.u4Stride = 0x220;
    dveconfig.Dve_Dvo_r.u4BufVA = buf_dvo_r_frame.virtAddr;
    dveconfig.Dve_Dvo_r.u4BufPA = buf_dvo_r_frame.phyAddr;
    dveconfig.Dve_Dvo_r.u4BufSize = buf_dvo_r_frame.size;
    dveconfig.Dve_Dvo_r.u4Stride = 0x220;

    dveconfig.Dve_Confo_l.u4BufVA = buf_confo_l_frame.virtAddr;
    dveconfig.Dve_Confo_l.u4BufPA = buf_confo_l_frame.phyAddr;
    dveconfig.Dve_Confo_l.u4BufSize = buf_confo_l_frame.size;
    dveconfig.Dve_Confo_l.u4Stride = 0x110;
    dveconfig.Dve_Confo_r.u4BufVA = buf_confo_r_frame.virtAddr;
    dveconfig.Dve_Confo_r.u4BufPA = buf_confo_r_frame.phyAddr;
    dveconfig.Dve_Confo_r.u4BufSize = buf_confo_r_frame.size;
    dveconfig.Dve_Confo_r.u4Stride = 0x110;

    dveconfig.Dve_Respo_l.u4BufVA = buf_respo_l_frame.virtAddr;
    dveconfig.Dve_Respo_l.u4BufPA = buf_respo_l_frame.phyAddr;
    dveconfig.Dve_Respo_l.u4BufSize = buf_respo_l_frame.size;
    dveconfig.Dve_Respo_l.u4Stride = 0x110;
    dveconfig.Dve_Respo_r.u4BufVA = buf_respo_r_frame.virtAddr;
    dveconfig.Dve_Respo_r.u4BufPA = buf_respo_r_frame.phyAddr;
    dveconfig.Dve_Respo_r.u4BufSize = buf_respo_r_frame.size;
    dveconfig.Dve_Respo_r.u4Stride = 0x110;

    #ifdef DVE_IS_SECURE
    dveconfig.Dve_Is_Secure = 1;
    #else
    dveconfig.Dve_Is_Secure = 0;
    #endif

    printf("BASEADDR_IMGI_L: 0x%lx\n", dveconfig.Dve_Imgi_l.u4BufPA);
    printf("BASEADDR_IMGI_R: 0x%lx\n", dveconfig.Dve_Imgi_r.u4BufPA);
    printf("BASEADDR_DVI_L: 0x%lx\n", dveconfig.Dve_Dvi_l.u4BufPA);
    printf("BASEADDR_DVI_R: 0x%lx\n", dveconfig.Dve_Dvi_r.u4BufPA);
    printf("BASEADDR_MASKI_L: 0x%lx\n", dveconfig.Dve_Maski_l.u4BufPA);
    printf("BASEADDR_MASKI_R: 0x%lx\n", dveconfig.Dve_Maski_r.u4BufPA);
    printf("BASEADDR_DVO_L: 0x%lx\n", dveconfig.Dve_Dvo_l.u4BufPA);
    printf("BASEADDR_DVO_R: 0x%lx\n", dveconfig.Dve_Dvo_r.u4BufPA);
    printf("BASEADDR_CONFO_L: 0x%lx\n", dveconfig.Dve_Confo_l.u4BufPA);
    printf("BASEADDR_CONFO_R: 0x%lx\n", dveconfig.Dve_Confo_r.u4BufPA);
    printf("BASEADDR_RESPO_L: 0x%lx\n", dveconfig.Dve_Respo_l.u4BufPA);
    printf("BASEADDR_RESPO_R: 0x%lx\n", dveconfig.Dve_Respo_r.u4BufPA);

    rDveParams.mDVEConfigVec.push_back(dveconfig);
    g_bDVECallback = MFALSE;

    #ifdef DVE_IS_SECURE
    mpImemDrv->initSecureM4U();
    #endif
    //enque
    ret=pStream->DVEenque(rDveParams);
    if(!ret)
    {
        printf("---ERRRRRRRRR [dpe_default..dve enque fail\n]");
    }
    else
    {
        printf("---[dpe_default..dve enque done\n]");
    }
#if DVE_ENABLE
    do{
        usleep(100000);
        if (MTRUE == g_bDVECallback)
        {
            break;
        }
    }while(1);
#endif

#ifdef DVE_IS_SECURE
#else
    MUINT32 DVE_ORG_WDITH = dveconfig.Dve_Org_Width;
    MUINT32 DVE_ORG_HEIGHT = dveconfig.Dve_Org_Height;
    MUINT32 DVE_HORZ_DS_MODE = dveconfig.Dve_Horz_Ds_Mode;
    MUINT32 DVE_VERT_DS_MODE = dveconfig.Dve_Vert_Ds_Mode;
    int int_data_dma_0, int_data_dma_1, int_data_dma_2, int_data_dma_3;
    MUINT32 blk_width;
    MUINT32 blk_height;
    MUINT32 golden_l_start_blk_x;
    int golden_l_end_blk_x;
    MUINT32 golden_l_start_blk_y;
    int golden_l_end_blk_y;

    MUINT32 golden_r_start_blk_x;
    int golden_r_end_blk_x;
    MUINT32 golden_r_start_blk_y;
    int golden_r_end_blk_y;

    int err_cnt_dma;

    if ( 0 == DVE_HORZ_DS_MODE)
    {
        blk_width = (DVE_ORG_WDITH+7) >> 3;
        golden_l_start_blk_x = (golden_l_start_x >> 3);
        if (golden_l_start_x & 0x7)
        {
            golden_l_start_blk_x += 1;
        }
        golden_l_end_blk_x  = (golden_l_end_x >> 3);
        golden_r_start_blk_x = (golden_r_start_x >> 3);
        if (golden_r_start_x & 0x7)
        {
            golden_r_start_blk_x += 1;
        }
        golden_r_end_blk_x  = (golden_r_end_x >> 3);
    }
    else
    {
        blk_width = (DVE_ORG_WDITH+3) >> 2;
        golden_l_start_blk_x = (golden_l_start_x >> 2);
        if (golden_l_start_x & 0x3)
        {
            golden_l_start_blk_x += 1;
        }
        golden_l_end_blk_x  = (golden_l_end_x >> 2);
        golden_r_start_blk_x = (golden_r_start_x >> 2);
        if (golden_r_start_x & 0x3)
        {
            golden_r_start_blk_x += 1;
        }
        golden_r_end_blk_x  = (golden_r_end_x >> 2);
    }

    if ( 0 == DVE_VERT_DS_MODE)
    {
        blk_height = (DVE_ORG_HEIGHT+7) >> 3;
        golden_l_start_blk_y = (golden_l_start_y >> 3);
        if (golden_l_start_y & 0x7)
        {
            golden_l_start_blk_y += 1;
        }
        golden_l_end_blk_y  = (golden_l_end_y >> 3);
        golden_r_start_blk_y = (golden_r_start_y >> 3);
        if (golden_r_start_y & 0x7)
        {
            golden_r_start_blk_y += 1;
        }
        golden_r_end_blk_y  = (golden_r_end_y >> 3);

    }
    else
    {
        blk_height = (DVE_ORG_HEIGHT+3) >> 2;
        golden_l_start_blk_y = (golden_l_start_y >> 2);
        if (golden_l_start_y & 0x3)
        {
            golden_l_start_blk_y += 1;
        }
        golden_l_end_blk_y  = (golden_l_end_y >> 2);
        golden_r_start_blk_y = (golden_r_start_y >> 2);
        if (golden_r_start_y & 0x3)
        {
            golden_r_start_blk_y += 1;
        }
        golden_r_end_blk_y  = (golden_r_end_y >> 2);

    }


    //Compare dpe_confo_l_frame_
#if DVE_ENABLE
    int_data_dma_0 = golden_l_start_blk_x;
    int_data_dma_1 = golden_l_start_blk_y;
    if (golden_l_end_x < 0)
    {
        int_data_dma_2 = -1;
    }
    else
    {
        int_data_dma_2 = golden_l_end_blk_x;
    }
    if (golden_l_end_y < 0)
    {
        int_data_dma_3 = -1;
    }
    else
    {
        int_data_dma_3 = golden_l_end_blk_y;
    }       
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_confo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_confo_r_frame);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_respo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_respo_r_frame);
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_dvo_l_frame); 
    mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_INVALID, &buf_dvo_r_frame);

    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_confo_l_frame, 
                            1, 
                            dveconfig.Dve_Confo_l.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Confo_l.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_l.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_l.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left confo bit true pass!!!\n");
    }
    

    //Compare the dpe_respo_l_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_respo_l_frame, 
                            1, 
                            dveconfig.Dve_Respo_l.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Respo_l.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_l.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_l.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left respo bit true pass!!!\n");
    }



    //Compare dpe_confo_r_frame_
    int_data_dma_0 = golden_r_start_blk_x;
    int_data_dma_1 = golden_r_start_blk_y;
    if (golden_r_end_x < 0)
    {
        int_data_dma_2 = -1;
    }
    else
    {
        int_data_dma_2 = golden_r_end_blk_x;
    }
    if (golden_r_end_y < 0)
    {
        int_data_dma_3 = -1;
    }
    else
    {
        int_data_dma_3 = golden_r_end_blk_y;
    }       
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_confo_r_frame, 
                            1, 
                            dveconfig.Dve_Confo_r.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Confo_r.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_r.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_r.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right confo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right confo bit true pass!!!\n");
    }


    //Compare the dpe_respo_r_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_respo_r_frame, 
                            1, 
                            dveconfig.Dve_Respo_r.u4BufVA,
                            blk_width,
                            blk_height,
                            dveconfig.Dve_Respo_r.u4Stride,
                            int_data_dma_0,
                            int_data_dma_1,
                            int_data_dma_2,
                            int_data_dma_3,
                            dveconfig.Dve_Maski_r.u4BufVA,
                            1,
                            dveconfig.Dve_Maski_r.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right respo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right respo bit true pass!!!\n");
    }


    //Compare the dpe_dvo_l_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_dvo_l_frame, 
                            0, 
                            dveconfig.Dve_Dvo_l.u4BufVA,
                            (blk_width << 1),
                            blk_height,
                            dveconfig.Dve_Dvo_l.u4Stride,
                            0,
                            0,
                            ((blk_width << 1)-1),
                            blk_height-1,
                            dveconfig.Dve_Maski_l.u4BufVA,
                            2,
                            dveconfig.Dve_Maski_l.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe left dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe left dvo bit true pass!!!\n");
    }


    //Compare the dpe_dvo_r_frame_
    err_cnt_dma = comp_roi_mem_with_file((char*)golden_dpe_dvo_r_frame, 
                            0, 
                            dveconfig.Dve_Dvo_r.u4BufVA,
                            (blk_width << 1),
                            blk_height,
                            dveconfig.Dve_Dvo_r.u4Stride,
                            0,
                            0,
                            ((blk_width << 1)-1),
                            blk_height-1,
                            dveconfig.Dve_Maski_r.u4BufVA,
                            2,
                            dveconfig.Dve_Maski_r.u4Stride
                            );  
    if (err_cnt_dma)
    {
        //Error
        printf("dpe right dvo bit true fail: errcnt (%d)!!!\n", err_cnt_dma);
    }
    else
    {
        //Pass
        printf("dpe right dvo bit true pass!!!\n");
    }


#endif

#endif

#if DVE_ENABLE

    #ifdef DVE_IS_SECURE
    mpImemDrv->freeSecureBuf(&buf_imgi_l_frame);
    mpImemDrv->freeSecureBuf(&buf_imgi_r_frame);
    mpImemDrv->freeSecureBuf(&buf_dvi_l_frame);
    mpImemDrv->freeSecureBuf(&buf_dvi_r_frame);
    mpImemDrv->freeSecureBuf(&buf_maski_l_frame);
    mpImemDrv->freeSecureBuf(&buf_maski_r_frame);

    mpImemDrv->freeSecureBuf(&buf_dvo_l_frame);
    mpImemDrv->freeSecureBuf(&buf_dvo_r_frame);
    mpImemDrv->freeSecureBuf(&buf_confo_l_frame);
    mpImemDrv->freeSecureBuf(&buf_confo_r_frame);
    mpImemDrv->freeSecureBuf(&buf_respo_l_frame);
    mpImemDrv->freeSecureBuf(&buf_respo_r_frame);
    #else
    mpImemDrv->freeVirtBuf(&buf_imgi_l_frame);
    mpImemDrv->freeVirtBuf(&buf_imgi_r_frame);
    mpImemDrv->freeVirtBuf(&buf_dvi_l_frame);
    mpImemDrv->freeVirtBuf(&buf_dvi_r_frame);
    mpImemDrv->freeVirtBuf(&buf_maski_l_frame);
    mpImemDrv->freeVirtBuf(&buf_maski_r_frame);

    mpImemDrv->freeVirtBuf(&buf_dvo_l_frame);
    mpImemDrv->freeVirtBuf(&buf_dvo_r_frame);
    mpImemDrv->freeVirtBuf(&buf_confo_l_frame);
    mpImemDrv->freeVirtBuf(&buf_confo_r_frame);
    mpImemDrv->freeVirtBuf(&buf_respo_l_frame);
    mpImemDrv->freeVirtBuf(&buf_respo_r_frame);
    #endif
#endif

    pStream->uninit();
    printf("--- [DpeStream uninit done\n");

    pStream->destroyInstance("test_dpe_default");
    mpImemDrv->uninit();
    printf("--- [Imem uninit done\n");

    return ret;
}

