/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.

 
#define LOG_TAG "fdvtstream_test"

#include <vector>

#include <sys/time.h>
#include <sys/prctl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <semaphore.h>
#include <pthread.h>
#include <utils/threads.h>
#include <mtkcam/drv/iopipe/PostProc/IEgnStream.h>
//#include <IEgnStream.h>

//#include <mtkcam/iopipe/PostProc/IFeatureStream.h>

#include <imem_drv.h>
#include <time.h>

//#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
//#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>

#include <fdvtcommon.h>
//

#include <sys/types.h>
#include <sys/stat.h>

#include "../hw/isp_60/fdvt_drv.h"
//#include <fdvt_drv.h>
#include "pic/fdvt_in_frame01_400x300.h"
#include "pic/fdvt_in2_frame01_400x300.h"
#include "pic/fdvt_in_frame01_320x240.h"
#include "pic/fdvt_in2_frame01_320x240.h"
#include "pic/fdvt_in_frame01_640x480.h"
#include "pic/fdvt_in2_frame01_640x480.h"
#include "pic/attr_in_frame01_128x128.h"
#include "pic/pose_in_frame01_128x128.h"
#include "pic/pose_in2_frame01_128x128.h"
//#include "pic/fdvt_fdvt_in1_frame01.h"
//#include "pic/fdvt_fdvt_in2_frame01.h"

#define ABS(a)		((a < 0) ? -a : a )
#define IMAGE_SRC_FROM_PHONE 0
#define IMAGE_ONE_PLANE      0
DipIMemDrv *ImemDrv = NULL;
IMEM_BUF_INFO Imem_Buffer_Y;
IMEM_BUF_INFO Imem_Buffer_UV;
IMEM_BUF_INFO Imem_Buffer_Info;
MUINT8	*Imem_pLogVir_Y=NULL, *Imem_pLogVir_UV=NULL;
MINT32	 Imem_MemID_Y, Imem_MemID_UV;
MUINT32  Imem_Size_Y, Imem_Size_UV;
MUINT32  Imem_Alloc_Num = 0;

using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSEgn;

MBOOL g_bFDVTCallback;

MVOID FDVTCallback(EGNParams<FDVTConfig>& rParams);

pthread_t       FdvtUserThread;
sem_t           FdvtSem;
volatile bool            g_bFdvtThreadTerminated = 0;

MINT32 Imem_alloc(MUINT32 size,MINT32 *memId,MUINTPTR *vAddr,MUINTPTR *pAddr)
{
    if ( NULL == ImemDrv ) {
        ImemDrv = DipIMemDrv::createInstance();
        ImemDrv->init();
    }
    //
    Imem_Buffer_Info.size = size;
    //Imem_Buffer_Info.useNoncache = 1;
    if (ImemDrv->allocVirtBuf(&Imem_Buffer_Info) != 0)
        printf("Imem Allocate Virtual Buffer Fail!\n");

    *memId = Imem_Buffer_Info.memID;
    *vAddr = (MUINTPTR)Imem_Buffer_Info.virtAddr;
    //
    if (ImemDrv->mapPhyAddr(&Imem_Buffer_Info) != 0)
        printf("Imem Map Physical Address Fail!\n");

    *pAddr = (MUINTPTR)Imem_Buffer_Info.phyAddr;
    Imem_Alloc_Num ++;

    printf("Imem_Alloc_Num(%d)\n",Imem_Alloc_Num);
    printf("vAddr(0x%lx) pAddr(0x%lx) Imem_Alloc_Num(%d)\n",(unsigned long)*vAddr,(unsigned long)*pAddr,Imem_Alloc_Num);

    return 0;
}


MINT32 Imem_free(MUINT8 *vAddr, MUINTPTR phyAddr,MUINT32 size,MINT32 memId)
{
    Imem_Buffer_Info.size = size;
    Imem_Buffer_Info.memID = memId;
    Imem_Buffer_Info.virtAddr = (MUINTPTR)vAddr;
    Imem_Buffer_Info.phyAddr = (MUINTPTR)phyAddr;
    //

    if(ImemDrv)
    {
        if (ImemDrv->unmapPhyAddr(&Imem_Buffer_Info) != 0)
            printf("Imem Unmap Physical Address Fail!\n");
        //
        if (ImemDrv->freeVirtBuf(&Imem_Buffer_Info) != 0)
            printf("Imem Free Virtual Buffer Fail!\n");

        if(Imem_Alloc_Num)
        {
            Imem_Alloc_Num--;
            if(Imem_Alloc_Num==0)
            {
                ImemDrv->uninit();
                ImemDrv = NULL;
            }
        }
    }
    else
    {
        printf("Warning! unmapPhyAddr Fail!");
    }

    printf("Imem_Alloc_Num(%d)\n",Imem_Alloc_Num);
    
    return 0;
}

void Allocate_TestPattern_Buffer(int subcase_index, int para1)
{
	if (subcase_index == 0)
	{
	    if (para1 == 0)
	    {
            #if IMAGE_SRC_FROM_PHONE
            Imem_Size_Y = 640*480*2;
            #else
            Imem_Size_Y = sizeof(fdvt_in_frame01_640x480); // 16-byte alignment
            #endif
            Imem_Size_UV = sizeof(fdvt_in2_frame01_640x480); // 16-byte alignment
            //Imem_Buffer_Y.useNoncache = 1;
            //Imem_Buffer_UV.useNoncache = 1;
	    }
	    else if (para1 == 1)
	    {
            #if IMAGE_SRC_FROM_PHONE
            Imem_Size_Y = 400*300*2;
            #else
            Imem_Size_Y = sizeof(fdvt_in_frame01_400x300); // 16-byte alignment
            #endif
            Imem_Size_UV = sizeof(fdvt_in2_frame01_400x300); // 16-byte alignment
            //Imem_Buffer_Y.useNoncache = 1;
            //Imem_Buffer_UV.useNoncache = 1;
	    }

	    Imem_alloc(Imem_Size_Y, &Imem_MemID_Y, &Imem_Buffer_Y.virtAddr, &Imem_Buffer_Y.phyAddr);
	    Imem_alloc(Imem_Size_UV, &Imem_MemID_UV, &Imem_Buffer_UV.virtAddr, &Imem_Buffer_UV.phyAddr);
	    //Imem_Buffer_Y.virtAddr=(MUINTPTR)Imem_pLogVir_Y;
	    //Imem_Buffer_UV.virtAddr=(MUINTPTR)Imem_pLogVir_UV;
	    Imem_Buffer_Y.memID=Imem_MemID_Y;
	    Imem_Buffer_UV.memID=Imem_MemID_UV;
	    Imem_Buffer_Y.size=Imem_Size_Y;
	    Imem_Buffer_UV.size=Imem_Size_UV;

	    #if IMAGE_SRC_FROM_PHONE
	    {
            FILE* fd1;
            fd1 = fopen("/sdcard/test.raw", "r+b");
            if(para1 == 0)
                size_t readSize = fread((void *)Imem_Buffer_Y.virtAddr, 1, 640*480*2, fd1);
            else if(para1 == 1)
                size_t readSize = fread((void *)Imem_Buffer_Y.virtAddr, 1, 400*300*2, fd1);				
            if (readSize == 0)
            {
                printf("Read src image fail\n");
            }
            fclose(fd1);
	    }
	    #else
	    if(para1 == 0)
            memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(fdvt_in_frame01_640x480), Imem_Size_Y);
	    else if(para1 == 1)
            memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(fdvt_in_frame01_400x300), Imem_Size_Y);
	    #endif

	    if(para1 == 0)
            memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(fdvt_in2_frame01_640x480), Imem_Size_UV);
	    else if(para1 == 1)
            memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(fdvt_in2_frame01_400x300), Imem_Size_UV);
	}
	else if (subcase_index == 1)
	{
	    if (para1 == 0)
	    {
            #if IMAGE_SRC_FROM_PHONE
            Imem_Size_Y = 128*128*2;
            #else
            Imem_Size_Y = sizeof(attr_in_frame01_128x128); // 16-byte alignment
            #endif	    
            Imem_Size_UV = sizeof(fdvt_in2_frame01_640x480); // dummy
            //Imem_Buffer_Y.useNoncache = 1;
            //Imem_Buffer_UV.useNoncache = 1;
	    }
	    else if (para1 == 1)
	    {
            Imem_Size_Y = sizeof(fdvt_in_frame01_400x300); // 16-byte alignment
            Imem_Size_UV = sizeof(fdvt_in2_frame01_400x300); // 16-byte alignment
            //Imem_Buffer_Y.useNoncache = 1;
            //Imem_Buffer_UV.useNoncache = 1;
	    }		

	    Imem_alloc(Imem_Size_Y, &Imem_MemID_Y, &Imem_Buffer_Y.virtAddr, &Imem_Buffer_Y.phyAddr);
	    Imem_alloc(Imem_Size_UV, &Imem_MemID_UV, &Imem_Buffer_UV.virtAddr, &Imem_Buffer_UV.phyAddr);
	    //Imem_Buffer_Y.virtAddr=(MUINTPTR)Imem_pLogVir_Y;
	    //Imem_Buffer_UV.virtAddr=(MUINTPTR)Imem_pLogVir_UV;
	    Imem_Buffer_Y.memID=Imem_MemID_Y;
	    Imem_Buffer_UV.memID=Imem_MemID_UV;
	    Imem_Buffer_Y.size=Imem_Size_Y;
	    Imem_Buffer_UV.size=Imem_Size_UV;

	    #if IMAGE_SRC_FROM_PHONE
	    {
            FILE* fd1;
            fd1 = fopen("/sdcard/test.raw", "r+b");
            if(para1 == 0)
                size_t readSize = fread((void *)Imem_Buffer_Y.virtAddr, 1, 128*128*2, fd1);
            else if(para1 == 1)
                size_t readSize = fread((void *)Imem_Buffer_Y.virtAddr, 1, 400*300*2, fd1);				
            if (readSize == 0)
            {
                printf("Read src image fail\n");
            }
            fclose(fd1);
	    }
	    #else
	    if(para1 == 0)
            memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(attr_in_frame01_128x128), Imem_Size_Y);
	    else if(para1 == 1)
            memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(fdvt_in_frame01_400x300), Imem_Size_Y);
	    #endif

	    if(para1 == 0)
            memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(fdvt_in2_frame01_640x480), Imem_Size_UV); // dummy
	    else if(para1 == 1)
            memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(fdvt_in2_frame01_400x300), Imem_Size_UV);
	}
	else if (subcase_index == 2)
	{
	    if (para1 == 0)
	    {
            #if IMAGE_SRC_FROM_PHONE
            Imem_Size_Y = 128*128*2;
            #else
            Imem_Size_Y = sizeof(pose_in_frame01_128x128); // 16-byte alignment
            #endif	    
            Imem_Size_UV = sizeof(pose_in2_frame01_128x128); // 16-byte alignment
            //Imem_Buffer_Y.useNoncache = 1;
            //Imem_Buffer_UV.useNoncache = 1;
	    }
	    else if (para1 == 1)
	    {
            Imem_Size_Y = sizeof(fdvt_in_frame01_400x300); // 16-byte alignment
            Imem_Size_UV = sizeof(fdvt_in2_frame01_400x300); // 16-byte alignment
            //Imem_Buffer_Y.useNoncache = 1;
            //Imem_Buffer_UV.useNoncache = 1;
	    }		

	    Imem_alloc(Imem_Size_Y, &Imem_MemID_Y, &Imem_Buffer_Y.virtAddr, &Imem_Buffer_Y.phyAddr);
	    Imem_alloc(Imem_Size_UV, &Imem_MemID_UV, &Imem_Buffer_UV.virtAddr, &Imem_Buffer_UV.phyAddr);
	    //Imem_Buffer_Y.virtAddr=(MUINTPTR)Imem_pLogVir_Y;
	    //Imem_Buffer_UV.virtAddr=(MUINTPTR)Imem_pLogVir_UV;
	    Imem_Buffer_Y.memID=Imem_MemID_Y;
	    Imem_Buffer_UV.memID=Imem_MemID_UV;
	    Imem_Buffer_Y.size=Imem_Size_Y;
	    Imem_Buffer_UV.size=Imem_Size_UV;

	    #if IMAGE_SRC_FROM_PHONE
	    {
            FILE* fd1;
            fd1 = fopen("/sdcard/test.raw", "r+b");
            if(para1 == 0)
                size_t readSize = fread((void *)Imem_Buffer_Y.virtAddr, 1, 128*128*2, fd1);
            else if(para1 == 1)
                size_t readSize = fread((void *)Imem_Buffer_Y.virtAddr, 1, 400*300*2, fd1);				
            if (readSize == 0)
            {
                printf("Read src image fail\n");
            }
            fclose(fd1);
	    }
	    #else
	    if(para1 == 0)
            memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(pose_in_frame01_128x128), Imem_Size_Y);
	    else if(para1 == 1)
            memcpy( (MUINT8*)(Imem_Buffer_Y.virtAddr), (MUINT8*)(fdvt_in_frame01_400x300), Imem_Size_Y);
	    #endif

	    if(para1 == 0)
            memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(pose_in2_frame01_128x128), Imem_Size_UV); // dummy
	    else if(para1 == 1)
            memcpy( (MUINT8*)(Imem_Buffer_UV.virtAddr), (MUINT8*)(fdvt_in2_frame01_400x300), Imem_Size_UV);
	}
    printf("Imem_Buffer_Y PA/VA: 0x%lx/0x%lx\n", (unsigned long)Imem_Buffer_Y.phyAddr, (unsigned long)Imem_Buffer_Y.virtAddr); 
}

void Free_TestPattern_Buffer()
{
    Imem_free((MUINT8 *)Imem_Buffer_Y.virtAddr, Imem_Buffer_Y.phyAddr, Imem_Buffer_Y.size, Imem_Buffer_Y.memID);
    Imem_free((MUINT8 *)Imem_Buffer_UV.virtAddr, Imem_Buffer_UV.phyAddr, Imem_Buffer_UV.size, Imem_Buffer_UV.memID);
}

static bool saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

static bool appendBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_APPEND , S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

int test_fdvtStream(int subcase_index,int para1)
{
#if 0
    printf("getchar()");
    getchar();
#endif
    	clock_t start_tick, end_tick;
    	double elapsed;
    	EGNInitInfo InitInfo;
    	InitInfo.MAX_SRC_IMG_WIDTH = 640;
    	InitInfo.MAX_SRC_IMG_HEIGHT = 640;
		
    	NSCam::NSIoPipe::NSEgn::IEgnStream<FDVTConfig>* pStream;
    	pStream= NSCam::NSIoPipe::NSEgn::IEgnStream<FDVTConfig>::createInstance("FDVT unit test");
    	pStream->init();
    	EGNParams<FDVTConfig> rFdvtParams;
    	rFdvtParams.mpEngineID = eFDVT;
    	rFdvtParams.mpfnCallback = FDVTCallback;

    	NSCam::NSIoPipe::FDVTConfig fdvtconfig;

    	printf("=====> start of test_fd (subcase_index: %d  para1: %d)\n", subcase_index, para1);
  
    	int ret = 0;
    	if(subcase_index == 0) // FDMODE
    	{ 
		    fdvtconfig.FD_MODE = FDMODE;
		    if (para1 == 0)
		    {
                fdvtconfig.SRC_IMG_WIDTH = 640;
                fdvtconfig.SRC_IMG_HEIGHT = 480;
		    }
		    else if (para1 == 1)
		    {
                fdvtconfig.SRC_IMG_WIDTH = 400;
                fdvtconfig.SRC_IMG_HEIGHT = 300;
		    }
		    #if IMAGE_SRC_FROM_PHONE
		    fdvtconfig.SRC_IMG_FMT = FMT_YUYV;
		    #else
		    fdvtconfig.SRC_IMG_FMT = FMT_YUV_2P;
		    #endif
		    Allocate_TestPattern_Buffer(subcase_index, para1);
		    fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
		    fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
		    printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);
		    printf("--- [test_fdvt_default...start to push_back\n");
		    rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);
    	}
    	else if(subcase_index == 1) // ATTRIBUTEMODE
    	{
		    fdvtconfig.FD_MODE = ATTRIBUTEMODE;
		    if (para1 == 0)
		    {
                fdvtconfig.SRC_IMG_WIDTH = 128;
                fdvtconfig.SRC_IMG_HEIGHT = 128;
                fdvtconfig.SRC_IMG_FMT = FMT_UYVY;
		    }
		    else if (para1 == 1)
		    {
                fdvtconfig.SRC_IMG_WIDTH = 400;
                fdvtconfig.SRC_IMG_HEIGHT = 300;
                fdvtconfig.SRC_IMG_FMT = FMT_YUV_2P;
		    }
		    Allocate_TestPattern_Buffer(subcase_index, para1);
		    fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
		    fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
		    printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);
		    printf("--- [test_fdvt_default...start to push_back\n");
		    rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);
    	}
    	else if(subcase_index == 2) // POSEMODE
    	{
		    fdvtconfig.FD_MODE = POSEMODE;
		    if (para1 == 0)
		    {
                fdvtconfig.SRC_IMG_WIDTH = 128;
                fdvtconfig.SRC_IMG_HEIGHT = 128;
                fdvtconfig.SRC_IMG_FMT = FMT_YUV_2P;
		    }
		    else if (para1 == 1)
		    {
                fdvtconfig.SRC_IMG_WIDTH = 400;
                fdvtconfig.SRC_IMG_HEIGHT = 300;
                fdvtconfig.SRC_IMG_FMT = FMT_YUV_2P;
		    }
		    Allocate_TestPattern_Buffer(subcase_index, para1);
		    fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
		    fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
		    printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);
		    printf("--- [test_fdvt_default...start to push_back\n");
		    rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);
    	}		
    	else if(subcase_index == 3) // ATTRIBUTEMODE + FDMODE
    	{
		    if (para1 == 0)
		    {
			    fdvtconfig.FD_MODE = ATTRIBUTEMODE;
			    fdvtconfig.SRC_IMG_WIDTH = 128;
			    fdvtconfig.SRC_IMG_HEIGHT = 128;
    			fdvtconfig.SRC_IMG_FMT = FMT_UYVY;
    			Allocate_TestPattern_Buffer(1, 0);
    			fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
    			fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
    			printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);
    			printf("--- [test_fdvt_default...start to push_back\n");
    			rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);

    			fdvtconfig.FD_MODE = FDMODE;
    			fdvtconfig.SRC_IMG_WIDTH = 640;
    			fdvtconfig.SRC_IMG_HEIGHT = 480;	
    			fdvtconfig.SRC_IMG_FMT = FMT_YUV_2P;
    			Allocate_TestPattern_Buffer(0, 0);
    			fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
    			fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
    			printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);
    			printf("--- [test_fdvt_default...start to push_back\n");
    			rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);			
		    }

		    else if (para1 == 1)
		    {
			    fdvtconfig.FD_MODE = ATTRIBUTEMODE;
			    fdvtconfig.SRC_IMG_WIDTH = 128;
			    fdvtconfig.SRC_IMG_HEIGHT = 128;
    			fdvtconfig.SRC_IMG_FMT = FMT_UYVY;
    			Allocate_TestPattern_Buffer(1, 0);
    			fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
    			fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
    			printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);			
    			printf("--- [test_fdvt_default...start to push_back\n");
    			rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);

    			fdvtconfig.FD_MODE = ATTRIBUTEMODE;
    			fdvtconfig.SRC_IMG_WIDTH = 400;
    			fdvtconfig.SRC_IMG_HEIGHT = 300;	
    			fdvtconfig.SRC_IMG_FMT = FMT_YUV_2P;
    			Allocate_TestPattern_Buffer(1, 1);
    			fdvtconfig.source_img_address = (MUINT64*)Imem_Buffer_Y.phyAddr;
    			fdvtconfig.source_img_address_UV = (MUINT64*)Imem_Buffer_UV.phyAddr;
    			printf("source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)fdvtconfig.source_img_address, (unsigned long)fdvtconfig.source_img_address_UV);
    			printf("--- [test_fdvt_default...start to push_back\n");
    			rFdvtParams.mEGNConfigVec.push_back(fdvtconfig);			
		    }			
    	}

    	printf("=====> Initial Driver Start\n");

    	//printf("Version: %d\n", FDVT_GetModelVersion());

    	g_bFDVTCallback = MFALSE;

//////////////////////////// First Time Enque/Deque //////////////////////////////////
    	//start_tick = clock();

    	printf("=====> FDVT Enque Start\n");

    	ret=pStream->EGNenque(rFdvtParams);
    	if(!ret)
    	{
        	printf("---FDVT Enque ERRRRRRRRR [.fdvt enque fail\n]");
    	}
    	else
    	{
        	printf("---FDVT Enque [.fdvt enque done\n]");
    	}

    	int q = 0;
    	do{
        	usleep(100000);
        	printf("---waiting for callback\n]");
        	q++;
        	if (MTRUE == g_bFDVTCallback)
        	{
            	break;
        	}
    	}while(q<50);

    	//end_tick = clock();
    	//elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
    	//printf("FD Enque Elapsed Time: %f\n", elapsed);

    	//printf("Push enter to deque FD\n");
    	//getchar();

    	//start_tick = clock();

    	//printf("=====> FDVT Deque Start\n");
    	//FDVT_Deque(FdDrv_output);
    	//getchar();

    	//end_tick = clock();
    	//elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
    	//printf("FD Deque Elapsed Time: %f\n", elapsed);
//////////////////////////////////////////////////////////////


/*
///////////////////////////// Second Time Enque/Deque /////////////////////////////////
    	start_tick = clock();

    	printf("=====> FDVT Enque Start\n");
    	FDVT_Enque(FdDrv_input);

    	end_tick = clock();
    	elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
    	printf("FD Enque Elapsed Time: %f\n", elapsed);

    	//printf("Push enter to deque FD\n");
    	//getchar();

    	start_tick = clock();

    	printf("=====> FDVT Deque Start\n");
    	FDVT_Deque(FdDrv_output);
    	//getchar();

    	end_tick = clock();
    	elapsed = (double) (end_tick - start_tick) / CLOCKS_PER_SEC;
    	printf("FD Deque Elapsed Time: %f\n", elapsed);
//////////////////////////////////////////////////////////////
*/

    	//printf("=====> Uninitial Driver Start\n");
    	//FDVT_CloseDriverWithUserCount();

    	NSCam::NSIoPipe::FDVTConfig* pFdvtConfig;
    	pFdvtConfig = &(rFdvtParams.mEGNConfigVec.at(0));

    	printf("Detected Face Number: %d\n", pFdvtConfig->FDOUTPUT.FD_TOTAL_NUM);

    	printf("=====> Free TestPattern Buffer Start\n");
    	Free_TestPattern_Buffer();

    	printf("=====> Uninitial Driver Start\n");
    	pStream->uninit();

    	printf("=====> end of test_fd\n");
    	return ret;
}

MVOID FDVTCallback(EGNParams<FDVTConfig>& rParams)
{
    (void) rParams;
    int i = 0, j = 0 ;
    int size = rParams.mEGNConfigVec.size();
    for (; i < size; i++){
        MUINT32 reg1,reg2;
        //reg1 = rParams.mEGNConfigVec.at(i).feedback.reg1;
        //reg2 = rParams.mEGNConfigVec.at(i).feedback.reg2;
        printf("[FDVTCallback]FD_MODE: %d\n", rParams.mEGNConfigVec.at(i).FD_MODE);
        printf("[FDVTCallback]source_img_address: 0x%lx  source_img_address_UV: 0x%lx\n", (unsigned long)rParams.mEGNConfigVec.at(i).source_img_address, (unsigned long)rParams.mEGNConfigVec.at(i).source_img_address_UV);

        if(rParams.mEGNConfigVec.at(i).FD_MODE == 0)
        {
        	printf("[FDVTCallback]Detected Face Number: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.FD_TOTAL_NUM);
        	printf("[FDVTCallback]PYRAMID0_RESULT: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.PYRAMID0_RESULT.fd_partial_result);
        	printf("[FDVTCallback]PYRAMID0_anchor_score: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.PYRAMID0_RESULT.anchor_score[0]);
        	printf("[FDVTCallback]PYRAMID1_RESULT: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.PYRAMID1_RESULT.fd_partial_result);
        	printf("[FDVTCallback]PYRAMID1_anchor_score: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.PYRAMID1_RESULT.anchor_score[0]);		
        	printf("[FDVTCallback]PYRAMID2_RESULT: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.PYRAMID2_RESULT.fd_partial_result);
        	printf("[FDVTCallback]PYRAMID2_anchor_score: %d\n", rParams.mEGNConfigVec.at(i).FDOUTPUT.PYRAMID2_RESULT.anchor_score[0]);
        }
        else if(rParams.mEGNConfigVec.at(i).FD_MODE == 1)
        {
        	printf("[FDVTCallback]RACE_RESULT00: %d\n", rParams.mEGNConfigVec.at(i).ATTRIBUTEOUTPUT.RACE_RESULT.RESULT[0][0]);
        	printf("[FDVTCallback]GENDER_RESULT00: %d\n", rParams.mEGNConfigVec.at(i).ATTRIBUTEOUTPUT.GENDER_RESULT.RESULT[0][0]);
        }
        else if(rParams.mEGNConfigVec.at(i).FD_MODE == 2)
        {
        	printf("[FDVTCallback]RIP_RESULT00: %d\n", rParams.mEGNConfigVec.at(i).POSEOUTPUT.RIP_RESULT.RESULT[0][0]);
        	printf("[FDVTCallback]ROP_RESULT00: %d\n", rParams.mEGNConfigVec.at(i).POSEOUTPUT.ROP_RESULT.RESULT[0][0]);
        }		
        //printf("Feedback statistics are:(0x%x,0x%x)\n", reg1,reg2);	
	}

	g_bFDVTCallback = MTRUE;
}

