#include "CTP_type.h"
#include "CTP_shell.h"
#include "frame_3.h"
char* frame_3_in_dpe_wmf_dpi_frame_0 = &frame_3_dpe_wmf_dpi_frame_00_00_0[0];

char* frame_3_in_dpe_wmf_dpi_frame_1 = &frame_3_dpe_wmf_dpi_frame_00_00_1[0];

char* frame_3_in_dpe_wmf_dpi_frame_2 = &frame_3_dpe_wmf_dpi_frame_00_00_2[0];

unsigned int frame_3_golden_dpe_wmf_dpo_0_size = frame_3_dpe_wmf_dpo_frame_00_00_0_size;
char* frame_3_golden_dpe_wmf_dpo_frame_0 = &frame_3_dpe_wmf_dpo_frame_00_00_0[0];

unsigned int frame_3_golden_dpe_wmf_dpo_1_size = frame_3_dpe_wmf_dpo_frame_00_00_1_size;
char* frame_3_golden_dpe_wmf_dpo_frame_1 = &frame_3_dpe_wmf_dpo_frame_00_00_1[0];

unsigned int frame_3_golden_dpe_wmf_dpo_2_size = frame_3_dpe_wmf_dpo_frame_00_00_2_size;
char* frame_3_golden_dpe_wmf_dpo_frame_2 = &frame_3_dpe_wmf_dpo_frame_00_00_2[0];

char* frame_3_in_dpe_wmf_imgi_frame_0 = &frame_3_dpe_wmf_imgi_frame_00_00_0[0];

char* frame_3_in_dpe_wmf_imgi_frame_1 = &frame_3_dpe_wmf_imgi_frame_00_00_1[0];

char* frame_3_in_dpe_wmf_imgi_frame_2 = &frame_3_dpe_wmf_imgi_frame_00_00_2[0];

char* frame_3_in_dpe_wmf_maski_frame_0 = &frame_3_dpe_wmf_maski_frame_00_00_0[0];

char* frame_3_in_dpe_wmf_maski_frame_1 = &frame_3_dpe_wmf_maski_frame_00_00_1[0];

char* frame_3_in_dpe_wmf_tbli_frame_0 = &frame_3_dpe_wmf_tbli_frame_00_00_0[0];

char* frame_3_in_dpe_wmf_tbli_frame_1 = &frame_3_dpe_wmf_tbli_frame_00_00_1[0];

char* frame_3_in_dpe_wmf_tbli_frame_2 = &frame_3_dpe_wmf_tbli_frame_00_00_2[0];


void getframe_3GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2
)
{
*golden_dpe_wmf_dpo_frame_0 = (unsigned long)&frame_3_dpe_wmf_dpo_frame_00_00_0[0];
*golden_dpe_wmf_dpo_frame_1 = (unsigned long)&frame_3_dpe_wmf_dpo_frame_00_00_1[0];
*golden_dpe_wmf_dpo_frame_2 = (unsigned long)&frame_3_dpe_wmf_dpo_frame_00_00_2[0];
}
