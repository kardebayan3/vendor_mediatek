#ifdef NDEBUG // enable assertion
#undef NDEBUG
#endif

#define DEBUG_LOG_TAG "NirPath"

#include "NirPath.h"

#ifndef LEGACY_PATH // non-legacy path
#include <mtkcam/drv/iopipe/PortMap.h>
#include <mtkcam/drv/iopipe/CamIO/INormalPipe.h>
#include <mtkcam/drv/iopipe/CamIO/IHalCamIO.h>

#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>
#else // legacy path
#include <core/iopipe/CamIO/PortMap.h>
#include <mtkcam/iopipe/CamIO/INormalPipe.h>
#include <mtkcam/iopipe/CamIO/IHalCamIO.h>
#include <hal/aaa/IEventIrq.h>
#include <isp_tuning/isp_tuning.h>

#include <mtkcam/utils/ImageBufferHeap.h>
#include <mtkcam/utils/imagebuf/IIonImageBufferHeap.h>
#include <mtkcam/utils/imagebuf/ISecureImageBufferHeap.h>
#include <mtkcam/utils/imagebuf/IDummyImageBufferHeap.h>
#include <mtkcam/hwutils/HwInfoHelper.h>
#include <v3/hwnode/HwEventIrq.h>
#endif // legacy path

#include <sys/prctl.h>
#include <unistd.h>

#include <mtkcam/utils/std/TypeTraits.h>
#include <mtkcam/utils/std/Sync.h>

#include <mtkcam/main/security/utils/Debug.h>

#include <chrono>

using namespace NSCam;
using namespace NSCam::NSIoPipe;
using namespace NSCam::NSIoPipe::NSCamIOPipe;

using namespace NSCam::Utils::Sync;
using namespace NSCam::security;

#ifdef LEGACY_PATH // legacy path
using namespace NS3Av3;
using namespace NSCam::v3;
#endif

// ---------------------------------------------------------------------------

static constexpr std::chrono::duration<int64_t> kDequeueBufferTimeout =
    std::chrono::seconds(1);
static constexpr uint32_t kSignalTimeoutInNanoseconds = (100 * 1000);
static constexpr uint32_t kDequeTimeoutInMilliseconds = 3000;
static constexpr int32_t kBufferIndex = 0xFFFF;
static constexpr int kBufferUsage =
    (eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

// FIXME: need to align with resized raw hardware limitation
static const MSize kResizedRAWSize(480, 270);

// FIXME: should be defined somewhere
static bool kSecurePath = false;

// ---------------------------------------------------------------------------

static auto* createDefaultNormalPipe(
        MUINT32 sensorIndex, __unused char const* szCallerName)
{
#ifndef LEGACY_PATH // non-legacy path
    INormalPipe* normalPipe = nullptr;

    auto pModule = INormalPipeModule::get();
    if (!pModule)
    {
        IRIS_LOGE("getNormalPipeModule() failed");
        return normalPipe;
    }

    //  Select CamIO version
    size_t count = 0;
    MUINT32 const* version = nullptr;
    int err = pModule->get_sub_module_api_version(&version, &count, sensorIndex);
    if (err < 0 || ! count || ! version) {
        IRIS_LOGE("[%d] INormalPipeModule::get_sub_module_api_version " \
                "- err:%#x count:%zu version:%p",
            sensorIndex, err, count, version);
        return normalPipe;
    }

    MUINT32 const selected_version = *(version + count - 1); //Select max. version
    IRIS_LOGD("[%d] count:%zu Selected CamIO Version:%0#x",
            sensorIndex, count, selected_version);

    pModule->createSubModule(sensorIndex, szCallerName, selected_version,
            reinterpret_cast<void**>(&normalPipe));
    return normalPipe;
#else
    return INormalPipe_FrmB::createInstance(sensorIndex, DEBUG_LOG_TAG, 1);
#endif
}

// FIXME: currently The dropped frame does not be recycled by IrisCamera
//        and cause buffer leakage. We need an error handling flow.
static void NSCam::security::onFrameDrop(uint32_t frameNumber, void* cookie)
{
    IRIS_LOGW("drop frame(%u)", frameNumber);
    if (!cookie)
    {
        IRIS_LOGE("invalid cookie");
        return;
    }

    reinterpret_cast<NirPath*>(cookie)->handleFrameDrop(frameNumber);
}

// TODO: add this helper into HwInfoHelper
static constexpr unsigned int getRAWBitDepth(int rawSensorBit)
{
    switch (rawSensorBit)
    {
        case RAW_SENSOR_8BIT:
            return 8;
        case RAW_SENSOR_10BIT:
            return 10;
        case RAW_SENSOR_12BIT:
            return 12;
        case RAW_SENSOR_14BIT:
            return 14;
        default:
            IRIS_LOGE("unknown raw sensor bit(0x%x), return 10 bit", rawSensorBit);
            return 10;
    }
}

static PortID getPortID(const StreamID& streamID)
{
    switch (streamID)
    {
        case StreamID::FULL_RAW:
            return PORT_IMGO;
        case StreamID::RESIZED_RAW:
            return PORT_RRZO;
        default:
            IRIS_LOGW("unknown stream(%d), return full raw",
                    toLiteral(streamID));
            return PORT_IMGO;
    }
}

// ---------------------------------------------------------------------------

static void NSCam::security::enqueLoop(NirPath& camera)
{
    AutoLog();

    bool stop = false;

    // set thread name
    prctl(PR_SET_NAME, __FUNCTION__, 0, 0, 0);

    while (!stop)
    {
        IRIS_LOGD("[%s] wait event", __FUNCTION__);
        sem_wait(&camera.mSemEnqueLoop);

        State eState = camera.getState();
        IRIS_LOGD("[%s] receive event: state(0x%04x)", __FUNCTION__, eState);

        switch (eState)
        {
            case State::PREVIEW:
                camera.StreamingEnque();
                IRIS_LOGD("preview stop in Enque!!");
                stop = true;
                sem_post(&camera.mSemEnqueLoopDone);
                break;
            case State::PREVIEW_STOP:
                sem_post(&camera.mSemEnqueLoopDone);
                break;
            case State::UNINIT:
                IRIS_LOGD("uninit thread");
                break;
            case State::EXIT:
                stop = true;
                break;
            default:
                IRIS_LOGE("Unknown state(0x%04x)", eState);
        }

        if (stop == true)
        {
            IRIS_LOGD("exit thread %s", __FUNCTION__);
            break;
        }
    }
}

static void NSCam::security::mainLoop(NirPath& camera)
{
    AutoLog();

    bool stop = false;

    // set thread name
    prctl(PR_SET_NAME, __FUNCTION__, 0, 0, 0);

    while (!stop)
    {
        IRIS_LOGD("[%s] wait event", __FUNCTION__);
        sem_wait(&camera.mSemMainLoop);

        State eState = camera.getState();
        IRIS_LOGD("[%s] receive event: state(0x%04x)", __FUNCTION__, eState);

        switch (eState)
        {
        case State::PREVIEW:
            camera.StreamingProc();
            IRIS_LOGD("preview stop!");
            stop = true;
            sem_post(&camera.mSemMainLoopDone);
            break;
        case State::PREVIEW_STOP:
            sem_post(&camera.mSemMainLoopDone);
            break;
        case State::UNINIT:
            IRIS_LOGD("uninit thread");
            break;
        case State::EXIT:
            stop = true;
            break;
        default:
            IRIS_LOGE("Unknown state(0x%04x)", eState);
        }

        if (stop == true)
        {
            IRIS_LOGD("exit thread %s", __FUNCTION__);
            break;
        }
    }
}

// ---------------------------------------------------------------------------

NirPath::NirPath(int maxBuffers)
      : mTimeline(ITimeline::create(DEBUG_LOG_TAG)),
      mTimelineCounter(0),
      mNormalPipe(nullptr),
      mEnqCount(1),
      mBufNum(maxBuffers),
      mSensorScenario(SENSOR_SCENARIO_ID_NORMAL_PREVIEW),
      mDevID(SENSOR_DEV_NONE)
{
    mState.first = State::NONE;
    mSensorArray.first = UINT_MAX;

    IRIS_LOGD_IF(mTimeline == nullptr,
            "no sync mechanism, switch to synchronous mode");
    mSensorStaticInfo.reset(new NSCam::SensorStaticInfo());

#ifdef LEGACY_PATH // legacy path
    mEventIrq = nullptr;
#endif

    // init semaphore
    sem_init(&mSemMainLoop, 0, 0);
    sem_init(&mSemMainLoopDone, 0, 0);

    sem_init(&mSemEnqueLoop, 0, 0);
    sem_init(&mSemEnqueLoopDone, 0, 0);
}

NirPath::~NirPath()
{
    AutoLog();

    {
        std::lock_guard<std::mutex> _l(mTimelineLock);
        mTimeline = nullptr;
    }

    // destroy semaphore
    sem_destroy(&mSemMainLoop);
    sem_destroy(&mSemMainLoopDone);

    sem_destroy(&mSemEnqueLoop);
    sem_destroy(&mSemEnqueLoopDone);
}

void NirPath::destroyInstance()
{
    AutoLog();

    // assure worker threads exit
    {
        setState(State::EXIT);

        if (mControlWorker.joinable())
        {
            sem_post(&mSemMainLoop);
            IRIS_LOGD("join control worker +");
            mControlWorker.join();
            IRIS_LOGD("join control worker -");
        }

        if (mEnqueWorker.joinable())
        {
            sem_post(&mSemEnqueLoop);
            IRIS_LOGD("join enque worker +");
            mEnqueWorker.join();
            IRIS_LOGD("join enque worker -");
        }
    }
}

void NirPath::setState(const State newState)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mState.second);

    IRIS_LOGD("Now(0x%04x), Next(0x%04x)", mState.first, newState);

    if (newState == State::ERROR)
        goto EXIT;

    switch (mState.first)
    {
    case State::NONE:
        switch (newState)
        {
        case State::INIT:
        case State::UNINIT:
            break;
        default:
            IRIS_LOGA("State error NONE");
        }
        break;
    case State::INIT:
        switch (newState)
        {
        case State::IDLE:
            break;
        default:
            IRIS_LOGA("State error INIT");
        }
        break;
    case State::IDLE:
        switch (newState)
        {
        case State::IDLE:
        case State::PREVIEW:
        case State::UNINIT:
            break;
        default:
            IRIS_LOGA("State error IDLE");
        }
        break;
    case State::PREVIEW:
        switch (newState)
        {
        case State::IDLE:
        case State::PREVIEW:
        case State::PREVIEW_STOP:
            break;
        default:
            IRIS_LOGA("State error PREVIEW");
            break;
        }
        break;
    case State::PREVIEW_STOP:
        switch (newState)
        {
        case State::IDLE:
            break;
        default:
            IRIS_LOGA("State error PREVIEW_STOP");
        }
        break;
    case State::UNINIT:
        switch (newState)
        {
        case State::EXIT:
            break;
        default:
            IRIS_LOGA("State error UNINIT");
        }
        break;
    case State::EXIT:
        IRIS_LOGD("Exit state");
        break;
    case State::ERROR:
        switch (newState)
        {
        case State::IDLE:
        case State::UNINIT:
            break;
        default:
            IRIS_LOGA("State error IRIS_MHAL_ERROR");
            break;
        }
        break;
    default:
        IRIS_LOGE("Unknown state Now(0x%04x), Next(0x%04x)",
                mState.first, newState);
    }

EXIT:
    mState.first = newState;
    IRIS_LOGD("Now(0x%04x)", mState.first);
}

State NirPath::getState()
{
    std::lock_guard<std::mutex> _l(mState.second);
    return mState.first;
}

Result NirPath::init(
        const std::unordered_map<StreamID, IrisStream>& streamMap)
{
    AutoLog();

    Result err = OK;

    setState(State::INIT);

    // copy data
    mStreamMap = streamMap;
    for (const auto& stream : mStreamMap)
        IRIS_LOGD("add stream index(%d)", toLiteral(stream.first));

    {
        std::unique_lock<std::mutex> _m(mTimelineLock);
        // a worker thread of controlling iris camera
        mControlWorker = std::thread(mainLoop, std::ref(*this));

        // a worker thread of controlling iris camera
        mEnqueWorker = std::thread(enqueLoop, std::ref(*this));
    }

    setState(State::IDLE);

    return err;
}

Result NirPath::unInit()
{
    AutoLog();

    Result err = OK;

    // Check it is in the idle mode
    // If it is not, has to wait until idle
    State eState = getState();

    IRIS_LOGD("eState(0x%04x)", eState);

    if (eState != State::NONE)
    {
        if ((eState != State::IDLE) && (eState != State::ERROR))
        {
            IRIS_LOGD("Camera is not in the idle state");
            if (toLiteral(eState) & toLiteral(State::PREVIEW_MASK))
            {
                err = StreamingOff();

                if (err != OK)
                {
                    IRIS_LOGE("irisMhalPreviewStop fail(0x%x)", err);
                }
            }

            while (eState != State::IDLE)
            {
                // Wait 10 ms per time
                usleep(10000);
                eState = getState();
            }
            IRIS_LOGD("Now camera is in the idle state");
        }

        setState(State::UNINIT);

        IRIS_LOGD("trigger main loop");
        sem_post(&mSemMainLoop);

        IRIS_LOGD("trigger enque loop");
        sem_post(&mSemEnqueLoop);
    }
    else
    {
        setState(State::UNINIT);
    }

    {
        std::unique_lock<std::mutex> _l(mStreamMapLock);
        mStreamMap.clear();
    }

    return OK;
}

Result NirPath::setSrcDev(unsigned int devID)
{
    AutoLog();

    mDevID.store(devID, memory_order_relaxed);

#ifndef LEGACY_PATH // non-legacy path
    IHalSensorList *halSensorList(MAKE_HalSensorList());
#else
    IHalSensorList *halSensorList = IHalSensorList::get();
#endif
    if (halSensorList == nullptr)
    {
        IRIS_LOGE("cannot get sensorlist");
        return INVALID_OPERATION;
    }

    // search and create sensor device
    {
        std::lock(mSensorHAL.second, mSensorArray.second);
        std::lock_guard<std::mutex> _l(mSensorHAL.second, std::adopt_lock);
        std::lock_guard<std::mutex> _m(mSensorArray.second, std::adopt_lock);

        if (!halSensorList->searchSensors())
        {
            IRIS_LOGE("search Sensor failed");
            return NAME_NOT_FOUND;
        }

        mSensorArray.first = UINT_MAX;
        const unsigned int sensors = halSensorList->queryNumberOfSensors();
        for (unsigned int i = 0; i < sensors; i++)
        {
            if (halSensorList->querySensorDevIdx(i) != devID)
            {
                IRIS_LOGD("sensor dev ID(%u) idx(%u)",
                        devID, halSensorList->querySensorDevIdx(i));
                continue;
            }

            mSensorArray.first = i;
            break;
        }
        IRIS_LOGD("sensor dev index(%u)", mSensorArray.first);

        if (mSensorArray.first == UINT_MAX)
        {
            IRIS_LOGE("Sensor Not found");
            return NAME_NOT_FOUND;
        }

        mSensorHAL.first =
            halSensorList->createSensor(DEBUG_LOG_TAG, 1, &mSensorArray.first);
        if (mSensorHAL.first == nullptr)
        {
            IRIS_LOGE("invalid sensor HAL");
            return UNKNOWN_ERROR;
        }
    }

    // update sensor information
    {
        std::lock_guard<std::mutex> _l(mSensorStaticInfoLock);
        halSensorList->querySensorStaticInfo(devID, mSensorStaticInfo.get());

        IRIS_LOGD("sensor dev(%d) index(%u) bitDepth(%u)",
                mDevID.load(memory_order_relaxed), mSensorArray.first,
                getRAWBitDepth(mSensorStaticInfo->rawSensorBit));
    }

    return OK;
}

Result NirPath::setShutterTime(uint32_t time)
{
    AutoLog();

    // set for shutter time in u-sec (10^(-6) sec). Ex: 1000000 means 1 sec
    std::lock_guard<std::mutex> _l(mSensorHAL.second);
    const auto deviceID(mDevID.load(memory_order_relaxed));
    mSensorHAL.first->sendCommand(deviceID,
            SENSOR_CMD_SET_SENSOR_EXP_TIME,
            reinterpret_cast<intptr_t>(&time), 0, 0);

    IRIS_LOGD("sensor dev(%d) exposure time(%d)", deviceID, time);

    return OK;
}

Result NirPath::setGainValue(uint32_t gain)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mSensorHAL.second);
    const auto deviceID(mDevID.load(memory_order_relaxed));
    mSensorHAL.first->sendCommand(deviceID,
            SENSOR_CMD_SET_SENSOR_GAIN,
            reinterpret_cast<intptr_t>(&gain), 0, 0);

    IRIS_LOGD("sensor dev(%d) gain(%d)", deviceID, gain);

    return OK;
}

Result NirPath::sensorPower(bool turnON)
{
    AutoLog();

    std::lock(mSensorHAL.second, mSensorArray.second);
    std::lock_guard<std::mutex> _l(mSensorHAL.second, std::adopt_lock);
    std::lock_guard<std::mutex> _m(mSensorArray.second, std::adopt_lock);

    if (mSensorHAL.first == nullptr)
    {
        IRIS_LOGE("invalid sensor HAL");
        return NO_INIT;
    }

    // TODO: check return status

    if (turnON)
    {
        // turn sensor on
        mSensorHAL.first->powerOn(DEBUG_LOG_TAG, 1, &mSensorArray.first);
    }
    else
    {
        // turn sensor off
        mSensorHAL.first->powerOff(DEBUG_LOG_TAG, 1, &mSensorArray.first);
    }

    return OK;
}

Result NirPath::getSensorSetting(NSCam::security::SensorStaticInfo& sensorSetting)
{
    AutoLog();

    // TODO: put HwInfoHelper as a data member
    NSCamHW::HwInfoHelper helper(mSensorArray.first);
    if (!helper.updateInfos())
    {
        IRIS_LOGE("update sensor static information failed");
        return BAD_VALUE;
    }

    if (!helper.getSensorSize(mSensorScenario, sensorSetting.size) ||
            !helper.getSensorFps(mSensorScenario, sensorSetting.fps) ||
            !helper.queryPixelMode(
                mSensorScenario, sensorSetting.fps, sensorSetting.pixelMode))
    {
        IRIS_LOGE("wrong parameter for IMGO");
        return BAD_VALUE;
    }
    IRIS_LOGD("sensor scenario(%u) fps(%d)", mSensorScenario, sensorSetting.fps);

    return OK;
}

std::vector<StreamInfo> NirPath::getStreamInfo(
        const NSCam::security::SensorStaticInfo& sensorSetting,
        const std::vector<StreamID>& identifiers)
{
    AutoLog();

    std::vector<StreamInfo> streamInfos;

    // TODO: put HwInfoHelper as a data member
    NSCamHW::HwInfoHelper helper(mSensorArray.first);
    if (!helper.updateInfos())
    {
        IRIS_LOGE("update sensor static information failed");
        return streamInfos;
    }

    // calculate full-sized Bayer-pattern image size

    int format;

    for (auto&& id : identifiers)
    {
        switch (id)
        {
            case StreamID::FULL_RAW:
                {
                    StreamInfo info(StreamID::FULL_RAW);
                    info.size = sensorSetting.size;
                    if (!helper.getImgoFmt(
                                getRAWBitDepth(mSensorStaticInfo->rawSensorBit), format) ||
                            !helper.alignPass1HwLimitation(
                                sensorSetting.pixelMode, format, true, info.size, info.stride))
                    {
                        IRIS_LOGE("wrong parameter for IMGO");
                        return streamInfos;
                    }

                    info.format = static_cast<EImageFormat>(format);
                    info.sizeInBytes = info.stride * info.size.h;

                    IRIS_LOGD("full imgSize(%u) = stride(%zu) * height(%d)",
                            info.sizeInBytes, info.stride , info.size.h);

                    streamInfos.push_back(std::move(info));
                }
                break;
            case StreamID::RESIZED_RAW:
                {
                    StreamInfo info(StreamID::RESIZED_RAW);
                    if (!helper.getRrzoFmt(
                                getRAWBitDepth(mSensorStaticInfo->rawSensorBit), format) ||
                            !helper.alignRrzoHwLimitation(
                                kResizedRAWSize, sensorSetting.size, info.size) ||
                            !helper.alignPass1HwLimitation(
                                sensorSetting.pixelMode, format, false, info.size, info.stride))
                    {
                        IRIS_LOGE("wrong parameter for RRZO");
                        return streamInfos;
                    }

                    info.format = static_cast<EImageFormat>(format);
                    info.sizeInBytes = info.stride * info.size.h;

                    IRIS_LOGD("resized imgSize(%u) = stride(%zu) * height(%d)",
                            info.sizeInBytes, info.stride , info.size.h);

                    streamInfos.push_back(std::move(info));
                }
                break;
            default:
                IRIS_LOGE("unknown stream ID(0x%x)", toLiteral(id));
        }
    }

    return streamInfos;
}

void NirPath::onBufferReleased()
{
    AutoLog();
    // TODO: used this event to optimize redundant waiting in dequeueBuffer()
}

Result NirPath::StreamingOn()
{
    AutoLog();

    Result err = OK;

    if (getState() != State::IDLE)
    {
        IRIS_LOGE("[irisMhalPreviewStart] Camera State is not IDLE");
        return INVALID_OPERATION;
    }

    mNormalPipe = createDefaultNormalPipe(mSensorArray.first, DEBUG_LOG_TAG);
    if (!mNormalPipe->init())
    {
        IRIS_LOGE("normal pipe init() failed");
        return UNKNOWN_ERROR;
    }

    NSCam::security::SensorStaticInfo sensorSetting;
    err = configPipe(*mNormalPipe, sensorSetting);
    if (err != OK)
        return err;

#ifdef LEGACY_PATH // legacy path
    mEventIrq = [this](const int devID) -> IEventIrq*
    {
        using namespace NSIspTuning;

        const auto sensorDev = [devID] {
            switch (devID)
            {
                case SENSOR_DEV_MAIN:
                    return ESensorDev_Main;
                case SENSOR_DEV_MAIN_2:
                    return ESensorDev_MainSecond;
                case SENSOR_DEV_SUB:
                    return ESensorDev_Sub;
                case SENSOR_DEV_SUB_2:
                    return ESensorDev_SubSecond;
                default:
                    IRIS_LOGE("unknown sensor dev(%d)", devID);
                    return ESensorDev_None;
            };
        }();

        // query sensor dynamic information to get the right TG information
        // NOTE: querySensorDynamicInfo() is only valid after configPipe()
        SensorDynamicInfo senInfo;
        {
            std::lock_guard<std::mutex> _l(mSensorHAL.second);
            if (!mSensorHAL.first->querySensorDynamicInfo(devID, &senInfo))
            {
                IRIS_LOGE("query sensor dynamic information failed");
                return nullptr;
            }
        }
        IRIS_LOGD("sensor dev(%d) TgInfo(%u)", devID, senInfo.TgInfo);

        if ((senInfo.TgInfo != CAM_TG_1) && (senInfo.TgInfo != CAM_TG_2))
        {
            IRIS_LOGE("RAW sensor is connected with TgInfo(%u)", senInfo.TgInfo);
            return nullptr;
        }

        const auto tgInfo =
            (senInfo.TgInfo == CAM_TG_1) ? IEventIrq::E_TG1 : IEventIrq::E_TG2;

        IEventIrq::ConfigParam irqConfig(
                sensorDev, tgInfo, IEventIrq::E_Event_Vsync);
        return IEventIrq::createInstance(irqConfig, DEBUG_LOG_TAG);
    }(mDevID.load(memory_order_relaxed));
#endif

    // prepare buffers needed at once
    std::vector<std::pair<StreamID, int>> dataSizeMap;
    for (auto i = 0; i < mBufNum; ++i)
    {
        // NOTE: all DMA ports apply the same count
        const auto frameNumber = mEnqCount++;

        // fill ISP with buffers
        QBufInfo outputPortInfo;
        for (auto&& stream : mStreamMap)
        {
            const auto streamID(toLiteral(stream.first));

            IrisBufferQueue::IrisBuffer buffer;
            stream.second->dequeueBuffer(&buffer, false, kSecurePath);
            IRIS_LOGD("dequeue stream(%d) buffer FD(%d) frameNumber(%u)",
                    streamID, buffer.ion_fd, frameNumber);

            // keep data size in mind
            if (i == 0)
            {
                dataSizeMap.push_back(std::make_pair(
                            stream.first, buffer.data_size));
            }

            const auto key = buffer.ion_fd;

            // query stream information
            std::vector<StreamID> identifiers;
            identifiers.push_back(stream.first);
            const auto streamInfo = getStreamInfo(sensorSetting, identifiers)[0];

            size_t bufStridesInBytes[3] { streamInfo.stride, 0, 0 };
            size_t bufBoundaryInBytes[3] {};
            IImageBufferAllocator::ImgParam imgParam(
                    streamInfo.format,
                    streamInfo.size,
                    bufStridesInBytes, bufBoundaryInBytes, 1);

            // NOTE:
            // 1. wrap allocated memory from BufferQueue as blob buffer
            // 2. virtual address is 0 for secure buffer
            PortBufInfo_v1 portBufInfo( key,
                    (kSecurePath) ? (uintptr_t)(buffer.sec_handle) : reinterpret_cast<uintptr_t>(buffer.va),
                    0, (kSecurePath) ? 1 : 0, 0);

            ImageBufferHeap* const bufferHeap(
                    ImageBufferHeap::create(DEBUG_LOG_TAG, imgParam ,portBufInfo));
            if (bufferHeap == nullptr)
            {
                IRIS_LOGE("create Image Buffer heap failed");
                return BAD_VALUE;
            }

            sp<IImageBuffer> imageBuffer = bufferHeap->createImageBuffer();
            if (imageBuffer == nullptr)
            {
                IRIS_LOGE("create Image Buffer failed");
                return BAD_VALUE;
            }

            // NOTE: lockBuf() is valid for normal buffer
            if(!kSecurePath && !(imageBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage)))
            {
                IRIS_LOGE("Image Buffer lock failed");
                return BAD_VALUE;
            }

            {
                const auto portID = getPortID(stream.first);

                BufInfo bufInfo(portID,
                        kSecurePath ? nullptr : imageBuffer.get(),
                        imageBuffer->getImgSize(),
                        MRect(imageBuffer->getImgSize().w,
                            imageBuffer->getImgSize().h),
                        frameNumber, 0, kBufferIndex);

                if (kSecurePath)
                {
                    // TODO: check data type conversion (size_t to unsigned int)
                    bufInfo.mSize = buffer.data_size;
                }

                outputPortInfo.mvOut.push_back(std::move(bufInfo));

                IRIS_LOGD("p1_enq: DMA port(0x%x) buffer FD(%d)",
                        portID.index, portBufInfo.memID[0]);
            }

            switch (stream.first)
            {
                case StreamID::FULL_RAW:
                    mFullSizedBufCtrl.mvBuffer.push_back(imageBuffer);
                    break;
                case StreamID::RESIZED_RAW:
                    mResizedBufCtrl.mvBuffer.push_back(imageBuffer);
                    break;
                default:
                    IRIS_LOGE("unknown stream(%d)", streamID);
            }

            // keep frame number, stream ID and buffer thereof in mind
            auto streamBuffer =
                std::make_tuple(frameNumber, stream.first, std::move(buffer));

            // establish buffer mapping rules
            std::lock_guard<std::mutex> _m(mMapLock);
            if (CC_LIKELY(mMap.emplace(key, std::move(streamBuffer)).second == false))
                IRIS_LOGA("cannot emplace bufer: key(0x%x)", key);
        }

        if (!mNormalPipe->enque(outputPortInfo))
        {
            IRIS_LOGE("p1 enque failed");
            return UNKNOWN_ERROR;
        }
    }

#ifdef LEGACY_PATH // legacy path
    // prepare dummy buffers needed at once
    // NOTE: ISP driver needs N ring buffers and N dummy buffers.
    //       For memory reduction, we allocate one blob buffer with
    //       base address offset to emulate N dedicated ones.
    std::vector<StreamID> identifiers;
    for (const auto dataSize : dataSizeMap)
    {
        const auto bufferSize =
            dataSize.second + (sizeof(intptr_t) * (mBufNum - 1));
        // FIXME: secure memory allocation for dummy buffer
        switch (dataSize.first)
        {
            case StreamID::FULL_RAW:
                {
                    mFullSizedDummyBufCtrl.create(bufferSize, mBufNum, kSecurePath);
                    identifiers.push_back(dataSize.first);
                }
                break;
            case StreamID::RESIZED_RAW:
                {
                    mResizedDummyBufCtrl.create(bufferSize, mBufNum, kSecurePath);
                    identifiers.push_back(dataSize.first);
                }
                break;
            default:
                IRIS_LOGE("unknown stream id(%d)", toLiteral(dataSize.first));
        }
    }

    // query stream information
    const auto streamInfo = getStreamInfo(sensorSetting, identifiers);
    for (auto i = 0; i < mBufNum; ++i)
    {
        // NOTE: all DMA ports apply the same count
        const auto frameNumber = mEnqCount++;

        // TODO: check validity of magic number (mEnqCount) and SOF index (0)
        QBufInfo outputPortInfo;
        // NOTE: the order of dataSizeMap and streamInfo are the same with each other
        size_t j = 0;
        for (const auto dataSize : dataSizeMap)
        {
            const auto streamID(dataSize.first);
            const DummyBufCtl* dummyBufCtl = [&]() -> DummyBufCtl*
            {
                switch (streamID)
                {
                    case StreamID::FULL_RAW:
                        return &mFullSizedDummyBufCtrl;
                    case StreamID::RESIZED_RAW:
                        return &mResizedDummyBufCtrl;
                    default:
                        IRIS_LOGE("unknown stream id(%d)", toLiteral(streamID));
                        return nullptr;
                }
            }();

            BufInfo bufInfo(getPortID(streamID),
                    kSecurePath ? nullptr : dummyBufCtl->itemAt(i),
                    streamInfo[j].size,
                    MRect(streamInfo[j].size.w, streamInfo[j].size.h),
                    frameNumber, 0, kBufferIndex);

            if (kSecurePath)
            {
                // TODO: check data type conversion (size_t to unsigned int)
                const auto& buffer(*dummyBufCtl->secureItemAt(i));
                bufInfo.mSize = buffer.data_size;
                bufInfo.mPa = buffer.sec_handle;
                bufInfo.mVa = buffer.sec_handle;
            }

            outputPortInfo.mvOut.push_back(std::move(bufInfo));

            j++;
        }

        if (!mNormalPipe->DummyFrame(outputPortInfo))
        {
            IRIS_LOGE("p1 enque dummy frame failed");
            return UNKNOWN_ERROR;
        }
    }
#endif

    mMapCondition.notify_one();

    IRIS_LOGD("preview state(0x%x)", State::PREVIEW);
    setState(State::PREVIEW);

    IRIS_LOGD("trigger main loop");
    sem_post(&mSemMainLoop);

    IRIS_LOGD("trigger enque loop");
    sem_post(&mSemEnqueLoop);

    return err;
}

Result NirPath::StreamingOff()
{
    AutoLog();

    Result err = OK;
    State state = getState();

    {
        std::unique_lock<std::mutex> _l(mRequestExit.second);
        mRequestExit.first = true;
    }

    IRIS_LOGD("state(0x%04x)", state);
    if (state == State::IDLE)
    {
        IRIS_LOGW("is in IDLE state");
        return INVALID_OPERATION;
    }
    else if (state != State::PREVIEW_STOP)
    {
        if (toLiteral(state) & toLiteral(State::PREVIEW_MASK))
        {
            setState(State::PREVIEW_STOP);
        }
        else
        {
            IRIS_LOGD("unknow state(%d)", state);
        }
    }

    IRIS_LOGD("wait main loop...");
    sem_wait(&mSemMainLoopDone);
    IRIS_LOGD("wait main loop done");

    IRIS_LOGD("wait enque loop...");
    sem_wait(&mSemEnqueLoopDone);
    IRIS_LOGD("wait enque loop done");

    if (!mNormalPipe->stop())
    {
        err = UNKNOWN_ERROR;
        IRIS_LOGE("stop normal pipe failed");
    }

    if (!mNormalPipe->uninit())
    {
        err = UNKNOWN_ERROR;
        IRIS_LOGE("uninit normal pipe failed");
    }

#ifdef LEGACY_PATH // legacy path
    mEventIrq->destroyInstance(DEBUG_LOG_TAG);
#endif

    IRIS_LOGD("NormalPipe destroyInstance");
    mNormalPipe->destroyInstance(DEBUG_LOG_TAG);
    mNormalPipe = nullptr;

    setState(State::IDLE);

    return err;
}

Result NirPath::StreamingProc()
{
    AutoLog();

    unsigned int frameCount = 0;

    IRIS_LOGD("Preview pass1 start");
    mNormalPipe->start();

#ifdef LEGACY_PATH // legacy path
    {
        HwEventIrq::ConfigParam irqConfig(1, 0, HwEventIrq::E_Event_Vsync);
        std::lock_guard<std::mutex> _l(mHwEventIrq.second);
        mHwEventIrq.first = HwEventIrq::createInstance(irqConfig, DEBUG_LOG_TAG);
    }
#endif

    // NOTE: From ISP hardware's aspect, we can call stop() even not receving
    //       the first frame from ISP.
    //       From ISP software's view, however, we need to wait for the first
    //       frame from ISP before calling stop().
    while ((toLiteral(getState()) & toLiteral(State::PREVIEW_MASK)) ||
           (frameCount == 0))
    {
        IRIS_LOGD("irisMhalGetState eState(0x%04x)", getState());
        IRIS_LOGW_IF(CC_UNLIKELY(frameCount == 0) && (getState() != State::PREVIEW)
                , "wait for the first frame before exit...");

        std::unordered_map<StreamID, IImageBuffer*> bufferMap;
        bool isDummy = false;
        [&bufferMap, &isDummy, this]
        {
            IRIS_LOGD("ISP deque");
            QBufInfo outputPortInfo;
            auto ret = dequeue(*mNormalPipe, outputPortInfo);
            if (ret != OK)
            {
                if (ret == NOT_ENOUGH_DATA)
                {
                    IRIS_LOGW("ISP deque: dummy frame, do nothing");
                    isDummy = true;
                }
                else
                {
                    IRIS_LOGA("ISP deque: failed");
                }
                outputPortInfo.mvOut.clear();
                return;
            }
            else
            {
                IRIS_LOGD("ISP deque: done");
            }

            if (CC_UNLIKELY(0 == outputPortInfo.mvOut.size()))
            {
                IRIS_LOGW("ISP deque: no buffer");
                return;
            }

            for (auto&& item : outputPortInfo.mvOut)
            {
                // find full-sized or resized image only
                const StreamID index = [&item]
                {
                    if (item.mPortID.index == PORT_IMGO.index)
                    {
                        return StreamID::FULL_RAW;
                    }
                    else if (item.mPortID.index == PORT_RRZO.index)
                    {
                        return StreamID::RESIZED_RAW;
                    }
                    else
                    {
                        IRIS_LOGE("unknown index(0x%x)", item.mPortID.index);
                        return StreamID::UNKNOWN;
                    }
                }();
                IImageBuffer *buffer = item.mBuffer;

                if (bufferMap.emplace(index, buffer).second == false)
                    IRIS_LOGA("cannot emplace bufer: index(%d)", toLiteral(index));

                IRIS_LOGD("returned buffer slot DMA port(0x%x) stream(%d) " \
                        "frameNumber(%u)",
                        item.mPortID.index, toLiteral(index),
                        item.mMetaData.mMagicNum_hal);
            }
        }();

        if (CC_LIKELY(!isDummy))
        {
            // find and remove filled buffers from mapping rules
            // FIXME: ensure the ordering of returned streams in bufferMap
            for (auto&& item : bufferMap)
            {
                const StreamID streamID(item.first);

                IrisBufferQueue::IrisBuffer buffer;
                {
                    const int index = item.second->getFD(0);
                    IRIS_LOGD("wait for available producer buffer index(%d)...",
                            index);

                    if (CC_UNLIKELY(!mFrameDropHandler.empty()))
                    {
                        IRIS_LOGD("wait frame drop handler +");
                        std::lock_guard<std::mutex> _l(mFrameDropHandlerLock);
                        for (auto it = mFrameDropHandler.begin();
                                  it != mFrameDropHandler.end(); )
                        {
                            // get handler's result
                            const auto frameNumber = it->get();

                            // then remove the retired dropped frame handler
                            it = mFrameDropHandler.erase(it);

                            IRIS_LOGD("frame drop handler finish frame(%u)",
                                    frameNumber);
                        }
                        IRIS_LOGD("wait frame drop handler -");
                    }

                    std::unique_lock<std::mutex> _l(mMapLock);

                    auto search = mMap.end();
                    bool noTimeout = mMapCondition.wait_for(_l, kDequeueBufferTimeout,
                        [&]
                        {
                            search = mMap.find(index);
                            if (search != mMap.end())
                            {
                                IRIS_LOGD("find available producer buffer(%d) stream(%d)",
                                    index, toLiteral(streamID));

                                // NOTE: keep interest result in mind for later usage
                                buffer = std::get<2>(search->second);

                                // consume the produced buffers
                                mMap.erase(index);

                                return true;
                            }
                            return false;
                        });

                    IRIS_LOGA_IF(!noTimeout, "no such element is found: index(%d)",
                            index);
                }

                {
                    std::unique_lock<std::mutex> _l(mRequestExit.second);
                    if (!mRequestExit.first)
                    {
                        std::lock_guard<std::mutex> _l(mTimelineLock);
                        if (mTimeline.get())
                        {
                            buffer.acquire_fence =
                                mTimeline->createFence(
                                        DEBUG_LOG_TAG, ++mTimelineCounter);
                            mTimeline->inc(1);
                        }

                        // TODO:
                        // 1. add frame orientation if necessary
                        // 2. return ION FD or VA according to secure or normal path
                        IrisCallBackInfo info(
                                streamID,
                                buffer.ion_fd,
                                std::make_pair(reinterpret_cast<void*>(buffer.va),
                                    buffer.data_size), 0);

                        mStreamMap[streamID]->queueBuffer(&buffer, &info);
                    }
                    else
                    {
                        mStreamMap[streamID]->cancelBuffer(buffer.index);
                        // TODO: check if this line is necessary
                        //mRequestExit.first = false;
                    }
                }
            }
        }

        frameCount++;

        IRIS_LOGD("frameCount(%d)", frameCount);
    }

#ifdef LEGACY_PATH // legacy path
    {
        std::lock_guard<std::mutex> _l(mHwEventIrq.second);
        mHwEventIrq.first->destroyInstance(DEBUG_LOG_TAG);
        mHwEventIrq.first = nullptr;
    }
#endif

    return OK;
}

Result NirPath::StreamingEnque()
{
    AutoLog();

    while (toLiteral(getState()) & toLiteral(State::PREVIEW_MASK))
    {
        IRIS_LOGD("irisMhalGetState eState(0x%04x)", getState());
        uint32_t sof_idx = 0;
        // wait ISP start-of-frame signal
        sendCommand(*mNormalPipe, PipeCommand::GET_CURRENT_START_OF_FRAME_INDEX,
                reinterpret_cast<intptr_t>(&sof_idx), 0, 0);
        IRIS_LOGD("SOF(%u)", sof_idx);

        QBufInfo enqQ;
        {
            // all DMA ports apply the same count
            const auto frameNumber = mEnqCount++;

            // fill ISP with buffers
            for (auto&& stream : mStreamMap)
            {
                const auto streamID(toLiteral(stream.first));

                IrisBufferQueue::IrisBuffer buffer;
                stream.second->dequeueBuffer(&buffer, false, kSecurePath);
                IRIS_LOGD("dequeue stream(%d) buffer FD(%d) frameNumber(%u)",
                        streamID, buffer.ion_fd, frameNumber);

                // establish buffer mapping rules
                // TODO: check if map manipulation causes any
                //       fence/file descriptor/handle leakage
                {
                    const auto index = buffer.ion_fd;

                    std::lock_guard<std::mutex> _l(mMapLock);
                    auto search = mMap.find(index);
                    if (search != mMap.end())
                    {
                        IRIS_LOGD("stream(%d) buffer index(%d) already exist, overwrite it",
                                streamID, index);
                        mMap.erase(index);
                    }

                    auto streamBuffer =
                        std::make_tuple(frameNumber, stream.first, std::move(buffer));
                    if (mMap.emplace(index, std::move(streamBuffer)).second == false)
                        IRIS_LOGA("cannot emplace bufer: index(%d)", index);
                }

                IImageBuffer* const imageBuffer = [&]() -> IImageBuffer*
                {
                    if (kSecurePath)
                        return nullptr;

                    switch (stream.first)
                    {
                        case StreamID::FULL_RAW:
                            return mFullSizedBufCtrl.mvBuffer[buffer.index].get();
                        case StreamID::RESIZED_RAW:
                            return mResizedBufCtrl.mvBuffer[buffer.index].get();
                        default:
                            IRIS_LOGE("unknown stream(%d)", streamID);
                            return nullptr;
                    }
                }();

                {
                    const auto portID = getPortID(stream.first);

                    BufInfo bufInfo(portID,
                            imageBuffer,
                            imageBuffer->getImgSize(),
                            MRect(imageBuffer->getImgSize().w,
                                imageBuffer->getImgSize().h),
                            frameNumber, sof_idx, kBufferIndex);

                    if (kSecurePath)
                    {
                        // TODO: check data type conversion (size_t to unsigned int)
                        bufInfo.mSize = buffer.data_size;
                    }

                    enqQ.mvOut.push_back(std::move(bufInfo));
                }
            }
        }

        if (waitStartOfFrame(*mNormalPipe) != OK)
            IRIS_LOGW("wait vsync timeout");

        if (mNormalPipe->enque(enqQ))
        {
            IRIS_LOGD("enque done");
        }
        else
        {
            IRIS_LOGE("enque failed");
        }
    }

    return OK;
}

Result NirPath::handleFrameDrop(const uint32_t frameNumber)
{
    AutoLog();

    // NOTE: each frame number may contain one or more streams,
    //       and we need to remove all of them.
    std::lock_guard<std::mutex> _l(mFrameDropHandlerLock);
    mFrameDropHandler.push_back(std::async(
        std::launch::async,
        [this](const uint32_t frameNumber) {
            IRIS_LOGD("handler frame(%u) +", frameNumber);

            std::lock_guard<std::mutex> _m(mMapLock);
            for (auto it = mMap.begin(); it != mMap.end(); )
            {
                // Firstly, we need to find all streams
                // belonging to the dropped frame number
                const auto search = std::get<uint32_t>(it->second);
                if (search != frameNumber)
                {
                    IRIS_LOGD("frame(%u) is not equal to dropped frame(%u)",
                        search, frameNumber);
                    ++it;
                    continue;
                }

                const auto streamID(toLiteral(std::get<StreamID>(it->second)));
                IRIS_LOGD("cancel stream(%d) buffer FD(%d) frameNumber(%u)",
                        streamID, it->first, frameNumber);
                // return all streams found back to buffer queue
                mStreamMap[std::get<StreamID>(it->second)]->cancelBuffer(
                        std::get<IrisBufferQueue::IrisBuffer>(it->second).index, true);

                // then remove dropped frame tracking
                it = mMap.erase(it);
            }

            IRIS_LOGD("handler frame(%u) -", frameNumber);

            return frameNumber;
        },
        frameNumber));

    return OK;
}

template <typename T>
Result NirPath::configPipe(T& normalPipe,
        NSCam::security::SensorStaticInfo& sensorSetting)
{
    AutoLog();

    // calculate pass1 frame buffer size and stride
    Result err = getSensorSetting(sensorSetting);
    if (err != OK)
    {
        IRIS_LOGE("get sensor setting failed");
        return err;
    }

    IRIS_LOGD("sensor size(%u, %u) orientation(%u)",
            sensorSetting.size.w, sensorSetting.size.h,
            mSensorStaticInfo->orientationAngle);

    std::vector<IHalSensor::ConfigParam> vSensorCfg;
    {
        IHalSensor::ConfigParam configParam
        {
            .index            = mSensorArray.first,
            .crop             = sensorSetting.size,
            .scenarioId       = mSensorScenario,
            .isBypassScenario = false,
            .isContinuous     = true,
            .HDRMode          = 0,
            .framerate        = static_cast<unsigned int>(sensorSetting.fps),
            .twopixelOn       = 0,
            .debugMode        = 0,
        };
        IRIS_LOGD("sensor index(%u) size(%d, %d) mode(%d) bypass(%d) " \
                "continous(%d) hdr(%d) fps(%d)",
                configParam.index,
                configParam.crop.w, configParam.crop.h,
                configParam.scenarioId,
                configParam.isBypassScenario,
                configParam.isContinuous,
                configParam.HDRMode,
                configParam.framerate);

        vSensorCfg.push_back(std::move(configParam));
    }

    std::vector<portInfo> vPortInfo;

    if (mStreamMap.find(StreamID::FULL_RAW) != mStreamMap.end())
    {
        // query full-sized raw stream information
        std::vector<StreamID> identifiers;
        identifiers.push_back(StreamID::FULL_RAW);
        const auto streamInfo = getStreamInfo(sensorSetting, identifiers)[0];
        portInfo outPort(
                PORT_IMGO,
                streamInfo.format,
                streamInfo.size, //dst size
                MRect(streamInfo.size.w, streamInfo.size.h), //crop
                streamInfo.stride, 0, 0,
                0, //pureraw
                true); //packed
        IRIS_LOGD("config DMA port(0x%x), fmt(%u), size(%dx%d), crop(%u,%u,%u,%u)",
                outPort.mPortID.index, outPort.mFmt,
                outPort.mDstSize.w, outPort.mDstSize.h,
                outPort.mCropRect.p.x, outPort.mCropRect.p.y,
                outPort.mCropRect.s.w, outPort.mCropRect.s.h);
        vPortInfo.push_back(std::move(outPort));
    }

    if (mStreamMap.find(StreamID::RESIZED_RAW) != mStreamMap.end())
    {
        // query resized raw stream information
        std::vector<StreamID> identifiers;
        identifiers.push_back(StreamID::RESIZED_RAW);
        const auto streamInfo = getStreamInfo(sensorSetting, identifiers)[0];

        portInfo outPort(
                PORT_RRZO,
                streamInfo.format,
                streamInfo.size, //dst size
                MRect(streamInfo.size.w, streamInfo.size.h), //crop
                streamInfo.stride, 0, 0,
                0, //pureraw
                true); //packed
        IRIS_LOGD("config DMA port(0x%x), fmt(%u), size(%dx%d), crop(%u,%u,%u,%u)",
                outPort.mPortID.index, outPort.mFmt,
                outPort.mDstSize.w, outPort.mDstSize.h,
                outPort.mCropRect.p.x, outPort.mCropRect.p.y,
                outPort.mCropRect.s.w, outPort.mCropRect.s.h);
        vPortInfo.push_back(std::move(outPort));
    }

    {
        bool bDynamicRawType = true;
        QInitParam param(0,
                getRAWBitDepth(mSensorStaticInfo->rawSensorBit)/*unused*/,
                vSensorCfg, vPortInfo, bDynamicRawType);
        param.m_DropCB = onFrameDrop;
        param.m_returnCookie = this;

        if (!normalPipe.configPipe(param))
        {
            IRIS_LOGE("NormalPipe: configPipe failed");
            return UNKNOWN_ERROR;
        }
    }

    return OK;
}

template <typename T, typename INFO>
Result NirPath::dequeue(T& normalPipe, INFO& outputPortInfo)
{
    AutoLog();

#ifndef LEGACY_PATH // non-legacy path
    QPortID deqPorts;
    {
        for (auto&& stream : mStreamMap)
        {
            const auto streamID(toLiteral(stream.first));
            switch (stream.first)
            {
                case StreamID::FULL_RAW:
                    deqPorts.mvPortId.push_back(PORT_IMGO);
                    break;
                case StreamID::RESIZED_RAW:
                    deqPorts.mvPortId.push_back(PORT_RRZO);
                    break;
                default:
                    IRIS_LOGE("unknown stream(%d)", streamID);
            }
        }
    }

    if (!normalPipe.deque(deqPorts, outputPortInfo,
                kDequeTimeoutInMilliseconds))
    {
        IRIS_LOGE("ISP deque: failed");
        return UNKNOWN_ERROR;
    }
#else
    {
        for (auto&& stream : mStreamMap)
        {
            const auto streamID(toLiteral(stream.first));
            NSCam::NSIoPipe::NSCamIOPipe::BufInfo bufInfo;
            switch (stream.first)
            {
                case StreamID::FULL_RAW:
                    bufInfo.mPortID = PORT_IMGO;
                    outputPortInfo.mvOut.push_back(bufInfo);
                    break;
                case StreamID::RESIZED_RAW:
                    bufInfo.mPortID = PORT_RRZO;
                    outputPortInfo.mvOut.push_back(bufInfo);
                    break;
                default:
                    IRIS_LOGE("unknown stream(%d)", streamID);
            }
        }
    }

    if (!normalPipe.deque(outputPortInfo))
    {
        if (outputPortInfo.mvOut.at(0).mMetaData.m_bDummyFrame)
        {
            IRIS_LOGW("ISP deque: dummy frame");
            return NOT_ENOUGH_DATA;
        } else {
            IRIS_LOGE("ISP deque: failed");
            return UNKNOWN_ERROR;
        }
    }
#endif

    return OK;
}

template <typename T>
Result NirPath::sendCommand(T& normalPipe,
        PipeCommand command, intptr_t arg1, intptr_t arg2, intptr_t arg3)
{
#ifndef LEGACY_PATH // non-legacy path
    ENPipeCmd pipeCommand = [](const PipeCommand& command)
    {
        switch (command)
        {
            case PipeCommand::GET_CURRENT_START_OF_FRAME_INDEX:
                return ENPipeCmd_GET_CUR_SOF_IDX;
            default:
                IRIS_LOGE("unknown pipe command(0x%x)", toLiteral(command));
                return ENPipeCmd_MAX;
        }
    }(command);

    if (pipeCommand == ENPipeCmd_MAX)
        return UNKNOWN_ERROR;

    if (!normalPipe.sendCommand(pipeCommand, arg1, arg2, arg3))
        return UNKNOWN_ERROR;
#else
    using namespace NSImageio::NSIspio;
    EPIPECmd pipeCommand = [](const PipeCommand& command)
    {
        switch (command)
        {
            case PipeCommand::GET_CURRENT_START_OF_FRAME_INDEX:
                return EPIPECmd_GET_CUR_SOF_IDX;
            default:
                IRIS_LOGE("unknown pipe command(0x%x)", toLiteral(command));
                return EPIPECmd_MAX;
        }
    }(command);

    if (pipeCommand == EPIPECmd_MAX)
        return UNKNOWN_ERROR;

    if (!normalPipe.sendCommand(pipeCommand, arg1, arg2, arg3))
        return UNKNOWN_ERROR;
#endif

    return OK;
}

template <typename T>
Result NirPath::waitStartOfFrame(__unused T& normalPipe)
{
    AutoLog();

    // start-of-frame signal
    const EPipeSignal pipeSignal = EPipeSignal_SOF;
#ifndef LEGACY_PATH // non-legacy path

    // ClearWait: ignore the previous signal and wait for the next signal's arrival
    // ClearNone: returned immediately if the previous signal has arrived;
    //            otherwise wait for the next signal's arrival
    const EPipeSignalClearType pipeSignalClearType = EPipeSignal_ClearWait;

    if (!normalPipe.wait(pipeSignal, pipeSignalClearType, 0,
                kSignalTimeoutInNanoseconds))
        return UNKNOWN_ERROR;
#else
    // TODO: replace IEventIrq with NormalPipe_FrmB::wait() if possible
    IEventIrq::Duration duration;

    IRIS_LOGD("wait event IRQ +");
    mEventIrq->wait(duration);
    IRIS_LOGD("wait event IRQ -");

    // TODO: add wait "drop status" from P1Node
#endif

    return OK;
}

// ---------------------------------------------------------------------------

void NirPath::BufCtl::clear()
{
    AutoLog();

    for (auto&& imageBuffer : mvBuffer)
    {
        if (imageBuffer.get())
            imageBuffer->unlockBuf(DEBUG_LOG_TAG);
    }
    mvBuffer.clear();
}

#ifdef LEGACY_PATH // legacy path
NirPath::DummyBufCtl::~DummyBufCtl()
{
    clear();
}

Result NirPath::DummyBufCtl::create(
        const int dataSize, const int bufferCount, bool isSecure)
{
    // set secure flag
    mIsSecure.store(isSecure, memory_order_relaxed);

    if (!isSecure)
    {
        // 1. allocate ION buffer heap
        IIonImageBufferHeap* const ionBufferHeap(
                IIonImageBufferHeap::create(DEBUG_LOG_TAG,
                    IImageBufferAllocator::ImgParam(dataSize, 0)));
        if (ionBufferHeap == nullptr)
        {
            IRIS_LOGE("create Image Buffer heap failed");
            return BAD_VALUE;
        }

        sp<IImageBuffer> _imageBuffer = ionBufferHeap->createImageBuffer();
        if (_imageBuffer == nullptr)
        {
            IRIS_LOGE("createIMageBUffer failed");
            return BAD_VALUE;
        }

        if (!_imageBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage))
        {
            IRIS_LOGE("Image Buffer lock failed");
            return BAD_VALUE;
        }

        mBufferList.buffer = _imageBuffer;

        // 2. convert ION buffer heap to dummy buffer heap with
        //    base address offset
        const int32_t memID[] = { ionBufferHeap->getHeapID(), 0, 0 };
        for (auto i = 0; i < bufferCount; i++)
        {
            const auto offset = sizeof(uintptr_t) * i;
            const uintptr_t virtAddr[] =
                { static_cast<uintptr_t>(ionBufferHeap->getBufVA(0) + offset), 0, 0 };
            const uintptr_t phyAddr[] =
                { static_cast<uintptr_t>(ionBufferHeap->getBufPA(0) + offset), 0, 0 };
            IImageBufferHeap* const bufferHeap =
                IDummyImageBufferHeap::create(DEBUG_LOG_TAG,
                        IImageBufferAllocator::ImgParam(dataSize, 0),
                        PortBufInfo_dummy(memID, virtAddr, phyAddr, 1));

            if (bufferHeap == nullptr)
            {
                IRIS_LOGE("create Image Buffer heap failed");
                return BAD_VALUE;
            }

            sp<IImageBuffer> imageBuffer = bufferHeap->createImageBuffer();
            if (imageBuffer == nullptr)
            {
                IRIS_LOGE("createIMageBUffer failed");
                return BAD_VALUE;
            }

            if (!(imageBuffer->lockBuf(DEBUG_LOG_TAG, kBufferUsage)))
            {
                IRIS_LOGE("Image Buffer lock failed");
                return BAD_VALUE;
            }

            mvBuffer.push_back(imageBuffer);
        }
    }
    else
    {
        // allocate separate buffers from BufferQueue
        IrisBufferQueue::BufferParam param(
                dataSize, 1, dataSize, HAL_PIXEL_FORMAT_BLOB,
                dataSize, IrisBufferQueue::CacheType::CACHEABLE);

        std::lock_guard<std::mutex> _l(mDummyStreamLock);
        mDummyStream.reset(new IrisBufferQueue(bufferCount));
        mDummyStream->setBufferParam(std::move(param));

        for (auto i = 0; i < bufferCount; i++)
        {
            IrisBufferQueue::IrisBuffer buffer;
            mDummyStream->dequeueBuffer(&buffer, false, isSecure);
            IRIS_LOGD("dequeue dummy stream, buffer FD(%d) count(%d)",
                    buffer.ion_fd, i);

            mBufferList.buffers.push_back(std::move(buffer));
        }
    }

    return OK;
}

void NirPath::DummyBufCtl::clear()
{
    AutoLog();

    if (!mIsSecure.load(memory_order_relaxed))
    {
        if (mBufferList.buffer)
        {
            mBufferList.buffer->unlockBuf(DEBUG_LOG_TAG);
            mBufferList.buffer = nullptr;
        }
    }
    else
    {
        std::lock_guard<std::mutex> _l(mDummyStreamLock);
        if (mDummyStream)
        {
            for (auto&& buffer : mBufferList.buffers)
                mDummyStream->cancelBuffer(buffer.index);

            mBufferList.buffers.clear();
            mDummyStream = nullptr;
        }
    }
}

inline NSCam::IImageBuffer* NirPath::DummyBufCtl::itemAt(
        size_t index) const
{
    if (mIsSecure.load(memory_order_relaxed))
    {
        IRIS_LOGE("invalid access to normal buffer in secure flow");
        return nullptr;
    }

    IRIS_LOGA_IF(index >= mvBuffer.size(),
            "%s: index=%zu out of range (%zu)", __PRETTY_FUNCTION__,
            index, mvBuffer.size());
    return mvBuffer[index].get();
}

inline const Iris::IrisBufferQueue::IrisBuffer* NirPath::DummyBufCtl::secureItemAt(
        size_t index) const
{
    if (!mIsSecure.load(memory_order_relaxed))
    {
        IRIS_LOGE("invalid access to secure buffer in non-secure flow");
        return nullptr;
    }

    const auto& buffers(mBufferList.buffers);

    IRIS_LOGA_IF(index >= buffers.size(),
            "%s: index=%zu out of range (%zu)", __PRETTY_FUNCTION__,
            index, buffers.size());

    return &buffers[index];
}
#endif
