$(info devicemgr: HAL Version=$(CAMERA_HAL_VERSION))
ifneq ($(CAMERA_HAL_VERSION), 3)
ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

-include $(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := libmtkcam_security
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam

LOCAL_SRC_FILES := \
	SecureCamera.cpp \
	IrisCallback.cpp \
	../utils/BufferQueue.cpp \
	../utils/IonHelper.cpp \
	NirPath.cpp \
	RgbPath.cpp \
	SecureCameraFactory.cpp

LOCAL_C_INCLUDES := \
	$(MTK_PATH_CAM)/include \
	system/media/camera/include \
	$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
	libmtkcam_stdutils \
	libcam.halsensor \
	libmtkcam_modulehelper

# Due to software architecture on legacy platforms may vary from one to another,
# we use this as a hint during the compilation time
ifeq ($(MTKCAM_IP_BASE), 1) # non-legacy parts
LOCAL_SHARED_LIBRARIES += \
	libimageio \
	libmtkcam_imgbuf \
	libmtkcam_hwutils
else # legacy parts
LOCAL_CFLAGS += -DLEGACY_PATH
LOCAL_C_INCLUDES += \
	$(MTK_PATH_CAM)/legacy/include \
	$(MTK_PATH_CAM)/legacy/include/mtkcam \
	$(MTK_MTKCAM_PLATFORM) \
	$(MTK_MTKCAM_PLATFORM)/include \
	$(MTK_PATH_CUSTOM_PLATFORM)/hal/inc
LOCAL_SRC_FILES += \
	../../../legacy/platform/$(MTK_PLATFORM_DIR)/v3/hwnode/HwEventIrq.cpp

LOCAL_SHARED_LIBRARIES += \
	libcam.iopipe \
	libcam_utils \
	libcam_hwutils \
	libcam.hal3a.v3 \
	libcamdrv
endif


# Active stereo support
ifeq ($(MTK_CAM_ACTIVE_STEREO_SUPPORT), yes)

MTKCAM_CFLAGS += -DMTK_SECAM_ACTIVE_STEREO_PATH
LOCAL_SRC_FILES += \
	../utils/WorkPool.cpp \
	../utils/SecureBufferPool.cpp \
	IrPath.cpp \
	DepthCamera.cpp \
	DepthCallback.cpp

LOCAL_SHARED_LIBRARIES += \
	libmtkcam.featurepipe.depthmap \
	libfeature.stereo.provider

endif

LOCAL_SHARED_LIBRARIES += \
	libmtkcam_pipeline \
	libmtkcam_metadata \
	libmtkcam_metastore \
	libmtkcam_streamutils \
	libmtkcam_hwnode \
	libcam.legacypipeline \
	libcam.camshot \
	libmtkcam_fwkutils \
	libui

ifeq ($(TARGET_BOARD_PLATFORM), $(filter $(TARGET_BOARD_PLATFORM), mt6739))
LOCAL_CFLAGS += -DISP_SW_MAIN_VERSION=3
endif

LOCAL_CFLAGS += $(MTKCAM_CFLAGS) -DLOG_TAG=\"MtkCam/Security\" -DUSE_SYSTRACE

# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
LOCAL_SHARED_LIBRARIES += \
	libion \
	libion_mtk
endif

# MTK TEE (GenieZone)
ifeq ($(strip $(MTK_ENABLE_GENIEZONE)), yes)
$(info MTK TEE (GenieZone) is enabled)
LOCAL_CFLAGS += -DUSING_MTK_TEE
LOCAL_C_INCLUDES += \
	$(MTK_PATH_SOURCE)/geniezone/external/uree/include
LOCAL_STATIC_LIBRARIES += libgz_uree
endif

include $(MTK_SHARED_LIBRARY)

include $(call first-makefiles-under,$(LOCAL_PATH))

endif # MTK_CAM_SECURITY_SUPPORT
endif # CAMERA_HAL_VERSION
