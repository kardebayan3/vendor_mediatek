#ifndef __FR_CA_TA_DEBUG_H__
#define __FR_CA_TA_DEBUG_H__

// === common ===
#define FRAS_SECCAM_CB_BUF_DUMP_PROP "vendor.fras.cbbufdump"
#define FRAS_SECCAM_CB_BUF_DUMP_PROP_DEF_VAL 1

// src type
#define FRAS_SRC_TYPE_UNKNOWN -1
#define FRAS_SRC_TYPE_SECURE 0
#define FRAS_SRC_TYPE_NON_SECURE 1
#define FRAS_SRC_TYPE_FILE 2

#define FRAS_SRC_TYPE_IR_FILE_WIDTH 1280 // 640
#define FRAS_SRC_TYPE_IR_FILE_HEIGHT 720 // 480
#define FRAS_SRC_TYPE_IR_BYTES_PER_PIXEL 2
#define FRAS_SRC_TYPE_DEPTH_FILE_WIDTH 480 // 640
#define FRAS_SRC_TYPE_DEPTH_FILE_HEIGHT 272 // 480
#define FRAS_SRC_TYPE_DEPTH_BYTES_PER_PIXEL 2

#define FRAS_SIM_NON_SECURE_VERIFY_NEEDED 0

// refer to include/mtkcam/def/ImageFormat.h
#define FRAS_IR_COLOR_FORMAT 0x2213 // eImgFmt_BAYER10_UNPAK? eImgFmt_UFO_FG_BAYER14?
// refer to include/mtkcam/def/ImageFormat.h, system/core/libsystem/include/system/graphics-base.h
// real
#define FRAS_DEPTH_COLOR_FORMAT 0x20363159 // eImgFmt_Y16: 0x22 + 15 => 34 + 15 = 49 = 0x31
// test
// #define FRAS_DEPTH_COLOR_FORMAT 0x31 // eImgFmt_Y16: 0x22 + 15 => 34 + 15 = 49 = 0x31
// debug:
//#define FRAS_DEPTH_COLOR_FORMAT 0x2213 // eImgFmt_Y16: 0x22 + 15 => 34 + 15 = 49 = 0x31

// === ca: asfr specific ===
#define FRAS_CA_SRC_TYPE_PROP "vendor.fras.ca.srctype"
#define FRAS_CA_SRC_TYPE_PROP_DEF_VAL FRAS_SRC_TYPE_NON_SECURE
#define FRAS_FORCE_CA_ENROLL_SRC_TYPE FRAS_SRC_TYPE_NON_SECURE // -1: no-set, 0:secure, 1:non-secure, 2: file
#define FRAS_FORCE_CA_VERIFY_SRC_TYPE FRAS_SRC_TYPE_NON_SECURE // -1: no-set, 0:secure, 1:non-secure, 2: file

#define FRAS_CA_LOGLEVEL_PROP "vendor.fras.ca.loglevel"
#define FRAS_CA_LOG_ON 1
#define FRAS_CA_LOGLEVEL 2
#define FRAS_CA_LOGLEVEL_PROP_DEF_VAL FRAS_CA_LOGLEVEL

#define FRAS_CA_NEED_PREP_FRINPUT_PROP "vendor.fras.ca.needprepfrinput"
#define FRAS_CA_NEED_PREP_FRINPUT_DEF_VAL 1

#define FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE 0
#define FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY 0
#define FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY 1
#define FRAS_CA_IR_SRC_SHAREDMEM_SIZE 0x200000 // 2M
#define FRAS_CA_DEPTH_SRC_SHAREDMEM_SIZE 0x200000 // 2M

#define FRAS_CA_LOG_BUF_CONTENT 1

#define FRAS_CA_CALL_TA_PROP "vendor.fras.ca.callta"
#define FRAS_CA_CALL_TA_PROP_DEF_VAL 1

#define FRAS_CA_IR_FILEDUMP_SHAREDMEM_SIZE 0x200000 // 2M
#define FRAS_CA_DEPTH_FILEDUMP_SHAREDMEM_SIZE 0x200000 // 2M

#define FRAS_CA_SHOULD_BE_RESTORED 1

// === ta: asfr specific ===
#define FRAS_TA_FEATURE_MASK_CLEAR(x)	(x)=0

#define FRAS_TA_LOGLEVEL_PROP "vendor.fras.ta.loglevel"
#define FRAS_TA_LOG_ON 1
#define FRAS_TA_LOGLEVEL 2
#define FRAS_TA_LOGLEVEL_PROP_DEF_VAL FRAS_TA_LOGLEVEL
#define FRAS_TA_FEATURE_MASK_ENABLE_LOGLEVEL(x)	(x)|=(1<<0)
#define FRAS_TA_FEATURE_MASK_IS_LOGLEVEL_ENABLED(x)	((x&(1<<0)) ? 1 : 0)

#define FRAS_TA_CALL_ALGO_PROP "vendor.fras.ta.callalgo"
#define FRAS_TA_CALL_ALGO_PROP_DEF_VALUE 0
#define FRAS_TA_FEATURE_MASK_ENABLE_CALL_ALGO(x)	(x)|=(1<<1)
#define FRAS_TA_FEATURE_MASK_IS_CALL_ALGO_ENABLED(x)	((x&(1<<1)) ? 1 : 0)

#define FRAS_TA_CHK_POLICY_PROP "vendor.fras.ta.chkpolicy"
#define FRAS_TA_CHK_POLICY_PROP_DEF_VALUE 0
#define FRAS_TA_FEATURE_MASK_ENABLE_CHK_POLICY(x)	(x)|=(1<<2)
#define FRAS_TA_FEATURE_MASK_IS_CHK_POLICY_ENABLED(x)	((x&(1<<2)) ? 1 : 0)

#define FRAS_TA_VERIFY_FACE_ID 1
#define FRAS_TA_LOG_BUF_CONTENT 1

#define FRAS_TA_SAVE_IR_NEEDED 1
#define FRAS_TA_SAVE_DEPTH_NEEDED 0

#define FRAS_TA_SHOULD_BE_RESTORED 1

#endif

