#ifndef __FR_TA_TYPES_H__
#define __FR_TA_TYPES_H__

// FR config
typedef enum FR_CONFIG {
    FRCFG_NONE = 0,
    FRCFG_DEFAULT,
    FRCFG_DUMMY,
// Vendor1
    FRCFG_FPP_BASE=100,
    FRCFG_FPP_2D,
    FRCFG_FPP_AS,
    FRCFG_FPP_3DSL,
// Vendor2
    FRCFG_ST_BASE=200,
    FRCFG_ST_2D,
    FRCFG_ST_AS,
    FRCFG_ST_3DSL,
// Vendor3
    FRCFG_XXX_BASE=300,

} FRCFG_ENUM;

// === CMD related ===
typedef enum FRCA_STATE {
// Generic:
    FRCA_STATE_NONE   = 0x0000,
    FRCA_STATE_ALIVE,
    FRCA_STATE_SAVE_FEATURE,
    FRCA_STATE_SAVE_FEATURE_COPYING_SECURE_BUFFER,
    FRCA_STATE_SAVE_FEATURE_ONGOING,
    FRCA_STATE_COMPARE_FEATURE,
    FRCA_STATE_COMPARE_FEATURE_COPYING_SECURE_BUFFER,
    FRCA_STATE_COMPARE_FEATURE_ONGOING,
    FRCA_STATE_DELETE_FEATURE,
    FRCA_STATE_STOP_FEATURE,
    FRCA_STATE_UNINIT,
    FRCA_STATE_UKNOWN,
// TODO: Custom here
// ex:
    FRCA_XXX_STATE_BASE=2000,
    FRCA_XXX_STATE_1,
    FRCA_XXX_STATE_2,
} FRCA_STATE_ENUM;

typedef enum FRCA_CMD {
// Generic:
    FRALGOCA_CMD_NONE   = 0x0000,
    FRALGOCA_CMD_COPY_SECURE_BUFFER_TO_TA,
    FRALGOCA_CMD_START_SAVE_FEATURE,
    FRALGOCA_CMD_STOP_SAVE_FEATURE,
    FRALGOCA_CMD_DELETE_FEATURE,
    FRALGOCA_CMD_START_COMPARE_FEATURE,
    FRALGOCA_CMD_STOP_COMPARE_FEATURE,

// TODO: Custom here
// ex:
    FRALGOCA_XXX_CMD_BASE=2000,
    FRALGOCA_XXX_CMD1,
    FRALGOCA_XXX_CMD2,
} FRCA_CMD_ENUM;


typedef enum FRTA_CMD {
// Generic:
    FRTA_CMD_BASE=0x0000,
    FRTA_CMD_INIT,
    FRTA_CMD_UNINIT,
    FRTA_CMD_RESET,
    FRTA_CMD_COPY_SECURE_BUFFER,
    FRTA_CMD_START_SAVE_FEATURE,
    FRTA_CMD_STOP_SAVE_FEATURE,
    FRTA_CMD_DELETE_FEATURE,
    FRTA_CMD_START_COMPARE_FEATURE,
    FRTA_CMD_STOP_COMPARE_FEATURE,

// TODO: Custom here
    FRTA_XXX_CMD_BASE=2000,
    FRTA_XXX_CMD1,
    FRTA_XXX_CMD2,

// Test function only
    FRTA_CMD_TEST_BASE=9000,
    FRTA_CMD_TEST_FPP_LIB_INIT,
    FRTA_CMD_TEST_ION_SHARE,
// Test only
};

/* FIXED size = 64bytes, every member should be int32_t type */
#define FRCATA_BUF_FIXED_SIZE 64
#define FRCATA_BUF_CHECKSUM 0xff180502

typedef struct FRCaTaBuf {
    int chksum; // checksum
    int bufferId;
    unsigned int gzHandle ;
    unsigned int size;
    int width;
    int height ;
    int stride ;
    int format;
    int timestampHigh;
    int timestampLow;

	int srcType; // src: secure:0/normal:1/file:2
	int debugCfg; // ca/ta debug cfg
    int reserved3; // reserved
    int reserved4; // reserved
    int reserved5; // reserved
    int reserved6; // reserved
} FRCaTaBuf;

typedef enum FRTA_ASFR_STATE_ENUM {
    FRTA_ASFR_STATE_WAIT_FOR_IR = 0,
    FRTA_ASFR_STATE_WAIT_FOR_DEPTH,
    FRTA_ASFR_STATE_WAIT_FOR_ALGO_HANDLING,
    FRTA_ASFR_STATE_STOP
} FRTA_ASFR_STATE_ENUM;


// TODO: these three should be moved to [custom] folder
// camera_custom_fr_base.h(default 2DFR setting + camera_custom_fr.h
#define FRCATA_USER_MAX_NAME_LEN 64

#define FRCATA_AS_BUF_NUM_NEEDED 2
#define FRCATA_2D_BUF_NUM_NEEDED 1
#define FRCATA_DUMMY_BUF_NUM_NEEDED 1

#define FRCATA_FACEID_FOLDER "/sdcard"

#define FRCATA_FACEID_PREFIX "FACE_"

#define FRCATA_FACEID_POSTFIX_AS_FEATURE "AS_FEATURE"
#define FRCATA_FACEID_POSTFIX_AS_IR "AS_IR"
#define FRCATA_FACEID_POSTFIX_AS_DEPTH "AS_DEPTH"
#define FRCATA_FACEID_POSTFIX_2D_RAW "2D_RAW"
#define FRCATA_FACEID_POSTFIX_2D_YUV "2D_YUV"

#endif // __FR_TA_TYPES_H_HH

