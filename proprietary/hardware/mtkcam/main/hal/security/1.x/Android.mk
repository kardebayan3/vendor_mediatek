$(info devicemgr: HAL Version=$(CAMERA_HAL_VERSION))
ifneq ($(CAMERA_HAL_VERSION), 3)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

-include $(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := vendor.mediatek.hardware.camera.security@1.0-impl
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE_OWNER := mtk
LOCAL_PROPRIETARY_MODULE := true
ifeq ($(MTK_CAM_HAL_VERSION), 3)
LOCAL_MULTILIB := first
endif

LOCAL_CFLAGS += -DMTK_SECAM_DISABLE_NORMAL_CAM
LOCAL_SRC_FILES := SecureCamera.cpp

LOCAL_C_INCLUDES := \
	$(MTK_PATH_SOURCE)/hardware/mtkcam/include

LOCAL_STATIC_LIBRARIES := \
	android.hardware.camera.common@1.0-helper \
	libmtkcam_ionhelper

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
	libmtkcam_stdutils \
	libmtkcam_sysutils \
	libhidlbase \
	libhidltransport \
	android.hardware.camera.common@1.0 \
	android.hardware.graphics.mapper@2.0 \
	vendor.mediatek.hardware.camera.security@1.0

# Active stereo support
ifeq ($(MTK_CAM_ACTIVE_STEREO_SUPPORT), yes)
LOCAL_CFLAGS += -DMTK_SECAM_ACTIVE_STEREO_PATH
endif

ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)
LOCAL_CFLAGS += -DMTK_CAM_SECURITY_SUPPORT
endif # MTK_CAM_SECURITY_SUPPORT

# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
LOCAL_SHARED_LIBRARIES += \
	libion \
	libion_mtk
endif # MTK_ION_SUPPORT

LOCAL_REQUIRED_MODULES := libmtkcam_security

include $(MTK_SHARED_LIBRARY)

endif # CAMERA_HAL_VERSION
