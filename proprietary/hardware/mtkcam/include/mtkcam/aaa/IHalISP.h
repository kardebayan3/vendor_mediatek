/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein is
* confidential and proprietary to MediaTek Inc. and/or its licensors. Without
* the prior written permission of MediaTek inc. and/or its licensors, any
* reproduction, modification, use or disclosure of MediaTek Software, and
* information contained herein, in whole or in part, shall be strictly
* prohibited.
*
* MediaTek Inc. (C) 2010. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
* ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
* WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
* WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
* NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
* RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
* INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
* TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
* RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
* OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
* SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
* RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
* ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
* RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
* MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
* CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek
* Software") have been modified by MediaTek Inc. All revisions are subject to
* any receiver's applicable license agreements with MediaTek Inc.
*/

/**
* @file IHalISP.h
* @brief Declarations of Abstraction of ISP Hal Class and Top Data Structures
*/

#ifndef __IHAL_ISP_V3_H__
#define __IHAL_ISP_V3_H__

#include <mtkcam/def/common.h>
#include <vector>

//Warning For Build Pass MetaSet_T
#include "IHal3A.h"
#include "aaa_hal_common.h"

using namespace std;

namespace NS3Av3
{

enum EISPCtrl_T
{
    EISPCtrl_Begin = 0,
    //ISP

    EISPCtrl_GetIspGamma      = 0x0001,
    EISPCtrl_ValidatePass1,
    EISPCtrl_SetIspProfile,
    EISPCtrl_GetOBOffset,
    EISPCtrl_GetRwbInfo,
    EISPCtrl_SetOperMode,
    EISPCtrl_GetOperMode,
    EISPCtrl_GetMfbSize,
    EISPCtrl_SetLcsoParam,
    EISPCtrl_Num
};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
 * @brief Interface of ISP Hal Class
 */
class IHalISP {

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  //    Ctor/Dtor.
                        IHalISP(){}
    virtual             ~IHalISP(){}

private: // disable copy constructor and copy assignment operator
                        IHalISP(const IHalISP&);
    IHalISP&             operator=(const IHalISP&);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:

    /**
     * @brief destroy instance of IHal3A
     * @param [in] strUser user name
     */
    virtual MVOID       destroyInstance(const char* strUser) {}

    /**
     * @brief config ISP setting
     * @param [in] rConfigInfo ISP setting of ConfigInfo_T
     * @return
     * - MINT32 value of TRUE/FALSE.
     */
    virtual MINT32      config(const ConfigInfo_T& rConfigInfo) = 0;

    virtual MINT32      config(MINT32 i4SubsampleCount = 0)
                        {
                            ConfigInfo_T info;
                            info.i4SubsampleCount = i4SubsampleCount;
                            return config(info);
                        };
    /**
     * @brief set sensor mode
     * @param [in] i4SensorMode
     */
    virtual MVOID       setSensorMode(MINT32 i4SensorMode) = 0;

    /**
     * @brief init ISP
     */
    virtual MBOOL       init(const char* strUser) = 0;

    /**
     * @brief uninit ISP
     */
    virtual MBOOL       uninit(const char* strUser) = 0;

     /**
     * @brief start ISP
     */
    virtual MBOOL       start() = 0;

      /**
     * @brief stop ISP
     */
    virtual MBOOL       stop() = 0;

    /**
     * @brief
     * @param [in]
     * @param [in]
     * @param [out]
     * @param [out]
     */
    virtual MBOOL       setP1Isp(const vector<MetaSet_T*>& requestQ, MBOOL const fgForce = 0) = 0;

    /**
     * @brief
     * @param [in]
     * @param [in]
     * @param [out]
     * @param [out]
     */
    virtual MBOOL       setP2Isp(MINT32 flowType, const MetaSet_T& control, TuningParam* pTuningBuf, MetaSet_T* pResult) = 0;

    virtual MINT32      sendIspCtrl(EISPCtrl_T eISPCtrl, MINTPTR iArg1, MINTPTR iArg2) = 0;

    /**
     * @brief Attach callback for notifying
     * @param [in] eId Notification message type
     * @param [in] pCb Notification callback function pointer
     */
    virtual MINT32      attachCb(/*IHal3ACb::ECb_T eId, IHal3ACb* pCb*/) = 0;

    /**
     * @brief Dettach callback
     * @param [in] eId Notification message type
     * @param [in] pCb Notification callback function pointer
     */
    virtual MINT32      detachCb(/*IHal3ACb::ECb_T eId, IHal3ACb* pCb*/) = 0;

    virtual MVOID       setFDEnable(MBOOL fgEnable) = 0;

    virtual MBOOL       setFDInfo(MVOID* prFaces) = 0;

    virtual MBOOL       setOTInfo(MVOID* prOT) = 0;

    virtual MINT32      dumpIsp(MINT32 flowType, const MetaSet_T& control, TuningParam* pTuningBuf, MetaSet_T* pResult) = 0;

    virtual MINT32      get(MUINT32 frmId, MetaSet_T& result) = 0;

    virtual MINT32      getCur(MUINT32 frmId, MetaSet_T& result) = 0;

    /**
     * @brief resume 3A
     * @param [in] MagicNum which want to resume 3A
     */
    virtual MVOID      resume(MINT32 MagicNum = 0) = 0;

//LCS Part
    virtual MINT32      InitLCS() = 0;
    virtual MINT32      UninitLCS() = 0;
    virtual MINT32      ConfigLcs() =0;

};

}; // namespace NS3Av3


/**
 * @brief The definition of the maker of IHalISP instance.
 */
typedef NS3Av3::IHalISP* (*HalISP_FACTORY_T)(MINT32 const i4SensorIdx, const char* strUser);
#define MAKE_HalISP(...) \
    MAKE_MTKCAM_MODULE(MTKCAM_MODULE_ID_AAA_HAL_ISP, HalISP_FACTORY_T, __VA_ARGS__)


#endif
