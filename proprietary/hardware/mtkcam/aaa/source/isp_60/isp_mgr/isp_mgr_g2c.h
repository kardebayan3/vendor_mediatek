/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef _ISP_MGR_G2C_H_
#define _ISP_MGR_G2C_H_


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  G2C
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define INIT_G2C_ADDR(reg)\
    INIT_REG_INFO_ADDR_P1_MULTI(EG2C_R1 ,reg, G2C_R1_G2C_);\
    INIT_REG_INFO_ADDR_P1_MULTI(EG2C_R2 ,reg, G2C_R2_G2C_);\
    INIT_REG_INFO_ADDR_P2_MULTI(EG2C_D1 ,reg, G2C_D1A_G2C_)


typedef class ISP_MGR_G2C : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_G2C  MyType;
private:

    enum
    {
        EG2C_R1, //Pass1
        EG2C_R2, //Pass1
        EG2C_D1, //Pass2
        ESubModule_NUM
    };

    MBOOL m_bEnable[ESubModule_NUM];
    MBOOL m_bCCTEnable[ESubModule_NUM];

    enum
    {
        ERegInfo_CONV_0A,
        ERegInfo_CONV_0B,
        ERegInfo_CONV_1A,
        ERegInfo_CONV_1B,
        ERegInfo_CONV_2A,
        ERegInfo_CONV_2B,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ESubModule_NUM][ERegInfo_NUM];

protected:
    ISP_MGR_G2C(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
    {
        for(int i=0; i<ESubModule_NUM; i++){
            m_bEnable[i]    = MFALSE;
            m_bCCTEnable[i] = MFALSE;
            ::memset(m_rIspRegInfo[i], 0, sizeof(RegInfo_T)*ERegInfo_NUM);
        }
        // register info addr init
        INIT_G2C_ADDR(CONV_0A);
        INIT_G2C_ADDR(CONV_0B);
        INIT_G2C_ADDR(CONV_1A);
        INIT_G2C_ADDR(CONV_1B);
        INIT_G2C_ADDR(CONV_2A);
        INIT_G2C_ADDR(CONV_2B);
    }

    virtual ~ISP_MGR_G2C() {}

public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    //template <class ISP_xxx_T>
    //MyType& put(MUINT8 SubModuleIndex, ISP_xxx_T const& rParam);

    //template <class ISP_xxx_T>
    //MyType& get(MUINT8 SubModuleIndex, ISP_xxx_T & rParam);

    MBOOL
    isEnable(MUINT8 SubModuleIndex)
    {
        return m_bEnable[SubModuleIndex];
    }

    MVOID
    setEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bEnable[SubModuleIndex] = bEnable;
    }

    MBOOL
    isCCTEnable(MUINT8 SubModuleIndex)
    {
        return m_bCCTEnable[SubModuleIndex];
    }

    MVOID
    setCCTEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bCCTEnable[SubModuleIndex] = bEnable;
    }

    MBOOL apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex=0);

    MBOOL apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg);

} ISP_MGR_G2C_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_G2C_DEV : public ISP_MGR_G2C_T
{
public:
    static
    ISP_MGR_G2C_T&
    getInstance()
    {
        static ISP_MGR_G2C_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_G2C_DEV()
        : ISP_MGR_G2C_T(eSensorDev)
    {}

    virtual ~ISP_MGR_G2C_DEV() {}

};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  G2CX
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define INIT_G2CX_ADDR(reg)\
    INIT_REG_INFO_ADDR_P2_MULTI(EG2CX_D1 ,reg, G2CX_D1A_G2CX_)


typedef class ISP_MGR_G2CX : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_G2CX  MyType;
private:

    enum
    {
        EG2CX_D1,
        ESubModule_NUM
    };

    MBOOL m_bEnable[ESubModule_NUM];
    MBOOL m_bCCTEnable[ESubModule_NUM];

    enum
    {
        ERegInfo_CONV_0A,
        ERegInfo_CONV_0B,
        ERegInfo_CONV_1A,
        ERegInfo_CONV_1B,
        ERegInfo_CONV_2A,
        ERegInfo_CONV_2B,
        //ERegInfo_SHADE_CON_1,
        //ERegInfo_SHADE_CON_2,
        //ERegInfo_SHADE_CON_3,
        //ERegInfo_SHADE_TAR,
        //ERegInfo_SHADE_SP,
        //ERegInfo_CFC_CON_1,
        //ERegInfo_CFC_CON_2,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ESubModule_NUM][ERegInfo_NUM];

protected:
    ISP_MGR_G2CX(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
    {
        for(int i=0; i<ESubModule_NUM; i++){
            m_bEnable[i]    = MFALSE;
            m_bCCTEnable[i] = MFALSE;
            ::memset(m_rIspRegInfo[i], 0, sizeof(RegInfo_T)*ERegInfo_NUM);
        }
        // register info addr init
        INIT_G2CX_ADDR(CONV_0A);
        INIT_G2CX_ADDR(CONV_0B);
        INIT_G2CX_ADDR(CONV_1A);
        INIT_G2CX_ADDR(CONV_1B);
        INIT_G2CX_ADDR(CONV_2A);
        INIT_G2CX_ADDR(CONV_2B);
        //INIT_G2CX_ADDR(SHADE_CON_1);
        //INIT_G2CX_ADDR(SHADE_CON_2);
        //INIT_G2CX_ADDR(SHADE_CON_3);
        //INIT_G2CX_ADDR(SHADE_TAR);
        //INIT_G2CX_ADDR(SHADE_SP);
        //INIT_G2CX_ADDR(CFC_CON_1);
        //INIT_G2CX_ADDR(CFC_CON_2);
    }

    virtual ~ISP_MGR_G2CX() {}

public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    template <class ISP_xxx_T>
    MyType& put(MUINT8 SubModuleIndex, ISP_xxx_T const& rParam);

    template <class ISP_xxx_T>
    MyType& get(MUINT8 SubModuleIndex, ISP_xxx_T & rParam);

    MVOID
    getDefaultValue(MUINT8 SubModuleIndex, ISP_NVRAM_G2CX_T & rParam)
    {

        switch (SubModuleIndex)
        {
            case EG2CX_D1:
            default:
                rParam.conv_0b.bits.G2CX_Y_OFST = 0;
                rParam.conv_1b.bits.G2CX_U_OFST = 0;
                rParam.conv_2b.bits.G2CX_V_OFST = 0;

                rParam.conv_0a.bits.G2CX_CNV_00 = 153;
                rParam.conv_0a.bits.G2CX_CNV_01 = 301;
                rParam.conv_0b.bits.G2CX_CNV_02 = 58;
                rParam.conv_1a.bits.G2CX_CNV_10 = -86;
                rParam.conv_1a.bits.G2CX_CNV_11 = -170;
                rParam.conv_1b.bits.G2CX_CNV_12 = 256;
                rParam.conv_2a.bits.G2CX_CNV_20 = 256;
                rParam.conv_2a.bits.G2CX_CNV_21 = -214;
                rParam.conv_2b.bits.G2CX_CNV_22 = -42;
        }
    }

    MBOOL
    isEnable(MUINT8 SubModuleIndex)
    {
        return m_bEnable[SubModuleIndex];
    }

    MVOID
    setEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bEnable[SubModuleIndex] = bEnable;
    }

    MBOOL
    isCCTEnable(MUINT8 SubModuleIndex)
    {
        return m_bCCTEnable[SubModuleIndex];
    }

    MVOID
    setCCTEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bCCTEnable[SubModuleIndex] = bEnable;
    }

    MBOOL apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg);

} ISP_MGR_G2CX_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_G2CX_DEV : public ISP_MGR_G2CX_T
{
public:
    static
    ISP_MGR_G2CX_T&
    getInstance()
    {
        static ISP_MGR_G2CX_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_G2CX_DEV()
        : ISP_MGR_G2CX_T(eSensorDev)
    {}

    virtual ~ISP_MGR_G2CX_DEV() {}

};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  C2G
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define INIT_C2G_ADDR(reg)\
    INIT_REG_INFO_ADDR_P2_MULTI(EC2G_D1 ,reg, C2G_D1A_C2G_)


typedef class ISP_MGR_C2G : public ISP_MGR_BASE_T
{
    typedef ISP_MGR_C2G  MyType;
private:

    enum
    {
        EC2G_D1,
        ESubModule_NUM
    };

    MBOOL m_bEnable[ESubModule_NUM];
    MBOOL m_bCCTEnable[ESubModule_NUM];

    enum
    {
        ERegInfo_CONV_0A,
        ERegInfo_CONV_0B,
        ERegInfo_CONV_1A,
        ERegInfo_CONV_1B,
        ERegInfo_CONV_2A,
        ERegInfo_CONV_2B,
        ERegInfo_NUM
    };

    RegInfo_T m_rIspRegInfo[ESubModule_NUM][ERegInfo_NUM];

protected:
    ISP_MGR_C2G(ESensorDev_T const eSensorDev)
        : ISP_MGR_BASE_T(m_rIspRegInfo, ERegInfo_NUM, eSensorDev)
    {
        for(int i=0; i<ESubModule_NUM; i++){
            m_bEnable[i]    = MFALSE;
            m_bCCTEnable[i] = MFALSE;
            ::memset(m_rIspRegInfo[i], 0, sizeof(RegInfo_T)*ERegInfo_NUM);
        }
        // register info addr init
        INIT_C2G_ADDR(CONV_0A);
        INIT_C2G_ADDR(CONV_0B);
        INIT_C2G_ADDR(CONV_1A);
        INIT_C2G_ADDR(CONV_1B);
        INIT_C2G_ADDR(CONV_2A);
        INIT_C2G_ADDR(CONV_2B);
    }

    virtual ~ISP_MGR_C2G() {}

public:
    static MyType&  getInstance(ESensorDev_T const eSensorDev);

public: // Interfaces.

    //template <class ISP_xxx_T>
    //MyType& put(MUINT8 SubModuleIndex, ISP_xxx_T const& rParam);

    //template <class ISP_xxx_T>
    //MyType& get(MUINT8 SubModuleIndex, ISP_xxx_T & rParam);

    MBOOL
    isEnable(MUINT8 SubModuleIndex)
    {
        return m_bEnable[SubModuleIndex];
    }

    MVOID
    setEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bEnable[SubModuleIndex] = bEnable;
    }

    MBOOL
    isCCTEnable(MUINT8 SubModuleIndex)
    {
        return m_bCCTEnable[SubModuleIndex];
    }

    MVOID
    setCCTEnable(MUINT8 SubModuleIndex, MBOOL bEnable)
    {
        m_bCCTEnable[SubModuleIndex] = bEnable;
    }

    MBOOL apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg);

} ISP_MGR_C2G_T;

template <ESensorDev_T const eSensorDev>
class ISP_MGR_C2G_DEV : public ISP_MGR_C2G_T
{
public:
    static
    ISP_MGR_C2G_T&
    getInstance()
    {
        static ISP_MGR_C2G_DEV<eSensorDev> singleton;
        return singleton;
    }
    virtual MVOID destroyInstance() {}

    ISP_MGR_C2G_DEV()
        : ISP_MGR_C2G_T(eSensorDev)
    {}

    virtual ~ISP_MGR_C2G_DEV() {}

};

#endif

