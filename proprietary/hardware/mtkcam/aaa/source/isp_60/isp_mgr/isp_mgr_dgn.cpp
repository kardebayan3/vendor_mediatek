/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_mgr_dgn"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include "isp_mgr.h"

namespace NSIspTuningv3
{
#define CLAMP(x,min,max)       (((x) > (max)) ? (max) : (((x) < (min)) ? (min) : (x)))

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DGN
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_DGN_T&
ISP_MGR_DGN_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_DGN_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_DGN_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_DGN_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_DGN_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_DGN_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_DGN_T&
ISP_MGR_DGN_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_DGN_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, GN0,             gn0);
    PUT_REG_INFO_MULTI(SubModuleIndex, GN1,             gn1);
    PUT_REG_INFO_MULTI(SubModuleIndex, GN2,             gn2);
    PUT_REG_INFO_MULTI(SubModuleIndex, GN3,             gn3);
    PUT_REG_INFO_MULTI(SubModuleIndex, LINK,            link);

    return  (*this);
}


template <>
ISP_MGR_DGN_T&
ISP_MGR_DGN_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_DGN_T & rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, GN0,             gn0);
    PUT_REG_INFO_MULTI(SubModuleIndex, GN1,             gn1);
    PUT_REG_INFO_MULTI(SubModuleIndex, GN2,             gn2);
    PUT_REG_INFO_MULTI(SubModuleIndex, GN3,             gn3);
    PUT_REG_INFO_MULTI(SubModuleIndex, LINK,            link);

    return  (*this);
}

MBOOL
ISP_MGR_DGN_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{
#if 0
    MBOOL bDGN_EN = isEnable();

    // Merge AE gain
    MINT32 OBGAIN0 = (reinterpret_cast<ISP_CAM_DGN_GAIN0_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN0])->DGN_GAIN_B * m_u4IspAEGain + 256) >> 9;
    MINT32 OBGAIN1 = (reinterpret_cast<ISP_CAM_DGN_GAIN1_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN1])->DGN_GAIN_GR * m_u4IspAEGain + 256) >> 9;
    MINT32 OBGAIN2 = (reinterpret_cast<ISP_CAM_DGN_GAIN2_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN2])->DGN_GAIN_GB * m_u4IspAEGain + 256) >> 9;
    MINT32 OBGAIN3 = (reinterpret_cast<ISP_CAM_DGN_GAIN3_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN3])->DGN_GAIN_R * m_u4IspAEGain + 256) >> 9;

    reinterpret_cast<ISP_CAM_DGN_GAIN0_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN0))->DGN_GAIN_B = CLAMP(OBGAIN0, 0, 8191);
    reinterpret_cast<ISP_CAM_DGN_GAIN1_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN1))->DGN_GAIN_GR = CLAMP(OBGAIN1, 0, 8191);
    reinterpret_cast<ISP_CAM_DGN_GAIN2_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN2))->DGN_GAIN_GB = CLAMP(OBGAIN2, 0, 8191);
    reinterpret_cast<ISP_CAM_DGN_GAIN3_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN3))->DGN_GAIN_R = CLAMP(OBGAIN3, 0, 8191);

    REG_INFO_VALUE(CAM_DGN_OFFST0) = m_rRegInput[ERegInfo_CAM_DGN_OFFST0];
    REG_INFO_VALUE(CAM_DGN_OFFST1) = m_rRegInput[ERegInfo_CAM_DGN_OFFST1];
    REG_INFO_VALUE(CAM_DGN_OFFST2) = m_rRegInput[ERegInfo_CAM_DGN_OFFST2];
    REG_INFO_VALUE(CAM_DGN_OFFST3) = m_rRegInput[ERegInfo_CAM_DGN_OFFST3];



    // Top Control
    rTuning.enableEngine(eTuningMgrFunc_DGN, bDGN_EN, i4SubsampleIdex);

    // Update
    rTuning.tuningEngine(eTuningMgrFunc_DGN,( bDGN_EN &&(!rRawIspCamInfo.rAEInfo.bDisableDGN) ), i4SubsampleIdex);
    //rTuning.tuningEngine(eTuningMgrFunc_DGN,1, i4SubsampleIdex);

//    rRawIspCamInfo.rDGN1.offst0.val = REG_INFO_VALUE(CAM_DGN_OFFST0);
//    rRawIspCamInfo.rDGN1.offst1.val = REG_INFO_VALUE(CAM_DGN_OFFST1);
//    rRawIspCamInfo.rDGN1.offst2.val = REG_INFO_VALUE(CAM_DGN_OFFST2);
//    rRawIspCamInfo.rDGN1.offst3.val = REG_INFO_VALUE(CAM_DGN_OFFST3);
//    rRawIspCamInfo.rDGN1.gain0.val  = REG_INFO_VALUE(CAM_DGN_GAIN0);
//    rRawIspCamInfo.rDGN1.gain1.val  = REG_INFO_VALUE(CAM_DGN_GAIN1);
//    rRawIspCamInfo.rDGN1.gain2.val  = REG_INFO_VALUE(CAM_DGN_GAIN2);
//    rRawIspCamInfo.rDGN1.gain3.val  = REG_INFO_VALUE(CAM_DGN_GAIN3);

    //get member value to rRawIspCamInfo.rDGN1
    get(rRawIspCamInfo.rDGN1);



#if 0
    rTuning.updateEngine(eTuningMgrFunc_DGN, bDGN_EN, i4SubsampleIdex);

        // TOP
    TUNING_MGR_WRITE_BITS_CAM((&rTuning), CAM_CTL_EN, DGN_EN, bDGN_EN, i4SubsampleIdex);
#endif

    ISP_MGR_CTL_EN_P1_T::getInstance(m_eSensorDev).setEnable_OB(bDGN_EN);

        // Register setting
    if (!rRawIspCamInfo.rAEInfo.bDisableDGN)
    rTuning.tuningMgrWriteRegs(
        static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_pRegInfo),
        m_u4RegInfoNum, i4SubsampleIdex);

    dumpRegInfoP1("DGN");
#endif
    return  MTRUE;
}

MBOOL
ISP_MGR_DGN_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{
#if 0
    MBOOL bDGN_EN = isEnable();

    // Merge AE gain
    MINT32 OBGAIN0 = (reinterpret_cast<ISP_CAM_DGN_GAIN0_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN0])->DGN_GAIN_B * m_u4IspAEGain + 256) >> 9;
    MINT32 OBGAIN1 = (reinterpret_cast<ISP_CAM_DGN_GAIN1_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN1])->DGN_GAIN_GR * m_u4IspAEGain + 256) >> 9;
    MINT32 OBGAIN2 = (reinterpret_cast<ISP_CAM_DGN_GAIN2_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN2])->DGN_GAIN_GB * m_u4IspAEGain + 256) >> 9;
    MINT32 OBGAIN3 = (reinterpret_cast<ISP_CAM_DGN_GAIN3_T*>(&m_rRegInput[ERegInfo_CAM_DGN_GAIN3])->DGN_GAIN_R * m_u4IspAEGain + 256) >> 9;

    reinterpret_cast<ISP_CAM_DGN_GAIN0_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN0))->DGN_GAIN_B = CLAMP(OBGAIN0, 0, 8191);
    reinterpret_cast<ISP_CAM_DGN_GAIN1_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN1))->DGN_GAIN_GR = CLAMP(OBGAIN1, 0, 8191);
    reinterpret_cast<ISP_CAM_DGN_GAIN2_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN2))->DGN_GAIN_GB = CLAMP(OBGAIN2, 0, 8191);
    reinterpret_cast<ISP_CAM_DGN_GAIN3_T*>(REG_INFO_VALUE_PTR(CAM_DGN_GAIN3))->DGN_GAIN_R = CLAMP(OBGAIN3, 0, 8191);

    REG_INFO_VALUE(CAM_DGN_OFFST0) = m_rRegInput[ERegInfo_CAM_DGN_OFFST0];
    REG_INFO_VALUE(CAM_DGN_OFFST1) = m_rRegInput[ERegInfo_CAM_DGN_OFFST1];
    REG_INFO_VALUE(CAM_DGN_OFFST2) = m_rRegInput[ERegInfo_CAM_DGN_OFFST2];
    REG_INFO_VALUE(CAM_DGN_OFFST3) = m_rRegInput[ERegInfo_CAM_DGN_OFFST3];



    // Top Control
    rTuning.enableEngine(eTuningMgrFunc_DGN, bDGN_EN, i4SubsampleIdex);

    // Update
    rTuning.tuningEngine(eTuningMgrFunc_DGN,( bDGN_EN &&(!rRawIspCamInfo.rAEInfo.bDisableDGN) ), i4SubsampleIdex);
    //rTuning.tuningEngine(eTuningMgrFunc_DGN,1, i4SubsampleIdex);

//    rRawIspCamInfo.rDGN1.offst0.val = REG_INFO_VALUE(CAM_DGN_OFFST0);
//    rRawIspCamInfo.rDGN1.offst1.val = REG_INFO_VALUE(CAM_DGN_OFFST1);
//    rRawIspCamInfo.rDGN1.offst2.val = REG_INFO_VALUE(CAM_DGN_OFFST2);
//    rRawIspCamInfo.rDGN1.offst3.val = REG_INFO_VALUE(CAM_DGN_OFFST3);
//    rRawIspCamInfo.rDGN1.gain0.val  = REG_INFO_VALUE(CAM_DGN_GAIN0);
//    rRawIspCamInfo.rDGN1.gain1.val  = REG_INFO_VALUE(CAM_DGN_GAIN1);
//    rRawIspCamInfo.rDGN1.gain2.val  = REG_INFO_VALUE(CAM_DGN_GAIN2);
//    rRawIspCamInfo.rDGN1.gain3.val  = REG_INFO_VALUE(CAM_DGN_GAIN3);

    //get member value to rRawIspCamInfo.rDGN1
    get(rRawIspCamInfo.rDGN1);



#if 0
    rTuning.updateEngine(eTuningMgrFunc_DGN, bDGN_EN, i4SubsampleIdex);

        // TOP
    TUNING_MGR_WRITE_BITS_CAM((&rTuning), CAM_CTL_EN, DGN_EN, bDGN_EN, i4SubsampleIdex);
#endif

    ISP_MGR_CTL_EN_P1_T::getInstance(m_eSensorDev).setEnable_OB(bDGN_EN);

        // Register setting
    if (!rRawIspCamInfo.rAEInfo.bDisableDGN)
    rTuning.tuningMgrWriteRegs(
        static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_pRegInfo),
        m_u4RegInfoNum, i4SubsampleIdex);

    dumpRegInfoP1("DGN");
#endif
    return  MTRUE;
}




}
