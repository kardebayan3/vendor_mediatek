/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
/**
 * @file af_hw_config.h
 * @brief AF isp hardware config.
 */
#ifndef _ISP_AF_CONFIG_H_
#define _ISP_AF_CONFIG_H_
#include <ResultPoolBase.h>
#include <stdio.h>
#include <string.h>

using namespace NS3Av3;
using namespace std;

enum
{
    ERegInfo_AFO_R1_AFO_XSIZE,
    ERegInfo_AFO_R1_AFO_YSIZE,
    // ERegInfo_AFO_R1_AFO_STRIDE,

    ERegInfo_AF_R1_AF_CON,
    ERegInfo_AF_R1_AF_CON2,
    ERegInfo_AF_R1_AF_SIZE,
    ERegInfo_AF_R1_AF_VLD,
    ERegInfo_AF_R1_AF_BLK_PROT,
    ERegInfo_AF_R1_AF_BLK_0,
    ERegInfo_AF_R1_AF_BLK_1,
    ERegInfo_AF_R1_AF_HFLT0_1,
    ERegInfo_AF_R1_AF_HFLT0_2,
    ERegInfo_AF_R1_AF_HFLT0_3,
    ERegInfo_AF_R1_AF_HFLT1_1,
    ERegInfo_AF_R1_AF_HFLT1_2,
    ERegInfo_AF_R1_AF_HFLT1_3,
    ERegInfo_AF_R1_AF_HFLT2_1,
    ERegInfo_AF_R1_AF_HFLT2_2,
    ERegInfo_AF_R1_AF_HFLT2_3,
    ERegInfo_AF_R1_AF_VFLT_1,
    ERegInfo_AF_R1_AF_VFLT_2,
    ERegInfo_AF_R1_AF_VFLT_3,
    ERegInfo_AF_R1_AF_PL_HFLT_1,
    ERegInfo_AF_R1_AF_PL_HFLT_2,
    ERegInfo_AF_R1_AF_PL_HFLT_3,
    ERegInfo_AF_R1_AF_PL_VFLT_1,
    ERegInfo_AF_R1_AF_PL_VFLT_2,
    ERegInfo_AF_R1_AF_PL_VFLT_3,
    ERegInfo_AF_R1_AF_TH_0,
    ERegInfo_AF_R1_AF_TH_1,
    ERegInfo_AF_R1_AF_TH_2,
    ERegInfo_AF_R1_AF_TH_3,
    ERegInfo_AF_R1_AF_TH_4,
    ERegInfo_AF_R1_AF_LUT_H0_0,
    ERegInfo_AF_R1_AF_LUT_H0_1,
    ERegInfo_AF_R1_AF_LUT_H0_2,
    ERegInfo_AF_R1_AF_LUT_H0_3,
    ERegInfo_AF_R1_AF_LUT_H0_4,
    ERegInfo_AF_R1_AF_LUT_H1_0,
    ERegInfo_AF_R1_AF_LUT_H1_1,
    ERegInfo_AF_R1_AF_LUT_H1_2,
    ERegInfo_AF_R1_AF_LUT_H1_3,
    ERegInfo_AF_R1_AF_LUT_H1_4,
    ERegInfo_AF_R1_AF_LUT_H2_0,
    ERegInfo_AF_R1_AF_LUT_H2_1,
    ERegInfo_AF_R1_AF_LUT_H2_2,
    ERegInfo_AF_R1_AF_LUT_H2_3,
    ERegInfo_AF_R1_AF_LUT_H2_4,
    ERegInfo_AF_R1_AF_LUT_V_0,
    ERegInfo_AF_R1_AF_LUT_V_1,
    ERegInfo_AF_R1_AF_LUT_V_2,
    ERegInfo_AF_R1_AF_LUT_V_3,
    ERegInfo_AF_R1_AF_LUT_V_4,
    ERegInfo_AF_R1_AF_SGG1_0,
    ERegInfo_AF_R1_AF_SGG1_1,
    ERegInfo_AF_R1_AF_SGG1_2,
    ERegInfo_AF_R1_AF_SGG1_3,
    ERegInfo_AF_R1_AF_SGG1_4,
    ERegInfo_AF_R1_AF_SGG1_5,
    ERegInfo_AF_R1_AF_SGG5_0,
    ERegInfo_AF_R1_AF_SGG5_1,
    ERegInfo_AF_R1_AF_SGG5_2,
    ERegInfo_AF_R1_AF_SGG5_3,
    ERegInfo_AF_R1_AF_SGG5_4,
    ERegInfo_AF_R1_AF_SGG5_5,
    ERegInfo_AF_R1_AF_PL_STAT_0,
    ERegInfo_AF_R1_AF_PL_STAT_1,

    EAFRegInfo_NUM
};

typedef struct AFResultConfig_t : public ResultPoolBase_T
{
    MBOOL   enableAFHw;
    MBOOL   isApplied;
    MINT32  configNum;
    MUINT32 rAFRegInfo[EAFRegInfo_NUM];
    AFResultConfig_t()
    {
        enableAFHw = 0;
        isApplied  = 0;
        configNum  = 0;

        ::memset(rAFRegInfo, 0, EAFRegInfo_NUM*sizeof(MUINT32));
    }

    MVOID clearMember()
    {
        AFResultConfig_t();
    }

    // copy operator of AFConfigResult_T
    AFResultConfig_t& operator = (const AFResultConfig_t& o)
    {
        ::memcpy(this, &o, sizeof(AFResultConfig_t));
        return *this;
    }

    // move operator of AFConfigResult_T
    AFResultConfig_t& operator = (AFResultConfig_t&& o) = default;

    virtual ~AFResultConfig_t() = default;

    // Get Id
    virtual int getId() const { return E_AF_CONFIGRESULTTOISP; }

    //----------------------------------------------------------
    // re-implement of ResultPoolBase_T
    //----------------------------------------------------------
    // override copy operator of ResultPoolBase_T
    virtual ResultPoolBase_T& operator =(const ResultPoolBase_T& o) override
    {
        _check_id(o.getId());
        typedef std::remove_pointer<decltype(this)>::type MY_TYPE;

        // be careful, if o is not the type of AFConfigResult, it happens UB
        *this = *static_cast<const MY_TYPE*>(&o);
        return *this;
    }

    // override move operator of ResultPoolBase_T
    virtual ResultPoolBase_T& operator=(ResultPoolBase_T && o) override
    {
        _check_id(o.getId());
        typedef std::remove_pointer<decltype(this)>::type MY_TYPE;

        // be careful, if o is not the type of AFConfigResult, it happens UB
        *this = std::move(*static_cast<const MY_TYPE*>(&o));

        return *this;
    }
} AFResultConfig_T;


#endif // _ISP_AF_CONFIG_H_
