/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_af_config"

#ifndef ENABLE_MY_LOG
#define ENABLE_MY_LOG       (1)
#endif

#include <aaa_types.h>       /* DataType, Ex : MINT32 */
#include <mtkcam/utils/std/Log.h>
#include <af_param.h>
#include <af_config.h>

#include <drv/isp_reg.h>

#include <isp_tuning.h>      /* Enum, Ex : ESensorDev_T */

using namespace NS3Av3;
using namespace NSIspTuning; /* Ex: ESensorDev_Main */

#define REG_AF_INFO(REG, VALUE) \
    { \
        pAFRegInfo[ERegInfo_##REG] = VALUE; \
    }

#define REG_INFO_VALUE(REG) (pAFRegInfo[ERegInfo_##REG].val)

#define CHECK_BLK_NUM_X(InHWBlkNumX) ((InHWBlkNumX < MIN_AF_HW_WIN_X) ? MIN_AF_HW_WIN_X : ((InHWBlkNumX > MAX_AF_HW_WIN_X) ? MAX_AF_HW_WIN_X : InHWBlkNumX))
#define CHECK_BLK_NUM_Y(InHWBlkNumY) ((InHWBlkNumY < MIN_AF_HW_WIN_Y) ? MIN_AF_HW_WIN_Y : ((InHWBlkNumY > MAX_AF_HW_WIN_Y) ? MAX_AF_HW_WIN_Y : InHWBlkNumY))
/* AFConfig end */

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// AF Statistics Config
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_AF_CONFIG_T&
ISP_AF_CONFIG_T::
getInstance(MINT32 const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_AF_CONFIG_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_AF_CONFIG_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_AF_CONFIG_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Sub Second Sensor
        return  ISP_AF_CONFIG_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev = %d", eSensorDev);
        return  ISP_AF_CONFIG_DEV<ESensorDev_Main>::getInstance();
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID ISP_AF_CONFIG_T::resetParam()
{
    m_bIsAFSupport = 0;
    m_u4ConfigNum  = 0;
    memset(m_rAFRegInfoPre, 0, EAFRegInfo_NUM*sizeof(MUINT32));
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID ISP_AF_CONFIG_T::AFConfig(AF_CONFIG_T *a_sAFConfig, AF_CONFIG_INFO_T *a_sOutAFInfo, AFResultConfig_T *p_sAFResultConfig)
{
    MUINT32 *pAFRegInfo = p_sAFResultConfig->rAFRegInfo;
    MINT32  BIN_SZ_W = a_sAFConfig->sBIN_SZ.i4W;
    MINT32  BIN_SZ_H = a_sAFConfig->sBIN_SZ.i4H;
    MINT32  TG_SZ_W  = a_sAFConfig->sTG_SZ.i4W;
    MINT32  TG_SZ_H  = a_sAFConfig->sTG_SZ.i4H;

    if ((BIN_SZ_W == 0) || (BIN_SZ_H == 0))
    {
        // setCamScenarioMode() and ISP_AF_CONFIG_T::config() are called at the same time.
        // sBIN_SZ will be zero.
        return;
    }
#if 0 // fix build error
    MUINT32 af_v_gonly   = a_sAFConfig->AF_V_GONLY;
    MUINT32 af_v_avg_lvl = a_sAFConfig->AF_V_AVG_LVL;

    /**************************     REG_AF_R1_AF_CON     ********************************/
    REG_AF_R1_AF_CON reg_af_con;
    reg_af_con.Raw = 0x200000; //default value
    reg_af_con.Bits.AF_BLF_EN       = 0;
    reg_af_con.Bits.AF_BLF_D_LVL    = 0;
    reg_af_con.Bits.AF_BLF_R_LVL    = 0;
    reg_af_con.Bits.AF_BLF_VFIR_MUX = 0;
    reg_af_con.Bits.AF_H_GONLY      = 0;
    reg_af_con.Bits.AF_V_GONLY      = 0;
    reg_af_con.Bits.AF_V_AVG_LVL    = 0;
    reg_af_con.Bits.AF_VFLT_MODE    = 0;
    *(a_sOutAFInfo->hwEnExtMode)    = a_sAFConfig->AF_EXT_ENABLE; /* remove AF_EXT_STAT_EN since ISP6.0 */


    //constraint : if AF_EXT_STAT_EN is TRUE, AF_H_GONLY must be 0
    reg_af_con.Bits.AF_H_GONLY      = 0;

    /**************************     REG_AF_R1_AF_CON2     ********************************/
    REG_AF_R1_AF_CON2 reg_af_con2;
    reg_af_con2.Raw = 0x0; //default value
    reg_af_con2.Bits.AF_DS_EN            = 0;
    reg_af_con2.Bits.AF_H_FV0_ABS        = 0;
    reg_af_con2.Bits.AF_H_FV1_ABS        = 0;
    reg_af_con2.Bits.AF_H_FV2_ABS        = 0;
    reg_af_con2.Bits.AF_V_FV0_ABS        = 0;
    reg_af_con2.Bits.AF_PL_H_FV_ABS      = 0;
    reg_af_con2.Bits.AF_PL_V_FV_ABS      = 0;
    reg_af_con2.Bits.AF_H_FV0_EN         = 0;
    reg_af_con2.Bits.AF_H_FV1_EN         = 0;
    reg_af_con2.Bits.AF_H_FV2_EN         = 0;
    reg_af_con2.Bits.AF_V_FV0_EN         = 0;
    reg_af_con2.Bits.AF_PL_FV_EN         = 0;
    reg_af_con2.Bits.AF_8BIT_LOWPOWER_EN = 0;
    reg_af_con2.Bits.AF_PL_BITSEL        = 0;

    //constraint
    if (BIN_SZ_W < 55)
        reg_af_con2.Bits.AF_DS_EN = 0;

    /**************************     REG_AF_R1_AF_SIZE     ********************************/
    REG_AF_R1_AF_SIZE reg_af_size;
    reg_af_size.Raw = 0;
    reg_af_size.Bits.AF_IMAGE_WD = BIN_SZ_W; /*case for no frontal binning*/

    /*************************     Configure ROI setting     *******************************/
    // Convert ROI coordinate from TG coordinate to BIN block coordinate. (AREA_T -> af_param.h)
    AREA_T Roi2HWCoord = AREA_T(a_sAFConfig->sRoi.i4X * BIN_SZ_W / TG_SZ_W,
                                a_sAFConfig->sRoi.i4Y * BIN_SZ_H / TG_SZ_H,
                                a_sAFConfig->sRoi.i4W * BIN_SZ_W / TG_SZ_W,
                                a_sAFConfig->sRoi.i4H * BIN_SZ_H / TG_SZ_H,
                                0);
    //min constraint
    MUINT32 minHSz=8;
    MUINT32 minVSz=1;
    if(     (af_v_avg_lvl == 3) && (af_v_gonly == 1)) minHSz = 32;
    else if((af_v_avg_lvl == 3) && (af_v_gonly == 0)) minHSz = 16;
    else if((af_v_avg_lvl == 2) && (af_v_gonly == 1)) minHSz = 16;
    else                                              minHSz =  8;

    // ROI boundary check :
    if ((Roi2HWCoord.i4X < 0) ||
        (Roi2HWCoord.i4X > BIN_SZ_W) ||
        (Roi2HWCoord.i4W < (MINT32)minHSz) ||
        (BIN_SZ_W < (Roi2HWCoord.i4X + Roi2HWCoord.i4W))) /*X*/
    {
        MINT32 x = BIN_SZ_W * 30 / 100;
        MINT32 w = BIN_SZ_W * 40 / 100;
        CAM_LOGD("HW-%s WAR : X %d->%d, W %d->%d", __FUNCTION__, Roi2HWCoord.i4X, x, Roi2HWCoord.i4W, w);
        Roi2HWCoord.i4X = x;
        Roi2HWCoord.i4W = w;
    }
    if ((Roi2HWCoord.i4Y < 0) ||
        (Roi2HWCoord.i4Y > BIN_SZ_H) ||
        (Roi2HWCoord.i4H < (MINT32)minVSz) ||
        (BIN_SZ_H < (Roi2HWCoord.i4Y + Roi2HWCoord.i4H))) /*Y*/
    {
        MINT32 y = BIN_SZ_H * 30 / 100;
        MINT32 h = BIN_SZ_H * 40 / 100;
        CAM_LOGD("HW-%s WAR : Y %d->%d, H %d->%d", __FUNCTION__, Roi2HWCoord.i4Y, y, Roi2HWCoord.i4H, h);
        Roi2HWCoord.i4Y = y;
        Roi2HWCoord.i4H = h;
    }
    AREA_T Roi2HWCoordTmp = Roi2HWCoord;

    /**************************     REG_AF_R1_AF_BLK_0     ******************************/
    REG_AF_R1_AF_BLK_0 reg_af_blk_0;
    reg_af_blk_0.Raw = 0;

    //-------------
    // AF block sz width
    //-------------
    *(a_sOutAFInfo->hwBlkNumX) = CHECK_BLK_NUM_X(a_sAFConfig->AF_BLK_XNUM);

    MUINT32 win_h_size = Roi2HWCoord.i4W / *(a_sOutAFInfo->hwBlkNumX);
    if( win_h_size > 40)
    {
        win_h_size = 40; //constraint for twin driver
        MINT32 nh = Roi2HWCoord.i4W / win_h_size;
        CAM_LOGD("HW-%s WAR : BlkSzW set to 40, blkNumX %d->%d", __FUNCTION__, *(a_sOutAFInfo->hwBlkNumX), nh);

        //Because block size is changed, check blcok number again.
        *(a_sOutAFInfo->hwBlkNumX) = CHECK_BLK_NUM_X(nh);
    }
    else if (win_h_size < minHSz)
    {
        win_h_size = minHSz;
        MINT32 nh = Roi2HWCoord.i4W / win_h_size;
        CAM_LOGD("HW-%s WAR : BlkSzW set to min Sz %d, (%d,%d), blkNumX %d->%d", __FUNCTION__, minHSz, af_v_avg_lvl, af_v_gonly, *(a_sOutAFInfo->hwBlkNumX), nh);

        //Because block size is changed, check blcok number again.
        *(a_sOutAFInfo->hwBlkNumX) = CHECK_BLK_NUM_X(nh);
    }
    if (af_v_gonly == 1)
        win_h_size = win_h_size / 4 * 4;
    else
        win_h_size = win_h_size / 2 * 2;

    //-------------
    // AF block sz height
    //-------------
    *(a_sOutAFInfo->hwBlkNumY) = CHECK_BLK_NUM_Y(a_sAFConfig->AF_BLK_YNUM);

    MUINT32 win_v_size = Roi2HWCoord.i4H / *(a_sOutAFInfo->hwBlkNumY);
    if (win_v_size > 128)
    {
        win_v_size = 128;
        MINT32 nv = Roi2HWCoord.i4H / win_v_size;
        CAM_LOGD("HW-%s WAR : BlkSzH set to 128, blkNumY %d->%d", __FUNCTION__, *(a_sOutAFInfo->hwBlkNumY), nv);

        //Because block size is changed, check blcok number again.
        *(a_sOutAFInfo->hwBlkNumY) = CHECK_BLK_NUM_Y(nv);
    }
    else if (win_v_size < minVSz)
    {
        win_v_size = minVSz;
        MINT32 nv = Roi2HWCoord.i4H / win_v_size;
        CAM_LOGD("HW-%s WAR : BlkSzH set to min Sz 1, blkNumY %d->%d", __FUNCTION__, *(a_sOutAFInfo->hwBlkNumY), nv);

        //Because block size is changed, check blcok number again.
        *(a_sOutAFInfo->hwBlkNumY) = CHECK_BLK_NUM_Y(nv);
    }

    //-------------
    // Set AF block size.
    //-------------
    *(a_sOutAFInfo->hwBlkSizeX) = win_h_size;
    *(a_sOutAFInfo->hwBlkSizeY) = win_v_size;
    reg_af_blk_0.Bits.AF_BLK_XSIZE = win_h_size;
    reg_af_blk_0.Bits.AF_BLK_YSIZE = win_v_size;

    /**************************     REG_AF_R1_AF_VLD     ********************************/
    REG_AF_R1_AF_VLD reg_af_vld;
    reg_af_vld.Raw = 0;

    //-------------
    // Final HW ROI.
    //-------------
    Roi2HWCoord.i4W = *(a_sOutAFInfo->hwBlkNumX) * win_h_size;
    Roi2HWCoord.i4H = *(a_sOutAFInfo->hwBlkNumY) * win_v_size;
    Roi2HWCoord.i4X = Roi2HWCoordTmp.i4X + (Roi2HWCoordTmp.i4W / 2) - (Roi2HWCoord.i4W / 2);
    Roi2HWCoord.i4Y = Roi2HWCoordTmp.i4Y + (Roi2HWCoordTmp.i4H / 2) - (Roi2HWCoord.i4H / 2);

    //-------------
    // HW ROI Size checking.
    //-------------
    if (BIN_SZ_W < (MINT32)(Roi2HWCoord.i4X + (*(a_sOutAFInfo->hwBlkNumX) * win_h_size)))
    {
        MINT32 x = BIN_SZ_W - *(a_sOutAFInfo->hwBlkNumX) * win_h_size;
        CAM_LOGD("HW-%s WAR : X %d->%d", __FUNCTION__, Roi2HWCoord.i4X, x);
        Roi2HWCoord.i4X = x;
    }

    if( BIN_SZ_H < (MINT32)(Roi2HWCoord.i4Y + (*(a_sOutAFInfo->hwBlkNumY) * win_v_size)))
    {
        MINT32 y = BIN_SZ_H - *(a_sOutAFInfo->hwBlkNumY) * win_v_size;
        CAM_LOGD("HW-%s WAR : Y %d->%d", __FUNCTION__, Roi2HWCoord.i4Y, y);
        Roi2HWCoord.i4Y = y;
    }

    //constraint : The window start point must be multiples of 2
    Roi2HWCoord.i4X = (Roi2HWCoord.i4X / 2) * 2;
    Roi2HWCoord.i4Y = (Roi2HWCoord.i4Y / 2) * 2;

    reg_af_vld.Bits.AF_VLD_XSTART = Roi2HWCoord.i4X;
    reg_af_vld.Bits.AF_VLD_YSTART = Roi2HWCoord.i4Y;


    /**************************     REG_AF_R1_AF_BLK_1     ******************************/
    REG_AF_R1_AF_BLK_1 reg_af_blk_1;
    reg_af_blk_1.Raw = 0;
    //window num
    reg_af_blk_1.Bits.AF_BLK_XNUM = *(a_sOutAFInfo->hwBlkNumX);
    reg_af_blk_1.Bits.AF_BLK_YNUM = *(a_sOutAFInfo->hwBlkNumY);

    // error check : should not be happened.
    if (BIN_SZ_W < (reg_af_vld.Bits.AF_VLD_XSTART + reg_af_blk_1.Bits.AF_BLK_XNUM * reg_af_blk_0.Bits.AF_BLK_XSIZE))
    {
        CAM_LOGE("HW-Configure AF ROI fail : [StartX] %d, [NumX] %d, [BlkSzX] %d, [ImgSzX] %d",
                 reg_af_vld.Bits.AF_VLD_XSTART,
                 reg_af_blk_1.Bits.AF_BLK_XNUM,
                 reg_af_blk_0.Bits.AF_BLK_XSIZE,
                 BIN_SZ_W);
    }
    if (BIN_SZ_H < (reg_af_vld.Bits.AF_VLD_YSTART + reg_af_blk_1.Bits.AF_BLK_YNUM * reg_af_blk_0.Bits.AF_BLK_YSIZE))
    {
        CAM_LOGE("HW-Configure AF ROI fail : [StartY] %d, [NumY] %d, [BlkSzY] %d, [ImgSzY] %d",
                 reg_af_vld.Bits.AF_VLD_YSTART,
                 reg_af_blk_1.Bits.AF_BLK_YNUM,
                 reg_af_blk_0.Bits.AF_BLK_YSIZE,
                 BIN_SZ_H);
    }

    #if 0
    CAM_LOGD_IF( /*m_bDebugEnable*/ 1, "ROI-BIN : [X]%4d [Y]%4d [W]%4d [H]%4d -> [X]%4d [Y]%4d [W]%4d [H]%4d",
                 Roi2HWCoordTmp.i4X,
                 Roi2HWCoordTmp.i4Y,
                 Roi2HWCoordTmp.i4W,
                 Roi2HWCoordTmp.i4H,
                 Roi2HWCoord.i4X,
                 Roi2HWCoord.i4Y,
                 Roi2HWCoord.i4W,
                 Roi2HWCoord.i4H);
    #endif

    // Now Roi2HWCoordTmp is the AF region which is output from algorithm.
    *(a_sOutAFInfo->hwArea) = AREA_T(Roi2HWCoord.i4X * TG_SZ_W / BIN_SZ_W,
                       Roi2HWCoord.i4Y * TG_SZ_H / BIN_SZ_H,
                       Roi2HWCoord.i4W * TG_SZ_W / BIN_SZ_W,
                       Roi2HWCoord.i4H * TG_SZ_H / BIN_SZ_H,
                       0);

    #if 0
    CAM_LOGD_IF( /*m_bDebugEnable*/ 1, "ROI-TG  : [X]%4d [Y]%4d [W]%4d [H]%4d -> [X]%4d [Y]%4d [W]%4d [H]%4d",
           a_sAFConfig->sRoi.i4X,
           a_sAFConfig->sRoi.i4Y,
           a_sAFConfig->sRoi.i4W,
           a_sAFConfig->sRoi.i4H,
           a_sOutAFInfo->hwArea.i4X,
           a_sOutAFInfo->hwArea.i4Y,
           a_sOutAFInfo->hwArea.i4W,
           a_sOutAFInfo->hwArea.i4H);
    #endif

    /**************************     REG_AF_R1_AF_TH_0     ******************************/
    REG_AF_R1_AF_TH_0 reg_af_th_0;
    reg_af_th_0.Raw = 0;
    reg_af_th_0.Bits.AF_H_TH_0 = a_sAFConfig->AF_TH_H[0];
    reg_af_th_0.Bits.AF_H_TH_1 = a_sAFConfig->AF_TH_H[1];
    /**************************     REG_AF_R1_AF_TH_1     ******************************/
    REG_AF_R1_AF_TH_1 reg_af_th_1;
    reg_af_th_1.Raw = 0;
    reg_af_th_1.Bits.AF_V_TH   = 0;
    reg_af_th_1.Bits.AF_H_TH_2 = 0;
    /**************************     REG_AF_R1_AF_TH_2     ******************************/
    REG_AF_R1_AF_TH_2 reg_af_th_2;
    reg_af_th_2.Raw = 0;
    reg_af_th_2.Bits.AF_PL_H_TH = 0;
    /**************************     REG_AF_R1_AF_TH_3     ******************************/
    REG_AF_R1_AF_TH_3 reg_af_th_3;
    reg_af_th_3.Raw = 0;
    reg_af_th_3.Bits.AF_G_SAT_TH = 0;
    reg_af_th_3.Bits.AF_B_SAT_TH = 0;
    /**************************     REG_AF_R1_AF_TH_4     ******************************/
    REG_AF_R1_AF_TH_4 reg_af_th_4;
    reg_af_th_4.Raw = 0;
    reg_af_th_4.Bits.AF_R_SAT_TH = 0;


    /**************************     REG_AF_R1_AF_LUT_H0_0  ******************************/
    REG_AF_R1_AF_LUT_H0_0 reg_af_lut_h0_0;
    reg_af_lut_h0_0.Raw =0;
    reg_af_lut_h0_0.Bits.AF_H_TH_0_LUT_MODE = a_sAFConfig->AF_LUT_MODE[0];
    reg_af_lut_h0_0.Bits.AF_H_TH_0_GAIN     = a_sAFConfig->AF_LUT_GAIN[0];
    /**************************     REG_AF_R1_AF_LUT_H0_1  ******************************/
    REG_AF_R1_AF_LUT_H0_1 reg_af_lut_h0_1;
    reg_af_lut_h0_1.Raw =0;
    reg_af_lut_h0_1.Bits.AF_H_TH_0_D1 = a_sAFConfig->AF_LUT_TH0_Dn[0];
    reg_af_lut_h0_1.Bits.AF_H_TH_0_D2 = a_sAFConfig->AF_LUT_TH0_Dn[1];
    reg_af_lut_h0_1.Bits.AF_H_TH_0_D3 = a_sAFConfig->AF_LUT_TH0_Dn[2];
    reg_af_lut_h0_1.Bits.AF_H_TH_0_D4 = a_sAFConfig->AF_LUT_TH0_Dn[3];
    /**************************     REG_AF_R1_AF_LUT_H0_2  ******************************/
    REG_AF_R1_AF_LUT_H0_2 reg_af_lut_h0_2;
    reg_af_lut_h0_2.Raw =0;
    reg_af_lut_h0_2.Bits.AF_H_TH_0_D5 = a_sAFConfig->AF_LUT_TH0_Dn[4];
    reg_af_lut_h0_2.Bits.AF_H_TH_0_D6 = a_sAFConfig->AF_LUT_TH0_Dn[5];
    reg_af_lut_h0_2.Bits.AF_H_TH_0_D7 = a_sAFConfig->AF_LUT_TH0_Dn[6];
    reg_af_lut_h0_2.Bits.AF_H_TH_0_D8 = a_sAFConfig->AF_LUT_TH0_Dn[7];
    /**************************     REG_AF_R1_AF_LUT_H0_3  ******************************/
    REG_AF_R1_AF_LUT_H0_3 reg_af_lut_h0_3;
    reg_af_lut_h0_3.Raw =0;
    reg_af_lut_h0_3.Bits.AF_H_TH_0_D9  = a_sAFConfig->AF_LUT_TH0_Dn[8];
    reg_af_lut_h0_3.Bits.AF_H_TH_0_D10 = a_sAFConfig->AF_LUT_TH0_Dn[9];
    reg_af_lut_h0_3.Bits.AF_H_TH_0_D11 = a_sAFConfig->AF_LUT_TH0_Dn[10];
    reg_af_lut_h0_3.Bits.AF_H_TH_0_D12 = a_sAFConfig->AF_LUT_TH0_Dn[11];
    /**************************     REG_AF_R1_AF_LUT_H0_4  ******************************/
    REG_AF_R1_AF_LUT_H0_4 reg_af_lut_h0_4;
    reg_af_lut_h0_4.Raw =0;
    reg_af_lut_h0_4.Bits.AF_H_TH_0_D13 = a_sAFConfig->AF_LUT_TH0_Dn[12];
    reg_af_lut_h0_4.Bits.AF_H_TH_0_D14 = a_sAFConfig->AF_LUT_TH0_Dn[13];
    reg_af_lut_h0_4.Bits.AF_H_TH_0_D15 = a_sAFConfig->AF_LUT_TH0_Dn[14];
    reg_af_lut_h0_4.Bits.AF_H_TH_0_D16 = a_sAFConfig->AF_LUT_TH0_Dn[15];
    /**************************     REG_AF_R1_AF_LUT_H1_0  ******************************/
    REG_AF_R1_AF_LUT_H1_0 reg_af_lut_h1_0;
    reg_af_lut_h1_0.Raw =0;
    reg_af_lut_h1_0.Bits.AF_H_TH_1_LUT_MODE = a_sAFConfig->AF_LUT_MODE[1];
    reg_af_lut_h1_0.Bits.AF_H_TH_1_GAIN     = a_sAFConfig->AF_LUT_GAIN[1];
    /**************************     REG_AF_R1_AF_LUT_H1_1  ******************************/
    REG_AF_R1_AF_LUT_H1_1 reg_af_lut_h1_1;
    reg_af_lut_h1_1.Raw =0;
    reg_af_lut_h1_1.Bits.AF_H_TH_1_D1 = a_sAFConfig->AF_LUT_TH1_Dn[0];
    reg_af_lut_h1_1.Bits.AF_H_TH_1_D2 = a_sAFConfig->AF_LUT_TH1_Dn[1];
    reg_af_lut_h1_1.Bits.AF_H_TH_1_D3 = a_sAFConfig->AF_LUT_TH1_Dn[2];
    reg_af_lut_h1_1.Bits.AF_H_TH_1_D4 = a_sAFConfig->AF_LUT_TH1_Dn[3];
    /**************************     REG_AF_R1_AF_LUT_H1_2  ******************************/
    REG_AF_R1_AF_LUT_H1_2 reg_af_lut_h1_2;
    reg_af_lut_h1_2.Raw =0;
    reg_af_lut_h1_2.Bits.AF_H_TH_1_D5 = a_sAFConfig->AF_LUT_TH1_Dn[4];
    reg_af_lut_h1_2.Bits.AF_H_TH_1_D6 = a_sAFConfig->AF_LUT_TH1_Dn[5];
    reg_af_lut_h1_2.Bits.AF_H_TH_1_D7 = a_sAFConfig->AF_LUT_TH1_Dn[6];
    reg_af_lut_h1_2.Bits.AF_H_TH_1_D8 = a_sAFConfig->AF_LUT_TH1_Dn[7];
    /**************************     REG_AF_R1_AF_LUT_H1_3  ******************************/
    REG_AF_R1_AF_LUT_H1_3 reg_af_lut_h1_3;
    reg_af_lut_h1_3.Raw =0;
    reg_af_lut_h1_3.Bits.AF_H_TH_1_D9  = a_sAFConfig->AF_LUT_TH1_Dn[8];
    reg_af_lut_h1_3.Bits.AF_H_TH_1_D10 = a_sAFConfig->AF_LUT_TH1_Dn[9];
    reg_af_lut_h1_3.Bits.AF_H_TH_1_D11 = a_sAFConfig->AF_LUT_TH1_Dn[10];
    reg_af_lut_h1_3.Bits.AF_H_TH_1_D12 = a_sAFConfig->AF_LUT_TH1_Dn[11];
    /**************************     REG_AF_R1_AF_LUT_H1_4  ******************************/
    REG_AF_R1_AF_LUT_H1_4 reg_af_lut_h1_4;
    reg_af_lut_h1_4.Raw =0;
    reg_af_lut_h1_4.Bits.AF_H_TH_1_D13 = a_sAFConfig->AF_LUT_TH1_Dn[12];
    reg_af_lut_h1_4.Bits.AF_H_TH_1_D14 = a_sAFConfig->AF_LUT_TH1_Dn[13];
    reg_af_lut_h1_4.Bits.AF_H_TH_1_D15 = a_sAFConfig->AF_LUT_TH1_Dn[14];
    reg_af_lut_h1_4.Bits.AF_H_TH_1_D16 = a_sAFConfig->AF_LUT_TH1_Dn[15];
    /**************************     REG_AF_R1_AF_LUT_V_0  ******************************/
    REG_AF_R1_AF_LUT_V_0 reg_af_lut_v_0;
    reg_af_lut_v_0.Raw =0;
    reg_af_lut_v_0.Bits.AF_V_TH_LUT_MODE = a_sAFConfig->AF_LUT_MODE[2];
    reg_af_lut_v_0.Bits.AF_V_TH_GAIN     = a_sAFConfig->AF_LUT_GAIN[2];
    /**************************     REG_AF_R1_AF_LUT_V_1  ******************************/
    REG_AF_R1_AF_LUT_V_1 reg_af_lut_v_1;
    reg_af_lut_v_1.Raw =0;
    reg_af_lut_v_1.Bits.AF_V_TH_D1 = a_sAFConfig->AF_LUT_TH2_Dn[0];
    reg_af_lut_v_1.Bits.AF_V_TH_D2 = a_sAFConfig->AF_LUT_TH2_Dn[1];
    reg_af_lut_v_1.Bits.AF_V_TH_D3 = a_sAFConfig->AF_LUT_TH2_Dn[2];
    reg_af_lut_v_1.Bits.AF_V_TH_D4 = a_sAFConfig->AF_LUT_TH2_Dn[3];
    /**************************     REG_AF_R1_AF_LUT_V_2  ******************************/
    REG_AF_R1_AF_LUT_V_2 reg_af_lut_v_2;
    reg_af_lut_v_2.Raw =0;
    reg_af_lut_v_2.Bits.AF_V_TH_D5 = a_sAFConfig->AF_LUT_TH2_Dn[4];
    reg_af_lut_v_2.Bits.AF_V_TH_D6 = a_sAFConfig->AF_LUT_TH2_Dn[5];
    reg_af_lut_v_2.Bits.AF_V_TH_D7 = a_sAFConfig->AF_LUT_TH2_Dn[6];
    reg_af_lut_v_2.Bits.AF_V_TH_D8 = a_sAFConfig->AF_LUT_TH2_Dn[7];
    /**************************     REG_AF_R1_AF_LUT_V_3  ******************************/
    REG_AF_R1_AF_LUT_V_3 reg_af_lut_v_3;
    reg_af_lut_v_3.Raw =0;
    reg_af_lut_v_3.Bits.AF_V_TH_D9  = a_sAFConfig->AF_LUT_TH2_Dn[8];
    reg_af_lut_v_3.Bits.AF_V_TH_D10 = a_sAFConfig->AF_LUT_TH2_Dn[9];
    reg_af_lut_v_3.Bits.AF_V_TH_D11 = a_sAFConfig->AF_LUT_TH2_Dn[10];
    reg_af_lut_v_3.Bits.AF_V_TH_D12 = a_sAFConfig->AF_LUT_TH2_Dn[11];
    /**************************     REG_AF_R1_AF_LUT_V_4  ******************************/
    REG_AF_R1_AF_LUT_V_4 reg_af_lut_v_4;
    reg_af_lut_v_4.Raw =0;
    reg_af_lut_v_4.Bits.AF_V_TH_D13 = a_sAFConfig->AF_LUT_TH2_Dn[12];
    reg_af_lut_v_4.Bits.AF_V_TH_D14 = a_sAFConfig->AF_LUT_TH2_Dn[13];
    reg_af_lut_v_4.Bits.AF_V_TH_D15 = a_sAFConfig->AF_LUT_TH2_Dn[14];
    reg_af_lut_v_4.Bits.AF_V_TH_D16 = a_sAFConfig->AF_LUT_TH2_Dn[15];

    //xsize/ysize
    #define FOOTPRINT_SIZ 40 /* AF_EXT_STAT_EN must be TRUE */
    MUINT32 xsize = reg_af_blk_1.Bits.AF_BLK_XNUM * FOOTPRINT_SIZ;
    MUINT32 ysize = reg_af_blk_1.Bits.AF_BLK_YNUM;

#if 0
    REG_AF_INFO(CAM_AFO_XSIZE,   xsize);
    REG_AF_INFO(CAM_AFO_YSIZE,   ysize);
    //REG_AF_INFO(CAM_AFO_STRIDE,  xsize + FOOTPRINT_SIZ);
    REG_AF_INFO(CAM_AF_SIZE,     reg_af_size.Raw);
    REG_AF_INFO(CAM_AF_CON,      reg_af_con.Raw);
    REG_AF_INFO(CAM_AF_VLD,      reg_af_vld.Raw);
    REG_AF_INFO(CAM_AF_BLK_0,    reg_af_blk_0.Raw);
    REG_AF_INFO(CAM_AF_BLK_1,    reg_af_blk_1.Raw);
    REG_AF_INFO(CAM_AF_TH_0,     reg_af_th_0.Raw);
    REG_AF_INFO(CAM_AF_TH_1,     reg_af_th_1.Raw);
    REG_AF_INFO(CAM_AF_FLT_1,    reg_af_flt_1.Raw);
    REG_AF_INFO(CAM_AF_FLT_2,    reg_af_flt_2.Raw);
    REG_AF_INFO(CAM_AF_FLT_3,    reg_af_flt_3.Raw);
    REG_AF_INFO(CAM_AF_FLT_4,    reg_af_flt_4.Raw);
    REG_AF_INFO(CAM_AF_FLT_5,    reg_af_flt_5.Raw);
    REG_AF_INFO(CAM_AF_FLT_6,    reg_af_flt_6.Raw);
    REG_AF_INFO(CAM_AF_FLT_7,    reg_af_flt_7.Raw);
    REG_AF_INFO(CAM_AF_FLT_8,    reg_af_flt_8.Raw);
    REG_AF_INFO(CAM_AF_FLT_9,    reg_af_flt_9.Raw);
    REG_AF_INFO(CAM_AF_FLT_10,   reg_af_flt_10.Raw);
    REG_AF_INFO(CAM_AF_FLT_11,   reg_af_flt_11.Raw);
    REG_AF_INFO(CAM_AF_FLT_12,   reg_af_flt_12.Raw);
    REG_AF_INFO(CAM_AF_LUT_H0_0, reg_af_lut_h0_0.Raw);
    REG_AF_INFO(CAM_AF_LUT_H0_1, reg_af_lut_h0_1.Raw);
    REG_AF_INFO(CAM_AF_LUT_H0_2, reg_af_lut_h0_2.Raw);
    REG_AF_INFO(CAM_AF_LUT_H0_3, reg_af_lut_h0_3.Raw);
    REG_AF_INFO(CAM_AF_LUT_H0_4, reg_af_lut_h0_4.Raw);
    REG_AF_INFO(CAM_AF_LUT_H1_0, reg_af_lut_h1_0.Raw);
    REG_AF_INFO(CAM_AF_LUT_H1_1, reg_af_lut_h1_1.Raw);
    REG_AF_INFO(CAM_AF_LUT_H1_2, reg_af_lut_h1_2.Raw);
    REG_AF_INFO(CAM_AF_LUT_H1_3, reg_af_lut_h1_3.Raw);
    REG_AF_INFO(CAM_AF_LUT_H1_4, reg_af_lut_h1_4.Raw);
    REG_AF_INFO(CAM_AF_LUT_V_0,  reg_af_lut_v_0.Raw);
    REG_AF_INFO(CAM_AF_LUT_V_1,  reg_af_lut_v_1.Raw);
    REG_AF_INFO(CAM_AF_LUT_V_2,  reg_af_lut_v_2.Raw);
    REG_AF_INFO(CAM_AF_LUT_V_3,  reg_af_lut_v_3.Raw);
    REG_AF_INFO(CAM_AF_LUT_V_4,  reg_af_lut_v_4.Raw);
    REG_AF_INFO(CAM_AF_CON2,     reg_af_con2.Raw);
    REG_AF_INFO(CAM_SGG1_PGN,    reg_sgg1_pgn.Raw);
    REG_AF_INFO(CAM_SGG1_GMRC_1, reg_sgg1_gmrc_1.Raw);
    REG_AF_INFO(CAM_SGG1_GMRC_2, reg_sgg1_gmrc_2.Raw);
    REG_AF_INFO(CAM_AF_TH_2,     reg_af_th_2.Raw);
    //REG_AF_INFO(CAM_AF_BLK_2,    reg_af_blk_2.Raw);
    REG_AF_INFO(CAM_SGG5_PGN,    reg_sgg5_pgn.Raw);
    REG_AF_INFO(CAM_SGG5_GMRC_1, reg_sgg5_gmrc_1.Raw);
    REG_AF_INFO(CAM_SGG5_GMRC_2, reg_sgg5_gmrc_2.Raw);
#endif

    p_sAFResultConfig->enableAFHw = 1;
    p_sAFResultConfig->isApplied  = 0;
    p_sAFResultConfig->configNum  = a_sAFConfig->u4ConfigNum;

    // CAM_LOGD("HW-%s configReg done", __FUNCTION__);

    if (memcmp(pAFRegInfo, m_rAFRegInfoPre, EAFRegInfo_NUM*sizeof(MUINT32)) != 0)
    {
        p_sAFResultConfig->isApplied  = 1;
        m_u4ConfigNum = a_sAFConfig->u4ConfigNum;
        memcpy(m_rAFRegInfoPre, pAFRegInfo, EAFRegInfo_NUM*sizeof(MUINT32));

        CAM_LOGD("HW-%s Config(%d)", __FUNCTION__, p_sAFResultConfig->configNum);
    }
#else
    p_sAFResultConfig->enableAFHw = 0;
    p_sAFResultConfig->isApplied  = 1;
#endif
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL ISP_AF_CONFIG_T::isHWRdy(MUINT32 i4Config)
{
    MBOOL ret = MFALSE;

    if (i4Config >= m_u4ConfigNum)
    {
        if (i4Config == m_u4ConfigNum)
        {
            CAM_LOGD("HW-%s ready(%d)", __FUNCTION__, m_u4ConfigNum);
        }

        ret = MTRUE;
    }

    return ret;
}

