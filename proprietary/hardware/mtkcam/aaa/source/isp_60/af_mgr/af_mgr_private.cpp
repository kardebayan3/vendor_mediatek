/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "af_mgr_v3"

#ifndef ENABLE_MY_LOG
#define ENABLE_MY_LOG       (1)
#endif

#include <utils/threads.h>  // For Mutex::Autolock.
#include <sys/stat.h>
#include <sys/time.h>
#include <cutils/properties.h>
#include <cutils/atomic.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <kd_camera_feature.h>
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Trace.h>
#include <aaa_trace.h>
#include <faces.h>
#include <private/aaa_hal_private.h>
#include <camera_custom_nvram.h>
#include <dbg_aaa_param.h>
#include <dbg_af_param.h>
#include <af_param.h>
#include <pd_param.h>
#include <af_tuning_custom.h>
#include <mcu_drv.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/sys/SensorProvider.h>
#include <cct_feature.h>
#include <af_feature.h>

#include "af_mgr.h"
#include <nvbuf_util.h>
#include "aaa_common_custom.h"
#include <pd_mgr_if.h>
#include "private/PDTblGen.h"
#include <laser_mgr_if.h>
#include <ae_param.h>

//
#include "mtkcam/utils/metadata/client/mtk_metadata_tag.h"

//configure HW
//#include <isp_mgr_af_stat.h>
#include <StatisticBuf.h>

#include <math.h>
#include <android/sensor.h>             // for g/gyro sensor listener
#include <mtkcam/utils/sys/SensorListener.h>    // for g/gyro sensor listener
#define SENSOR_ACCE_POLLING_MS  20
#define SENSOR_GYRO_POLLING_MS  20
#define SENSOR_ACCE_SCALE       100
#define SENSOR_GYRO_SCALE       100

#define LASER_TOUCH_REGION_W    0
#define LASER_TOUCH_REGION_H    0

#define AF_ENLOG_STATISTIC 2
#define AF_ENLOG_ROI 4

#define DBG_MSG_BUF_SZ 1024 //byte

#if defined(HAVE_AEE_FEATURE)
#include <aee.h>
#define AEE_ASSERT_AF(String) \
          do { \
              aee_system_warning( \
                  "af_mgr", \
                  NULL, \
                  DB_OPT_DEFAULT|DB_OPT_FTRACE, \
                  String); \
          } while(0)
#else
#define AEE_ASSERT_AF(String)
#endif

// LaunchCamTrigger
#define AESTABLE_TIMEOUT 0
#define VALIDPD_TIMEOUT  0

#define GYRO_THRESHOLD 15
#define MAX_PDMOVING_COUNT 6

using namespace NS3Av3;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSCamIOPipe;
using namespace NSCam::Utils;

MVOID AfMgr::resetAFParams()
{
    //reset member.
    memset( &m_sAFInput,           0, sizeof(AF_INPUT_T ));
    memset( &m_sAFOutput,          0, sizeof(AF_OUTPUT_T));
    memset( &m_sCropRegionInfo,    0, sizeof(AREA_T     ));
    memset( &m_sArea_Focusing,     0, sizeof(AREA_T     ));
    memset( &m_sArea_Center,       0, sizeof(AREA_T     ));
    memset( &m_sArea_APCmd,        0, sizeof(AREA_T     ));
    memset( &m_sArea_APCheck,      0, sizeof(AREA_T     ));
    memset( &m_sArea_OTFD,         0, sizeof(AREA_T     ));
    memset( &m_sArea_HW,           0, sizeof(AREA_T     ));
    memset( &m_sArea_Bokeh,        0, sizeof(AREA_T     ));
    memset( &m_sPDRois[0],         0, sizeof(PD_CALCULATION_ROI_T)*eIDX_ROI_ARRAY_NUM);
    memset( &m_sPDCalculateWin[0], 0, sizeof(AFPD_BLOCK_ROI_T    )*AF_PSUBWIN_NUM    );
    memset( &m_aAEBlkVal,          0, sizeof(MUINT8              )*25                );
    memset( &m_sIspSensorInfo,     0, sizeof(ISP_SENSOR_INFO_T));
    memset( &m_sFRMInfo,           0, sizeof(AF_FRAME_INFO_T));
    memset( &m_sAFResultConfig,    0, sizeof(AFResultConfig_T));
    m_vFrmInfo.clear();
    m_vISQueue.clear();
    m_i4EnableAF    = -1;
    m_i4PDCalculateWinNum = 0;
    m_i4DZFactor    = 100; /* Initial digital zoom factor. */
    m_sFocusDis.i4LensPos = 0; /* default value, ref: af_tuning_customer.cpp*/
    m_sFocusDis.i4Dist    = 0.33; /* default value, ref: af_tuning_customer.cpp*/
    m_i4MvLensTo    = -1;
    m_i4MvLensToPre = 0;
    m_i4HWBlkNumX   = 0;
    m_i4HWBlkNumY   = 0;
    m_i4HWBlkSizeX  = 0;
    m_i4HWBlkSizeY  = 0;
    m_i4HWEnExtMode = 0;
    m_i4IsFocused   = 0;
    m_u4ReqMagicNum = 0;
    m_u4StaMagicNum = 0;
    m_u4ConfigHWNum = 0;
    m_u4ConfigLatency = 3;
    m_bPdInputExpected = MFALSE;
    m_sArea_TypeSel = AF_ROI_SEL_NONE;

    m_i4OTFDLogLv   = 0;
    m_eEvent        = EVENT_CMD_START;
    m_i4IsAFSearch_PreState = AF_SEARCH_DONE; /*Force to select ROI to focusing as first in, ref:SelROIToFocusing */
    m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
    m_bRunPDEn          = MFALSE;
    m_bSDAFEn           = MFALSE;
    m_bMZAFEn           = MFALSE;
    m_bGetMetaData      = MFALSE;
    m_i4TAFStatus       = TAF_STATUS_RESET;
    m_i4DbgOISPos       = 0;
    m_u4FrameCount      = 0;
    i4IsLockAERequest   = 0;
    m_i4IsCAFWithoutFace = 0;
    m_bForceDoAlgo = MFALSE;
    CAM_LOGD("AF-%-15s: Dev(0x%04x), set CurAFMode to EDOF to enable LaunchCamTrigger",
             __FUNCTION__,
             m_i4CurrSensorDev);
    m_eCurAFMode = MTK_CONTROL_AF_MODE_EDOF;
    // LaunchCamTrigger : disable at the first
    m_i4LaunchCamTriggered_Prv = m_i4LaunchCamTriggered = E_LAUNCH_AF_IDLE;
    m_i4AEStableFrameCount = -1;
    m_i4ValidPDFrameCount = -1;
    m_i4AEStableTriggerTimeout = AESTABLE_TIMEOUT;
    m_i4ValidPDTriggerTimeout = VALIDPD_TIMEOUT;

    m_i4FirsetCalPDFrameCount = -1;
    m_i4isAEStable = MFALSE;
    m_i4ContinuePDMovingCount = 0;
    m_pdaf_raw_fmt = -1;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::SetCurFrmNum( MUINT32 u4FrmNum, MUINT32 u4FrmNumCur)
{
    // Request
    m_u4ReqMagicNum = u4FrmNum;
    // Statistic - u4FrmNumCur
    m_eEvent = EVENT_NONE;
    CAM_LOGD_IF( m_i4DgbLogLv&2, "Req #%5d", m_u4ReqMagicNum);
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::SetCropRegionInfo( MUINT32 u4XOffset, MUINT32 u4YOffset, MUINT32 u4Width, MUINT32 u4Height, MUINT32 u4Caller)
{
    if( u4Width == 0 || u4Height == 0 )
    {
        return S_AF_OK;
    }

    /* Digital Zoom : skip crop reggion info durning AF seraching */
    if( m_i4IsAFSearch_CurState!=AF_SEARCH_DONE)
    {
        return S_AF_OK;
    }

    if( (u4XOffset<m_i4TGSzW) &&
            (u4YOffset<m_i4TGSzH) &&
            (u4XOffset+u4Width<=m_i4TGSzW) &&
            (u4YOffset+u4Height<=m_i4TGSzH) )
    {
        // set crop region information and update center ROI automatically.
        if( m_sCropRegionInfo.i4X != (MINT32)u4XOffset ||
                m_sCropRegionInfo.i4Y != (MINT32)u4YOffset ||
                m_sCropRegionInfo.i4W != (MINT32)u4Width   ||
                m_sCropRegionInfo.i4H != (MINT32)u4Height )
        {
            m_sCropRegionInfo.i4X = (MINT32)u4XOffset;
            m_sCropRegionInfo.i4Y = (MINT32)u4YOffset;
            m_sCropRegionInfo.i4W = (MINT32)u4Width;
            m_sCropRegionInfo.i4H = (MINT32)u4Height;

            //calculate zoom information : 1X-> 100, 2X->200, ...
            MUINT32 dzfX = 100*m_i4TGSzW/m_sCropRegionInfo.i4W;
            MUINT32 dzfY = 100*m_i4TGSzH/m_sCropRegionInfo.i4H;
            //Should be the same.
            m_i4DZFactor = dzfX<dzfY ? dzfX : dzfY;

            if( u4Caller==AF_MGR_CALLER)
            {
                CAM_LOGD( "#(%5d,%5d) %s (x,y,w,h)=(%d, %d, %d, %d), dzX(%d), dzY(%d), Fac(%d)",
                          m_u4ReqMagicNum,
                          m_u4StaMagicNum,
                          __FUNCTION__,
                          u4XOffset,
                          u4YOffset,
                          u4Width,
                          u4Height,
                          dzfX,
                          dzfY,
                          m_i4DZFactor);
            }
            else
            {
                CAM_LOGD( "#(%5d,%5d) cmd-%s (x,y,w,h)=(%d, %d, %d, %d), dzX(%d), dzY(%d), Fac(%d)",
                          m_u4ReqMagicNum,
                          m_u4StaMagicNum,
                          __FUNCTION__,
                          u4XOffset,
                          u4YOffset,
                          u4Width,
                          u4Height,
                          dzfX,
                          dzfY,
                          m_i4DZFactor);
            }

            //Accroding to crop region, updating center ROI coordinate automatically
            UpdateCenterROI( m_sArea_Center);

            //Reset all focusing window.
            m_sArea_Focusing = m_sArea_APCheck = m_sArea_APCmd = m_sArea_OTFD = m_sArea_HW = m_sArea_Center;
            m_sArea_TypeSel = ROI_TYPE_CENTER;

            // it is always valid for calculation center roi once crop region is set.
            m_sPDRois[eIDX_ROI_ARRAY_CENTER].valid       = MTRUE;
            m_sPDRois[eIDX_ROI_ARRAY_CENTER].info.sType  = ROI_TYPE_CENTER;
            m_sPDRois[eIDX_ROI_ARRAY_CENTER].info.sPDROI = m_sArea_Center;
        }
        else
        {
            CAM_LOGD_IF( m_i4DgbLogLv&2, "%s same cmd", __FUNCTION__);
        }
    }
    else
    {
        CAM_LOGD_IF( m_i4DgbLogLv, "%s not valid", __FUNCTION__);
    }
    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setAFArea( CameraFocusArea_T &sInAFArea)
{
    /* sInAFArea is TG base coordinate */

    MRESULT ret = E_3A_ERR;

    char  dbgMsgBuf[DBG_MSG_BUF_SZ];
    char* ptrMsgBuf = dbgMsgBuf;

    if( m_i4DgbLogLv)
    {
        ptrMsgBuf += sprintf( ptrMsgBuf, "#(%5d,%5d) cmd-%s : [Cnt]%d [L]%d [R]%d [U]%d [B]%d, ",
                              m_u4ReqMagicNum,
                              m_u4StaMagicNum,
                              __FUNCTION__,
                              sInAFArea.u4Count,
                              sInAFArea.rAreas[0].i4Left,
                              sInAFArea.rAreas[0].i4Right,
                              sInAFArea.rAreas[0].i4Top,
                              sInAFArea.rAreas[0].i4Bottom);
    }

    if( 1<=sInAFArea.u4Count)
    {
        if( // boundary check.
            (sInAFArea.rAreas[0].i4Left < sInAFArea.rAreas[0].i4Right ) &&
            (sInAFArea.rAreas[0].i4Top  < sInAFArea.rAreas[0].i4Bottom) &&
            (0 <= sInAFArea.rAreas[0].i4Left && sInAFArea.rAreas[0].i4Right <= (MINT32)m_i4TGSzW) &&
            (0 <= sInAFArea.rAreas[0].i4Top  && sInAFArea.rAreas[0].i4Bottom <= (MINT32)m_i4TGSzH))
        {
            // To remain the center
            MINT32 InWidth  = sInAFArea.rAreas[0].i4Right - sInAFArea.rAreas[0].i4Left;
            MINT32 InHeight = sInAFArea.rAreas[0].i4Bottom - sInAFArea.rAreas[0].i4Top;
            MINT32 Center_X = sInAFArea.rAreas[0].i4Left + InWidth/2;
            MINT32 Center_Y = sInAFArea.rAreas[0].i4Top + InHeight/2;

            MINT32 Percent_TouchAFROI = 100;
            // To resize the Touch AF ROI if it is required from users.
            if (m_ptrNVRam->rAFNVRAM.i4EasyTuning[3]>0 && m_ptrNVRam->rAFNVRAM.i4EasyTuning[3]<=200)
            {
                Percent_TouchAFROI = m_ptrNVRam->rAFNVRAM.i4EasyTuning[3];
            }
            // To override the Percent_TouchAFROI based on ISO target
            if (m_ptrNVRam->rAFNVRAM.i4EasyTuning[5]>0 && m_ptrNVRam->rAFNVRAM.i4EasyTuning[5]<=200 && m_ptrNVRam->rAFNVRAM.i4EasyTuning[4]>0)
            {
                if (m_sAFInput.i4ISO > m_ptrNVRam->rAFNVRAM.i4EasyTuning[4])
                {
                    Percent_TouchAFROI = m_ptrNVRam->rAFNVRAM.i4EasyTuning[5];
                }
            }
            // To resize the Touch AF ROI if it is required from users.
            if(Percent_TouchAFROI!=100)
            {
                InWidth =  InWidth * Percent_TouchAFROI / 100.0;
                InHeight = InHeight * Percent_TouchAFROI / 100.0;
                // To resize the ROI from tuning parameter
                MINT32 TempLeft   = Center_X - (InWidth/2);
                MINT32 TempRight  = TempLeft + InWidth;
                MINT32 TempTop    = Center_Y - (InHeight/2);
                MINT32 TempBottom = TempTop  + InHeight;
                // Left Boundary Check
                if(TempLeft < 0)
                {
                    TempLeft  = 0;
                    TempRight = InWidth;
                }
                // Right Boundary Check
                if(TempRight >= m_i4TGSzW)
                {
                    TempRight = (m_i4TGSzW-1);
                    TempLeft  = TempRight - InWidth;
                }
                // Top Boundary Check
                if(TempTop < 0)
                {
                    TempTop    = 0;
                    TempBottom = InHeight;
                }
                // Bottom Boundary Check
                if(TempBottom >= m_i4TGSzH)
                {
                    TempBottom = (m_i4TGSzH-1);
                    TempTop    = TempBottom - InHeight;
                }
                CAM_LOGD_IF(m_i4DgbLogLv,
                            "#(%5d,%5d) cmd-%s InAFArea is resized from users' requirement, Resize Percent(%d), ROI(L,R,U,B): Ori(%d,%d,%d,%d)->Rescale(%d,%d,%d,%d)",
                            m_u4ReqMagicNum,
                            m_u4StaMagicNum,
                            __FUNCTION__,
                            Percent_TouchAFROI,
                            sInAFArea.rAreas[0].i4Left,
                            sInAFArea.rAreas[0].i4Right,
                            sInAFArea.rAreas[0].i4Top,
                            sInAFArea.rAreas[0].i4Bottom,
                            TempLeft, TempRight, TempTop, TempBottom );
                // To update the resized touch AF ROI
                sInAFArea.rAreas[0].i4Left   = TempLeft;
                sInAFArea.rAreas[0].i4Right  = TempRight;
                sInAFArea.rAreas[0].i4Top    = TempTop;
                sInAFArea.rAreas[0].i4Bottom = TempBottom;
            }
            /*
            // If InAFArea.W < ROIWidth || InAFArea.H < ROIWidth, resize the AreaSize
            // Resize rule : the rectangular centered by the touch position
            //  - Center = InAFArea.Center
            //  - Width = ShortLine * PERCENT_AFROI / 100.0;
            */
            MINT32 Percent_AFROI = 10; // default : 20%
            if(m_ptrNVRam->rAFNVRAM.i4EasyTuning[1]>=1 && m_ptrNVRam->rAFNVRAM.i4EasyTuning[1]<=100)
            {
                Percent_AFROI = m_ptrNVRam->rAFNVRAM.i4EasyTuning[1];
            }
            MDOUBLE ShortLine = min(m_i4TGSzW, m_i4TGSzH);
            MINT32 ROIWidth  = (MINT32)(ShortLine * Percent_AFROI / 100.0);
            if(InWidth<ROIWidth || InHeight<ROIWidth)
            {
                // To resize the rectangular
                MINT32 TempLeft   = Center_X - ROIWidth/2;
                MINT32 TempRight  = TempLeft + ROIWidth;
                MINT32 TempTop    = Center_Y - ROIWidth/2;
                MINT32 TempBottom = TempTop  + ROIWidth;
                // Left Boundary Check
                if(TempLeft < 0)
                {
                    TempLeft  = 0;
                    TempRight = ROIWidth;
                }
                // Right Boundary Check
                if(TempRight >= m_i4TGSzW)
                {
                    TempRight = m_i4TGSzW-1;
                    TempLeft  = TempRight - ROIWidth;
                }
                // Top Boundary Check
                if(TempTop < 0)
                {
                    TempTop    = 0;
                    TempBottom = ROIWidth;
                }
                // Bottom Boundary Check
                if(TempBottom >= m_i4TGSzH)
                {
                    TempBottom = (m_i4TGSzH-1);
                    TempTop    = TempBottom - ROIWidth;
                }
                CAM_LOGD("#(%5d,%5d) cmd-%s InAFArea is too small, MinROIWidth(%d), Ori(%d,%d,%d,%d)->rescale(%d,%d,%d,%d)",
                         m_u4ReqMagicNum,
                         m_u4StaMagicNum,
                         __FUNCTION__,
                         ROIWidth,
                         sInAFArea.rAreas[0].i4Left,
                         sInAFArea.rAreas[0].i4Right,
                         sInAFArea.rAreas[0].i4Top,
                         sInAFArea.rAreas[0].i4Bottom,
                         TempLeft, TempRight, TempTop, TempBottom );
                // update the InAFArea
                sInAFArea.rAreas[0].i4Left   = TempLeft;
                sInAFArea.rAreas[0].i4Right  = TempRight;
                sInAFArea.rAreas[0].i4Top    = TempTop;
                sInAFArea.rAreas[0].i4Bottom = TempBottom;
            }

            AREA_T roi = AREA_T( sInAFArea.rAreas[0].i4Left,
                                 sInAFArea.rAreas[0].i4Top,
                                 sInAFArea.rAreas[0].i4Right  - sInAFArea.rAreas[0].i4Left,
                                 sInAFArea.rAreas[0].i4Bottom - sInAFArea.rAreas[0].i4Top,
                                 AF_MARK_NONE);

            if( memcmp( &roi, &m_sArea_APCheck, sizeof(AREA_T))!=0)
            {
                //store command.
                m_sArea_APCheck = m_sArea_APCmd = roi;

                m_sPDRois[eIDX_ROI_ARRAY_AP].info.sType  = ROI_TYPE_AP;
                m_sPDRois[eIDX_ROI_ARRAY_AP].info.sPDROI = m_sArea_APCmd;

                UpdateState( EVENT_CMD_SET_AF_REGION);

                CAM_LOGD( "#(%5d,%5d) cmd-%s Got ROI changed cmd. [Cnt]%d (L,R,U,B)=(%d,%d,%d,%d) => (X,Y,W,H)=(%d,%d,%d,%d)",
                          m_u4ReqMagicNum,
                          m_u4StaMagicNum,
                          __FUNCTION__,
                          sInAFArea.u4Count,
                          sInAFArea.rAreas[0].i4Left,
                          sInAFArea.rAreas[0].i4Right,
                          sInAFArea.rAreas[0].i4Top,
                          sInAFArea.rAreas[0].i4Bottom,
                          m_sArea_APCmd.i4X,
                          m_sArea_APCmd.i4Y,
                          m_sArea_APCmd.i4W,
                          m_sArea_APCmd.i4H);

                // control laser AF touch behavior.
                if (m_bLDAFEn == MTRUE)
                {
                    MINT32 Centr_X = m_sCropRegionInfo.i4X + (m_sCropRegionInfo.i4W / 2);
                    MINT32 Centr_Y = m_sCropRegionInfo.i4Y + (m_sCropRegionInfo.i4H / 2);
                    MINT32 Touch_X = (sInAFArea.rAreas[0].i4Left + sInAFArea.rAreas[0].i4Right) / 2;
                    MINT32 Touch_Y = (sInAFArea.rAreas[0].i4Top  + sInAFArea.rAreas[0].i4Bottom) / 2;

                    m_sAFInput.sLaserInfo.i4AfWinPosX = abs(Centr_X - Touch_X);
                    m_sAFInput.sLaserInfo.i4AfWinPosY = abs(Centr_Y - Touch_Y);

                    if ((m_sAFInput.sLaserInfo.i4AfWinPosX < LASER_TOUCH_REGION_W) && (m_sAFInput.sLaserInfo.i4AfWinPosY < LASER_TOUCH_REGION_H))
                    {
                        if ((m_eLIB3A_AFMode == LIB3A_AF_MODE_AFS) || (m_eLIB3A_AFMode == LIB3A_AF_MODE_MACRO))
                        {
                            m_sAFInput.sLaserInfo.i4AfWinPosCnt++;
                        }
                    }
                    else
                    {
                        m_sAFInput.sLaserInfo.i4AfWinPosCnt = 0;
                    }
                }

                ret = S_AF_OK;
            }
            else
            {
                if( m_i4DgbLogLv)
                {
                    ptrMsgBuf += sprintf( ptrMsgBuf, "ROI cmd is same");
                }
            }
        }
        else
        {
            //command is not valid, using center window.
            if( m_i4DgbLogLv)
            {
                ptrMsgBuf += sprintf( ptrMsgBuf, "ROI cmd is not correct");
            }
        }
    }
    else
    {
        //command is not valid, using center window.
        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf, "ROI cnt=0!!");
        }
    }

    if( ret==E_3A_ERR)
    {
        CAM_LOGD_IF( m_i4DgbLogLv&2, "%s", dbgMsgBuf);
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::setFocusDistance( MFLOAT lens_focusDistance)
{
    MINT32 fdistidx = 0;
    MINT32 i4distmm;
    MINT32 i4tblLL;
    MINT32 i4ResultDac;

    if( lens_focusDistance<0)
    {
        return;
    }
    //lens_focusDistance is in unit dipoter, means 1/distance,
    //if distance is 100 cm,  then the value is  1/(0.1M) =10,
    // 10 cm => dipoter 100
    // 1 cm => dipoter 1000
    i4tblLL = m_pAFParam->i4TBLL;
    if( lens_focusDistance <= (1000.0f/m_pAFParam->i4Dist[i4tblLL-1])) /*infinity*/
    {
        i4ResultDac=m_pAFParam->i4Dacv[i4tblLL-1];
    }
    else if( (1000.0f/m_pAFParam->i4Dist[0]) <= lens_focusDistance)  /*marco*/
    {
        i4ResultDac = m_pAFParam->i4Dacv[0];
    }
    else
    {
        i4distmm = (MINT32)(1000/lens_focusDistance);

        for( fdistidx=0; fdistidx< i4tblLL ; fdistidx++)
        {
            if( i4distmm<m_pAFParam->i4Dist[fdistidx])
                break;

        }

        if( fdistidx==0)
        {
            i4ResultDac = m_pAFParam->i4Dacv[0];
        }
        else
        {
            i4ResultDac=
                ( m_pAFParam->i4Dacv[fdistidx]   * (i4distmm - m_pAFParam->i4Dist[fdistidx-1])
                  + m_pAFParam->i4Dacv[fdistidx-1] * (m_pAFParam->i4Dist[fdistidx] - i4distmm ))
                /(m_pAFParam->i4Dist[fdistidx] - m_pAFParam->i4Dist[fdistidx-1] );
        }
    }

    // API2: At MTK_CONTROL_AF_MODE_OFF mode, configure algorithm as MF mode.
    //          The auto-focus routine does not control the lens. Lens is controlled by the application.
    if( (m_eLIB3A_AFMode == LIB3A_AF_MODE_MF) &&
            (m_sAFOutput.i4AFPos != i4ResultDac) &&
            (0<=i4ResultDac) &&
            (i4ResultDac<=1023))
    {
        CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d) DAC(%d->%d) Dist(%f)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_sAFOutput.i4AFPos,
                  i4ResultDac,
                  lens_focusDistance);

        m_i4MFPos = i4ResultDac;

        if (m_pIAfAlgo)
        {
            m_pIAfAlgo->setMFPos(m_i4MFPos);
            m_pIAfAlgo->trigger();
        }
        else
        {
            CAM_LOGD("Null m_pIAfAlgo");
        }
    }
    else
    {
        CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d) !!skip!! DAC(%d->%d) Dist(%f) lib_afmode(%d)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_sAFOutput.i4AFPos,
                  i4ResultDac,
                  lens_focusDistance,
                  m_eLIB3A_AFMode);
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::WaitTriggerAF( MBOOL bWait)
{
    if( m_i4EnableAF==0)
    {
        return S_AF_OK;
    }
    if( bWait==MTRUE)
    {
        UpdateState( EVENT_SET_WAIT_FORCE_TRIGGER);
    }
    else
    {
        UpdateState( EVENT_CANCEL_WAIT_FORCE_TRIGGER);
    }
    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::triggerAF( MUINT32 u4Caller)
{
    if( u4Caller==AF_MGR_CALLER)
        CAM_LOGD( "%s  Dev(%d) lib_afmode(%d)", __FUNCTION__, m_i4CurrSensorDev, m_eLIB3A_AFMode);
    else
        CAM_LOGD( "cmd-%s  Dev(%d) lib_afmode(%d)", __FUNCTION__, m_i4CurrSensorDev, m_eLIB3A_AFMode);

    UpdateState( EVENT_CMD_TRIGGERAF_WITH_AE_STBL);

    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::autoFocus()
{
    CAM_LOGD( "cmd-%s Dev %d : lib_afmode %d", __FUNCTION__, m_i4CurrSensorDev, m_eLIB3A_AFMode);

    if( m_i4EnableAF==0)
    {
        CAM_LOGD( "autofocus : dummy lens");
        return;
    }

    UpdateState( EVENT_CMD_AUTOFOCUS);

    if( m_pIAfAlgo)
    {
        m_pIAfAlgo->targetAssistMove();
    }

    //calibration flow testing
    if (m_bLDAFEn == MTRUE)
    {
        int Offset = 0;
        int XTalk = 0;

        int Mode = property_get_int32("vendor.laser.calib.mode", 0);

        if( Mode == 1 )
        {
            CAM_LOGD( "LaserCali : getLaserOffsetCalib Start");
            Offset = ILaserMgr::getInstance().getLaserOffsetCalib(m_i4CurrSensorDev);
            CAM_LOGD( "LaserCali : getLaserOffsetCalib : %d", Offset);
            CAM_LOGD( "LaserCali : getLaserOffsetCalib End");
        }

        if( Mode == 2 )
        {
            CAM_LOGD( "LaserCali : getLaserXTalkCalib Start");
            XTalk = ILaserMgr::getInstance().getLaserXTalkCalib(m_i4CurrSensorDev);
            CAM_LOGD( "LaserCali : getLaserXTalkCalib : %d", XTalk);
            CAM_LOGD( "LaserCali : getLaserXTalkCalib End");
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::cancelAutoFocus()
{
    CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d) lib_afmode(%d)",
              m_u4ReqMagicNum,
              m_u4StaMagicNum,
              __FUNCTION__,
              m_i4CurrSensorDev,
              m_eLIB3A_AFMode);

    //update parameters and status.
    UpdateState(EVENT_CMD_AUTOFOCUS_CANCEL);
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::setAdptCompInfo( const AdptCompTimeData_T &AdptCompTime)
{
    // info from AfMgr
    m_sAFInput.TS_MLStart  = m_u8MvLensTS;          // unit: us
    // info from Hal3A
    TS_AFDone_Pre = m_sAFInput.TS_AFDone;           // previous TS_AFDone unit: us
    m_sAFInput.TS_AFDone  = AdptCompTime.TS_AFDone; // current TS_AFDone, unit: us
    // debug
    if(m_i4DgbLogLv)
    {
        char  dbgMsgBuf[DBG_MSG_BUF_SZ];
        char* ptrMsgBuf = dbgMsgBuf;
        ptrMsgBuf += sprintf( ptrMsgBuf, "#(%5d,%5d) cmd-%s Dev(%d): ", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        ptrMsgBuf += sprintf( ptrMsgBuf, "TS_AFDone = %" PRId64 " \\ ", m_sAFInput.TS_AFDone);
        ptrMsgBuf += sprintf( ptrMsgBuf, "TS_MLStart= %" PRId64 " \\ ", m_sAFInput.TS_MLStart);
        ptrMsgBuf += sprintf( ptrMsgBuf, "PixelClk = %d \\ ", m_sAFInput.PixelClk);
        ptrMsgBuf += sprintf( ptrMsgBuf, "PixelInLine = %d \\ ", m_sAFInput.PixelInLine);
        CAM_LOGD("%s", dbgMsgBuf);
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::doAF( MVOID* /*ptrInAFData*/)
{
    prepareSPData();
    if( m_i4EnableAF==0)
    {
        m_sAFOutput.i4IsAfSearch = AF_SEARCH_DONE;
        m_sAFOutput.i4IsFocused = 0;
        m_sAFOutput.i4AFPos = 0;
        m_bRunPDEn = MTRUE;
        CAM_LOGD_IF( m_i4DgbLogLv, "disableAF");

        if (m_i4IsEnableFVInFixedFocus)
        {
            m_sArea_Focusing = m_sArea_OTFD;
            m_sArea_TypeSel  = (m_sArea_Focusing.i4W!=0 || m_sArea_Focusing.i4H!=0) ? AF_ROI_SEL_FD : AF_ROI_SEL_NONE;
        }
        return S_AF_OK;
    }

    char  dbgMsgBuf[DBG_MSG_BUF_SZ];
    char* ptrMsgBuf = dbgMsgBuf;
    if( m_i4DgbLogLv)
    {
        ptrMsgBuf += sprintf( ptrMsgBuf, "#(%5d,%5d) %s Dev(%d): ", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
    }

    MUINT32 i4curFrmNum = m_u4ReqMagicNum;

    AAA_TRACE_D("doAF #(%d,%d)", m_u4ReqMagicNum, m_u4StaMagicNum);

    //-------------------------------------------------------------------------------------------------------------
    if( m_ptrNVRam->rAFNVRAM.i4SceneMonitorLevel>0)
    {
        m_pIAfAlgo->setAEBlockInfo( m_aAEBlkVal, 25);
    }

    //----------------------------------Prepare AF Algorithm input data------------------------------------------------
    //==========
    // Stereo depth AF
    //==========
    // prepareSDData();

    //==========
    // Laser distance AF
    //==========
    prepareLDData();

    //==========
    //   PDAF
    //==========
    preparePDData();

    //---------------------------------------Run hybrid AF core flow---------------------------------------------
    if(IspMgrAFStatHWPreparing())
    {
        /* No need execute handleAF */
    }
    else if(m_i4DbgPDVerifyEn)
    {
        i4IsLockAERequest   = property_get_int32("vendor.debug.pdmgr.lockae", 0);
        m_i4DbgMotorDisable = property_get_int32("vendor.debug.af_motor.disable", 0);
        m_i4DbgMotorMPos    = property_get_int32("vendor.debug.af_motor.position", 1024);
        m_u8MvLensTS        = MoveLensTo( m_i4DbgMotorMPos, AF_MGR_CALLER);
    }
    else
    {
        //get current lens position.
        getLensInfo( m_sCurLensInfo);
        //Do AF is triggered when Vsync is came, so use previours lens information.
        m_sAFInput.sLensInfo = m_sCurLensInfo;
        if (m_i4DbgMotorDisable || m_i4DbgPDVerifyEn)
            m_sAFInput.sLensInfo.i4CurrentPos = m_sAFOutput.i4AFPos;
        //Pre-processing input data for hybrid AF.
        HybridAFPreprocessing();
        //select focusing ROI.
        m_sAFInput.sAFArea.i4Count  = 1;
        m_sAFInput.sAFArea.sRect[0] = SelROIToFocusing( m_sAFOutput);
        //set MZ infotmation.
        m_sAFInput.i4IsMZ = m_bMZAFEn;
        //select hybrid af behavior.
        m_sAFInput.i4HybridAFMode = GetHybridAFMode();
        //get ae relate information
        ISP_SENSOR_INFO_T* ptrSensorSetting = getMatchedISInfoFromFrameId(m_u4StaMagicNum);
        if(ptrSensorSetting->i4FrameId!=-1)
        {
            m_sAFInput.i4DeltaBV          = ptrSensorSetting->i4deltaIndex;
            m_sAFInput.i4IsAEidxReset     = ptrSensorSetting->bAEScenarioChange;
            m_sAFInput.u4AEFinerEVIdxBase = ptrSensorSetting->u4AEFinerEVIdxBase;
            m_sAFInput.u4AEidxCurrentF    = ptrSensorSetting->u4AEidxCurrentF;
        }
        //Run algorithm
        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf,
                                  "MZen(%d)/temp(%d)/hybrid mode(0x%x)/curPos(%4d)/",
                                  m_sAFInput.i4IsMZ,
                                  m_sAFInput.u4CurTemperature,
                                  m_sAFInput.i4HybridAFMode,
                                  m_sAFInput.sLensInfo.i4CurrentPos);
        }
        AAA_TRACE_D("handleAF");
        m_pIAfAlgo->handleAF( m_sAFInput, m_sAFOutput);
        AAA_TRACE_END_D;
        //Move lens position.
        AAA_TRACE_D("Move Lens (%d)", m_sAFOutput.i4AFPos);
        m_u8MvLensTS = MoveLensTo( m_sAFOutput.i4AFPos, AF_MGR_CALLER);
        AAA_TRACE_END_D;
        m_i4LensPosExit = m_sAFOutput.i4AFPos;
        //Update parameter for flow controlling
        m_i4IsSelHWROI_PreState = m_i4IsSelHWROI_CurState;
        m_i4IsSelHWROI_CurState = m_sAFOutput.i4IsSelHWROI;
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState;
        m_i4IsAFSearch_CurState = m_sAFOutput.i4IsAfSearch;

        //Update LockAERequest: lockAE when Contrast AF
        i4IsLockAERequest = (m_sAFOutput.i4IsAfSearch<=AF_SEARCH_TARGET_MOVE)? 0:1;

        // Warning for AdpAlarm
        if(m_sAFOutput.i4AdpAlarm)
        {
            // error log
            CAM_LOGE("#(%5d,%5d) %s Dev(%d) i4AdpAlarm(%d)",
                     m_u4ReqMagicNum,
                     m_u4StaMagicNum,
                     __FUNCTION__,
                     m_i4CurrSensorDev,
                     m_sAFOutput.i4AdpAlarm);
            if (property_get_int32("vendor.debug.af_mgr.adpalarm", 0) > 0)
            {
                // yellow screen (only enable after SQC1)
                AEE_ASSERT_AF("AF AdpAlarm");
            }
        }
    }

    //------------------------------------Configure/update HW setting----------------------------------
    IspMgrAFStatUpdateHw();

    //------------------------------------store debug information while searching----------------------
    if( m_i4IsAFSearch_CurState!=AF_SEARCH_DONE)
    {
        MUINT64 ts = getTimeStamp_us();
        WDBGTSInfo( MGR_TS, (MINT32)ts, 0);
    }

    //------------------------------------Update parameter for flow controlling----------------------------------
    m_i4IsFocused = m_sAFOutput.i4IsFocused;

    //------------------------------------Update parameter for depth AF----------------------------------
    updateSDParam();

    //------------------------------------Update parameter for PD----------------------------------
    UpdatePDParam( m_sAFOutput.i4ROISel);

    //Event
    if(  m_i4IsAFSearch_PreState!=m_i4IsAFSearch_CurState)
    {
        CAM_LOGD("#(%5d,%5d) %s Dev(%d) isAFSearch changed : %d -> %d",
                 m_u4ReqMagicNum,
                 m_u4StaMagicNum,
                 __FUNCTION__,
                 m_i4CurrSensorDev,
                 m_i4IsAFSearch_PreState,
                 m_i4IsAFSearch_CurState);
        if( m_i4IsAFSearch_CurState!=AF_SEARCH_DONE)
        {
            // Positive Edge: Searching Start
            if (m_pMcuDrv)
            {
                m_pMcuDrv->setMCUParam(MCU_CMD_OIS_DISABLE, MTRUE);
            }
            CleanMgrDbgInfo();
            CleanTSDbgInfo();

            // CONTINUOUS_VIDEO or CONTINUOUS_PICTURE ==> need to check if send callback
            if(m_bNeedCheckSendCallback)
            {
                if(m_bNeedLock)
                {
                    /* It have to send callback once got autofocus command from host */
                    m_bNeedSendCallback = 1;
                }
                else
                {
                    m_sCallbackInfo.isSearching        = m_i4IsAFSearch_CurState;
                    m_sCallbackInfo.CompSet_PDCL.Value = m_sAFOutput.i4PdTrigUiConf;
                    m_sCallbackInfo.CompSet_ISO.Value  = m_sAFInput.i4ISO;
                    m_sCallbackInfo.CompSet_FPS.Value  = (m_sAFInput.TS_AFDone - TS_AFDone_Pre > 0) ? 1000000/(m_sAFInput.TS_AFDone - TS_AFDone_Pre) : 0;
                    m_bNeedSendCallback |= checkSendCallback(m_sCallbackInfo); // isSearch switch between 1~3 still need SendCallback
                    CAM_LOGD("#(%5d,%5d) %s Dev(%d) checkSendCallback: m_bNeedSendCallback(%d), AfterAutoMode(%d), Searching(%d), PDCL(%d, %d), ISO(%d, %d), FPS(%d, %d)",
                             m_u4ReqMagicNum,
                             m_u4StaMagicNum,
                             __FUNCTION__,
                             m_i4CurrSensorDev,
                             m_bNeedSendCallback,
                             m_sCallbackInfo.isAfterAutoMode,
                             m_sCallbackInfo.isSearching,
                             m_sCallbackInfo.CompSet_PDCL.Value,m_sCallbackInfo.CompSet_PDCL.Target,
                             m_sCallbackInfo.CompSet_ISO.Value,m_sCallbackInfo.CompSet_ISO.Target,
                             m_sCallbackInfo.CompSet_FPS.Value,m_sCallbackInfo.CompSet_FPS.Target);
                }
                if(m_bNeedSendCallback==1 && m_sAFOutput.i4ROISel!=AF_ROI_SEL_FD)
                {
                    m_i4IsCAFWithoutFace = 1;   // CAFWithoutFace && NeedSendCallback
                }
            }
            else // else ==> always send callback
            {
                m_bNeedSendCallback = 1;
            }

            if(m_bNeedSendCallback)
            {
                UpdateState( EVENT_SEARCHING_START);
            }
        }
        else if(m_i4IsAFSearch_CurState==AF_SEARCH_DONE)
        {
            // Negative Edge: Searching End
            if (m_pMcuDrv)
            {
                m_pMcuDrv->setMCUParam(MCU_CMD_OIS_DISABLE, m_i4OISDisable);
            }
            SetMgrDbgInfo();

            if(m_bNeedLock)
            {
                /* It have to send callback once got autofocus command from host */
                m_bNeedSendCallback = 1;
            }

            if(m_bNeedSendCallback)
            {
                UpdateState( EVENT_SEARCHING_END);
                m_bNeedSendCallback=0;
                m_i4IsCAFWithoutFace = 0;
                m_bForceDoAlgo = MFALSE;
            }
            else
            {
                UpdateState( EVENT_SEARCHING_DONE_RESET_PARA); /* Hybrid AF : PDAF, LDAF, ... */
            }
            // The af search for LaunchCamTrigger is done
            if(m_i4LaunchCamTriggered==E_LAUNCH_AF_TRIGGERED)
            {
                m_i4LaunchCamTriggered = E_LAUNCH_AF_DONE;
            }
        }
    }


    if( (m_i4IsAFSearch_PreState!=m_i4IsAFSearch_CurState) ||
            (m_i4IsSelHWROI_PreState!=m_i4IsSelHWROI_CurState) ||
            (m_i4DgbLogLv))
    {
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d): [Status] IsAFSearch(%d->%d), IsSelHWROI(%d->%d), ROISel(%d), Event(0x%x)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_i4IsAFSearch_PreState,
                  m_i4IsAFSearch_CurState,
                  m_i4IsSelHWROI_PreState,
                  m_i4IsSelHWROI_CurState,
                  m_sAFOutput.i4ROISel,
                  m_eEvent);
    }

    m_i4DbgMotorDisable = property_get_int32("vendor.debug.af_motor.disable", 0);
    m_i4DbgMotorMPos    = property_get_int32("vendor.debug.af_motor.position", 1024);

    AAA_TRACE_END_D;

    if(m_i4LaunchCamTriggered_Prv==E_LAUNCH_AF_IDLE && m_i4LaunchCamTriggered==E_LAUNCH_AF_WAITING)
    {
        // positive edge
        AAA_TRACE_D("LaunchCamTrigger Start: #(%d,%d)", m_u4ReqMagicNum, m_u4StaMagicNum);
        AAA_TRACE_END_D;
    }
    else if(m_i4LaunchCamTriggered_Prv==E_LAUNCH_AF_TRIGGERED && m_i4LaunchCamTriggered==E_LAUNCH_AF_DONE)
    {
        // negetive edge
        AAA_TRACE_D("LaunchCamTrigger End: #(%d,%d)", m_u4ReqMagicNum, m_u4StaMagicNum);
        AAA_TRACE_END_D;
    }
    m_i4LaunchCamTriggered_Prv = m_i4LaunchCamTriggered;

    //---------------------------------------------------------------------------------------------------
    //get current sensor's temperature
    m_sAFInput.u4CurTemperature = getSensorTemperature();

    CAM_LOGD_IF( m_i4DgbLogLv, "%s %d", dbgMsgBuf, (MINT32)(ptrMsgBuf-dbgMsgBuf));
    return S_AF_OK;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_SENSOR_INFO_T* AfMgr::getMatchedISInfoFromFrameId(MINT32 frameId)
{
    m_sIspSensorInfo.i4FrameId = -1;

    MUINT32 front = m_vISQueue.head;
    MUINT32 end = m_vISQueue.tail;

    if (end > front)
        front += m_vISQueue.queueSize;

    // assume frameid is continuous, calculate idx by frameid difference
    MUINT32 offset = m_vISQueue.content[(front % m_vISQueue.queueSize)].i4FrameId - frameId;
    MUINT32 idx = (front-offset) % m_vISQueue.queueSize;

    int enableDebug = m_i4DbgAfegainQueue & 0x2;
    if (enableDebug)
    {
        CAM_LOGD("search afegain queue... magic: %d, head frameid: %d, idx: %d, indexed frameid: %d",
                 frameId, m_vISQueue.content[(front % m_vISQueue.queueSize)].i4FrameId, idx, m_vISQueue.content[idx].i4FrameId);
    }

    if (idx < m_vISQueue.queueSize && m_vISQueue.content[idx].i4FrameId == frameId) //found
    {
        memcpy(&m_sIspSensorInfo, &(m_vISQueue.content[idx]), sizeof(ISP_SENSOR_INFO_T));
    }
    else // fail by indexing, search one by one
    {
        if (enableDebug)
        {
            CAM_LOGD("search afegain queue... Failed by indexing, search one by one, found idx = %d", idx);
        }
        for (MUINT32 i=front; i>end; i--)
        {
            idx = i % m_vISQueue.queueSize;
            if (m_vISQueue.content[idx].i4FrameId == frameId) //found
                memcpy(&m_sIspSensorInfo, &(m_vISQueue.content[idx]), sizeof(ISP_SENSOR_INFO_T));
        }
    }
    // cannot get matched afegain from queue
    if (m_sIspSensorInfo.i4FrameId == -1)
    {
        CAM_LOGW("Cannot get matched afegain from m_vISQueue!");

        if (enableDebug)
        {
            front = m_vISQueue.head;
            end = m_vISQueue.tail;

            if (end > front)
                front += m_vISQueue.queueSize;
            for (MUINT32 i=front; i>end; i--)
            {
                MUINT32 idx = i % m_vISQueue.queueSize;
                CAM_LOGD("AfeGain qIdx(%d): frameId(%d), afeGain(%d), ispGain(%d), AEIdxCurrentF(%d)",
                         idx, m_vISQueue.content[idx].i4FrameId, m_vISQueue.content[idx].u4AfeGain, m_vISQueue.content[idx].u4IspGain, m_vISQueue.content[idx].u4AEidxCurrentF);
            }
        }
    }
    return &m_sIspSensorInfo;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MUINT32 AfMgr::getSensorTemperature()
{
    MUINT32 u4temperature = 0;

    if( m_ptrIHalSensor)
    {
        m_ptrIHalSensor->sendCommand( m_i4CurrSensorDev, SENSOR_CMD_GET_TEMPERATURE_VALUE, (MINTPTR)& u4temperature, 0, 0);
    }
    else
    {
        CAM_LOGE( "%s m_ptrIHalSensor is NULL", __FUNCTION__);
    }

    return u4temperature;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfMgr::sendAFNormalPipe( MINT32 cmd, MINTPTR arg1, MINTPTR arg2, MINTPTR arg3)
{
    MBOOL ret = MFALSE;

    INormalPipe* pPipe = (INormalPipe*)INormalPipeUtils::get()->createDefaultNormalPipe( m_i4SensorIdx, LOG_TAG);

    if( pPipe==NULL)
    {
        CAM_LOGE( "Fail to create NormalPipe");
    }
    else
    {
        ret = pPipe->sendCommand( cmd, arg1, arg2, arg3);
        pPipe->destroyInstance( LOG_TAG);
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::readOTP(CAMERA_CAM_CAL_TYPE_ENUM enCamCalEnum)
{
    MUINT32 result = 0;
    CAM_CAL_DATA_STRUCT GetCamCalData;
    CamCalDrvBase *pCamCalDrvObj = CamCalDrvBase::createInstance();
    MINT32 i4SensorDevID;

    CAM_LOGD("Read (%d) calibration data from EEPROM by camcal", enCamCalEnum);
    switch( m_i4CurrSensorDev)
    {
    case ESensorDev_Main:
        i4SensorDevID = SENSOR_DEV_MAIN;
        break;
    case ESensorDev_Sub:
        i4SensorDevID = SENSOR_DEV_SUB;
        break;
    case ESensorDev_MainSecond:
        i4SensorDevID = SENSOR_DEV_MAIN_2;
        break;
    case ESensorDev_SubSecond:
        i4SensorDevID = SENSOR_DEV_SUB_2;
        break;
    case ESensorDev_Main3D:
        i4SensorDevID = SENSOR_DEV_MAIN_3D;
        return S_AWB_OK;
    default:
        i4SensorDevID = SENSOR_DEV_NONE;
        return S_AWB_OK;
    }

    result = pCamCalDrvObj->GetCamCalCalData(i4SensorDevID, enCamCalEnum, (void *)&GetCamCalData);
    CAM_LOGD_IF( m_i4DgbLogLv, "(0x%8x)=pCamCalDrvObj->GetCamCalCalData", result);

    if (enCamCalEnum == CAMERA_CAM_CAL_DATA_3A_GAIN)
    {
        if (result&CamCalReturnErr[enCamCalEnum])
        {
            CAM_LOGD( "err (%s)", CamCalErrString[enCamCalEnum]);
            //return E_AF_NOSUPPORT;
        }

        CAM_LOGD_IF( m_i4DgbLogLv,
                     "OTP data [S2aBitEn]%d [S2aAfBitflagEn]%d [S2aAf0]%d [S2aAf1]%d",
                     GetCamCalData.Single2A.S2aBitEn,
                     GetCamCalData.Single2A.S2aAfBitflagEn,
                     GetCamCalData.Single2A.S2aAf[0],
                     GetCamCalData.Single2A.S2aAf[1]);

        m_i4InfPos    = GetCamCalData.Single2A.S2aAf[0];
        m_i4MacroPos  = GetCamCalData.Single2A.S2aAf[1];
        m_i450cmPos   = GetCamCalData.Single2A.S2aAf[3];
        m_i4MiddlePos = GetCamCalData.Single2A.S2aAF_t.AF_Middle_calibration;

        if( 0<m_i4InfPos && m_i4MacroPos<1024 && m_i4MacroPos>m_i4InfPos )
        {
            if( (m_i4MacroPos>m_i450cmPos) && (m_i450cmPos>m_i4InfPos) )
            {
                if (m_bLDAFEn == MTRUE)
                {
                    ILaserMgr::getInstance().setLensCalibrationData(m_i4CurrSensorDev, m_i4MacroPos, m_i450cmPos);
                }

                CAM_LOGD( "OTP [50cm]%d", m_i450cmPos);
            }

            if( m_i4MiddlePos>m_i4InfPos && m_i4MiddlePos<m_i4MacroPos)
            {
                CAM_LOGD( "Middle OTP cal:%d\n", m_i4MiddlePos);
            }
            else
            {
                m_i4MiddlePos = m_i4InfPos + (m_i4MacroPos - m_i4InfPos) * m_ptrNVRam->rAFNVRAM.i4DualAFCoefs[1] / 100;
                CAM_LOGD( "Middle OTP adjust:%d NVRAM:%d INF:%d Mac:%d\n", m_i4MiddlePos, m_ptrNVRam->rAFNVRAM.i4DualAFCoefs[1],m_i4InfPos,m_i4MacroPos);
            }

            m_i4AFTabStr=0; // get AF table start & end from updateAFtableBoundary
            m_i4AFTabEnd=0;
#if 0// fix build error #if !CXUAF_ENABLE
            if( m_pIAfAlgo)
            {
                m_pIAfAlgo->updateAFtableBoundary( m_i4InfPos, m_i4MacroPos);
                m_pIAfAlgo->updateMiddleAFPos(m_i4MiddlePos);
            }
#endif
            if(m_ptrNVRam->rAFNVRAM.i4EasyTuning[0] == 10000 && m_i4LensPosExit != 0)
            {
                m_i4InitPos = m_i4LensPosExit;
            }
            else
            {
                m_i4InitPos = m_i4InfPos + (m_i4MacroPos - m_i4InfPos) * m_ptrNVRam->rAFNVRAM.i4EasyTuning[0] / 100;
                m_i4InitPos = (m_i4InitPos < 0) ? 0 : ((m_i4InitPos > 1023) ? 1023 : m_i4InitPos);
            }

            // adjust depth dac_min, dac_max
            m_sDAF_TBL.af_dac_inf         = m_i4InfPos;
            m_sDAF_TBL.af_dac_marco       = m_i4MacroPos;
            m_sDAF_TBL.af_dac_min         = m_i4InfPos;
            m_sDAF_TBL.af_dac_max         = m_i4MacroPos;

            if( (m_i4AFTabStr >0) && ( m_i4AFTabEnd > m_i4AFTabStr))
            {
                m_sDAF_TBL.af_dac_min    = m_i4AFTabStr;
                m_sDAF_TBL.af_dac_max    = m_i4AFTabEnd;
            }

            m_sDAF_TBL.af_distance_inf   = GetCamCalData.Single2A.S2aAF_t.AF_infinite_pattern_distance;
            m_sDAF_TBL.af_distance_marco = GetCamCalData.Single2A.S2aAF_t.AF_Macro_pattern_distance;
            CAM_LOGD( "AF-%-15s: calibrated data [af_dac_inf]%d [af_dac_marco]%d [af_dac_min]%d [af_dac_max]%d [af_distance_inf]%d [af_distance_marco]%d\n",
                      __FUNCTION__,
                      (MINT32)m_sDAF_TBL.af_dac_inf,
                      (MINT32)m_sDAF_TBL.af_dac_marco,
                      (MINT32)m_sDAF_TBL.af_dac_min,
                      (MINT32)m_sDAF_TBL.af_dac_max,
                      (MINT32)m_sDAF_TBL.af_distance_inf,
                      (MINT32)m_sDAF_TBL.af_distance_marco);

            CAM_LOGD( "%s : [Inf]%d [Macro]%d [50cm]%d [InitPos]%d", __FUNCTION__, m_i4InfPos, m_i4MacroPos, m_i450cmPos, m_i4InitPos);
        }
        else
        {
            CAM_LOGD( "%s : cam_cal is abnormal [Inf]%d [Macro]%d",
                      __FUNCTION__,
                      m_i4InfPos,
                      m_i4MacroPos);
        }
    }
    else if (enCamCalEnum == CAMERA_CAM_CAL_DATA_PDAF)
    {
        if (result==CAM_CAL_ERR_NO_ERR)
        {
            memcpy( m_ptrAfPara->rPdCaliData.uData, GetCamCalData.PDAF.Data, sizeof(MUINT8)*PD_CALI_DATA_SIZE);
            m_ptrAfPara->rPdCaliData.i4Size = GetCamCalData.PDAF.Size_of_PDAF;

            CAM_LOGD("Size of PD calibration data = %d", m_ptrAfPara->rPdCaliData.i4Size);
            CAM_LOGD_IF(m_i4DgbLogLv&2, "pd calibration data:");
            for (int index = 0; index < m_ptrAfPara->rPdCaliData.i4Size;)
            {
                CAM_LOGD_IF(m_i4DgbLogLv&2, "%02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n",
                            m_ptrAfPara->rPdCaliData.uData[index+ 0], m_ptrAfPara->rPdCaliData.uData[index+ 1],
                            m_ptrAfPara->rPdCaliData.uData[index+ 2], m_ptrAfPara->rPdCaliData.uData[index+ 3],
                            m_ptrAfPara->rPdCaliData.uData[index+ 4], m_ptrAfPara->rPdCaliData.uData[index+ 5],
                            m_ptrAfPara->rPdCaliData.uData[index+ 6], m_ptrAfPara->rPdCaliData.uData[index+ 7],
                            m_ptrAfPara->rPdCaliData.uData[index+ 8], m_ptrAfPara->rPdCaliData.uData[index+ 9],
                            m_ptrAfPara->rPdCaliData.uData[index+10], m_ptrAfPara->rPdCaliData.uData[index+11],
                            m_ptrAfPara->rPdCaliData.uData[index+12], m_ptrAfPara->rPdCaliData.uData[index+13],
                            m_ptrAfPara->rPdCaliData.uData[index+14], m_ptrAfPara->rPdCaliData.uData[index+15]);
                index += 16;
            }
        }
        else
        {
            CAM_LOGE("FAILED getting pd calibration data from GetCamCalCalData()!");
        }
    }

    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::ConvertDMABufToStat( MINT32 &i4CurPos, MVOID *ptrInStatBuf, AF_STAT_PROFILE_T &sOutSata)
{
    StatisticBufInfo *ptrStatInfo = reinterpret_cast<StatisticBufInfo *>( ptrInStatBuf);
    AF_STAT_T* pDMABuf            = reinterpret_cast<AF_STAT_T*>( ptrStatInfo->mVa);
    MUINT32 u4BlockStatSize       = sizeof(AF_STAT_T);                                      // (unit:Bytes) Statistics Data : 288bits=36bytes
    MUINT32 u4BlockStrideOfDMABuf = ptrStatInfo->mStride/u4BlockStatSize;                   // (unit:Blocks)BlockNumX + 1 (1 comes from the requirement of twin mode)
    MUINT32 u4FullStatSize        = ptrStatInfo->mSize;                                     // (unit:Bytes) MAX_AF_HW_WIN_X*MAX_AF_HW_WIN_Y*u4StatSizePerBlock

    CAM_LOGD_IF( m_i4DgbLogLv,
                 "#(%5d,%5d) %s(%d) Latency(%d) BlkW,H(%3d,%3d blocks) Str(%3d blocks) BlkSzW,H(%4d,%4d pixels) FullSz(%d bytes)",
                 m_u4ReqMagicNum, m_u4StaMagicNum,
                 __FUNCTION__, m_u4ConfigHWNum, m_u4ConfigLatency,
                 m_i4HWBlkNumX, m_i4HWBlkNumY,   // Width, Height (blocks)
                 u4BlockStrideOfDMABuf,          // Stride (blocks)
                 m_i4HWBlkSizeX, m_i4HWBlkSizeY, // Width, Height (pixels)
                 u4FullStatSize);                // (bytes)

    //reset last time data
    memset( &sOutSata, 0, sizeof(AF_STAT_PROFILE_T));

    if( m_i4HWBlkNumX<=0 || m_i4HWBlkNumY<=0 || m_i4HWBlkNumX>MAX_AF_HW_WIN_X || m_i4HWBlkNumY>MAX_AF_HW_WIN_Y)
    {
        //Should not be happened.
        CAM_LOGE( "HW-Setting Fail");
    }
    else if( pDMABuf == NULL)
    {
        //Should not be happened.
        CAM_LOGE( "AFO Buffer NULL");
    }
    else if( u4FullStatSize < m_i4HWBlkNumX*m_i4HWBlkNumY*u4BlockStatSize)
    {
        //Should not be happened.
        CAM_LOGE( "AFO Size Fail");
    }
    else
    {
        //number of AF statistic blocks.
        MUINT32 nblkW = m_i4HWBlkNumX;
        MUINT32 nblkH = m_i4HWBlkNumY;

        //==========
        // Outputs
        //==========
        // AF extend mode
        sOutSata.u4AfExtValid = m_i4HWEnExtMode;
        sOutSata.u4ConfigNum  = m_u4ConfigHWNum;
        sOutSata.i4AFPos      = i4CurPos;

        //statistic information.
        sOutSata.u4NumBlkX   = m_i4HWBlkNumX;        // unit: blocks
        sOutSata.u4NumBlkY   = m_i4HWBlkNumY;        // unit: blocks
        sOutSata.u4NumStride = u4BlockStrideOfDMABuf; // unit: blocks
        sOutSata.u4SizeBlkX  = m_i4HWBlkSizeX;       // unit: pixels
        sOutSata.u4SizeBlkY  = m_i4HWBlkSizeY;       // unit: pixels

        //AF statistic - ISP5.0
        sOutSata.ptrStat = pDMABuf;


        MINT32 Blk_FaceLeft = 0;
        MINT32 Blk_FaceRight = 0;
        MINT32 Blk_FaceTop = 0;
        MINT32 Blk_FaceBottom = 0;

        if (m_i4IsEnableFVInFixedFocus)
        {
            MINT32 FaceLeft    = m_sArea_OTFD.i4X;
            MINT32 FaceRight   = m_sArea_OTFD.i4X + m_sArea_OTFD.i4W;
            MINT32 FaceTop     = m_sArea_OTFD.i4Y;
            MINT32 FaceBottom  = m_sArea_OTFD.i4Y + m_sArea_OTFD.i4H;

            Blk_FaceLeft    = FaceLeft   / m_i4HWBlkSizeX;
            Blk_FaceRight   = FaceRight  / m_i4HWBlkSizeX;
            Blk_FaceTop     = FaceTop    / m_i4HWBlkSizeY;
            Blk_FaceBottom  = FaceBottom / m_i4HWBlkSizeY;

            CAM_LOGD_IF(m_i4DgbLogLv, "#(%5d,%5d) %s FVinFixedFocus FaceROI(L,R,T,B)=(%d,%d,%d,%d), Blk_FaceROI(L,R,T,B)=(%d,%d,%d,%d), BlkNum(%d,%d), BlkSize(%d,%d)",
                        m_u4ReqMagicNum,
                        m_u4StaMagicNum,
                        __FUNCTION__,
                        FaceLeft, FaceRight, FaceTop, FaceBottom,
                        Blk_FaceLeft, Blk_FaceRight, Blk_FaceTop, Blk_FaceBottom,
                        m_i4HWBlkNumX, m_i4HWBlkNumY,
                        m_i4HWBlkSizeX, m_i4HWBlkSizeY);
        }

        if(m_i4DgbLogLv || m_i4IsEnableFVInFixedFocus)
        {
            MUINT64 FV_H0=0, FV_H1=0, FV_H2=0, FV_V=0;
            MUINT64 FV_H0_FVinFixedFocus=0;
            // block stride = BlockNumberX + 1, 1 comes from the requirement of twin mode
            MUINT32 NumBlkStride = sOutSata.u4NumStride;
            for(MUINT32 row=0; row<sOutSata.u4NumBlkY; row++)
            {
                for(MUINT32 col=0; col<sOutSata.u4NumBlkX; col++)
                {
                    AF_STAT_T* ptrStat = sOutSata.ptrStat + (row*NumBlkStride + col);
                    FV_H0 += ptrStat->u4FILH0;
                    FV_H1 += ptrStat->u4FILH1;
                    FV_H2 += ptrStat->u4FILH2;
                    FV_V  += ptrStat->u4FILV;

                    if (m_i4IsEnableFVInFixedFocus)
                    {
                        if(row>=Blk_FaceTop && row<=Blk_FaceBottom && col>=Blk_FaceLeft && col<=Blk_FaceRight)
                        {
                            FV_H0_FVinFixedFocus += ptrStat->u4FILH0;
                        }
                        m_sAFOutput.i8AFValue = FV_H0_FVinFixedFocus;
                    }
                    // debug : stt for each block
                    CAM_LOGD_IF( m_i4DgbLogLv&0x2,
                                 "[%3d][%3d] : [H0]%8d, [H1]%8d, [H2]%8d, [V]%8d, [GSum]%8d",
                                 col, row,
                                 ptrStat->u4FILH0,
                                 ptrStat->u4FILH1,
                                 ptrStat->u4FILH2,
                                 ptrStat->u4FILV,
                                 ptrStat->u4GSum);
                }
            }
            CAM_LOGD_IF((m_i4IsEnableFVInFixedFocus&&m_i4DgbLogLv), "#(%5d,%5d) %s FVinFixedFocus = %10llu",
                        m_u4ReqMagicNum,
                        m_u4StaMagicNum,
                        __FUNCTION__,
                        FV_H0_FVinFixedFocus);
            // debug : stt for sum of blocks excluding the spare block.
            CAM_LOGD_IF(m_i4DgbLogLv,
                        "#(%5d,%5d) %s(%d) [Pos]%4d [H0]%10llu [H1]%10llu [H2_EXT]%10llu [V]%llu",
                        m_u4ReqMagicNum,
                        m_u4StaMagicNum,
                        __FUNCTION__,
                        m_u4ConfigHWNum,
                        i4CurPos,
                        (unsigned long long)FV_H0,
                        (unsigned long long)FV_H1,
                        (unsigned long long)FV_H2,
                        (unsigned long long)FV_V);
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::HybridAFPreprocessing()
{
    char  dbgMsgBuf[DBG_MSG_BUF_SZ];
    char* ptrMsgBuf = dbgMsgBuf;

    if( m_i4DgbLogLv)
    {
        ptrMsgBuf += sprintf( ptrMsgBuf, "#(%5d,%5d) %s Dev(%d) PDEn(%d) LaserEn(%d) FDDetect(%d): ",
                              m_u4ReqMagicNum,
                              m_u4StaMagicNum,
                              __FUNCTION__,
                              m_i4CurrSensorDev,
                              m_bEnablePD,
                              m_bLDAFEn,
                              m_sAFOutput.i4FDDetect);
    }


    /* for reference */
    AREA_T LastAFArea   = m_sArea_Focusing;
    MINT32 i4ISO        = m_sAFInput.i4ISO;
    MINT32 i4IsAEStable = m_sAFInput.i4IsAEStable;
    MUINT8 AEBlockV[25];
    memcpy( AEBlockV, m_aAEBlkVal, 25);

    /* do PD data preprocessing */
    if( m_bEnablePD)
    {
        MINT32   PDOut_numRes = m_sAFInput.sPDInfo.i4PDBlockInfoNum;
        MUINT16 pPDAF_DAC = 0;
        MUINT16 pPDAF_Conf = 0;

        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf, "[PD (DAC, CL)] org{ ");
            for( MINT32 i=0; i<PDOut_numRes; i++)
            {
                pPDAF_DAC = m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafDacIndex;
                pPDAF_Conf = m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafConfidence;
                ptrMsgBuf += sprintf( ptrMsgBuf, "#%d(%d, %d) ", i, pPDAF_DAC, pPDAF_Conf);
            }
            ptrMsgBuf += sprintf( ptrMsgBuf, "}, ");
        }

        /*****************************  PD result preprocessing (start) *****************************/
#if 0
        {
            // To Do :
            // customer can process PD result here for customization.
        }
#endif
        /*****************************  PD result preprocessing (end)  *****************************/

        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf, "new{ ");
            for( MINT32 i=0; i<PDOut_numRes; i++)
            {
                pPDAF_DAC = m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafDacIndex;
                pPDAF_Conf = m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafConfidence;
                ptrMsgBuf += sprintf( ptrMsgBuf, "#%d(%d, %d) ", i, pPDAF_DAC, pPDAF_Conf);
            }
            ptrMsgBuf += sprintf( ptrMsgBuf, "}, ");
        }

        // LaunchCamTrigger
        if(m_i4LaunchCamTriggered == E_LAUNCH_AF_WAITING)
        {
            if(m_i4ValidPDFrameCount==-1 && PDOut_numRes>0)
            {
                for(MINT32 i=0; i<PDOut_numRes; i++)
                {
                    pPDAF_Conf = m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafConfidence;
                    if(pPDAF_Conf > 0)
                    {
                        m_i4ValidPDFrameCount = m_u4ReqMagicNum;
                        CAM_LOGD("#(%5d,%5d) %s Dev(%d) LaunchCamTrigger VALID PD(%d) - BUT NOT TRIGGER YET",
                                 m_u4ReqMagicNum, m_u4StaMagicNum,
                                 __FUNCTION__,
                                 m_i4CurrSensorDev,
                                 m_i4ValidPDFrameCount);
                        break;
                    }
                }
            }

            if(m_i4ValidPDFrameCount != -1 && m_u4ReqMagicNum >= (m_i4ValidPDFrameCount + m_i4ValidPDTriggerTimeout))
            {
                // Valid PD and m_i4ValidPDTriggerTimeout(for waiting Face)
                m_i4LaunchCamTriggered = E_LAUNCH_AF_TRIGGERED;
                CAM_LOGD( "#(%5d,%5d) %s Dev(%d) LaunchCamTrigger VALID PD(%d + %d) - UnlockAlgo + TRIGGERAF lib_afmode(%d)",
                          m_u4ReqMagicNum, m_u4StaMagicNum,
                          __FUNCTION__,
                          m_i4CurrSensorDev,
                          m_i4ValidPDFrameCount, m_i4ValidPDTriggerTimeout,
                          m_eLIB3A_AFMode);
                UnlockAlgo();
                triggerAF(AF_MGR_CALLER);
                m_i4IsLockForLaunchCamTrigger = 0;
            }

        }
    }
    else
    {
        // To set the target to make sure the ROI always be drawn with Contrast AF.
        m_sCallbackInfo.CompSet_PDCL.Target = 101;  // value > target ==> no draw
    }

    /* do laser data preprocessing */
    if( m_bLDAFEn == MTRUE)
    {
        MINT32 LaserStatus = ILaserMgr::getInstance().getLaserCurStatus(m_i4CurrSensorDev);

        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf, "[Laser %d (DAC, CONF, DIST)] org:(%d, %d, %d) ",
                                  LaserStatus,
                                  m_sAFInput.sLaserInfo.i4CurPosDAC,
                                  m_sAFInput.sLaserInfo.i4Confidence,
                                  m_sAFInput.sLaserInfo.i4CurPosDist);
        }

        /*****************************  Laser result preprocessing (start) *****************************/
        {
            switch( LaserStatus)
            {
            case STATUS_RANGING_VALID:
                m_sAFInput.sLaserInfo.i4Confidence = 80;
                break;

            case STATUS_MOVE_DMAX:
            case STATUS_MOVE_MAX_RANGING_DIST:
                m_sAFInput.sLaserInfo.i4Confidence = 49;
                break;

            default:
                m_sAFInput.sLaserInfo.i4Confidence = 20;
                break;
            }

            //Touch AF : if the ROI isn't in the center, the laser data need to set low confidence.
            if( (m_eLIB3A_AFMode == LIB3A_AF_MODE_AFS) || (m_eLIB3A_AFMode == LIB3A_AF_MODE_MACRO))
            {
                if( m_sAFInput.sLaserInfo.i4AfWinPosX >= LASER_TOUCH_REGION_W ||
                        m_sAFInput.sLaserInfo.i4AfWinPosY >= LASER_TOUCH_REGION_H ||
                        m_sAFInput.sLaserInfo.i4AfWinPosCnt > 1 )
                {
                    m_sAFInput.sLaserInfo.i4Confidence = 20;
                    m_sAFInput.sLaserInfo.i4CurPosDAC  = 0;
                }
            }

            if( 1 == m_sAFOutput.i4FDDetect)
            {
                m_sAFInput.sLaserInfo.i4Confidence = 20;
                m_sAFInput.sLaserInfo.i4CurPosDAC  = 0;
            }
        }
        /*****************************  Laser result preprocessing (end)  *****************************/


        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf, "[Laser %d (DAC, CONF, DIST)] new:(%d, %d, %d) ",
                                  LaserStatus,
                                  m_sAFInput.sLaserInfo.i4CurPosDAC,
                                  m_sAFInput.sLaserInfo.i4Confidence,
                                  m_sAFInput.sLaserInfo.i4CurPosDist);
        }
    }

    CAM_LOGD_IF( m_i4DgbLogLv, "%s", dbgMsgBuf);
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MUINT32 AfMgr::GetHybridAFMode()
{
    MUINT32 HybridAFMode = 0;

    //Depth AF
    if ((m_sDAF_TBL.is_daf_run & E_DAF_RUN_DEPTH_ENGINE) && (m_bSDAFEn == MTRUE))
    {
        HybridAFMode |= 1;
    }

    //PDAF
    if(m_bEnablePD)
    {
        HybridAFMode |= 2;  //2'b 0010
    }

    //Laser AF
    if (m_bLDAFEn == MTRUE)
    {
        HybridAFMode |= 4;
    }

    CAM_LOGD_IF( m_i4DgbLogLv&4, "%s:(0x%x)", __FUNCTION__, HybridAFMode);

    return HybridAFMode;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::PrintHWRegSetting( AF_CONFIG_T &sAFHWCfg)
{
    CAM_LOGD( "HW-TGSZ %d, %d, BINSZ %d, %d",
              sAFHWCfg.sTG_SZ.i4W,
              sAFHWCfg.sTG_SZ.i4H,
              sAFHWCfg.sBIN_SZ.i4W,
              sAFHWCfg.sBIN_SZ.i4H);

    CAM_LOGD( "HW-sROI %d, %d %d, %d %d",
              sAFHWCfg.sRoi.i4X,
              sAFHWCfg.sRoi.i4Y,
              sAFHWCfg.sRoi.i4W,
              sAFHWCfg.sRoi.i4H,
              sAFHWCfg.sRoi.i4Info);

    CAM_LOGD( "HW-nBLK %d, %d",
              sAFHWCfg.AF_BLK_XNUM,
              sAFHWCfg.AF_BLK_YNUM);

    CAM_LOGD( "HW-SGG %d, %d, %d, %d, %d, %d, %d, %d",
              sAFHWCfg.i4SGG_GAIN,
              sAFHWCfg.i4SGG_GMR1,
              sAFHWCfg.i4SGG_GMR2,
              sAFHWCfg.i4SGG_GMR3,
              sAFHWCfg.i4SGG_GMR4,
              sAFHWCfg.i4SGG_GMR5,
              sAFHWCfg.i4SGG_GMR6,
              sAFHWCfg.i4SGG_GMR7);


    CAM_LOGD( "HW-HVGL %d, %d, %d",
              sAFHWCfg.AF_H_GONLY,
              sAFHWCfg.AF_V_GONLY,
              sAFHWCfg.AF_V_AVG_LVL);

    CAM_LOGD( "HW-BLF %d, %d, %d, %d",
              sAFHWCfg.AF_BLF[0],
              sAFHWCfg.AF_BLF[1],
              sAFHWCfg.AF_BLF[2],
              sAFHWCfg.AF_BLF[3]);

    CAM_LOGD( "HW-TH %d, %d, %d, %d",
              sAFHWCfg.AF_TH_H[0],
              sAFHWCfg.AF_TH_H[1],
              sAFHWCfg.AF_TH_V,
              sAFHWCfg.AF_TH_G_SAT);

    CAM_LOGD( "HW-FIL0 %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
              sAFHWCfg.AF_FIL_H0[0],
              sAFHWCfg.AF_FIL_H0[1],
              sAFHWCfg.AF_FIL_H0[2],
              sAFHWCfg.AF_FIL_H0[3],
              sAFHWCfg.AF_FIL_H0[4],
              sAFHWCfg.AF_FIL_H0[5],
              sAFHWCfg.AF_FIL_H0[6],
              sAFHWCfg.AF_FIL_H0[7],
              sAFHWCfg.AF_FIL_H0[8],
              sAFHWCfg.AF_FIL_H0[9],
              sAFHWCfg.AF_FIL_H0[10],
              sAFHWCfg.AF_FIL_H0[11]);

    CAM_LOGD( "HW-FIL1 %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d",
              sAFHWCfg.AF_FIL_H1[0],
              sAFHWCfg.AF_FIL_H1[1],
              sAFHWCfg.AF_FIL_H1[2],
              sAFHWCfg.AF_FIL_H1[3],
              sAFHWCfg.AF_FIL_H1[4],
              sAFHWCfg.AF_FIL_H1[5],
              sAFHWCfg.AF_FIL_H1[6],
              sAFHWCfg.AF_FIL_H1[7],
              sAFHWCfg.AF_FIL_H1[8],
              sAFHWCfg.AF_FIL_H1[9],
              sAFHWCfg.AF_FIL_H1[10],
              sAFHWCfg.AF_FIL_H1[11]);

    CAM_LOGD( "HW-FILV %d, %d, %d, %d",
              sAFHWCfg.AF_FIL_V[0],
              sAFHWCfg.AF_FIL_V[1],
              sAFHWCfg.AF_FIL_V[2],
              sAFHWCfg.AF_FIL_V[3]);

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfMgr::ConfigHWReg( AF_CONFIG_T &sInHWCfg, AREA_T &sOutHWROI, MINT32 &i4OutHWBlkNumX, MINT32 &i4OutHWBlkNumY, MINT32 &i4OutHWBlkSizeX, MINT32 &i4OutHWBlkSizeY)
{
    //-------------
    // AF HAL control flow :
    //-------------
    if (m_i4BINSzW != 0 && m_i4TGSzW != 0)
    {
        sInHWCfg.sTG_SZ.i4W  = m_i4TGSzW;
        sInHWCfg.sTG_SZ.i4H  = m_i4TGSzH;
        sInHWCfg.sBIN_SZ.i4W = m_i4BINSzW;
        sInHWCfg.sBIN_SZ.i4H = m_i4BINSzH;
    }
    else
    {
        CAM_LOGE( "%s : [TG_SZ]%d %d [BIN_SZ]%d %d -> [TG_SZ]%d %d [BIN_SZ]%d %d",
                  __FUNCTION__,
                  m_i4TGSzW,
                  m_i4TGSzH,
                  m_i4BINSzW,
                  m_i4BINSzH,
                  sInHWCfg.sTG_SZ.i4W,
                  sInHWCfg.sTG_SZ.i4H,
                  sInHWCfg.sBIN_SZ.i4W,
                  sInHWCfg.sBIN_SZ.i4H);
    }

    CAM_LOGD_IF( m_i4DgbLogLv,
                 "#(%5d,%5d) %s Dev(%d): set(X,Y,W,H)=( %d, %d, %d, %d)",
                 m_u4ReqMagicNum,
                 m_u4StaMagicNum,
                 __FUNCTION__,
                 m_i4CurrSensorDev,
                 sInHWCfg.sRoi.i4X,
                 sInHWCfg.sRoi.i4Y,
                 sInHWCfg.sRoi.i4W,
                 sInHWCfg.sRoi.i4H);

    /**
     * configure HW :
     * Output parameters :
     * Because HW constraint is applied, HW setting maybe be changed.
     * sOutHWROI is used to align HW analyzed region to any other algorithm, for example, phase difference algorithm.
     */
    ConfigAFOutput_T ConfigOutput;

    ConfigOutput.hwConfigInfo.hwArea      = &m_sArea_HW;
    ConfigOutput.hwConfigInfo.hwBlkNumX   = &m_i4HWBlkNumX;
    ConfigOutput.hwConfigInfo.hwBlkNumY   = &m_i4HWBlkNumY;
    ConfigOutput.hwConfigInfo.hwBlkSizeX  = &m_i4HWBlkSizeX;
    ConfigOutput.hwConfigInfo.hwBlkSizeY  = &m_i4HWBlkSizeY;
    ConfigOutput.hwConfigInfo.hwEnExtMode = &m_i4HWEnExtMode;
    ConfigOutput.resultConfig             = &m_sAFResultConfig;

    ISP_AF_CONFIG_T::getInstance(m_i4CurrSensorDev).AFConfig(&sInHWCfg, &ConfigOutput.hwConfigInfo, ConfigOutput.resultConfig);

    // error log : should not be happened.
    if( sInHWCfg.AF_BLK_XNUM != i4OutHWBlkNumX  ||
            sInHWCfg.AF_BLK_YNUM != i4OutHWBlkNumY )
    {
        CAM_LOGE( "WAR-ROI : [X]%d [Y]%d [W]%d [H]%d -> [X]%d [Y]%d [W]%d [H]%d, [XNUM] %d->%d, [YNUM] %d->%d",
                  sInHWCfg.sRoi.i4X,
                  sInHWCfg.sRoi.i4Y,
                  sInHWCfg.sRoi.i4W,
                  sInHWCfg.sRoi.i4H,
                  sOutHWROI.i4X,
                  sOutHWROI.i4Y,
                  sOutHWROI.i4W,
                  sOutHWROI.i4H,
                  sInHWCfg.AF_BLK_XNUM,
                  i4OutHWBlkNumX,
                  sInHWCfg.AF_BLK_YNUM,
                  i4OutHWBlkNumY);
    }

    return S_AF_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::ApplyZoomEffect( AREA_T &sOutAFRegion)
{
    // zoom effect of ROI is configured from customized parameter
    m_i4DzWinCfg = (MUINT32)m_ptrNVRam->rAFNVRAM.i4ZoomInWinChg;

    // error check
    if(      m_i4DzWinCfg<1) m_i4DzWinCfg = 1;
    else if( 4<m_i4DzWinCfg) m_i4DzWinCfg = 4;

    // scale should be same as m_i4DZFactor
    m_i4DzWinCfg *= 100;
    CAM_LOGD_IF( m_i4DgbLogLv&2,
                 "%s [DZ]%d, [Cfg]%d",
                 __FUNCTION__,
                 m_i4DZFactor,
                 m_i4DzWinCfg);


    if( m_i4DzWinCfg==400)
    {
        // i4WinCfg=4-> DigZoomFac>4, AF win align digital effect.
    }
    else if( m_bMZAFEn==MFALSE)
    {
        /**
         * i4WinCfg = 1~3
         * i4WinCfg = 1 : DigZoomFac>1, AF win no change
         * i4WinCfg = 2 : DigZoomFac>2, AF win no change
         * i4WinCfg = 3 : DigZoomFac>3, AF win no change
         */
        if( m_i4DzWinCfg <= m_i4DZFactor) /* fix to upper bound */
        {

            //scale up window.
            AREA_T scaledWin;
            scaledWin.i4W = sOutAFRegion.i4W*m_i4DZFactor/m_i4DzWinCfg;
            scaledWin.i4H = sOutAFRegion.i4H*m_i4DZFactor/m_i4DzWinCfg;
            scaledWin.i4X = (sOutAFRegion.i4X + sOutAFRegion.i4W/2) -  scaledWin.i4W/2;
            scaledWin.i4Y = (sOutAFRegion.i4Y + sOutAFRegion.i4H/2) -  scaledWin.i4H/2;

            CAM_LOGD_IF( m_i4DgbLogLv&2,
                         "%s [W]%d [H]%d [X]%d [Y]%d -> [W]%d [H]%d [X]%d [Y]%d",
                         __FUNCTION__,
                         scaledWin.i4W,
                         scaledWin.i4H,
                         scaledWin.i4X,
                         scaledWin.i4Y,
                         sOutAFRegion.i4W,
                         sOutAFRegion.i4H,
                         sOutAFRegion.i4X,
                         sOutAFRegion.i4Y );

            sOutAFRegion = scaledWin;
        }
        else /* (i4DzFactor < i4WinCfg*100), AF win change aligning to digital zoom factor */
        {
            //CAM_LOGD("[applyZoomInfo] <bound2DZ, DZ=%d, Bound=%d \n",i4DzFactor,i4WinCfg*100);
        }
    }


    //CAM_LOGD("[applyZoomInfo] >bound2fix, DZ=%d, Bound=%d\n",i4DzFactor,i4WinCfg*100);

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::setOTFDInfo( MVOID* sInROIs, MINT32 i4Type)
{
    MRESULT ret=E_3A_ERR;

    /*TG base coordinate*/
    MtkCameraFaceMetadata *ptrWins = (MtkCameraFaceMetadata *)sInROIs;

    //prepare information and set to hybrid AF
    AREA_T OriFDArea = AREA_T( 0, 0, 0, 0, AF_MARK_NONE);

    AF_AREA_T sAreaInfo;
    sAreaInfo.i4Score  = 0;
    sAreaInfo.i4Count  = 0;
    sAreaInfo.sRect[0] = AREA_T( 0, 0, 0, 0, AF_MARK_NONE);

    if( ptrWins!=NULL)
    {
        //=== Portrait ===//
        sAreaInfo.i4PortEnable = (MINT32)(ptrWins->CNNFaces.PortEnable);
        sAreaInfo.i4IsTrueFace = (MINT32)(ptrWins->CNNFaces.IsTrueFace);
        sAreaInfo.f4CnnResult0 = (MFLOAT)(ptrWins->CNNFaces.CnnResult0);
        sAreaInfo.f4CnnResult1 = (MFLOAT)(ptrWins->CNNFaces.CnnResult1);
        CAM_LOGD_IF( m_i4DgbLogLv,
                     "#(%5d,%5d) %s Dev(%d) Portrait ==> i4PortEnable: %d, i4IsTrueFace: %d, f4CnnResult: (%d, %d)",
                     m_u4ReqMagicNum,
                     m_u4StaMagicNum,
                     __FUNCTION__,
                     m_i4CurrSensorDev,
                     sAreaInfo.i4PortEnable,
                     sAreaInfo.i4IsTrueFace,
                     (MINT32)(sAreaInfo.f4CnnResult0*1000),
                     (MINT32)(sAreaInfo.f4CnnResult1*1000));

        sAreaInfo.i4Score  = i4Type!=0 ? ptrWins->faces->score : 0;  /*for algorithm to check input is FD or object, 0:face, 100:object*/
        if( ptrWins->number_of_faces!=0)
        {
            if( ptrWins->faces!=NULL)
            {
                // LaunchCamTrigger
                // there's at least one face ==> clear the TimeoutCount for LaunchCamTrigger
                m_i4AEStableTriggerTimeout = 0;
                m_i4ValidPDTriggerTimeout = 0;

                // rect => 0:left, 1:top, 2:right, 3:bottom
                OriFDArea.i4X = ptrWins->faces[0].rect[0];
                OriFDArea.i4Y = ptrWins->faces[0].rect[1];
                OriFDArea.i4W = ptrWins->faces[0].rect[2] - ptrWins->faces[0].rect[0];
                OriFDArea.i4H = ptrWins->faces[0].rect[3] - ptrWins->faces[0].rect[1];

                if( (OriFDArea.i4W!=0) || (OriFDArea.i4H!=0))
                {
                    // OTFD data is valid.
                    sAreaInfo.i4Count  = 1;
                    sAreaInfo.sRect[0] = OriFDArea;
                    // new FD info +
                    sAreaInfo.i4Id[0]        = ptrWins->faces[0].id;
                    sAreaInfo.i4Type[0]      = ptrWins->faces_type[0];
                    sAreaInfo.i4Motion[0][0] = ptrWins->motion[0][0];
                    sAreaInfo.i4Motion[0][1] = ptrWins->motion[0][1];
                    // fd roi is calculated once a FD region is set
                    m_sPDRois[eIDX_ROI_ARRAY_FD].valid       = MTRUE;
                    m_sPDRois[eIDX_ROI_ARRAY_FD].info.sType  = ROI_TYPE_FD;
                    m_sPDRois[eIDX_ROI_ARRAY_FD].info.sPDROI = sAreaInfo.sRect[0]; // FDROI before extended

                    //=== Landmark ===//
                    // landmark CV
                    sAreaInfo.i4LandmarkCV[0] = ptrWins->fa_cv[0];
                    // left eye
                    sAreaInfo.i4Landmark[0][0][0] = ptrWins->leyex0[0];
                    sAreaInfo.i4Landmark[0][0][1] = ptrWins->leyey0[0];
                    sAreaInfo.i4Landmark[0][0][2] = ptrWins->leyex1[0];
                    sAreaInfo.i4Landmark[0][0][3] = ptrWins->leyey1[0];
                    // right eye
                    sAreaInfo.i4Landmark[0][1][0] = ptrWins->reyex0[0];
                    sAreaInfo.i4Landmark[0][1][1] = ptrWins->reyey0[0];
                    sAreaInfo.i4Landmark[0][1][2] = ptrWins->reyex1[0];
                    sAreaInfo.i4Landmark[0][1][3] = ptrWins->reyey1[0];
                    // mouth
                    sAreaInfo.i4Landmark[0][2][0] = ptrWins->mouthx0[0];
                    sAreaInfo.i4Landmark[0][2][1] = ptrWins->mouthy0[0];
                    sAreaInfo.i4Landmark[0][2][2] = ptrWins->mouthx1[0];
                    sAreaInfo.i4Landmark[0][2][3] = ptrWins->mouthy1[0];
                    // rip/rop info of FLD
                    sAreaInfo.i4LandmarkRIP[0] = ptrWins->fld_rip[0];
                    sAreaInfo.i4LandmarkROP[0] = ptrWins->fld_rop[0];
                    CAM_LOGD_IF( m_i4DgbLogLv,
                                 "[%s] Landmark ==> CV: %d, left eye: (%d,%d,%d,%d), right eye: (%d,%d,%d,%d), mouth: (%d,%d,%d,%d), rip/rop: (%d,%d)",
                                 __FUNCTION__,
                                 sAreaInfo.i4LandmarkCV[0],     // CV
                                 sAreaInfo.i4Landmark[0][0][0], // left eye
                                 sAreaInfo.i4Landmark[0][0][1],
                                 sAreaInfo.i4Landmark[0][0][2],
                                 sAreaInfo.i4Landmark[0][0][3],
                                 sAreaInfo.i4Landmark[0][1][0], // right eye
                                 sAreaInfo.i4Landmark[0][1][1],
                                 sAreaInfo.i4Landmark[0][1][2],
                                 sAreaInfo.i4Landmark[0][1][3],
                                 sAreaInfo.i4Landmark[0][2][0], // mouth
                                 sAreaInfo.i4Landmark[0][2][1],
                                 sAreaInfo.i4Landmark[0][2][2],
                                 sAreaInfo.i4Landmark[0][2][3],
                                 sAreaInfo.i4LandmarkRIP[0],    // rip/rop
                                 sAreaInfo.i4LandmarkROP[0]);

                    ret = S_AF_OK;
                }
                else
                {
                    CAM_LOGD_IF( m_i4OTFDLogLv!=1, "[%s] data is not valid", __FUNCTION__);
                    m_i4OTFDLogLv = 1;
                }
            }
            else
            {
                CAM_LOGD_IF( m_i4OTFDLogLv!=2, "[%s] data is NULL ptr", __FUNCTION__);
                m_i4OTFDLogLv = 2;
            }
        }
        else
        {
            CAM_LOGD_IF( m_i4OTFDLogLv!=3, "[%s] num 0", __FUNCTION__);
            m_i4OTFDLogLv = 3;
        }
    }
    else
    {
        CAM_LOGD_IF( m_i4OTFDLogLv!=4, "[%s] Input NULL ptr", __FUNCTION__);
        m_i4OTFDLogLv = 4;
    }

    // 1. sAreaInfo is passed to AfAlgo
    // 2. FaceROI is extended by algo for HW setting
    if (m_i4IsEnableFVInFixedFocus == 0)
    {
        m_pIAfAlgo->extendFDWin(sAreaInfo);
    }
    if(sAreaInfo.i4Count!=0)
    {
        CAM_LOGD( "[%s]cnt:%d, type %d, FD extend = [X]%d [Y]%d [W]%d [H]%d -> [X]%d [Y]%d [W]%d [H]%d",
                  __FUNCTION__,
                  sAreaInfo.i4Count,
                  sAreaInfo.i4Score,
                  OriFDArea.i4X, OriFDArea.i4Y, OriFDArea.i4W, OriFDArea.i4H,
                  sAreaInfo.sRect[0].i4X, sAreaInfo.sRect[0].i4Y, sAreaInfo.sRect[0].i4W, sAreaInfo.sRect[0].i4H);
    }
    // new FD info -
    // latch last valid FD information.
    m_sArea_OTFD = sAreaInfo.sRect[0];

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
AREA_T& AfMgr::SelROIToFocusing( AF_OUTPUT_T &sInAFInfo)
{
    if( (m_i4IsSelHWROI_PreState!=m_i4IsSelHWROI_CurState && m_i4IsSelHWROI_CurState==MTRUE) ||
            (m_bLatchROI==MTRUE) ||
            (m_i4DgbLogLv))
    {
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) latch(%d) type(%d) sel(%d) issel(%d) ROI(X,Y,W,H) : Center(%d, %d, %d, %d), AP(%d, %d, %d, %d), OT(%d, %d, %d, %d), HW(%d, %d, %d, %d)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_bLatchROI,
                  m_sArea_TypeSel,
                  sInAFInfo.i4ROISel,
                  m_i4IsSelHWROI_CurState,
                  m_sArea_Center.i4X,
                  m_sArea_Center.i4Y,
                  m_sArea_Center.i4W,
                  m_sArea_Center.i4H,
                  m_sArea_APCmd.i4X,
                  m_sArea_APCmd.i4Y,
                  m_sArea_APCmd.i4W,
                  m_sArea_APCmd.i4H,
                  m_sArea_OTFD.i4X,
                  m_sArea_OTFD.i4Y,
                  m_sArea_OTFD.i4W,
                  m_sArea_OTFD.i4H,
                  m_sArea_HW.i4X,
                  m_sArea_HW.i4Y,
                  m_sArea_HW.i4W,
                  m_sArea_HW.i4H);
    }


    if( m_bLatchROI==MTRUE) /* Got changing ROI command from host. Should be trigger searching.*/
    {
        /**
         *   force using the new ROI which is sent from host, and do one time searching :
         *   @LIB3A_AF_MODE_AFS -> wiait autofocus command.
         *   @LIB3A_AF_MODE_AFC_VIDEO, LIB3A_AF_MODE_AFC ->  focuse to trigger searching by switching af mode to auto mode in AF HAL.
         */
        m_sArea_Focusing = m_sArea_APCmd;
        m_sArea_TypeSel  = AF_ROI_SEL_AP;

        CAM_LOGD("#(%5d,%5d) %s [CMD] %d (X,Y,W,H)=(%d, %d, %d, %d)",
                 m_u4ReqMagicNum,
                 m_u4StaMagicNum,
                 __FUNCTION__,
                 m_sArea_TypeSel,
                 m_sArea_Focusing.i4X,
                 m_sArea_Focusing.i4Y,
                 m_sArea_Focusing.i4W,
                 m_sArea_Focusing.i4H);

        //apply zoom information.
        ApplyZoomEffect( m_sArea_Focusing);
        //used to control select AF ROI at AFS mode.
    }
    else if( sInAFInfo.i4ROISel==AF_ROI_SEL_NONE)
    {
        /**
         *  Do nothing
         *  This case is just happened after af is inited.
         *  Wait algo to check using FD or center ROI to do focusing.
         *  Should get i4IsMonitorFV==TRUE. i4IsMonitorFV will be FALSE when ROI is selected.
         */
    }
    else if( m_i4IsSelHWROI_CurState==MTRUE) /* Without got changing ROI command from host, and need to do searching*/
    {
        switch( sInAFInfo.i4ROISel)
        {
        case AF_ROI_SEL_NONE :
            //This case cannot be happened.
            break;
        case AF_ROI_SEL_AP :
            //This case cannot be happened.
            m_sArea_Focusing = m_sArea_APCmd;
            break;
        case AF_ROI_SEL_OT :
        case AF_ROI_SEL_FD :
            m_sArea_Focusing = m_sArea_APCmd = m_sArea_OTFD; //rest AP ROI
            break;
        case AF_ROI_SEL_CENTER :
        case AF_ROI_SEL_DEFAULT :
        default :
            m_sArea_Focusing = m_sArea_APCmd = m_sArea_Center; //rest AP ROI
            break;
        }
        m_sArea_TypeSel = sInAFInfo.i4ROISel;

        CAM_LOGD( "#(%5d,%5d) %s [SEL] %d (X,Y,W,H)=(%d, %d, %d, %d)",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_sArea_TypeSel,
                  m_sArea_Focusing.i4X,
                  m_sArea_Focusing.i4Y,
                  m_sArea_Focusing.i4W,
                  m_sArea_Focusing.i4H);

        //apply zoom information.
        ApplyZoomEffect( m_sArea_Focusing);
    }

    return m_sArea_Focusing;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::getLensInfo( LENS_INFO_T &a_rLensInfo)
{
    MRESULT ret = E_3A_ERR;
    mcuMotorInfo rMotorInfo;

    if( m_pMcuDrv)
    {
        ret = m_pMcuDrv->getMCUInfo(&rMotorInfo);

        if( a_rLensInfo.i4CurrentPos!=(MINT32)rMotorInfo.u4CurrentPosition)
        {
            CAM_LOGD_IF( m_i4DgbLogLv&2,
                         "%s Dev %d, curPos %d, ",
                         __FUNCTION__,
                         m_i4CurrSensorDev,
                         (MINT32)rMotorInfo.u4CurrentPosition);
        }

        a_rLensInfo.i4CurrentPos   = (MINT32)rMotorInfo.u4CurrentPosition;
        a_rLensInfo.bIsMotorOpen   = rMotorInfo.bIsMotorOpen;
        a_rLensInfo.bIsMotorMoving = rMotorInfo.bIsMotorMoving;
        a_rLensInfo.i4InfPos       = (MINT32)rMotorInfo.u4InfPosition;
        a_rLensInfo.i4MacroPos     = (MINT32)rMotorInfo.u4MacroPosition;
        a_rLensInfo.bIsSupportSR   = rMotorInfo.bIsSupportSR;

        CAM_LOGD_IF( m_i4DgbLogLv&2,
                     "%s Dev %d, %d, %d, %d, %d, %d, %d",
                     __FUNCTION__,
                     m_i4CurrSensorDev,
                     a_rLensInfo.i4CurrentPos,
                     a_rLensInfo.bIsMotorOpen,
                     a_rLensInfo.bIsMotorMoving,
                     a_rLensInfo.i4InfPos,
                     a_rLensInfo.i4MacroPos,
                     a_rLensInfo.bIsSupportSR);

        ret = S_AF_OK;
    }
    else
    {
        CAM_LOGD_IF( m_i4DgbLogLv,
                     "%s Fail, Dev %d",
                     __FUNCTION__,
                     m_i4CurrSensorDev);
    }
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MUINT64 AfMgr::getTimeStamp_us()
{
    struct timespec t;

    t.tv_sec = t.tv_nsec = 0;
    clock_gettime(CLOCK_MONOTONIC, &t);

    MINT64 timestamp =((t.tv_sec) * 1000000000LL + t.tv_nsec)/1000;
    return timestamp; // from nano to us
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::WDBGInfo( MUINT32 i4InTag, MUINT32 i4InVal, MUINT32 i4InLineKeep)
{
    MRESULT ret = E_3A_ERR;

    if( m_i4MgrExifSz<MGR_EXIF_SIZE)
    {
        m_sMgrExif[ m_i4MgrExifSz].u4FieldID    = AAATAG( AAA_DEBUG_AF_MODULE_ID, i4InTag, i4InLineKeep);
        m_sMgrExif[ m_i4MgrExifSz].u4FieldValue = i4InVal;
        m_i4MgrExifSz++;
        ret = S_3A_OK;
    }
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::WDBGCapInfo( MUINT32 i4InTag, MUINT32 i4InVal, MUINT32 i4InLineKeep)
{
    MRESULT ret = E_3A_ERR;

    if( m_i4MgrCapExifSz<MGR_CAPTURE_EXIF_SIZE)
    {
        m_sMgrCapExif[ m_i4MgrCapExifSz].u4FieldID    = AAATAG( AAA_DEBUG_AF_MODULE_ID, i4InTag, i4InLineKeep);
        m_sMgrCapExif[ m_i4MgrCapExifSz].u4FieldValue = i4InVal;
        m_i4MgrCapExifSz++;
        ret = S_3A_OK;
    }
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::WDBGTSInfo( MUINT32 i4InTag, MUINT32 i4InVal, MUINT32 i4InLineKeep)
{
    MRESULT ret = E_3A_ERR;

    if( m_i4MgrTsExifSz<MGR_TS_EXIF_SIZE)
    {
        m_sMgrTSExif[ m_i4MgrTsExifSz].u4FieldID    = AAATAG( AAA_DEBUG_AF_MODULE_ID, i4InTag, i4InLineKeep);
        m_sMgrTSExif[ m_i4MgrTsExifSz].u4FieldValue = i4InVal;
        m_i4MgrTsExifSz++;
        ret = S_3A_OK;
    }
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::SetMgrDbgInfo()
{
    MRESULT ret = E_3A_ERR;

    CAM_LOGD_IF( m_i4DgbLogLv, "%s", __FUNCTION__);

    ret = CleanMgrDbgInfo();

    MUINT32 mzNum = static_cast<MUINT32>(m_sAFOutput.sROIStatus.i4TotalNum);
    if( mzNum<=MAX_MULTI_ZONE_WIN_NUM)
    {
        ret = WDBGInfo( MZ_WIN_NUM, m_sAFOutput.sROIStatus.i4TotalNum, 0);
        ret = WDBGInfo( MZ_WIN_W, m_sAFOutput.sROIStatus.sROI[0].i4W, 0);
        ret = WDBGInfo( MZ_WIN_H, m_sAFOutput.sROIStatus.sROI[0].i4H, 1);

        for( MUINT32 i=0; i<mzNum; i++)
        {
            ret = WDBGInfo( MZ_WIN_X, m_sAFOutput.sROIStatus.sROI[i].i4X, 0);
            ret = WDBGInfo( MZ_WIN_Y, m_sAFOutput.sROIStatus.sROI[i].i4Y, 1);
            ret = WDBGInfo( MZ_WIN_RES, m_sAFOutput.sROIStatus.sROI[i].i4Info, 1);
            if( ret==E_3A_ERR)
            {
                break;
            }
        }
    }

    if( ret==S_3A_OK)
    {
        ret = WDBGInfo( MGR_TG_W, m_i4TGSzW, 0);
        ret = WDBGInfo( MGR_TG_H, m_i4TGSzH, 1);
        ret = WDBGInfo( MGR_BIN_W, m_i4BINSzW, 0);
        ret = WDBGInfo( MGR_BIN_H, m_i4BINSzH, 1);
        ret = WDBGInfo( MGR_CROP_WIN_X, m_sCropRegionInfo.i4X, 0);
        ret = WDBGInfo( MGR_CROP_WIN_Y, m_sCropRegionInfo.i4Y, 1);
        ret = WDBGInfo( MGR_CROP_WIN_W, m_sCropRegionInfo.i4W, 1);
        ret = WDBGInfo( MGR_CROP_WIN_H, m_sCropRegionInfo.i4H, 1);
        ret = WDBGInfo( MGR_DZ_CFG, m_i4DzWinCfg, 0);
        ret = WDBGInfo( MGR_DZ_FACTOR, m_i4DZFactor, 1);
        ret = WDBGInfo( MGR_FOCUSING_WIN_X, m_sArea_Focusing.i4X, 0);
        ret = WDBGInfo( MGR_FOCUSING_WIN_Y, m_sArea_Focusing.i4Y, 1);
        ret = WDBGInfo( MGR_FOCUSING_WIN_W, m_sArea_Focusing.i4W, 1);
        ret = WDBGInfo( MGR_FOCUSING_WIN_H, m_sArea_Focusing.i4H, 1);
        ret = WDBGInfo( MGR_OTFD_WIN_X, m_sArea_OTFD.i4X, 0);
        ret = WDBGInfo( MGR_OTFD_WIN_Y, m_sArea_OTFD.i4Y, 1);
        ret = WDBGInfo( MGR_OTFD_WIN_W, m_sArea_OTFD.i4W, 1);
        ret = WDBGInfo( MGR_OTFD_WIN_H, m_sArea_OTFD.i4H, 1);
        ret = WDBGInfo( MGR_CENTER_WIN_X, m_sArea_Center.i4X, 0);
        ret = WDBGInfo( MGR_CENTER_WIN_Y, m_sArea_Center.i4Y, 1);
        ret = WDBGInfo( MGR_CENTER_WIN_W, m_sArea_Center.i4W, 1);
        ret = WDBGInfo( MGR_CENTER_WIN_H, m_sArea_Center.i4H, 1);
        ret = WDBGInfo( MGR_CMD_WIN_X, m_sArea_APCmd.i4X, 0);
        ret = WDBGInfo( MGR_CMD_WIN_Y, m_sArea_APCmd.i4Y, 1);
        ret = WDBGInfo( MGR_CMD_WIN_W, m_sArea_APCmd.i4W, 1);
        ret = WDBGInfo( MGR_CMD_WIN_H, m_sArea_APCmd.i4H, 1);
        ret = WDBGInfo( MGR_LASER_VAL, m_sAFInput.sLaserInfo.i4CurPosDist, 0);
        ret = WDBGInfo( MGR_FOCUSING_POS, m_sFocusDis.i4LensPos, 0);
        ret = WDBGInfo( MGR_FOCUSING_DST, m_sFocusDis.i4Dist, 0);
    }

    return ret;

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::SetMgrCapDbgInfo()
{
    MRESULT ret = E_3A_ERR;

    CAM_LOGD_IF( m_i4DgbLogLv&4, "%s", __FUNCTION__);

    ret = CleanMgrCapDbgInfo();

    if( ret==S_3A_OK)
    {
        ret = WDBGCapInfo( MGR_CURRENT_POS, m_sCurLensInfo.i4CurrentPos, 0);
        ret = WDBGCapInfo( MGR_GYRO_SENSOR_X, m_i4GyroInfo[0], 0);
        ret = WDBGCapInfo( MGR_GYRO_SENSOR_Y, m_i4GyroInfo[1], 1);
        ret = WDBGCapInfo( MGR_GYRO_SENSOR_Z, m_i4GyroInfo[2], 1);
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::CleanMgrDbgInfo()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "%s", __FUNCTION__);
    memset( &m_sMgrExif[0], 0, sizeof( AAA_DEBUG_TAG_T)*MGR_EXIF_SIZE);
    m_i4MgrExifSz = 0;
    return S_3A_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::CleanMgrCapDbgInfo()
{
    CAM_LOGD_IF( m_i4DgbLogLv&4, "%s", __FUNCTION__);
    memset( &m_sMgrCapExif[0], 0, sizeof( AAA_DEBUG_TAG_T)*MGR_CAPTURE_EXIF_SIZE);
    m_i4MgrCapExifSz = 0;
    return S_3A_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::CleanTSDbgInfo()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "%s", __FUNCTION__);
    memset( &m_sMgrTSExif[0], 0, sizeof( AAA_DEBUG_TAG_T)*MGR_TS_EXIF_SIZE);
    m_i4MgrTsExifSz = 0;
    return S_3A_OK;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AfMgr::GetMgrDbgInfo( AF_DEBUG_INFO_T &sOutMgrDebugInfo)
{
    MRESULT ret = E_3A_ERR;

    /* Store current status to EXIF*/
    SetMgrCapDbgInfo();


    /* Output */
    MUINT32 idx = AF_DEBUG_TAG_SIZE;
    for( MUINT32 i=0; i<AF_DEBUG_TAG_SIZE; i++)
    {
        if( sOutMgrDebugInfo.Tag[i].u4FieldID==0)
        {
            idx = i;
            break;
        }
    }

    CAM_LOGD_IF( m_i4DgbLogLv&4,
                 "%s %d %d %d %d %d %d",
                 __FUNCTION__,
                 AF_DEBUG_TAG_SIZE,
                 idx,
                 MGR_EXIF_SIZE,
                 m_i4MgrExifSz,
                 MGR_CAPTURE_EXIF_SIZE,
                 m_i4MgrCapExifSz);

    if( ((AF_DEBUG_TAG_SIZE-idx)>=m_i4MgrExifSz) && (0<m_i4MgrExifSz))
    {
        memcpy( &sOutMgrDebugInfo.Tag[idx], &m_sMgrExif[0], sizeof( AAA_DEBUG_TAG_T)*m_i4MgrExifSz);
        ret = S_3A_OK;
        idx += m_i4MgrExifSz;
    }

    if( (AF_DEBUG_TAG_SIZE-idx)>=m_i4MgrCapExifSz && (0<m_i4MgrCapExifSz))
    {
        memcpy( &sOutMgrDebugInfo.Tag[idx], &m_sMgrCapExif[0], sizeof( AAA_DEBUG_TAG_T)*m_i4MgrCapExifSz);
        ret = S_3A_OK;
        idx += m_i4MgrCapExifSz;
    }

    if( (AF_DEBUG_TAG_SIZE-idx)>=m_i4MgrTsExifSz && (0<m_i4MgrTsExifSz))
    {
        memcpy( &sOutMgrDebugInfo.Tag[idx], &m_sMgrTSExif[0], sizeof( AAA_DEBUG_TAG_T)*m_i4MgrTsExifSz);
        ret = S_3A_OK;
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
E_AF_STATE_T AfMgr::UpdateStateMFMode( E_AF_STATE_T &eInCurSate, AF_EVENT_T &sInEvent)
{
    E_AF_STATE_T NewState = E_AF_INACTIVE;
    switch( sInEvent)
    {
    case EVENT_CMD_CHANGE_MODE :
        m_bNeedCheckSendCallback = MFALSE;
        NewState = E_AF_INACTIVE;
        m_i4TAFStatus = TAF_STATUS_RESET;
        // LaunchCamTrigger get disable in afmodes except ContinuousMode
        m_i4LaunchCamTriggered = E_LAUNCH_AF_DONE;
        m_i4AEStableFrameCount = -1;
        m_i4ValidPDFrameCount = -1;
        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
        break;
    case EVENT_CMD_AUTOFOCUS :
    case EVENT_CMD_TRIGGERAF_WITH_AE_STBL :
        NewState = eInCurSate;
        if (m_pIAfAlgo)
            m_pIAfAlgo->trigger();
        break;
    case EVENT_SET_WAIT_FORCE_TRIGGER :
    case EVENT_CANCEL_WAIT_FORCE_TRIGGER :
        NewState = eInCurSate;
        break;
    case EVENT_CMD_AUTOFOCUS_CANCEL :
        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
    // intentionally fall through
    case EVENT_CMD_SET_AF_REGION :
    case EVENT_CMD_STOP :
    case EVENT_SEARCHING_START :
    case EVENT_SEARCHING_END :
    case EVENT_AE_IS_STABLE :
    default :
        m_sCallbackInfo.isAfterAutoMode = 0;
        NewState = E_AF_INACTIVE;
        break;
    }

    return NewState;

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
E_AF_STATE_T AfMgr::UpdateStateOFFMode( E_AF_STATE_T &eInCurSate, AF_EVENT_T &sInEvent)
{
    E_AF_STATE_T NewState = E_AF_INACTIVE;
    switch( sInEvent)
    {
    case EVENT_CMD_CHANGE_MODE :
        NewState = E_AF_INACTIVE;
        m_i4TAFStatus = TAF_STATUS_RESET;
        m_bNeedCheckSendCallback = MFALSE;
        // LaunchCamTrigger get disable in afmodes except ContinuousMode
        m_i4LaunchCamTriggered = E_LAUNCH_AF_DONE;
        m_i4AEStableFrameCount = -1;
        m_i4ValidPDFrameCount = -1;
        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
        break;
    case EVENT_CMD_AUTOFOCUS :
    case EVENT_CMD_TRIGGERAF_WITH_AE_STBL :
        NewState = eInCurSate;
        if (m_pIAfAlgo)
            m_pIAfAlgo->trigger();
        break;
    case EVENT_SET_WAIT_FORCE_TRIGGER :
    case EVENT_CANCEL_WAIT_FORCE_TRIGGER :
        NewState = eInCurSate;
        break;
    case EVENT_CMD_AUTOFOCUS_CANCEL :
        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
    // intentionally fall through
    case EVENT_CMD_SET_AF_REGION :
    case EVENT_CMD_STOP :
    case EVENT_SEARCHING_START :
    case EVENT_SEARCHING_END :
    case EVENT_AE_IS_STABLE :
    default :
        m_sCallbackInfo.isAfterAutoMode = 0;
        NewState = E_AF_INACTIVE;
        break;
    }

    return NewState;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
E_AF_STATE_T AfMgr::UpdateStateContinuousMode( E_AF_STATE_T &eInCurSate, AF_EVENT_T &sInEvent)
{
    E_AF_STATE_T NewState = E_AF_INACTIVE;
    switch( sInEvent)
    {
    case EVENT_CMD_CHANGE_MODE :
        /**
         *  For normal continuous AF flow, AF will do lock once got autofocus command from host.
         *  Reset flags.
         */
        m_bLock = m_bNeedLock = MFALSE;

        /**
         *  For normal TAF flow, AF HAL got both auto mode and AF region information.
         *  Setting m_bForceTrigger and m_bLatchROI as MFALSE here to wait trigger searching(change ROI only).
         */
        m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = m_bWaitForceTrigger = MFALSE;

        m_bNeedCheckSendCallback = MTRUE;

        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;

        if( eInCurSate==E_AF_INACTIVE || m_i4LaunchCamTriggered!=E_LAUNCH_AF_DONE)
        {
            // To prevent Compatibility Test Suite fail because of the wrong af state while switching mode w/o trigger (original: E_AF_INACTIVE)
            NewState = E_AF_PASSIVE_UNFOCUSED;
            if( m_i4TAFStatus == TAF_STATUS_RECEIVE_CANCELAUTOFOCUS)
            {
                CAM_LOGD( "Don't do AF searching after TAF");
            }
            else
            {
                CAM_LOGD( "[%s] LaunchCamTrigger, LockAlgo for waiting (AEStable or PDValid) + FDinfo", __FUNCTION__);
                m_bForceTrigger = m_bTriggerCmdVlid = MTRUE;
                // LaunchCamTrigger get enabled while force trigger searching
                m_i4LaunchCamTriggered = E_LAUNCH_AF_WAITING;
                m_i4AEStableFrameCount = -1;
                m_i4ValidPDFrameCount = -1;
                LockAlgo(); // to stop continuous af triggered by algo while launching camera
                m_i4IsLockForLaunchCamTrigger = 1;
            }
        }
        else if( eInCurSate==E_AF_ACTIVE_SCAN       ) NewState = E_AF_PASSIVE_UNFOCUSED;
        else if( eInCurSate==E_AF_FOCUSED_LOCKED    ) NewState = E_AF_PASSIVE_FOCUSED;
        else if( eInCurSate==E_AF_NOT_FOCUSED_LOCKED) NewState = E_AF_PASSIVE_UNFOCUSED;
        else                                                NewState = eInCurSate;

        m_i4TAFStatus = TAF_STATUS_RESET;
        break;

    case EVENT_CMD_AUTOFOCUS :
        if( m_bForceTrigger==MTRUE)
        {
            // force to trigger searching when changing ROI at continuous mode.
            NewState = E_AF_PASSIVE_SCAN;
            CAM_LOGD( "Wait force trigger and lock until searching done");
            m_bNeedLock = MTRUE;
        }
        else
        {
            if(      eInCurSate==E_AF_INACTIVE          ) NewState = E_AF_NOT_FOCUSED_LOCKED;
            else if( eInCurSate==E_AF_PASSIVE_SCAN      )
            {
                if( m_eCurAFMode==MTK_CONTROL_AF_MODE_CONTINUOUS_PICTURE)
                {
                    /**
                     * During continuous-picture mode :
                     * Eventual transition once the focus is good.
                     * If autofocus(AF_TRIGGER) command is sent during searching, AF will be locked once searching done.
                     */
                    NewState = eInCurSate;
                }
                else
                {
                    /**
                     * During continuous-video mode :
                     * Immediate transition to lock state
                     */
                    NewState = E_AF_NOT_FOCUSED_LOCKED;
                }
            }
            else if( eInCurSate==E_AF_PASSIVE_FOCUSED   ) NewState = E_AF_FOCUSED_LOCKED;
            else if( eInCurSate==E_AF_PASSIVE_UNFOCUSED ) NewState = E_AF_NOT_FOCUSED_LOCKED;
            else if( eInCurSate==E_AF_FOCUSED_LOCKED    ) NewState = E_AF_FOCUSED_LOCKED;
            else if( eInCurSate==E_AF_NOT_FOCUSED_LOCKED) NewState = E_AF_NOT_FOCUSED_LOCKED;
            else                                                NewState = eInCurSate; /*Should not be happened*/

            if( m_eEvent&EVENT_CMD_CHANGE_MODE)
            {
                NewState = E_AF_PASSIVE_SCAN;
                CAM_LOGD( "Got changing mode and AF_TRIGGER at same time, force trigger searching");
                if (m_pIAfAlgo)
                {
                    m_pIAfAlgo->cancel();
                    m_pIAfAlgo->trigger();
                }
            }

            if( NewState != E_AF_PASSIVE_SCAN)
            {
                LockAlgo();
            }
            else
            {
                // It will change AF state
                CAM_LOGD( "LockAF until searching done");
                m_bNeedLock = MTRUE;
            }
        }
        break;

    case EVENT_AE_IS_STABLE :
        /*
            if (NO_TRIGGER_CMD) keep previous AF state.  -> NO_TRIGGER_CMD = !(TRIGGER_CMD)
            else                trigger AF
        */
        if (!((m_bForceTrigger==MTRUE && m_bTriggerCmdVlid==MTRUE) && m_bWaitForceTrigger==MFALSE))
        {
            NewState = eInCurSate;
            break;
        }
        else
        {
            // LaunchCamTrigger
            if(m_i4LaunchCamTriggered==E_LAUNCH_AF_WAITING)
            {
                if(m_i4AEStableFrameCount==-1)
                {
                    m_i4AEStableFrameCount = m_u4ReqMagicNum;
                    CAM_LOGD("#(%5d,%5d) %s Dev(%d) LaunchCamTrigger EVENT_AE_IS_STABLE(%d) - BUT NOT TRIGGER YET",
                             m_u4ReqMagicNum, m_u4StaMagicNum,
                             __FUNCTION__,
                             m_i4CurrSensorDev,
                             m_i4AEStableFrameCount);
                }

                if(m_i4AEStableFrameCount != -1 && m_u4ReqMagicNum >= (m_i4AEStableFrameCount + m_i4AEStableTriggerTimeout))
                {
                    // AE stable and AEStableTimeout(for waiting Face)
                    m_i4LaunchCamTriggered = E_LAUNCH_AF_TRIGGERED;
                    CAM_LOGD( "#(%5d,%5d) %s Dev(%d) LaunchCamTrigger EVENT_AE_IS_STABLE(%d + %d) - UnlockAlgo + TRIGGERAF lib_afmode(%d)",
                              m_u4ReqMagicNum, m_u4StaMagicNum,
                              __FUNCTION__,
                              m_i4CurrSensorDev,
                              m_i4AEStableFrameCount, m_i4AEStableTriggerTimeout,
                              m_eLIB3A_AFMode);
                    UnlockAlgo();
                    m_i4IsLockForLaunchCamTrigger = 0;
                    // no break : Intentionally fall through to triggerAF
                }
                else
                {
                    NewState = eInCurSate;
                    break; // AE stable but not timeout yet ==> skip triggerAF
                }
            }
            else
            {
                NewState = eInCurSate;
                break; // no break : Intentionally fall through to triggerAF
            }
        }
    case EVENT_CMD_TRIGGERAF_WITH_AE_STBL :
        if( m_bForceTrigger==MTRUE && m_bTriggerCmdVlid==MTRUE)
        {
            NewState = eInCurSate;

            m_bTriggerCmdVlid = MFALSE;
            // force to trigger searching when changing ROI at continuous mode.
            CAM_LOGD( "Force trigger searching [NeedLock(%d) LatchROI(%d) isAFSearch(%d)]", m_bNeedLock, m_bLatchROI, m_i4IsAFSearch_CurState);
            if (m_pIAfAlgo)
            {
                m_pIAfAlgo->cancel();
                m_pIAfAlgo->trigger();
            }
            /* HAL trigger AF during seraching without callback */
            m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
        }
        else
        {
            NewState = eInCurSate;
        }
        break;

    case EVENT_CMD_AUTOFOCUS_CANCEL :
        if( eInCurSate==E_AF_FOCUSED_LOCKED)
        {
            NewState = E_AF_PASSIVE_FOCUSED;
            UnlockAlgo();
        }
        else if( eInCurSate==E_AF_NOT_FOCUSED_LOCKED)
        {
            NewState = E_AF_PASSIVE_UNFOCUSED;
            UnlockAlgo();
        }
        else if( eInCurSate == E_AF_PASSIVE_SCAN)
        {
            NewState = E_AF_INACTIVE;
            CAM_LOGD( "Abort search");
            if (m_pIAfAlgo)
                m_pIAfAlgo->cancel();
        }
        else
        {
            NewState = eInCurSate;
        }
        m_bNeedLock = MFALSE;
        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
        break;

    case EVENT_CMD_SET_AF_REGION :
        if( eInCurSate==E_AF_PASSIVE_SCAN)
        {
            NewState = E_AF_PASSIVE_UNFOCUSED;
            if (m_pIAfAlgo)
                m_pIAfAlgo->cancel();
        }
        else
        {
            NewState = eInCurSate;
        }

        if( eInCurSate!=E_AF_FOCUSED_LOCKED && eInCurSate!=E_AF_NOT_FOCUSED_LOCKED)
        {
            /**
             *  For normal TAF flow, AF HAL got both auto mode and AF region information.
             *  So both m_bForceTrigger and m_bLatchROI will be set MTURE under normal TAF flow.
             *  If TAF is processed under continuous mode, setting m_bForceTrigger and m_bLatchROI as MTRUE here to force trigger searching.
             */
            m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = MTRUE;

            CAM_LOGD( "Force trigger with setting ROI");
        }
        break;

    case EVENT_SEARCHING_START :
        CAM_LOGD( "[%d]", eInCurSate);
        if(m_sCallbackInfo.isAfterAutoMode>0)
        {
            m_sCallbackInfo.isAfterAutoMode--;
        }
        if(      eInCurSate == E_AF_INACTIVE         ) NewState = E_AF_PASSIVE_SCAN;
        else if( eInCurSate == E_AF_PASSIVE_FOCUSED  ) NewState = E_AF_PASSIVE_SCAN;
        else if( eInCurSate == E_AF_PASSIVE_UNFOCUSED) NewState = E_AF_PASSIVE_SCAN;
        else                                                 NewState = eInCurSate;
        break;

    case EVENT_CANCEL_WAIT_FORCE_TRIGGER :
        m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = m_bWaitForceTrigger = MFALSE;
        CAM_LOGD( "isAFSearch[%d]", m_i4IsAFSearch_CurState);
        if( m_i4IsAFSearch_CurState)
        {
            NewState = eInCurSate;
            break;
        }
    case EVENT_CMD_STOP : // intentionally fall through
        m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = m_bWaitForceTrigger = MFALSE;
    // intentionally fall through
    case EVENT_SEARCHING_END :
        CAM_LOGD( "TriggerCmdValid(%d)/WaitForceTrigger(%d)/bNeedLock(%d)/IsFocused(%d)/eInCurSate(%d)",
                  m_bTriggerCmdVlid,
                  m_bWaitForceTrigger,
                  m_bNeedLock,
                  m_i4IsFocused,
                  eInCurSate);
        if( m_bTriggerCmdVlid==MTRUE && m_bWaitForceTrigger==MTRUE)
        {
            NewState = E_AF_PASSIVE_SCAN;
        }
        else
        {
            //reset parameters
            m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = m_bWaitForceTrigger = MFALSE;

            if( m_bNeedLock==MTRUE)
            {
                if( m_i4IsFocused==1) NewState = E_AF_FOCUSED_LOCKED;
                else                  NewState = E_AF_NOT_FOCUSED_LOCKED;

                //lock
                LockAlgo();
                m_bNeedLock = MFALSE;

            }
            else if( eInCurSate == E_AF_PASSIVE_SCAN)
            {
                if( m_i4IsFocused==1) NewState = E_AF_PASSIVE_FOCUSED;
                else                  NewState = E_AF_PASSIVE_UNFOCUSED;
            }
            else
            {
                NewState = eInCurSate;

                if( sInEvent==EVENT_CMD_STOP && eInCurSate == E_AF_INACTIVE)
                {
                    // force doing one time searching when next time start preview.
                    m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = MTRUE;
                    CAM_LOGD( "Do one time searching when next time start preview.");
                }
            }
        }
        break;

    case EVENT_SEARCHING_DONE_RESET_PARA:
        NewState = eInCurSate;
        if( m_bTriggerCmdVlid==MTRUE && m_bWaitForceTrigger==MTRUE)
        {
            /* For capture with flash */
            CAM_LOGD( "hybrid AF searching durig capture state");
            /*    _________            _____________
                           |__________|                     First time searching with PD doesn't change state
                  ___________________            ________
                                     |__________|           capture flow during first time searching with PD
             */
        }
        else
        {
            //reset parameters
            m_bLatchROI = m_bForceTrigger = m_bTriggerCmdVlid = MFALSE;
        }
        break;

    case EVENT_SET_WAIT_FORCE_TRIGGER :
        if( eInCurSate!=E_AF_FOCUSED_LOCKED && eInCurSate!=E_AF_NOT_FOCUSED_LOCKED)
        {
            m_bTriggerCmdVlid = m_bForceTrigger = m_bWaitForceTrigger = MTRUE;
            CAM_LOGD( "Set wait force trigger for preCap");
            if( m_i4IsAFSearch_CurState==AF_SEARCH_DONE)
            {
                LockAlgo();
            }
            // send callback for the next two times
            if(m_sCallbackInfo.isAfterAutoMode==1)
            {
                m_sCallbackInfo.isAfterAutoMode = 2;
            }
        }

        NewState = eInCurSate;
        break;

    default : /*Should not be happened*/
        NewState = eInCurSate;
        break;
    }

    return NewState;

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
E_AF_STATE_T AfMgr::UpdateStateAutoMode( E_AF_STATE_T &eInCurSate, AF_EVENT_T &sInEvent)
{
    E_AF_STATE_T NewState = E_AF_INACTIVE;
    switch( sInEvent)
    {
    case EVENT_CMD_CHANGE_MODE :
        NewState = E_AF_INACTIVE;
        /**
         *  For normal TAF flow, AF HAL got both auto mode and AF region information.
         *  Setting m_bForceTrigger and m_bLatchROI as MTRUE here to wait trigger searching..
         */
        m_bNeedCheckSendCallback = MFALSE;
        m_i4TAFStatus   = TAF_STATUS_RESET;
        // LaunchCamTrigger get disable in afmodes except ContinuousMode
        m_i4LaunchCamTriggered = E_LAUNCH_AF_DONE;
        m_i4AEStableFrameCount = -1;
        m_i4ValidPDFrameCount = -1;
        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
        break;

    case EVENT_CMD_AUTOFOCUS :
        if(      eInCurSate == E_AF_INACTIVE          ) NewState = E_AF_ACTIVE_SCAN;
        else if( eInCurSate == E_AF_FOCUSED_LOCKED    ) NewState = E_AF_ACTIVE_SCAN;
        else if( eInCurSate == E_AF_NOT_FOCUSED_LOCKED) NewState = E_AF_ACTIVE_SCAN;
        else                                                  NewState = eInCurSate; /*Should be at E_AF_ACTIVE_SCAN*/

        // get Autofocus when [(CenterROI exist) or (AE locked)] ==> ForceDoAlgo to speed up SEARCH_END
        if(m_i4IsCAFWithoutFace==1 || i4IsLockAERequest==1)
        {
            m_bForceDoAlgo = MTRUE;
        }
        m_sCallbackInfo.isAfterAutoMode = 1;
        m_i4TAFStatus = TAF_STATUS_RECEIVE_AUTOFOCUS;
        break;

    case EVENT_CMD_TRIGGERAF_WITH_AE_STBL :
        NewState = eInCurSate;
        if (m_pIAfAlgo)
        {
            m_pIAfAlgo->trigger();
        }
        break;

    case EVENT_CMD_AUTOFOCUS_CANCEL :
        if(      eInCurSate == E_AF_ACTIVE_SCAN       ) NewState = E_AF_INACTIVE;
        else if( eInCurSate == E_AF_FOCUSED_LOCKED    ) NewState = E_AF_INACTIVE;
        else if( eInCurSate == E_AF_NOT_FOCUSED_LOCKED) NewState = E_AF_INACTIVE;
        else                                            NewState = eInCurSate; /*Should be at E_AF_INACTIVE*/

        if( m_i4TAFStatus == TAF_STATUS_RECEIVE_AUTOFOCUS)
        {
            m_i4TAFStatus = TAF_STATUS_RECEIVE_CANCELAUTOFOCUS;
            CAM_LOGD( "receive cancelautofocus after seraching end");
        }

        if (m_pIAfAlgo)
            m_pIAfAlgo->cancel();

        // Reseting the para to preventing undesired isAFSearch change.
        m_i4IsAFSearch_PreState = m_i4IsAFSearch_CurState = AF_SEARCH_DONE;
        break;

    case EVENT_CMD_SET_AF_REGION :
        /**
         *  For normal TAF flow, AF HAL got both auto mode and AF region information.
         *  Setting m_bForceTrigger and m_bLatchROI as MTRUE here to wait trigger searching..
         */
        m_bLatchROI = MTRUE;
        NewState = eInCurSate;
        break;

    case EVENT_SEARCHING_START :
        NewState = E_AF_ACTIVE_SCAN;
        break;

    case EVENT_CANCEL_WAIT_FORCE_TRIGGER :
    case EVENT_CMD_STOP :
    case EVENT_SEARCHING_END :
        m_bLatchROI = MFALSE;
        if( eInCurSate==E_AF_ACTIVE_SCAN)
        {
            if( m_i4IsFocused==1) NewState = E_AF_FOCUSED_LOCKED;
            else                  NewState = E_AF_NOT_FOCUSED_LOCKED;
        }
        else
        {
            NewState = eInCurSate;
        }
        break;

    case EVENT_SET_WAIT_FORCE_TRIGGER :
        NewState = eInCurSate;
        break;

    case EVENT_AE_IS_STABLE :
    default : /*Should not be happened*/
        NewState = eInCurSate;
        break;
    }

    return NewState;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
E_AF_STATE_T AfMgr::UpdateState( AF_EVENT_T sInEvent)
{
    Mutex::Autolock lock( m_AFStateLock);

    E_AF_STATE_T NewState;

    switch( sInEvent)
    {
    case EVENT_CMD_CHANGE_MODE:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CMD_CHANGE_MODE", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CMD_CHANGE_MODE;
        break;
    case EVENT_CMD_AUTOFOCUS:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CMD_AUTOFOCUS", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CMD_AUTOFOCUS;
        break;
    case EVENT_CMD_TRIGGERAF_WITH_AE_STBL:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CMD_TRIGGERAF_WITH_AE_STBL", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CMD_TRIGGERAF_WITH_AE_STBL;
        break;
    case EVENT_CMD_AUTOFOCUS_CANCEL:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CMD_AUTOFOCUS_CANCEL", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CMD_AUTOFOCUS_CANCEL;
        break;
    case EVENT_CMD_SET_AF_REGION:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CMD_SET_AF_REGION", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CMD_SET_AF_REGION;
        break;
    case EVENT_CMD_STOP:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CMD_STOP", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CMD_STOP;
        break;
    case EVENT_SEARCHING_START:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_SEARCHING_START", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_SEARCHING_START;
        break;
    case EVENT_SEARCHING_END:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_SEARCHING_END", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_SEARCHING_END;
        break;
    case EVENT_SEARCHING_DONE_RESET_PARA:
        m_eEvent |= EVENT_SEARCHING_DONE_RESET_PARA;
        break;
    case EVENT_AE_IS_STABLE:
        //CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_AE_IS_STABLE", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_AE_IS_STABLE;
        break;
    case EVENT_SET_WAIT_FORCE_TRIGGER :
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_SET_WAIT_FORCE_TRIGGER", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_SET_WAIT_FORCE_TRIGGER;
        break;
    case EVENT_CANCEL_WAIT_FORCE_TRIGGER :
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_CANCEL_WAIT_FORCE_TRIGGER", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        m_eEvent |= EVENT_CANCEL_WAIT_FORCE_TRIGGER;
        break;
    default:
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d) EVENT_WRONG", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev);
        break;
    }

    switch( m_eCurAFMode)
    {
    case MTK_CONTROL_AF_MODE_OFF :
        NewState = UpdateStateMFMode( m_eAFState, sInEvent);
        break;
    case MTK_CONTROL_AF_MODE_AUTO :
    case MTK_CONTROL_AF_MODE_MACRO :
        NewState = UpdateStateAutoMode( m_eAFState, sInEvent);
        break;
    case MTK_CONTROL_AF_MODE_CONTINUOUS_VIDEO :
    case MTK_CONTROL_AF_MODE_CONTINUOUS_PICTURE :
        NewState = UpdateStateContinuousMode( m_eAFState, sInEvent);
        break;
    case MTK_CONTROL_AF_MODE_EDOF :
    default :
        NewState = UpdateStateOFFMode( m_eAFState, sInEvent);
        break;
    }

    if( m_eAFState!=NewState)
    {
        CAM_LOGD( "#(%5d,%5d) %s  Dev(%d) : %d->%d",
                  m_u4ReqMagicNum,
                  m_u4StaMagicNum,
                  __FUNCTION__,
                  m_i4CurrSensorDev,
                  m_eAFState,
                  NewState);
    }

    m_eAFState = NewState;

    return NewState;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::UpdateCenterROI( AREA_T &sOutAreaCenter)
{
    MUINT32 scalex = 100;
    MUINT32 scaley = 100;

    if( m_bMZAFEn)
    {
        scalex = m_ptrNVRam->rAFNVRAM.i4MultiAFCoefs[0];
        scaley = m_ptrNVRam->rAFNVRAM.i4MultiAFCoefs[1];
    }
    else if( m_bEnablePD)
    {
        scalex = m_ptrNVRam->rAFNVRAM.i4HybridAFCoefs[1];
        scaley = m_ptrNVRam->rAFNVRAM.i4HybridAFCoefs[2];
    }
    else
    {
        scalex = m_ptrNVRam->rAFNVRAM.i4SPOT_PERCENT_W;
        scaley = m_ptrNVRam->rAFNVRAM.i4SPOT_PERCENT_H;
    }

    if(     100<scalex) scalex=100;
    else if( scalex<=0) scalex=15;

    if(     100<scaley) scaley=100;
    else if( scaley<=0) scaley=15;

    CAM_LOGD( "%s %d %d %d %d %d %d - scale %d %d",
              __FUNCTION__,
              m_ptrNVRam->rAFNVRAM.i4MultiAFCoefs[0],
              m_ptrNVRam->rAFNVRAM.i4MultiAFCoefs[1],
              m_ptrNVRam->rAFNVRAM.i4HybridAFCoefs[1],
              m_ptrNVRam->rAFNVRAM.i4HybridAFCoefs[2],
              m_ptrNVRam->rAFNVRAM.i4SPOT_PERCENT_W,
              m_ptrNVRam->rAFNVRAM.i4SPOT_PERCENT_H,
              scalex,
              scaley);

    MUINT32 croiw = m_sCropRegionInfo.i4W*scalex/100;
    MUINT32 croih = m_sCropRegionInfo.i4H*scaley/100;
    MUINT32 croix = m_sCropRegionInfo.i4X + (m_sCropRegionInfo.i4W-croiw)/2;
    MUINT32 croiy = m_sCropRegionInfo.i4Y + (m_sCropRegionInfo.i4H-croih)/2;

    //updateing.
    sOutAreaCenter = AREA_T( croix, croiy, croiw, croih, AF_MARK_NONE);

    CAM_LOGD( "ROI-C [X]%d [Y]%d [W]%d [H]%d",
              sOutAreaCenter.i4X,
              sOutAreaCenter.i4Y,
              sOutAreaCenter.i4W,
              sOutAreaCenter.i4H);

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfMgr::LockAlgo(MUINT32 u4Caller)
{
    if( u4Caller==AF_MGR_CALLER)
    {
        m_bLock = MTRUE;
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d): Lock(%d), Pause(%d)",
                  m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__,
                  m_i4CurrSensorDev, m_bLock, m_bPauseAF);
    }
    else
    {
        m_bPauseAF = MTRUE;
        CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d): Lock(%d), Pause(%d)",
                  m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__,
                  m_i4CurrSensorDev, m_bLock, m_bPauseAF);
    }

    if (m_bLock || m_bPauseAF)
    {
        if (m_pIAfAlgo)
        {
            m_pIAfAlgo->cancel();
            m_pIAfAlgo->lock();
            CAM_LOGD( "%s", __FUNCTION__);
        }
    }

    return m_bLock || m_bPauseAF;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfMgr::UnlockAlgo(MUINT32 u4Caller)
{
    if( u4Caller==AF_MGR_CALLER)
    {
        m_bLock = MFALSE;
        CAM_LOGD( "#(%5d,%5d) %s Dev(%d): Lock(%d), Pause(%d)",
                  m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__,
                  m_i4CurrSensorDev, m_bLock, m_bPauseAF);
    }
    else
    {
        m_bPauseAF = MFALSE;
        CAM_LOGD( "#(%5d,%5d) cmd-%s Dev(%d): Lock(%d), Pause(%d)",
                  m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__,
                  m_i4CurrSensorDev, m_bLock, m_bPauseAF);
    }

    if (m_bLock == MFALSE && m_bPauseAF == MFALSE)
    {
        if (m_pIAfAlgo)
        {
            m_pIAfAlgo->unlock();
            CAM_LOGD( "%s", __FUNCTION__);
        }
    }

    return m_bLock || m_bPauseAF;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::IspMgrAFStatStart()
{
    // First time HW setting is got after calling initAF.
    ConfigHWReg( m_sHWCfg, m_sArea_HW, m_i4HWBlkNumX, m_i4HWBlkNumY, m_i4HWBlkSizeX, m_i4HWBlkSizeY);
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::IspMgrAFStatStop()
{
    //uninitial isp_mgr_af_stat for configure HW
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL AfMgr::IspMgrAFStatHWPreparing()
{
    MBOOL bSkip   = MTRUE;
    MBOOL isHWRdy = MTRUE;

    char  dbgMsgBuf[DBG_MSG_BUF_SZ];
    char* ptrMsgBuf = dbgMsgBuf;

    MUINT32 u4FrameCount = m_u4StaMagicNum;

    ptrMsgBuf += sprintf( ptrMsgBuf, "#(%5d,%5d) %s Dev(%d) Config(%d) ", m_u4ReqMagicNum, m_u4StaMagicNum, __FUNCTION__, m_i4CurrSensorDev, m_u4ConfigHWNum);

    if( m_eCurAFMode == MTK_CONTROL_AF_MODE_OFF)
    {
        /*
         * AF off mode don't need to check statistic data, but need run handleAF.
         */
        bSkip = MFALSE;
    }
    else if(m_bForceDoAlgo==MTRUE)
    {
        bSkip = MFALSE;
        CAM_LOGD("%s ForceDoAlgo to speed up SEARCH_END", __FUNCTION__);
    }
    else
    {
        /**
         * The condition AF_ROI_SEL_NONE is for the first time lunch camera only. [Ref] SelROIToFocusing
         */
        isHWRdy = ISP_AF_CONFIG_T::getInstance(m_i4CurrSensorDev).isHWRdy(m_u4ConfigHWNum);
        if(m_sAFOutput.i4ZECChg==1 || m_sAFOutput.i4IsTargetAssitMove==1)
        {
            // algo requirement : do algo
            bSkip = MFALSE;
        }
        else if(isHWRdy || m_sAFOutput.i4ROISel==AF_ROI_SEL_NONE)
        {
            // flow requirement : do algo
            bSkip = MFALSE;
        }
    }


    if( m_i4DgbLogLv || bSkip==MTRUE)
    {
        ptrMsgBuf += sprintf( ptrMsgBuf,
                              "bSkip(%d) PauseAF(%d) FrmCnt(%d) Afmode(%d) VsyncUpdate(%d) IsTargetAssitMove(%d) isHWRdy(%d) ROISel(%d) ZECChg(%d) DbgPDVerifyEn(%d)",
                              bSkip,
                              m_bPauseAF,
                              u4FrameCount,
                              m_eCurAFMode,
                              m_i4VsyncUpdate,
                              m_sAFOutput.i4IsTargetAssitMove,
                              isHWRdy,
                              m_sAFOutput.i4ROISel,
                              m_sAFOutput.i4ZECChg,
                              m_i4DbgPDVerifyEn);
        CAM_LOGD( "%s", dbgMsgBuf);
    }



    return bSkip;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::IspMgrAFStatUpdateHw()
{
    ConfigHWReg( m_sAFOutput.sAFStatConfig, m_sArea_HW, m_i4HWBlkNumX, m_i4HWBlkNumY, m_i4HWBlkSizeX, m_i4HWBlkSizeY);
}

MVOID AfMgr::IspMgrAFGetROIFromHw(AREA_T &Area, MUINT32 &isTwin)
{
    // ISP_MGR_AF_STAT_CONFIG_T::getInstance(static_cast<ESensorDev_T>(m_i4CurrSensorDev)).getROIFromHw(Area, isTwin);
    // transfer AFROI(BINSize) to AFROI(TGSize)
    if(m_i4BINSzW!=0)
    {
        Area.i4X *= m_i4TGSzW/m_i4BINSzW;
        Area.i4Y *= m_i4TGSzW/m_i4BINSzW;
        Area.i4W *= m_i4TGSzW/m_i4BINSzW;
        Area.i4H *= m_i4TGSzW/m_i4BINSzW;
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::preparePDData()
{
    if( m_bRunPDEn==MTRUE)
    {
#define PERFRAME_GET_PD_RESULT 1
        // LaunchCamTrigger Search Done ==> get PD every 2 frames
        if( m_i4LaunchCamTriggered!=E_LAUNCH_AF_DONE)
        {
            m_bPdInputExpected = (m_i4FirsetCalPDFrameCount>0)&&(m_u4ReqMagicNum>m_i4FirsetCalPDFrameCount);
        }
        else if( m_ptrNVRam->rAFNVRAM.i4EasyTuning[2]==PERFRAME_GET_PD_RESULT)
        {
            m_bPdInputExpected = MTRUE;
        }
        else
        {
            m_bPdInputExpected = 1 - m_bPdInputExpected;
        }

        //
        m_sAFInput.sPDInfo.i4Valid = 0;
        if( m_bPdInputExpected)
        {
            //
            PD_CALCULATION_OUTPUT *ptrPDRes = nullptr;
            //
            if( IPDMgr::getInstance().getPDTaskResult( m_i4CurrSensorDev, &ptrPDRes) == S_3A_OK)
            {
                if( ptrPDRes)
                {
                    // set pd result to pd algorithm
                    memset( &m_sAFInput.sPDInfo, 0, sizeof(AFPD_INFO_T));

                    m_sAFInput.sPDInfo.i4Valid  = MTRUE;
                    m_sAFInput.sPDInfo.i4FrmNum = ptrPDRes->frmNum;

                    //
                    MINT32 numRes=0;
                    for(MINT32 i=0; i<ptrPDRes->numRes; i++)
                    {
                        if(ptrPDRes->Res[i].sROIInfo.sType==eAF_ROI_TYPE_AP)
                        {
                            if( /* for multi touch case*/
                                (ptrPDRes->Res[i].sROIInfo.sPDROI.i4X != m_sArea_APCmd.i4X) ||
                                (ptrPDRes->Res[i].sROIInfo.sPDROI.i4Y != m_sArea_APCmd.i4Y) ||
                                (ptrPDRes->Res[i].sROIInfo.sPDROI.i4W != m_sArea_APCmd.i4W) ||
                                (ptrPDRes->Res[i].sROIInfo.sPDROI.i4H != m_sArea_APCmd.i4H))
                            {
                                continue;
                            }
                        }

                        m_sAFInput.sPDInfo.sPDBlockInfo[numRes].sBlockROI.sType   = ptrPDRes->Res[i].sROIInfo.sType;
                        m_sAFInput.sPDInfo.sPDBlockInfo[numRes].sBlockROI.sPDROI  = ptrPDRes->Res[i].sROIInfo.sPDROI;
                        m_sAFInput.sPDInfo.sPDBlockInfo[numRes].i4PDafDacIndex    = ptrPDRes->Res[i].Defocus;
                        m_sAFInput.sPDInfo.sPDBlockInfo[numRes].i4PDafConverge    = ptrPDRes->Res[i].PhaseDifference;
                        m_sAFInput.sPDInfo.sPDBlockInfo[numRes].i4PDafConfidence  = (MUINT32)ptrPDRes->Res[i].DefocusConfidenceLevel;
                        m_sAFInput.sPDInfo.sPDBlockInfo[numRes].i4SatPercent      = ptrPDRes->Res[i].SaturationPercent;

                        numRes++;
                    }
                    m_sAFInput.sPDInfo.i4PDBlockInfoNum = numRes;

                    //print log if necessary
                    if( m_i4DgbLogLv&2)
                    {
                        CAM_LOGD_IF( m_i4DgbLogLv&2,
                                     "%s PD Res vd(%d) frmNum(#%d) curPos(%d) winNum(%d) winInfo(fmt,x,y,w,h,targetPos,pd,cl,satPercent):",
                                     __FUNCTION__,
                                     m_sAFInput.sPDInfo.i4Valid,
                                     m_sAFInput.sPDInfo.i4FrmNum,
                                     ptrPDRes->curLensPos,
                                     m_sAFInput.sPDInfo.i4PDBlockInfoNum);

                        for(MINT32 i=0; i<ptrPDRes->numRes; )
                        {
                            char  dbgMsgBufPD[DBG_MSG_BUF_SZ];
                            char* ptrMsgBufPD = dbgMsgBufPD;

                            for(MINT32 j=0; j<3 && i<ptrPDRes->numRes; j++)
                            {
                                ptrMsgBufPD += sprintf( ptrMsgBufPD,
                                                        " #%d(%d,%4d,%4d,%4d,%4d,%4d,%6d,%3d,%d)\n",
                                                        i,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].sBlockROI.sType,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].sBlockROI.sPDROI.i4X,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].sBlockROI.sPDROI.i4Y,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].sBlockROI.sPDROI.i4W,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].sBlockROI.sPDROI.i4H,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafDacIndex,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafConverge,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].i4PDafConfidence,
                                                        m_sAFInput.sPDInfo.sPDBlockInfo[i].i4SatPercent);
                                i++;
                            }
                            CAM_LOGD_IF( m_i4DgbLogLv&2, "%s", dbgMsgBufPD);

                        }
                    }

                    //release resource
                    delete ptrPDRes;
                    ptrPDRes = nullptr;
                }
                else
                {
                    m_sAFInput.sPDInfo.i4Valid = 0;
                    CAM_LOGE( "get null pointer result from pd manager, should not be happened !!");
                }
            }
            else
            {
                m_sAFInput.sPDInfo.i4Valid = 0;
                CAM_LOGW( "pd result is not ready! statMagicNum(%d)", m_u4StaMagicNum);
            }
        }


        if( m_i4DgbLogLv)
        {
            CAM_LOGD("PDExpt(%d %d)/PDVd(%d)/LaunchCam(%d)/FirsetCalPDFrameCount(%d)/",
                      m_bPdInputExpected,
                      m_ptrNVRam->rAFNVRAM.i4EasyTuning[2],
                      m_sAFInput.sPDInfo.i4Valid,
                      m_i4LaunchCamTriggered,
                      m_i4FirsetCalPDFrameCount);

        }

    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::UpdatePDParam( MINT32 &i4InROISel)
{
    if( m_bEnablePD)
    {
        char  dbgMsgBuf[DBG_MSG_BUF_SZ];
        char* ptrMsgBuf = dbgMsgBuf;

        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf,
                                  "#(%5d,%5d) %s total roi Fmt(%d) Info(vd,Fmt,X,Y,W,H,Info): ",
                                  m_u4ReqMagicNum,
                                  m_u4StaMagicNum,
                                  __FUNCTION__,
                                  eIDX_ROI_ARRAY_NUM);
        }


        MINT32 PDRoisNum = 0;
        AFPD_BLOCK_ROI_T PDRois[eIDX_ROI_ARRAY_NUM];

        for( MUINT16 i=0; i<eIDX_ROI_ARRAY_NUM; i++)
        {
            MBOOL select = MFALSE;

            if( m_i4DgbLogLv)
            {

                ptrMsgBuf += sprintf( ptrMsgBuf,
                                      "#%d(%d,%d,%d,%d,%d,%d,%d) ",
                                      i,
                                      m_sPDRois[i].valid,
                                      m_sPDRois[i].info.sType,
                                      m_sPDRois[i].info.sPDROI.i4X,
                                      m_sPDRois[i].info.sPDROI.i4Y,
                                      m_sPDRois[i].info.sPDROI.i4W,
                                      m_sPDRois[i].info.sPDROI.i4H,
                                      m_sPDRois[i].info.sPDROI.i4Info);
            }

            switch(i)
            {
            case eIDX_ROI_ARRAY_AP:
                select = m_sPDRois[i].info.sType==eAF_ROI_TYPE_AP ? MTRUE : MFALSE;
                m_sPDRois[i].valid = i4InROISel==AF_ROI_SEL_AP ? MTRUE : m_sPDRois[i].valid; // force calculating FD window when hybrid AF need PD result of AP roi.
                break;

            case eIDX_ROI_ARRAY_FD:
                select = m_sPDRois[i].info.sType==eAF_ROI_TYPE_FD ? MTRUE : MFALSE;
                m_sPDRois[i].valid = ((i4InROISel==AF_ROI_SEL_FD) || (i4InROISel==AF_ROI_SEL_OT)) ? MTRUE : m_sPDRois[i].valid; // force calculating FD window when hybrid AF is at FDAF stage.
                break;

            case eIDX_ROI_ARRAY_CENTER:
                select = m_sPDRois[i].info.sType==ROI_TYPE_CENTER ? MTRUE : MFALSE;
                break;

            default :
                select = MFALSE;
                break;

            }

            // Only center ROI is calculated for verification flow.
            if( m_i4DbgPDVerifyEn)
            {
                m_sPDRois[i].valid = (i==eIDX_ROI_ARRAY_CENTER) ? MTRUE : MFALSE;
            }

            if( select && m_sPDRois[i].valid)
            {
                //
                PDRois[PDRoisNum] = m_sPDRois[i].info;

                //apply zoom information.
                ApplyZoomEffect( PDRois[PDRoisNum].sPDROI);

                //debug log
                if( m_i4DgbLogLv)
                {
                    ptrMsgBuf += sprintf( ptrMsgBuf,
                                          "->(%d,%d,%d,%d,%d,%d,%d) ",
                                          m_sPDRois[i].valid,
                                          PDRois[PDRoisNum].sType,
                                          PDRois[PDRoisNum].sPDROI.i4X,
                                          PDRois[PDRoisNum].sPDROI.i4Y,
                                          PDRois[PDRoisNum].sPDROI.i4W,
                                          PDRois[PDRoisNum].sPDROI.i4H,
                                          PDRois[PDRoisNum].sPDROI.i4Info);
                }

                //
                PDRoisNum++;
            }


        }

        MRESULT res = m_pIAfAlgo->getFocusWindows( &PDRois[0],
                                                    PDRoisNum,
                                                    AF_PSUBWIN_NUM, /* Size of m_sPDCalculateWin */
                                                    &m_sPDCalculateWin[0],
                                                    &m_i4PDCalculateWinNum /* How many windows are stored in m_sPDCalculateWin*/);

        // error check
        if( m_i4DgbLogLv)
        {
            ptrMsgBuf += sprintf( ptrMsgBuf, "PDCalculateWin(total%d) Info(Fmt,X,Y,W,H,Info): ", m_i4PDCalculateWinNum);
        }

        for( MINT32 i=0; i<m_i4PDCalculateWinNum; i++)
        {
            if( m_i4DgbLogLv)
            {
                ptrMsgBuf += sprintf( ptrMsgBuf,
                                      "#%d(%d,%d,%d,%d,%d) ",
                                      i,
                                      m_sPDCalculateWin[i].sType,
                                      m_sPDCalculateWin[i].sPDROI.i4X,
                                      m_sPDCalculateWin[i].sPDROI.i4Y,
                                      m_sPDCalculateWin[i].sPDROI.i4W,
                                      m_sPDCalculateWin[i].sPDROI.i4H);
            }

            if( /* Boundary checking */
                (m_sPDCalculateWin[i].sPDROI.i4W <= 0) ||
                (m_sPDCalculateWin[i].sPDROI.i4H <= 0) ||
                (m_sPDCalculateWin[i].sPDROI.i4X <  0) ||
                (m_sPDCalculateWin[i].sPDROI.i4Y <  0) )
            {
                CAM_LOGE( "PD Calculation window is wrong, please check function(getFocusWindows) behavior!!");
                res = E_3A_ERR;
                break;
            }

        }

        if( res!=S_3A_OK)
        {
            m_i4PDCalculateWinNum = 1;
            m_sPDCalculateWin[0]  = m_sPDRois[eIDX_ROI_ARRAY_CENTER].info;
        }
        else if( m_i4DbgPDVerifyEn && (m_i4PDCalculateWinNum<AF_PSUBWIN_NUM))
        {
            m_i4PDCalculateWinNum++;
            m_sPDCalculateWin[m_i4PDCalculateWinNum-1] = m_sPDRois[eIDX_ROI_ARRAY_CENTER].info;
        }

        m_bRunPDEn = MTRUE;

        CAM_LOGD_IF( m_i4DgbLogLv, "%s", dbgMsgBuf);

        /* exectuing from first request */
        AAA_TRACE_D("UpdatePDParam (%d)", m_u4StaMagicNum);
        IPDMgr::getInstance().UpdatePDParam( m_i4CurrSensorDev,
                                             m_u4StaMagicNum,
                                             m_i4PDCalculateWinNum,
                                             m_sPDCalculateWin,
                                             m_sAFOutput.i4SearchRangeInf,
                                             m_sAFOutput.i4SearchRangeMac,
                                             (MBOOL)(m_i4LaunchCamTriggered!=E_LAUNCH_AF_DONE));
        AAA_TRACE_END_D;

        /*****************************************************************************************************************
         * FD roi is calculated once a FD region is set
         * So reset status once region is read out
         *****************************************************************************************************************/
        if( m_sPDRois[eIDX_ROI_ARRAY_FD].valid)
        {
            m_sPDRois[eIDX_ROI_ARRAY_FD].valid = MFALSE;
        }

        //
        if((m_i4FirsetCalPDFrameCount==-1) && (m_u4ReqMagicNum>0))
        {
            m_i4FirsetCalPDFrameCount = m_u4ReqMagicNum;
        }

    }
    else
    {
        m_bRunPDEn = MFALSE;
    }

}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::initLD()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s]", __FUNCTION__);
    // --- init Laser ---
    if (m_bLDAFEn == MFALSE)
    {
        if (ILaserMgr::getInstance().init(m_i4CurrSensorDev) == 1)
        {
            m_bLDAFEn = MTRUE;
            CAM_LOGD( "[%s] ILaserMgr init() done", __FUNCTION__);
        }
        else
        {
            CAM_LOGD_IF( m_i4DgbLogLv, "[%s] ILaserMgr init() fail", __FUNCTION__);
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::startLD()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s]", __FUNCTION__);

    if(m_bLDAFEn == MTRUE)
    {
        if( property_get_int32("vendor.laser.calib.disable", 0) == 0 )
        {
            MUINT32 OffsetData = (MUINT32)m_ptrNVRam->rAFNVRAM.i4Revs[201];
            MUINT32 XTalkData  = (MUINT32)m_ptrNVRam->rAFNVRAM.i4Revs[202];
            ILaserMgr::getInstance().setLaserCalibrationData(m_i4CurrSensorDev, OffsetData, XTalkData);
        }

        MUINT32 LaserMaxDistance = (MUINT32)m_ptrNVRam->rAFNVRAM.i4Revs[203];
        MUINT32 LaserTableNum    = (MUINT32)m_ptrNVRam->rAFNVRAM.i4Revs[204];
        ILaserMgr::getInstance().setLaserGoldenTable(m_i4CurrSensorDev, (MUINT32*)&m_ptrNVRam->rAFNVRAM.i4Revs[205], LaserTableNum, LaserMaxDistance);

        m_sAFInput.sLaserInfo.i4AfWinPosCnt = 0;

        if( ILaserMgr::getInstance().checkHwSetting(m_i4CurrSensorDev)==0 || property_get_int32("vendor.laser.disable", 0)==1)
        {
            ILaserMgr::getInstance().uninit(m_i4CurrSensorDev);
            m_bLDAFEn = MFALSE;
            CAM_LOGE( "AF-%-15s: ILaserMgr checkHwSetting() fail", __FUNCTION__);
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::camPwrOffLD()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s]", __FUNCTION__);

    if (m_bLDAFEn == MTRUE)
    {
        CAM_LOGD( "[%s] ILaserMgr uninit() done", __FUNCTION__);
        ILaserMgr::getInstance().uninit(m_i4CurrSensorDev);
        m_bLDAFEn = MFALSE;
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::prepareLDData()
{
    if (m_bLDAFEn == MTRUE)
    {
        MINT32 LaserStatus = ILaserMgr::getInstance().getLaserCurStatus(m_i4CurrSensorDev);

        if( LaserStatus == STATUS_RANGING_VALID)
        {
            m_sAFInput.sLaserInfo.i4CurPosDAC   = ILaserMgr::getInstance().getLaserCurDac(m_i4CurrSensorDev);
            m_sAFInput.sLaserInfo.i4CurPosDist  = ILaserMgr::getInstance().getLaserCurDist(m_i4CurrSensorDev);
        }
        else
        {
            m_sAFInput.sLaserInfo.i4CurPosDAC   = ILaserMgr::getInstance().predictAFStartPosDac(m_i4CurrSensorDev);
            m_sAFInput.sLaserInfo.i4CurPosDist  = ILaserMgr::getInstance().predictAFStartPosDist(m_i4CurrSensorDev);
            m_sAFInput.sLaserInfo.i4AfWinPosCnt = 0;
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::initSD()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s]", __FUNCTION__);
    m_sDAF_TBL.is_daf_run = E_DAF_OFF;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::startSD()
{
    CAM_LOGD_IF( m_i4DgbLogLv, "[%s]", __FUNCTION__);

    if( m_i4CurrLensId != 0xffff)
    {
        MINT32 AFtbl_Num;
        MINT32 AFtbl_Marco;
        MINT32 AFtbl_Inf;

        m_bSDAFEn = (m_ptrNVRam->rAFNVRAM.i4SDAFCoefs[1]>0) ? MTRUE : MFALSE;
        AFtbl_Num =  m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4NormalNum;
        if(AFtbl_Num>1)
        {
            AFtbl_Inf   = m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Offset + m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[0];
            AFtbl_Marco = m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Offset + m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[AFtbl_Num-1];
        }
        else //AF table method2
        {
            AFtbl_Inf   = m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[25] - m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[21];
            if(AFtbl_Inf   < 0        ) AFtbl_Inf=0;
            AFtbl_Marco = AFtbl_Inf + m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[24]+ m_ptrNVRam->rAFNVRAM.sAF_Coef.sTABLE.i4Pos[21];
            if(AFtbl_Marco > 1023     ) AFtbl_Marco=1023;
            if(AFtbl_Marco < AFtbl_Inf) AFtbl_Marco=AFtbl_Inf+1;
        }

        m_sDAF_TBL.af_dac_min        = AFtbl_Inf;
        m_sDAF_TBL.af_dac_max        = AFtbl_Marco;
        m_sDAF_TBL.af_dac_inf        = AFtbl_Inf;
        m_sDAF_TBL.af_dac_marco      = AFtbl_Marco;
        m_sDAF_TBL.af_distance_inf   = 0;
        m_sDAF_TBL.af_distance_marco = 0;

        CAM_LOGD( "AF-%-15s: default data [af_dac_inf]%d [af_dac_marco]%d [af_dac_min]%d [af_dac_max]%d [af_distance_inf]%d [af_distance_marco]%d\n",
                  __FUNCTION__,
                  (MINT32)m_sDAF_TBL.af_dac_inf,
                  (MINT32)m_sDAF_TBL.af_dac_marco,
                  (MINT32)m_sDAF_TBL.af_dac_min,
                  (MINT32)m_sDAF_TBL.af_dac_max,
                  (MINT32)m_sDAF_TBL.af_distance_inf,
                  (MINT32)m_sDAF_TBL.af_distance_marco);
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::prepareSDData()
{

}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::updateSDParam()
{
    //==========
    // Prepare AF info to Vsdof
    //==========
    if( m_sDAF_TBL.is_daf_run & E_DAF_RUN_STEREO)
    {
        MUINT32 MagicNum, CurDafTblIdx;

        MagicNum = m_u4StaMagicNum + 1; // m_sAFOutput can match next image status

        CurDafTblIdx = MagicNum % DAF_TBL_QLEN;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].frm_mun          = MagicNum;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_mode          = m_eCurAFMode;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_roi_sel       = m_sAFOutput.i4ROISel;
        // Contrast AF output info
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_dac_pos       = m_sAFOutput.i4AFPos;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].thermal_lens_pos = m_sAFOutput.i4ThermalLensPos;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].posture_dac      = m_sAFOutput.i4PostureDac;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_dac_index     = m_sAFOutput.i4AfDacIndex;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].is_af_stable     = ((m_i4IsAFSearch_CurState==AF_SEARCH_DONE) && m_sCurLensInfo.bIsMotorOpen)?1:0;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].is_scene_stable  = !m_sAFOutput.i4IsSceneChange;

        if (m_i4IsAFSearch_CurState != AF_SEARCH_DONE)
        {
            if (m_sArea_TypeSel == AF_ROI_SEL_AP)
            {
                m_sArea_Bokeh = m_sArea_Focusing;
            }
            else if (m_sArea_TypeSel == AF_ROI_SEL_FD)
            {
                // use the current face roi to avoid the finally inconsistency between depth point and focusing roi
                m_sArea_Bokeh = m_sArea_OTFD;
            }
            else
            {
                if (m_i4IsAFSearch_CurState == AF_SEARCH_CONTRAST)
                {
                    m_sArea_Bokeh = m_sArea_Focusing;
                }
                else
                {
                    m_sArea_Bokeh = m_sAFOutput.sPDWin;
                }
            }
        }

        if ((m_sArea_Bokeh.i4W == 0) || (m_sArea_Bokeh.i4H == 0))
        {
            m_sArea_Bokeh = m_sArea_Center;
        }

        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_start_x = (MUINT16)m_sArea_Bokeh.i4X;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_start_y = (MUINT16)m_sArea_Bokeh.i4Y;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_end_x   = (MUINT16)m_sArea_Bokeh.i4X + m_sArea_Bokeh.i4W;
        m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_end_y   = (MUINT16)m_sArea_Bokeh.i4Y + m_sArea_Bokeh.i4H;

        CAM_LOGD_IF(m_i4DgbLogLv, "[%s] #%d X1(%d) X2(%d) Y1(%d) Y2(%d), isAFStable(%d), DAC(%d)", __FUNCTION__, CurDafTblIdx,
                    m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_start_x, m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_end_x,
                    m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_start_y, m_sDAF_TBL.daf_vec[CurDafTblIdx].af_win_end_y,
                    m_sDAF_TBL.daf_vec[CurDafTblIdx].is_af_stable, m_sDAF_TBL.daf_vec[CurDafTblIdx].af_dac_pos);
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::initSP()
{
    // --- Create SensorProvider Object ---
    AAA_TRACE_D("SensorProvider");
    if( mpSensorProvider == NULL)
    {
        mpSensorProvider = SensorProvider::createInstance(LOG_TAG);

        if( mpSensorProvider!=nullptr)
        {
            MUINT32 interval= 30;

            interval = property_get_int32("vendor.debug.af_mgr.gyrointerval", SENSOR_GYRO_POLLING_MS);
            if( mpSensorProvider->enableSensor(SENSOR_TYPE_GYRO, interval))
            {
                m_bGryoVd = MTRUE;
                CAM_LOGD("Dev(%d):enable SensorProvider success for Gyro ", m_i4CurrSensorDev);
            }
            else
            {
                m_bGryoVd = MFALSE;
                CAM_LOGE("Enable SensorProvider fail for Gyro");
            }

            interval = property_get_int32("vendor.debug.af_mgr.gyrointerval", SENSOR_ACCE_POLLING_MS);
            if( mpSensorProvider->enableSensor(SENSOR_TYPE_ACCELERATION, interval))
            {
                m_bAcceVd = MTRUE;
                CAM_LOGD("Dev(%d):enable SensorProvider success for Acce ", m_i4CurrSensorDev);
            }
            else
            {
                m_bAcceVd = MFALSE;
                CAM_LOGE("Enable SensorProvider fail for Acce");
            }

        }
    }
    AAA_TRACE_END_D;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::unintSP()
{
    if( mpSensorProvider != NULL)
    {
        mpSensorProvider->disableSensor(SENSOR_TYPE_GYRO);
        mpSensorProvider->disableSensor(SENSOR_TYPE_ACCELERATION);
        mpSensorProvider = NULL;
    }
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MVOID AfMgr::prepareSPData()
{
    char  dbgMsgBuf[DBG_MSG_BUF_SZ];
    char* ptrMsgBuf = dbgMsgBuf;

    //-------------------------------------------------------------------------------------------------------------
    MUINT32 u4ACCEScale=0, u4GyroScale=0;

    if(m_ptrNVRam->rAFNVRAM.i4SensorEnable > 0)
    {
        // get gyro/acceleration data
        SensorData gyroDa;
        MBOOL gyroDaVd = m_bGryoVd ? mpSensorProvider->getLatestSensorData( SENSOR_TYPE_GYRO, gyroDa) : MFALSE;

        if( gyroDaVd && gyroDa.timestamp)
        {
            m_u8PreGyroTS = m_u8GyroTS;
            m_i4GyroInfo[0] = gyroDa.gyro[0] * SENSOR_GYRO_SCALE;
            m_i4GyroInfo[1] = gyroDa.gyro[1] * SENSOR_GYRO_SCALE;
            m_i4GyroInfo[2] = gyroDa.gyro[2] * SENSOR_GYRO_SCALE;
            m_u8GyroTS = gyroDa.timestamp;

            u4GyroScale = m_u8GyroTS!=m_u8PreGyroTS ? SENSOR_GYRO_SCALE : 0;
        }
        else
        {
            CAM_LOGD_IF( m_i4DgbLogLv, "Gyro InValid!");
            u4GyroScale = 0; // set scale 0 means invalid to algo
        }

        SensorData acceDa;
        MBOOL acceDaVd = m_bAcceVd ? mpSensorProvider->getLatestSensorData( SENSOR_TYPE_ACCELERATION, acceDa) : MFALSE;

        if( acceDaVd && acceDa.timestamp)
        {
            m_u8PreAcceTS = m_u8AcceTS;
            m_i4AcceInfo[0] = acceDa.acceleration[0] * SENSOR_ACCE_SCALE;
            m_i4AcceInfo[1] = acceDa.acceleration[1] * SENSOR_ACCE_SCALE;
            m_i4AcceInfo[2] = acceDa.acceleration[2] * SENSOR_ACCE_SCALE;
            m_u8AcceTS = acceDa.timestamp;

            u4ACCEScale = m_u8AcceTS!=m_u8PreAcceTS ? SENSOR_ACCE_SCALE : 0;
        }
        else
        {
            CAM_LOGD_IF( m_i4DgbLogLv, "Acce InValid!");
            u4ACCEScale = 0; // set scale 0 means invalid to algo
        }
    }

    if (m_pIAfAlgo)
    {
        m_pIAfAlgo->setAcceSensorInfo( m_i4AcceInfo, u4ACCEScale);
        m_pIAfAlgo->setGyroSensorInfo( m_i4GyroInfo, u4GyroScale);
    }

    if( m_i4DgbLogLv)
    {
        ptrMsgBuf += sprintf( ptrMsgBuf,
                              "Acce preTS(%11lld) TS(%11lld) Info(%4d %4d %4d)/Gyro preTS(%11lld) TS(%11lld) Info(%4d %4d %4d)/",
                              m_u8PreAcceTS,
                              m_u8AcceTS,
                              m_i4AcceInfo[0],
                              m_i4AcceInfo[1],
                              m_i4AcceInfo[2],
                              m_u8PreGyroTS,
                              m_u8GyroTS,
                              m_i4GyroInfo[0],
                              m_i4GyroInfo[1],
                              m_i4GyroInfo[2]);
    }

    MINT32 SqrGyroX = m_i4GyroInfo[0]*m_i4GyroInfo[0];
    MINT32 SqrGyroY = m_i4GyroInfo[1]*m_i4GyroInfo[1];
    MINT32 SqrGyroZ = m_i4GyroInfo[2]*m_i4GyroInfo[2];
    m_i4GyroValue = (MINT32)(sqrt((double)(SqrGyroX+SqrGyroY+SqrGyroZ)));

    if( m_i4DgbLogLv)
    {
        ptrMsgBuf += sprintf( ptrMsgBuf,
                              " Gyro VectorValue(%4d)",
                              m_i4GyroValue);
    }

    CAM_LOGD_IF( m_i4DgbLogLv, "%s %d", dbgMsgBuf, (MINT32)(ptrMsgBuf-dbgMsgBuf));
}

