/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef _AF_MGR_IF_H_
#define _AF_MGR_IF_H_

#include <af_param.h>
#include <dbg_aaa_param.h>
#include <utils/Vector.h>
#include <dbg_aaa_param.h>
#include <isp_config/af_config.h>
#include <ae_param.h>

//define command interface is called by host or af_mgr internally.
#define AF_MGR_CALLER 0
#define AF_CMD_CALLER 1


namespace NS3Av3
{

typedef enum
{
    E_AF_INACTIVE,
    E_AF_PASSIVE_SCAN,
    E_AF_PASSIVE_FOCUSED,
    E_AF_ACTIVE_SCAN,
    E_AF_FOCUSED_LOCKED,
    E_AF_NOT_FOCUSED_LOCKED,
    E_AF_PASSIVE_UNFOCUSED,
} E_AF_STATE_T;

// af_cxu::config
typedef struct ConfigAFInput_t
{
    AF_CONFIG_T *defaultConfig;
} ConfigAFInput_T;

typedef struct ConfigAFOutput_t
{
    AF_CONFIG_INFO_T hwConfigInfo;
    AFResultConfig_T *resultConfig;
} ConfigAFOutput_T;

// af_cxu::start
typedef struct InitAFInput_t
{
    // -1 : no valid data from OTP
    MINT32          otpInfPos;
    MINT32          otpMacroPos;
    MINT32          otpMiddlePos;
    LIB3A_AF_MODE_T libAfMode;

    MINT32           *otpAfTableStr;
    MINT32           *otpAfTableEnd;
    AF_INPUT_T       *afInput;
    AF_PARAM_T const *defaultParam; // a pointer of the constant content
    AF_CONFIG_T      *defaultCfg;
    AF_NVRAM_T       *afNVRAM;
    DUALCAM_NVRAM_T  *dualCamNVRAM;
} InitAFInput_T;
typedef AF_OUTPUT_T& InitAFOutput_T;

// af_cxu::doAF
typedef struct DoAFInput_t
{
    AFCommand_T afCommand;
    AF_INPUT_T afInput;
} DoAFInput_T;
typedef AF_OUTPUT_T& DoAFOutput_T;

/*******************************************************************************
*
*******************************************************************************/
class IAfMgr
{
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //    Ctor/Dtor.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
private:    ////    Disallowed.
    //    Copy constructor is disallowed.
    IAfMgr(IAfMgr const&);
    //    Copy-assignment operator is disallowed.
    IAfMgr& operator=(IAfMgr const&);
public:  ////
    IAfMgr() {};
    virtual ~IAfMgr() {};

    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //    Operations.
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    static IAfMgr& getInstance(MINT32 sensorDev);

    //=====Init=====//
#if 1
    virtual MINT32 init(MINT32 sensorIdx);
    virtual MINT32 PDPureRawInterval();
#else
    virtual MINT32 init(AFInitInfo);
#endif
    virtual MINT32 camPwrOn();
    virtual MINT32 config(); // ISP5.0: Start
#if 1
    virtual MINT32 start();  // ISP5.0: AFThreadStart
    virtual MBOOL setSensorMode( MINT32 i4NewSensorMode, MUINT32 i4BINInfo_SzW, MUINT32 i4BINInfo_SzH);
    virtual MRESULT setFullScanstep( MINT32 a_i4Step);
    virtual MVOID SetAETargetMode(eAETargetMODE eAETargetMode);
#else
    virtual MINT32 start(AFStartInfo);  // ISP5.0: AFThreadStart
#endif
    //=====Uninit=====//
    virtual MINT32 stop();
    virtual MINT32 camPwrOff();
    virtual MINT32 uninit();

    //=====Process=====//
    virtual MINT32 process(AFInputData_T data, AFCommand_T command);
#if 1
    virtual MVOID   setIspSensorInfo2AF(ISP_SENSOR_INFO_T ispSensorInfo);
    virtual MRESULT setAFMode( MINT32 a_eAFMode, MUINT32 u4Caller);
    virtual MRESULT SetPauseAF( MBOOL bIsPause);
    virtual MVOID setAE2AFInfo( AE2AFInfo_T &rAEInfo);
    virtual MRESULT passAFBuffer( MVOID *ptrInAFData);
    virtual MINT32 sendAFCtrl(MUINT32 eAFCtrl, MINTPTR iArg1, MINTPTR iArg2);
    virtual MRESULT setMFPos( MINT32 a_i4Pos, MUINT32 u4Caller);
    virtual MVOID updateSensorListenerParams(MINT32 *i4SensorInfo);
#endif

    virtual MINT32 getResult(AFResult_T &result);
    virtual MVOID  getHWCfgReg(AFResultConfig_T *pResultConfig);

#if 1
    virtual MINT32 getMaxLensPos();
    virtual MINT32 getMinLensPos();
    virtual MINT32 getLensMoving();
    virtual MINT32 getAFBestPos();
    virtual MINT32 getAFPos();
    virtual MINT32 getAFStable();
    virtual MINT32 getAFTableOffset();
    virtual MINT32 getAFTableMacroIdx();
    virtual MINT32 getAFTableIdxNum();
    virtual MVOID* getAFTable();
    virtual MINT32 getDAFtbl( MVOID **ptbl);
    virtual MRESULT getFocusArea( std::vector<MINT32> &vecOut);
    virtual MINT32 isFocusFinish();
    virtual MINT32 isFocused();
    virtual MINT32 isLockAE();
    virtual E_AF_STATE_T getAFState();
    virtual MRESULT getFocusAreaResult( std::vector<MINT32> &vecOutPos, std::vector<MUINT8> &vecOutRes, MINT32 &i4OutSzW, MINT32 &i4OutSzH);
    virtual MRESULT getDebugInfo( AF_DEBUG_INFO_T &rAFDebugInfo);
    virtual MVOID getAFRefWin( CameraArea_T &rWinSize);
    virtual MVOID getAF2AEInfo( AF2AEInfo_T &rAFInfo);
    virtual MINT32 getLensState();
#endif

    //=====Control=====//
    virtual MINT32 isAFSupport();
    virtual MRESULT setFDInfo( MVOID *a_sFaces);
    virtual MRESULT setOTInfo( MVOID *a_sObtinfo);
    virtual MRESULT setNVRAMIndex(MUINT32 a_eNVRAMIndex);
    //virtual MINT32 getFSCInitInfo(FSC_DAC_INIT_INFO_t &fscInitInfo);
    virtual MVOID TimeOutHandle();
    virtual MINT32 getAFMaxAreaNum();
    virtual MUINT64 MoveLensTo( MINT32 &i4TargetPos, MUINT32 u4Caller);

    // OBSELETE
    virtual MVOID   setAperture( MFLOAT lens_aperture);
    virtual MFLOAT  getAperture();
    virtual MVOID   setFilterDensity( MFLOAT lens_filterDensity);
    virtual MFLOAT  getFilterDensity();
    virtual MVOID   setFocalLength( MFLOAT lens_focalLength);
    virtual MFLOAT  getFocalLength();
    virtual MFLOAT  getFocusDistance();
    virtual MVOID   setOpticalStabilizationMode (MINT32 ois_OnOff);
    virtual MINT32  getOpticalStabilizationMode();
    virtual MVOID   getFocusRange( MFLOAT *vnear, MFLOAT *vfar);

    // CCT feature
    virtual MRESULT CCTMCUNameinit( MINT32 i4SensorIdx);
    virtual MRESULT CCTMCUNameuninit();
    virtual MINT32 CCTOPAFOpeartion();
    virtual MINT32 CCTOPCheckAutoFocusDone();
    virtual MINT32 CCTOPMFOpeartion(MINT32 a_i4MFpos);
    virtual MINT32 CCTOPAFGetAFInfo(MVOID *a_pAFInfo, MUINT32 *a_pOutLen);
    virtual MINT32 CCTOPAFGetBestPos(MINT32 *a_pAFBestPos, MUINT32 *a_pOutLen);
    virtual MINT32 CCTOPAFCaliOperation(MVOID *a_pAFCaliData, MUINT32 *a_pOutLen);
    virtual MINT32 CCTOPAFSetFocusRange(MVOID *a_pFocusRange);
    virtual MINT32 CCTOPAFGetFocusRange(MVOID *a_pFocusRange, MUINT32 *a_pOutLen);
    virtual MINT32 CCTOPAFGetNVRAMParam(MVOID *a_pAFNVRAM, MUINT32 *a_pOutLen);
    virtual MINT32 CCTOPAFApplyNVRAMParam(MVOID *a_pAFNVRAM, MUINT32 u4CamScenarioMode = 0);
    virtual MINT32 CCTOPAFSaveNVRAMParam();
    virtual MINT32 CCTOPAFGetFV(MVOID *a_pAFPosIn, MVOID *a_pAFValueOut, MUINT32 *a_pOutLen);
    virtual MINT32 CCTOPAFEnable();
    virtual MINT32 CCTOPAFDisable();
    virtual MINT32 CCTOPAFGetEnableInfo(MVOID *a_pEnableAF, MUINT32 *a_pOutLen);
    virtual MRESULT CCTOPAFSetAfArea(MUINT32 a_iPercent);

    // AF Sync
    virtual MVOID  SyncAFReadDatabase();
    virtual MVOID  SyncAFWriteDatabase();
    virtual MVOID  SyncAFSetMode( MINT32 a_i4SyncMode);
    virtual MVOID  SyncAFGetMotorRange( AF_SyncInfo_T& sCamInfo);
    virtual MINT32 SyncAFGetInfo( AF_SyncInfo_T& sCamInfo);
    virtual MVOID  SyncAFSetInfo( MINT32 a_i4Pos, AF_SyncInfo_T& sSlaveHisCamInfo);
    virtual MVOID  SyncAFGetCalibPos( AF_SyncInfo_T& sCamInfo);
    virtual MVOID  SyncAFCalibPos( AF_SyncInfo_T& sCamInfo);
};
};    //    namespace NS3Av3
#endif // _AE_MGR_N3D_H_

