/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "ADP_HAL"

#include "adp_hal_imp.h"
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/feature/stereo/hal/stereo_common.h>
#include <mtkcam/feature/stereo/hal/stereo_size_provider.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>
#include <stereo_tuning_provider.h>
#include "../inc/stereo_dp_util.h"
#include <vsdof/hal/ProfileUtil.h>

using namespace StereoHAL;

//NOTICE: property has 31 characters limitation
//debug.STEREO.log.hal.occ [0: disable] [1: enable]
#define LOG_PERPERTY    PROPERTY_ENABLE_LOG".hal.adp"

#define ADP_HAL_DEBUG

#ifdef ADP_HAL_DEBUG    // Enable debug log.

#if defined(__func__)
#undef __func__
#endif
#define __func__ __FUNCTION__

#define MY_LOGD(fmt, arg...)    CAM_LOGD("[%s]" fmt, __func__, ##arg)
#define MY_LOGI(fmt, arg...)    CAM_LOGI("[%s]" fmt, __func__, ##arg)
#define MY_LOGW(fmt, arg...)    CAM_LOGW("[%s] WRN(%5d):" fmt, __func__, __LINE__, ##arg)
#define MY_LOGE(fmt, arg...)    CAM_LOGE("[%s] %s ERROR(%5d):" fmt, __func__,__FILE__, __LINE__, ##arg)

#else   // Disable debug log.
#define MY_LOGD(a,...)
#define MY_LOGI(a,...)
#define MY_LOGW(a,...)
#define MY_LOGE(a,...)

#endif  // ADP_HAL_DEBUG

#define MY_LOGD_IF(cond, arg...)    if (cond) { MY_LOGD(arg); }
#define MY_LOGI_IF(cond, arg...)    if (cond) { MY_LOGI(arg); }
#define MY_LOGW_IF(cond, arg...)    if (cond) { MY_LOGW(arg); }
#define MY_LOGE_IF(cond, arg...)    if (cond) { MY_LOGE(arg); }

#define SINGLE_LINE_LOG 0
#if (1==SINGLE_LINE_LOG)
#define FAST_LOGD(fmt, arg...)  __fastLogger.FastLogD(fmt, ##arg)
#define FAST_LOGI(fmt, arg...)  __fastLogger.FastLogI(fmt, ##arg)
#else
#define FAST_LOGD(fmt, arg...)  __fastLogger.FastLogD("[%s]" fmt, __func__, ##arg)
#define FAST_LOGI(fmt, arg...)  __fastLogger.FastLogI("[%s]" fmt, __func__, ##arg)
#endif
#define FAST_LOGW(fmt, arg...)  __fastLogger.FastLogW("[%s] WRN(%5d):" fmt, __func__, __LINE__, ##arg)
#define FAST_LOGE(fmt, arg...)  __fastLogger.FastLogE("[%s] %s ERROR(%5d):" fmt, __func__,__FILE__, __LINE__, ##arg)

#define FAST_LOG_PRINT  __fastLogger.print()

ADP_HAL *
ADP_HAL::createInstance(ADP_HAL_INIT_PARAM &initParam)
{
    return new ADP_HAL_IMP(initParam);
}

ADP_HAL_IMP::ADP_HAL_IMP(ADP_HAL_INIT_PARAM &initParam)
    : LOG_ENABLED( StereoSettingProvider::isLogEnabled(LOG_PERPERTY) )
    , __fastLogger(LOG_TAG, LOG_PERPERTY)
{
    __fastLogger.setSingleLineMode(SINGLE_LINE_LOG);

    //Create ADP instance
    __pADP = MTKADP::createInstance();
    if(NULL == __pADP) {
        MY_LOGE("Cannot create ADP");
        return;
    }

    //Init ADP
    ::memset(&__initInfo, 0, sizeof(__initInfo));
    __initInfo.eExeEnvMode     = (initParam.isSecurity) ? ADP_EXE_ENV_MTEE : ADP_EXE_ENV_UREE;
    __initInfo.i4DepthMode     = StereoSettingProvider::getSensorRelativePosition();
    __initInfo.eInputImgFormat = ADP_INPUT_IMAGE_DISABLE;
    __initInfo.u4FlipFlag      = (STEREO_SENSOR_PROFILE_FRONT_FRONT == StereoSettingProvider::stereoProfile());

    //=== Init sizes ===
    StereoArea inputArea = StereoSizeProvider::getInstance()->getBufferSize(E_DMP_H, eSTEREO_SCENARIO_PREVIEW);
    __initInfo.u2DepthInW = inputArea.size.w;
    __initInfo.u2DepthInH = inputArea.size.h;
    __initInfo.u2DepthInX = inputArea.startPt.x;
    __initInfo.u2DepthInY = inputArea.startPt.y;

    MSize outputSize = StereoSizeProvider::getInstance()->getBufferSize(E_DEPTH_MAP, eSTEREO_SCENARIO_PREVIEW);
    __initInfo.u2DepthOutW = outputSize.w;
    __initInfo.u2DepthOutH = outputSize.h;

    //=== Init tuning info ===
    std::vector<std::pair<std::string, int>> tuningParamList;
    StereoTuningProvider::getADPTuningInfo(tuningParamList);

    for(auto &param : tuningParamList) {
        ADP_TUNING_PARAM_T tuning;
        strncpy((char *)tuning.szKey, param.first.c_str(), ADP_PARAM_KEY_MAX_LEN-1);
        tuning.i4Value = param.second;
        __tuningParams.push_back(tuning);
    }

    __initInfo.rTuningInfo.u4NumOfParam = __tuningParams.size();
    __initInfo.rTuningInfo.prParams     = &__tuningParams[0];

    switch(StereoSettingProvider::getModuleRotation())
    {
    case eRotate_90:
        __initInfo.eRotateFlag = ADP_ROTATE_COUNTER_CLOCKWISE_90;
        break;
    case  eRotate_270:
        __initInfo.eRotateFlag = ADP_ROTATE_CLOCKWISE_90;
        break;
    case eRotate_180:
        __initInfo.eRotateFlag = ADP_ROTATE_180;
        break;
    case eRotate_0:
    default:
        __initInfo.eRotateFlag = ADP_ROTATE_0;
        break;
    }

    __pADP->Init(&__initInfo);

    //Get working buffer size
    ADP_WORKING_BUFFER_INFO_T sizeInfo;
    MRESULT result = __pADP->FeatureCtrl(ADP_FEATURE_GET_WORKBUF_SIZE, NULL, &sizeInfo);
    if(S_ADP_OK != result) {
        MY_LOGE("Fail to get woking buffer size, result %d", result);
    }

    //Allocate working buffer and set to ADP
    if(sizeInfo.u4Size > 0) {
        __workingBuffer.u4Size = sizeInfo.u4Size;
        __workingBuffer.pcBuffer = new(std::nothrow) MINT8[__workingBuffer.u4Size];
        if(__workingBuffer.pcBuffer) {
            MY_LOGD_IF(LOG_ENABLED, "Alloc %d bytes for ADP working buffer", __workingBuffer.u4Size);
            result = __pADP->FeatureCtrl(ADP_FEATURE_SET_WORKBUF_ADDR, &__workingBuffer, 0);
            if(S_ADP_OK != result) {
                MY_LOGE("Fail to set woking buffer size, result %d", result);
            }
        } else {
            MY_LOGE("Cannot create ADP working buffer of size %d", __workingBuffer.u4Size);
        }
    } else {
        MY_LOGW("Working buffer size is 0");
    }

    __logInitData();
}

ADP_HAL_IMP::~ADP_HAL_IMP()
{
    MY_LOGD_IF(LOG_ENABLED, "+");
    if(__workingBuffer.pcBuffer) {
        delete [] __workingBuffer.pcBuffer;
        __workingBuffer.pcBuffer = NULL;
        __workingBuffer.u4Size   = 0;
    }

    if(__pADP) {
        __pADP->Reset();
        __pADP->destroyInstance();
        __pADP = NULL;
    }

    MY_LOGD_IF(LOG_ENABLED, "-");
}

bool
ADP_HAL_IMP::ADPHALRun(ADP_HAL_IN_DATA &inParams, ADP_HAL_OUT_DATA &output)
{
    AutoProfileUtil profile(LOG_TAG, __FUNCTION__);
    __setParams(inParams, output);

    //================================
    //  Run ADP
    //================================
    {
        AutoProfileUtil profile(LOG_TAG, "Run ADP Main");
        MRESULT result = __pADP->Main();
        if(S_ADP_OK != result) {
            MY_LOGE("Fail to run Main, result %d", result);
        }
    }

    return true;
}

void
ADP_HAL_IMP::__setParams(ADP_HAL_IN_DATA &inParams, ADP_HAL_OUT_DATA &output)
{
    AutoProfileUtil profile(LOG_TAG, "ADP Set Proc");

    MY_LOGD_IF(LOG_ENABLED, "+");

    if(ADP_EXE_ENV_MTEE == __initInfo.eExeEnvMode) {
        __procInfo.rBufferDepthInL.bIsSharedMem = true;
        __procInfo.rBufferDepthInL.u4Handle0    = (MUINT32)inParams.dispMapL->getBufPA(0);

        __procInfo.rBufferDepthInR.bIsSharedMem = true;
        __procInfo.rBufferDepthInR.u4Handle0    = (MUINT32)inParams.dispMapR->getBufPA(0);

        __procInfo.rBufferConfIn.bIsSharedMem   = true;
        __procInfo.rBufferConfIn.u4Handle0      = (MUINT32)inParams.confidenceMap->getBufPA(0);

        __procInfo.rBufferDepthOut.bIsSharedMem = true;
        __procInfo.rBufferDepthOut.u4Handle0    = (MUINT32)output.depthmap->getBufPA(0);
    } else {
        __procInfo.rBufferDepthInL.pcBuffer0 = (MINT8 *)inParams.dispMapL->getBufVA(0);
        __procInfo.rBufferDepthInL.u4Size0   = inParams.dispMapL->getBufSizeInBytes(0);

        __procInfo.rBufferDepthInR.pcBuffer0 = (MINT8 *)inParams.dispMapR->getBufVA(0);
        __procInfo.rBufferDepthInR.u4Size0   = inParams.dispMapR->getBufSizeInBytes(0);

        __procInfo.rBufferConfIn.pcBuffer0   = (MINT8 *)inParams.confidenceMap->getBufVA(0);
        __procInfo.rBufferConfIn.u4Size0     = inParams.confidenceMap->getBufSizeInBytes(0);

        __procInfo.rBufferDepthOut.pcBuffer0 = (MINT8 *)output.depthmap->getBufVA(0);
        __procInfo.rBufferDepthOut.u4Size0   = output.depthmap->getBufSizeInBytes(0);
    }

    __procInfo.rBufferDepthInL.u2Width  = inParams.dispMapL->getImgSize().w;
    __procInfo.rBufferDepthInL.u2Height = inParams.dispMapL->getImgSize().h;
    __procInfo.rBufferDepthInR.u2Width  = inParams.dispMapR->getImgSize().w;
    __procInfo.rBufferDepthInR.u2Height = inParams.dispMapR->getImgSize().h;
    __procInfo.rBufferConfIn.u2Width    = inParams.confidenceMap->getImgSize().w;
    __procInfo.rBufferConfIn.u2Height   = inParams.confidenceMap->getImgSize().h;
    __procInfo.rBufferDepthOut.u2Width  = output.depthmap->getImgSize().w;
    __procInfo.rBufferDepthOut.u2Height = output.depthmap->getImgSize().h;

    //================================
    //  Set to ADP
    //================================
    __logSetProcData();
    MRESULT result = __pADP->FeatureCtrl(ADP_FEATURE_SET_PROC_INFO, &__procInfo, NULL);
    if(S_ADP_OK != result) {
        MY_LOGE("Fail to set proc info, result %d", result);
    }
    MY_LOGD_IF(LOG_ENABLED, "-");
}

void
ADP_HAL_IMP::__logInitData()
{
    if(!LOG_ENABLED) {
        return;
    }

    FAST_LOGD("========= ADP Init Info =========");
    FAST_LOGD("[eExeEnvMode]         %d", __initInfo.eExeEnvMode);
    FAST_LOGD("[i4DepthMode]         %d", __initInfo.i4DepthMode);
    FAST_LOGD("[eInputImgFormat]     %d", __initInfo.eInputImgFormat);

    FAST_LOGD("[u2DepthInX]          %d", __initInfo.u2DepthInX);
    FAST_LOGD("[u2DepthInY]          %d", __initInfo.u2DepthInY);
    FAST_LOGD("[u2DepthInW]          %d", __initInfo.u2DepthInW);
    FAST_LOGD("[u2DepthInH]          %d", __initInfo.u2DepthInH);

    FAST_LOGD("[u2DepthOutW]         %d", __initInfo.u2DepthOutW);
    FAST_LOGD("[u2DepthOutH]         %d", __initInfo.u2DepthOutH);

    FAST_LOGD("[Working buffer size] %d", __workingBuffer.u4Size);

    for(MUINT32 ti = 0; ti < __initInfo.rTuningInfo.u4NumOfParam; ++ti) {
        FAST_LOGD("[TuningInfo.params][%d] %s: %d", ti,
                  __tuningParams[ti].szKey,
                  __tuningParams[ti].i4Value);
    }

    FAST_LOG_PRINT;
}

void
ADP_HAL_IMP::__logSetProcData()
{
    if(!LOG_ENABLED) {
        return;
    }

    FAST_LOGD("========= ADP Set Proc Info =========");
    __logBufferInfo(__procInfo.rBufferDepthInL, "rBufferDepthInL");
    __logBufferInfo(__procInfo.rBufferDepthInR, "rBufferDepthInR");
    __logBufferInfo(__procInfo.rBufferConfIn,   "rBufferConfIn");
    __logBufferInfo(__procInfo.rBufferDepthOut, "rBufferDepthOut");

    FAST_LOG_PRINT;
}

void
ADP_HAL_IMP::__logBufferInfo(ADP_BUFFER_INFO_T &buf, const char *name)
{
    if(!LOG_ENABLED) {
        return;
    }

    if(ADP_EXE_ENV_MTEE == __initInfo.eExeEnvMode) {
        FAST_LOGD("[%16s][bIsSharedMem]         %d", name, buf.bIsSharedMem);
        FAST_LOGD("[%16s][u4Handle0]            %d", name, buf.u4Handle0);
    } else {
        FAST_LOGD("[%16s][pcBuffer0]            %p", name, buf.pcBuffer0);
        FAST_LOGD("[%16s][u4Size0]              %d", name, buf.u4Size0);
    }

    FAST_LOGD("[%16s][u2Width]              %d", name, buf.u2Width);
    FAST_LOGD("[%16s][u2Height]             %d", name, buf.u2Height);
}