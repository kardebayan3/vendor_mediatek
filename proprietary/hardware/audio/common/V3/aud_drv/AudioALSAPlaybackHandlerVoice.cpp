#include "AudioALSAPlaybackHandlerVoice.h"

#include "SpeechDriverFactory.h"
#include "SpeechBGSPlayer.h"

#include <SpeechUtility.h>


#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "AudioALSAPlaybackHandlerVoice"

namespace android {

const char *PROPERTY_KEY_BGS_NO_SLEEP = "persist.vendor.audiohal.speech.bgs.no_sleep";



AudioALSAPlaybackHandlerVoice::AudioALSAPlaybackHandlerVoice(const stream_attribute_t *stream_attribute_source) :
    AudioALSAPlaybackHandlerBase(stream_attribute_source),
    mBGSPlayer(BGSPlayer::GetInstance()) {
    mSpeechDriver = NULL;
    mPlaybackHandlerType = PLAYBACK_HANDLER_VOICE;
    mBGSPlayBuffer = NULL;

    memset((void *)&mOpenTime, 0, sizeof(mOpenTime));
    memset((void *)&mCurTime, 0, sizeof(mCurTime));
    mWriteCnt = 0;

    memset((void *)&mNewtimeLatency, 0, sizeof(mNewtimeLatency));
    memset((void *)&mOldtimeLatency, 0, sizeof(mOldtimeLatency));
    memset((void *)&mLatencyTimeMs, 0, sizeof(mLatencyTimeMs));

    mLatencyUs = 0;

    mBypassBgsSleep = 0;
}


AudioALSAPlaybackHandlerVoice::~AudioALSAPlaybackHandlerVoice() {
}

status_t AudioALSAPlaybackHandlerVoice::open() {
    // debug pcm dump
    OpenPCMDump(LOG_TAG);


    // HW attribute config // TODO(Harvey): query this
    mStreamAttributeTarget.audio_format = AUDIO_FORMAT_PCM_16_BIT;
    mStreamAttributeTarget.audio_channel_mask = AUDIO_CHANNEL_OUT_MONO;
    mStreamAttributeTarget.num_channels = popcount(mStreamAttributeTarget.audio_channel_mask);
    mStreamAttributeTarget.sample_rate = BGS_TARGET_SAMPLE_RATE; // same as source stream
    mStreamAttributeTarget.u8BGSDlGain = mStreamAttributeSource->u8BGSDlGain;
    mStreamAttributeTarget.u8BGSUlGain = mStreamAttributeSource->u8BGSUlGain;
    mStreamAttributeTarget.buffer_size = BGS_PLAY_BUFFER_LEN;

    mLatencyUs = getBufferLatencyUs(mStreamAttributeSource,
                                    mStreamAttributeSource->buffer_size);

    ALOGD("%s(), audio_mode: %d, u8BGSUlGain: %d, u8BGSDlGain: %d"
          ", audio_format: %d => %d, sample_rate: %u => %u, ch: %u => %u"
          ", buffer_size: (write)%u, (bgs)%u, flag: 0x%x, mLatencyUs: %u",
          __FUNCTION__, mStreamAttributeSource->audio_mode,
          mStreamAttributeSource->u8BGSUlGain,
          mStreamAttributeSource->u8BGSDlGain,
          mStreamAttributeSource->audio_format,
          mStreamAttributeTarget.audio_format,
          mStreamAttributeSource->sample_rate,
          mStreamAttributeTarget.sample_rate,
          mStreamAttributeSource->num_channels,
          mStreamAttributeTarget.num_channels,
          mStreamAttributeSource->buffer_size,
          mStreamAttributeTarget.buffer_size,
          mStreamAttributeSource->mAudioOutputFlags,
          (uint32_t)(mLatencyUs & 0xFFFFFFFF));


    // bit conversion
    initBitConverter();


    // open background sound
    if (mStreamAttributeTarget.num_channels > 2) {
        mBGSPlayBuffer = mBGSPlayer->CreateBGSPlayBuffer(
                             mStreamAttributeSource->sample_rate,
                             2,
                             mStreamAttributeTarget.audio_format);

    } else {
        mBGSPlayBuffer = mBGSPlayer->CreateBGSPlayBuffer(
                             mStreamAttributeSource->sample_rate,
                             mStreamAttributeSource->num_channels,
                             mStreamAttributeTarget.audio_format);
    }

    mSpeechDriver = SpeechDriverFactory::GetInstance()->GetSpeechDriver();
    mBGSPlayer->Open(mSpeechDriver, mStreamAttributeTarget.u8BGSUlGain, mStreamAttributeTarget.u8BGSDlGain);

    mBypassBgsSleep = get_uint32_from_property(PROPERTY_KEY_BGS_NO_SLEEP);

    clock_gettime(CLOCK_MONOTONIC, &mOpenTime);
    mWriteCnt = 0;

    clock_gettime(CLOCK_MONOTONIC, &mNewtimeLatency);
    mOldtimeLatency = mNewtimeLatency;

    return NO_ERROR;
}


status_t AudioALSAPlaybackHandlerVoice::close() {
    ALOGD("%s(), flag: 0x%x", __FUNCTION__, mStreamAttributeSource->mAudioOutputFlags);

    // close background sound
    mBGSPlayer->Close();

    mBGSPlayer->DestroyBGSPlayBuffer(mBGSPlayBuffer);


    // bit conversion
    deinitBitConverter();


    // debug pcm dump
    ClosePCMDump();

    return NO_ERROR;
}


status_t AudioALSAPlaybackHandlerVoice::routing(const audio_devices_t output_devices __unused) {
    return INVALID_OPERATION;
}

uint32_t AudioALSAPlaybackHandlerVoice::ChooseTargetSampleRate(uint32_t SampleRate) {
    ALOGD("ChooseTargetSampleRate SampleRate = %d ", SampleRate);
    uint32_t TargetSampleRate = 44100;
    if ((SampleRate % 8000) == 0) { // 8K base
        TargetSampleRate = 48000;
    }
    return TargetSampleRate;
}

ssize_t AudioALSAPlaybackHandlerVoice::write(const void *buffer, size_t bytes) {
    ALOGV("%s()", __FUNCTION__);

    uint64_t spendTimeUs = 0;
    uint64_t writeTimeUs = 0;
    uint64_t sleepUs = 0;

    mWriteCnt++;

    if (mSpeechDriver == NULL) {
        ALOGW("%s(), mSpeechDriver == NULL!!", __FUNCTION__);
        return bytes;
    }

    clock_gettime(CLOCK_MONOTONIC, &mNewtimeLatency);
    mLatencyTimeMs[0] = get_time_diff_ms(&mOldtimeLatency, &mNewtimeLatency);
    mOldtimeLatency = mNewtimeLatency;

    if (mSpeechDriver->CheckModemIsReady() == false) {
        uint32_t sleep_ms = getBufferLatencyMs(mStreamAttributeSource, bytes);
        if (sleep_ms != 0) {
            ALOGW("%s(), modem not ready, sleep %u ms", __FUNCTION__, sleep_ms);
            usleep(sleep_ms * 1000);
        }
        return bytes;
    }

    void *newbuffer[96 * 1024] = {0};
    unsigned char *aaa;
    unsigned char *bbb;
    size_t i = 0;
    size_t j = 0;
    int retval = 0;
    // const -> to non const
    void *pBuffer = const_cast<void *>(buffer);
    ASSERT(pBuffer != NULL);

    aaa = (unsigned char *)newbuffer;
    bbb = (unsigned char *)buffer;


    if (mStreamAttributeSource->audio_format == AUDIO_FORMAT_PCM_16_BIT) {
        if (mStreamAttributeTarget.num_channels == 8) {
            for (i = 0 ; j < bytes; i += 4) {
                memcpy(aaa + i, bbb + j, 4);
                j += 16;
            }
            bytes = (bytes >> 2);
        } else if (mStreamAttributeTarget.num_channels == 6) {
            for (i = 0 ; j < bytes; i += 4) {
                memcpy(aaa + i, bbb + j, 4);
                j += 12;
            }
            bytes = (bytes / 3);
        } else {
            memcpy(aaa, bbb, bytes);
        }
    } else {
        if (mStreamAttributeTarget.num_channels == 8) {
            for (i = 0 ; j < bytes; i += 8) {
                memcpy(aaa + i, bbb + j, 8);
                j += 32;
            }
            bytes = (bytes >> 2);
        } else if (mStreamAttributeTarget.num_channels == 6) {
            for (i = 0 ; j < bytes; i += 8) {
                memcpy(aaa + i, bbb + j, 8);
                j += 24;
            }
            bytes = (bytes / 3);
        } else {
            memcpy(aaa, bbb, bytes);
        }

    }

    // bit conversion
    void *pBufferAfterBitConvertion = NULL;
    uint32_t bytesAfterBitConvertion = 0;
    doBitConversion(newbuffer, bytes, &pBufferAfterBitConvertion, &bytesAfterBitConvertion);


    // write data to background sound
    WritePcmDumpData(pBufferAfterBitConvertion, bytesAfterBitConvertion);

    uint32_t u4WrittenBytes = BGSPlayer::GetInstance()->Write(mBGSPlayBuffer, pBufferAfterBitConvertion, bytesAfterBitConvertion);
    if (u4WrittenBytes != bytesAfterBitConvertion) { // TODO: 16/32
        ALOGE("%s(), BGSPlayer::GetInstance()->Write() error, u4WrittenBytes(%u) != bytesAfterBitConvertion(%u)", __FUNCTION__, u4WrittenBytes, bytesAfterBitConvertion);
    }
    clock_gettime(CLOCK_MONOTONIC, &mNewtimeLatency);
    mLatencyTimeMs[1] = get_time_diff_ms(&mOldtimeLatency, &mNewtimeLatency);
    mOldtimeLatency = mNewtimeLatency;

    /* HAL sleep latency time to consume data smoothly */
    if (mBypassBgsSleep == false) {
        clock_gettime(CLOCK_MONOTONIC, &mCurTime);
        spendTimeUs = get_time_diff_ns(&mOpenTime, &mCurTime) / 1000;
        writeTimeUs = mWriteCnt * mLatencyUs;
        if (spendTimeUs < writeTimeUs) {
            sleepUs = writeTimeUs - spendTimeUs;
            if (mBGSPlayBuffer->isBufferEnough()) {
                usleep(sleepUs);
            } else {
                if (sleepUs > 1000) {
                    sleepUs -= 1000;
                    usleep(sleepUs);
                } else {
                    sleepUs = 0;
                }
            }
        } else if (spendTimeUs > (writeTimeUs + MODEM_FRAME_MS * 1000)) {
            if (getBGSLogEnableByLevel(BGS_LOG_LEVEL_PLAYBACK_HANDLER)) {
                ALOGW("%s(), spendTimeUs %u, writeTimeUs %u", __FUNCTION__,
                      (uint32_t)(spendTimeUs & 0xFFFFFFFF),
                      (uint32_t)(writeTimeUs & 0xFFFFFFFF));
            }
        }
    }

    clock_gettime(CLOCK_MONOTONIC, &mNewtimeLatency);
    mLatencyTimeMs[2] = get_time_diff_ms(&mOldtimeLatency, &mNewtimeLatency);
    mOldtimeLatency = mNewtimeLatency;

    uint64_t logThresholdMs = (mLatencyUs) / 1000;
    if (logThresholdMs < MODEM_FRAME_MS) {
        logThresholdMs = MODEM_FRAME_MS;
    }

    if (mLatencyTimeMs[0] > logThresholdMs ||
        mLatencyTimeMs[1] > logThresholdMs ||
        mLatencyTimeMs[2] > logThresholdMs) {
        ALOGW("latency_in_ms, %3u, %3u, %3u, u4WrittenBytes: %u, mLatencyUs: %u, spendTimeUs: %u, writeTimeUs: %u, sleepUs: %u",
              (uint32_t)(mLatencyTimeMs[0] & 0xFFFFFFFF),
              (uint32_t)(mLatencyTimeMs[1] & 0xFFFFFFFF),
              (uint32_t)(mLatencyTimeMs[2] & 0xFFFFFFFF),
              u4WrittenBytes,
              (uint32_t)(mLatencyUs & 0xFFFFFFFF),
              (uint32_t)(spendTimeUs & 0xFFFFFFFF),
              (uint32_t)(writeTimeUs & 0xFFFFFFFF),
              (uint32_t)(sleepUs & 0xFFFFFFFF));
    } else if (getBGSLogEnableByLevel(BGS_LOG_LEVEL_PLAYBACK_HANDLER)) {
        ALOGD("latency_in_ms, %3u, %3u, %3u, u4WrittenBytes: %u, mLatencyUs: %u, spendTimeUs: %u, writeTimeUs: %u, sleepUs: %u",
              (uint32_t)(mLatencyTimeMs[0] & 0xFFFFFFFF),
              (uint32_t)(mLatencyTimeMs[1] & 0xFFFFFFFF),
              (uint32_t)(mLatencyTimeMs[2] & 0xFFFFFFFF),
              u4WrittenBytes,
              (uint32_t)(mLatencyUs & 0xFFFFFFFF),
              (uint32_t)(spendTimeUs & 0xFFFFFFFF),
              (uint32_t)(writeTimeUs & 0xFFFFFFFF),
              (uint32_t)(sleepUs & 0xFFFFFFFF));
    }

    return bytes;
}


} // end of namespace android
