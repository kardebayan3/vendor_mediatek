#ifndef ANDROID_SPEECH_BACKGROUND_SOUND_PLAYER_H
#define ANDROID_SPEECH_BACKGROUND_SOUND_PLAYER_H

#include <pthread.h>
#include "AudioType.h"
#include "AudioUtility.h"
#include "MtkAudioComponent.h"
#include <AudioLock.h>

namespace android {
// for debug
//#define DUMP_BGS_DATA
//#define DUMP_BGS_BLI_BUF
//#define BGS_USE_SINE_WAVE


#if defined(SPH_SR48K) || defined(SPH_SR32K)
#define BGS_TARGET_SAMPLE_RATE  (32000)
#else
#define BGS_TARGET_SAMPLE_RATE  (16000)
#endif

// BGS ring buffer
#define MODEM_FRAME_MS (20)
#define BGS_KEEP_NUM_FRAME (4)
#define BGS_EXTRA_NUM_FRAME (3)

#if BGS_KEEP_NUM_FRAME < BGS_EXTRA_NUM_FRAME
#error BGS_KEEP_NUM_FRAME >= BGS_EXTRA_NUM_FRAME
#endif

// pcm_16_bit * 1ch * sample rate * ms
#define BGS_PERIOD_SIZE \
    ((2 * 1 * BGS_TARGET_SAMPLE_RATE * MODEM_FRAME_MS) / 1000)

#define BGS_PLAY_BUFFER_LEN \
    ((BGS_PERIOD_SIZE * BGS_KEEP_NUM_FRAME) + RING_BUF_SIZE_OFFSET)


enum { /* uint32_t: bgs_log_level_t */ /* PROPERTY_KEY_BGS_LOG_LEVEL */
    BGS_LOG_LEVEL_PLAYBACK_HANDLER = 1,
    BGS_LOG_LEVEL_MODEM = 2,
    BGS_LOG_LEVEL_BGS = 4
};

bool getBGSLogEnableByLevel(const uint32_t bgs_log_level); /* bgs_log_level_t */


/*=============================================================================
 *                              Class definition
 *===========================================================================*/

class BGSPlayer;
class SpeechDriverInterface;

class BGSPlayBuffer {
public:
    bool        isBufferEnough(void);

private:
    BGSPlayBuffer();
    virtual ~BGSPlayBuffer(); // only destroied by friend class BGSPlayer
    friend          class BGSPlayer;
    status_t        InitBGSPlayBuffer(BGSPlayer *playPointer, uint32_t sampleRate, uint32_t chNum, int32_t mFormat);
    uint32_t        Write(char *buf, uint32_t num);
    bool        IsBGSBlisrcDumpEnable();

    int32_t         mFormat;
    RingBuf         mRingBuf;
    MtkAudioSrcBase     *mBliSrc;
    char           *mBliOutputLinearBuffer;

    AudioLock       mBGSPlayBufferRuningMutex;
    AudioLock       mBGSPlayBufferMutex;

    bool            mExitRequest;

    bool        mIsBGSBlisrcDumpEnable;
    FILE        *pDumpFile;
};

class BGSPlayer {
public:
    virtual ~BGSPlayer();

    static BGSPlayer        *GetInstance();
    BGSPlayBuffer        *CreateBGSPlayBuffer(uint32_t sampleRate, uint32_t chNum, int32_t format);
    void        DestroyBGSPlayBuffer(BGSPlayBuffer *pBGSPlayBuffer);
    bool        Open(SpeechDriverInterface *pSpeechDriver, uint8_t uplink_gain, uint8_t downlink_gain);
    bool        Close();
    bool        IsBGSDumpEnable();
    uint32_t        Write(BGSPlayBuffer *pBGSPlayBuffer, void *buf, uint32_t num);
    uint32_t        PutData(BGSPlayBuffer *pBGSPlayBuffer, char *target_ptr, uint16_t num_data_request);
    uint16_t        PutDataToSpeaker(char *target_ptr, uint16_t num_data_request);


private:
    BGSPlayer();

    static BGSPlayer        *mBGSPlayer; // singleton
    SpeechDriverInterface        *mSpeechDriver;
    SortedVector<BGSPlayBuffer *> mBGSPlayBufferVector;
    char        *mBufBaseTemp;

    AudioLock    mBGSPlayBufferVectorLock;
    AudioLock    mCountLock;
    uint16_t     mCount;

    bool        mIsBGSDumpEnable;
    FILE        *pDumpFile;

    uint16_t    mBgsPeriodSize;
    uint16_t    mUnderflowCount;
};


} // end namespace android

#endif //ANDROID_SPEECH_BACKGROUND_SOUND_PLAYER_H
