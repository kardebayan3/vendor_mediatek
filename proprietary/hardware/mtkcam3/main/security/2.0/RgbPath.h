/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _V2_0_RGBPATH_H_
#define _V2_0_RGBPATH_H_

#include <thread>

#include "IPath.h"
#include <utils/RefBase.h>
#include <utils/StrongPointer.h>
#include <semaphore.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>

#include <mtkcam3/pipeline/pipeline/IPipelineDAG.h>
#include <mtkcam3/pipeline/pipeline/IPipelineNode.h>

#include <mtkcam3/pipeline/pipeline/PipelineContext.h>

#include "StreamId.h"
#include <mtkcam3/pipeline/utils/streambuf/StreamBufferPool.h>
#include <mtkcam3/pipeline/utils/streambuf/StreamBuffers.h>
#include <mtkcam3/pipeline/utils/streaminfo/MetaStreamInfo.h>
#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>

#include "buffer/BufferCallbackHandler.h"
#include "buffer/StreamBufferProvider.h"

#include <mtkcam3/main/security/utils/StreamInfoHelper.h>

#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>

#include <mtkcam/drv/IHalSensor.h>

#include <mtkcam3/pipeline/hwnode/NodeId.h>
#include <mtkcam3/pipeline/hwnode/P1Node.h>
#include <mtkcam3/pipeline/hwnode/P2StreamingNode.h>

#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#include <mtkcam/utils/metastore/ITemplateRequest.h>
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <hardware/camera3.h> // for template

#include <mtkcam/drv/iopipe/CamIO/INormalPipe.h>

#include <mtkcam/utils/metadata/IMetadataTagSet.h>
#include <mtkcam/utils/metadata/IMetadataConverter.h>
#include <mtkcam/utils/metadata/IMetadata.h>

#include <mtkcam3/main/security/utils/Debug.h>

#include <mtkcam3/main/security/2.0/ISecureCamera.h>

using namespace NSCam;
using namespace NSCam::v3::Utils;
using namespace android;
using namespace NSCam::v3::NSPipelineContext;
using namespace std;

namespace NSCam {
namespace security {
namespace V2_0 {

// ---------------------------------------------------------------------------

class RgbPath;

// ---------------------------------------------------------------------------

struct CallbackListener
{
    virtual ~CallbackListener() = default;

    virtual void onMetaReceived(
            const uint32_t requestNo, const StreamId_T streamId,
            const bool errorResult, const IMetadata& result) = 0;

    virtual void onDataReceived(
            const uint32_t requestNo,
            const StreamId_T streamId,
            const android::sp<IImageBuffer>& buffer) = 0;
};

class ImageCallback final : public IImageCallback
{
public:
    ImageCallback(CallbackListener *listener) : mListener(listener) {}

    /**
     * interface of IImageCallback
     *
     * @param[in] RequestNo : request number.
     *
     * @param[in] pBuffer : IImageBuffer.
     *
     */
    MERROR onResultReceived(
            const uint32_t requestNo, const StreamId_T streamId,
            const bool errorBuffer, const android::sp<IImageBuffer>& buffer) override
    {
        (void) errorBuffer;

        if (mListener)
            mListener->onDataReceived(requestNo, streamId, buffer);

        return OK;
    }

private:
    CallbackListener *mListener;
};

class MetadataListener final : public ResultProcessor::IListener
{
public:
    MetadataListener(CallbackListener *listener)
        : mListener(listener)
    {}

    // interface of ResultProcessor::IListener
    void onResultReceived(
            const uint32_t requestNo, const StreamId_T streamId,
            const bool errorResult, const IMetadata& result) override
    {
        if (mListener)
            mListener->onMetaReceived(
                    requestNo, streamId, errorResult, result);
    };

    virtual void onFrameEnd(uint32_t __unused requestNo) override {};

    virtual String8 getUserName() override { return String8(DEBUG_LOG_TAG); }

private:
    CallbackListener *mListener;
};

static void StreamingLoop(RgbPath& camera);

// TODO: assure all APIs are thread-safe
class RgbPath final : public IPath, public CallbackListener
{
public:
    RgbPath(int maxBuffers);

    ~RgbPath();

    // interface of IPath
    void destroyInstance() override;

    Result init(const std::unordered_map<StreamID, Stream>& streamMap) override;

    Result unInit() override;

    Result setSrcDev(unsigned int devID) override;

    Result sensorPower(bool turnON) override;

    Result StreamingOn() override;

    Result StreamingOff() override;

    void onBufferReleased() override;

    void registerCallback(SecureCameraCallback* cb, void* priv);

private:
    friend void StreamingLoop(RgbPath& camera);

    void prepareConfiguration();

    void setupMetaStreamInfo();

    void setupImageStreamInfo();

    void createWorkingBuffers();

    void setupPipelineContext();

    void setupRequestBuilder();

    sp<IMetaStreamBuffer> get_default_request();

    IMetaStreamBuffer* createMetaStreamBuffer(
            android::sp<IMetaStreamInfo> pStreamInfo,
            IMetadata const& rSettings,
            MBOOL const repeating
            );

    void processRequest();

    void finishPipelineContext();

    // interface of CallbackListener
    void onMetaReceived(
            const uint32_t requestNo, const StreamId_T streamId,
            const bool errorResult, const IMetadata& result) override;

    void onDataReceived(
            const uint32_t requestNo,
            const StreamId_T streamId,
            const android::sp<IImageBuffer>& buffer) override;

    void setState(const State newState);
    State getState();

    std::pair<State, std::mutex> mState;

    IHalSensor* mpSensorHalObj;
    //
    unsigned int mSensorId;
    MUINT32 mRequestTemplate;

    P1Node::SensorParams mSensorParam;
    P1Node::ConfigParams mP1ConfigParam;
    P2StreamingNode::ConfigParams mP2ConfigParam;
    //
    MSize mRrzoSize;
    MINT mRrzoFormat;
    size_t mRrzoStride;
    //
    MSize mImgoSize;
    MINT mImgoFormat;
    size_t mImgoStride;
    //
    MSize mYuvSize;
    MINT mYuvFormat;
    //
    std::pair<sp<PipelineContext>, std::mutex> mContext;
    //
    // StreamInfos
    sp<IMetaStreamInfo> mControlMeta_App;
    sp<IMetaStreamInfo> mControlMeta_Hal;
    sp<IMetaStreamInfo> mResultMeta_P1_App;
    sp<IMetaStreamInfo> mResultMeta_P1_Hal;
    sp<IMetaStreamInfo> mResultMeta_P2_App;
    sp<IMetaStreamInfo> mResultMeta_P2_Hal;
    //
    sp<IImageStreamInfo> mImage_RrzoRaw;
    sp<IImageStreamInfo> mImage_ImgoRaw;
    sp<IImageStreamInfo> mImage_LcsoRaw;
    sp<IImageStreamInfo> mImage_Yuv;

    sp<StreamBufferProvider> mImgoProducer;
    sp<StreamBufferProvider> mRrzoProducer;
    sp<StreamBufferProvider> mLcsoProducer;
    sp<StreamBufferProvider> mYuvProducer;

    sp<BufferCallbackHandler> mCallbackHandler;

    sp<ImageCallback> mCallback;

    sp<MetadataListener> mMetaListener;

    // requestBuilder
    sp<RequestBuilder> mRequestBuilderP1;
    sp<RequestBuilder> mRequestBuilderPrv;

    sp<ResultProcessor> mResultProcessor;
    sp<TimestampProcessor> mTimestampProcessor;

    std::thread mStreamWorker;

    sem_t mSemStreamLoop;

    sem_t mSemStreamLoopDone;

    MINT32 mEnableDump;

    uint32_t mSensorScenario;

    mutable std::mutex mSensorStaticInfoLock;
    std::unique_ptr<NSCam::SensorStaticInfo> mSensorStaticInfo;

    std::atomic<bool> mExitRequest;

    // callback lists
    using RegisteredCallback =
        std::unordered_map<callback_descriptor_t,
        std::pair<callback_function_pointer_t, void*>>;
    mutable std::mutex mRegisteredCallbacksLock;
    RegisteredCallback mRegisteredCallbacks;
}; // class RgbPath

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _V2_0_RGBPATH_H_
