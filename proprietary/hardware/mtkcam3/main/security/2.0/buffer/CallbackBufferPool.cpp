/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "CallbackBufferPool"

#include "CallbackBufferPool.h"

#include <sys/prctl.h>

#include <mtkcam/utils/std/Format.h>
#include <mtkcam3/main/security/utils/Debug.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::security::V2_0;
using namespace NSCam::security::utils;
using namespace NSCam::Utils;

using NSCam::v3::IImageStreamInfo;

// ---------------------------------------------------------------------------

CallbackBufferPool::CallbackBufferPool(
        const sp<IImageStreamInfo>& streamInfo, SecType secType, bool allocBuffer)
    : mStreamInfo(streamInfo),
      mMaxBuffer(0),
      mMinBuffer(0),
      mNoNeedReturnBuffer(false),
      mForceNoNeedReturnBuffer(false),
      mSecType(secType)
{
    if (allocBuffer) allocateBuffer();
}

CallbackBufferPool::~CallbackBufferPool()
{
    AutoLog();

    if (mBufferAllocationWorker->joinable())
    {
        SEC_LOGD("wait for buffer allocation worker...");
        mBufferAllocationWorker->join();
        SEC_LOGD("wait for buffer allocation done");
    }

    std::lock_guard<std::mutex> _l(mBufferHeapLock);
    mAvailableBuf.clear();

    if (mInUseBuf.size())
    {
        auto iter = mInUseBuf.begin();
        while (iter != mInUseBuf.end())
        {
            SEC_LOGW("buffer(%" PRIXPTR ") not return to pool",
            reinterpret_cast<uintptr_t>((*iter).get()));
            iter++;
        }
    }
    mInUseBuf.clear();
}

MERROR CallbackBufferPool::acquireFromPool(
        const std::string& callerName, int32_t requestNo,
        sp<IImageBufferHeap>& bufferHeap,
        uint32_t& transform)
{
    AutoLog();

    SEC_LOGD("[%s] mAvailableBuf:%zu mInUseBuf:%zu",
            callerName.c_str(), mAvailableBuf.size(), mInUseBuf.size());

    std::unique_lock<std::mutex> _l(mBufferHeapLock);

    // wait for maxWaitTime for asynchronous buffer allocation
    if (mAvailableBuf.empty())
    {
        const std::chrono::duration<int64_t> maxWaitTime(std::chrono::seconds(1));
        SEC_LOGD("No available buffer heap, start waiting for %" PRId64 " second(s)",
                maxWaitTime.count());

        auto status = mBufferHeapCond.wait_for(_l, maxWaitTime);
        SEC_LOGW_IF(status == std::cv_status::timeout, "waiting timeout");

        if (mAvailableBuf.empty())
        {
            SEC_LOGE("available buffer heap is still empty");
            return TIMED_OUT;
        }
    }

    // find and return an available image buffer heap
    auto iter = mAvailableBuf.begin();
    mInUseBuf.push_back(*iter);
    bufferHeap = *iter;
    transform = mStreamInfo->getTransform();
    mAvailableBuf.erase(iter);

    SEC_LOGD("[%s] buffer heap acquired(%" PRIxPTR "): " \
            "heapAvailable(%zu) heapInUse(%zu)",
            callerName.c_str(), reinterpret_cast<uintptr_t>(bufferHeap.get()),
            mAvailableBuf.size(), mInUseBuf.size());

    sp<IBufferPool::Listener> listener = [this]
    {
        std::lock_guard<std::mutex> _l(mListenerLock);
        return mListener.promote();
    }();

    if (CC_LIKELY(listener.get()))
    {
        listener->onBufferAcquired(requestNo, mStreamInfo->getStreamId());
        return OK;
    }
    else
    {
       SEC_LOGE("listener does not exist");
       return UNKNOWN_ERROR;
    }
}

MERROR CallbackBufferPool::releaseToPool(
        const std::string& callerName, int32_t requestNo,
        const sp<IImageBufferHeap>& bufferHeap,
        uint64_t timestamp, bool invalidBuffer)
{
    (void) timestamp;

    AutoLog();

    sp<IBufferPool::Listener> listener = [this]
    {
        std::lock_guard<std::mutex> _l(mListenerLock);
        return mListener.promote();
    }();

    if (CC_LIKELY(listener.get()))
    {
        MERROR ret = OK;
        SEC_LOGD("Ready to onBufferReleased, streamid(%#" PRIx64 "), bufHeap:%p",
                mStreamInfo->getStreamId(), bufferHeap.get());

        listener->onBufferReleased(
                requestNo, mStreamInfo->getStreamId(), invalidBuffer, bufferHeap);

        if (mNoNeedReturnBuffer || mForceNoNeedReturnBuffer)
            ret = removeBufferFromPool(callerName, bufferHeap);
        else
            ret = returnBufferToPool(callerName, bufferHeap);

        return ret;
    }

    SEC_LOGD("there is no listener, return buffer heap(s) to pool directly");

    return returnBufferToPool(callerName, bufferHeap);
}

std::string CallbackBufferPool::poolName() const
{
    return LOG_TAG;
}

void CallbackBufferPool::dumpPool() const
{
    // TODO
}

MERROR CallbackBufferPool::allocateBuffer()
{
    AutoLog();

    if (CC_UNLIKELY(mStreamInfo == nullptr))
    {
        SEC_LOGE("invalid stream info");
        return NO_INIT;
    }

    mMaxBuffer = mStreamInfo->getMaxBufNum();
    mMinBuffer = mStreamInfo->getMinInitBufNum();

    if (CC_UNLIKELY(mMinBuffer > mMaxBuffer))
    {
        SEC_LOGE("minBuffer(%zu) is larger than maxBuffer(%zu)",
                mMinBuffer, mMaxBuffer);
        return UNKNOWN_ERROR;
    }

    // firstly, we allocate the minimum required buffers
    for (size_t i = 0; i < mMinBuffer; i++)
    {
        sp<IImageBufferHeap> bufferHeap = createImageBufferHeap();
        if (bufferHeap == nullptr)
        {
            SEC_LOGE("allocate buffer failed");
            continue;
        }

        std::lock_guard<std::mutex> _l(mBufferHeapLock);
        SEC_LOGD("add bufHeap(%" PRIXPTR ")",
                reinterpret_cast<uintptr_t>(bufferHeap.get()));
        mAvailableBuf.push_back(bufferHeap);
    }

    // then allocate the rest parts asynchronously if necessary
    {
        std::lock_guard<std::mutex> _l(mBufferHeapLock);
        if ((mAvailableBuf.size() + mInUseBuf.size()) < mMaxBuffer)
        {
            mBufferAllocationWorker = std::make_unique<std::thread>(
                    CallbackBufferPool::bufferAllocationWorker, std::ref(*this));
            if (CC_UNLIKELY(!mBufferAllocationWorker))
            {
                SEC_LOGE("create buffer allocation worker thread failed");
                return UNKNOWN_ERROR;
            }
        }
    }

    SEC_LOGD("(min,max):(%d,%d)", mMinBuffer, mMaxBuffer);

    return OK;
}

void CallbackBufferPool::setListener(const wp<IBufferPool::Listener>& listener)
{
    std::lock_guard<std::mutex> _l(mListenerLock);
    mListener = listener;
}

MERROR CallbackBufferPool::setMaxBufferCount(int bufferCount)
{
    AutoLog();

    MERROR ret = OK;

    if ((mStreamInfo == nullptr) || (bufferCount <= 0))
    {
        SEC_LOGE_IF(mStreamInfo == nullptr, "invalid imageStreamInfo");
        SEC_LOGE_IF(bufferCount == 0, "invalid buffer count");

        return UNKNOWN_ERROR;
    }

    SEC_LOGD("streamID %#" PRIx64 ", add %d buffer",
            mStreamInfo->getStreamId(), bufferCount);

    // allocate and add image buffer heaps to the available image buffer heap
    for (int i = 0; i < bufferCount; i++)
    {
        sp<IImageBufferHeap> bufferHeap = createImageBufferHeap();
        if (bufferHeap == nullptr)
        {
            SEC_LOGE("allocate buffer failed");
            continue;
        }

        std::lock_guard<std::mutex> _l(mBufferHeapLock);
        SEC_LOGD("streamID %#" PRIx64 ", add bufHeap: %p",
                mStreamInfo->getStreamId(), bufferHeap.get());
        mAvailableBuf.push_back(bufferHeap);
    }

    // update max buffer count
    mMaxBuffer = mMaxBuffer + bufferCount;
    mStreamInfo->setMaxBufNum(mMaxBuffer);

    return ret;
}

MERROR CallbackBufferPool::attachBuffer(const sp<IImageBuffer>& buffer)
{
    AutoLog();

    if (buffer == nullptr)
    {
        SEC_LOGE("invalid image buffer");
        return INVALID_OPERATION;
    }

    mNoNeedReturnBuffer = true;

    auto bufferHeap = buffer->getImageBufferHeap();
    SEC_LOGD("streamID %#" PRIx64", add bufHeap %p",
            mStreamInfo->getStreamId(), bufferHeap);

    std::lock_guard<std::mutex> _l(mBufferHeapLock);
    mAvailableBuf.push_back(bufferHeap);

    return OK;
}

MERROR CallbackBufferPool::returnBufferToPool(const std::string& callerName,
        const sp<IImageBufferHeap>& bufferHeap)
{
    (void) callerName;

    AutoLog();

    std::lock_guard<std::mutex> _l(mBufferHeapLock);

    auto iter = mInUseBuf.begin();
    while (iter != mInUseBuf.end())
    {
        // return the buffer heap ownership to the available buffer heap list
        if (*iter == bufferHeap)
        {
            SEC_LOGD("buffer heap returned(%" PRIxPTR "): " \
                    "heapAvailable(%zu) heapInUse(%zu)",
                    reinterpret_cast<uintptr_t>(bufferHeap.get()),
                    mAvailableBuf.size(), mInUseBuf.size());

            mAvailableBuf.push_back(*iter);
            mInUseBuf.erase(iter);

            mBufferHeapCond.notify_all();

            return OK;
        }
        iter++;
    }
    SEC_LOGE("cannot find buffer heap(%" PRIxPTR ")",
            reinterpret_cast<uintptr_t>(bufferHeap.get()));

    mBufferHeapCond.notify_one();

    return UNKNOWN_ERROR;
}

sp<IImageStreamInfo> CallbackBufferPool::getStreamInfo() const
{
    return mStreamInfo;
}

void CallbackBufferPool::flush()
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mBufferHeapLock);
    mAvailableBuf.clear();
    mInUseBuf.clear();
}

MERROR CallbackBufferPool::setForceNoNeedReturnBuffer(bool noNeedReturnBuffer)
{
    std::unique_lock<std::mutex> _l(mBufferHeapLock);

    SEC_LOGD("mAvailableBuf(%zu) mInUseBuf(%zu)",
            mAvailableBuf.size(), mInUseBuf.size());

    while (!mInUseBuf.empty())
    {
        const std::chrono::duration<int64_t> maxWaitTime(std::chrono::seconds(5));
        SEC_LOGD("in-use buffer heap list is not empty, " \
                "start waiting for %" PRId64 " second(s)", maxWaitTime.count());

        auto status = mBufferHeapCond.wait_for(_l, maxWaitTime);
        if (status == std::cv_status::timeout)
        {
            SEC_LOGE("buffer heap list is still in-use");
            return TIMED_OUT;
        }
    }

    SEC_LOGD("noNeedReturn (%d)->(%d)", mForceNoNeedReturnBuffer, noNeedReturnBuffer);

    mForceNoNeedReturnBuffer = noNeedReturnBuffer;

    return OK;
}

MERROR CallbackBufferPool::removeBufferFromPool(
        const std::string& callerName,
        const sp<IImageBufferHeap>& bufferHeap)
{
    (void) callerName;

    AutoLog();

    std::lock_guard<std::mutex> _l(mBufferHeapLock);

    auto iter = mInUseBuf.begin();
    while (iter != mInUseBuf.end())
    {
        if (*iter == bufferHeap)
        {
            mInUseBuf.erase(iter);

            SEC_LOGD("in-use buffer heap erased(%" PRIxPTR "): " \
                    "heapAvailable(%zu) heapInUse(%zu)",
                    reinterpret_cast<uintptr_t>(bufferHeap.get()),
                    mAvailableBuf.size(), mInUseBuf.size());

            return OK;
        }
        iter++;
    }

    iter = mAvailableBuf.begin();
    while (iter != mAvailableBuf.end())
    {
        if (*iter == bufferHeap)
        {
            mAvailableBuf.erase(iter);

            SEC_LOGD("available buffer heap erased(%" PRIxPTR "): " \
                    "heapAvailable(%zu) heapInUse(%zu)",
                    reinterpret_cast<uintptr_t>(bufferHeap.get()),
                    mAvailableBuf.size(), mInUseBuf.size());

            return OK;
        }
        iter++;
    }

    SEC_LOGE("cannot find buffer heap(%" PRIxPTR ")",
            reinterpret_cast<uintptr_t>(bufferHeap.get()));

    return UNKNOWN_ERROR;
}

sp<IImageBufferHeap> CallbackBufferPool::createImageBufferHeap()
{
    const auto& bufPlanes = mStreamInfo->getBufPlanes();
    size_t bufStridesInBytes[3] = {0};
    size_t bufBoundaryInBytes[3]= {0};
    size_t bufCustomSizeInBytes[3] = {0};

    for (size_t i = 0; i < bufPlanes.size(); i++)
    {
        bufStridesInBytes[i] = bufPlanes[i].rowStrideInBytes;
        bufCustomSizeInBytes[i] = bufPlanes[i].sizeInBytes;
    }

    const auto format(mStreamInfo->getImgFormat());
    const auto size(mStreamInfo->getImgSize());

    sp<IImageBufferHeap> bufferHeap;
    if (eImgFmt_BLOB == format)
    {
        IImageBufferAllocator::ImgParam imgParam(
                mStreamInfo->getImgSize(),
                *bufStridesInBytes, *bufBoundaryInBytes);
        imgParam.imgFormat = eImgFmt_BLOB;

        if (CC_UNLIKELY(mSecType == SecType::mem_normal))
        {
            bufferHeap = IIonImageBufferHeap::create(
                    mStreamInfo->getStreamName(),
                    imgParam,
                    IIonImageBufferHeap::AllocExtraParam());
        }
        else
        {
            bufferHeap = ISecureImageBufferHeap::create(
                    mStreamInfo->getStreamName(),
                    imgParam,
                    ISecureImageBufferHeap::AllocExtraParam(0, 1, 0, false, mSecType));
        }
    }
    else
    {
        bufferHeap = [&]() -> IImageBufferHeap*
        {
            auto getMultiplaneSize = [&] {
                size_t allPlaneSize = 0;
                // multi-plane
                for (size_t i = 0; i < bufPlanes.size(); i++) {
                    allPlaneSize +=
                        (Format::queryPlaneWidthInPixels(format, i, size.w) *
                         Format::queryPlaneBitsPerPixel(format, i) + 7) / 8 *
                         Format::queryPlaneHeightInPixels(format, i, size.h);
                }
                return allPlaneSize;
            };
            IImageBufferAllocator::ImgParam imgParam(
                    format, mStreamInfo->getImgSize(),
                    bufStridesInBytes, bufBoundaryInBytes,
                    bufCustomSizeInBytes, bufPlanes.size());
            IImageBufferAllocator::ImgParam multiplePlaneImgParam(getMultiplaneSize(), 0);
            const bool isMultiplePlane = (bufPlanes.size() > 1) ? true : false;
            const char* streamName(mStreamInfo->getStreamName());

            if (mSecType == SecType::mem_normal)
            {
                if (isMultiplePlane)
                {
                    // multi-plane
                    sp<IIonImageBufferHeap> blobHeap = IIonImageBufferHeap::create(
                            streamName, multiplePlaneImgParam);
                    return blobHeap->createImageBufferHeap_FromBlobHeap(
                            streamName, imgParam);
                }
                else
                {
                    // single-plane
                    return IIonImageBufferHeap::create(streamName, imgParam);
                }
            }
            else
            {
                const auto extParam = ISecureImageBufferHeap::AllocExtraParam(
                        0, 1, 0, isMultiplePlane ? true : false, mSecType);
                if (isMultiplePlane)
                {
                    // multi-plane
                    sp<ISecureImageBufferHeap> blobHeap = ISecureImageBufferHeap::create(
                            streamName, multiplePlaneImgParam, extParam);
                    return blobHeap->createImageBufferHeap_FromBlobHeap(
                            streamName, imgParam);
                }
                else
                {
                    // single-plane
                    return ISecureImageBufferHeap::create(
                            streamName, imgParam, extParam);
                }
            }
        }();
    }

    SEC_LOGD("createImageBufferHeap: format(%x) size(%d,%d) planes(%zu) " \
            "stride(%zu,%zu,%zu) boundary(%zu,%zu,%zu) customized size(%zu,%zu,%zu)",
            format, size.w, size.h, bufPlanes.size(),
            bufStridesInBytes[0], bufStridesInBytes[1],bufStridesInBytes[2],
            bufBoundaryInBytes[0], bufBoundaryInBytes[1],bufBoundaryInBytes[2],
            bufCustomSizeInBytes[0], bufCustomSizeInBytes[1], bufCustomSizeInBytes[2]);

    return bufferHeap;
}

void CallbackBufferPool::bufferAllocationWorker(CallbackBufferPool& self)
{
    AutoLog();

    // set name
    ::prctl(PR_SET_NAME, reinterpret_cast<unsigned long>(DEBUG_LOG_TAG), 0, 0, 0);

    if (CC_UNLIKELY((self.mAvailableBuf.size() + self.mInUseBuf.size()) >= self.mMaxBuffer))
    {
        SEC_LOGW("The sum of available and in-use buffer heaps exceeds " \
                "that of maximum buffer, do nothing");
        return;
    }

    bool tryAgain = false;

    do {
        sp<IImageBufferHeap> bufferHeap = self.createImageBufferHeap();
        if (bufferHeap == nullptr)
        {
            SEC_LOGE("allocate buffer failed");
            tryAgain = true;
        }

        {
            std::lock_guard<std::mutex> _l(self.mBufferHeapLock);
            self.mAvailableBuf.push_back(bufferHeap);
            self.mBufferHeapCond.notify_one();

            // try again if the allocated buffer heaps are
            // smaller than maximum buffer heaps
            tryAgain =
                (self.mAvailableBuf.size() + self.mInUseBuf.size()) < self.mMaxBuffer;
        }
    } while (tryAgain);
}
