/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _V2_0_BUFFER_BUFFERCALLBACKHANDLER_H_
#define _V2_0_BUFFER_BUFFERCALLBACKHANDLER_H_

#include <utils/Vector.h>

#include <mtkcam3/pipeline/stream/IStreamInfo.h>

#include "CallbackBufferPool.h"
#include "IImageCallback.h"

#include <mutex>

namespace NSCam {
namespace security {
namespace V2_0 {

// ------------------------------------------------------------------------

using NSCam::v3::IImageStreamInfo;
using NSCam::v3::StreamId_T;

// ------------------------------------------------------------------------

class BufferCallbackHandler final : public IBufferPool::Listener
{
public:
    BufferCallbackHandler(unsigned int sensorID);

    ~BufferCallbackHandler() = default;

    void setImageCallback(const android::wp<IImageCallback>& callback)
    {
        mCallback = callback;
    }

    android::sp<CallbackBufferPool> queryBufferPool(StreamId_T id)
    {
        return mvBufferPool.valueFor(id).pool;
    }

    void addBufferPool(const android::sp<CallbackBufferPool>& pool);

    void onLastStrongRef(const void* /*id*/);

private:
    const unsigned int kSensorId;

    struct Buffer_T
    {
        uint32_t   requestNo;
        StreamId_T streamId;
        bool       isReturned;
        bool       isError;
        android::sp<IImageBuffer> buffer;
    };

    struct Info_T
    {
        StreamId_T streamId;
        android::sp<CallbackBufferPool> pool;
    };

    android::KeyedVector<StreamId_T, Info_T> mvBufferPool;
    android::wp<IImageCallback> mCallback;

    mutable std::mutex mRequestOrderMapLock;
    android::KeyedVector<uint32_t, android::Vector<Buffer_T>> mRequestOrderMap;

    // interface of NSCam::security::v2_0::IBufferPool::Listener
    void onBufferAcquired(
            const uint32_t requestNo, const StreamId_T streamId) override;

    bool onBufferReleased(
            const uint32_t requestNo, const StreamId_T streamId,
            const bool errorBuffer,
            const android::sp<NSCam::IImageBufferHeap>& bufferHeap) override;
}; // class BufferCallbackHandler

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _V2_0_BUFFER_BUFFERCALLBACKHANDLER_H_
