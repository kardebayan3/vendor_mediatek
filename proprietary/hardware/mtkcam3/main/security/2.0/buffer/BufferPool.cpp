/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "BufferPool"

#include "BufferPool.h"

#include <cutils/properties.h>

#include <sys/prctl.h>

#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>

#include <mtkcam3/main/security/utils/Debug.h>

using namespace NSCam;
using namespace NSCam::security::V2_0;
using namespace NSCam::security::utils;

// ---------------------------------------------------------------------------

BufferPool::BufferPool(const sp<IImageStreamInfo>& streamInfo, bool isSecurity)
    : mStreamInfo(streamInfo),
      mMaxBuffer(0),
      mExit(false),
      mSecurity(isSecurity)
{
    mLogLevel = ::property_get_int32("vendor.debug.camera.log", 0);
    if (mLogLevel == 0)
        mLogLevel = ::property_get_int32("vendor.debug.camera.bufferpool", 0);
}

MERROR BufferPool::acquireFromPool(
        const std::string& callerName, int32_t requestNo,
        android::sp<IImageBufferHeap>& bufferHeap,
        uint32_t& transform)
{
    AutoLog();

    SEC_LOGD_IF(mLogLevel >= 1, "[%d] %s", requestNo, callerName.c_str());

    {
        std::unique_lock<std::mutex> _l(mAvailLock, std::defer_lock);
        std::unique_lock<std::mutex> _m(mInUseLock, std::defer_lock);
        std::lock(_l, _m);

        SEC_LOGD_IF(mLogLevel >= 1, "heapAvailable(%zu) heapInUse(%zu)",
                mAvailableBuf.size(), mInUseBuf.size());

        // find and return an available image buffer heap
        if (!mAvailableBuf.empty())
        {
            auto iter = mAvailableBuf.begin();
            mInUseBuf.push_back(*iter);
            bufferHeap = *iter;
            transform = mStreamInfo->getTransform();
            mAvailableBuf.erase(iter);

            SEC_LOGD("buffer heap acquired(%" PRIxPTR "): " \
                    "heapAvailable(%zu) heapInUse(%zu)",
                    bufferHeap.get(), mAvailableBuf.size(), mInUseBuf.size());

            return OK;
        }

        return TIMED_OUT;
    }
}

MERROR BufferPool::releaseToPool(
        const std::string& callerName, int32_t requestNo,
        const android::sp<IImageBufferHeap>& bufferHeap,
        uint64_t timestamp, bool invalidBuffer)
{
    (void) timestamp;
    (void) invalidBuffer;

    AutoLog();

    SEC_LOGD("[%d] %s", requestNo, callerName.c_str());

    std::unique_lock<std::mutex> _l(mAvailLock, std::defer_lock);
    std::unique_lock<std::mutex> _m(mInUseLock, std::defer_lock);
    std::lock(_l, _m);

    if (mInUseBuf.empty())
    {
        SEC_LOGE("no buffer heap in use: heapAvailable(%zu)",
                mAvailableBuf.size());
        return INVALID_OPERATION;
    }

    auto iter = mInUseBuf.begin();
    while (iter != mInUseBuf.end())
    {
        // return the buffer heap ownership to the available buffer heap list
        if (*iter == bufferHeap)
        {
            SEC_LOGD("buffer heap returned(%" PRIxPTR "): " \
                    "heapAvailable(%zu) heapInUse(%zu)",
                    bufferHeap.get(), mAvailableBuf.size(), mInUseBuf.size());

            mAvailableBuf.push_back(*iter);
            mInUseBuf.erase(iter);

            mAvailCond.notify_one();

            return OK;
        }
        iter++;
    }
    SEC_LOGE("cannot find buffer heap(%" PRIxPTR ")", bufferHeap.get());

    return UNKNOWN_ERROR;
}

void BufferPool::setListener(const wp<IBufferPool::Listener>& listener)
{
    (void) listener;
    SEC_LOGW("invalid operation, do nothing");
}


std::string BufferPool::poolName() const
{
    return (mStreamInfo == 0) ? mStreamInfo->getStreamName() : DEBUG_LOG_TAG;
}

void BufferPool::dumpPool() const
{
    SEC_LOGI(
        "[%#" PRIx64 " %s] stream:%dx%d format:%#x transform:%d",
        mStreamInfo->getStreamId(), mStreamInfo->getStreamName(),
        mStreamInfo->getImgSize().w, mStreamInfo->getImgSize().h,
        mStreamInfo->getImgFormat(), mStreamInfo->getTransform());

    std::unique_lock<std::mutex> _l(mAvailLock, std::defer_lock);
    std::unique_lock<std::mutex> _m(mInUseLock, std::defer_lock);
    std::lock(_l, _m);

    SEC_LOGI("logLevel(%d) Max(%zu) Min(%zu) future(%zu) inUse(%zu) available(%zu)",
            mLogLevel, mMaxBuffer, mMinBuffer, mvFutures.size(),
            mInUseBuf.size(), mAvailableBuf.size());

    {
        String8 str = String8::format("Available Buffer: ");
        typename List< sp<IImageBufferHeap> >::const_iterator iter = mAvailableBuf.begin();
        while( iter != mAvailableBuf.end() ) {
            str += String8::format("%p", (*iter).get());
            iter++;
        }
        SEC_LOGI("%s", str.string());
    }
    //
    {
        String8 str = String8::format("Inuse Buffer: ");
        typename List< sp<IImageBufferHeap> >::const_iterator iter = mInUseBuf.begin();
        while( iter != mInUseBuf.end() ) {
            str += String8::format("%p", (*iter).get());
            iter++;
        }
        SEC_LOGI("%s", str.string());
    }
}

MERROR BufferPool::updateBufferCount(
        const std::string& callerName, size_t maxBufferCount)
{
    (void) callerName;

    MERROR result = OK;
    for (auto &fut : mvFutures)
        result = fut.get();
    mvFutures.clear();

    mvFutures.push_back(
        std::async(std::launch::async,
            [this](auto maxBufferCount) {
                ::prctl(PR_SET_NAME,
                    reinterpret_cast<unsigned long>("updateBufferCount"), 0, 0, 0);

                MERROR err = OK;
                while ((mMaxBuffer != maxBufferCount) && !mExit)
                {
                    if (mMaxBuffer < maxBufferCount)
                    {
                        // increase buffer
                        sp<IImageBufferHeap> bufferHeap;
                        if (CC_UNLIKELY(do_construct(bufferHeap) == NO_MEMORY))
                        {
                            SEC_LOGE("allocate buffer failed");
                            continue;
                        }

                        std::lock_guard<std::mutex> _l(mAvailLock);
                        mAvailableBuf.push_back(bufferHeap);
                        mMaxBuffer++;
                    }
                    else if (mMaxBuffer > maxBufferCount)
                    {
                        // reduce buffer
                        std::unique_lock<std::mutex> _l(mAvailLock);
                        mAvailCond.wait(_l, [this]
                                { return !mAvailableBuf.empty(); });

                        if (!mAvailableBuf.empty())
                        {
                            mAvailableBuf.erase(mAvailableBuf.begin());
                            mMaxBuffer--;
                        }
                    }
                }

                return err;
            }, maxBufferCount)
    );

    return OK;
}

MERROR
BufferPool::
allocateBuffer(
    char const* szCallerName,
    size_t maxNumberOfBuffers,
    size_t minNumberOfInitialCommittedBuffers
)
{
    if ( mStreamInfo == 0 ) {
        SEC_LOGE("No ImageStreamInfo.");
        return UNKNOWN_ERROR;
    }

    if ( minNumberOfInitialCommittedBuffers > maxNumberOfBuffers) {
        SEC_LOGE("mMinBuffer(%zu) > mMaxBuffer(%zu)",
                minNumberOfInitialCommittedBuffers, maxNumberOfBuffers);
        return UNKNOWN_ERROR;
    }

    mMaxBuffer = minNumberOfInitialCommittedBuffers;
    mMinBuffer = minNumberOfInitialCommittedBuffers;

    for (size_t i = 0; i < mMinBuffer; i++) {
        sp<IImageBufferHeap> pHeap;
        if( do_construct(pHeap) == NO_MEMORY ) {
            SEC_LOGE("do_construct allocate buffer failed");
            continue;
        }
        {
            std::lock_guard<std::mutex> _l(mAvailLock);
            mAvailableBuf.push_back(pHeap);
        }
    }
    return updateBufferCount(szCallerName, maxNumberOfBuffers);
}

void
BufferPool::
onLastStrongRef(const void* /*id*/)
{
    mExit = true;
    mAvailCond.notify_one();
    //
    MERROR result = OK;
    for( auto &fut : mvFutures ) {
        result = fut.get();
    }
    mvFutures.clear();
    //
    {
        std::lock_guard<std::mutex> _l(mAvailLock);
        mAvailableBuf.clear();
    }

    {
        std::lock_guard<std::mutex> _l(mInUseLock);
        if ( mInUseBuf.size() > 0 ) {
            auto iter = mInUseBuf.begin();
            while( iter != mInUseBuf.end() ) {
                SEC_LOGW("[%s] buffer %p not return to pool.",
                        poolName().c_str(), (*iter).get());
                iter++;
            }
        }

        mInUseBuf.clear();
    }
}

MERROR
BufferPool::
do_construct(
    sp<IImageBufferHeap>& pImageBufferHeap
)
{
    IImageStreamInfo::BufPlanes_t const& bufPlanes = mStreamInfo->getBufPlanes();
    size_t bufStridesInBytes[3] = {0};
    size_t bufBoundaryInBytes[3]= {0};
    for (size_t i = 0; i < bufPlanes.size(); i++) {
        bufStridesInBytes[i] = bufPlanes[i].rowStrideInBytes;
    }

    if ( eImgFmt_JPEG == mStreamInfo->getImgFormat() ||
         eImgFmt_BLOB == mStreamInfo->getImgFormat() )
    {
        IImageBufferAllocator::ImgParam imgParam(
                mStreamInfo->getImgSize(),
                (*bufStridesInBytes),
                (*bufBoundaryInBytes));
        imgParam.imgFormat = eImgFmt_BLOB;
        SEC_LOGD("eImgFmt_JPEG -> eImgFmt_BLOB");
        if (!mSecurity) {
            pImageBufferHeap = IIonImageBufferHeap::create(
                                mStreamInfo->getStreamName(),
                                imgParam,
                                IIonImageBufferHeap::AllocExtraParam(),
                                MFALSE
                            );
        } else {
            pImageBufferHeap = ISecureImageBufferHeap::create(
                                mStreamInfo->getStreamName(),
                                imgParam,
                                ISecureImageBufferHeap::AllocExtraParam(0, 1, 0),
                                MFALSE
                            );
        }
    }
    else
    {
        IImageBufferAllocator::ImgParam imgParam(
            mStreamInfo->getImgFormat(),
            mStreamInfo->getImgSize(),
            bufStridesInBytes, bufBoundaryInBytes,
            bufPlanes.size()
            );
        SEC_LOGD("format:%x, size:(%d,%d), stride:%zu, boundary:%zu, planes:%zu",
            mStreamInfo->getImgFormat(),
            mStreamInfo->getImgSize().w, mStreamInfo->getImgSize().h,
            bufStridesInBytes[0], bufBoundaryInBytes[0], bufPlanes.size()
            );
            if (!mSecurity) {
                pImageBufferHeap = IIonImageBufferHeap::create(
                        mStreamInfo->getStreamName(),
                        imgParam,
                        IIonImageBufferHeap::AllocExtraParam(),
                        MFALSE
                        );
            } else {
                pImageBufferHeap = ISecureImageBufferHeap::create(
                        mStreamInfo->getStreamName(),
                        imgParam,
                        ISecureImageBufferHeap::AllocExtraParam(0, 1, 0),
                        MFALSE
                        );
            }
    }

    return OK;
}
