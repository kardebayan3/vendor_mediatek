/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "StreamBufferProvider"

#include "StreamBufferProvider.h"
#include "BufferPool.h"

#include <cassert>
#include <mtkcam3/main/security/utils/Debug.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::security::V2_0;
using namespace NSCam::security::utils;
using NSCam::v3::Utils::HalImageStreamBufferProvider;

// ---------------------------------------------------------------------------

StreamBufferProvider::StreamBufferProvider(bool needTimestamp)
    : mStreamName(DEBUG_LOG_TAG),
      kNeedTimestamp(needTimestamp)
{
}

MERROR StreamBufferProvider::dequeStreamBuffer(
        const uint32_t requestNo,
        const android::sp<IImageStreamInfo> streamInfo,
        android::sp<HalImageStreamBuffer>& streamBuffer)
{
    if (CC_UNLIKELY(mBufferPool == nullptr))
    {
        SEC_LOGE("buffer pool does not exist");
        return UNKNOWN_ERROR;
    }

    uint32_t transform = 0;
    sp<IImageBufferHeap> bufferHeap;
    {
        sp<IBufferPool> bufferPool = [this]
        {
            std::lock_guard<std::mutex> _l(mBufferPoolLock);
            return mBufferPool;
        }();

        if (bufferPool->acquireFromPool(
                    DEBUG_LOG_TAG, requestNo, bufferHeap, transform) != OK)
        {
            SEC_LOGW("Cannot acquire from pool. Something wrong...");
            return NO_MEMORY;
        }
    }
    streamInfo->setTransform(transform);

    {
        std::lock_guard<std::mutex> _l(mImageStreamInfoLock);
        streamBuffer = new HalImageStreamBufferProvider(
                mImageStreamInfo, bufferHeap, this);
    }
    if (CC_UNLIKELY(streamBuffer == nullptr))
    {
        SEC_LOGE("Cannot new HalImageStreamBuffer. Something wrong...");
        return NO_MEMORY;
    }

    SEC_LOGD("[%u] streamBuffer(%" PRIxPTR ") bufferHeap(%" PRIxPTR ")",
            requestNo,
            reinterpret_cast<uintptr_t>(streamBuffer.get()),
            reinterpret_cast<uintptr_t>(bufferHeap.get()));

    {
        std::lock_guard<std::mutex> _l(mBufferMapLock);
        mBufferMap.emplace(streamBuffer.get(), Buffer{requestNo, bufferHeap});
    }

    return OK;
}

MERROR StreamBufferProvider::enqueStreamBuffer(
        const android::sp<IImageStreamInfo> streamInfo,
        android::sp<HalImageStreamBuffer> streamBuffer,
        uint32_t bufferStatus)
{
    (void) streamInfo;

    if (CC_UNLIKELY((mBufferPool == nullptr) || (streamBuffer == nullptr)))
    {
        SEC_LOGE_IF(mBufferPool == nullptr, "buffer pool does not exist");
        SEC_LOGE_IF(streamBuffer == nullptr, "invalid HAL image stream buffer");
        return UNKNOWN_ERROR;
    }

    Buffer buffer = [this, &streamBuffer]
    {
        std::lock_guard<std::mutex> _l(mBufferMapLock);
        auto search = mBufferMap.find(streamBuffer.get());
        if (search != mBufferMap.end())
        {
            mBufferMap.erase(streamBuffer.get());
            return search->second;
        }
        return Buffer();
    }();

    SEC_LOGD("requestNo(%u) bufferHeap(%" PRIxPTR ") bufferStatus(%d)",
            buffer.requestNo, reinterpret_cast<uintptr_t>(buffer.heap.get()),
            bufferStatus);
    {
        std::vector<ResultSet> tempSetMap
        {{ buffer.requestNo, buffer.heap,
             (bufferStatus & STREAM_BUFFER_STATUS::ERROR) != 0, -1 }};
        handleResult(tempSetMap);
    }

    return OK;
}

void StreamBufferProvider::handleResult(const std::vector<ResultSet>& resultSet)
{
    std::lock_guard<std::mutex> _l(mResultSetLock);

    for (auto&& result : resultSet)
    {
        auto search = mResultSetMap.find(result.requestNo);
        if (search == mResultSetMap.end())
        {
            SEC_LOGV("ADD: requestNo(%u) heap(%" PRIxPTR ")",
                    result.requestNo,
                    reinterpret_cast<uintptr_t>(result.heap.get()));

            mResultSetMap.emplace(result.requestNo, result);
        }
        else
        {
            SEC_LOGV("EDIT: requestNo(%u) heap(%" PRIxPTR ")",
                    result.requestNo,
                    reinterpret_cast<uintptr_t>(result.heap.get()));

            ResultSet& resultSet(search->second);
            resultSet.error |= result.error;
            if (result.heap.get())
                resultSet.heap = result.heap;
            if (result.timestamp >= 0)
                resultSet.timestamp = result.timestamp;
        }
    }

    handleReturn();
}

status_t StreamBufferProvider::handleReturn()
{
    for (auto iter = mResultSetMap.begin(); iter != mResultSetMap.end();)
    {
        auto& resultSet(iter->second);

        // skip check if the buffer heap does not exist
        if (resultSet.heap == nullptr)
        {
            ++iter;
            continue;
        }

        if ((resultSet.timestamp >= 0) || !kNeedTimestamp)
        {
            SEC_LOGW_IF((resultSet.timestamp == 0) && kNeedTimestamp,
                    "No timestamp information in result metadata.");
            {
                SEC_LOGD("release requestNo(%u) heap(%" PRIxPTR ") " \
                        "timestamp(%" PRId64 ") needTimestamp(%d) err(%d)",
                        resultSet.requestNo,
                        reinterpret_cast<uintptr_t>(resultSet.heap.get()),
                        resultSet.timestamp,
                        kNeedTimestamp,
                        resultSet.error);

                std::lock_guard<std::mutex> _l(mBufferPoolLock);
                mBufferPool->releaseToPool(
                        mStreamName, resultSet.requestNo, resultSet.heap,
                        resultSet.timestamp, resultSet.error);
            }

            iter = mResultSetMap.erase(iter);
        }
    }

    // remove invalid result set
    auto frameEnd = [this]
    {
        std::lock_guard<std::mutex> _l(mFrameEndLock);
        std::vector<uint32_t> __frameEnd(std::move(mFrameEnd));
        assert(mFrameEnd.empty());
        return __frameEnd;
    }();

    for (auto&& requestNo : frameEnd)
        mResultSetMap.erase(requestNo);

    return OK;
}

status_t StreamBufferProvider::updateBufferCount(
        const std::string& callerName, size_t maxBufferCount)
{
    {
        std::lock_guard<std::mutex> _l(mImageStreamInfoLock);
        mImageStreamInfo->setMaxBufNum(maxBufferCount);
    }

    std::lock_guard<std::mutex> _l(mBufferPoolLock);
    return (mBufferPool.get()) ? mBufferPool->updateBufferCount(
            callerName, maxBufferCount) : INVALID_OPERATION;
}

status_t StreamBufferProvider::setImageStreamInfo(
        const android::sp<IImageStreamInfo>& streamInfo)
{
    std::lock_guard<std::mutex> _l(mImageStreamInfoLock);
    mImageStreamInfo = streamInfo;
    mStreamName = mImageStreamInfo->getStreamName();
    return OK;
}

sp<IImageStreamInfo> StreamBufferProvider::queryImageStreamInfo()
{
    std::lock_guard<std::mutex> _l(mImageStreamInfoLock);
    return mImageStreamInfo;
}

status_t StreamBufferProvider::setBufferPool(
        const android::sp<IBufferPool>& bufferPool)
{
    std::lock_guard<std::mutex> _l(mBufferPoolLock);
    mBufferPool = bufferPool;

    return OK;
}

void StreamBufferProvider::onLastStrongRef(const void* /*id*/)
{
    AutoLog();

    auto ResultSetMapPrinter = [](std::unordered_map<uint32_t, ResultSet>& resultSetMap,
                const char* prefix = "")
    {
        for (auto&& resultSet : resultSetMap)
        {
            auto& result(resultSet.second);
            CAM_LOGW("%s: requestNo (%u) heap(%" PRIxPTR ") timeStamp(%" PRId64 ") error(%d)",
                    prefix,
                    result.requestNo,
                    reinterpret_cast<uintptr_t>(result.heap.get()),
                    result.timestamp,
                    result.error);
        }
    };

    ResultSetMapPrinter(mResultSetMap,
            String8::format("[%s] %s" , mStreamName.c_str(), "not return to pool").c_str());
}

void StreamBufferProvider::flush()
{
    std::lock_guard<std::mutex> _l(mResultSetLock);
    for (auto&& resultSet : mResultSetMap)
    {
        auto& result(resultSet.second);
        if (result.heap.get())
        {
            std::lock_guard<std::mutex> _l(mBufferPoolLock);
            mBufferPool->releaseToPool(
                    mStreamName, result.requestNo, result.heap,
                    result.timestamp, result.error);
        }
    }
    mResultSetMap.clear();
}

void StreamBufferProvider::doTimestampCallback(
        const uint32_t requestNo, const bool errorResult, const int64_t timestamp)
{
    SEC_LOGD("reqNo(%u) err(%d) timestamp(%" PRId64 ")",
            requestNo, errorResult, timestamp);

    std::vector<ResultSet> tempSetMap
    {{ requestNo, nullptr, errorResult, timestamp }};
    handleResult(tempSetMap);
}

void StreamBufferProvider::onFrameEnd(uint32_t requestNo)
{
    std::lock_guard<std::mutex> _l(mFrameEndLock);
    mFrameEnd.push_back(requestNo);
}

// ---------------------------------------------------------------------------

sp<StreamBufferProvider> StreamBufferProviderBuilder::create(
        const sp<IImageStreamInfo>& streamInfo,
        const sp<IBufferPool>& pool, bool needTimeStamp)
{
    sp<StreamBufferProvider> provider = new StreamBufferProvider(needTimeStamp);
    provider->setImageStreamInfo(streamInfo);

    if (pool.get())
    {
        provider->setBufferPool(pool);
    }
    else
    {
        sp<BufferPool> localPool = new BufferPool(streamInfo);
        localPool->allocateBuffer(streamInfo->getStreamName(),
                streamInfo->getMaxBufNum(), streamInfo->getMinInitBufNum());
        provider->setBufferPool(localPool);
    }

    return provider;
}
