LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)

-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := frhidl_test
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam

LOCAL_SRC_FILES := main_frhidl_test.cpp

LOCAL_CFLAGS := -DLOG_TAG=\"frhandler_test\"

LOCAL_C_INCLUDES := \
    $(MTK_PATH_CAM)/include

LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/hardware/mtkcam/include
LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/hardware/mtkcam3/include

LOCAL_STATIC_LIBRARIES := \
	android.hardware.camera.common@1.0-helper \
	libmtkcam_ionhelper

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libmtkcam_stdutils \
	libhidlbase \
	android.hardware.camera.common@1.0 \
	android.hardware.graphics.mapper@2.0 
# ori: vendor.mediatek.hardware.camera.frhandler@1.0_vendor

LOCAL_SHARED_LIBRARIES += vendor.mediatek.hardware.camera.frhandler@1.0


# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
LOCAL_SHARED_LIBRARIES += \
	libion \
	libion_mtk
endif

LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/hardware/mtkcam/main/hal/frhandler
LOCAL_MODULE_TAGS := debug eng tests

include $(BUILD_NATIVE_TEST)

endif # MTK_CAM_SECURITY_SUPPORT

