#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_DUMMY_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_DUMMY_H

#include <utils/RefBase.h> // android::RefBase

#include "FRAlgoCA.h"

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {

class FRAlgoCA_dummy: public FRAlgoCA {

private:
    // FR-custom
    struct SecureCamClientCallback : virtual public ISecureCameraClientCallback
    {
        friend FRAlgoCA_dummy;
        SecureCamClientCallback(const wp<FRAlgoCA_dummy>& frAlgoCA_dummy, uint64_t cookie=0) : mwpFRAlgoCA_dummy(frAlgoCA_dummy), mKCookie(cookie) {}
        // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
        virtual Return<void> onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream) override;

        wp<FRAlgoCA_dummy> mwpFRAlgoCA_dummy;
        // 
        const uint64_t mKCookie;
    };

public:
        // FR-custom: start
        FRAlgoCA_dummy(const char *pCaller, const FRCFG_ENUM frConfig);
        virtual ~FRAlgoCA_dummy();
        virtual int init();
        virtual int uninit();
        virtual void reset();
        virtual ISecureCameraClientCallback* getSecureCameraClientCallback();
        virtual int fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream);
        // FR-custom: end
    
private:
        
protected:

};


}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware

#endif // VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_DUMMY_H




