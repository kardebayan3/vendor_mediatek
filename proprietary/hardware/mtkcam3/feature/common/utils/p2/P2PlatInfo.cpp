/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <mtkcam3/feature/utils/p2/P2PlatInfo.h>

#include <DpDataType.h>
#include <DpIspStream.h>
#include <mtkcam/aaa/INvBufUtil.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <camera_custom_nvram.h>
#if MTK_CAM_NEW_NVRAM_SUPPORT
#include <mtkcam/utils/mapping_mgr/cam_idx_mgr.h>
#endif // MTK_CAM_NEW_NVRAM_SUPPORT
#include <isp_tuning/isp_tuning.h>

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "MtkCam/P2PlatInfo"
#include <mtkcam/utils/std/Log.h>

#ifdef MY_LOGD
#undef MY_LOGD
#endif
#ifdef MY_LOGE
#undef MY_LOGE
#endif

#define MY_LOGD(s,...) CAM_LOGD("[%s]" s, __FUNCTION__, ##__VA_ARGS__)
#define MY_LOGE(s,...) CAM_LOGE("[%s]" s, __FUNCTION__, ##__VA_ARGS__)

using NSCam::NSIoPipe::EDIPHWVersion_40;
using NSCam::NSIoPipe::EDIPHWVersion_50;
using NSCam::NSIoPipe::EDIPHWVersion_60;
using NSCam::NSIoPipe::EDIPINFO_DIPVERSION;
using NSCam::NSIoPipe::NSPostProc::INormalStream;
using NSCam::NSIoPipe::PortID;

using NSCam::NSIoPipe::PORT_IMGI;
using NSCam::NSIoPipe::PORT_IMGBI;
using NSCam::NSIoPipe::PORT_IMGCI;
using NSCam::NSIoPipe::PORT_VIPI;
using NSCam::NSIoPipe::PORT_DEPI;
using NSCam::NSIoPipe::PORT_LCEI;
using NSCam::NSIoPipe::PORT_DMGI;
using NSCam::NSIoPipe::PORT_BPCI;
using NSCam::NSIoPipe::PORT_LSCI;
using NSCam::NSIoPipe::PORT_YNR_FACEI;
using NSCam::NSIoPipe::PORT_YNR_LCEI;
using NSCam::NSIoPipe::PORT_UNKNOWN;

using NSImageio::NSIspio::EPortIndex_UNKNOW;
using NSCam::NSIoPipe::EPortType_Memory;
using NSCam::NSIoPipe::EPortCapbility_None;

namespace NSCam {
namespace Feature {
namespace P2Util {

template<typename T>
MBOOL tryGet(const IMetadata &meta, MUINT32 tag, T &val)
{
    MBOOL ret = MFALSE;
    IMetadata::IEntry entry = meta.entryFor(tag);
    if( !entry.isEmpty() )
    {
        val = entry.itemAt(0, Type2Type<T>());
        ret = MTRUE;
    }
    return ret;
}

template<typename T>
MBOOL tryGet(const IMetadata *meta, MUINT32 tag, T &val)
{
    return (meta != NULL) ? tryGet<T>(*meta, tag, val) : MFALSE;
}

template<typename T>
MBOOL trySet(IMetadata &meta, MUINT32 tag, const T &val)
{
    MBOOL ret = MFALSE;
    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    ret = (meta.update(tag, entry) == android::OK);
    return ret;
}

template<typename T>
MBOOL trySet(IMetadata *meta, MUINT32 tag, const T &val)
{
    return (meta != NULL) ? trySet<T>(*meta, tag, val) : MFALSE;
}

class P2PlatInfoImp : public P2PlatInfo
{
public:
    P2PlatInfoImp(MUINT32 sensorID);
    ~P2PlatInfoImp();
    virtual MBOOL isDip50() const;
    virtual MBOOL supportClearZoom() const;
    virtual MBOOL supportDRE() const;
    virtual MRect getActiveArrayRect() const;
    virtual void* queryNVRamData(FeatureID fID, MINT32 magic3A, uint32_t &customIndex) const;

    virtual PortID getLsc2Port() const;
    virtual PortID getBpc2Port() const;
    virtual PortID getYnrFaceiPort() const;
    virtual PortID getYnrLceiPort() const;
    virtual MBOOL hasYnvFacePort() const;
    virtual MBOOL hasYnvLceiPort() const;

private:
    MVOID initSensorDev();
    MVOID initActiveArrayRect();
    MVOID initNVRam();
    MVOID initIdxMgr();
    MVOID initTuningPort();

private:

    MUINT32 mSensorID = -1;

    std::map<NSIoPipe::EDIPInfoEnum, MUINT32> mDipInfo;
    MUINT32 mDipVersion = NSIoPipe::EDIPHWVersion_40;

    std::map<DP_ISP_FEATURE_ENUM, bool> mIspFeature;
    MBOOL mSupportClearZoom = MFALSE;
    MBOOL mSupportDRE = MFALSE;

    NSCam::IHalSensorList *mHalSensorList = NULL;
    ESensorDev_T mSensorDev;

    MRect mActiveArrayRect;
    NVRAM_CAMERA_FEATURE_STRUCT *mpNVRam = NULL;
#if MTK_CAM_NEW_NVRAM_SUPPORT
    IdxMgr *mIdxMgr = NULL;
#endif// MTK_CAM_NEW_NVRAM_SUPPORT

    PortID mLsc2Port = PORT_UNKNOWN;
    PortID mBpc2Port = PORT_UNKNOWN;
    PortID mYnrFaceIPort = PORT_UNKNOWN;
    PortID mYnrLceiPort = PORT_UNKNOWN;
};

P2PlatInfoImp::P2PlatInfoImp(MUINT32 sensorID)
{
    mSensorID = sensorID;
    mDipInfo[EDIPINFO_DIPVERSION] = EDIPHWVersion_40;
    if( !INormalStream::queryDIPInfo(mDipInfo) )
    {
        MY_LOGE("queryDIPInfo fail!");
    }
    mDipVersion = mDipInfo[EDIPINFO_DIPVERSION];
    DpIspStream::queryISPFeatureSupport(mIspFeature);
    mSupportClearZoom = mIspFeature[ISP_FEATURE_CLEARZOOM];
    mSupportDRE = mIspFeature[ISP_FEATURE_DRE];
    initSensorDev();
    initActiveArrayRect();
    initNVRam();
    initIdxMgr();
    initTuningPort();
}

P2PlatInfoImp::~P2PlatInfoImp()
{
}

MBOOL P2PlatInfoImp::isDip50() const
{
    return mDipVersion == EDIPHWVersion_50;
}

MBOOL P2PlatInfoImp::supportClearZoom() const
{
    return mSupportClearZoom;
}

MBOOL P2PlatInfoImp::supportDRE() const
{
    return mSupportDRE;
}

MRect P2PlatInfoImp::getActiveArrayRect() const
{
    return mActiveArrayRect;
}

PortID P2PlatInfoImp::getLsc2Port() const
{
    return mLsc2Port;
}

PortID P2PlatInfoImp::getBpc2Port() const
{
    return mBpc2Port;
}

PortID P2PlatInfoImp::getYnrFaceiPort() const
{
    return mYnrFaceIPort;
}

PortID P2PlatInfoImp::getYnrLceiPort() const
{
    return mYnrLceiPort;
}

MBOOL P2PlatInfoImp::hasYnvFacePort() const
{
    return mYnrFaceIPort.index != EPortIndex_UNKNOW;
}
MBOOL P2PlatInfoImp::hasYnvLceiPort() const
{
    return mYnrLceiPort.index != EPortIndex_UNKNOW;
}


void* P2PlatInfoImp::queryNVRamData(FeatureID fID, MINT32 magic3A, uint32_t &customIndex) const
{
    (void)fID;
    (void)magic3A;
    void *data = NULL;

    customIndex = 0;
#if MTK_CAM_NEW_NVRAM_SUPPORT
    if( mIdxMgr && mpNVRam )
    {
        CAM_IDX_QRY_COMB rMappingInfo;
        mIdxMgr->getMappingInfo(mSensorDev, rMappingInfo, magic3A);
        if( fID == FID_CLEARZOOM )
        {
            MUINT32 idx = mIdxMgr->query(mSensorDev, NSIspTuning::EModule_ClearZoom, rMappingInfo, __FUNCTION__);
            data = &(mpNVRam->ClearZoom[idx]);
        }
        else if( fID == FID_DRE )
        {
            customIndex = mIdxMgr->query(mSensorDev, NSIspTuning::EModule_CA_LTM, rMappingInfo, __FUNCTION__);
            data = &(mpNVRam->CA_LTM[customIndex]);
        }
        else
        {
            MY_LOGE("unknown featureID=%d magic3A=%d", fID, magic3A);
        }
    }
#endif // MTK_CAM_NEW_NVRAM_SUPPORT

    return data;
}

MVOID P2PlatInfoImp::initSensorDev()
{
    mHalSensorList = MAKE_HalSensorList();
    mSensorDev = (ESensorDev_T)mHalSensorList->querySensorDevIdx(mSensorID);
}

MVOID P2PlatInfoImp::initActiveArrayRect()
{
    mActiveArrayRect = MRect(1600,1200);
    android::sp<IMetadataProvider> metaProvider = NSCam::NSMetadataProviderManager::valueFor(mSensorID);

    if( metaProvider == NULL )
    {
        MY_LOGE("get NSMetadataProvider failed, use (1600,1200)");
        return;
    }

    IMetadata meta = metaProvider->getMtkStaticCharacteristics();
    if( !tryGet<MRect>(meta, MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION, mActiveArrayRect) )
    {
        MY_LOGE("MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION failed, use (1600,1200)");
        return;
    }

    MY_LOGD("Sensor(%d) Active array(%d,%d)(%dx%d)",
             mSensorID,
             mActiveArrayRect.p.x, mActiveArrayRect.p.y,
             mActiveArrayRect.s.w, mActiveArrayRect.s.h);
}

MVOID P2PlatInfoImp::initNVRam()
{
    auto pNVBufUtil = MAKE_NvBufUtil();
    if( !mHalSensorList || !pNVBufUtil )
    {
        MY_LOGE("MAKE_HalSensorList = %p, MAKE_NvBUfUtil = %p", mHalSensorList, pNVBufUtil);
    }
    else
    {
        if( 0 != pNVBufUtil->getBufAndRead(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)mpNVRam) )
        {
            MY_LOGE("NvBufUtil->getBufAndRead() failed");
            mpNVRam = NULL;
        }
    }
}

MVOID P2PlatInfoImp::initIdxMgr()
{
#if MTK_CAM_NEW_NVRAM_SUPPORT
    mIdxMgr = IdxMgr::createInstance(mSensorDev);
#endif // MTK_CAM_NEW_NVRAM_SUPPORT
}

MVOID P2PlatInfoImp::initTuningPort()
{
    switch(mDipVersion)
    {
        case EDIPHWVersion_60:
        {
            mLsc2Port = PORT_LSCI;
            mBpc2Port = PORT_BPCI;
            mYnrFaceIPort = PORT_YNR_FACEI;
            mYnrLceiPort = PORT_YNR_LCEI;
            break;
        }
        case EDIPHWVersion_50:
        {
            mLsc2Port = PORT_IMGCI;
            mBpc2Port = PORT_IMGBI;
            mYnrFaceIPort = PORT_UNKNOWN;
            mYnrLceiPort = PORT_DEPI;
            break;
        }
        default:
        {
            mLsc2Port = PORT_DEPI;
            mBpc2Port = PORT_DMGI;
            mYnrFaceIPort = PORT_UNKNOWN;
            mYnrLceiPort = PORT_UNKNOWN;
            break;
        }
    }
}

template <unsigned ID>
const P2PlatInfo* getPlatInfo_()
{
    static P2PlatInfoImp sPlatInfo(ID);
    return &sPlatInfo;
}

const P2PlatInfo* P2PlatInfo::getInstance(MUINT32 sensorID)
{
    switch(sensorID)
    {
    case 0:   return getPlatInfo_<0>();
    case 1:   return getPlatInfo_<1>();
    case 2:   return getPlatInfo_<2>();
    case 3:   return getPlatInfo_<3>();
    case 4:   return getPlatInfo_<4>();
    case 5:   return getPlatInfo_<5>();
    case 6:   return getPlatInfo_<6>();
    case 7:   return getPlatInfo_<7>();
    case 8:   return getPlatInfo_<8>();
    case 9:   return getPlatInfo_<9>();
    default:  MY_LOGE("invalid sensorID=%d", sensorID);
              return NULL;
    };
}

} // namespace P2Util
} // namespace Feature
} // namespace NSCam
