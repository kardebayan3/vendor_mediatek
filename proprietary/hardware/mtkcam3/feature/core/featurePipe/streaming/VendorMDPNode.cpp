/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "VendorMDPNode.h"

#define PIPE_CLASS_TAG "VMDPNode"
#define PIPE_TRACE TRACE_VMDP_NODE
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam3/feature/eis/eis_ext.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

static MBOOL refineBoundaryF(const MSizeF &size, MRectF &crop)
{
    MBOOL isRefined = MFALSE;

    MRectF refined = crop;
    if( crop.p.x < 0 )
    {
        refined.p.x = 0;
        isRefined = MTRUE;
    }
    if( crop.p.y < 0 )
    {
        refined.p.y = 0;
        isRefined = MTRUE;
    }
    if( (refined.p.x + crop.s.w) > size.w )
    {
        refined.s.w = size.w - refined.p.x;
        isRefined = MTRUE;
    }
    if( (refined.p.y + crop.s.h) > size.h )
    {
        refined.s.h = size.h - refined.p.y;
        isRefined = MTRUE;
    }
    if( isRefined )
    {
        MY_LOGW("size:(%.0fx%.0f), crop:(%f,%f,%f,%f) -> crop:(%f,%f,%f,%f)",
            size.w, size.h,
            crop.p.x, crop.p.y, crop.s.w, crop.s.h,
            refined.p.x, refined.p.y, refined.s.w, refined.s.h);
        crop = refined;
    }

    return isRefined;
}

VendorMDPNode::VendorMDPNode(const char *name)
    : StreamingFeatureNode(name)
    , mMDP(PIPE_CLASS_TAG)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mData);
    TRACE_FUNC_EXIT();
}

VendorMDPNode::~VendorMDPNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL VendorMDPNode::onData(DataID id, const BasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    if( id == ID_VENDOR_TO_NEXT )
    {
        this->mData.enque(data);
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::onData(DataID id, const RequestPtr& pRequest)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", pRequest->mRequestNo, ID2Name(id));
    if( id == ID_N3D_TO_VMDP )
    {
        this->mN3DReadyData.enque(pRequest);
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

IOPolicyType VendorMDPNode::getIOPolicy(StreamType stream, const StreamingReqInfo &reqInfo) const
{
    IOPolicyType policy = IOPOLICY_BYPASS;
    if( reqInfo.isMaster() &&
        ( HAS_VENDOR_V1(reqInfo.mFeatureMask) ||
          HAS_VENDOR_V2(reqInfo.mFeatureMask) ) )
    {
        if( stream == STREAMTYPE_RECORD && HAS_EIS(reqInfo.mFeatureMask) && !mPipeUsage.supportVendorFullImg() )
        {
            policy = IOPOLICY_BYPASS;
        }
        else
        {
            policy = IOPOLICY_INOUT;
        }
    }

    return policy;
}

MBOOL VendorMDPNode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();
    if( mPipeUsage.supportN3D() )
    {
        this->addWaitQueue(&mN3DReadyData);
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::onUninit()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr request;
    BasicImgData data;

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mData.deque(data) )
    {
        MY_LOGE("Data deque out of sync");
        return MFALSE;
    }

    if( data.mRequest == NULL)
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    if( mPipeUsage.supportN3D() )
    {
        if( !mN3DReadyData.deque(request) )
        {
            MY_LOGE("mN3DReadyData deque out of sync");
            return MFALSE;
        }
        if(request->mRequestNo != data.mRequest->mRequestNo)
        {
            MY_LOGE("Request out of sync");
            return MFALSE;
        }
    }

    TRACE_FUNC_ENTER();
    request = data.mRequest;
    request->mTimer.startVMDP();
    TRACE_FUNC("Frame %d in MDP", request->mRequestNo);
    processMDP(request, data.mData);
    request->mTimer.stopVMDP();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::processMDP(const RequestPtr &request, const BasicImg &fullImg)
{
    TRACE_FUNC_ENTER();
    MBOOL result = MFALSE;

    BasicImg nextFullImg;

    if( request->needVendorMDP() && request->hasGeneralOutput() )
    {
        MDPWrapper::SFP_OUTPUT_ARRAY outputs;
        if( prepareMDPOut(request, fullImg, outputs, nextFullImg) )
        {
            if( fullImg.mBuffer != NULL )
            {
                result = mMDP.process(fullImg.mBuffer->getImageBufferPtr(), outputs, request->needPrintIO());
                request->updateResult(result);

                if( request->needDump() )
                {
                    for( unsigned i = 0; i < outputs.size(); ++i )
                    {
                        dumpData(request, outputs[i].mBuffer, "vmdp_%d", i);
                    }
                }
            }
            else
            {
                MY_LOGW("sensor(%d) Frame %d fullImg is NULL", mSensorIndex, request->mRequestNo);
            }
        }
    }

    TRACE_FUNC("frame %d need=%d result=%d", request->mRequestNo, request->needVendorMDP(), result);

    FeaturePipeParam::MSG_TYPE msg = mPipeUsage.supportWarpNode() ? FeaturePipeParam::MSG_DISPLAY_DONE : FeaturePipeParam::MSG_FRAME_DONE;
    handleData(ID_VMDP_TO_HELPER, HelperData(HelpReq(msg), request));

    if( mPipeUsage.supportWarpNode() )
    {
        handleData(ID_VMDP_TO_NEXT_FULLIMG, BasicImgData(nextFullImg.mBuffer != NULL ? nextFullImg : fullImg, request));
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorMDPNode::prepareMDPOut(const RequestPtr &request, const BasicImg &fullImg, MDPWrapper::SFP_OUTPUT_ARRAY& outputs, BasicImg &nextFullImg)
{
    TRACE_FUNC_ENTER();

    SFPOutput sfpOut;
    // display
    if( request->needDisplayOutput(this) &&
        request->getDisplayOutput(sfpOut) &&
        refineCrop(request, fullImg, sfpOut) )
    {
        outputs.push_back(sfpOut);
    }

    // record
    if( request->needRecordOutput(this) &&
        request->getRecordOutput(sfpOut) &&
        refineCrop(request, fullImg, sfpOut) )
    {
        outputs.push_back(sfpOut);
    }

    // Extra data
    std::vector<SFPOutput> sfpOutList;
    if( request->needExtraOutput(this) &&
        request->getExtraOutputs(sfpOutList))
    {
        for(auto&& out : sfpOutList)
        {
            if(refineCrop(request, fullImg, out))
            {
                outputs.push_back(out);
            }
        }
    }

    // next FullImg
    const MUINT32 masterID = request->mMasterID;
    if( request->needNextFullImg(this, masterID) )
    {
        MSize outSize = fullImg.mBuffer->getImageBuffer()->getImgSize();
        MSize resize;
        nextFullImg.mBuffer = request->requestNextFullImg(this, masterID, resize);
        nextFullImg.setDomainInfo(fullImg);
        if( resize.w && resize.h )
        {
            outSize = resize;
        }

        SFPOutput out;
        out.mBuffer = nextFullImg.mBuffer->getImageBufferPtr();
        out.mBuffer->setExtParam(outSize);
        out.mCropRect = MRect(MPoint(0,0), fullImg.mBuffer->getImageBuffer()->getImgSize());
        out.mCropDstSize = outSize;
        outputs.push_back(out);
    }

    TRACE_FUNC_EXIT();

    return !outputs.empty();
}


MBOOL VendorMDPNode::refineCrop(const RequestPtr &/*request*/, const BasicImg &fullImg, SFPOutput &sfpOut)
{
    MRectF cropRect = sfpOut.mCropRect;
    cropRect.p.x = max(sfpOut.mCropRect.p.x - fullImg.mDomainOffset.x, 0.0f);
    cropRect.p.y = max(sfpOut.mCropRect.p.y - fullImg.mDomainOffset.y, 0.0f);

    if( fullImg.mDomainTransformScale != MSizeF(1.0f, 1.0f) )
    {
        float ratioW = fullImg.mDomainTransformScale.w;
        float ratioH = fullImg.mDomainTransformScale.h;

        cropRect.p.x *= ratioW;
        cropRect.p.y *= ratioH;
        cropRect.s.w *= ratioW;
        cropRect.s.h *= ratioH;

        TRACE_FUNC("No(%d), ratio:%fx%f domain(%f,%f) source(%f,%f)(%fx%f) -> result(%f,%f)(%fx%f)",
            request->mRequestNo, ratioW, ratioH,
            fullImg.mDomainOffset.x, fullImg.mDomainOffset.y,
            sfpOut.mCropRect.p.x, sfpOut.mCropRect.p.y, sfpOut.mCropRect.s.w, sfpOut.mCropRect.s.h,
            cropRect.p.x, cropRect.p.y, cropRect.s.w, cropRect.s.h);
    }

    MSizeF sourceSize = fullImg.mBuffer->getImageBuffer()->getImgSize();
    refineBoundaryF(sourceSize, cropRect);
    sfpOut.mCropRect = cropRect;

    return MTRUE;
}

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
