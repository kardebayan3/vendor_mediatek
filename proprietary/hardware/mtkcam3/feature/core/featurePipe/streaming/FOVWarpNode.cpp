/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

// =========================================================
#if SUPPORT_FOV
// =========================================================
#include "FOVWarpNode.h"
#include "GPUWarpStream.h"
#include "WPEWarpStream.h"
#include <mtkcam3/feature/DualCam/FOVHal.h>
#include <DpBlitStream.h>
#include <mtkcam/drv/iopipe/PortMap.h>


#define PIPE_CLASS_TAG "FOVWarpNode"
#define PIPE_TRACE TRACE_FOVWARP_NODE
#include <featurePipe/core/include/PipeLog.h>

#define NORMAL_PIPE_NAME "fovWarp"


namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

using namespace NSCam::NSIoPipe;
using NSImageio::NSIspio::EPortIndex_WROTO;
using NSImageio::NSIspio::EPortIndex_WDMAO;

FOVWarpNode::FOVWarpNode(const char *name)
    : StreamingFeatureNode(name)
    , mWarpStream(NULL)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mFOVDatas);
    this->addWaitQueue(&mFullImgDatas);
    TRACE_FUNC_EXIT();
}

FOVWarpNode::~FOVWarpNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MVOID FOVWarpNode::setOutputBufferPool(const android::sp<IBufferPool> &pool)
{
    TRACE_FUNC_ENTER();
    mOutputBufferPool = pool;
    TRACE_FUNC_EXIT();
}

MBOOL FOVWarpNode::onData(DataID id, const BasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;
    if( id == ID_P2A_TO_FOV_WARP )
    {
        this->mFullImgDatas.enque(data);
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL FOVWarpNode::onData(DataID id, const FOVData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;
    if( id == ID_FOV_TO_FOV_WARP )
    {
        this->mFOVDatas.enque(data);
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

IOPolicyType FOVWarpNode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo &reqInfo) const
{
    IOPolicyType policy = IOPOLICY_BYPASS;

    if( HAS_FOV(reqInfo.mFeatureMask) && reqInfo.isMaster())
    {
        policy = IOPOLICY_INOUT;
    }

    return policy;
}

MBOOL FOVWarpNode::onInit()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    StreamingFeatureNode::onInit();
    if( mPipeUsage.supportWPE() )
    {
        mWarpStream = WPEWarpStream::createInstance();
    }
    else
    {
        mWarpStream = GPUWarpStream::createInstance();
    }

    if( mWarpStream == NULL )
    {
        MY_LOGE("Failed to create warp module");
        ret = MFALSE;
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL FOVWarpNode::onUninit()
{
    TRACE_FUNC_ENTER();
    mOutputBufferPool = NULL;
    if( mWarpStream )
    {
        mWarpStream->destroyInstance();
        mWarpStream = NULL;
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL FOVWarpNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;

    if( mWarpStream != NULL )
    {
        // using GPU warp
        MSize fullSize = mPipeUsage.getStreamingSize();
        fullSize.w = align(fullSize.w, 6);
        fullSize.h = align(fullSize.h, 6);
        MBOOL forceRGBA = getPropertyValue(KEY_FORCE_GPU_RGBA, VAL_FORCE_GPU_RGBA);
        mWarpOutBufferPool = GraphicBufferPool::create("fpipe.fovo",
                                                        fullSize.w,
                                                        fullSize.h,
                                                        forceRGBA ? HAL_PIXEL_FORMAT_RGBA_8888 : HAL_PIXEL_FORMAT_YV12,
                                                        GraphicBufferPool::USAGE_HW_TEXTURE);
        mWarpOutBufferPool->allocate(3);

        if( !mWarpStream->init(mSensorIndex, fullSize, MAX_WARP_SIZE) )
        {
            MY_LOGE("Failed to create warp module");
            ret = MFALSE;
        }
    }

    if (mPipeUsage.supportVendor() && mOutputBufferPool != NULL)
    {
        Timer timer;
        timer.start();
        mOutputBufferPool->allocate(mPipeUsage.getNumFOVWarpOutBuffer());
        timer.stop();
        MY_LOGD("fov warp %s %d buf in %d ms", STR_ALLOCATE, mPipeUsage.getNumFOVWarpOutBuffer(), timer.getElapsed());
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL FOVWarpNode::onThreadStop()
{
    TRACE_FUNC_ENTER();

    if ( mWarpStream != NULL )
    {
        this->waitWarpStreamBaseDone();
        mWarpStream->uninit();

        if( mWarpOutBufferPool != NULL )
        {
            IBufferPool::destroy(mWarpOutBufferPool);
            mWarpOutBufferPool = NULL;
        }
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL FOVWarpNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr request;
    FOVData fovData;
    BasicImgData fullImg;

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mFOVDatas.deque(fovData) )
    {
        MY_LOGE("WarpMapData deque out of sync");
        return MFALSE;
    }
    if( !mFullImgDatas.deque(fullImg) )
    {
        MY_LOGE("FullImgData deque out of sync");
        return MFALSE;
    }

    if( fovData.mRequest == NULL ||
        fovData.mRequest != fullImg.mRequest )
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    TRACE_FUNC_ENTER();

    request = fovData.mRequest;
    // if not need fov
    if(!request->needFOV())
    {
        TRACE_FUNC("Frame:%d, do not need FOV", request->mRequestNo);
        handleResultData(request, fullImg.mData);
        return MTRUE;
    }
    else
    {
        int ratio = request->getVar<MUINT32>(VAR_DUALCAM_ZOOM_RATIO, 0 );
        TRACE_FUNC("Frame:%d, need FOV, ratio=%d", request->mRequestNo, ratio);
    }

    if (request->needPrintIO())
    {
        printIO(request, fullImg, fovData);
    }

    TRACE_FUNC("Frame %d in FOVWarp", request->mRequestNo);

    if( mPipeUsage.supportFOVCombineEIS() )
    {
        processMDP(request, fovData.mData, fullImg.mData);
    }
    else
    {
        processWarp(request, fovData.mData, fullImg.mData);
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL FOVWarpNode::processMDP(const RequestPtr &request, const FOVResult &fovResult, BasicImg &fullImg)
{
    TRACE_FUNC_ENTER();

    //MDP
    BasicImg fovOutImg;
    SFPOutput displayOutput;
    if( request->getDisplayOutput(displayOutput) )
    {
        //MCropRect crop = displayCrop.mCropRect;
        MSize FullImgSize = fullImg.mBuffer->getImageBufferPtr()->getImgSize();
        displayOutput.mCropRect.p.x -= fullImg.mDomainOffset.w;
        displayOutput.mCropRect.p.y -= fullImg.mDomainOffset.h;
        applyFOVCrop(fovResult.mFOVScale, fovResult.mFOVShift, displayOutput.mCropRect, FullImgSize);

        MDPWrapper::SFP_OUTPUT_ARRAY outputs;
        outputs.push_back(displayOutput);

        request->mTimer.startFOVWarp();

        if( !mMDP.process(fullImg.mBuffer->getImageBufferPtr(), outputs, request->needPrintIO()) )
        {
            MY_LOGE("FOVWarp MDP process fail!");
        }

        request->mTimer.stopFOVWarp();
    }
    else
    {
        MY_LOGD("No display output");
    }

    handleResultData(request, fullImg);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL FOVWarpNode::processWarp(const RequestPtr &request, const FOVResult &fovResult, BasicImg &fullImg)
{
    TRACE_FUNC_ENTER();
    WarpParam param;
    ImgBuffer outBuf = mWarpOutBufferPool->requestIIBuffer();
    BasicImg fovOutImg;

    param.mRequest      = request;
    param.mIn           = fullImg.mBuffer;
    param.mWarpMap      = BasicImg(fovResult.mWarpMap);
    param.mByPass       = MFALSE;
    param.mWarpOut      = outBuf;
    param.mInSize       = param.mIn->getImageBuffer()->getImgSize();
    param.mOutSize      = fovResult.mWPESize;
    param.mDomainOffset = fullImg.mDomainOffset;
    outBuf->getImageBufferPtr()->setExtParam(param.mOutSize);

    prepareWarpOutput(request, fovResult, fovOutImg, param);

    MY_LOGD("mIn(%dx%d) mOut(%dx%d) fovMargin(%dx%d)", param.mInSize.w, param.mInSize.h, param.mOutSize.w, param.mOutSize.h,
        request->getFOVMarginPixel().w, request->getFOVMarginPixel().h);

    // start timer
    request->mTimer.startFOVWarp();
    this->incExtThreadDependency();
    EnqueCookie cookie(request, this, fovResult, fullImg);
    cookie.fovOutImg = fovOutImg;
    this->enqueWarpStreamBase(mWarpStream, param, cookie);
    TRACE_FUNC("sensor(%d) Frame %d fov gpu enque done", mSensorIndex, request->mRequestNo);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL FOVWarpNode::prepareWarpOutput(const RequestPtr &request, const FOVResult &fovResult, BasicImg &fovOutImg, WarpParam &param)
{
    TRACE_FUNC_ENTER();

    MBOOL bExistOutput = MFALSE;
    MDPWrapper::SFP_OUTPUT_ARRAY outList;
    MUINT32 masterID = request->mMasterID;
    // get output : display
    SFPOutput yuvOut;
    if( request->needDisplayOutput(this) && request->getDisplayOutput(yuvOut) && fovResult.mDisplayCrop.s.w )
    {
        yuvOut.mCropRect = fovResult.mDisplayCrop;
        outList.push_back(yuvOut);
        TRACE_FUNC("Display YUV (%d,%d) (%d,%d)",
                    yuvOut.mCropRect.p.x, yuvOut.mCropRect.p.y, yuvOut.mCropRect.s.w, yuvOut.mCropRect.s.h);

        bExistOutput = MTRUE;
    }

    if( request->needRecordOutput(this) && request->getRecordOutput(yuvOut) && fovResult.mRecordCrop.s.w )
    {
        yuvOut.mCropRect = fovResult.mRecordCrop;
        outList.push_back(yuvOut);
        TRACE_FUNC("Record YUV (%d,%d) (%d,%d)",
                    yuvOut.mCropRect.p.x, yuvOut.mCropRect.p.y, yuvOut.mCropRect.s.w, yuvOut.mCropRect.s.h);

        bExistOutput = MTRUE;
    }

    std::vector<SFPOutput> extraList;
    if( request->needExtraOutput(this) && request->getExtraOutputs(extraList) && fovResult.mExtraCrop.s.w )
    {
        int i = 0;
        for(auto&& extra : extraList)
        {
            extra.mCropRect = fovResult.mExtraCrop;
            outList.push_back(extra);
            TRACE_FUNC("Extra YUV[%d] (%d,%d) (%d,%d)", i,
                        extra.mCropRect.p.x, extra.mCropRect.p.y, extra.mCropRect.s.w, extra.mCropRect.s.h);
            i++;
        }
        bExistOutput = MTRUE;
    }

    if( request->needFullImg(this, masterID) )
    {
        fovOutImg.mBuffer = mOutputBufferPool->requestIIBuffer();
        fovOutImg.mDomainOffset = param.mDomainOffset + request->getFOVMarginPixel();

        SFPOutput out;
        out.mBuffer = fovOutImg.mBuffer->getImageBufferPtr();
        out.mBuffer->setExtParam(fovResult.mWPESize);
        out.mCropRect = MRect(fovResult.mWPESize.w, fovResult.mWPESize.h);

        outList.push_back(out);
        bExistOutput = MTRUE;
    }

    if( request->needNextFullImg(this, masterID))
    {
        MSize outSize = fovResult.mWPESize;
        MSize resize;
        fovOutImg.mBuffer = request->requestNextFullImg(this, masterID, resize);
        fovOutImg.mDomainOffset = param.mDomainOffset + request->getFOVMarginPixel();
        if( resize.w && resize.h )
        {
            outSize = resize;
        }

        SFPOutput out;
        out.mBuffer = fovOutImg.mBuffer->getImageBufferPtr();
        out.mBuffer->setExtParam(outSize);
        out.mCropRect = MRect(outSize.w, outSize.h);

        outList.push_back(out);
        bExistOutput = MTRUE;
    }

    param.mMDPOut.clear();
    if( !bExistOutput )
    {
        MY_LOGE("FOVWarp no exist any yuv buffer");
    }
    else
    {
        mMDP.generateOutArray(outList, param.mMDPOut);
    }

    TRACE_FUNC_EXIT();
    return bExistOutput;
}


MVOID FOVWarpNode::onWarpStreamBaseCB(const WarpParam &param, const EnqueCookie &cookie)
{
    RequestPtr request = cookie.request;
    BasicImg fovOutImg = cookie.fovOutImg;
    // stop timer
    request->mTimer.stopFOVWarp();
    TRACE_FUNC("sensor(%d) Frame %d, fov gpu warp done in %d ms, result = %d",
                mSensorIndex, request->mRequestNo, request->mTimer.getElapsedFOVWarp(), param.mResult);

    handleResultData(request, fovOutImg);

    this->decExtThreadDependency();

    TRACE_FUNC_EXIT();
}

MVOID FOVWarpNode::bufferDump(const RequestPtr &request, IImageBuffer* pImgBuf, const char* name)
{
    if(!request->needDump())
        return;

    TRACE_FUNC("Frame:%d", request->mRequestNo);
    char filepath[1024];
    makePath("/sdcard/wpe", 0660);
    snprintf(filepath, 1024, "/sdcard/wpe/%s_%d_%dx%d.yuv", name, request->mRequestNo, pImgBuf->getImgSize().w, pImgBuf->getImgSize().h);
    pImgBuf->saveToFile(filepath);
    TRACE_FUNC_EXIT();
}

MVOID FOVWarpNode::printIO(const RequestPtr &request, const BasicImgData &fullImg, const FOVData &fovData)
{
    SFPOutput output;
    if(request->getDisplayOutput(output))
    {
        MY_LOGD("display crop() (%d, %d) %dx%d",
            output.mCropRect.p.x, output.mCropRect.p.y,
            output.mCropRect.s.w, output.mCropRect.s.h);
    }

    if(request->getRecordOutput(output))
    {
        MY_LOGD("record crop() (%d, %d) %dx%d",
            output.mCropRect.p.x, output.mCropRect.p.y,
            output.mCropRect.s.w, output.mCropRect.s.h);
    }
     MY_LOGD("fullImg size:(%dx%d) request->fovMargin(%d, %d)",
        fullImg.mData.mBuffer->getImageBufferPtr()->getImgSize().w,
        fullImg.mData.mBuffer->getImageBufferPtr()->getImgSize().h,
        request->getFOVMarginPixel().w, request->getFOVMarginPixel().h);
     MY_LOGD("fovData scale(%f) shift(%d, %d) wpeSize(%dx%d) wpeDisplay crop(%d, %d)(%dx%d) wpeRecord crop(%d, %d)(%dx%d)",
         fovData.mData.mFOVScale, fovData.mData.mFOVShift.x, fovData.mData.mFOVShift.y,
         fovData.mData.mWPESize.w, fovData.mData.mWPESize.h,
         fovData.mData.mDisplayCrop.p.x, fovData.mData.mDisplayCrop.p.y, fovData.mData.mDisplayCrop.s.w, fovData.mData.mDisplayCrop.s.h,
         fovData.mData.mRecordCrop.p.x, fovData.mData.mRecordCrop.p.y, fovData.mData.mRecordCrop.s.w, fovData.mData.mRecordCrop.s.h);
}

MVOID FOVWarpNode::handleResultData(
    const RequestPtr &request,
    const BasicImg &fullImg
)
{
    TRACE_FUNC_ENTER();
    if (mPipeUsage.supportVendor())
    {
        handleData(ID_FOV_WARP_TO_VENDOR, BasicImgData(fullImg, request));
    }
    else if( mPipeUsage.supportEISNode() )
    {
        handleData(ID_FOV_WARP_TO_HELPER, HelperData(FeaturePipeParam::MSG_DISPLAY_DONE, request));
        handleData(ID_FOV_TO_EIS_FULLIMG, BasicImgData(fullImg, request));
    }
    else
    {
        handleData(ID_FOV_WARP_TO_HELPER, HelperData(FeaturePipeParam::MSG_FRAME_DONE, request));
    }
    TRACE_FUNC_EXIT();
    return;
}

MVOID FOVWarpNode::applyFOVCrop(
    const float &scale,
    const MPoint &shift,
    MRect &crop,
    const MSize maxSize
)
{
    crop.s.w *= scale;
    crop.s.h *= scale;
    crop.p += shift;
    if ((crop.p.x + crop.s.w) > maxSize.w)
    {
        crop.p.x = maxSize.w - crop.s.w;
    }
    if ((crop.p.y + crop.s.h) > maxSize.h)
    {
        crop.p.y = maxSize.h - crop.s.h;
    }
    crop.p.x = (crop.p.x < 0) ? 0 : crop.p.x;
    crop.p.y = (crop.p.y < 0) ? 0 : crop.p.y;
    TRACE_FUNC("fov crop(%d, %d)%dx%d", crop.p.x, crop.p.y, crop.s.w, crop.s.h);

}

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
// =========================================================
#endif // SUPPORT_FOV
// =========================================================
