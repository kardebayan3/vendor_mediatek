/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "TPIMgr_Util.h"

#include "DebugControl.h"
#define PIPE_CLASS_TAG "TPI_MGR"
#define PIPE_TRACE TRACE_TPI_MGR
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

const char* toScenarioString(TPI_SCENARIO_TYPE scenario)
{
  switch(scenario)
  {
  case TPI_SCENARIO_STREAMING_SINGLE:       return "streaming single";
  case TPI_SCENARIO_STREAMING_DUAL_BASIC:   return "streaming dual basic";
  case TPI_SCENARIO_STREAMING_DUAL_DEPTH:   return "streaming dual depth";
  case TPI_SCENARIO_STREAMING_DUAL_VSDOF:   return "streaming dual vsdof";
  case TPI_SCENARIO_SLOWMOTION:             return "slowmotion";
  default:                                  return "unknown";
  };
}

void print(const TPI_BufferInfo &buffer)
{
  MY_LOGD("Buffer ID:0x%x fmt:0x%08x size(%dx%d) setting(0x%x/0x%x)",
    buffer.mBufferID, buffer.mFormat, buffer.mSize.w, buffer.mSize.h,
    buffer.mBufferSetting, buffer.mCustomSetting);
}

void print(const TPI_NodeInfo &node)
{
  MY_LOGD("Node ID:0x%x Opt:(0x%x/0x%x) Buf#%zu",
    node.mNodeID, node.mNodeOption, node.mCustomOption,
    node.mBufferInfoListCount);
  for( size_t i = 0; i < node.mBufferInfoListCount; ++i )
  {
    print(node.mBufferInfoList[i]);
  }
}

void print(const TPI_PathInfo &path)
{
  MY_LOGD("[0x%x:0x%x] => [0x%x:0x%x]",
    path.mSrc.mNode, path.mSrc.mPort, path.mDst.mNode, path.mDst.mPort);
}

void print(const TPI_Session &session)
{
  print(&session);
}

void print(const TPI_Session *session)
{
  if( session )
  {
    MY_LOGD("Session:%d(%p) sensor:%d scenario:%d(%s) cookie:%p",
      session->mSessionID, session, session->mLogicalSensorID,
      session->mScenario, toScenarioString(session->mScenario),
      session->mSessionCookie);

    for( size_t i = 0; i < session->mNodeInfoListCount; ++i )
    {
      print(session->mNodeInfoList[i]);
    }

    for( size_t i = 0; i < session->mPathInfoListCount; ++i )
    {
      print(session->mPathInfoList[i]);
    }
  }
}

void addBufferInfo(TPI_NodeInfo &node, const TPI_BufferInfo &buffer)
{
  if( node.mBufferInfoListCount < TPI_BUFFER_INFO_LIST_SIZE )
  {
    size_t index = node.mBufferInfoListCount;
    node.mBufferInfoList[index] = buffer;
    node.mBufferInfoListCount++;
  }
}

void addNodeInfo(TPI_Session &session, const TPI_NodeInfo &info)
{
  if( session.mNodeInfoListCount < TPI_NODE_INFO_LIST_SIZE )
  {
    size_t index = session.mNodeInfoListCount;
    session.mNodeInfoList[index] = info;
    session.mNodeInfoListCount++;
  }
}

void addPathInfo(TPI_Session &session, unsigned srcID, unsigned srcPort, unsigned dstID, unsigned dstPort)
{
  if( session.mPathInfoListCount < TPI_PATH_INFO_LIST_SIZE )
  {
    size_t index = session.mPathInfoListCount;
    session.mPathInfoList[index].mSrc.mNode = srcID;
    session.mPathInfoList[index].mSrc.mPort = srcPort;
    session.mPathInfoList[index].mDst.mNode = dstID;
    session.mPathInfoList[index].mDst.mPort = dstPort;
    session.mPathInfoListCount++;
  }
}

#if 0
TPI_VendorMetas toVendorMetas(TPI_Meta *mMetaList, size_t count)
{
  TPI_VendorMetas metas;
  for( size_t i = 0; i < count; ++i )
  {
    switch(mMetaList[i].mMetaID)
    {
    case TPI_META_ID_MTK_IN_APP:        metas.mInApp = mMetaList[i].mMetaPtr;       break;
    case TPI_META_ID_MTK_IN_P1_APP:     metas.mInP1App = mMetaList[i].mMetaPtr;     break;
    case TPI_META_ID_MTK_IN_P1_HAL:     metas.mInP1Hal = mMetaList[i].mMetaPtr;     break;
    case TPI_META_ID_MTK_IN_P1_APP_2:   metas.mInP1App_2 = mMetaList[i].mMetaPtr;   break;
    case TPI_META_ID_MTK_IN_P1_HAL_2:   metas.mInP1Hal_2 = mMetaList[i].mMetaPtr;   break;
    case TPI_META_ID_MTK_OUT_P2_APP:    metas.mOutP2App = mMetaList[i].mMetaPtr;    break;
    case TPI_META_ID_MTK_OUT_P2_HAL:    metas.mOutP2Hal = mMetaList[i].mMetaPtr;    break;
    default: break;
    }
  }
  return metas;
}
#endif

#if 0
TPI_VendorBuffers toVendorBuffers(TPI_Buffer *mBufferList, size_t count)
{
  TPI_VendorBuffers buffers;
  for( size_t i = 0; i < count; ++i )
  {
    switch(mBufferList[i].mBufferID)
    {
    case TPI_BUFFER_ID_MTK_YUV:         buffers.mYUV = mBufferList[i].mBufferPtr;       break;
    case TPI_BUFFER_ID_MTK_YUV_2:       buffers.mYUV_2 = mBufferList[i].mBufferPtr;     break;
    case TPI_BUFFER_ID_MTK_DEPTH:       buffers.mDepth = mBufferList[i].mBufferPtr;     break;
    case TPI_BUFFER_ID_MTK_PURE:        buffers.mPure = mBufferList[i].mBufferPtr;      break;
    case TPI_BUFFER_ID_MTK_PURE_2:      buffers.mPure_2 = mBufferList[i].mBufferPtr;    break;
    case TPI_BUFFER_ID_MTK_OUT_YUV:     buffers.mOut_YUV = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_MTK_OUT_YUV_2:   buffers.mOut_YUV_2 = mBufferList[i].mBufferPtr; break;
    case TPI_BUFFER_ID_VENDOR_1:        buffers.mVendor1 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_2:        buffers.mVendor2 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_3:        buffers.mVendor3 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_4:        buffers.mVendor4 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_5:        buffers.mVendor5 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_6:        buffers.mVendor6 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_7:        buffers.mVendor7 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_8:        buffers.mVendor8 = mBufferList[i].mBufferPtr;   break;
    case TPI_BUFFER_ID_VENDOR_9:        buffers.mVendor9 = mBufferList[i].mBufferPtr;   break;
    default: break;
    }
  }
  return buffers;
}
#endif

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
