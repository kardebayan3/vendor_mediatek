/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "inc/tpi_mgr.h"
#include "inc/tpi_client.h"

#include <atomic>

#undef LOG_TAG
#define LOG_TAG "MtkCam/TPI_MGR"
#include <mtkcam/utils/std/Log.h>

#undef MY_LOGE
#undef MY_LOGW
#undef MY_LOGI
#undef MY_LOGD
#define MY_LOGE(fmt, arg...) CAM_LOGE("[%s]" fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...) CAM_LOGW("[%s]" fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...) CAM_LOGI("[%s]" fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...) CAM_LOGD("[%s]" fmt, __FUNCTION__, ##arg)

bool TPI_Mgr::isVendorNode(unsigned nodeID)
{
  return nodeID >= 0 && nodeID < TPI_NODE_ID_RESERVED;
}

TPI_IOMap TPI_Mgr::toIOMap(const TPI_Session &session)
{
  TPI_IOMap map;
  for( unsigned i = 0; i < session.mNodeInfoListCount; ++i )
  {
    map[session.mNodeInfoList[i].mNodeID] = TPI_IO(session.mNodeInfoList[i]);
  }
  for( unsigned i = 0; i < session.mPathInfoListCount; ++i )
  {
    TPI_PathInfo path = session.mPathInfoList[i];
    map[path.mDst.mNode].addPrev(path);
    map[path.mSrc.mNode].addNext(path);
  }
  return map;
}

TPI_Mgr::TPI_Mgr()
{
}

TPI_Mgr::~TPI_Mgr()
{
  if( mState == STATE_RUN )
  {
    MY_LOGD("User should call stop before exit");
    stop();
  }
  if( mState == STATE_STOP )
  {
    MY_LOGD("User should call uninit before exit");
    uninitSession();
  }
  if( mState == STATE_UNINIT )
  {
    MY_LOGD("User should call destroy before exit");
    destroySession();
  }
}

bool TPI_Mgr::createSession(unsigned logicalSensorID, TPI_SCENARIO_TYPE scenario, const IMetadata *meta)
{
  bool ret = false;
  MY_LOGI("+");
  if( mState == TPI_Mgr::STATE_IDLE )
  {
    mSession.mSessionID = generateSessionID();
    mSession.mLogicalSensorID = logicalSensorID;
    mSession.mScenario = scenario;

    if( !TPI_createSession(&mSession, meta) )
    {
      MY_LOGE("Create session failed");
    }
    else if( !checkSession(mSession) )
    {
      MY_LOGE("Validate session failed");
      TPI_destroySession(&mSession);
      mState = TPI_Mgr::STATE_DESTROY;
    }
    else
    {
      mState = TPI_Mgr::STATE_CREATE;
      ret = true;
    }
  }
  MY_LOGI("- ret=%d", ret);
  return true;
}

bool TPI_Mgr::destroySession()
{
  bool ret = false;
  MY_LOGI("+");
  if( mState == TPI_Mgr::STATE_UNINIT )
  {
    TPI_destroySession(&mSession);
    mSession.mSessionCookie = NULL;
    mState = TPI_Mgr::STATE_DESTROY;
    ret = true;
  }
  MY_LOGI("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::initSession()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_CREATE )
  {
    mState = TPI_Mgr::STATE_INIT;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::uninitSession()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_INIT ||
      mState == TPI_Mgr::STATE_STOP )
  {
    mState = TPI_Mgr::STATE_UNINIT;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::initNode(unsigned nodeID)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_INIT )
  {
    if( mNodes[nodeID] == NULL )
    {
        TPI_Node *node = NULL;
        bool ret = TPI_createNode(nodeID, node);
        if( node == NULL )
        {
            MY_LOGE("Invalid node[0x%zx]=%p", nodeID, node);
            ret = false;
        }
        if( ret )
        {
            TPI_InitData data;
            mNodes[nodeID] = node;
            node->onInit(data);
        }
    }
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::uninitNode(unsigned nodeID)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_STOP )
  {
    TPI_Node *node = mNodes[nodeID];
    if( node != NULL )
    {
      node->onUninit();
      TPI_destroyNode(node);
      node = NULL;
      mNodes[nodeID] = NULL;
      ret = true;
    }
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::start()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_INIT )
  {
    mState = TPI_Mgr::STATE_RUN;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::stop()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_RUN )
  {
    mState = TPI_Mgr::STATE_STOP;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::genFrame(unsigned &frame)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_RUN )
  {
    frame = ++mFrameID;
    ret = true;
  }
  MY_LOGD("- ret=%d frame=%d", ret, frame);
  return ret;
}

bool TPI_Mgr::releaseFrame(unsigned frame)
{
  MY_LOGD("+");
  MY_LOGD("- frame=%d", frame);
  return true;
}

bool TPI_Mgr::enqueNode(unsigned nodeID, unsigned frame, TPI_Meta meta[], size_t metaCount, TPI_Buffer img[], size_t imgCount)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPI_Mgr::STATE_RUN )
  {
    TPI_Node *node = mNodes[nodeID];
    if( node != NULL )
    {
      TPI_EnqueData data;
      data.mFrameID = frame;
      data.mMetaListCount = metaCount;
      for( unsigned i = 0; i < metaCount; ++i )
      {
          data.mMetaList[i] = meta[i];
      }
      data.mBufferListCount = imgCount;
      for( unsigned i = 0; i < imgCount; ++i )
      {
          data.mBufferList[i] = img[i];
      }
      ret = node->onEnque(data);
    }
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPI_Mgr::getSessionInfo(TPI_Session &session) const
{
  bool ret = false;
  if( mState != STATE_IDLE &&
      mState != STATE_DESTROY )
  {
    session = mSession;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

unsigned TPI_Mgr::generateSessionID()
{
  static std::atomic<unsigned> sSessionID(0);
  return sSessionID++;
}

bool TPI_Mgr::checkSession(const TPI_Session &session) const
{
  bool ret = true;
  MY_LOGD("+");
  if( session.mMgrVersion != TPI_VERSION ||
      session.mClientVersion != TPI_VERSION )
  {
    MY_LOGE("Invalid API VERSION");
    ret = false;
  }
  else if( session.mScenario == TPI_SCENARIO_UNKNOWN )
  {
    MY_LOGE("Invalid scenario");
    ret = false;
  }
  else if( session.mNodeInfoListCount > TPI_NODE_INFO_LIST_SIZE ||
      session.mPathInfoListCount > TPI_PATH_INFO_LIST_SIZE )
  {
    MY_LOGE("Invalid info list count");
    ret = false;
  }
  for( size_t i = 0; i < session.mNodeInfoListCount; ++i )
  {
  }
  for( size_t i = 0; i < session.mPathInfoListCount; ++i )
  {
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}
