/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_TPI_MGR_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_TPI_MGR_H_

#include "tpi_session.h"
#include <list>
#include <map>

class TPI_Mgr
{
public:
  static bool isVendorNode(unsigned nodeID);
  static TPI_IOMap toIOMap(const TPI_Session &session);

public:
  TPI_Mgr();
  ~TPI_Mgr();
  bool createSession(unsigned logicalSensorID, TPI_SCENARIO_TYPE scenario, const IMetadata *meta);
  bool destroySession();

  bool initSession();
  bool uninitSession();

  bool initNode(unsigned nodeID);
  bool uninitNode(unsigned nodeID);

  bool start();
  bool stop();

  bool genFrame(unsigned &frame);
  bool releaseFrame(unsigned frame);

  bool enqueNode(unsigned nodeID, unsigned frame, TPI_Meta meta[], size_t metaCount, TPI_Buffer img[], size_t imgCount);

  bool getSessionInfo(TPI_Session &session) const;

private:
  static unsigned generateSessionID();

private:
  bool checkSession(const TPI_Session &session) const;

private:
  enum eState {
    STATE_IDLE,
    STATE_CREATE,
    STATE_INIT,
    STATE_RUN,
    STATE_STOP,
    STATE_UNINIT,
    STATE_DESTROY,
  };

private:
  eState mState = TPI_Mgr::STATE_IDLE;
  TPI_Session mSession;
  unsigned mFrameID = 0;

  std::map<unsigned, TPI_Node*> mNodes;

};

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_TPI_MGR_H_
