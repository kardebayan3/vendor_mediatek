/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define LOG_TAG "mtkcam-CustomSuperNightEvDecision"
//
#include <cutils/properties.h>
// MTKCAM
#include <mtkcam/utils/hw/HwInfoHelper.h>
//
#include <mtkcam3/3rdparty/customer/CustomSuperNightEvDecision.h>
//
#define __DEBUG // enable function scope debug
#ifdef __DEBUG
#define FUNCTION_SCOPE          auto __scope_logger__ = create_scope_logger(__FUNCTION__)
#include <memory>
#include <functional>
static std::shared_ptr<char> create_scope_logger(const char* functionName)
{
    char* pText = const_cast<char*>(functionName);
    CAM_LOGD_IF(pText, "[%s] + ", pText);
    return std::shared_ptr<char>(pText, [](char* p){ CAM_LOGD_IF(p, "[%s] -", p); });
}
#else
#define FUNCTION_SCOPE          do{}while(0)
#endif
/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3::pipeline::policy::evdecision;
using namespace NSCam::v3::pipeline::policy::supernightevdecision;
using namespace NS3Av3; //IHal3A
using namespace NSCamHW; //HwInfoHelper
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)

// must disable this debug flag before MP
#define DEBUG_CUSTOM_SUPER_NIGHT_EV_DECISION  (1)

// state for super night
enum SuperNightState
{
    eSuperNightState_None = 0,
    eSuperNightState_Tripod = 1,
    eSuperNightState_Handheld = 2,
    eSuperNightState_Face = 3,
    eSuperNightState_Unknow, // equal or over this value is invalid.
};

// frames count for different super night state
#define SUPER_NIGHT_STATE_FRAMES_COUNT_TRIPOD   (17)
#define SUPER_NIGHT_STATE_FRAMES_COUNT_HANDHELD  (7)
#define SUPER_NIGHT_STATE_FRAMES_COUNT_FACE      (7)

/******************************************************************************
 *
 ******************************************************************************/
CustomSuperNightEvDecision::
CustomSuperNightEvDecision(
    CreationParams const& params
)
    :mDecisionParams(params)
    ,mFrameIndex(0)
    ,mTotalFramesCount(0)
    ,mIsLastFrame(false)
    ,mIsKeepTuning(false)
{
    FUNCTION_SCOPE;
    mbDebug = ::property_get_int32("vendor.debug.camera.supernightevdecision.log", DEBUG_CUSTOM_SUPER_NIGHT_EV_DECISION);
    mForcedSuperNightState = ::property_get_int32("vendor.debug.supernight.state", 0);

    // query sensor name by sensorId
    {
        //IHalSensorList* const pHalSensorList = IHalSensorList::get();
        IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
        if (pHalSensorList == nullptr) {
            MY_LOGE("get pHalSensorList(%p) failed!", pHalSensorList);
        }
        //pHalSensorList->searchSensors();
        mSensorName = (pHalSensorList) ? pHalSensorList->queryDriverName(mDecisionParams.sensorId) : "UNKNOW_SENSOR_NAME";
        MY_LOGD_IF(mSensorName.c_str(), "mSensorName:%s", mSensorName.c_str());
    }

    // create Hal3A for 3A info query
    std::shared_ptr<IHal3A> hal3a
    (
        MAKE_Hal3A(mDecisionParams.sensorId, LOG_TAG),
        [](IHal3A* p){ if(p) p->destroyInstance(LOG_TAG); }
    );
    if (hal3a.get()) {
        mHal3a = hal3a;
    }
    else {
        MY_LOGE("MAKE_Hal3A failed, it nullptr");
    }
}

auto
CustomSuperNightEvDecision::
evaluateEvDecision(
    DecisionOutputParams* out,
    DecisionInputParams const* in
) -> bool
{
    FUNCTION_SCOPE;
    // Check Control Metadata In/Out is valid or not,
    // you could get the input control app meata values from in->pAppControlMetadata by getEntry API.
    if (in->pAppControlMetadata == nullptr ||
        out->pAdditionalAppControlMetadata == nullptr ||
        out->pAdditionalHalControlMetadata == nullptr )
    {
        MY_LOGE("pAppControlMetadata(%p), pAdditionalAppControlMetadata(%p), pAdditionalHalControlMetadata(%p) is invalid nullptr!",
                in->pAppControlMetadata, out->pAdditionalAppControlMetadata.get(), out->pAdditionalHalControlMetadata.get());
        return false;
    }


    // TODO: modify to read customer's super nigt state hint
    int32_t superNightState = mForcedSuperNightState;

    // current sensor name: mSensorName.c_str()
    MY_LOGD_IF(mSensorName.c_str(), "mSensorName:%s", mSensorName.c_str());

    // update SuperNight request raw16 output frame index
    mFrameIndex++;

    // error handling: abnormal frame count!!
    // set frame index to 1, and reset flags as new super night reprocessing behavior
    if (mFrameIndex > mTotalFramesCount) {
        MY_LOGW("invalid frameIndex(%d), which is out of mTotalFramesCount(%u)", mFrameIndex, mTotalFramesCount);

        mFrameIndex = 1;
        mTotalFramesCount = 0;
        mIsLastFrame = false;
        mIsKeepTuning = false;
        ::memset(&mPreview3AInfo, 0, sizeof(Current3AInfo));

        MY_LOGW("reset the flags(mFrameIndex:%u, mTotalFramesCount:%u, mIsLastFrame:%d, mIsKeepTuning:%d) as new super night request",
                mFrameIndex, mTotalFramesCount, mIsLastFrame, mIsKeepTuning);
    }

    // query current 3A info for test.
    if (mFrameIndex == 1) {
        MY_LOGD("get preview 3A info, current frame index(%u)", mFrameIndex);
        ::memset(&mPreview3AInfo, 0, sizeof(Current3AInfo)); // reset value
        if (!getCurrent3AInfo(mPreview3AInfo)) {
            MY_LOGE("get preview 3A info by getCurrent3AInfo failed!");
            return false;
        }
    }

    // get super night raw16 ouput capture frames count by super night state
    if (mTotalFramesCount <= 0) {
        if (getSupportedFramesCountByState(superNightState, mTotalFramesCount) == false) {
            MY_LOGE("getSupportedFramesCountByState failed!");
            return false;
        }
    }
    else {
        MY_LOGD_IF(mbDebug, "current frame index(%u), no need update frames count(%u)", mFrameIndex, mTotalFramesCount);
    }

    // check the valid setting
    if (mTotalFramesCount < 1) {
        MY_LOGE("invalid mTotalFramesCount(%u)", mTotalFramesCount);
        return false;
    }

    // return SuperNight frame index for debug
    out->frameIndex = mFrameIndex;
    MY_LOGI_IF(mbDebug, "current req#%u requested raw16 output frame count(%u)",in->requestNo,  out->frameIndex);


    // TODO: example code: it could be optimize and design by customer's ev reuirement
    //============================= start: ev setting example code =====================================
    MINT32 evSetting = 0;
    if (!getEvSettingByHint(superNightState, mFrameIndex, evSetting)) {
        MY_LOGW("getEvSettingByHint failed!");
        return false;
    }

    // use android defined metadata to indicate HAL3A ae compensation
    // App Control Metadata Out, set EV setting here by setEntry API.
    IMetadata::setEntry<MINT32>(
            out->pAdditionalAppControlMetadata.get(), MTK_CONTROL_AE_EXPOSURE_COMPENSATION, evSetting);

    // TODO: use to hint keep the tuning buffer for reprocess, suggest to keep one of 0EV frame.
    if (evSetting == 0 && mIsKeepTuning == false) {
        mIsKeepTuning = true; // reprocessing tuning setting has been keep, enable this flasg
        out->isKeepTuning = true;
        MY_LOGI_IF(mbDebug, "keep tuning frame for frameIndex:%u, mIsKeepTuning:%d, evSetting:%d",
                   mFrameIndex, mIsKeepTuning, evSetting);
    }
    else {
        out->isKeepTuning = false;
    }
    //============================= end: ev setting example code =====================================

    // TODO: use to hint the last frame, to append dummy frames for preview resume stable.
    if (out->frameIndex == mTotalFramesCount) {
        out->isLastFrame = true;
        mIsLastFrame = true;
        MY_LOGI_IF(mbDebug, "It is last frame(%u), hint to append dummy frames for preview stable.", out->frameIndex);
    }
    else {
        out->isLastFrame = false;
    }

    return true;
}

auto
CustomSuperNightEvDecision::
reprocessTriggered(
) -> bool
{
    FUNCTION_SCOPE;
    bool ret = true;

    // check status for monitor HAL MW and Camera App behavior
    if (mIsLastFrame == false) {
        MY_LOGW("never mark last frame(mIsLastFrame:%d) for this shot, it may occur preview unsable during resume preview.", mIsLastFrame);
        ret = false;
    }

    if (mIsKeepTuning == false) {
        MY_LOGW("never mark keep tuning frame(mIsKeepTuning:%d) for this shot, the raw16 reprocess may be fatal", mIsKeepTuning);
        ret = false;
    }

    // check app request frame count and total frames count is match or not.
    MY_LOGI("requested raw16 output frame count, current index(%u)", mFrameIndex);
    if (mFrameIndex != mTotalFramesCount) {
        MY_LOGW("request frame index(%u) is not equal to total frames count(%u)", mFrameIndex, mTotalFramesCount);
    }

    // reset flags for next super night reprocessing behavior
    mFrameIndex = 0;
    mTotalFramesCount = 0;
    mIsLastFrame = false;
    mIsKeepTuning = false;
    // reset preview 3A info
    ::memset(&mPreview3AInfo, 0, sizeof(Current3AInfo));

    MY_LOGI_IF(mbDebug, "reset the flags(mFrameIndex:%u, mTotalFramesCount:%u, mIsLastFrame:%d, mIsKeepTuning:%d)",
               mFrameIndex, mTotalFramesCount, mIsLastFrame, mIsKeepTuning);
    return ret;
}

auto
CustomSuperNightEvDecision::
getSupportedFramesCountByState(
    uint32_t  superNightState,
    uint32_t& superNightFramesCount
) -> bool
{
    switch (superNightState) {
        case eSuperNightState_Tripod:
            superNightFramesCount = SUPER_NIGHT_STATE_FRAMES_COUNT_TRIPOD;
            break;
        case eSuperNightState_Handheld:
            superNightFramesCount = SUPER_NIGHT_STATE_FRAMES_COUNT_HANDHELD;
            break;
        case eSuperNightState_Face:
            superNightFramesCount = SUPER_NIGHT_STATE_FRAMES_COUNT_FACE;
            break;
        default:
            MY_LOGE("invalid and unknow super night state:%d", superNightState);
            return false;
    }
    return true;
}

auto
CustomSuperNightEvDecision::
getEvSettingByHint(
    uint32_t  superNightState,
    uint32_t  frameIndex,
    int32_t& evSetting
) -> bool
{
    switch (superNightState) {
        case eSuperNightState_Tripod:
            if (frameIndex <= 0 ) {
                MY_LOGE("invalid frameIndex:%u", frameIndex);
                return false;
            }
            else if (frameIndex <= 3 ) {
                evSetting = -2;
            }
            else if (frameIndex <= 6 ) {
                evSetting = -1;
            }
            else if (frameIndex <= 10 ) {
                evSetting = 0;
            }
            else if (frameIndex <= 13 ) {
                evSetting = 1;
            }
            else if (frameIndex <= 17 ) {
                evSetting = 2;
            }
            break;
        case eSuperNightState_Handheld:
            if (frameIndex <= 0 ) {
                MY_LOGE("invalid frameIndex:%u", frameIndex);
                return false;
            }
            else if (frameIndex <= 1 ) {
                evSetting = -2;
            }
            else if (frameIndex <= 2 ) {
                evSetting = -1;
            }
            else if (frameIndex <= 3 ) {
                evSetting = 0;
            }
            else if (frameIndex <= 6 ) {
                evSetting = 1;
            }
            else if (frameIndex <= 7 ) {
                evSetting = 2;
            }
            break;
        case eSuperNightState_Face:
            if (frameIndex <= 0 ) {
                MY_LOGE("invalid frameIndex:%u", frameIndex);
                return false;
            }
            else if (frameIndex <= 1 ) {
                evSetting = -2;
            }
            else if (frameIndex <= 2 ) {
                evSetting = -1;
            }
            else if (frameIndex <= 3 ) {
                evSetting = 0;
            }
            else if (frameIndex <= 6 ) {
                evSetting = 1;
            }
            else if (frameIndex <= 7 ) {
                evSetting = 2;
            }
            break;
        default:
            MY_LOGE("invalid and unknow super night state:%d", superNightState);
            return false;
    }
    //
    MY_LOGI_IF(mbDebug, "superNightState:%u, frameIndex:%u, evSetting:%d",
               superNightState, frameIndex, evSetting);
    return true;
}

auto
CustomSuperNightEvDecision::
getCurrent3AInfo(
    Current3AInfo& current3AInfo
) -> bool
{
    bool ret = true;
    auto hal3a = mHal3a;
    CaptureParam_T captureParam;
    FrameOutputParam_T frameOutpurParam;
    //
    if (hal3a.get()) {
        std::lock_guard<std::mutex> _l(mHal3aLocker);
        ExpSettingParam_T expParam;
        hal3a->send3ACtrl(E3ACtrl_GetExposureInfo,  (MINTPTR)&expParam, 0);
        hal3a->send3ACtrl(E3ACtrl_GetExposureParam, (MINTPTR)&captureParam, 0);
        hal3a->send3ACtrl(E3ACtrl_GetRTParamsInfo, (MINTPTR)&frameOutpurParam, 0);

    }
    else {
        MY_LOGE("create IHal3A instance failed! cannot get current 3A info");
        ::memset(&captureParam, 0, sizeof(CaptureParam_T));
        ::memset(&frameOutpurParam, 0, sizeof(FrameOutputParam_T));
        ret = false;
    }
    current3AInfo.iso = captureParam.u4RealISO;
    current3AInfo.exposureTime = captureParam.u4Eposuretime; //us
    //current3AInfo.luxValue = frameOutpurParam.i4LuxValue_x10000; // lux value (0.0001 ~ 100000)*10000
    current3AInfo.avgY = frameOutpurParam.u4AvgY; // preview average brightness (0 ~ 255)

    MY_LOGD("get 3A info(iso:%u, shutter:%u us, luxValue:%u, avgY:%u)",
            current3AInfo.iso, current3AInfo.exposureTime, current3AInfo.luxValue, current3AInfo.avgY);
    return ret;
}
