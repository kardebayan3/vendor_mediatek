/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_EVDECION_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_EVDECION_H_
//
//// MTKCAM
#include <mtkcam/def/common.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Trace.h>
 #include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
//
#include <map>
#include <memory>
#include <vector>

/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {
namespace evdecision {


/**
 * Used on the input of evaluateEvDecision()
 */
struct DecisionInputParams
{
    /**
     * Request number, sent at the request stage.
     *
     */
    uint32_t                                    requestNo = 0;

    /**
     * Request App metadata control, sent at the request stage.
     *
     */
    IMetadata const*                            pAppControlMetadata = nullptr;
};


/**
 * Used in the following structures:
 * Used on the output of evaluateEvDecision()
 */
struct DecisionOutputParams
{
    /**
     * Additional metadata
     *
     * EV Decision provide frames setting by hal/app metadata control.
     */
    std::shared_ptr<IMetadata>     pAdditionalAppControlMetadata = nullptr;
    std::shared_ptr<IMetadata>     pAdditionalHalControlMetadata = nullptr;

    /**
     * Frame Index Hint
     **/
    // return SuperNight frame index for debug
    uint32_t frameIndex = 0;
    // use to hint the last frame, to append dummy frames for preview resume stable.
    bool isLastFrame = false;
    // use to hint keep the tuning buffer for reprocess, suggest to keep one of 0EV frame.
    bool isKeepTuning = false;
};


/**
 * A structure for creation parameters.
 */
struct  CreationParams
{
    int32_t                                     openId = -1;
    int32_t                                     sensorId = -1;
    /**
     * The operation mode of pipeline.
     * The caller must promise its value.
     */
    uint32_t                                    operationMode = 0;

    /**
     * Config App metadata control.
     *
     */
    IMetadata const*                            pSessionParams = nullptr;
};

/**
 *
 */
class EvDecision
{
public:
    virtual         ~EvDecision() = default;

    /**
    * The API is in charge of hint the reprocessing has been triggered to reset the frame count of EV setting.
    *
    * @return
    *      true indicates success; otherwise failure.
    */
    virtual auto    reprocessTriggered(
                    ) -> bool                                                = 0;

    /**
     * The SPI is in charge of query the ev setting and requirement at the SuperNight request stage.
     *
     * @param[out] out:
     *  Callers must ensure it's a non-nullptr.
     *
     * @param[in] in:
     *  Callers must promise its content. The callee is not allowed to modify it.
     *
     * @return
     *      true indicates success; otherwise failure.
     */
    virtual auto    evaluateEvDecision(
                        DecisionOutputParams* out,
                        DecisionInputParams const* in
                    ) -> bool                                               = 0;

};


auto createEvDecisionInstance(
    CreationParams const& params
) -> std::shared_ptr<EvDecision>;



/******************************************************************************
 *
 ******************************************************************************/
};  //namespace
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_EVDECION_H_

