/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_UTILS_STREAMINFO_METASTREAMINFO_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_UTILS_STREAMINFO_METASTREAMINFO_H_
//
#include <string>
//
#include <mtkcam3/pipeline/stream/IStreamInfo.h>
#include "BaseStreamInfoImp.h"


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace Utils {


/**
 * meta stream info builder.
 */
class MetaStreamInfoBuilder
{
public:
    virtual         ~MetaStreamInfoBuilder() = default;
    virtual auto    build() const -> android::sp<IMetaStreamInfo>;

public:
    virtual auto    setStreamName(std::string&& name) -> MetaStreamInfoBuilder& {
                        mStreamName = name;
                        return *this;
                    }

    virtual auto    setStreamId(StreamId_T streamId) -> MetaStreamInfoBuilder& {
                        mStreamId = streamId;
                        return *this;
                    }

    virtual auto    setStreamType(MUINT32 streamType) -> MetaStreamInfoBuilder& {
                        mStreamType = streamType;
                        return *this;
                    }

    virtual auto    setMaxBufNum(size_t maxBufNum) -> MetaStreamInfoBuilder& {
                        mMaxBufNum = maxBufNum;
                        return *this;
                    }

    virtual auto    setMinInitBufNum(size_t minInitBufNum) -> MetaStreamInfoBuilder& {
                        mMinInitBufNum = minInitBufNum;
                        return *this;
                    }

protected:  ////    Data Members.
    std::string     mStreamName{"unknown"};
    StreamId_T      mStreamId = -1L;
    MUINT32         mStreamType = 0;

    size_t          mMaxBufNum = 0;
    size_t          mMinInitBufNum = 0;

};


/**
 * metadata stream info.
 */
class MetaStreamInfo : public IMetaStreamInfo
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IStreamInfo Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Attributes.

    virtual char const*             getStreamName() const;

    virtual StreamId_T              getStreamId() const;

    virtual MUINT32                 getStreamType() const;

    virtual size_t                  getMaxBufNum() const;

    virtual MVOID                   setMaxBufNum(size_t count);

    virtual size_t                  getMinInitBufNum() const;

    virtual android::String8        toString() const override;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////                    Instantiation.
                                    MetaStreamInfo(
                                        char const* streamName,
                                        StreamId_T  streamId,
                                        MUINT32     streamType,
                                        size_t      maxBufNum,
                                        size_t      minInitBufNum = 0
                                    );

protected:  ////                    Data Members.
    BaseStreamInfoImp               mImp;               /**< base implementator. */

};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace Utils
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_UTILS_STREAMINFO_METASTREAMINFO_H_

