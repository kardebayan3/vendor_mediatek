/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_INCLUDE_MTKCAM_V3_SYNCHELPER_SYNCHELPERBASE_H_
#define _MTK_HARDWARE_INCLUDE_MTKCAM_V3_SYNCHELPER_SYNCHELPERBASE_H_

#include <vector>
#include <semaphore.h>
#include <utils/KeyedVector.h>
#include <utils/Mutex.h>
#include <chrono>

#include "mtkcam3/pipeline/utils/SyncHelper/ISyncHelperBase.h"

using namespace std;

/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace Utils {
namespace Imp {

/******************************************************************************
 *
 ******************************************************************************/
class SyncContext {

private:
    std::vector<int>  mSyncCam;

public:
    SyncContext()
    {
        sem_init(&mSyncSem, 0, 0);
        sem_init(&mResultSem, 0, 0);
        mStatus = status_Inited;
    }

    ~SyncContext()
    {
        sem_destroy(&mSyncSem);
        sem_destroy(&mResultSem);
        mStatus = status_Uninit;
    }
    sem_t             mSyncSem;
    sem_t             mResultSem;
    syncStatus        mStatus;
    DefaultKeyedVector<int, int64_t> mTimeStampList;
    bool              mSyncDrop = false;
};
/******************************************************************************
 *
 ******************************************************************************/
class Timer
{
public:
    Timer();
    ~Timer();
public:
    std::chrono::duration<double> getTimeDiff();
private:
    std::chrono::time_point<std::chrono::system_clock> pre_time;
};
/******************************************************************************
 *
 ******************************************************************************/

class SyncHelperBase
    : public virtual ISyncHelperBase
{
private:
    struct sync_result
    {
        std::size_t ref_count = 0;
        bool result = false;
    };
private:
    KeyedVector<int, shared_ptr<SyncContext>> mContextMap;

    std::vector<int>  mSyncQueue;
    std::vector<int>  mResultQueue;
    mutable Mutex     mSyncQLock;
    mutable Mutex     mResultQLock;
    std::atomic<int>  mUserCounter;
    // time diff for debug
    Timer mSyncTimer;
    Timer mResultTimer;
    // flush lock
    mutable Mutex     mFlushLock;
    bool mbFlush = false;
    // context lock
    mutable Mutex     mContextMapLock;
    //
    mutable Mutex     mCamMapLock;
    DefaultKeyedVector<int, int> mCamMapByFrameNo;
    mutable Mutex     mCamSyncResultLock;
    DefaultKeyedVector<int, sync_result> mCamSyncResultByFrameNo;
    //
    int mLogLevel = -1;

public:
    SyncHelperBase();
    virtual ~SyncHelperBase();

    status_t start(int CamId) override;
    status_t stop(int CamId) override;
    status_t init(int CamId) override;
    status_t uninit(int CamId) override;
    status_t syncEnqHW(SyncParam &sParam) override;
    status_t syncResultCheck(SyncParam &sParam) override;
    status_t flush(int CamId) override;

private:
    // request map
    bool add_frameno_to_map(SyncParam &sParam);
    bool remove_frameno_from_map(SyncParam &sParam, bool forceRemove = false);
    // enque hw util
    bool enque_sync_check(
                            SyncParam &sParam,
                            std::vector<int> &searchSyncResult);
    bool earse_sync_queue_by_cam_id(int CamId);
    status_t do_sync_wait(int CamId);
    status_t do_sync_post(int CamId);
    // sync result check util
    bool result_sync_check(
                            SyncParam &sParam,
                            std::vector<int> &searchResult);
    bool earse_result_queue_by_cam_id(int CamId);
    status_t do_result_wait(int CamId);
    status_t do_result_post(int CamId);
    bool get_timestamp_check_result(
                                        int64_t time1,
                                        int64_t time2,
                                        int64_t tolerance);
    bool process_drop_frame_check(SyncParam &sParam);
    bool add_timestamp(int CamId, int frameNo, int64_t &timestamp);
    bool remove_timestamp(int CamId, int frameNo);
    bool get_timestamp(int CamId, int frameNo, int64_t &timestamp);
    bool add_timestamp_check_result(int CamId, int frameNo, std::size_t initRefCount, bool result);
    bool get_timestamp_check_result(int CamId, int frameNo, bool &result);
    bool remove_timestamp_check_result(int CamId, int frameNo);
    //
    shared_ptr<SyncContext> getSyncContext(int CamId);
    //
    bool isFlush();

};

/******************************************************************************
 *
 ******************************************************************************/
}
}
}
}
/******************************************************************************
 *
 ******************************************************************************/

#endif // _MTK_HARDWARE_INCLUDE_MTKCAM_V3_HWNODE_P2_UTILS_H_
