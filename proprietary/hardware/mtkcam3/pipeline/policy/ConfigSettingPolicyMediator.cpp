/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-ConfigSettingPolicyMediator"

#include <mtkcam3/pipeline/policy/IConfigSettingPolicyMediator.h>
#include <mtkcam3/pipeline/policy/InterfaceTableDef.h>
//
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#include "MyUtils.h"


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3::pipeline::policy::pipelinesetting;

#define ThisNamespace   ConfigSettingPolicyMediator_Default

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
#define RETURN_IF_ERROR(_expr_, fmt, arg...)                            \
    do {                                                                \
        int const err = (_expr_);                                       \
        if( CC_UNLIKELY(err != 0) ) {                                   \
            MY_LOGE("err:%d(%s) - " fmt, err, ::strerror(-err), ##arg); \
            return err;                                                 \
        }                                                               \
    } while(0)


/******************************************************************************
 *
 ******************************************************************************/
class ThisNamespace
    : public IConfigSettingPolicyMediator
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Data Members.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    Static data (unchangable)
    std::shared_ptr<PipelineStaticInfo const>       mPipelineStaticInfo;
    std::shared_ptr<PipelineUserConfiguration const>mPipelineUserConfiguration;
    std::shared_ptr<PolicyTable const>              mPolicyTable;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Instantiation.
                    ThisNamespace(MediatorCreationParams const& params);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IConfigSettingPolicyMediator Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Interfaces.

    virtual auto    evaluateConfiguration(
                        ConfigurationOutputParams& out,
                        ConfigurationInputParams const& in
                    ) -> int override;

};


/******************************************************************************
 *
 ******************************************************************************/
std::shared_ptr<IConfigSettingPolicyMediator>
makeConfigSettingPolicyMediator_Default(MediatorCreationParams const& params)
{
    return std::make_shared<ThisNamespace>(params);
}


/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
ThisNamespace(MediatorCreationParams const& params)
    : IConfigSettingPolicyMediator()
    , mPipelineStaticInfo(params.pPipelineStaticInfo)
    , mPipelineUserConfiguration(params.pPipelineUserConfiguration)
    , mPolicyTable(params.pPolicyTable)
{
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
evaluateConfiguration(
    ConfigurationOutputParams& out,
    ConfigurationInputParams const& in __unused
) -> int
{
    //---------------------------------
    // 1st level

    featuresetting::ConfigurationInputParams featureIn;
    featuresetting::ConfigurationOutputParams featureOut;
    featureIn.pSessionParams = &mPipelineUserConfiguration->pParsedAppConfiguration->sessionParams;
    featureIn.isP1DirectFDYUV = mPipelineStaticInfo->isP1DirectFDYUV;

    // check zsl enable tag in config stage
    MUINT8 bConfigEnableZsl = false;
    if(IMetadata::getEntry<MUINT8>(featureIn.pSessionParams, MTK_CONTROL_CAPTURE_ZSL_MODE, bConfigEnableZsl))
    {
        MY_LOGI("ZSL mode in SessionParams meta (0x%x) : %d", MTK_CONTROL_CAPTURE_ZSL_MODE, bConfigEnableZsl);
    } else {
        // get default zsl mode
        android::sp<IMetadataProvider const>
        pMetadataProvider = NSMetadataProviderManager::valueForByDeviceId(mPipelineStaticInfo->openId);
        IMetadata::IEntry const& entry = pMetadataProvider->getMtkStaticCharacteristics()
                                         .entryFor(MTK_CONTROL_CAPTURE_DEFAULT_ZSL_MODE);
        if ( !entry.isEmpty() ) {
            bConfigEnableZsl = entry.itemAt(0, Type2Type<MUINT8>());
            MY_LOGI("default ZSL mode in static meta (0x%x) : %d", MTK_CONTROL_CAPTURE_DEFAULT_ZSL_MODE, bConfigEnableZsl);
        }
    }
    featureIn.isZslMode = (bConfigEnableZsl) ? true: false;
    // VR mode do not to enable ZSL
    if(mPipelineUserConfiguration->pParsedAppImageStreamInfo->hasVideoConsumer)
    {
        MY_LOGD("Force to disable ZSL in VR");
        featureIn.isZslMode = false;
    }
    if((mPipelineUserConfiguration->pParsedAppImageStreamInfo->pAppImage_Input_Yuv.get())||
        (mPipelineUserConfiguration->pParsedAppImageStreamInfo->pAppImage_Output_Priv.get())||
        (mPipelineUserConfiguration->pParsedAppImageStreamInfo->pAppImage_Input_Priv.get()))
    {
        MY_LOGD("Force to disable ZSL in reprocessing mode");
        featureIn.isZslMode = false;
    }

    RETURN_IF_ERROR(
        mPolicyTable->mFeaturePolicy->evaluateConfiguration(
        &featureOut, &featureIn),
        "mFeaturePolicy->evaluateConfiguration"
    );

    *(out.pStreamingFeatureSetting) = featureOut.StreamingParams;
    *(out.pCaptureFeatureSetting)   = featureOut.CaptureParams;
    if (out.pIsZSLMode != nullptr)
    {
        *(out.pIsZSLMode) = featureIn.isZslMode;
    }

    if(!in.bypassNodeNeedPolicy)
    {
        RETURN_IF_ERROR(
        mPolicyTable->fConfigPipelineNodesNeed(Configuration_PipelineNodesNeed_Params{
            .pOut = out.pPipelineNodesNeed,
            .pPipelineStaticInfo = mPipelineStaticInfo.get(),
            .pPipelineUserConfiguration = mPipelineUserConfiguration.get()}),
        "fConfigPipelineNodesNeed" );
    }

    RETURN_IF_ERROR(
        mPolicyTable->fConfigPipelineTopology(Configuration_PipelineTopology_Params{
            .pOut = out.pPipelineTopology,
            .pPipelineNodesNeed = out.pPipelineNodesNeed,
            .pPipelineStaticInfo = mPipelineStaticInfo.get(),
            .pPipelineUserConfiguration = mPipelineUserConfiguration.get()}),
        "fConfigPipelineTopology" );

    //---------------------------------
    // 2nd level

    if(!in.bypassSensorSetting)
    {
        RETURN_IF_ERROR(
            mPolicyTable->fConfigSensorSetting(Configuration_SensorSetting_Params{
                .pvOut = out.pSensorSetting,
                .pStreamingFeatureSetting = out.pStreamingFeatureSetting,
                .pPipelineStaticInfo = mPipelineStaticInfo.get(),
                .pPipelineUserConfiguration = mPipelineUserConfiguration.get()}),
            "fConfigSensorSetting" );
    }

    //---------------------------------
    // 3rd level

    RETURN_IF_ERROR(
        mPolicyTable->fConfigP1HwSetting(Configuration_P1HwSetting_Params{
            .pvOut = out.pP1HwSetting,
            .pSensorSetting = out.pSensorSetting,
            .pStreamingFeatureSetting = out.pStreamingFeatureSetting,
            .pPipelineNodesNeed = out.pPipelineNodesNeed,
            .pPipelineStaticInfo = mPipelineStaticInfo.get(),
            .pPipelineUserConfiguration = mPipelineUserConfiguration.get()}),
        "fConfigP1HwSetting" );

    RETURN_IF_ERROR(
        mPolicyTable->fConfigP1DmaNeed(
            out.pP1DmaNeed,
            out.pP1HwSetting,
            out.pStreamingFeatureSetting,
            mPipelineStaticInfo.get(), mPipelineUserConfiguration.get()),
        "fConfigP1DmaNeed" );

    RETURN_IF_ERROR(
        mPolicyTable->fConfigStreamInfo_P1(Configuration_StreamInfo_P1_Params{
            .pvOut = out.pParsedStreamInfo_P1,
            .pvP1HwSetting = out.pP1HwSetting,
            .pvP1DmaNeed = out.pP1DmaNeed,
            .pPipelineNodesNeed = out.pPipelineNodesNeed,
            .pCaptureFeatureSetting = out.pCaptureFeatureSetting,
            .pPipelineStaticInfo = mPipelineStaticInfo.get(),
            .pPipelineUserConfiguration = mPipelineUserConfiguration.get()}),
        "fConfigStreamInfo_P1" );

    //---------------------------------
    // 4th level

    RETURN_IF_ERROR(
        mPolicyTable->fConfigStreamInfo_NonP1(Configuration_StreamInfo_NonP1_Params{
            .pOut = out.pParsedStreamInfo_NonP1,
            .pPipelineNodesNeed = out.pPipelineNodesNeed,
            .pCaptureFeatureSetting = out.pCaptureFeatureSetting,
            .pPipelineStaticInfo = mPipelineStaticInfo.get(),
            .pPipelineUserConfiguration = mPipelineUserConfiguration.get()}),
        "fConfigStreamInfo_NonP1" );

    return OK;
}

