/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PipelineFrameBuilder"

#include <impl/PipelineFrameBuilder.h>
//
#include "MyUtils.h"

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::v3::NSPipelineContext;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


/******************************************************************************
 *
 ******************************************************************************/
static auto getDebugLogLevel() -> int32_t
{
    return ::property_get_int32("persist.vendor.debug.camera.log", 0);
}
static int32_t gLogLevel = getDebugLogLevel();


/******************************************************************************
 *
 ******************************************************************************/
static void print(android::Printer& printer, BuildPipelineFrameInputParams const& o)
{
    printer.printFormatLine(".requestNo=%u", o.requestNo);

    if  (auto p = o.pAppImageStreamBuffers) {
        android::String8 os;
        os += "App image stream buffers=";
        os += toString(*p);
        printer.printLine(os.c_str());
    }
    if  (auto p = o.pAppMetaStreamBuffers) {
        android::String8 os;
        os += "App meta stream buffers=";
        for (auto const& v : *p) {
            os += "\n    ";
            os += v->toString();
        }
        printer.printLine(os.c_str());
    }
    if  (auto p = o.pHalImageStreamBuffers) {
        android::String8 os;
        os += "Hal image stream buffers=";
        for (auto const& v : *p) {
            os += "\n    ";
            os += v->toString();
        }
        printer.printLine(os.c_str());
    }
    if  (auto p = o.pHalMetaStreamBuffers) {
        android::String8 os;
        os += "Hal meta stream buffers=";
        for (auto const& v : *p) {
            os += "\n    ";
            os += v->toString();
        }
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pvUpdatedImageStreamInfo) {
        android::String8 os;
        os += "Updated image stream info=";
        for (auto const& v : *p) {
            os += "\n    ";
            os += v.second->toString();
        }
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pnodeSet) {
        android::String8 os;
        os += ".nodes={ ";
        for (auto const& v : *p) {
            os += android::String8::format("%#" PRIxPTR " ", v);
        }
        os += "}";
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pRootNodes) {
        android::String8 os;
        os += ".root=";
        os += NSCam::v3::NSPipelineContext::toString(*p);
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pEdges) {
        android::String8 os;
        os += ".edges=";
        os += toString(*p);
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pnodeIOMapImage) {
        android::String8 os;
        os += "IOMap(image)";
        for (auto const& v : *p) {
            os += android::String8::format("\n    <nodeId %#" PRIxPTR ">=", v.first);
            os += toString(v.second);
        }
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pnodeIOMapMeta) {
        android::String8 os;
        os += "IOMap(meta)";
        for (auto const& v : *p) {
            os += android::String8::format("\n    <nodeId %#" PRIxPTR ">=", v.first);
            os += toString(v.second);
        }
        printer.printLine(os.c_str());
    }

    if  (auto p = o.pCallback.unsafe_get()) {
        android::String8 os;
        os += android::String8::format(".pCallback=%p", p);
        printer.printLine(os.c_str());
    }
}


/******************************************************************************
 *
 ******************************************************************************/
static void dumpToLog(
    BuildPipelineFrameInputParams const& o,
    android_LogPriority logPriority,
    bool isDumpPipelineContext
)
{
    android::LogPrinter logPrinter(LOG_TAG, logPriority, "");
    logPrinter.printLine("BuildPipelineFrameInputParams=");
    print(logPrinter, o);

    if (isDumpPipelineContext) {
        if  (auto p = o.pPipelineContext.get()) {
            logPrinter.printLine(".pPipelineContext=");
            p->dumpState(logPrinter, {});
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto buildPipelineFrame(
    android::sp<IPipelineFrame>& out __unused,
    BuildPipelineFrameInputParams const& in __unused
) -> int
{
    CAM_TRACE_NAME(__FUNCTION__);

    RequestBuilder builder;
    builder.setReprocessFrame(in.bReprocessFrame);
    builder.setRootNode( *in.pRootNodes );
    builder.setNodeEdges( *in.pEdges );

    if  (auto&& p = in.pvUpdatedImageStreamInfo) {
        for ( auto&& s : *p ) {
            builder.replaceStreamInfo( s.first, s.second );
        }
    }

    // IOMap of Image/Meta
    for ( auto key : *(in.pnodeSet) ) {
        auto const& it_image = in.pnodeIOMapImage->find(key);
        auto const& it_meta  = in.pnodeIOMapMeta->find(key);
        builder.setIOMap(
                key,
                (it_image !=in.pnodeIOMapImage->end() ) ? it_image->second : IOMapSet::empty(),
                (it_meta !=in.pnodeIOMapMeta->end() )   ? it_meta->second  : IOMapSet::empty()
            );
    }

    // set StreamBuffers of Image/Meta
    // app images and check this image stream is physical or not.
    std::vector<int32_t> physicalIdList;
    if  (auto&& pAppImageStreamBuffers = in.pAppImageStreamBuffers) {
        for ( auto&& s : pAppImageStreamBuffers->vAppImage_Output_Proc ) {
            if ( s.second.get() )
                builder.setImageStreamBuffer(s.first, s.second);
                if(strlen(s.second->getStreamInfo()->getPhysicalCameraId()) != 0)
                {
                    MY_LOGD_IF(gLogLevel>=2, "contian physical stream (%s)", s.second->getStreamInfo()->getPhysicalCameraId());
                    auto id = atoi(s.second->getStreamInfo()->getPhysicalCameraId());
                    auto iter = std::find(physicalIdList.begin(), physicalIdList.end(), id);
                    if(iter == physicalIdList.end())
                        physicalIdList.push_back(id);
                }
        }
#define SET_APP_IMAGE(_pSBuffer_, _builder_)                                        \
    do {                                                                            \
        auto&& p = _pSBuffer_;                                                      \
        if ( p.get() )  {                                                           \
            _builder_.setImageStreamBuffer(p->getStreamInfo()->getStreamId(), p );  \
            MY_LOGD("setImageStreamBuffer for app image (%#" PRIx64 ")",p->getStreamInfo()->getStreamId());  }      \
    } while(0)

        SET_APP_IMAGE(pAppImageStreamBuffers->pAppImage_Input_Yuv, builder);
        SET_APP_IMAGE(pAppImageStreamBuffers->pAppImage_Output_Priv, builder);
        SET_APP_IMAGE(pAppImageStreamBuffers->pAppImage_Input_Priv, builder);
        SET_APP_IMAGE(pAppImageStreamBuffers->pAppImage_Output_RAW16, builder);
        SET_APP_IMAGE(pAppImageStreamBuffers->pAppImage_Input_RAW16, builder);
        SET_APP_IMAGE(pAppImageStreamBuffers->pAppImage_Jpeg, builder);

    // set physical camera setting
    builder.setPhysicalCameraSetting(physicalIdList);
#undef SET_APP_IMAGE
    }

    // app meta
    if  (auto&& pSBuffers = in.pAppMetaStreamBuffers) {
        for ( auto&& pSBuffer : *pSBuffers ) {
            builder.setMetaStreamBuffer(pSBuffer->getStreamInfo()->getStreamId(), pSBuffer);
        }
    }

    // hal image
    if  (auto&& pSBuffers = in.pHalImageStreamBuffers) {
        for ( auto&& pSBuffer : *pSBuffers ) {
            builder.setImageStreamBuffer(pSBuffer->getStreamInfo()->getStreamId(), pSBuffer);
        }
    }

    // hal meta
    if  (auto&& pSBuffers = in.pHalMetaStreamBuffers) {
        for ( auto&& pSBuffer : *pSBuffers ) {
            builder.setMetaStreamBuffer(pSBuffer->getStreamInfo()->getStreamId(), pSBuffer);
        }
    }

    sp<IPipelineFrame> pFrame = builder
        .updateFrameCallback(in.pCallback)
        .build(in.requestNo, in.pPipelineContext);
    if  ( CC_UNLIKELY( pFrame.get()==nullptr ) ) {
        MY_LOGE("IPipelineFrame build fail(%u)", in.requestNo);
        dumpToLog(in, ANDROID_LOG_ERROR, true);
        return -EINVAL;
    }

    out = pFrame;

    if  ( CC_UNLIKELY(gLogLevel >= 2) )
    {
        dumpToLog(in, ANDROID_LOG_INFO, (gLogLevel >= 3));
    }
    return OK;
}
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

