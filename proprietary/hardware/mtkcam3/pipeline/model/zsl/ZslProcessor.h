/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_ZSLPROCESSOR_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_ZSLPROCESSOR_H_

#include <utils/Timers.h>
#include "impl/IZslProcessor.h"
#include "impl/IHistoryBufferContainer.h"
#include "mtkcam3/pipeline/pipeline/PipelineContext.h"
#include "mtkcam3/pipeline/utils/streambuf/StreamBuffers.h"
#include "mtkcam3/pipeline/model/types.h"
#include "mtkcam/utils/metadata/IMetadata.h"
#include <map>

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace std;
using namespace NSCam;
using namespace NSCam::v3::NSPipelineContext;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::pipeline::policy;


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


class ZslProcessor
    : public IZslProcessor
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Definitions.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    typedef Utils::HalMetaStreamBuffer::Allocator
                                    HalMetaStreamBufferAllocatorT;
    typedef IHistoryBufferContainer::BufferSet_T  BufferSet_T;
    typedef IHistoryBufferContainer::MetaSet_T  MetaSet_T;
    typedef IHistoryBufferContainer::ImageSet_T  ImageSet_T;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Data Members.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    Instantiation data (initialized at the creation stage).
    int32_t const                           mOpenId = -1;
    std::string                             mUserName;
    int32_t                                 mLogLevel = 0;
    //

protected:  ////    Configuration data (initialized at the configuration stage).
    android::sp<IStreamInfoSet>             mCfgInfoSet;
    std::vector<MINTPTR>                    mvUserId;   // set node ID to collect meta result
    std::vector<ParsedStreamInfo_P1>        mvParsedStreamInfo_P1;  // reserve for buffer selection for ZSL request
    android::wp<IPipelineModelCallback>     mPipelineModelCallback;
    android::sp<IHistoryBufferContainer>    mpHBC;

protected:  ////    Request data.
    std::map< uint32_t, std::shared_ptr<InputZslRequestParams>const >
                                            mPendingRequest;
    int64_t                                 mPendingReqTimeMs;
    int64_t                                 mLastTimeMS;

    android::Vector<uint32_t>               mvInflightZslRequestNo;     // Record ZSL request No in pipeline now
    mutable Mutex                           mLock;
    int64_t                                 mZSLTimeDiffMs;
    int32_t                                 mZSLForcePolicy;
    int64_t                                 mZSLForceTimeOutMs;
    uint32_t                                mLastSelBufReq;
    android::Vector<uint32_t>               mvFakeShutterRequestNo;

private:
    void dumpPending();
    void dumpInputZslRequestParams(std::shared_ptr<InputZslRequestParams>const &rpParams);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Implementations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:
    void
    callBackShutter( uint32_t reqNo,                   // in
                               int64_t shutterTimeStamp);       // in

    android::status_t
    buildFrame_Locked(shared_ptr<InputZslRequestParams> pZslReqParam,      // in
                                Vector<BufferSet_T>& vZSLBufSets,       // in
                                vector<sp<IPipelineFrame>>&        vFrame);            // out
    void
    prepareHalImg(const ImageSet_T&  vImageSet,        // in
                          vector<sp<HalImageStreamBuffer>>& vpHalImg);    // out

    void
    prepareMeta(MBOOL   bSubFrame,  // in
                       const vector<sp<IMetaStreamBuffer>>& vpAppMeta,         // in
                       const vector<sp<HalMetaStreamBuffer>>& vpHalMeta,       // in
                       const MetaSet_T&     vMetaSet,           // in
                       vector<sp<IMetaStreamBuffer>>& vpAppMetaZsl,       // out
                       vector<sp<HalMetaStreamBuffer>>& vpHalMetaZsl);     // out
    void
    appendToP1OutAppMeta(const vector<sp<IMetaStreamBuffer>>& vpAppMeta,         // in
                                     sp<HalMetaStreamBuffer>& pMetaBuffer                   // in/out
                                     );
    void
    appendToP1OutHalMeta(android::sp<IMetaStreamBuffer>& pStreamBuffer,  // in
                                      android::sp<IMetaStreamInfo>& spStreamInfo,    // in
                                     const vector<sp<HalMetaStreamBuffer>>& vpHalMeta,       // in
                                     sp<HalMetaStreamBuffer>& pMetaBuffer           // out
                                     );
    android::status_t
    selectBuf_Locked(uint32_t reqNo,        // in
                               shared_ptr<policy::pipelinesetting::RequestOutputParams> pCapParams, // in
                               Vector<BufferSet_T>& vBufSetsOut);    // out
    void
    selectBufByTimeStamp(Vector<BufferSet_T>& vHBC_Buf_Sets,  // in
                                    int64_t ReqTimeStampDiff,           // in
                                    size_t& TimeSelBuf);                 // out

    MBOOL checkQuality(const MetaSet_T &rvResult,int32_t ZslPolicy);
    MBOOL isAfOkBuffer(const MetaSet_T &rvResult);
    MBOOL isAeOkBuffer(const MetaSet_T &rvResult);
    MBOOL isFameSyncOkBuffer(const MetaSet_T &rvResult);
    MBOOL isPDProcessRawBuffer(const MetaSet_T &rvResult);
    MINT64 getTimeStamp(const MetaSet_T &rvResult);

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Instantiation.
                    ZslProcessor(int32_t openId, std::string const& name);

public:     ////    Operations.
    virtual auto    dump() -> void;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IZslProcessor Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Interfaces.
    /**
     * The IZslProcessor is in charge of reporting its requirement at the configuration stage.
     *
     * @param[in] in:
     *  Callers must promise its content. The callee is not allowed to modify it.
     *
     * @param[out] out:
     *  On this call, the callee must allocate and set up its content.
     *
     * @return
     *      OK indicates success; otherwise failure.
     */
    virtual auto    configure(
                         shared_ptr<InputZslConfigParams>const& in,
                         shared_ptr<OutputZslConfigParams>& out
                    ) -> android::status_t override ;

    /**
     * Get the Zsl stream buffer provider. This call is valid after configure().
     *
     * @return
     *      The instance of IStreamBufferProvider.
     */
    virtual auto    getStreamBufferProvider() const
                        -> android::sp<Utils::IStreamBufferProvider> override;

    /**
     * The IZslProcessor is in charge of evaluating zsl/non-zsl flow might be applied to this input
     * params from User(application) and Capture Setting Policy module.
     *
     * @param[in] in:
     *  Callers must promise its content. The callee is not allowed to modify it.
     *
     * @param[out] out:
     *  On this call, the callee must allocate and set up its content.
     *
     * @return
     *      OK indicates success; otherwise failure.
     */
    virtual auto    submitZslRequest(
                         shared_ptr<InputZslRequestParams>const& in,
                         shared_ptr<OutputZslRequestParams>& out
                    ) -> android::status_t override ;

    /**
     * The IZslProcessor has pending zsl capture request. User should submitZslRequest even
     * though there exists no zsl capture request in current reqeust.
     *
     * @param[in] in:
     *
     * @param[out] out:
     *
     * @return
     *      true indicates has pending request;
     */
    virtual auto    hasPendingZslRequest() const -> bool override ;

    /**
     * The IZslProcessor is in charge of update configured metadata to HistoryBufferContainer.
     *
     * @param[in] in:
     *  Callers must promise its content. The callee is not allowed to modify it.
     *
     * @param[out] out:
     *  On this call, the callee must allocate and set up its content.
     *
     * @return
     *
     */
    virtual auto    onFrameUpdated(
                        uint32_t const requestNo,
                        MINTPTR  const userId,
                        IPipelineBufferSetFrameControl::IAppCallback::Result const& result
                    ) -> void override ;

    /**
     * The IZslProcessor is in charge of pipeline flush and mark error for StreamBuffer
     *
     * @param[in] in:
     *
     * @param[out] out:
     *
     * @return
     *
     */
    virtual auto    flush(
                        shared_ptr<OutputZslRequestParams>& out
                    ) -> void override ;


    virtual auto    setBufferEnqueCnt(
                        MUINT32 const iRequestNo,
                        size_t iStreamCnt,
                        size_t iMetaCnt
                    ) -> void;

};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_ZSLPROCESSOR_H_

