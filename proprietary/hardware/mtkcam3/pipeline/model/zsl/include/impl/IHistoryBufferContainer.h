/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_IHISTORYBUFFERCONTAINER_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_IHISTORYBUFFERCONTAINER_H_
//
#include "IBufferPool.h"

#include <mtkcam3/pipeline/utils/streambuf/IStreamBufferProvider.h>

#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <string>

//

/******************************************************************************
 *
 ******************************************************************************/
//
namespace NSCam {
namespace v3 {

/**
 * HistoryBufferContainer:
 *
 * This module can extend lifecycle of user-specified StreamBuffers for two purposes:
 * (1) ZSD capture: acquire history buffer set (including image & metadata) and determine
 *                  one as ZSD capture's source.
 * (2) performance: acquire history buffer set (including image & metadata) to ignore isp pass1
 *                  flow.
 *
 * Users(PipelineModels/HwNodes) MUST interact with this module in these three stages:
 *
 * 1. CONFIGURATION STAGE:
 *      APIs: beginConfigStreams() / endConfigStreams() / setBufferSize() / setBufferPool()
 *      a) User(pipeline) creates instance of this module and set as image StreamBufferProvider.
 *      b) User(pipeline) determine how many streams (image+metadata) are the target of this module
 *         to keep track for extending thier lifecycle.
 *      c) This module is resposible for image streambuffers allocation which is implemented
 *         through BufferPool mechanism. User can assign BufferPool in configuration time.
 *      d) This module is not responsible for meta streambuffers allocation. User can extend meta
 *         streambuffers' lifecycle by enque operation.
 *
 * 2. STREAMING REQUEST/CALLBACK STAGE:
 *      APIs: enqueMetaBuffers() / dequeStreamBuffer() / enqueStreamBuffer()
 *      a) This module will keep track of enqued meta & image for each request.
 *      b) This module track history buffer set with a request number, which directly maps to
 *         request number from framework.
 *      c) In this stage, meta and image might be enqued in different timestamps. This module
 *         determines internal state machine for each history buffer set. Furthermore, this module
 *         MUST auto-release oldest unuse history buffer set if no more available images can be
 *         allocated from BufferPool.
 *      d) User(pipeline) enque result meta of configured streams to this module for further use.
 *      e) User(nodes) deque image streambuffers from this module through pipelinecontext mechanism.
 *         Finally, these image streambuffers MUST be enqued back when all users release through
 *         pipelineframe mechanism.
 *
 * 3. ZSD CAPTURE REQUEST/CALLBACK STAGE:
 *      APIs: getAvailableRequestList() / acquireBufferSetsOfList() / acquireBufferSets() /
 *            cancelBufferSets() / cancelBufferSet / returnBufferSets() /
 *            dequeStreamBuffer() / enqueStreamBuffer()
 *      a) User(pipeline) determine whether utilize history buffer or not for each capture request.
 *      b) User(pipeline) can get list of available request.
 *      c) User(pipeline) can acquire one or multiple of history buffer from previous list result
 *         and set specific request number from framework. User(pipeline) might determine which
 *         history buffer set is suitable for capture request.
 *      d) User(pipeline) MUST cancel history buffer set if the set is not suitable to use.
 *      e) User(pipeline) MUST use acquired image and meta buffers to construct pipelineframe.
 *      f) User(pipeline) MUST return when zsl request done.
 *      g) The image streambuffers MUST be enqued back when all users release throght pipelineframe
 *         mechanism as STREAMING STAGE(2.e).
 *
 */

class IHistoryBufferContainer
    : public Utils::IStreamBufferProvider
{
public:
    static auto     createInstance(
                        int32_t const deviceId,
                        std::string const& name
                    ) -> android::sp<IHistoryBufferContainer>;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Definition.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:   //// definition.
    typedef Utils::HalImageStreamBuffer HalImageStreamBuffer;

    typedef android::DefaultKeyedVector<StreamId_T, android::sp<HalImageStreamBuffer> >
                                        ImageSet_T;
    typedef android::DefaultKeyedVector<StreamId_T, android::sp<IMetaStreamBuffer> >
                                        MetaSet_T;
    /**
     * BufferSet_T:
     * This structure is defined to carry vector of image and meta streambuffers.
     *
     * SrcRequest_T:
     * This structure is to carry key of source from history buffer. User invoke returnBuffers
     * with this structure to inform the relation of previous streaming and current zsd capture
     * request.
     */
    struct BufferSet_T {
        ImageSet_T      vImageSet;
        MetaSet_T       vMetaSet;
        uint32_t        mRefReqNo = 0;
    };

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:   //// operations.

    /**
     * beginConfigStreams:
     *
     * initial with configured streams, comprising image and metadata streams.
     * This module will create BufferPool mechanism for image streams, whereas leave buffer
     * allocation timing to endConfigStreams() because user could replace BufferPool.
     *
     * @param[in] rpInfoSet: set of image and meta streaminfo;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    beginConfigStreams(
                        const android::sp<IStreamInfoSet>& rpInfoSet
                    ) -> android::status_t                                  = 0;

    /**
     * endConfigStreams:
     *
     * allocate image streambuffers through BufferPool mechanism. Pool allocates buffers of an
     * inited numbers and then return. Other buffers will be allocated in another background thread.
     *
     * @param[in]:
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    endConfigStreams() -> android::status_t                 = 0;

    /**
     * setBufferSize:
     *
     * reset Pool size of specific images. This method can be called in configuration time or
     * run-time.
     *
     * @param[in] rvBufferSize: vector of image size;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    setBufferSize(
                        const android::KeyedVector<StreamId_T, size_t>& rvBufferSize
                    ) -> android::status_t                                  = 0;

    /**
     * setBufferPool:
     *
     * reset Pool inatance of specific images. This method can be called only in configuration time.
     *
     * @param[in] rvBufferPool: vector of image pool;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    setBufferPool(
                        const android::KeyedVector<StreamId_T, android::sp<IBufferPool> >& rvBufferPool
                    ) -> android::status_t                                  = 0;

    /**
     * enqueBuffers:
     *
     * enque vector of bufferset with start request number. This method can be called multiple
     * time for each request. User can enque result metadata from updateFrame.
     *
     * @param[in] rvBufferSet: vector of bufferset;
     *
     * @param[in] startRequestNo: the request number of first frame;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    enqueMetaBuffers(
                        const android::Vector< android::sp<IMetaStreamBuffer> >& rvAppResult,
                        const android::Vector< android::sp<IMetaStreamBuffer> >& rvHalResult,
                        uint32_t const requestNo
                    ) -> android::status_t                                  = 0;


    /**
     * getAvailableRequestList:
     *
     * get available list from history buffer.
     *
     * @param[out] rvRequestNo: vector of available request number;
     *
     * @param[in] pRequiredStreamSet: preserved for further use;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    getAvailableRequestList(
                        android::Vector<uint32_t>& rvRequestNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet = nullptr
                    ) -> android::status_t                                  = 0;

    /**
     * acquireBuffers:
     *
     * acquire vector of bufferset from history buffer with start request number. User MUST
     * determine which one or multiple history buffer are suitable for further use. This module
     * MUST fill SrcRequest_T stucture, and user MUST set this information for later cancelBuffers
     * and returnBuffers operation.
     *
     * @param[out] rvBufSet: vector of acquired bufferset;
     *
     * @param[in] requestNo: the referenced history request number of first frame;
     *
     * @param[in] frameCnt: the expected size;
     *
     * @param[in] pRequiredStreamSet: prserved for further use;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    acquireBufferSetsOfList(
                        const android::Vector<uint32_t>& rvRequestNo,
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet = nullptr
                    ) -> android::status_t                                  = 0;

    virtual auto    acquireBufferSets(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet = nullptr
                    ) -> android::status_t                                  = 0;


    /**
     * cancelBuffers:
     *
     * cancel vector of bufferset from history buffer with start request number. This module might
     * query key of internal map from SrcRequest_T stucture.
     *
     * @param[in] rvBufSet: vector of canceled bufferset;
     *
     * @param[in] requestNo: the referenced history request number of first frame;
     *
     * @param[in] frameCnt: the canceled size;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    cancelBufferSets(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo
                    ) -> void                                               = 0;

    virtual auto    cancelBufferSet(
                        BufferSet_T& rBufSet,
                        uint32_t const newRequestNo
                    ) -> void                                               = 0;

    /**
     * returnBuffers:
     *
     * return vector of bufferset of zsd request with start request number. This module might
     * query key of internal map from SrcRequest_T stucture. And then, this module might link
     * referenced history request number to expected new request number. Later, deque operation
     * of image streambuffer with new request number will pass from referenced request number.
     *
     * @param[in] rvBufSet: vector of returned bufferset;
     *
     * @param[in] requestNo: the expected new request number of first frame;
     *
     * @param[in] frameCnt: the returned size;
     *
     * @return: 0 indicates success; non-zero indicates an error code.
     */
    virtual auto    returnBufferSets(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo
                    ) -> void                                               = 0;

    virtual auto    setBufferEnqueCnt(
                        MUINT32 const iRequestNo,
                        size_t iStreamCnt,
                        size_t iMetaCnt) -> void                            = 0;

    /**
     * clear:
     *
     * @param[in] requestNo: the speficied request number;
     */
    virtual auto    clear(uint32_t const requestNo) -> void                 = 0;

    virtual auto    clear() -> void                                         = 0;

    virtual auto    dump() -> void                                         = 0;

};

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_IHISTORYBUFFERCONTAINER_H_

