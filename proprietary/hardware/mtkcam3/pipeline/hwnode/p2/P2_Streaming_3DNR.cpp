/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <cutils/properties.h>
#include "P2_StreamingProcessor.h"

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    Streaming_3DNR
#define P2_TRACE        TRACE_STREAMING_3DNR
#include "P2_LogHeader.h"
#include <mtkcam3/feature/3dnr/3dnr_defs.h>
#include <mtkcam3/feature/eis/eis_ext.h>
#include <camera_custom_3dnr.h>

#define UNUSED(var) (void)(var)

using namespace NSCam;
using namespace NSCam::NR3D;
using namespace NSCam::v3;
using namespace NSCam::NSCamFeature::NSFeaturePipe;

// anonymous namespace
namespace
{
    static MINT32 getIso3DNR(const sp<P2::P2Request> &request)
    {
        const MINT32 DEFAULT_ISO = 100;
        MINT iso = request->mP2Pack.getSensorData().mISO;
        if( iso == INVALID_P1_ISO_VAL )
        {
            iso = DEFAULT_ISO;
        }
        return iso;
    }
}

namespace P2
{

MVOID StreamingProcessor::init3DNR()
{
    m3dnrDebugLevel = property_get_int32("vendor.camera.3dnr.log.level", 0);

    for( MUINT32 sensorID : mP2Info.getConfigInfo().mAllSensorID )
    {
        mUtil3dnrMap[sensorID] = new Util3dnr(sensorID);
        mUtil3dnrMap[sensorID]->init(mP2Info.getConfigInfo().mUsageHint.m3DNRMode &
            E3DNR_MODE_MASK_HAL_FORCE_SUPPORT);
    }

    MY_LOGD("usageHint.3DNRMode(0x%x)", mP2Info.getConfigInfo().mUsageHint.m3DNRMode);
}


MVOID StreamingProcessor::uninit3DNR()
{
    mUtil3dnrMap.clear();
}


MBOOL StreamingProcessor::is3DNRFlowEnabled(
    MINT32 force3DNR, IMetadata *appInMeta, IMetadata *halInMeta, const ILog &log) const
{
    MINT32 e3DnrMode = MTK_NR_FEATURE_3DNR_MODE_OFF;
    MINT32 eHal3DnrMode = MTK_NR_FEATURE_3DNR_MODE_OFF;

    if( appInMeta == NULL ||
        !tryGet<MINT32>(appInMeta, MTK_NR_FEATURE_3DNR_MODE, e3DnrMode) )
    {
        MY_LOGD("no MTK_NR_FEATURE_3DNR_MODE: appInMeta: %p", appInMeta);
    }

    if (halInMeta == NULL ||
        !tryGet<MINT32>(halInMeta, MTK_DUALZOOM_3DNR_MODE, eHal3DnrMode))
    {
        // Only valid for dual cam. On single cam, we don't care HAL meta,
        // and can assume HAL is "ON" on single cam.
        eHal3DnrMode = MTK_NR_FEATURE_3DNR_MODE_ON;
    }

    if (force3DNR)
    {
        char EnableOption[PROPERTY_VALUE_MAX] = {'\0'};
        property_get("vendor.debug.camera.3dnr.enable", EnableOption, "n");
        if (EnableOption[0] == '1')
        {
            e3DnrMode = MTK_NR_FEATURE_3DNR_MODE_ON;
        }
        else if (EnableOption[0] == '0')
        {
            e3DnrMode = MTK_NR_FEATURE_3DNR_MODE_OFF;
        }
    }

    if (m3dnrDebugLevel >= 1)
    {
        MY_S_LOGD(log,
            "[3DNR] Meta App: %d, Hal(dual-cam): %d", e3DnrMode, eHal3DnrMode);
    }

    return (e3DnrMode == MTK_NR_FEATURE_3DNR_MODE_ON &&
            eHal3DnrMode == MTK_NR_FEATURE_3DNR_MODE_ON);
}

MBOOL StreamingProcessor::getInputCrop3DNR(
    MBOOL &isEIS4K, MBOOL &isIMGO, MRect &inputCrop,
    P2Util::SimpleIn& input, const ILog &log) const
{
    MSize nr3dInputSize = input.getInputSize();

    isEIS4K = isEIS12() &&
              ((nr3dInputSize.w >= VR_UHD_W && nr3dInputSize.h >= VR_UHD_H) ||
               (nr3dInputSize.h >= VR_UHD_W && nr3dInputSize.w >= VR_UHD_H));

    isIMGO = !(input.isResized());
    sp<Cropper> cropper = input.mRequest->getCropper();

    if (isEIS4K)
    {
        MUINT32 cropFlag = Cropper::USE_EIS_12;
        cropFlag |= input.isResized() ? Cropper::USE_RESIZED : 0;
        MCropRect cropRect = cropper->calcViewAngle(
            log,
            cropper->getActiveCrop().s,
            cropFlag);
        inputCrop.p = cropRect.p_integral;
        inputCrop.s = cropRect.s;
    }
    else if (isIMGO)
    {
        inputCrop = cropper->getP1Crop();
    }
    else
    {
        inputCrop.p.x = 0;
        inputCrop.p.y = 0;
        inputCrop.s = nr3dInputSize;
    }

    if (m3dnrDebugLevel >= 2)
    {
        MY_S_LOGD(log,
            "[3DNR] isEIS4K: %d, isIMGO: %d, input(%d,%d), inputCrop(%d,%d;%d,%d)",
            (isEIS4K ? 1 : 0), (isIMGO ? 1 : 0),
            nr3dInputSize.w, nr3dInputSize.h,
            inputCrop.p.x, inputCrop.p.y, inputCrop.s.w, inputCrop.s.h);
    }

    return (isEIS4K || isIMGO);
}

MINT32 StreamingProcessor::get3DNRIsoThreshold(MUINT32 /*sensorID*/, MUINT8 ispProfile) const
{
    MINT32 isoThreshold = NR3DCustom::get_3dnr_off_iso_threshold(ispProfile, mP2Info.getConfigInfo().mUsageHint.m3DNRMode &
            E3DNR_MODE_MASK_HAL_FORCE_SUPPORT);
    MY_LOGD_IF(m3dnrDebugLevel,"Get isoThreshold : %d", isoThreshold);
    return isoThreshold;
}

MBOOL StreamingProcessor::prepare3DNR_FeatureData(
    MBOOL en3DNRFlow, MBOOL isEIS4K, MBOOL isIMGO,
    P2Util::SimpleIn& input, P2MetaSet &/*metaSet*/, MUINT8 ispProfile, const ILog &log) const
{
    UNUSED(log);
    TRACE_S_FUNC_ENTER(log);

    FeaturePipeParam &featureParam = input.mFeatureParam;
    sp<P2Request> &request = input.mRequest;
    const sp<Util3dnr>& util3dnr = mUtil3dnrMap.at(request->getSensorID());
    if (util3dnr == NULL)
    {
        MY_LOGW("No util3dnr!");
        return MFALSE;
    }
    sp<Cropper> cropper = request->getCropper();

    LMVInfo lmv = cropper->getLMVInfo();
    NR3DMVInfo mvInfo;
    mvInfo.status = lmv.is_valid ? NR3DMVInfo::VALID : NR3DMVInfo::INVALID;
    mvInfo.x_int = lmv.x_int;
    mvInfo.y_int = lmv.y_int;
    mvInfo.gmvX = lmv.gmvX;
    mvInfo.gmvY = lmv.gmvY;
    mvInfo.confX = lmv.confX;
    mvInfo.confY = lmv.confY;
    mvInfo.maxGMV = lmv.gmvMax;

    MINT32 iso = getIso3DNR(request);
    MINT32 isoThreshold = get3DNRIsoThreshold(request->getSensorID(), ispProfile);
    MBOOL canEnable3dnrOnThisFrame = util3dnr->canEnable3dnr(en3DNRFlow, iso, isoThreshold);
    MSize rrzoSize = cropper->getP1OutSize();
    MRect p1Crop = cropper->getP1Crop();

    util3dnr->modifyMVInfo(MTRUE, isIMGO, p1Crop, rrzoSize, mvInfo);
    util3dnr->prepareFeatureData(
        canEnable3dnrOnThisFrame, mvInfo, iso, isoThreshold, isEIS4K, featureParam);
    util3dnr->prepareGyro(NULL, &featureParam);

    TRACE_S_FUNC_EXIT(log);
    return canEnable3dnrOnThisFrame;
}

MBOOL StreamingProcessor::prepare3DNR(P2Util::SimpleIn& input, const ILog &log) const
{
    TRACE_S_FUNC_ENTER(log);

    MBOOL en3DNRFlow = MFALSE;
    MBOOL forceSupport =
        ((mP2Info.getConfigInfo().mUsageHint.m3DNRMode & E3DNR_MODE_MASK_HAL_FORCE_SUPPORT) != 0);
    MBOOL uiSupport =
        ((mP2Info.getConfigInfo().mUsageHint.m3DNRMode & E3DNR_MODE_MASK_UI_SUPPORT) != 0);
    if (uiSupport || forceSupport)
    {
        P2MetaSet metaSet = input.mRequest->getMetaSet();
        en3DNRFlow = is3DNRFlowEnabled(forceSupport, &metaSet.mInApp, &metaSet.mInHal, log);
        if (en3DNRFlow)
        {
            FeaturePipeParam &featureParam = input.mFeatureParam;
            featureParam.setFeatureMask(MASK_3DNR, en3DNRFlow);
            featureParam.setFeatureMask(MASK_3DNR_RSC, (mP2Info.getConfigInfo().mUsageHint.m3DNRMode &
                NSCam::NR3D::E3DNR_MODE_MASK_RSC_EN));
        }

        MBOOL isEIS4K = MFALSE, isIMGO = MFALSE;
        MRect inputCrop;
        MUINT8 ispProfile = 0;
        if (!tryGet<MUINT8>(&metaSet.mInHal, MTK_3A_ISP_PROFILE, ispProfile))
        {
            MY_LOGD_IF(m3dnrDebugLevel,"no ISPProfile from HalMeta");
        }

        prepare3DNR_FeatureData(en3DNRFlow, isEIS4K, isIMGO, input, metaSet, ispProfile, log);

    }

    if (m3dnrDebugLevel >= 2)
    {
        MY_S_LOGD(log, "[3DNR] en3DNRFlow: %d, forceSupport: %d",
                (en3DNRFlow ? 1 : 0), (forceSupport ? 1 : 0));
    }
    TRACE_S_FUNC_EXIT(log);
    return en3DNRFlow;
}

}; // namespace P2
