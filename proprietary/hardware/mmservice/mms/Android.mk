#for hidl lib
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
LOCAL_SRC_FILES := \
    Mms.cpp

LOCAL_C_INCLUDES := \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/dpframework/include \
    $(TOP)/$(MTK_PATH_SOURCE)/external/libion_mtk \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/gralloc_extra/include \
    $(TOP)/system/core/libion/include/ion \
    $(TOP)/device/mediatek/common/kernel-headers/linux \
    $(TOP)/system/core/base/include

LOCAL_SHARED_LIBRARIES := \
    libhidlbase \
    libhidltransport \
    libhwbinder \
    liblog \
    libutils \
    libcutils \
    libion \
    libion_mtk \
    libhardware \
    libdpframework \
    libgralloc_extra \
    vendor.mediatek.hardware.mms@1.0 \
    vendor.mediatek.hardware.mms@1.1

ifneq (,$(filter $(strip $(TARGET_BOARD_PLATFORM)), mt6765))
LOCAL_CFLAGS += -DMMS_SUPPORT_JPG_ENC

LOCAL_C_INCLUDES += \
    $(TOP)/$(MTK_PATH_SOURCE)/hardware/jpeg/include/enc

LOCAL_SHARED_LIBRARIES += \
    libJpgEncPipe
endif

LOCAL_MODULE := vendor.mediatek.hardware.mms@1.1-impl
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_RELATIVE_PATH := hw
include $(MTK_SHARED_LIBRARY)