ANDROID_NN_DRIVER_LOCAL_PATH := $(call my-dir)
LOCAL_PATH := $(ANDROID_NN_DRIVER_LOCAL_PATH)

my_device := mtk_device
my_platform := $(shell echo $(MTK_PLATFORM) | tr A-Z a-z )

ifneq ($(filter $(TARGET_BUILD_VARIANT),eng),)
my_build_variant := prebuilt_eng
else
my_build_variant := prebuilt_user
endif

SERVICE_MODULE := android.hardware.neuralnetworks@1.1-service-gpunn

MTKNN_ARM_SHARED_LIBRARIES := \
        libcutils \
        libneuropilot_hal_utils

include $(CLEAR_VARS)
LOCAL_INIT_RC := $(my_device)/$(my_platform)/$(my_build_variant)/$(SERVICE_MODULE).rc
LOCAL_MODULE := $(SERVICE_MODULE)
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_SRC_FILES := $(my_device)/$(my_platform)/$(my_build_variant)/arm64/$(SERVICE_MODULE)
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_PROPRIETARY_MODULE := true

LOCAL_SHARED_LIBRARIES := \
        libbase \
        libhidlbase \
        libhidltransport \
        libhidlmemory \
        libdl \
        libhardware \
        liblog \
        libtextclassifier_hash \
        libutils \
        android.hardware.neuralnetworks@1.0 \
        android.hardware.neuralnetworks@1.1 \
        android.hidl.allocator@1.0 \
        android.hidl.memory@1.0 \
        libOpenCL
LOCAL_SHARED_LIBRARIES += $(MTKNN_ARM_SHARED_LIBRARIES)
LOCAL_PREFER_SOURCE_PATH := $(MTK_PATH_SOURCE)/hardware/gpu_nn
include $(MTK_PREBUILT)


include $(ANDROID_NN_DRIVER_LOCAL_PATH)/$(my_device)/Android.mk
